﻿using System.Collections;
using System;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using TMPro;
using UnityEngine.UI;
using System.Net.NetworkInformation;

public class Client : MonoBehaviour {

    private static Client instance = null;

    public static Client Instance
    {
        get { return instance; }
    }

    public GameObject chatCointainer;
    public ChatList chatList;//a list of the chat messages. So we can truncate them later.

    //private int tips; //THIS NEEDS TO BE CHANGED TO GREEN GEMS

    public InputField clientName;
    public InputField messageInput;

    public int clientSpriteInt = 0;

    private bool socketReady = false;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamReader reader;
    private StreamWriter writer;
    public string mySeed = "";

    private string host = "13.52.99.220"; //IP of AWS Server
    private int port = 6321; //port of AWS server

    public float dataTimer = 0; //this checks how long its been since response from server after sent data
    public bool dataTimerOn = false;
    public float dataTimeOut = 10;

    public float timeSinceLastMsg = 3f;
    public Image sendButtonCooldown;

    public string clanTag;


    private bool reconnectRoutineStarted = false;

    //these are the gameobjects for signing in. they will dissappear after the player has connected. they will reappear if the player is disconnected.
    public GameObject leftIconArrow;
    public GameObject rightIconArrow;
    public GameObject playerIconObj;
    public GameObject nameField;
    public GameObject connectButton;

    public GameObject guildHolder;
    public GameObject fadedBkg;

    //these are the sending message GOs. Type message and send message
    public GameObject sendMessagePanel;

    //for the chatroom button
    public GameObject chatRoomHolder;//this is an empty obj that is used to show and not show the chatroom.
    public GameObject chatRoomNotification; //for when the user receives a message and the chat room is minimized.
    //for tips
    public IngredientsMaster ingredientsScript;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

        public void Start()
    {
        mySeed = UnityEngine.Random.Range(0,9999999).ToString();

        if (PlayerPrefs.GetString("UserName") == "")
        {
            //do nothing
        }
        else
        {
            clientName.text = PlayerPrefs.GetString("UserName"); //gets the last used name for them to use when they logon
        }

    }

    public void ConnectToServer()
    {
        //if we are already connected, ignore this fuinction

        if (socketReady)
            return;

        if (clientName.text == "") //if they didnt input anything and nothing is saved
            clientName.text = "Guest";

        try
        {
            initiateConnection();
            disableEnableLoginButtons(false); //this will DISABLE the login buttons
            enableChatMessaging(true);//if the player was able to connect show the chat messages
        }
        catch (Exception e)
        {
            //Debug.Log("Socket error " + e.Message);
            chatList.addMessageToList("***Could not connect, please try again later", -1, "");//this adds the chat message to a list of messages. if messages are longer than a certain amt it deletes the old messages. data is the message text
        }
    }

    private void initiateConnection()
    {
        socket = new TcpClient(host, port); //created a socket with the servers host and port
        stream = socket.GetStream();//the data stream
        writer = new StreamWriter(stream);//write to a data stream
        reader = new StreamReader(stream);
        socketReady = true; //we are now ready to use our socket

        if (!reconnectRoutineStarted)
        {
            StartCoroutine(checkReconnect());
        }

    }

    private void disableEnableLoginButtons(bool boolHolder) //set to true when you want to activate these GOs, false when u want to deactivate
    {
    leftIconArrow.SetActive(boolHolder);
    rightIconArrow.SetActive(boolHolder);
    playerIconObj.SetActive(boolHolder);
    nameField.SetActive(boolHolder);
    connectButton.SetActive(boolHolder);
    }

    private void enableChatMessaging(bool boolHolder) //this will enable the area to type message and send button. Only after player has conncted. Disable when player disconnectes.
    {
    sendMessagePanel.SetActive(boolHolder);
    }

    public void openChatRoom(bool boolHolder)
    {
        
        chatRoomHolder.SetActive(boolHolder);
        guildHolder.SetActive(boolHolder);
        fadedBkg.SetActive(boolHolder);

        if (chatRoomNotification != null)
        {
            chatRoomNotification.SetActive(false);
        }

        if (boolHolder == true)
        {
            Camera.main.GetComponent<MainMenu>().windowOpen = true;
        }
        else
        {
            Camera.main.GetComponent<MainMenu>().windowOpen = false;

        }

    }

    private void Update()
    {
        if (socketReady) //to check for reconnect, maybe put a timer here for like 15 seconds that checks if you test the socket that connects to the server. See socket = new tcpCLient(host,port) above
        {

            if (stream.DataAvailable)
            {
                dataTimerOn = false;
                dataTimer = 0;

                string data = reader.ReadLine();
                if (data != null)
                {

                    OnIncomingData(data);
                }
            }

            timeoutCheck(); //10 seconds to get a response from server. If no response, print server unavailable

        }

        if (timeSinceLastMsg > 0)
        {
            timeSinceLastMsg -= Time.deltaTime;
            sendButtonCooldown.fillAmount = timeSinceLastMsg / 3f; //3 is the max time wait for message
        }
    }

    private void timeoutCheck()
    {
        if (dataTimerOn == true)
        {
            dataTimer += Time.deltaTime;

            if (dataTimer >= 10)
            {
              //  print("server not available!");

                StopCoroutine(checkReconnect()); //so it doesnt check for reconnections
                reconnectRoutineStarted = false;

                CloseSocket();//closes the connection with the server

                dataTimerOn = false;
                dataTimer = 0;

                chatList.addMessageToList("***Server not found, please reconnect later", -1, "");//this adds the chat message to a list of messages. if messages are longer than a certain amt it deletes the old messages. data is the message text
                disableEnableLoginButtons(true); //this will ENABLE the login buttons
                enableChatMessaging(false);//if the player was able to connect show the chat messages

            }
        }
    }

    private IEnumerator checkReconnect()
    {
        while (true)
        {
            reconnectRoutineStarted = true; //so we cannot make this routine twice
            yield return new WaitForSeconds(10f);
            //print("checking cnnct with server");

            Send("!"); //checks if the server is still in contact
        }
    }

    private void OnIncomingData(string data)
    {
        if(data == "%NAME")
        {
            Send("&NAME|" + clanTag + clientName.text + " (Lv" + PlayerPrefs.GetInt("Rank").ToString()+")" + "&SPRITE/" + clientSpriteInt.ToString() + "&SEED-" + mySeed);
            return; //so it doesnt print anything initially
        }

        if(data == "!")
        {
            //this is so the client knows when the server has gone offline
            //print("Server connected test received");
            return;
        }

        if (data.Contains("%TIP"))
        {
            string gotTip = data.Split('|')[1];

          //  print("sender seed is" + gotTip);

            Send("&ENDTIP|"+ gotTip);//receives the tip and broadcasts the message that the tip was made
            receiveTip();
            return;
        }

        string testStr = ""; //for storing icon
        string dataStr = data; //this is because we remove the characters that tell us which icon to use
        string seedStr = ""; //for the senders Seed (the person who wrote the msg)

        if (data.Contains("/")) //this will find the first slash, which will always be before the players name and give the index of the icon
        {

            int iconIntLocation = data.IndexOf("/", StringComparison.Ordinal); //finds where the "/" is
            
            testStr = data.Substring(0, iconIntLocation); //testStr is a string that stores which icon that player has

            dataStr = data.Remove(0, iconIntLocation+1); //the +1 removes the "/". This is so we remove the numbers that tell us which icon it is

            int seedIntLocation = dataStr.IndexOf("$", StringComparison.Ordinal); //finds where the "$" is

            seedStr = dataStr.Substring(0, seedIntLocation); //end of the seed str

            dataStr = dataStr.Remove(0, seedIntLocation + 1); //removes all text up to the $ sign to just get the message

        }
        else
        {
            testStr = "-1"; //it wont change the image if it finds -1
            //seed str will be ""
        }

        //note testStr could be -1 if just initialized. If so, dont change the sprite
        chatList.addMessageToList(dataStr, int.Parse(testStr) , seedStr);//this adds the chat message to a list of messages. if messages are longer than a certain amt it deletes the old messages. data is the message text

        //check if chat windows is open, if its not open, show the notification of a new msg
        if (chatRoomHolder.activeSelf == true)
        {

        }
        else
        {
            if (chatRoomNotification != null)
            {
                chatRoomNotification.SetActive(true);
            }
        }

    }

    private void Send(string data)
    {

        if (!socketReady)
        {
            //print("there is no server connection");
            return;
        }

        //print("you are sending : " + data);

            writer.WriteLine(data);
            writer.Flush();
            dataTimerOn = true; //this will start the timer to see if the server is connected. If >10 seconds. We will know server is down and we can try to reconnect

        if (data != "!") //if this isnt a message that checks if we are still connected to the server or not
        {
            timeSinceLastMsg = 3f;
            //print("send button refresh!");
        }

    }

    public void OnSendButton()
    {
        string message = messageInput.text;//GameObject.Find("SendInput").GetComponent<TMP_InputField>().text;

        if (message == "")//checking if they are sending a blank message
            return;
        if (timeSinceLastMsg > 0)
            return;

        Send(message);

        messageInput.text = ""; //erase the text to protect from spamming
    }

    public void OnTipButton(string messageClicked) //gets a message from the Iconimg script so we can find the username of the person who sent it and send them a tip
    {
        string message = "&TIP|" + messageClicked;

        if (ingredientsScript != null) //in case the button hasnt assigned it to this script yet
        {
            if (ingredientsScript.totalGreenGems > 0) //if u have gems...
            {
                if (PlayerPrefs.GetInt("TipsLeft")>0) { //if u can tip...

                    ingredientsScript.totalGreenGems -= 1; //subs a green gem from the sender
                    ingredientsScript.setNewPrefs(); //saves the new amount of green gems
                    PlayerPrefs.SetInt("TipsLeft", PlayerPrefs.GetInt("TipsLeft") - 1);//-1 tips
                    chatList.addMessageToList("You have: " + PlayerPrefs.GetInt("TipsLeft").ToString() + " tips remaining today.", -1, "");

                }
                else //if u run out of tips print this msg
                {
                    chatList.addMessageToList("Sorry, you cannot give any more tips today!", -1, "");
                    return;
                }

            }
            else
            {
                chatList.addMessageToList("***You have no gems to tip!", -1, "");
                return;
            }
        }
        else
        {
            return;
        }

        Send(message);

    }

    private void receiveTip() //gets a message from the Iconimg script so we can find the username of the person who sent it and send them a tip
    {
        if (ingredientsScript != null)
        {
                ingredientsScript.totalGreenGems += 1; //subs a green gem from the sender
                ingredientsScript.setNewPrefs(); //saves the new amount of green gems
        }
    }

    private void CloseSocket()
    {
        if (!socketReady)
            return; //if the socket was never connected to server, do nutin

        writer.Close();
        reader.Close();
        socket.Close();
        socketReady = false;

    }

    private void OnApplicationQuit()
    {
        CloseSocket();
    }

    private void OnDisable()
    {
        //CloseSocket();
    }

    public void setClientSpriteInt(int spriteIndex)
    {
        clientSpriteInt = spriteIndex;
    }

    //                    Camera.main.GetComponent<MainMenu>().gpEarnObj.SetActive(true);

    public void startGPEarn()
    {
        StartCoroutine(checkGpEarn());
    }

    private IEnumerator checkGpEarn()
    {
        while (true)
        {

            if (Camera.main.GetComponent<MainMenu>().gpEarnObj.activeSelf == false)
            {
                Camera.main.GetComponent<MainMenu>().gpEarnObj.SetActive(true);
            }

            yield return new WaitForSeconds(3f);
        }
    }
}
