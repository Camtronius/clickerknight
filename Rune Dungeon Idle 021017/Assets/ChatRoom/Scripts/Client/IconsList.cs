﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconsList : MonoBehaviour {

    public List<Sprite> iconList = new List<Sprite>();
    public List<int> unlockedPetsList = new List<int>(); //just the indicies of pets unlocked

    public Client clientIconInt; //int that corresponds to an icon in the icon list (will be pets in clicker knight)
    public PetGenerator petGenerator; //int that corresponds to an icon in the icon list (will be pets in clicker knight)

    public int currentListInt = 0;
    public int currentSpriteIndex = 0;//this is the current index of the pet.

    public Sprite defaultPlayerIcon;
    public Sprite serverIcon;
    public Image ButtonSprite; //not sure which icon this is///

    public Sprite getIconListSprite(int index)
    {
        return iconList[index];
    }

    private void Start()
    {
        currentListInt = 0;
        ButtonSprite.sprite = defaultPlayerIcon;
        clientIconInt.setClientSpriteInt(99);
    }

    public bool checkPetUnlocked()
    {
        for (int i = 0; i < petGenerator.UnlockedPets.Length; i++)
        {
            if (petGenerator.UnlockedPets[i] > 0)
            {
                return true;
            }
        }
        return false;
    }

    public void increaseClientSprite()//when the player clicks the icon to change sprite
    {

        unlockedPetsList.Clear(); //clears the list in case new pets have been added

        unlockedPetsList.Add(99);//this is for the default character sprite

        for (int i = 0; i < petGenerator.UnlockedPets.Length; i++)
        {
            if (petGenerator.UnlockedPets[i] > 0)
            {
                unlockedPetsList.Add(i);
            }
        }

        if (checkPetUnlocked()==true) //if a pet has been unlocked?
        {
            currentListInt += 1;

            if (currentListInt > unlockedPetsList.Count-1)
            {
                currentListInt = 0;
            }

            currentSpriteIndex = unlockedPetsList[currentListInt];

            //print("current sprite index is " + currentSpriteIndex + " the current list int is " + currentListInt);

            if (currentSpriteIndex==99) //if the default sprite is being shown
            {
                //print("showing default sprite!");
                ButtonSprite.sprite = defaultPlayerIcon;
            }
            else
            {
                ButtonSprite.sprite = getIconListSprite(currentSpriteIndex);
            }

            clientIconInt.setClientSpriteInt(currentSpriteIndex); //this is okay if its 99

        }
        else //no pets are unlocked, make the player automatically the default sprite
        {
            ButtonSprite.sprite = defaultPlayerIcon;

            clientIconInt.setClientSpriteInt(99);
        }
    }

    public void decreaseClientSprite()//when the player clicks the icon to change sprite
    {

        unlockedPetsList.Clear(); //clears the list in case new pets have been added

        unlockedPetsList.Add(99);//this is for the default character sprite

        for (int i = 0; i < petGenerator.UnlockedPets.Length; i++)
        {
            if (petGenerator.UnlockedPets[i] > 0)
            {
                unlockedPetsList.Add(i);
            }
        }

        if (checkPetUnlocked() == true) //if a pet has been unlocked?
        {
            currentListInt -= 1;

            if (currentListInt < 0)
            {
                currentListInt = unlockedPetsList.Count - 1;
            }

            currentSpriteIndex = unlockedPetsList[currentListInt];

            //print("current sprite index is " + currentSpriteIndex + " the current list int is " + currentListInt);

            if (currentSpriteIndex == 99) //if the default sprite is being shown
            {
                //print("showing default sprite!");
                ButtonSprite.sprite = defaultPlayerIcon;
            }
            else
            {
                ButtonSprite.sprite = getIconListSprite(currentSpriteIndex);
            }

            clientIconInt.setClientSpriteInt(currentSpriteIndex); //this is okay if its 99

        }
        else //no pets are unlocked, make the player automatically the default sprite
        {
            ButtonSprite.sprite = defaultPlayerIcon;

            clientIconInt.setClientSpriteInt(99);
        }
    }
}
