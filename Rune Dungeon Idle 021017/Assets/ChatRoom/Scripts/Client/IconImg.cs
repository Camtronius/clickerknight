﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IconImg : MonoBehaviour {

    public Image icon;
    public Button tipButton;
    public Text textMessage;
    public Client clientScript; //this is so we can pass the tipping code to the client
    public string seed=""; //this is the persons unique seed which will be used for tipping


    public void pressTipButton()
    {
        clientScript.OnTipButton(seed);
        tipButton.gameObject.SetActive(false); //so they cant press the same button multiple times and spam the server
    }
}
