﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatList : MonoBehaviour {

    public List<GameObject> chatMessages = new List<GameObject>();
    public List<GameObject> serverMessages = new List<GameObject>();


    public IconsList listOfIcons;
    public Client clientClass;
    public GameObject vertLayoutGroup; //this is the layout group that holds the chat messages. We need this so we can create our server messages

    public void addMessageToList(string newMessage , int iconIdentifier, string seedStr)
    {

        //trying to make it so it pools the messages based on the sibling index
        //need to make it so it takes the first sibling index it finds, then moves it to the bottom of the indexes...
        for (int i=0; i<chatMessages.Count; i++)
        {

            if (chatMessages[i].transform.GetSiblingIndex() == 0)
            {
                chatMessages[i].GetComponent<IconImg>().tipButton.gameObject.SetActive(false); //settting tip button initially to false

                chatMessages[i].transform.SetSiblingIndex(vertLayoutGroup.transform.childCount); //sets at the bottom
                chatMessages[i].GetComponentInChildren<Text>().text = newMessage;

                if (iconIdentifier >= 0 && iconIdentifier<99) //this means it actually is a pet icon
                {
                    chatMessages[i].GetComponent<IconImg>().icon.sprite = listOfIcons.getIconListSprite(iconIdentifier);
                    chatMessages[i].SetActive(true);

                }
                else if (iconIdentifier == -1)
                {
                    for (int j=0; j < serverMessages.Count; j++)
                    {

                        if (serverMessages[j].activeSelf == false)
                        {
                            serverMessages[j].GetComponent<Text>().text = newMessage;
                            serverMessages[j].transform.SetSiblingIndex(vertLayoutGroup.transform.childCount);
                            serverMessages[j].SetActive(true);
                            break;
                        }

                    }

                    break;
                }
                else if (iconIdentifier == 99)
                {
                    chatMessages[i].GetComponent<IconImg>().icon.sprite = listOfIcons.defaultPlayerIcon; //set as checkmark if no icons are found
                    chatMessages[i].SetActive(true);

                }

                if (seedStr != "")
                {
                    chatMessages[i].GetComponent<IconImg>().seed = seedStr; //sets the seed identifier for the message. So we know who to send a tip to :)

                    if (seedStr == clientClass.mySeed)
                    {
                        Color myColor = new Color();
                        ColorUtility.TryParseHtmlString("#0097f9", out myColor);

                        chatMessages[i].GetComponent<IconImg>().tipButton.gameObject.SetActive(false);
                        chatMessages[i].GetComponentInChildren<Image>().color = myColor; //the first image component found should be the bkg img
                    }
                    else
                    {
                        Color friendColor = new Color();
                        ColorUtility.TryParseHtmlString("#b5864d", out friendColor);

                        chatMessages[i].GetComponent<IconImg>().tipButton.gameObject.SetActive(true);
                        chatMessages[i].GetComponentInChildren<Image>().color = friendColor;

                    }

                    //print("the received seed is " + seedStr);
                }

                break;
            }
        }

        //check for destroying any server msg's
        checkServerMsgHeirarchy();

    }

    public void checkServerMsgHeirarchy()
        {
            for (int j = 0; j < serverMessages.Count; j++)
            {
                if ((vertLayoutGroup.transform.childCount - serverMessages.Count*2) >= serverMessages[j].transform.GetSiblingIndex()) //this checks if the server message is in a position servmsg.coun-1 in the heirarchy. if so, deactive it so it can be used later
                {
                    serverMessages[j].SetActive(false);
                    serverMessages[j].transform.SetSiblingIndex(vertLayoutGroup.transform.childCount);

                }
            }
    }
}
