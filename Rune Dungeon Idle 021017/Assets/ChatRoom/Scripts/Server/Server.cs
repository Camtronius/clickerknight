﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Net;
using System.IO;
using TMPro; 

public class Server : MonoBehaviour {

    public List<ServerClient> clients;
    public List<ServerClient> disconnectList;

    public int port = 6321;
    private TcpListener server;
    private bool serverStarted;

    public List<GameObject> debugMessages = new List<GameObject>();

    private void Start()
    {
        clients = new List<ServerClient>(); //list of clients connected to the server
        disconnectList = new List<ServerClient>(); //list of clients who have been disconnected

        //print("test seed" + (int)System.DateTime.Now.Ticks);


        try
        {
            //Debug.Log("the ip adress of any is : " + IPAddress.Any);
            server = new TcpListener(IPAddress.Any,port); //this makes a new TcpListener obj and listens to the server at the given IP address and port... IPAddress.Parse("192.168.0.25")
            server.Start(); //starts a server

            StartListening();
            serverStarted = true;
            addDebugMessageToList("server has been started on the port " + port + " and at server" + server);
        }
        catch (Exception e)
        {
            //Debug.Log("Socket Error " + e.Message); //cannot find the server error?

        }
    }

    private void Update()
    {
        if (!serverStarted)
            return;

        foreach(ServerClient c in clients) //checks all clients to see if they are connected
        {
            //is the cilent still connected?

            if (!IsConnected(c.tcp)) //if they are not connected, close their connection and add them to the disconnt list. continue with the foreach.
            {
                c.tcp.Close();
                disconnectList.Add(c);
                break; //changed from continue to break because it would still send data with an incorrect list of clients? 
            }
            else
            {

                NetworkStream s = c.tcp.GetStream(); //checks if there is data from the server for the clients
                if (s.DataAvailable)
                {
                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();

                    if (data != null)
                    {

                        OnIncomingData(c, data);
                    }
                }
            }

            //if he is check for messages from the client
        }

        for (int i = 0; i < disconnectList.Count; i++) //??? maybe this shouldnt be -1?
        {

            addDebugMessageToList("dsc list index: " + i + " name : " + disconnectList[i].clientName);

            //Broadcast(disconnectList[i].clientName + " has left the chat", clients); //this is what causes the write error, but its not too bad...

            clients.Remove(disconnectList[i]); //removes from client list the person who disconnected

            disconnectList.RemoveAt(i);

        }

    }

    private void OnIncomingData(ServerClient c, string data) //serverclient is who sent the message, and the string is the message sent
    {
        if (data.Contains("&NAME"))
        {
            //
            string nameString = data.Split('|')[1];
            int iconIntLocation = nameString.IndexOf("&", StringComparison.Ordinal); //finds where the "/" is
            c.clientName = nameString.Substring(0, iconIntLocation); //testStr is a string that stores which icon that player has
            print("remaing data is : " + nameString);
            //
            string spriteString = nameString.Split('/')[1];
            iconIntLocation = spriteString.IndexOf("&", StringComparison.Ordinal); //finds where the "/" is
            c.spriteInt = int.Parse(spriteString.Substring(0, iconIntLocation)); //testStr is a string that stores which icon that player has
            print("remaing data is : " + spriteString);
            //
            string seedString = spriteString.Split('-')[1];
            c.seedIdentifier = seedString; //because there are no remaining characters, so no need to find an index of "&"
            
            Broadcast(c.clientName + " has connected!", clients); //sends the name of who connected to all the clients
            addDebugMessageToList("user " + c.clientName + " icon " + c.spriteInt + " seed " + c.seedIdentifier);
            return;
        }

        if (data =="!")
        {
            Broadcast("!", new List<ServerClient> { c });

            //addDebugMessageToList("server connection with guest is active ");
            return;
        }

        if (data.Contains("&TIP"))
        {
            
            string receivingTip = data.Split('|',':')[1];
            addDebugMessageToList(c.clientName + " " + c.seedIdentifier + " has tipped " + receivingTip);

            for(int i=0; i<clients.Count; i++)
            {
                if (receivingTip == clients[i].seedIdentifier)
                {
                    Broadcast("%TIP|"+ c.seedIdentifier, new List<ServerClient> { clients[i] });
                }
            }
            return;
        }

        if (data.Contains("&ENDTIP"))
        {
            string receivingTip = data.Split('|')[1];
            addDebugMessageToList("BROADCAST " + c.clientName + " has tipped " + receivingTip);

            for (int i = 0; i < clients.Count; i++)
            {
                if (receivingTip == clients[i].seedIdentifier)
                {
                    Broadcast(clients[i].clientName + " has tipped " + c.clientName  + "! :)", clients);
                }
            }
            return;
        }

        addDebugMessageToList(c.spriteInt + "/" + c.clientName + " has sent the following message " + data); 

        Broadcast(c.spriteInt + "/" + c.seedIdentifier +"$" + c.clientName +  ": " + data,clients);

    }

    private void Broadcast(string data, List<ServerClient> cl)
    {
        
        foreach(ServerClient c in cl)
        {
            try
            {
                StreamWriter writer = new StreamWriter(c.tcp.GetStream());
                writer.WriteLine(data);//write a line of data 
                writer.Flush();//what does this do?
            }
            catch(Exception e)
            {
                addDebugMessageToList("write error: " + e.Message + " to client " + c.clientName);
            }
        }

    }

    private void StartListening() 
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server); //lets the server accept new clients
    }

    private bool IsConnected(TcpClient c)
    {

        try
        {
            if(c!=null && c.Client!=null && c.Client.Connected) //if they are connected
            {

                if (c.Client.Poll(0, SelectMode.SelectRead)) //try sending a byte to see if they receive it
                {
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek)==0); //checks if the person received the byte or not. If not return false
                }
                return true; //not sure why we dont need an else here

            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }

    }

    private void AcceptTcpClient(IAsyncResult ar) //adding a new client to start broadcasting to?
    {
        TcpListener listener = (TcpListener)ar.AsyncState;
        clients.Add(new ServerClient(listener.EndAcceptTcpClient(ar))); //adds the server client to the server
        StartListening();

        //send a message to everyone saying they have connected
        //Broadcast(clients[clients.Count - 1].clientName + " has connected!", clients);
        //code to get their name
        Broadcast("%NAME", new List<ServerClient> { clients[clients.Count-1]}); //this sends a hidden message to the client who just connected asking what their NAME is...? Only plays when client is accepted so this will only be asked once
        //Broadcast("%SPRITE", new List<ServerClient> { clients[clients.Count-1]}); //this sends a hidden message to the client who just connected asking what their SPRITE is...? Only plays when client is accepted so this will only be asked once
        //Broadcast("%SEED", new List<ServerClient> { clients[clients.Count-1]}); //this sends a hidden message to the client who just connected asking what their SEED is...? Only plays when client is accepted so this will only be asked once


    }

    public void addDebugMessageToList(string newMessage)
    {

        //trying to make it so it pools the messages based on the sibling index
        //need to make it so it takes the first sibling index it finds, then moves it to the bottom of the indexes...
        for (int i = 0; i < debugMessages.Count; i++)
        {

            if (debugMessages[i].transform.GetSiblingIndex() == 0)
            {
                debugMessages[i].transform.SetSiblingIndex(debugMessages.Count);
                debugMessages[i].GetComponentInChildren<TextMeshProUGUI>().text = newMessage;
                debugMessages[i].SetActive(true);
                break;

            }

        }

    }

}



public class ServerClient
{
    public TcpClient tcp;
    public string clientName;
    public int spriteInt;
    public string seedIdentifier;

    public ServerClient(TcpClient clientSocket)
    {

        clientName = "guest";
        tcp = clientSocket;
    }



}
