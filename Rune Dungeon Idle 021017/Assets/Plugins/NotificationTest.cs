﻿using UnityEngine;
using System.Collections;

public class NotificationTest : MonoBehaviour
{
    void Awake()
    {
        LocalNotification.ClearNotifications();
    }

    public void OneTime()
    {
        LocalNotification.SendNotification(1, 5000, "Title", "Long message text", new Color32(0xff, 0x44, 0x44, 255));
    }

    public void OneTimeBigIconOneDay()
    {//was 86400000. testing 5 mins instead to see if notifications are working on android 8.0
     // LocalNotification.SendNotification(1, 5000, "Title", "Long message text", new Color32(0xff, 0x44, 0x44, 255));
        if(PlayerPrefs.GetInt("PrestigeLv") >= 1)
        {
            LocalNotification.SendNotification(1, 86400000, "Daily Dungeons Ready!", "Collect Powerful Pets in a Daily Dungeon!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");
        }
        else
        {
            LocalNotification.SendNotification(1, 86400000, "Daily Login Bonus Ready!", "A Ton of Bonus Gold is Waiting For You!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");
        }
    }

	public void OneTimeBigIcon3Days()
    {//was 259200000
        if (PlayerPrefs.GetInt("PrestigeLv") >= 1)
        {
            LocalNotification.SendNotification(2, 259200000, "Event and Dungeons Ready!", "Lots to come back for!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");
        }
        else
        {
            LocalNotification.SendNotification(2, 259200000, "Huge Login Bonus Available!", "Don't Forget to Collect Your Bonus!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");
        }        
    }

	public void OneTimeBigIcon5Days()
    {//was 432000000
        if (PlayerPrefs.GetInt("PrestigeLv") >= 1)
        {
            LocalNotification.SendNotification(3, 432000000, "Gold Bonus, New Event, Dungeons!", "Gold, a New Event, and Dungeons are waiting!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");

        }
        else
        {
            LocalNotification.SendNotification(3, 432000000, "Big Login Bonus Available!", "Don't Forget to Collect that Juicy Login Bonus!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");

        }
    }

    public void NewEventNotification(float hoursRemaining)
    {//was 432000000

        int hoursToMillis = Mathf.FloorToInt(hoursRemaining * 3600000f + 3600000f); //add one hour to the end so it doesnt notify them right away

        LocalNotification.SendNotification(4, hoursToMillis, "New Event Ready!", "Grab Those Event Points and Gems!", new Color32(0xff, 0x44, 0x44, 255), false, false, false, "app_icon");
    }

    public void OneTimeWithActions()
    {
        LocalNotification.Action action1 = new LocalNotification.Action("background", "In Background", this);
        action1.Foreground = false;
        LocalNotification.Action action2 = new LocalNotification.Action("foreground", "In Foreground", this);
		LocalNotification.SendNotification(1, 5000, "Title", "Long message text with actions", new Color32(0xff, 0x44, 0x44, 255), true, true, true, null, "boing", "default", action1, action2);
    }

    public void Repeating()
    {
        LocalNotification.SendRepeatingNotification(1, 5000, 60000, "Title", "Long message text", new Color32(0xff, 0x44, 0x44, 255));
    }

    public void Stop()
    {
        LocalNotification.CancelNotification(1);
		LocalNotification.CancelNotification(2);
		LocalNotification.CancelNotification(3);


    }

    public void OnAction(string identifier)
    {
        Debug.Log("Got action " + identifier);
    }
}
