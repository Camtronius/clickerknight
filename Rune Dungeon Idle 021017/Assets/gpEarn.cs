﻿using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using TMPro;
using UnityEngine.UI;

public class gpEarn : MonoBehaviour {

    public float gpTimerMax = 10f;
    private float gpTimer = 10f;
    public TextMeshProUGUI gpAmtText;
    public Image gpIcon;
    // Update is called once per frame

    private void Start()
    {
        gpTimer = gpTimerMax; //sets time to the max by default
        updGpAmtText();
    }

    void Update ()
    {
            accumulateGP();
	}

    public void updGpAmtText()
    {
        gpAmtText.text = ObscuredPrefs.GetFloat("GP").ToString() + " GP";
    }

    private void accumulateGP()
    {
        gpTimer -= Time.deltaTime;

        gpIcon.fillAmount = 1-gpTimer / gpTimerMax;

        if (gpTimer <= 0)
        {
            ObscuredPrefs.SetFloat("GP", ObscuredPrefs.GetFloat("GP") + 1);
            gpTimer = gpTimerMax;
            updGpAmtText();
        }
    }
}
