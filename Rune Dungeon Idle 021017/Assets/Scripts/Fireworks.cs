﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireworks : MonoBehaviour {

	// Use this for initialization

	public SoundManager soundManager;

	void Start () {

		soundManager = Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ();

	}
	
	// Update is called once per frame
	public void playFireworksSfx(){

		soundManager.fireworks ();

	}

	public void playFanFare(){

		soundManager.fanfare ();

	}
}
