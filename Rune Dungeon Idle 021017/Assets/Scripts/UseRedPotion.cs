﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using CodeStage.AntiCheat.ObscuredTypes;


public class UseRedPotion : MonoBehaviour {

	public enum PotionType {AtkDmgIncr,MagDmgIncr,AtkRateIncr,SplRateIncr,HPregen,AdGold,AdSilver,AdExp,AdRefresh,Event};

	public AtkFlurryCooldown atkFlurryScript;
	public AtkFlurryCooldown splFlurryScript;

	public MegaSlashCooldwn megaSlashScript;
	public FreezeTimeCooldown freezeTimeScript;

    public SoulChargeCooldown soulChargeScript;
    public PetFlurryCooldown petFlurryScript;
    public ShieldCooldown shieldScript;




    public PotionType _potionType;

	public int potionNumber;// is this the first or second potion item in the heirarchy
	public int currentPotionLevel;

	// Use this for initialization
	public float CoolDownTimeMax;
	public float CoolDownTime;
	public Image CoolDownGfx;

	public Image spriteHolder; //thue button target image is the spr

	public Sprite redPotion;
	public Sprite redPotion2;
	public Sprite redPotion3;

	public Sprite bluePotion;
	public Sprite bluePotion2;
	public Sprite bluePotion3;

	public Sprite yellowPotion;
	public Sprite yellowPotion2;
	public Sprite yellowPotion3;

	public Sprite purplePotion;
	public Sprite purplePotion2;
	public Sprite purplePotion3;

	public Sprite greenPotion;
	public Sprite greenPotion2;
	public Sprite greenPotion3;

	public Sprite orangePotion;
	public Sprite orangePotion2;
	public Sprite orangePotion3;

	public Sprite silverPotion;
	public Sprite silverPotion2;
	public Sprite silverPotion3;

    public Sprite whitePotion;
    public Sprite whitePotion2;
    public Sprite whitePotion3;

    //atkModPot;      0
    //magModPot;      1
    //atkRateModPot;  2
    //splRateModPot;  3
    //hpRegenModPot;  4

    //atkModPot2;     5
    //magModPot2;     6
    //atkRateModPot2; 7
    //splRateModPot2; 8
    //hpRegenModPot2; 9

    public float potionUseTime = 0;
	public float potionUseTimeMax = 20;
	public float ThreeSecCount = 3;
	public int currentLevel = 0;

	public ParticleSystem potion1Particles;
	public ParticleSystem potion2Particles;
    public ParticleSystem potion3Particles;


    public Sprite nullSprite;

	public bool isReady=false;
	public bool countingDownReady=false;

	public bool emis1used = false;
	public bool emis2used = false;
    public bool emis3used = false;


    public TextMeshProUGUI timeCountDown;
	public string timeString;

	public PotionMasterScript potionMaster;

	public MainMenu CameraPotionList;
	public float potionAccelerator =1f; //this is so we can test potions faster

	public ActiveAbilityManager abilityManager; //this is for the using potion anim

	void OnEnable () {

        //	potionMaster.loadPotionData ();


        //		print ("looking for the potion data scripot");
        //	potionMaster.potionDataScript = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ();
        potionMaster.loadPotionData();//do this in case no potion data has been loaded yet :)

		CoolDownTime = CoolDownTimeMax;

	//	print ("the potion number is " + potionNumber);
		getStoredPotions (); //gets any saved potions


	}

	// Update is called once per frame
	void Update () {

		if (CoolDownTime >= 0 && countingDownReady==true ) { //&& CameraPotionList.windowOpen==false
			CoolDownTime -= Time.deltaTime;
			CoolDownGfx.fillAmount = CoolDownTime / CoolDownTimeMax;

			//isReady = false;


			timeCountDown.text = timeString;

		} else if(CoolDownTime<0) {
			isReady = true;
				if (PlayerPrefs.GetFloat (this.gameObject.name) > 0) { //checks if the potion was in use and if it was then to use it
					

				usePotion ();
				}

		}

		if (potionUseTime > 0) {
			isReady = false;

			spriteHolder.fillAmount = potionUseTime / potionUseTimeMax;

			if (timeCountDown.gameObject.activeSelf==true) {

			} else {

				timeCountDown.gameObject.SetActive (true);
			}

			timeString = potionUseTime.ToString ();

			if (potionUseTime >= 100) {
				timeString = timeString.Substring (0, 3); //10B
			} else if (potionUseTime > 10 && potionUseTime < 100) {
				timeString = timeString.Substring (0, 2); //10B
			} else {//less tha n10?
				timeString = timeString.Substring (0, 1); //10B
			}

			timeCountDown.text = timeString;

			if (CameraPotionList.windowOpen == false) { //so it doesnt count down potion time when a window is open
				potionUseTime -= Time.deltaTime* potionAccelerator;

				ThreeSecCount -= Time.deltaTime; //just an interval so im not constantly saving to playerprefs

			} else {

			}


			if(ThreeSecCount<=0){ //writes to the player prefs every 3 seconds so im not continually writing to it

				PlayerPrefs.SetFloat (this.gameObject.name, potionUseTime);// this is to save the time so the potion can be reused with less time in another level...
				ThreeSecCount = 3;
			}


			if (potionUseTime <= 0 && spriteHolder.sprite != nullSprite) {

				isReady = true;

				PlayerPrefs.SetInt (_potionType.ToString () + potionNumber.ToString(), 0);
			
				PlayerPrefs.SetFloat (this.gameObject.name, 0);// this is to save the time so the potion can be reused with less time in another level...



				timeCountDown.gameObject.SetActive (false);
				//stop emission of the thing
				spriteHolder.sprite = nullSprite; //this is what tells the game the slot is empty!! (they check for the nullsprite)

				spriteHolder.fillAmount = 1;

				if (emis1used == true) {
				
					ParticleSystem.EmissionModule emis1 = potion1Particles.emission;
					emis1.enabled = false;
					emis1used = false;
					this.GetComponent<Button> ().interactable = true;

					for(int i=0; i<5; i++){
						CameraPotionList.potionModList [i] = 0;

				//		print ("potion + " + i + " is done");
					}


				}

				if (emis2used == true) {

					ParticleSystem.EmissionModule emis2 = potion2Particles.emission;
					emis2.enabled = false;
					emis2used = false;
					this.GetComponent<Button> ().interactable = true;

					for(int i=5; i<10; i++){ 
						CameraPotionList.potionModList [i] = 0;
				//		print ("potion + " + i + " is done");
					}
				}

                if (emis3used == true)
                {

                    ParticleSystem.EmissionModule emis3 = potion3Particles.emission;
                    emis3.enabled = false;
                    emis3used = false;
                    this.GetComponent<Button>().interactable = true;

                    for (int i = 10; i < CameraPotionList.potionModList.Count; i++)
                    {
                        CameraPotionList.potionModList[i] = 0;
                        //		print ("potion + " + i + " is done");
                    }
                }

                //this is so the max buttons go away.

                potionMaster.checkMaxPotions ();
			}
		}

		// spriteHolder is the sprite that holds the potion

	}

	public void getStoredPotions(){

		for (int i = 0; i < 3; i++) {
			
			if (PlayerPrefs.GetInt ("AtkDmgIncr" + (i+1).ToString()) == potionNumber) {
				_potionType = UseRedPotion.PotionType.AtkDmgIncr;
				setPotionAttributes (PlayerPrefs.GetInt ("RedPotLv1Lv")); //this actually just sets the icon

			}
			if (PlayerPrefs.GetInt ("MagDmgIncr" + (i+1).ToString()) == potionNumber) {
				_potionType = UseRedPotion.PotionType.MagDmgIncr;
				setPotionAttributes (PlayerPrefs.GetInt ("BluePotLv1Lv"));

			}
			if (PlayerPrefs.GetInt ("HPregen" + (i+1).ToString()) == potionNumber) {
				_potionType = UseRedPotion.PotionType.HPregen;
				setPotionAttributes (PlayerPrefs.GetInt ("GreenPotLv1Lv"));

			}
			//ad potions
			if (PlayerPrefs.GetInt ("AdGold" + (i+1).ToString()) == potionNumber) {//gold potion for dmg
				_potionType = UseRedPotion.PotionType.AdGold;
				setPotionAttributes (PlayerPrefs.GetInt ("GoldAdPotionGoldLv"));

			}
			if (PlayerPrefs.GetInt ("AdSilver" + (i+1).ToString()) == potionNumber) { //silver potion for dmg/magicdmg
				_potionType = UseRedPotion.PotionType.AdSilver;
				setPotionAttributes (PlayerPrefs.GetInt ("SilverAdPotionDMGLv"));

			}
			if (PlayerPrefs.GetInt ("AdExp" + (i+1).ToString()) == potionNumber) {//gold potion for dmg
				_potionType = UseRedPotion.PotionType.AdExp;
				setPotionAttributes (PlayerPrefs.GetInt ("ExpGainPotionLv"));

			}
			if (PlayerPrefs.GetInt ("AdRefresh" + (i+1).ToString()) == potionNumber) { //silver potion for dmg/magicdmg
				_potionType = UseRedPotion.PotionType.AdRefresh;
				setPotionAttributes (PlayerPrefs.GetInt ("RefreshAbilitiesPotionLv"));

			}
            if (PlayerPrefs.GetInt("Event" + (i + 1).ToString()) == potionNumber)
            { //silver potion for dmg/magicdmg
                _potionType = UseRedPotion.PotionType.Event;
                setPotionAttributes(PlayerPrefs.GetInt("EventAdPotionLv"));

            }


        }

		potionMaster.checkMaxPotions (); //check for the max when gettins saved pots
	}

	public void setPotionAttributes (int potionLevel){
		CoolDownTime = CoolDownTimeMax;

		currentPotionLevel = potionLevel;

		if (_potionType == PotionType.AtkDmgIncr) {
		
			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = redPotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = redPotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = redPotion3;
			}

		}
		if (_potionType == PotionType.MagDmgIncr) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = bluePotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = bluePotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = bluePotion3;
			}

		}
		if (_potionType == PotionType.AdGold) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = yellowPotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = yellowPotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = yellowPotion3;
			}

		}
		if (_potionType == PotionType.AdSilver) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = silverPotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = silverPotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = silverPotion3;
			}

		}

		if (_potionType == PotionType.AdExp) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = orangePotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = orangePotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = orangePotion3;
			}

		}

		if (_potionType == PotionType.AdRefresh) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = purplePotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = purplePotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = purplePotion3;
			}

		}

        if (_potionType == PotionType.Event)
        {


            if (currentPotionLevel >= 0 && currentPotionLevel < 4)
            {
                spriteHolder.sprite = whitePotion;
            }
            if (currentPotionLevel >= 4 && currentPotionLevel < 8)
            {
                spriteHolder.sprite = whitePotion2;
            }
            if (currentPotionLevel >= 8 && currentPotionLevel < 100)
            {
                spriteHolder.sprite = whitePotion3;
            }

        }

        if (_potionType == PotionType.HPregen) {


			if (currentPotionLevel >= 0 && currentPotionLevel < 4) {
				spriteHolder.sprite = greenPotion;
			}
			if (currentPotionLevel >= 4 && currentPotionLevel < 8) {
				spriteHolder.sprite = greenPotion2;
			}
			if (currentPotionLevel >= 8 && currentPotionLevel < 100) {
				spriteHolder.sprite = greenPotion3;
			}

		}

		CoolDownGfx.fillAmount = CoolDownTime / CoolDownTimeMax;

		countingDownReady = true;

	//	print("setting potion attibus");

	}

	public float checkChemistBonus(float potionTime){

		if (ObscuredPrefs.GetInt ("ChemistPack") == 1) {

			potionTime = 2 * potionTime;
		} else {

		}

		return potionTime;

			
	}

	public void usePotion(){
		
		if (isReady == true && spriteHolder.sprite != nullSprite) {

			if (potionNumber == 1 || potionNumber == 3) {//for sfx

				CameraPotionList.persistantSoundManager.GetComponent<SoundManager> ().UsePotion1 ();

            }
            else
            { 
				CameraPotionList.persistantSoundManager.GetComponent<SoundManager> ().UsePotion2 ();
			}

			if (_potionType != PotionType.AdGold 
				&& _potionType != PotionType.AdSilver//this one will have to have its own seperate conditions
				&& _potionType!=PotionType.AdExp) {

				if (PlayerPrefs.GetFloat (this.gameObject.name) == 0) {
					

					if (_potionType == PotionType.AdRefresh || _potionType == PotionType.Event) { //if this is the ad poti
						potionUseTimeMax = 2.00f;
					} else {
						potionUseTimeMax = Convert.ToSingle (potionMaster.data [currentPotionLevel - 1] ["POTTIME"]) + Camera.main.GetComponent<MainMenu> ().potionMasteryTimeBonus + Camera.main.GetComponent<MainMenu> ().potionMasteryTimePrestige;

						potionUseTimeMax = checkChemistBonus (potionUseTimeMax);//this double potion time if chemist prestige is taken

					}

					potionUseTime = potionUseTimeMax; //this is 5* the saved rank number of talent

				}else{
					potionUseTimeMax = Convert.ToSingle (potionMaster.data [currentPotionLevel - 1] ["POTTIME"]) + Camera.main.GetComponent<MainMenu> ().potionMasteryTimeBonus + Camera.main.GetComponent<MainMenu> ().potionMasteryTimePrestige;

					potionUseTimeMax = checkChemistBonus (potionUseTimeMax);//this double potion time if chemist prestige is taken


					potionUseTime = PlayerPrefs.GetFloat (this.gameObject.name); //this is for potions that were already in progress being used again

				}

			} else { //it is an ad potion
				
				if (PlayerPrefs.GetFloat (this.gameObject.name) == 0) {
					
					potionUseTimeMax = Convert.ToSingle (potionMaster.data [currentPotionLevel - 1] ["POTTIME"]) * 12 + Camera.main.GetComponent<MainMenu> ().potionMasteryTimeBonus + Camera.main.GetComponent<MainMenu> ().potionMasteryTimePrestige;

					potionUseTimeMax = checkChemistBonus (potionUseTimeMax);//this double potion time if chemist prestige is taken


					potionUseTime = potionUseTimeMax; //this is 5* the saved rank number of talent

				}else{
					
					potionUseTimeMax = Convert.ToSingle (potionMaster.data [currentPotionLevel - 1] ["POTTIME"]) * 12 + Camera.main.GetComponent<MainMenu> ().potionMasteryTimeBonus + Camera.main.GetComponent<MainMenu> ().potionMasteryTimePrestige;

					potionUseTimeMax = checkChemistBonus (potionUseTimeMax);//this double potion time if chemist prestige is taken


					potionUseTime = PlayerPrefs.GetFloat (this.gameObject.name); //this is for potions that were already in progress being used again

				}
			}

            ParticleSystem.MainModule main1 = potion1Particles.main;
            ParticleSystem.EmissionModule emis1 = potion1Particles.emission;
            ParticleSystem.MainModule main2 = potion2Particles.main;
            ParticleSystem.EmissionModule emis2 = potion2Particles.emission;
            ParticleSystem.MainModule main3 = potion3Particles.main;
            ParticleSystem.EmissionModule emis3 = potion3Particles.emission;

            if (spriteHolder.sprite == redPotion || 
				spriteHolder.sprite == redPotion2 ||
				spriteHolder.sprite == redPotion3) { //atk dmg incr

				

                if (emis1.enabled == false) {
					
					main1.startColor = Color.red;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite (); //sets the potion time

					CameraPotionList.potionModList [0] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["ATKPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f;  //0.1f; //modify this later based on potion upgrades

				} else if(emis2.enabled == false){
					
					main2.startColor = Color.red;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();

					CameraPotionList.potionModList[5] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["ATKPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f; //modify this later based on potion upgrades

				} else if(emis3.enabled == false){
					
					main3.startColor = Color.red;
					emis3.enabled = true;
					emis3used = true; //need to create this
					replacePotionSprite (); //need to modify this 

                    //need to create another area for potion mods ** this will throw an index exception right now
					CameraPotionList.potionModList[10] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["ATKPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f; //modify this later based on potion upgrades

				}
			}

			if (spriteHolder.sprite == bluePotion ||
				spriteHolder.sprite == bluePotion2 ||
				spriteHolder.sprite == bluePotion3) { //mag dmg incr

				if (emis1.enabled == false) {

					main1.startColor = Color.blue;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();

					CameraPotionList.potionModList[1] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["MAGPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f; //modify this later based on potion upgrades


				} else if(emis2.enabled == false){

					main2.startColor = Color.blue;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();

					CameraPotionList.potionModList[6] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["MAGPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f; //modify this later based on potion upgrades

				}
                else if (emis3.enabled == false)
                {

                    main3.startColor = Color.blue;
                    emis3.enabled = true;
                    emis3used = true; //need to create this
                    replacePotionSprite(); //need to modify this 

                    //need to create another area for potion mods ** this will throw an index exception right now
                    CameraPotionList.potionModList[11] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["MAGPOT"]) * (1 + PlayerPrefs.GetInt("PrestigeLv")/2f)/100f; //modify this later based on potion upgrades

                }
            }

			if (spriteHolder.sprite == greenPotion ||
				spriteHolder.sprite == greenPotion2 ||
				spriteHolder.sprite == greenPotion3) { //HP Regenn pot

				if (emis1.enabled == false) {

					main1.startColor = Color.green;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();

                    CameraPotionList.potionModList [4] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["HPPOT"]) / 100f;


				} else if(emis2.enabled == false){

					main2.startColor = Color.green;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();

                    CameraPotionList.potionModList[9] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["HPPOT"]) / 100f;

				}
                else if (emis3.enabled == false)
                {

                    main3.startColor = Color.green;
                    emis3.enabled = true;
                    emis3used = true; //need to create this
                    replacePotionSprite(); //need to modify this 

                    //need to create another area for potion mods ** this will throw an index exception right now
                    CameraPotionList.potionModList[14] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["HPPOT"]) / 100f;

                }
            }

			if (spriteHolder.sprite == yellowPotion ||
				spriteHolder.sprite == yellowPotion2 ||
				spriteHolder.sprite == yellowPotion3) { //Gold

				if (emis1.enabled == false) {

					main1.startColor = Color.yellow;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();

                    CameraPotionList.potionModList [2] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["GOLDPOT"])/100f;  //for gold increase
					//because this potion can only be used 1 at a time, it will just be mod 2 for each

				} else if(emis2.enabled == false){

					main2.startColor = Color.yellow;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();

                    CameraPotionList.potionModList [7] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["GOLDPOT"])/100f;  //for gold increase
					//because this potion can only be used 1 at a time, it will just be mod 2 for each, the other mod was mod 7. If you want to change this you also have to change the dropped gold script

				}
                else if (emis3.enabled == false)
                {

                    main3.startColor = Color.yellow;
                    emis3.enabled = true;
                    emis3used = true; //need to create this
                    replacePotionSprite(); //need to modify this 

                    //need to create another area for potion mods ** this will throw an index exception right now
                    CameraPotionList.potionModList[12] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["GOLDPOT"])/100f;  //for gold increase

                }
            }


			if (spriteHolder.sprite == silverPotion ||
				spriteHolder.sprite == silverPotion2 ||
				spriteHolder.sprite == silverPotion3) { //if its red pot

				if (emis1.enabled == false) {

					main1.startColor = Color.white;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();
					CameraPotionList.potionModList [0] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["SILVPOT"]) * (1 + PlayerPrefs.GetInt("PrestigeLv") / 2f) / 100f;  //atk dmg increase
                    CameraPotionList.potionModList [1] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["SILVPOT"]) * (1 + PlayerPrefs.GetInt("PrestigeLv") / 2f) / 100f;  //magic damage increase

                } else if(emis2.enabled == false){

					main2.startColor = Color.white;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();
					CameraPotionList.potionModList [5] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["SILVPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f;  //atk dmg increase
					CameraPotionList.potionModList [6] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["SILVPOT"])*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)/100f;  //magic damage increase

				}
                else if (emis3.enabled == false)
                {
                    main3.startColor = Color.white;
                    emis3.enabled = true;
                    emis3used = true;
                    replacePotionSprite();
                    CameraPotionList.potionModList[10] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["SILVPOT"]) * (1 + PlayerPrefs.GetInt("PrestigeLv") / 2f) / 100f;  //atk dmg increase
                    CameraPotionList.potionModList[11] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["SILVPOT"]) * (1 + PlayerPrefs.GetInt("PrestigeLv") / 2f) / 100f;  //magic damage increase

                }
            }

			if (spriteHolder.sprite == purplePotion ||
				spriteHolder.sprite == purplePotion2 ||
				spriteHolder.sprite == purplePotion3) { //if its red pot

				if (emis1.enabled == false) {

					main1.startColor = Color.magenta;
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();//this potion just refreshes the abilities.
					resetCooldowns();


				} else if(emis2.enabled == false){

					main2.startColor = Color.magenta;
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();//this potion just refreshes the abilities.
					resetCooldowns();

				}
                else if (emis3.enabled == false)
                {
                    main3.startColor = Color.magenta;
                    emis3.enabled = true;
                    emis3used = true;
                    replacePotionSprite();//this potion just refreshes the abilities.
                    resetCooldowns();

                }
            }

            if (spriteHolder.sprite == whitePotion ||
                spriteHolder.sprite == whitePotion2 ||
                spriteHolder.sprite == whitePotion3)
            { //if its red pot

                if (emis1.enabled == false)
                {

                    main1.startColor = Color.white;
                    emis1.enabled = true;
                    emis1used = true;
                    replacePotionSprite();//this potion just refreshes the abilities.
                    PlayerPrefs.SetInt("EventSpeed", 1);//this is set to one if a potion is used for an event. if the level is completed the potion is 0.
                    Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.eventPotionSpeedUp = true;


                }
                else if (emis2.enabled == false)
                {

                    main2.startColor = Color.white;
                    emis2.enabled = true;
                    emis2used = true;
                    replacePotionSprite();//this potion just refreshes the abilities.
                    PlayerPrefs.SetInt("EventSpeed", 1);//this is set to one if a potion is used for an event. if the level is completed the potion is 0.
                    Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.eventPotionSpeedUp = true;
                }
                else if (emis3.enabled == false)
                {

                    main3.startColor = Color.white;
                    emis3.enabled = true;
                    emis3used = true;
                    replacePotionSprite();//this potion just refreshes the abilities.
                    PlayerPrefs.SetInt("EventSpeed", 1);//this is set to one if a potion is used for an event. if the level is completed the potion is 0.
                    Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.eventPotionSpeedUp = true;
                }
            }

            if (spriteHolder.sprite == orangePotion ||
				spriteHolder.sprite == orangePotion2 ||
				spriteHolder.sprite == orangePotion3) { //if its red pot

				if (emis1.enabled == false) {

					main1.startColor = Color.yellow; //this will need to be changed to match orange
					emis1.enabled = true;
					emis1used = true;
					replacePotionSprite ();
					CameraPotionList.potionModList[3] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["EXPPOT"]); //modify this later based on potion upgrades


				} else if(emis2.enabled == false){

					main2.startColor = Color.yellow; //this will need to be changed to match orange
					emis2.enabled = true;
					emis2used = true;
					replacePotionSprite ();
					CameraPotionList.potionModList[8] = Convert.ToSingle(potionMaster.data [currentPotionLevel - 1] ["EXPPOT"]);

				}
                else if (emis3.enabled == false)
                {

                    main3.startColor = Color.yellow; //this will need to be changed to match orange
                    emis3.enabled = true;
                    emis3used = true;
                    replacePotionSprite();
                    CameraPotionList.potionModList[13] = Convert.ToSingle(potionMaster.data[currentPotionLevel - 1]["EXPPOT"]);

                }
            }

            this.GetComponent<Button>().interactable = false; //when does interactable become tru tho?

            //	PlayerPrefs.SetInt (_potionType.ToString () + potionNumber.ToString(), 0); //if its 0 then there is no potion, but if its 1 or 2 there is a potion and that would be its place in the inventory
            if (PlayerPrefs.GetFloat(this.gameObject.name)==0){
			PlayerPrefs.SetFloat (this.gameObject.name,potionUseTimeMax); //this is so we can do time remaining of potions between levels
			}
		
			abilityManager.unlockWeaponAnim (this.gameObject.transform,spriteHolder.sprite);

		}
	}

	public void replacePotionSprite(){

		CoolDownTime =CoolDownTimeMax; //sets the countdown back at max for the next potion
		countingDownReady = false;
	}

	public void resetCooldowns(){

		atkFlurryScript.CoolDownTime=0.1f;
		splFlurryScript.CoolDownTime=0.1f;

		megaSlashScript.CoolDownTime=1;
		megaSlashScript.decreaseMegaSlashCooldown ();

		freezeTimeScript.CoolDownTime = 1;
		freezeTimeScript.decreaseFreezeCooldown ();

        shieldScript.CoolDownTime = 0.1f;
        petFlurryScript.CoolDownTime = 0.1f;

        soulChargeScript.CoolDownTime = 0.1f;
        soulChargeScript.decreaseFreezeCooldown();

    }

}
