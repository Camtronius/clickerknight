﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WaterBubbleBehavior : MonoBehaviour {

//	public TurnOnRagdoll parentPlayerObject;
	public bool BubbleFloat=false;
	public bool createdByPlayer1 = false;
	public bool createdByPlayer2 = false;

	public float newGravityScale = 0.00f;

	public List<GameObject> partsList = new List<GameObject>();
	// Use this for initialization
	void Start () {
	
		StartCoroutine ("FloatInBubble");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other) {

//		if (other.GetComponent<TurnOnRagdoll>()!=null 
//		    && other.GetComponent<TurnOnRagdoll> ().enabled == false) {
//
//			if(createdByPlayer1==true && other.GetComponent<ControlPlayer2Character>().isShielded==false){
//			other.GetComponent<TurnOnRagdoll>().enabled = true;
//			other.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//			other.GetComponent<Animator> ().enabled = false;
//
//				//where Damage is calcualted
//				if(other.GetComponent<ControlPlayer2Character>().isShielded==false){
//					
//					float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBubble") 
//						+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[2] //adding fire item mods
//					                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod;
//					
//					//condition for super effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//
//						calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//						
//
//					}
//
//
//					//hitting p2
//					//check for crit against p2
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//					   ){ //did the player crit on their turn??
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//
//					other.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p2 notification
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().EnemyStunned ();
//
//				}
//
//				//End where Damage is calcualted
//			}
//
//
//
//			if(createdByPlayer2==true && other.GetComponent<ControlPlayer1Character>().isShielded==false && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
//			other.GetComponent<TurnOnRagdoll>().enabled = true;
//			other.GetComponent<TurnOnRagdoll> ().createRagDoll();
//				
//			other.GetComponent<Animator> ().enabled = false;
//
//				//where Damage is calcualted
//				if(other.GetComponent<ControlPlayer1Character>().isShielded==false){
//					
//					float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBubble") 
//						+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[2] //adding fire item mods
//					                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod;
//					
//					//condition for super effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//						
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//
//						calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//						
//
//					}
//
//					//check for crit against p1
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//						   ){ //did the player crit on their turn??
//							calculatedHealthLoss = 2*calculatedHealthLoss;
//							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//						}
//						
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//							calculatedHealthLoss = 2*calculatedHealthLoss;
//							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//							
//						}
//
//							other.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p2 notification
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().EnemyStunned ();
//
//				}
//				//End where Damage is calcualted
//			}
//
//			if(createdByPlayer2==true && other.GetComponent<ControlPlayer1Character>().isShielded==false && Camera.main.GetComponent<CampaignGameOrganizer>()!=null){ //this is for final boss act 2
//
//				//where Damage is calcualted
//				if(other.GetComponent<ControlPlayer1Character>().isShielded==false){
//					
//					double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//
//					
//					//condition for super effective
//					if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
//						
//						DamageToPlayer = 2*DamageToPlayer;
//						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//						
//						
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
//						
//						DamageToPlayer = (0.5f)*DamageToPlayer;
//						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//						
//					}
//					
//					if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite ||
//					   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
//					   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//						
//						
//					}
//
//					other.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//					Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//					
//					if(DamageToPlayer >= other.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
//						
//						other.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
//						other.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//						other.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//						
//									
//					}else{
//						Camera.main.GetComponent<CampaignGameOrganizer>().stunManager(2,0); //this should stun p1 for 1 round?
//						Camera.main.GetComponent<CampaignGameOrganizer>().PlayerStunned();
//						other.gameObject.GetComponent<ControlPlayer1Character>().soundManager.confusionSound();
//					}
//										
//
//				}
//				//End where Damage is calcualted
//			}
//
//		}
//
//		if (other.gameObject.tag == "Player2" && Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
//			damageEnemy(other);
//		}
//
//		if (other.gameObject.tag == "Enemy" && Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
//			damageEnemy(other);
//		}
//
	}



	public void damageEnemy(Collider2D other){
	
		//this is for when an enemy gets hit by the water bubble
				
			print ("water bubble is colliding");
			
			//	other.GetComponentInChildren<EnemyRagdoll>().enabled = true;
			//	other.GetComponentInChildren<EnemyRagdoll> ().createRagDoll();
			
			//where Damage is calcualted
			if(other.GetComponent<ControlEnemy>().isShielded==false){
				
				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBubble") 
			                              + PlayerPrefs.GetFloat ("WaterItemMod") + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
				
				//condition for super effective
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){

					calculatedHealthLoss = 2*calculatedHealthLoss;

					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}
				
				//condition for Not very effective
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
					
					calculatedHealthLoss = (.5f)*calculatedHealthLoss;

					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness

				}
				
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite ||
				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
					
				}
				
			
					if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
						
						calculatedHealthLoss = 2*calculatedHealthLoss;
						print("CRITS!!!!!!!!!");
						
					}
					//crit stuff here
					other.gameObject.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
					
					Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);




				if(calculatedHealthLoss >= other.GetComponent<ControlEnemy>().playerHealthCurrent){
					
					other.gameObject.GetComponent<ControlEnemy>().playerDead=true;
					other.gameObject.GetComponent<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll
					other.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll();


						this.gameObject.GetComponentInChildren<CircleCollider2D>().enabled = false; //makes it so it doesnt kill enemies at the end
					
				}else{
					
					other.gameObject.GetComponent<ControlEnemy>().initiateStun();
					
				Camera.main.GetComponent<CampaignGameOrganizer> ().EnemyStunned ();

				}
				
			}
	
		}







	void OnTriggerStay2D (Collider2D other) {
		
		if (!partsList.Contains (other.gameObject)) {
			
			partsList.Add(other.gameObject);
			
		}
		BubbleFloat = true;
	}
	/*
	void OnTriggerExit2D (Collider2D other) {
		BubbleFloat = false;
		parentPlayerObject.gravityScale = 0.4f;
		parentPlayerObject.redoGravity();
		
	}
*/
	IEnumerator FloatInBubble(){
		
		while (true) {
			
			yield return new WaitForSeconds (0.1f);

			if (BubbleFloat == true) {

				for (int i=0; i<partsList.Count; i++) {

					if (this.gameObject.GetComponent<CircleCollider2D> ().IsTouching(partsList[i].GetComponent<BoxCollider2D>()) 
					    && partsList[i].GetComponent<Rigidbody2D>()!=null) {
					
						partsList[i].GetComponent<Rigidbody2D>().gravityScale = newGravityScale;

						partsList[i].GetComponent<Rigidbody2D>().AddForce((this.gameObject.GetComponent<CircleCollider2D>().transform.position - partsList[i].transform.position)*3f);

//						print ("detecing if within trig collider");

					}else{
						if(partsList[i].GetComponent<Rigidbody2D>()!=null){
						partsList[i].GetComponent<Rigidbody2D>().gravityScale =0.1f;
						}
					}
				}

			
				//	return false;
			}
		}
	}

	public void returnToNormalGravity(){

		//this is for when a player is receiving a spell online
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
			if (createdByPlayer1 == true //this is sent by player 1 received by player 2
				&& Camera.main.GetComponent<TurnedBasedGameOrganizer> ().playerInt == 2
				&& GameObject.Find ("Player2Character").GetComponentInChildren<ControlPlayer2Character> ().player2Invis == false
				&& GameObject.Find ("Player2Character").GetComponentInChildren<ControlPlayer2Character> ().isShielded == false) {

				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().stunManager (0, 2); //this actually creates the stun

				if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == false) {
					//9/9/15 this was added so the player can make a turn even though he is stunned online
					GameObject.Find ("Player2Character").GetComponentInChildren<ControlPlayer2Character> ().player2Stun = true; 
				}

				//	player is not set to istunned=true because TurnOnRagdoll resets this
				GameObject.Find ("Player2Character").GetComponentInChildren<Animator> ().SetBool ("IsStunned", true);
				GameObject.Find ("Player2Character").GetComponentInChildren<TurnOnRagdoll> ().stunCasted = true; 
				//makes it so the stun spell can be casted twice, and it doesnt reset the stun if this happens
			}



			if (createdByPlayer2 == true //this is sent by player 2 received by player 1
				&& Camera.main.GetComponent<TurnedBasedGameOrganizer> ().playerInt == 1
				&& GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == false
				&& GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().isShielded == false) {

				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().stunManager (2, 0); //this actually creates the stun
				//	player is not set to istunned=true because TurnOnRagdoll resets this
				if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == false) {
					//9/9/15 this was added so the player can make a turn even though he is stunned online
					GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Stun = true; 
				}

				GameObject.Find ("Player1Character").GetComponentInChildren<Animator> ().SetBool ("IsStunned", true);
				GameObject.Find ("Player1Character").GetComponentInChildren<TurnOnRagdoll> ().stunCasted = true;
				//makes it so the stun spell can be casted twice, and it doesnt reset the stun if this happens
			}





			//this is for just when the spell is first sent
			if (createdByPlayer1 == true 
				&& GameObject.Find ("Player2Character").GetComponentInChildren<ControlPlayer2Character> ().player2Invis == false
				&& GameObject.Find ("Player2Character").GetComponentInChildren<ControlPlayer2Character> ().isShielded == false) {

				if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == true) {
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().stunManager (0, 2);
				}

				GameObject.Find ("Player2Character").GetComponentInChildren<Animator> ().SetBool ("IsStunned", true);

				GameObject.Find ("Player2Character").GetComponentInChildren<TurnOnRagdoll> ().stunCasted = true; 


				print ("water bubble created by player 1 should set stun to true...");

			}
			if (createdByPlayer2 == true 
				&& GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == false
				&& GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().isShielded == false) {


				if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == true) {
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().stunManager (2, 0);
				}

				GameObject.Find ("Player1Character").GetComponentInChildren<Animator> ().SetBool ("IsStunned", true);

				GameObject.Find ("Player1Character").GetComponentInChildren<TurnOnRagdoll> ().stunCasted = true; 


				print ("water bubble created by player 2 should set stun to true...");


			}

			this.gameObject.GetComponent<CircleCollider2D> ().enabled = false;

			for (int i=0; i<partsList.Count; i++) {
				if (partsList [i].GetComponent<Rigidbody2D> () != null) {						
					partsList [i].GetComponent<Rigidbody2D> ().gravityScale = 0.2f;
				}
			}



		}
	}
}
	


