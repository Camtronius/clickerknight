﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class TutorialCampaign : MonoBehaviour {

	private static Tutorial instance = null;

	public GameObject mainMenuFinger;

	//for lv1
	public bool isTutorial = false;
	public bool spellHasBeenUnlocked = false;
	public bool villageUnderAttack = false;
	public bool castFireSpell = false;
	public bool castFireSpell2 = false;
	public bool swordAttack = false;
	public bool statsDialogueBool = false;
	public bool StatsExplained = false;
	public bool ItemExplainEquipBool = false;
	public bool ReadyToEquipItemBool = false;
	public bool explainItemBuyBool = false;

	public bool showFinalBossStolenBook = false;
	public bool showFinalBossStolenBook2 = false;
	public bool showFinalBossNewWolf = false;

	public bool showSoldierDeath = false;
	public bool showSoldierDeath2 = false;

	public bool showPortal = false;
	public bool showPortal2 = false;

	public bool showFinalBossNewReptile = false;
	public bool showFinalBossNewReptile2 = false;

	public bool showPortalBack = false;
	public bool showPortalBack2 = false;

	public bool TurnBackNow = false;
	public bool TurnBackNow2 = false;
	public bool TurnBackNow3 = false;
	
	public bool explainEffectiveness = false;
	public bool explainEffectiveness2 = false;
	public bool explainEffectiveness3 = false;

	public bool DefeatedOurMaster = false;
	public bool DefeatedOurMaster2 = false;
	public bool DefeatedOurMaster3 = false;

	public bool NeverMakeItAlive = false;
	public bool HaltBool = false;
	public bool HaltBool2 = false;

	public bool TooLate1 = false;
	public bool TooLate2 = false;

	public bool CutYouBool1 = false;
	public bool CutYouBool2 = false;

	public bool FinalBossAct2DeathBool = false;
	public bool FinalBossAct2DeathBool2 = false;

	public bool KindOfArmy2 = false;
	public bool KindOfArmy = false;


	//for lv2
	public bool itemDroppedTutorial = false;

	//for main menu
	public EnemyStatsInheriter EnemyStatsInheriterObj; //this is the actual selection of levels

	public GameObject QuestButton; //this is the actual selection of levels
	public TextMeshProUGUI QuestButtonText; //this is the actual selection of levels

	public GameObject QuestDisplay; //this is the actual selection of levels
	public TextMeshProUGUI QuestText; //this is the actual selection of levels

	public GameObject SpriteHolder;
	public Sprite GirlNormal;
	public Sprite GirlSurprised;
	public Sprite GirlScreaming;
	public Sprite EvilBossSprite;
	public Sprite PirateSoldier;
	public Sprite Reptillian;
	public Sprite Golem;
	public Sprite PurpleReptile;
	public Sprite FinalBossAct2;

	public GameObject EquipmentList;
	public GameObject EquipmentList2;			
	
	public GameObject equipmentSelectToggle;

	public List<GameObject> NewSpellsList = new List<GameObject> ();
	
	public GameObject LV1;

	public GameObject TutorialFinger;

	public LevelComplete LevelCompleteObj;

	
	void Start () {

	//	PlayerPrefs.SetFloat ("Level1", 1);
	//	PlayerPrefs.SetFloat ("Level2", 1);
		checkAct2Stuff ();
	//	PlayerPrefs.SetFloat ("Level3", 1);

		
		EnemyStatsInheriterObj = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ();

		if (EnemyStatsInheriterObj.CurrentLevel == 1 && PlayerPrefs.GetFloat("Level1")==0) {

			isTutorial=true;
			villageUnderAttack=true;

			VillageUnderAttack();
		}

		//comment this out if you dont want the tutorial to play

		if (EnemyStatsInheriterObj.CurrentLevel == 2 && PlayerPrefs.GetFloat("Level2")==0) {
			
			isTutorial=true;
			villageUnderAttack=true;
			
			ExplainSpellBookUse();
		}


		if (EnemyStatsInheriterObj.CurrentLevel == 3 && PlayerPrefs.GetFloat("Level3")==0) {
			
			isTutorial=true;

			explainEffectiveness = true;

		//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 4 && PlayerPrefs.GetFloat("Level4")==0) {
			
			isTutorial=true;
			
			showFinalBossStolenBook = true;
			
			//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 7 && PlayerPrefs.GetFloat("Level7")==0) { //dont forget the current lv thing
			
			isTutorial=true;
			
			showFinalBossNewWolf = true;

			villageUnderAttack = true; //so it stops the camera at the start

			ShowFinalBossWolfReveal();
			
			//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 9 && PlayerPrefs.GetFloat("Level9")==0) { //dont forget the current lv thing
			
			isTutorial=true;
			
			showSoldierDeath = true;

			//ShowDeadSoldier();
			
			//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 10 && PlayerPrefs.GetFloat("Level10")==0) { //dont forget the current lv thing

			showPortal=true;
			isTutorial=true;

			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 11 && PlayerPrefs.GetFloat("Level11")==0) { //dont forget the current lv thing
			
			isTutorial=true;
			
			showFinalBossNewReptile = true;

			villageUnderAttack = true; //so it stops the camera at the start
			
			ShowFinalBossReptileReveal();
			
			//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 13 && PlayerPrefs.GetFloat("Level13")==0) { //dont forget the current lv thing
			
			showPortalBack=true;
			isTutorial=true;
			
			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 14 && PlayerPrefs.GetFloat("Level14")==0) { //dont forget the current lv thing

			isTutorial = true;

			TurnBackNow = true;

			villageUnderAttack = true; //so it stops the camera at the start

			TurnBackNowDialogue();
			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 15 && PlayerPrefs.GetFloat("Level15")==0) { //dont forget the current lv thing
			
			isTutorial = true;
			
			DefeatedOurMaster = true;
			
			villageUnderAttack = true; //so it stops the camera at the start
			
			DefeatedOurMasterDialogue();
			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 17 && PlayerPrefs.GetFloat("Level17")==0) { //dont forget the current lv thing
			
			isTutorial = true;
			
			villageUnderAttack = true; //so it stops the camera at the start
			
			NeverMakeItAliveDialogue();
			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 19 && PlayerPrefs.GetFloat("Level19")==0) { //dont forget the current lv thing
			
			isTutorial = true;
			
			villageUnderAttack = true; //so it stops the camera at the start
			
			Halt();
			//for the portal
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 20 && PlayerPrefs.GetFloat("Level20")==0) {
			
			isTutorial=true;
			
			TooLate1 = true;
			
			//	explainElementalWeakness1();
		}

		if (EnemyStatsInheriterObj.CurrentLevel == 23 && PlayerPrefs.GetFloat("Level23")==0) {
			
			isTutorial=true;
			villageUnderAttack = true; //so it stops the camera at the start

			CutYouBool1 = true;
			
			CutYouGood1();
		}

		//Do something at 25 last boss says 1st pane "What kind of evil army is this?! 2nd pane: "Dont let him inside my Rune Dungeon!"
		if (EnemyStatsInheriterObj.CurrentLevel == 25 && PlayerPrefs.GetFloat("Level25")==0) { //dont forget the current lv thing
			
			isTutorial = true;
			
			KindOfArmy = true;
			
			villageUnderAttack = true; //so it stops the camera at the start
			
			WhatKindOfArmy();
			//for the portal
		}
	}

	public void checkAct2Stuff(){


		
		if (PlayerPrefs.GetFloat ("Level14") == 1) { //this is if the you are at the title screen
			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){
				
				EquipmentList.SetActive(false);
				EquipmentList2.SetActive(true);			
				
				equipmentSelectToggle.SetActive (true);
				for (int i=0; i<NewSpellsList.Count; i++) {
					NewSpellsList [i].SetActive (true);
				}
				
			}
			
		} else {
			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){ //this is if the you are at the title screen
				
				EquipmentList.SetActive(true);
				EquipmentList2.SetActive(false);
				
				equipmentSelectToggle.SetActive (false);
				for (int i=0; i<NewSpellsList.Count; i++) {
					NewSpellsList [i].SetActive (false);
				}
			}
			
			
		}


	}

	public void resetTeleType(){

//		QuestDisplay.GetComponentInChildren<TeleType> ().AssignNewLabel ();
//		QuestDisplay.GetComponentInChildren<TeleType> ().StartCoroutine ("refreshText");
	}
	
	public void closeDialogue(){

		if(villageUnderAttack = true && showFinalBossNewReptile == false && TurnBackNow==false && DefeatedOurMaster==false && DefeatedOurMaster2==false){
			
			villageUnderAttack = false;

			castFireSpell = true;

			QuestDisplay.SetActive(false);

		}

		if (explainEffectiveness2 == true && explainEffectiveness==false) {

			castFireSpell=false;
			castFireSpell2=false;
			explainElementalWeakness3();
		}

		if (explainEffectiveness == true && explainEffectiveness2==false) {

			castFireSpell=false;
			castFireSpell2=false;
			explainElementalWeakness2();

		}

		if(showFinalBossStolenBook2==true){ //this is for the end of the equip item tutorial
			ShowFinalBossHasStolenBook3();
			
		}
		
		if(showFinalBossStolenBook==true){ //this is for the end of the equip item tutorial
			
			print ("IS CALLING SHOW FINAL BOSS BOOK2 through the close button!");
			
			ShowFinalBossHasStolenBook2();
			
		}

		if (showFinalBossNewWolf == true) {

			castFireSpell = false;

			showFinalBossNewWolf = false;
			
			GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().TutorialBossHolder.GetComponentInChildren<Animator>().SetBool ("Disappear", true); 

			//reset start end pos here? 

			GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().resetStartEndPos(); //this is show the amror wolf shows up level with the player (used to be skewed)

			villageUnderAttack = false;
			
			QuestDisplay.SetActive(false);
		}

		if (showSoldierDeath == true) {
			ShowDeadSoldier2();
		}

		if (showSoldierDeath2 == true) {
		GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().EnemySpawned.GetComponentInChildren<Animator>().SetBool ("isDead", true); //so he will die
	//	QuestDisplay.SetActive(false);
			isTutorial=false;

		}

		if (showPortal2 == true) {
			
			QuestDisplay.SetActive(false);

			LevelCompleteObj.restoreAllButtons ();

			isTutorial = false;


		}

		if (showPortal == true) {

			ShowPortalTutorial2(); //this brings up the 2nd part of the portal tutorial

		}

		if (showFinalBossNewReptile2 == true) {
			
			castFireSpell = false;
			
			showFinalBossNewReptile = false;
			showFinalBossNewReptile2 = false;
			
			villageUnderAttack = false;
			
			QuestDisplay.SetActive(false);
		}

		if (showFinalBossNewReptile == true) {
			ShowFinalBossReptileReveal2();	
		}
			

		if (showPortalBack2 == true) { //for going back in the portal
			
			QuestDisplay.SetActive(false);
			
			LevelCompleteObj.restoreAllButtons ();
			
			isTutorial = false;
			
			
		}
		
		if (showPortalBack == true) {
			
			ShowPortalBackTutorial2(); //this brings up the 2nd part of the portal tutorial
			
		}

		if (TurnBackNow == true) {

			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);

			//prepare for end of level dialogue tho...
		}

		if (TurnBackNow3== true) {
			
			QuestDisplay.SetActive(false);
			GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().EnemySpawned.GetComponentInChildren<Animator>().SetBool ("FinalBossDeadEnd", true); //so he will die
			TurnBackNow2 = false;
		}

		if (TurnBackNow2 == true) {

			TurnBackNowDialogue3();
		
		}


		if (DefeatedOurMaster2 == true) {

			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);

		}

		if (DefeatedOurMaster == true) {

			villageUnderAttack = true;

			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
			DefeatedOurMasterDialogue2();
			//prepare for end of level dialogue tho...
		}

		if (NeverMakeItAlive == true) {
			
			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);

		}

		if (HaltBool2 == true) {
			
			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
		}
		
		if (HaltBool == true) {
			
			villageUnderAttack = true;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
			Halt2();
			//prepare for end of level dialogue tho...
		}

		if (CutYouBool2 == true) {
			
			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
		}
		
		if (CutYouBool1 == true) {
			
			villageUnderAttack = true;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
			CutYouGood2();
			//prepare for end of level dialogue tho...
		}

		if(TooLate2==true){ //this is for the end of the equip item tutorial
			
			print ("Final Boss Dissappear!");
			GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().TutorialBossHolder.GetComponentInChildren<Animator>().SetBool ("BossDisappear", true); //so he will die
			LevelCompleteObj.restoreAllButtons ();

			//make the final boss dissappear
			
		}

		if(TooLate1==true){ //this is for the end of the equip item tutorial
			
			print ("IS CALLING SHOW FINAL BOSS BOOK2 through the close button!");
			
			YoureTooLate2();
			
		}

		if (KindOfArmy2 == true) {
			
			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);

			KindOfArmy2 = false;
			
		}
		
		if (KindOfArmy == true) {
			
			villageUnderAttack = true;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			
			WhatKindOfArmy2();
			//prepare for end of level dialogue tho...
		}

		if (FinalBossAct2DeathBool2 == true) {
			
			GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().EnemySpawned.GetComponentInChildren<Animator>().SetBool ("EndDeathFinalBoss", true); //so he will die
			villageUnderAttack = false;
			
			castFireSpell = false;
			
			QuestDisplay.SetActive(false);
			FinalBossAct2DeathBool2 = false;
		}

		if (FinalBossAct2DeathBool == true) {

			FinalBossAct2FirstDeath2();
		}

	}

	public void VillageUnderAttack(){

		SpriteHolder.GetComponent<Image> ().sprite = GirlScreaming;

		QuestText.text = "Help, our village is under attack!";

	//	UITextTypeWriter Type = QuestText.gameObject.AddComponent<UITextTypeWriter> ();
		QuestDisplay.SetActive(true);


		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "I'll Help!";

	}

	public void FireballDialogue(){

		for (int i=0; i<Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count; i++) {

			if( i == 0 || i == 1 || i == 2 || i == 5){

				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=true;


			}else{

				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=false;
		//		print ("RNE IS NOT SELECTABL!!!");
			}


		}

		//    Camera.main.GetComponent<CampaignGameOrganizer>().meteorList

		QuestText.GetComponent<TextMeshProUGUI>().text = "Select these <color=red>4 </color>Runes for a Fireball Spell!";



		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;

		QuestDisplay.SetActive(true);

		resetTeleType ();

		QuestButton.SetActive (false);
			
		TutorialFinger.SetActive(true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("Fireball1", true);


	}

	public void FireballDialogue2(){


		for (int i=0; i<Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count; i++) {
			
			if( i == 12 || i == 13 || i == 14 || i == 15){
				
				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=true;
				
				
			}else{
				
				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=false;
			//	print ("RNE IS NOT SELECTABL!!!");
			}

		}

		QuestText.GetComponent<TextMeshProUGUI>().text = "You just need at least <color=red>3 Fire Runes!</color> <color=green>Order doesnt matter!</color>";

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
				
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);
		
		TutorialFinger.SetActive(true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("Fireball2", true);

	}

	public void SwordDialogue(){

		for (int i=0; i<Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count; i++) {
			
			if( i == 9 || i == 10 || i == 6 || i == 7){
				
				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=true;

			}else{
				
				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=false;
			//	print ("RNE IS NOT SELECTABL!!!");
			}
			
			
		}

		QuestText.GetComponent<TextMeshProUGUI>().text = "If no spell is made, or the spell is not known, you will use your sword!";

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
				
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);
		
		TutorialFinger.SetActive(true);
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("SwordTutorial", true);

	}

	public void StatsDialogue(){
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "You Ranked up! You can now upgrade your stats!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

		LevelCompleteObj.EquipsItemButton.SetActive (false);

		QuestButton.SetActive (false);

		TutorialFinger.transform.SetAsLastSibling (); //this moves the finger to the end of the list so it appears on top

		TutorialFinger.SetActive(true);
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("UpgradeStats", true);

	}

	public void explainStatsUpgrade(){
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Use the + and - buttons to upgrade your stat points. Spend all available points.";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (true);

		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Got it!";
				
		TutorialFinger.SetActive(false);

		StatsExplained = true; //this makes it so it will show the spell button after this has been upgraded

	}

	public void explainSpellsUpgrade(){
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Great Work! You can learn a new spell now too!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);
		
	//	QuestButtonText.GetComponent<Text>().text = "Got it!";
		
		TutorialFinger.SetActive(true);

		LevelCompleteObj.LearnSpellsButton.SetActive (true); //unlocks the spell book button
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("UpgradeSpell", true);

	}

	public void explainSpellsBuy(){
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Tap on any spell listed here to see its effect and to unlock it";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (true);

		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Awesome!";
		
		TutorialFinger.SetActive(false);

	}

	public void StatsBackButton(){
		
		TutorialFinger.SetActive(true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("BackFromSpellUnlock", false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("BackButton", true);
		
	}

	public void BackFromSpellUnlock(){
		
		TutorialFinger.SetActive(true);

		spellHasBeenUnlocked = true;

		TutorialFinger.GetComponent<Animator> ().SetBool ("BackFromSpellUnlock", true);
		
	}

	public void OffBackButton(){
		
		TutorialFinger.SetActive(false);
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("BackButton", false);

		explainSpellsUpgrade ();
		
	}

	public void ExplainContinueLevel1(){

		TutorialFinger.GetComponent<Animator> ().SetBool ("BackFromSpellUnlock", false); //these 2 are to make sure the finger doesnt appear anymore
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("BackButton", false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("NextLevelBool", true);

		TutorialFinger.transform.SetAsFirstSibling ();

		TutorialFinger.transform.position = LevelCompleteObj.NextLevelButton.transform.position;

		
		TutorialFinger.SetActive(true);


		SpriteHolder.GetComponent<Image> ().sprite = GirlSurprised;
				
		QuestText.GetComponent<TextMeshProUGUI>().text = "The monsters ran into the Dungeon! What could they be after?!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);
				
		isTutorial = false;

		castFireSpell2 = false;




		LevelCompleteObj.NextLevelButton.SetActive (true);
		LevelCompleteObj.TitleScreenButton.SetActive (true);

	}

	public void ExplainSpellBookUse(){ //shows the player how to use the spellbook on lv2

		TutorialFinger.transform.SetAsLastSibling (); //this moves the finger to the end of the list so it appears on top

		TutorialFinger.SetActive(true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("HowUseSpellBook", true);

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Use your <color=red>SpellBook</color> to see how spells are made!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);

	}

	public void explainItemEquip(){
		
		TutorialFinger.transform.SetSiblingIndex (3);

		LevelCompleteObj.LearnSpellsButton.SetActive (false);

		LevelCompleteObj.UpgradableStats.SetActive (false);

		TutorialFinger.transform.position = LevelCompleteObj.EquipsItemButton.transform.position; //sets the finger on the bag icon


		TutorialFinger.SetActive(true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("NewItemFingerTap", true);

		
		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;

		QuestText.GetComponent<TextMeshProUGUI>().text = "You found a new item! Open your <color=red>Equipment Bag</color> to equip it";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		QuestButton.SetActive (false);

		ItemExplainEquipBool = true; //this is so the finger can switch once the items are up

	}



	public void showNewEquipmentUnlocked(){
		//this shows the new equipment that has been unlocked
		QuestDisplay.SetActive(false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("NewItemFingerTap", false);

		TutorialFinger.SetActive (false);
		


		foreach (GameObject item in Camera.main.GetComponent<MainMenu>().itemList)
		{


			if(PlayerPrefs.GetInt (item.gameObject.GetComponent<Image> ().sprite.name.ToString ()) == 2){

				TutorialFinger.transform.position = item.transform.position; //sets the finger at the location of the unlocked item

			}
		
		
		}

		TutorialFinger.SetActive (true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("NewItemFingerTap", true);

		//	SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
	//	QuestText.GetComponent<TextMeshProUGUI>().text = "New items glow! Open the glowing Item";
		
	//	QuestDisplay.SetActive(true);

	//	resetTeleType ();
		
		QuestButton.SetActive (false);

	}

	public void EquipNewItem(){
		//this shows the new equipment that has been unlocked
	//	ItemExplainEquipBool = false; //this is so the finger can switch once the items are up

		ReadyToEquipItemBool = true; //this makes it so it doesnt give an explaination for what equipped items are before u have equipped the new item

		TutorialFinger.SetActive (false);

		TutorialFinger.transform.position = Camera.main.GetComponent<MainMenu> ().itemDescriptionEquip.transform.position;

		TutorialFinger.SetActive (true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("NewItemFingerTap", true);//equip items button anim

	//	SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;

	//	QuestText.GetComponent<TextMeshProUGUI>().text = "Here you can see information about your item and equip it";
		
	//	QuestDisplay.SetActive(true);

	//	resetTeleType ();
		
	//	QuestButton.SetActive (false);


	}

	public void BackFromItemDescription(){

		TutorialFinger.SetActive (false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("HowUseEquipItem", false);//equip items button anim

		TutorialFinger.SetActive (true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("BackFromSpellUnlock", true);//equip items button anim

	}

	public void explainGreenItemEquip(){

		if (explainItemBuyBool == true) {

			LevelCompleteObj.BackButton.SetActive (true);

		//	TutorialFinger.GetComponent<Animator> ().SetBool ("BackFromSpellUnlock", false);//equip items button anim

			TutorialFinger.SetActive (false);

		//	TutorialFinger.GetComponent<Animator> ().SetBool ("BackButton", true); //these 2 are to make sure the finger doesnt appear anymore

			SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
			QuestText.GetComponent<TextMeshProUGUI> ().text = "You can also <color=red>Buy Items</color> by tapping on <color=#9C9C9C>Grey</color> items";
		
			QuestDisplay.SetActive (true);

			resetTeleType ();
		
			QuestButton.SetActive (false);
		
		}

	}

	public void ExplainContinueLevel2(){

		TutorialFinger.SetActive (false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("UpgradeSpell", false); //these 2 are to make sure the finger doesnt appear anymore
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("BackButton", false);

		TutorialFinger.transform.SetAsFirstSibling ();

		TutorialFinger.SetActive(true);

		TutorialFinger.transform.position = LevelCompleteObj.NextLevelButton.transform.position;
				
		TutorialFinger.GetComponent<Animator> ().SetBool ("NextLevelBool", true);

		QuestDisplay.SetActive(false);

		LevelCompleteObj.statsButton.SetActive (true);
		LevelCompleteObj.NextLevelButton.SetActive (true);
		LevelCompleteObj.TitleScreenButton.SetActive (true);
		LevelCompleteObj.LearnSpellsButton.SetActive (true);

		isTutorial = false; //makes it so tutorial stuff doesnt keep happening after it is over, and it resets back to normal

	}

	public void UpgradedStatsAndWeapons(){

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;

		QuestText.GetComponent<TextMeshProUGUI>().text = "Good Work! I have added <color=red> NEW spells and armor </color> for you :) Check it out!";
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Thanks!";

		QuestButton.SetActive (true);
		
				
	}

	//-*-*-*-*-*-*-*-*-*-*-*-*-*-8below is turning panes on and off

	public void EndFireballDialogue(){

		TutorialFinger.GetComponent<Animator> ().SetBool ("Fireball1", false);

		if (castFireSpell == true) {

			castFireSpell=false;
			castFireSpell2=true;
		}

		QuestDisplay.SetActive(false);
		
		TutorialFinger.SetActive(false);

	}

	public void EndFireballDialogue2(){

		TutorialFinger.GetComponent<Animator> ().SetBool ("Fireball2", false);

		if (castFireSpell2 == true) {
			
			castFireSpell2=false;
			swordAttack=true;
		}

		QuestDisplay.SetActive(false);
		
		TutorialFinger.SetActive(false);

	}

	public void EndSwordDialogue(){
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("SwordTutorial", false);
		
		if (swordAttack == true) {
			
			swordAttack=false;
			statsDialogueBool = true;

		}
		
		QuestDisplay.SetActive(false);
		
		TutorialFinger.SetActive(false);

		for (int i=0; i<Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count; i++) {

				
				Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[i].isSelectable=true;
			//	print ("RNE IS NOT SELECTABL!!!");

		}
		
	}

	public void selectStatsButton(){
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("UpgradeStats", false);

		
		QuestDisplay.SetActive(false);
		
		TutorialFinger.SetActive(false);
		
	}

	public void spellBookOpened(){

		QuestDisplay.SetActive(false);

	}

	public void endSpellBookFinger(){

		TutorialFinger.GetComponent<Animator> ().SetBool ("HowUseSpellBook", false);
		TutorialFinger.SetActive(false);
		villageUnderAttack = false;
	}

	public void explainElementalWeakness1(){

		villageUnderAttack = false;

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Certain Spell Types are more effective versus others.";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "OK!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

	}

	public void explainElementalWeakness2(){

		TutorialFinger.transform.SetAsLastSibling ();

		explainEffectiveness = false;
		explainEffectiveness2 = true;
		villageUnderAttack = false;
				
		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;

		TutorialFinger.SetActive (true);

		TutorialFinger.GetComponent<Animator> ().SetBool ("SpellEffectivenessFinger", true);

		QuestText.GetComponent<TextMeshProUGUI>().text = "Because the enemy has <color=green>Earth Type</color>, he is weak versus <color=red>Fire</color>.";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Cool!";
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

	}

	public void explainElementalWeakness3(){
		
//		print ("ele weakness 3 goin");

		explainEffectiveness2 = false;

		explainEffectiveness3 = true; 

		villageUnderAttack = false;
		
		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		TutorialFinger.GetComponent<Animator> ().SetBool ("SpellEffectivenessFinger", false);

		TutorialFinger.GetComponent<Animator> ().SetBool ("HowUseSpellBook", true);

		TutorialFinger.SetActive (true);
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "You can see what element types are effective against other types in your <color=red>Spellbook</color>";

		LevelCompleteObj.closeBuyableSpellsPane.GetComponent<Animator> ().enabled = true;
		LevelCompleteObj.closeBuyableSpellsPane.GetComponent<Animator> ().SetBool ("Effectiveness", true);


		QuestButton.SetActive (false);
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

	}

	public void ShowFinalBossHasStolenBook1(){

		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss

		LevelCompleteObj.LearnSpellsButton.SetActive (false);

		GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ().FinalBossLaugh();

		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;

		QuestText.GetComponent<TextMeshProUGUI>().text = "Hahaha, The Book of Runes is MINE!";

		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "*Gasp!*";

		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

		showFinalBossStolenBook=true;
		//showFinalBossStolenBook2=true;

	}

	public void ShowFinalBossHasStolenBook2(){

	//	SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Now with an army I will conquer the world!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "I wont let you!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

		showFinalBossStolenBook = false;
		showFinalBossStolenBook2=true;
	//	showFinalBossStolenBook3=true;

		print ("IS CALLING SHOW FINAL BOSS BOOK2!");

	}

	public void ShowFinalBossHasStolenBook3(){ //this is where the boss dissapperars
		
		//SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "The World Is Mine! MUAHAHAHAHA";
		
		QuestButton.SetActive (false);
		
		QuestDisplay.SetActive(true);

		GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ().FinalBossLaugh();

		resetTeleType ();

		LevelCompleteObj.restoreAllButtons ();

		GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().TutorialBossHolder.GetComponentInChildren<Animator>().SetBool ("Disappear", true); 

		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Not yet!";
		
		QuestButton.SetActive (true);

		showFinalBossStolenBook2=false;

		TutorialFinger.SetActive (false);

		TutorialFinger.transform.SetAsFirstSibling ();

			//.GetComponentInChildren<Animator> ().SetBool ("Disappear", true);
		
	}

	public void ShowFinalBossWolfReveal(){
		
		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;

		GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ().FinalBossLaugh();

		QuestText.GetComponent<TextMeshProUGUI>().text = "You are becoming strong... Perhaps I use my newest fighter";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Bring it!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

	//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();

	}

	public void ShowDeadSoldier(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss

		LevelCompleteObj.LearnSpellsButton.SetActive (false);
		
		LevelCompleteObj.UpgradableStats.SetActive (false);

		SpriteHolder.GetComponent<Image> ().sprite = PirateSoldier;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "They were... too strong... they went to... the ruins";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Hold on!!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		showSoldierDeath2=true; //to go to next pane
		
	}

	public void ShowDeadSoldier2(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss
		
		SpriteHolder.GetComponent<Image> ().sprite = PirateSoldier;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Dont let them open the... Portal... Blarrghhhh";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Nooooooo!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

		LevelCompleteObj.restoreAllButtons ();

		showSoldierDeath=false;
		showSoldierDeath2 = true; //to go to next pane
	//	isTutorial = false;
		
	}

	public void ShowPortalTutorial(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss
		
		LevelCompleteObj.LearnSpellsButton.SetActive (false);
		
		LevelCompleteObj.UpgradableStats.SetActive (false);
		
		SpriteHolder.GetComponent<Image> ().sprite = GirlSurprised;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Oh no, the Portal has been opened!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Darn!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		resetTeleType ();

	}

	public void ShowPortalTutorial2(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;

		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "We must go after him before he makes a magic army!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Lets Go!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

		showPortal = false;
		showPortal2 = true; //to go to next pane

	}

	public void ShowFinalBossReptileReveal(){
		
		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "You again?! Destroy him my <color=green>Reptillians</color>!!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Your What?";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		showFinalBossNewReptile = true;

		
		resetTeleType ();
		
	//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
		
	}

	public void ShowFinalBossReptileReveal2(){
		
		SpriteHolder.GetComponent<Image> ().sprite = Reptillian;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=green>Yessssss Masssster!!!</color> ";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Uh oh...";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		showFinalBossNewReptile = false;
		showFinalBossNewReptile2 = true;

		
		resetTeleType ();
		
	//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
		
	}

	public void ShowPortalBackTutorial(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss
		
		LevelCompleteObj.LearnSpellsButton.SetActive (false);
		
		LevelCompleteObj.UpgradableStats.SetActive (false);
		
		SpriteHolder.GetComponent<Image> ().sprite = GirlSurprised;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Oh no... He went back with the <color=green>Reptillians</color> ";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Blast!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
	}

	public void ShowPortalBackTutorial2(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		
		SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "We must go back and defeat him, this is our <color=red>Last Chance!!</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Im Ready!!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		showPortalBack = false;
		showPortalBack2 = true; //to go to next pane
		
	}

	public void TurnBackNowDialogue(){
		
		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "This is your last chance, Turn Back now or <color=red>DIE!!!</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Never!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);

		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void TurnBackNowDialogue2(){
		
		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "No...It wasn't supposed to end like this...";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Its over...";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

		TurnBackNow2=true;

	}

	public void TurnBackNowDialogue3(){
		
		SpriteHolder.GetComponent<Image> ().sprite = EvilBossSprite;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "You have won this time, but my Evil will live on! Yarrrghhhhhh";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "I Win!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();

		TurnBackNow3 = true;
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
		
	}

	public void DefeatedOurMasterDialogue(){
		
		SpriteHolder.GetComponent<Image> ().sprite = Reptillian;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=green>You did usssss a favor by defeating the Ssssshadow Dragon...</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Huh...?";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();


		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void DefeatedOurMasterDialogue2(){

		DefeatedOurMaster2 = true;
		DefeatedOurMaster = false;

				SpriteHolder.GetComponent<Image> ().sprite = Reptillian;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=green> Now we ssssummon a new masssster and rule!</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Never!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void NeverMakeItAliveDialogue(){
		
		NeverMakeItAlive = true;
		
		SpriteHolder.GetComponent<Image> ().sprite = Reptillian;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=red> Hssss!! You'll never make it to the temple alive!</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Watch me!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void Halt(){
		
		HaltBool = true;
		
		SpriteHolder.GetComponent<Image> ().sprite = Golem;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=red> Halt!</color> ... Only those worthy may enter!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Im Worthy!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void Halt2(){
		
		HaltBool2 = true;
		HaltBool = false;
		
		SpriteHolder.GetComponent<Image> ().sprite = Golem;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Then proove yourself puny human!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Lets go!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void YoureTooLate1(){
		
		Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite = Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite;
		//makes it so there isnt a rune blocking the last boss
		
		LevelCompleteObj.LearnSpellsButton.SetActive (false);
		
		GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ().FinalBossLaugh();
		
		SpriteHolder.GetComponent<Image> ().sprite = FinalBossAct2;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Muahaha, Another world for me to destroy!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "No Way...";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();



		TooLate1 = true;
		//showFinalBossStolenBook2=true;
		
	}
	
	public void YoureTooLate2(){
		
		//	SpriteHolder.GetComponent<Image> ().sprite = GirlNormal;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "I wont waste my time with you. Come find me when you're ready!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Get Back Here!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		TooLate1 = false;
		TooLate2 = true;
		//	showFinalBossStolenBook3=true;
		
		print ("IS CALLING SHOW FINAL BOSS BOOK2!");
		
	}

	public void CutYouGood1(){
		
		CutYouBool1 = true;
		
		SpriteHolder.GetComponent<Image> ().sprite = PurpleReptile;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Look what we have here...a human?! HsssHsssHsssHsss!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Huh?";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}
	
	public void CutYouGood2(){
		
		CutYouBool2 = true;
		CutYouBool1 = false;
		
		SpriteHolder.GetComponent<Image> ().sprite = PurpleReptile;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "You better <color=red> Move Fast</color> or im gonna cut ya real nice!!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Hiya!!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

	public void FinalBossAct2FirstDeath(){
		
		SpriteHolder.GetComponent<Image> ().sprite = FinalBossAct2;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Im...Impossible! Such powerful Rune Magic...";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Thats right";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		FinalBossAct2DeathBool=true;
		
	}

	public void FinalBossAct2FirstDeath2(){
		
		SpriteHolder.GetComponent<Image> ().sprite = FinalBossAct2;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "Maybe one day... Evil shall rise again... In a second Rune Dungeon....Dahhhh";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "We'll See!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		FinalBossAct2DeathBool = false;
		FinalBossAct2DeathBool2=true;
		
	}

	public void WhatKindOfArmy(){
		
		SpriteHolder.GetComponent<Image> ().sprite = FinalBossAct2;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "What kind of evil army is this?!";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Umm...?";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}
	
	public void WhatKindOfArmy2(){
		
		KindOfArmy2 = true;
		KindOfArmy = false;
		
		SpriteHolder.GetComponent<Image> ().sprite = FinalBossAct2;
		
		QuestText.GetComponent<TextMeshProUGUI>().text = "<color=red>Dont let him in my RUNE DUNGEON!!!!</color>";
		
		QuestButtonText.GetComponent<TextMeshProUGUI>().text = "Never!";
		
		QuestButton.SetActive (true);
		
		QuestDisplay.SetActive(true);
		
		resetTeleType ();
		
		//	GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().spawnStolenBookTutorial ();
	}

}