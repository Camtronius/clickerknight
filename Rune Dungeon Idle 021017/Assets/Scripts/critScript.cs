﻿using UnityEngine;
using System.Collections;

public class critScript : MonoBehaviour {

	// Use this for initialization

	public GameObject critNotifier; //plays the animation showing if the last attack was a crit or not
	public bool didCrit = false;

	void Start () {
	
		//critNotifier = GameObject.Find ("Canvas/CriticalHitEnemy");

	}
	
	public void critNotification(){

		if (didCrit == true) {
			critNotifier.gameObject.SetActive (true);

			didCrit=false;
		}
	}
}
