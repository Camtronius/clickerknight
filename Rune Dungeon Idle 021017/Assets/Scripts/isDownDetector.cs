﻿using UnityEngine;
using System.Collections;

public class isDownDetector : MonoBehaviour {

	// Use this for initialization
	public GameObject playerObject;
	public bool A_playerDead = false;

	public float sloMoCount = 0f;
	public bool sloMo = false;
	//public bool isDown=false;

	void Start () {
	
		if (this.transform.parent.tag == "Player2" && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {

			playerObject = GameObject.Find("Player2");
		}

		if (this.transform.parent.tag == "Player1") {
			
			playerObject = GameObject.Find("Player1");
		}



		this.enabled = false;
	}
	
	void OnCollisionEnter2D (Collision2D player) {
		
		if (this.enabled == true) {

			if (player.gameObject.tag == "Floor") {

//				print (this.gameObject.name + " has collider with the floor");
				if(playerObject.GetComponent<TurnOnRagdoll> ()!=null){
				playerObject.GetComponent<TurnOnRagdoll> ().isDown = true;
				}
				if(playerObject.GetComponent<EnemyRagdoll> ()!=null){
					playerObject.GetComponent<EnemyRagdoll> ().isDown = true;
				}

				if(playerObject.GetComponent<EnemyRagdoll> ()!=null && this.transform.parent.name == "Skele"){
					playerObject.GetComponent<EnemyRagdoll> ().BoneDropSound();
				}

			}


		}
	}
}

