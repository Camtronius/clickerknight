﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCollider : MonoBehaviour {

	//public Color bldColor;
	public Material bloodSpat;
	public Material bloodCircle;
	public bool isWoundFx;

	public bool startDeactivation = false;
	public float countDownTime = 0.5f;

	void OnParticleCollision(GameObject other){

		if (this.gameObject.GetComponent<ParticleSystemRenderer> ().material != bloodSpat && isWoundFx==false) {
			this.gameObject.GetComponent<ParticleSystemRenderer> ().material = bloodSpat;
				startDeactivation = true;

		}
	}

	void Update () {

		if (startDeactivation == true) {

			countDownTime -= Time.deltaTime;

			if (countDownTime <= 0) {
				this.gameObject.SetActive (false);
				countDownTime = 0.5f;
				startDeactivation = false;
				this.gameObject.GetComponent<ParticleSystemRenderer> ().material = bloodCircle;

			}
		}

	}


}
