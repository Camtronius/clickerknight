﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class SpellUpgrades : MonoBehaviour {

	public enum SpellType {Fire,Water,Earth,Wind,Fire2,Water2,Earth2,Wind2}; //Wind2 can be lightning
	public SpellType ItemSpellType;

	public double _itemCost;
	public double _newItemCost; //this is the cost after the level is factored in
	public double _itemDMG;
	public int _itemLV;

	public string _itemName;

	public int itemNumber; //this is the number this item corresponds to the data loaded depending on the wpn unlocked
    public int indexNumber; //this is the number this item corresponds to its place in the list of wpnUpgrades (0-5)

    public TextMeshProUGUI itemCost;
	public TextMeshProUGUI itemLv;
	public TextMeshProUGUI itemDMG;
	public TextMeshProUGUI itemNameText;

	public Image GreyScreen;
	public Image SpellSprite;

	public Sprite upgradeSprite;
	public Sprite cantUpgradeSprite;
	public Sprite nullSprite;

	public Image buttonSprite;

	public MasterSpellList masterSplList;

	void Start () {
		updateSpellCosts ();
	}
	
	public void updateSpellCosts(){
		if (itemNumber < MainMenu.LVCAP+3) { //bcos there are only 49 instances of weapons
			
			_itemLV = PlayerPrefs.GetInt ("Spl" + itemNumber.ToString ());

			//newstuff
			_itemCost = masterSplList.getSplCost (indexNumber); // gets the cost of the wpn

			_itemName = masterSplList.getSplName (indexNumber);

			_itemDMG = masterSplList.getSplDmg (indexNumber);

			itemNameText.text = _itemName;

			SpellSprite.sprite = masterSplList.getSpellIcon (indexNumber);

			ItemSpellType = (SpellUpgrades.SpellType)(masterSplList.getSplType (indexNumber));//fireball
			//new stuff end

			_newItemCost = _itemCost + _itemLV * _itemCost;


			if (_itemLV < 5) {
				if (Camera.main.GetComponent<MainMenu> ().reduceSpellCost == true) {

					_newItemCost = _newItemCost * (Camera.main.GetComponent<MainMenu> ().SpellCostReduction-Camera.main.GetComponent<MainMenu> ().BarginBinReductionPrestige);

					itemCost.text = LargeNumConverter.setDmg (_newItemCost); //10% discount for test


				} else {

					_newItemCost = _newItemCost * (1-Camera.main.GetComponent<MainMenu> ().BarginBinReductionPrestige);

					itemCost.text = LargeNumConverter.setDmg (_newItemCost);

				}

                checkButtonSprite(); //this needs to go here so it will check the price afer the newitemcost is calc'd

                itemDMG.text = LargeNumConverter.setDmg ((_itemDMG + (_itemLV) * (_itemDMG / 5))) + " DMG";

			} else {
				itemCost.text = "";
				itemDMG.text = LargeNumConverter.setDmg ((_itemDMG + (_itemLV) * (_itemDMG / 5))) + " DMG";
				buttonSprite.sprite = nullSprite;

			}


			itemLv.text = "Item LV: " + _itemLV.ToString ();
		} else {

			this.gameObject.SetActive(false);

		}

	}

	public void checkButtonSprite(){

		ControlPlayer1Character playerObj = masterSplList.playerObj.GetComponent<ControlPlayer1Character> ();

		if (playerObj.playerGold >= _newItemCost) {

			buttonSprite.sprite = upgradeSprite;

		} else {

			buttonSprite.sprite = cantUpgradeSprite;

		}

	}

	public void purchaseSpell(){                        //has not ben updated yet!!

        if (buttonSprite.sprite == upgradeSprite)
        {
            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UpgradeSpell();//plays upg spoell sound
        }

        for (int i = 0; i < masterSplList.multiplier; i++) //buys amt of item depending on multiplier
        {
            buySpell();
        }

        
    }

    private void buySpell()
    {

        ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<MasterSpellList>().playerObj.GetComponent<ControlPlayer1Character>();

        if (playerObj.playerGold < _newItemCost)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Not Enough Gold!");

        }

        if (playerObj.playerGold >= _newItemCost && PlayerPrefs.GetInt("Spl" + itemNumber.ToString()) < 5)
        { //so level cant go above 5

            //	print ("item purchased");

            masterSplList.unlockWeaponAnim(this.gameObject.GetComponentInChildren<Button>().transform, SpellSprite.sprite, SpellSprite.gameObject.transform);

            PlayerPrefs.SetInt("Spl" + itemNumber.ToString(), _itemLV + 1); //levels up the item


            if (_itemLV + 1 >= 5)
            {

                masterSplList.unlockedSplArray[itemNumber] = 2;
                masterSplList.writeSplArray();

            }



            playerObj.playerGold -= _newItemCost;


            PlayerPrefs.SetString("playerSplDmg", (_itemDMG + PlayerPrefs.GetInt("Spl" + itemNumber.ToString()) * (_itemDMG / 5)).ToString());//the only other place this is used is in ControlPlayer1 and referencfed in Main Menu

            //		print ("players spell dmg is now " + PlayerPrefs.GetFloat("playerSplDmg"));

            PlayerPrefs.SetInt("CurrentSpell", (int)ItemSpellType);

            if (_itemLV + 1 >= 5)
            {

                // masterSplList.unlockedSplArray[itemNumber] = 2;
                // masterSplList.writeSplArray();
                masterSplList.updateSpellAvailability();
                masterSplList.loadSpellUpgrades();
            }

            masterSplList.updatePrices();

            //		print ("the item spell type is" + (int)ItemSpellType);

        }
    }
}
