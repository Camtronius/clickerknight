﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class DragonSpellPath : MonoBehaviour {

	public int createdByPlayer = 0;

	public float timeAlive = 0;
	public GameObject SpellCreator; //this is for the isSpellComplete condition

	Vector3 m_centerPosition;
	float m_degrees;
	
	[SerializeField]
	float m_speed = 1.0f;
	
	[SerializeField]
	float m_amplitude = 1.0f;
	
	[SerializeField]
	float m_period = 1.0f;

	public GameObject flameDragonChild;
	//public GameObject ParticleExplosion;
	//public GameObject smallExplosions;
	public GameObject VeryLargeExplosion;
	public GameObject ExplosiveForce;
	public GameObject ScreenShaker;

	private bool closeDragonMouth = false;


	// Use this for initialization
	void Start () {

		ScreenShaker = GameObject.Find ("ScreenShakerController");

		SpellCreator = GameObject.Find ("SpellListener");

		m_centerPosition = transform.position;

	}
	
	// Update is called once per frame
	void Update () {

		timeAlive += Time.deltaTime;
		
		if(timeAlive>5){
			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
			Destroy(this.gameObject);
			}

			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
				Destroy(this.gameObject);
			}
			
		}
	
		if (createdByPlayer == 1) {

		//	if (closeDragonMouth == false) {
				float deltaTime = Time.deltaTime;
		
				// Move center along x axis
				m_centerPosition.x += deltaTime * m_speed;

				// Update degrees
				float degreesPerSecond = 360.0f / m_period;
				m_degrees = Mathf.Repeat (m_degrees + (deltaTime * degreesPerSecond), 360.0f);
				float radians = m_degrees * Mathf.Deg2Rad;

				transform.Rotate (new Vector3 (0, 0, m_amplitude * Mathf.Sin (radians)));


				// Offset by sin wave
				Vector3 offset = new Vector3 (0.0f, m_amplitude * Mathf.Sin (radians), 0.0f);
				transform.position = m_centerPosition + offset;
		//	}
		}

		if (createdByPlayer == 2) {
			
		//	if (closeDragonMouth == false) {
				float deltaTime = Time.deltaTime;
				
				// Move center along x axis
				m_centerPosition.x -= deltaTime * m_speed;
				
				// Update degrees
				float degreesPerSecond = 360.0f / m_period;
				m_degrees = Mathf.Repeat (m_degrees + (deltaTime * degreesPerSecond), 360.0f);
				float radians = m_degrees * Mathf.Deg2Rad;
				
				transform.Rotate (new Vector3 (0, 0, m_amplitude * Mathf.Sin (radians)));
				
				
				// Offset by sin wave
				Vector3 offset = new Vector3 (0.0f, m_amplitude * Mathf.Sin (radians), 0.0f);
				transform.position = m_centerPosition + offset;
			}
	//	}

	}

	void OnTriggerEnter2D (Collider2D player) {
		
//		if (createdByPlayer==1 && player.tag == "Player2" && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//
//			flameDragonChild.GetComponent<Animator>().speed = 5;
//			flameDragonChild.GetComponent<Animator>().SetBool ("CloseMouth", true);
//
//			closeDragonMouth=true;
//
//		//	ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (player.transform.position.x, player.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (1.5f,0.2f); //shakes the screen
//
//		//	smallExplosions = Instantiate (smallExplosions, new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
//			VeryLargeExplosion = Instantiate (VeryLargeExplosion, new Vector3 (player.GetComponent<ControlPlayer2Character>().Player2Torso.transform.position.x,
//			                                                                   player.GetComponent<ControlPlayer2Character>().Player2Torso.transform.position.y, 0), Quaternion.identity) as GameObject;
//			player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//			ExplosiveForce = Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//
//
////for damage
//			if(player.GetComponent<ControlPlayer2Character>().isShielded==false){
//				
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "FireDragon")
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[1] //adding fire item mods
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod);
//				
//				//condition for super effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//					
//				}
//				
//				//condition for Not very effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//
//					calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//					
//				}
//				
//				//hitting p2
//				//check for crit against p2
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
//				   ){ //did the player crit on their turn??
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//
//				player.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p1 notification
//
//			}
////end dmg
//
//			if(player.GetComponent<ControlPlayer2Character>().player2FlameDOT==false
//			   && player.GetComponent<ControlPlayer2Character>().isShielded==false){
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().flameDOTManager(0,4);
//
//			player.GetComponent<ControlPlayer2Character>().justBurned=true;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().EnemyBurned ();
//
//			}
//
//			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
//		}
//
//		if (createdByPlayer==2 && player.tag == "Player1" && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) { //for pvp
//
//			flameDragonChild.GetComponent<Animator>().speed = 5;
//			flameDragonChild.GetComponent<Animator>().SetBool ("CloseMouth", true);
//
//			closeDragonMouth=true;
//
//		//	ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (player.transform.position.x, player.transform.position.y, 0), Quaternion.identity) as GameObject;
//		//	smallExplosions = Instantiate (smallExplosions, new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (1.5f,0.2f); //shakes the screen
//
//			VeryLargeExplosion = Instantiate (VeryLargeExplosion, new Vector3 (player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.x,
//			                                                                   player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//			player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//			ExplosiveForce = Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//
////for damage
//			if(player.GetComponent<ControlPlayer1Character>().isShielded==false ){
//
//			float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "FireDragon")
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[1] //adding fire item mods
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod););
//
//			//condition for super effective
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//				
//
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//			}
//			
//			//condition for Not very effective
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//				
//
//				calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//			}
//			
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//			   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				== Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//				
//			}
//
//				//hit p1
//				//check for crit against p1
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//				   ){ //did the player crit on their turn??
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p1 notification
//
//			}
////end dmg
//
//			if(player.GetComponent<ControlPlayer1Character>().player1FlameDOT==false 
//			   && player.GetComponent<ControlPlayer1Character>().isShielded==false){
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().flameDOTManager(4,0);
//			
//
//			player.GetComponent<ControlPlayer1Character>().justBurned=true;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().EnemyBurned ();
//
//			}
//			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
//
//		}
//
//		if (createdByPlayer==2 && player.tag == "Player1" && Camera.main.GetComponent<CampaignGameOrganizer>()!=null) { //for final boss act 2
//			
//			flameDragonChild.GetComponent<Animator>().speed = 5;
//			flameDragonChild.GetComponent<Animator>().SetBool ("CloseMouth", true);
//			
//			closeDragonMouth=true;
//				
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (1.5f,0.2f); //shakes the screen
//			
//			VeryLargeExplosion = Instantiate (VeryLargeExplosion, new Vector3 (player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.x,
//			                                                                   player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.y, 0), Quaternion.identity) as GameObject;
//	
//			
//			//for damage
//			if(player.GetComponent<ControlPlayer1Character>().isShielded==false ){
//				
//				float DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//
//				
//				//condition for super effective
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//					
//					
//					DamageToPlayer = 2*DamageToPlayer;
//					
//					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				
//				//condition for Not very effective
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
//					
//					
//					DamageToPlayer = (0.5f)*DamageToPlayer;
//					
//					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
//				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
//				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//					
//				}
//
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//
//				if(DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
//					
//					player.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
//					player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//					ExplosiveForce.GetComponent<ExplosiveForce>().explosionColliderRadius = 1.0f;
//
//					ExplosiveForce = Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//				}
//
//
//			}
//			//end dmg
//			
//			if(player.GetComponent<ControlPlayer1Character>().player1FlameDOT==false 
//			   && player.GetComponent<ControlPlayer1Character>().isShielded==false){
//				Camera.main.GetComponent<CampaignGameOrganizer>().flameDOTManager(4,0);
//				
//				
//				player.GetComponent<ControlPlayer1Character>().justBurned=true;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer> ().EnemyBurned ();
//				
//			}
//			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
//			
//		}
//
//		if (player.tag == "Enemy") {
//		
//			damageEnemy(player);
//			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
//
//		}
//
//		if (player.tag == "Player2" && Camera.main.GetComponent<CampaignGameOrganizer>()!=null && createdByPlayer==1) {
//			
//			damageEnemy(player);
//			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
//
//		}
//
//
	}

	public void damageEnemy(Collider2D player){

			
			flameDragonChild.GetComponent<Animator>().speed = 5;
			flameDragonChild.GetComponent<Animator>().SetBool ("CloseMouth", true);
			
			closeDragonMouth=true;
			
		//	ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (player.transform.position.x, player.transform.position.y, 0), Quaternion.identity) as GameObject;
		//	smallExplosions = Instantiate (smallExplosions, new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
			
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (1.5f,0.2f); //shakes the screen
			
			VeryLargeExplosion = Instantiate (VeryLargeExplosion, new Vector3 (player.GetComponent<ControlEnemy>().transform.position.x,
			                                                                   player.GetComponent<ControlEnemy>().transform.position.y, 0), Quaternion.identity) as GameObject;

			//for damage
			if(player.GetComponent<ControlEnemy>().isShielded==false){
				
				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "FireDragon")
				                              + PlayerPrefs.GetFloat ("FireItemMod") 
			                              + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
				
				//condition for super effective
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){

					calculatedHealthLoss = 2*calculatedHealthLoss;
					
					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
	
				}
				
				//condition for Not very effective
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
					
					calculatedHealthLoss = (.5f)*calculatedHealthLoss;
					
					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
	
				}
				
				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
				   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
		
				}

			//put the crit scripts here

			if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
				
				calculatedHealthLoss = 2*calculatedHealthLoss;
				print("CRITS!!!!!!!!!");
				
			}

			Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);

			player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;

				if(calculatedHealthLoss >= player.GetComponent<ControlEnemy>().playerHealthCurrent){
					
					player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
					player.gameObject.GetComponent<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll
					player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll();
					
					ExplosiveForce = Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.GetComponent<EnemyRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity) as GameObject;
					
				}else{
				
				if(player.gameObject.GetComponent<ControlEnemy>().EnemyFlameDOT==false
				   && player.gameObject.GetComponent<ControlEnemy>().isShielded==false){
					Camera.main.GetComponent<CampaignGameOrganizer>().flameDOTManager(0,4);
					
					player.gameObject.GetComponent<ControlEnemy>().justBurned=true;
					player.gameObject.GetComponent<ControlEnemy>().EnemyFlameDOT=true;
				}

				Camera.main.GetComponent<CampaignGameOrganizer> ().EnemyBurned ();

			}

		}



			//end dmg
			/*
			if(player.GetComponent<CampaignGameOrganizer>().player1FlameDOT==false 
			   && player.GetComponent<CampaignGameOrganizer>().isShielded==false){
				Camera.main.GetComponent<CampaignGameOrganizer>().flameDOTManager(4,0);

				
				player.GetComponent<ControlPlayer1Character>().justBurned=true;

			}
			*/	

		//end of enemy hit by dragon

	}

	public void resetAnimSpeed(){
		flameDragonChild.GetComponent<Animator>().speed = 1;
	}

}
