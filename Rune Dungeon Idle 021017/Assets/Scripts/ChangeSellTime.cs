﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeSellTime : MonoBehaviour {

	// Use this for initialization
	public TextMeshProUGUI timeText;
    public TextMeshProUGUI lvText;


    public int sellTime;
    public int levelLoad;



    void OnEnable () {

        levelLoad = 0;
		sellTime = PlayerPrefs.GetInt ("AutoSellTime");
		timeText.text = sellTime.ToString () + "s";

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClicked (){

		if (sellTime >= 10) {

			sellTime -= 5;

		}else{

			sellTime = 60;

	}
		timeText.text = sellTime.ToString () + "s";
		PlayerPrefs.SetInt ("AutoSellTime", sellTime);
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

    }

    public void OnClickedLevelHax()
    {

        if (levelLoad < 150)
        {

            levelLoad +=1;

        }
        else
        {

            levelLoad = 1;

        }
        PlayerPrefs.SetInt("LVLOAD", levelLoad);
        lvText.text = levelLoad.ToString() + "";
    }

    public void loadThatLv()
    {

        Camera.main.GetComponent<MainMenu>().loadAnyLevel(PlayerPrefs.GetInt("LVLOAD"));
    }
}