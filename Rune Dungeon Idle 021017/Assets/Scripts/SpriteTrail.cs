﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class SpriteTrail : MonoBehaviour {

public 	List<GameObject> trailParts = new List<GameObject>();
//	public AudioSource trailSoundSource;
//	public AudioClip traiSound;

	void Start()
	{

	//	trailSoundSource.PlayOneShot (traiSound, 0.6f);
		InvokeRepeating("SpawnTrailPart", 0, 0.075f); // replace 0.2f with needed repeatRate
	}
	
	void SpawnTrailPart()
	{
		GameObject trailPart = new GameObject();
		SpriteRenderer trailPartRenderer = trailPart.AddComponent<SpriteRenderer>();
		trailPart.GetComponent<SpriteRenderer> ().sortingOrder = 21;
		trailPart.GetComponent<SpriteRenderer> ().sortingLayerName = "UIOverPlayer";
		trailPartRenderer.sprite = GetComponent<SpriteRenderer>().sprite;
		trailPart.transform.position = transform.position;
		trailPart.transform.localScale = transform.localScale; // We forgot about this line!!!


		int randomInt = Random.Range (0, 3);

		if(randomInt==0){
		//	trailPartRenderer.color = Color.grey;

			trailPartRenderer.color = Color.red;
		} else
		if(randomInt==1){

		//	trailPartRenderer.color = Color.grey;

			trailPartRenderer.color = Color.green;
		} else 
		if(randomInt==2){

		//	trailPartRenderer.color = Color.grey;

			trailPartRenderer.color = Color.magenta;
		}

		trailParts.Add(trailPart);
		
		StartCoroutine(FadeTrailPart(trailPartRenderer));

		Destroy(trailPart, 0.2f); // replace 0.5f with needed lifeTime

		trailParts = trailParts.Where(item => item != null).ToList(); //cleans up the list from null objects

	}

	void OnDestroy() {
		CancelInvoke("SpawnTrailPart");
	}
	
	IEnumerator FadeTrailPart(SpriteRenderer trailPartRenderer)
	{
		Color color = trailPartRenderer.color;
		color.a -= 0.6f; // replace 0.5f with needed alpha decrement
		trailPartRenderer.color = color;

		yield return new WaitForEndOfFrame();
	}

}