﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;


public class PotionMasterScript : MonoBehaviour {

	// Use this for initialization
	public List<GameObject> WeaponUnlockAnims = new List<GameObject> ();

	public List<BrewPotion> potionsList = new List<BrewPotion> ();
	public List<UseRedPotion> potionsListToDrink = new List<UseRedPotion> ();
    public List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

    public int maxPotionBrewed; //brew 2 potions unless u have the new unlock potion pkg

    public IngredientsMaster masterIngredientsHolder;
	public EnemyStatsInheriter potionDataScript;

	public Sprite maxSprite;
	public Sprite BrewPotionSprite;
	public Sprite WatchAdSprite;

    public GameObject potionAdGfx;
    public GameObject thirdPotionLock;

    public GameObject watchAdObject; //this is for having 5 potions and being able to watch the ad

    void OnEnable () {

        masterLoadOnEnable();

    }

    public void masterLoadOnEnable()//this is so we can call from another script.
    {
        loadPotionData();
        maxPotionBrewed = getMaxPotionBrewed();
        checkMaxPotions();
    }

	public void getPotionSaves(){



	}

    public int getMaxPotionBrewed()
    {

        int maxPots = 2 + ObscuredPrefs.GetInt("UtilityPack"); //allows 2 potions brewed unless u have the extra potion pkg

        return maxPots;
    }

	public void unlockWeaponAnim(Transform location, Sprite wpnSprite, Transform swordSpritePos)
    {

		for (int i = 0; i < WeaponUnlockAnims.Count; i++) {

			if (!WeaponUnlockAnims [i].activeInHierarchy) {

				WeaponUnlockAnims [i].GetComponent<Image> ().color = Color.yellow;
				WeaponUnlockAnims [i].GetComponent<SpriteHolder> ().imageHeld.sprite = wpnSprite;

				WeaponUnlockAnims [i].transform.position = new Vector3 (location.position.x, location.position.y, 90); //instantiates the beam offscre
                WeaponUnlockAnims[i].GetComponent<SpriteHolder>().imageHeld.transform.position = new Vector3(swordSpritePos.position.x, swordSpritePos.position.y, 90); //instantiates the beam offscre


                //	magicCritDmgList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

                WeaponUnlockAnims[i].SetActive (true);

				break;
			}

		}

	}

	public void loadPotionData(){

        //temp moving the code to useredpotion
        if (data.Count == 0)
        {
            data = CSVReader.Read("Potions");
        }

    }

    // Update is called once per frame
    void Update () {
		
	}

	public void checkMaxPotions(){

		bool potionSlotsAvailable = false;

		for (int i = 0; i < maxPotionBrewed; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite == potionsListToDrink [i].nullSprite) {
				potionSlotsAvailable = true;
			}
		}

		if (potionSlotsAvailable == true) {

			for (int i = 0; i < potionsList.Count; i++) {

				if (potionsList [i]._potionType != BrewPotion.PotionType.AdExp
					&& potionsList [i]._potionType != BrewPotion.PotionType.AdRefresh
                    && potionsList[i]._potionType != BrewPotion.PotionType.Event
                    && potionsList [i]._potionType != BrewPotion.PotionType.AdGold
				    && potionsList [i]._potionType != BrewPotion.PotionType.AdSilver) {
					
					potionsList [i].buttonImage.sprite = BrewPotionSprite;
				} else {
					potionsList [i].watchAdText.enabled = true;

					potionsList [i].buttonImage.sprite = WatchAdSprite;

				}



			}

		} else {
			for (int i = 0; i < potionsList.Count; i++) {
				if (potionsList [i].watchAdText != null) {
					potionsList [i].watchAdText.enabled = false;
				}
				potionsList [i].buttonImage.sprite = maxSprite;
			}
		}

        if (potionsListToDrink[0].spriteHolder.sprite == potionsListToDrink[0].nullSprite 
            && potionsListToDrink[1].spriteHolder.sprite == potionsListToDrink[1].nullSprite
            && PlayerPrefs.GetInt("CurrentLevel")>3 & PlayerPrefs.GetInt("vidAdsViewed")==0)
        {
            potionAdGfx.SetActive(true);
        }else{
            potionAdGfx.SetActive(false);
        }
        


    }

	public void setRedPotActive(int potionLevel){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.AtkDmgIncr;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

		//		print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();

	}

	public void setBluePotActive(int potionLevel){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.MagDmgIncr;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

		//		print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();

	}

	public void setGoldPotActive(int potionLevel){ //need to change this from UseRedPotion atk rate incr to something else

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.AdGold;
				potionsListToDrink [i].setPotionAttributes (potionLevel); //this just changes the picture

				savePotionInventory ();

	//			print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();


	}

	public void setSilvPotActive(int potionLevel){ //need to change this from UseRedPotion atkrate to something else

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.AdSilver;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

	//			print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();


	}

	public void setPurpPotionActive(int potionLevel){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.AdRefresh;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

		//		print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();

	}

    public void setWhitePotionActive(int potionLevel)
    {

        for (int i = 0; i < potionsListToDrink.Count; i++)
        {

            if (potionsListToDrink[i].spriteHolder.sprite == potionsListToDrink[i].nullSprite)
            {

                potionsListToDrink[i]._potionType = UseRedPotion.PotionType.Event;
                potionsListToDrink[i].setPotionAttributes(potionLevel);

                savePotionInventory();

                //		print ("setting potions attributes");
                break;
            }

        }

        checkMaxPotions();

    }

    public void setOrangePotActive(int potionLevel){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.AdExp;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

	//			print ("setting potions attributes");
				break;
			}

		}

		checkMaxPotions ();

	}

	public void setGreenPotionActive(int potionLevel){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite==potionsListToDrink [i].nullSprite) {

				potionsListToDrink [i]._potionType = UseRedPotion.PotionType.HPregen;
				potionsListToDrink [i].setPotionAttributes (potionLevel);

				savePotionInventory ();

	//			print ("setting potions attributes");
				break;
			}

		}


		checkMaxPotions ();

	}

	public void savePotionInventory(){

		for (int i = 0; i < potionsListToDrink.Count; i++) {

			if (potionsListToDrink [i].spriteHolder.sprite !=potionsListToDrink [i].nullSprite) {

				PlayerPrefs.SetInt (potionsListToDrink [i]._potionType.ToString () + (i+1).ToString(), i + 1); //if its 0 then there is no potion, but if its 1 or 2 there is a potion and that would be its place in the inventory

		//		print (potionsListToDrink [i]._potionType.ToString () + " the saved potion pref is: " + PlayerPrefs.GetInt (potionsListToDrink [i]._potionType.ToString ()).ToString());
					
			} else {

				PlayerPrefs.SetInt (potionsListToDrink [i]._potionType.ToString () + (i+1).ToString(), 0); //if its 0 then there is no potion, but if its 1 or 2 there is a potion and that would be its place in the inventory

			}
		}
	}
}
