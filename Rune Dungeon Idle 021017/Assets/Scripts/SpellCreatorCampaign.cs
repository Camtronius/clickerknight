﻿using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms;


public class SpellCreatorCampaign : MonoBehaviour {

	public List<int> sentSpell = new List<int>();
	public List<int> receivedSpell = new List<int>();

	public int fireCount;
	public int waterCount;
	public int windCount;
	public int leafCount;

	public int primarySpellType = 4;

	public GameObject player1;	
	public GameObject player2; //placeholder for the enemy

	public CampaignLevelsEnemySpawner campaignLevelEnemySpawnerObj;
	public TutorialCampaign TutorialObj;

	public bool spellCompleted = false;
	public bool sentByPlayer1 = false;

	// Use this for initialization
	void Start () {

	//	PlayGamesPlatform.Activate();

	}

	public void sentSpellDesignatorPlayer1(){

		for (int i = 0; i< sentSpell.Count; i++) {

			switch (sentSpell[i]){
			
			case 0:
				windCount+=1;
				break;

			case 1:
				fireCount+=1;
				break;

			case 2:
				waterCount+=1;
				break;

			case 3:
				leafCount+=1;
				break;

			}
		}
		determineSpell(1);

	}

	public void sentSpellDesignatorPlayer2(){
		
		for (int i = 0; i< sentSpell.Count; i++) {
			
			switch (sentSpell[i]){
				
			case 0:
				windCount+=1;
				break;
				
			case 1:
				fireCount+=1;
				break;
				
			case 2:
				waterCount+=1;
				break;
				
			case 3:
				leafCount+=1;
				break;
			}
		}
		
		determineSpell(2);
	}

	public void recievedSpellDesignatorPlayer(int Player){

		for (int i = 0; i< receivedSpell.Count; i++) {
			
			switch (receivedSpell[i]){
				
			case 0:
				windCount+=1;
				break;
				
			case 1:
				fireCount+=1;
				break;
				
			case 2:
				waterCount+=1;
				break;
				
			case 3:
				leafCount+=1;
				break;
				
			}
		}

		if (Player == 1) {

			player1.GetComponent<TurnOnRagdoll> ().healthRecordable = true;

			determineSpell(2); //because player 1 is receiving a spell from player two... the last casted spell...

			resetAllCounts();

		}

		if (Player == 2) {
		//	player2.GetComponent<TurnOnRagdoll> ().healthRecordable = true;

			print ("determining the spell from the skeleton!");

			determineSpell(1);

			resetAllCounts();

		}


	}

	public void determineSpell(int Player){

		/*
		print ("flame dragon player pref is " + PlayerPrefs.GetFloat ("FireDragLearned") + 
		        
			" water bub play pref is " + PlayerPrefs.GetFloat ("WaterBubbleLearned") + " treant is " +

		       PlayerPrefs.GetFloat ("TreantLearned") + "SHIELD spell is " + PlayerPrefs.GetFloat ("ShieldLearned"));

		*/

		Camera.main.GetComponent<CampaignGameOrganizer>().enemyDamageAmount.GetComponent<TextMeshProUGUI>().color = Color.red;


		if (Player == 1 && player1.GetComponent<ControlPlayer1Character>().player1Stun==false) {

			if (fireCount == 3 ) { //fireball spell

				if(TutorialObj.castFireSpell2==true){
					
					TutorialObj.EndFireballDialogue2();
				}

					if(TutorialObj.castFireSpell==true){

						TutorialObj.EndFireballDialogue();
					}

				player1.GetComponent<ControlPlayer1Character> ().castFireSpell (); 
			}

			else if (fireCount == 4 && PlayerPrefs.GetFloat ("FireDragLearned")==1){ //flame dragon learned

				player1.GetComponent<ControlPlayer1Character> ().castDragSpell (); 

			}
					else if (fireCount == 4 && PlayerPrefs.GetFloat ("FireDragLearned")==0){ //flame dragon not learned default to fireball
								
						player1.GetComponent<ControlPlayer1Character> ().castFireSpell (); 

					}

			else if(leafCount == 3 && PlayerPrefs.GetFloat ("BoulderThrowLearned")==1){
				
				player1.GetComponent<ControlPlayer1Character> ().castRockThrowSpell (); //this starts the rock spell
			}

				else if(leafCount == 4 && PlayerPrefs.GetFloat ("TreantLearned")==1){
					
					player1.GetComponent<ControlPlayer1Character> ().castTreantSpell (); //if treant learned
				}

					else if(leafCount == 4 && PlayerPrefs.GetFloat ("TreantLearned")==0 && PlayerPrefs.GetFloat ("BoulderThrowLearned")==1){
						
						player1.GetComponent<ControlPlayer1Character> ().castRockThrowSpell (); //tdefault to rock throw
					
						//prompt spell not learned!
						}
		
			else if(waterCount == 3 && PlayerPrefs.GetFloat ("WaterBeamLearned")==1){
				player1.GetComponent<ControlPlayer1Character> ().castWaterBeamSpell (); //this starts the water beam spell
				
			}

				else if(waterCount== 4 && PlayerPrefs.GetFloat ("WaterBubbleLearned") == 1){

					player1.GetComponent<ControlPlayer1Character> ().castWaterBubble(); 

				}

					else if(waterCount== 4 && PlayerPrefs.GetFloat ("WaterBubbleLearned") == 0 && PlayerPrefs.GetFloat ("WaterBeamLearned")==1){ //default to water beam if not learned
							
							player1.GetComponent<ControlPlayer1Character> ().castWaterBeamSpell(); 
							
						}

			else if(windCount == 3 && PlayerPrefs.GetFloat ("TornadoLearned")==1){
				player1.GetComponent<ControlPlayer1Character> ().castTornadoSpell(); //this starts the tornado spell
				
			}

				else if(windCount == 4 && PlayerPrefs.GetFloat ("InvisibilityLearned") == 1){
					player1.GetComponent<ControlPlayer1Character> ().castInvisSpell (); 
					
				}

					else if(windCount == 4 && PlayerPrefs.GetFloat ("InvisibilityLearned") == 0 && PlayerPrefs.GetFloat ("TornadoLearned")==1){
						player1.GetComponent<ControlPlayer1Character> ().castTornadoSpell (); //if invis not learned, defualt to tornado
						
					}

			else if(windCount == 2 && waterCount==2 && PlayerPrefs.GetFloat("LightningBoltLearned")==1){ //This if for LIGHTNING add spell player pref here when ready
				player1.GetComponent<ControlPlayer1Character> ().castLightningSpell (); //if invis not learned, defualt to tornado
				
			}

			else if(leafCount == 2 && fireCount==2 && PlayerPrefs.GetFloat("SolarBeamLearned")==1){ //This is for SOLARBEAM add spell player pref here when ready
					player1.GetComponent<ControlPlayer1Character> ().castSolarBeamSpell (); //if invis not learned, defualt to tornado
				}

			else if(fireCount==1 && leafCount==1 && waterCount==1 && windCount==1 && PlayerPrefs.GetFloat("EnchantSwordLearned")==1){ //this spell has to be learned...

				if(	Camera.main.GetComponent<CampaignGameOrganizer>().swordEnch[0]<=0){

				player1.GetComponent<ControlPlayer1Character> ().castEnchantSword(); //this starts the fire spell
				}else{

					player1.GetComponent<ControlPlayer1Character> ().castDefaultAttack(); //this starts the fire spell
					
					if(TutorialObj.swordAttack==true){
						
						TutorialObj.EndSwordDialogue();
					}
				}
			}

			else {
			
//				print("default atk spell p1");

		//		resetAllCounts(); //using this to see if it clears the elemental affinity icon

				player1.GetComponent<ControlPlayer1Character> ().castDefaultAttack(); //this starts the fire spell

				if(TutorialObj.swordAttack==true){
					
					TutorialObj.EndSwordDialogue();
				}

			}
		}
	
		player2 = GameObject.Find ("Enemy");

		if (Player == 2 && player2.GetComponentInChildren<ControlEnemy>().player2Stun==false
		    && player2.GetComponentInChildren<ControlEnemy>().playerDead==false) {

//			print ("name of the enemy is: " + player2.GetComponentInChildren<ControlEnemy>().gameObject.name);

			if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "Skele"){

				player2.GetComponentInChildren<ControlEnemy> ().castFireSpell (); //this is the bone throw

			}

			if( player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "SkeleWarrior" ||
			   player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "Reptillian"){

				if(fireCount>=3 || leafCount>=3 || windCount>=3 || waterCount>=3){
				
					player2.GetComponentInChildren<ControlEnemy> ().castComboSpell();
				
				}else{

					player2.GetComponentInChildren<ControlEnemy> ().castFireSpell (); //this is the normal atk

				}
			}

			if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "PurpleReptillian"){
					
					player2.GetComponentInChildren<ControlEnemy> ().castPurpReptileAtk();
									
			}

			if( player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "SkeleMage"){
				
				if(windCount>=3){
					
					player2.GetComponentInChildren<ControlEnemy>().setSpellType(0);
					player2.GetComponentInChildren<ControlEnemy> ().castSkeleMageSpell();
					
				}

				if(fireCount>=3){
					//set the spell type
					player2.GetComponentInChildren<ControlEnemy>().setSpellType(1);
					player2.GetComponentInChildren<ControlEnemy>().castSkeleMageSpell();
					
				}

				if(waterCount>=3){

					player2.GetComponentInChildren<ControlEnemy>().setSpellType(2);
					player2.GetComponentInChildren<ControlEnemy> ().castSkeleMageSpell();
					
				}

				if(leafCount>=3){

					player2.GetComponentInChildren<ControlEnemy>().setSpellType(3);
					player2.GetComponentInChildren<ControlEnemy> ().castSkeleMageSpell();
					
				}

				if (windCount <3 && fireCount<3 && waterCount<3 && leafCount<3){

					player2.GetComponentInChildren<ControlEnemy>().setSpellType(4); //this is the null hit
					player2.GetComponentInChildren<ControlEnemy> ().castSkeleMageSpell();

				}

			}



			if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "Wolf" || 
			   player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "ArmoredWolf"){

				if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "ArmoredWolf"){

					if (windCount >=3 || fireCount>=3 || waterCount>=3 || leafCount>=3){
						
						player2.GetComponentInChildren<ControlEnemy> ().castWolfAttack2 (); //this is the bone throw

						
					}else{

						player2.GetComponentInChildren<ControlEnemy> ().castWolfAttack (); //this is the bone throw

					}

				}else{

					player2.GetComponentInChildren<ControlEnemy> ().castWolfAttack (); //this is the bone throw

				}

				
			}

			if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "EvilEnemy" || 
			   player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "FinalBoss"){
			
				if(windCount>=3){
					player2.GetComponentInChildren<ControlEnemy> ().castTornadoSpell();
				}
				if(fireCount>=3){
					player2.GetComponentInChildren<ControlEnemy>().evilGuyFireball();
				}
				if(waterCount>=3){
					player2.GetComponentInChildren<ControlEnemy> ().castWaterBeamSpell();
				}
				if(leafCount>=3){
					player2.GetComponentInChildren<ControlEnemy> ().castRockThrowSpell();
				}
				if (windCount <3 && fireCount<3 && waterCount<3 && leafCount<3){

					player2.GetComponentInChildren<ControlEnemy> ().castPlayerEnemySwordAtk();

					player2.GetComponentInChildren<ControlEnemy>().setSpellType(4); //this is the null hit


				
				}
			}

			if(player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "FinalBossAct2"){
				
				if(windCount>=3){
					player2.GetComponentInChildren<ControlPlayer2Character> ().castTornadoSpell();
				}
				if(fireCount>=3){
					player2.GetComponentInChildren<ControlPlayer2Character>().castDragSpell();
				}
				if(waterCount>=3){
					player2.GetComponentInChildren<ControlPlayer2Character> ().castWaterBubble();
				}
				if(leafCount>=3){
					player2.GetComponentInChildren<ControlPlayer2Character> ().castTreantSpell();
				}
				if (windCount <3 && fireCount<3 && waterCount<3 && leafCount<3){
					
					player2.GetComponentInChildren<ControlPlayer2Character> ().castDefaultAttack();
					
					player2.GetComponentInChildren<ControlEnemy>().setSpellType(4); //this is the null hit
					
					
					
				}
			}

			if( player2.GetComponentInChildren<ControlEnemy>().gameObject.name == "Golem"){
				
				if(fireCount>=3 || leafCount>=3 || windCount>=3 || waterCount>=3){
					
					player2.GetComponentInChildren<ControlEnemy> ().castGolemStunAtk();
					
				}else{
					
					player2.GetComponentInChildren<ControlEnemy> ().castGolemRegAtk (); //this is the normal atk
					
				}
			}

		}

		if (Player == 2 && player2.GetComponentInChildren<ControlEnemy> ().player2Stun == true) {

			print ("enemy is taking a turn while stunned");

			StartCoroutine("TakeTurnWhileStunned"); //this makes it so if the player is stunned, he can respawn

		}


		if (Player == 1 && player1.GetComponent<ControlPlayer1Character>().player1Stun==true) {
			
			StartCoroutine("TakeTurnWhileStunned"); //this makes it so if the player is stunned, he can respawn
		}
		/*
		if (Player == 2 && player2.GetComponent<ControlPlayer2Character>().player2Stun==true) {
			
			StartCoroutine("TakeTurnWhileStunned"); //this makes it so if the player is stunned, he can respawn
		}
		*/
	}

	public void CounterAttackSpell(int createdByPlayer){
	//	print ("counter atk stopped early...");

		player2 = GameObject.Find ("Enemy");

		if (createdByPlayer == 1 && player2.GetComponentInChildren<ControlEnemy>()!=null
		    && player2.GetComponentInChildren<ControlEnemy>().playerDead==false
		    && player2.GetComponentInChildren<ControlEnemy>().player2Stun==false) {

			player2.GetComponentInChildren<EnemyRagdoll> ().takeTurnProper (); //this starts the fire spell

		//	print ("counter atk working");

		}

		if (createdByPlayer == 1 && player2.GetComponentInChildren<ControlEnemy>()!=null && player2.GetComponentInChildren<ControlEnemy>().playerDead==false
		    && player2.GetComponentInChildren<ControlEnemy>().player2Stun==true) {
			//dont return attack

			Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();

			Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);

			Camera.main.GetComponent<CampaignGameOrganizer>().WhosTurn(true,"null"); //this shows whos  turn it is


		//	print ("removing the stun");
			
		}

		/*
		if (createdByPlayer == 2 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		//    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false) {
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
				
		}

		//not in Unity
		if (createdByPlayer == 1 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		 //   && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false) {
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
			
		}
*/
		//this is for the normal counter attack

		if (createdByPlayer == 2 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true) {
			print ("counter atk working");
			player1.GetComponent<ControlPlayer1Character> ().castFireSpell (); //this starts the fire spell

		}


		//this is to differentiate if its a player enemy caster or just a monster enemy
		if (createdByPlayer == 2 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==false && player2.GetComponentInChildren<ControlPlayer2Character>()!=null) {

			Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();

			Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);
			
			Camera.main.GetComponent<CampaignGameOrganizer>().WhosTurn(true,"null"); //this shows whos  turn it is
			
		}


/*



		//end the normal counter attack

			//if both the players are invis
				//if in Unity
	/*
			if (createdByPlayer == 2 
			    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		    	&& player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    	&& Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true) {

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);

			}
				

			if (createdByPlayer == 1 
			    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
			    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    	&& Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true) {
				
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
				
			}			
*/
			//end if both the players are invis
	
		if (createdByPlayer == 2 //this burns p2 after spell casted
		    && player2.GetComponentInChildren<ControlEnemy>().EnemyFlameDOT==true
		    ) {
			print ("enemyisbeingburned");
			player2.GetComponentInChildren<ControlEnemy> ().CreateFlameDOT(); //this creates the flame DOT on the player 
																				
		}
		/*
		if (createdByPlayer == 1 && player1.GetComponent<ControlPlayer1Character>().player1FlameDOT==true) {
			print ("counter atk working");
			player1.GetComponent<ControlPlayer1Character> ().CreateFlameDOT (); //this creates the flame DOT on the player 
			
		}
*/
		if (createdByPlayer == 2 //this closes shield after spell is complete
		    && player1.GetComponent<ControlPlayer1Character>().isShielded==true
		    ) {
			print ("shield closer working");
			player1.GetComponentInChildren<SphereShieldScript> ().closeShield=true; //this creates the flame DOT on the player 
			
		}


	/*
		if (createdByPlayer == 1 //this burns p1 after spell casted
		    && player2.GetComponent<ControlPlayer2Character>().isShielded==true
		    ) {
			print ("shield closer working");
			player2.GetComponentInChildren<SphereShieldScript> ().closeShield=true; //this creates the flame DOT on the player 
			
		}
		*/

	}


	public void primaryElement(){

		if (windCount >= 3) {

			if(sentByPlayer1==true){ //this is so it only adds achievements when p1 casts a spell
				
				if(PlayerPrefs.GetInt("WindSpellCount")==0){
					
					PlayerPrefs.SetInt("WindSpellCount",1);
					
				}else{
					
					PlayerPrefs.SetInt("WindSpellCount",PlayerPrefs.GetInt("WindSpellCount")+1);
					
				}

					if (Social.localUser.authenticated) {

					if(PlayerPrefs.GetInt("WindSpellCount")>=4){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQCg", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("WindSpellCount")>=10){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQCw", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("WindSpellCount")>=25){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQDA", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}

					}
			}

			primarySpellType = 0;

		} else

		if (fireCount >= 3) {


			if(sentByPlayer1==true){ //this is so it only adds achievements when p1 casts a spell

			if(PlayerPrefs.GetInt("FireSpellCount")==0){
				
				PlayerPrefs.SetInt("FireSpellCount",1);
				
			}else{
				
				PlayerPrefs.SetInt("FireSpellCount",PlayerPrefs.GetInt("FireSpellCount")+1);
				
			}

				if (Social.localUser.authenticated) {

				if(PlayerPrefs.GetInt("FireSpellCount")>=4){

					Social.ReportProgress("CgkIvsac4ekTEAIQAQ", 100.0f, (bool success) => {
					// handle success or failure
					});

				}

					if(PlayerPrefs.GetInt("FireSpellCount")>=10){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQAg", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}

						if(PlayerPrefs.GetInt("FireSpellCount")>=25){
							
							Social.ReportProgress("CgkIvsac4ekTEAIQAw", 100.0f, (bool success) => {
								// handle success or failure
							});
							
						}

				}
			
			
			}

			primarySpellType = 1;



		} else

		if (waterCount >= 3) {

			if(sentByPlayer1==true){ //this is so it only adds achievements when p1 casts a spell
				
				if(PlayerPrefs.GetInt("WaterSpellCount")==0){
					
					PlayerPrefs.SetInt("WaterSpellCount",1);
					
				}else{
					
					PlayerPrefs.SetInt("WaterSpellCount",PlayerPrefs.GetInt("WaterSpellCount")+1);
					
				}

					if (Social.localUser.authenticated) {
					
					if(PlayerPrefs.GetInt("WaterSpellCount")>=4){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQBA", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("WaterSpellCount")>=10){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQBQ", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("WaterSpellCount")>=25){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQBg", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
				}
		}

			primarySpellType = 2;
		} else

		if (leafCount >= 3) {

			if(sentByPlayer1==true){ //this is so it only adds achievements when p1 casts a spell
				
				if(PlayerPrefs.GetInt("EarthSpellCount")==0){
					
					PlayerPrefs.SetInt("EarthSpellCount",1);
					
				}else{
					
					PlayerPrefs.SetInt("EarthSpellCount",PlayerPrefs.GetInt("EarthSpellCount")+1);
					
				}

				if (Social.localUser.authenticated) {

					if(PlayerPrefs.GetInt("EarthSpellCount")>=4){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQBw", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("EarthSpellCount")>=10){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQCA", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}
					
					if(PlayerPrefs.GetInt("EarthSpellCount")>=25){
						
						Social.ReportProgress("CgkIvsac4ekTEAIQCQ", 100.0f, (bool success) => {
							// handle success or failure
						});
						
					}


				}

			}

			primarySpellType = 3;
		} else {

			primarySpellType = 4; //this is the type for a null rune icon

		}

		resetAllCounts(); //makes sure there are no remaining counts after this spell is casted

	}

	public int getPrimaryElement{

		get { 

			primaryElement();


			return primarySpellType; }

	}

	public void resetAllCounts(){

		windCount = 0;
		waterCount = 0;
		fireCount = 0;
		leafCount = 0;

		sentSpell.Clear ();

		receivedSpell.Clear ();

	}

	public void LevelCompleteAchievement(){

		//this enables the level completed achievement
		if (Social.localUser.authenticated) {
			Social.ReportProgress ("CgkIvsac4ekTEAIQDg", 100.0f, (bool success) => {
				// handle success or failure
			});
		}
	}



	IEnumerator TakeTurnWhileStunned(){
		
		while (true) {

			yield return new WaitForSeconds(1f);

			Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();
			
			Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);

			Camera.main.GetComponent<CampaignGameOrganizer>().WhosTurn(true,"null"); //this shows whos  turn it is

			player2.GetComponentInChildren<ControlEnemy>().disableStun();


			yield break; //stops the coroutine

		}

	}

	// Update is called once per frame
	void Update () {
	
	}

}
