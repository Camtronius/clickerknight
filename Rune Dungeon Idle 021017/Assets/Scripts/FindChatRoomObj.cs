﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindChatRoomObj : MonoBehaviour {

    // Use this for initialization
    public Client clientScript;
    public GameObject chatNotification; //to notify the player when a new chat msg is received and they arent watching their screen
    public IngredientsMaster ingredientsScript;
    public PetGenerator petGenScript; //this is so the chatroom icon can assign the chatroom the list of pets the player has unlocked

	void OnEnable () {
        clientScript = GameObject.Find("ChatRoomCanvas").GetComponent<Client>();
        clientScript.gameObject.transform.GetComponentInChildren<IconsList>(true).petGenerator = petGenScript;

        //clientScript.chatRoomButton = this.gameObject; //because the client script wont go find these GOs on each new scene, it has to be assigned to it.
        clientScript.chatRoomNotification = chatNotification;
        clientScript.ingredientsScript = ingredientsScript;//this is so we know where the tips are to go

        if (PlayerPrefs.GetInt("CurrentLevel") < 5)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            //do nothing because its already active
        }

    }
	
    public void openChatWindow()
    {
        clientScript.openChatRoom(true);
    }


}
