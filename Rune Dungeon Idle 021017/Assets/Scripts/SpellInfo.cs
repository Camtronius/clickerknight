﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class SpellInfo : MonoBehaviour {

	public string spellTitle;
	public Sprite spellSprite;
	public string spellDescription;
	public string spellPlayerPref;

	public GameObject isLocked;
	public GameObject isLockedParticleEffects;
	public Sprite lockIcon;
	public Sprite unlockIcon;

	void Start () {



	}

	public void checkIfLocked(){

		if (PlayerPrefs.GetFloat (spellPlayerPref) == 1) {
			
			unlockSpell();
			
		} else {
			if(isLocked.gameObject!=null){
			isLocked.gameObject.GetComponent<Image>().sprite = lockIcon;
			isLockedParticleEffects.SetActive(true);
			}
		}

	}

	public void unlockSpell(){
		if (isLocked.gameObject != null) {

			this.gameObject.transform.SetSiblingIndex(1); //this is so the unlocked spells appear on top

			isLocked.gameObject.GetComponent<Image> ().sprite = unlockIcon;
			isLocked.gameObject.GetComponent<Image> ().color = Color.red;
			isLockedParticleEffects.SetActive (false);
			this.gameObject.GetComponentInChildren<Animator> ().enabled = false;
		}
	}

}
