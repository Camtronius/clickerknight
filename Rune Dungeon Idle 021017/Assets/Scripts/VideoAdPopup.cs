﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using TMPro;

public class VideoAdPopup : MonoBehaviour {

	public TextMeshProUGUI goldAmount;

	void Start () {

		Advertisement.Initialize ("113304" , true); //the true is test mode or not MAKE SURE YOU SAY FALSE BEFORE YOU MAKE PUBLIC
	}
	
	void OnDisable() {
		// Remove handler
	//	Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
	}

	public void closePopUp(){

		this.gameObject.SetActive (false);
	}

	public void watchVideoAdGems(){

		ShowOptions options = new ShowOptions ();
		options.resultCallback = AdCallbackhandler;
		
		if (Advertisement.IsReady("rewardedVideoZone"))
		{
			Advertisement.Show("rewardedVideoZone", options);
		}
	


		/*
			if (Chartboost.hasRewardedVideo(CBLocation.MainMenu)) { 
				Chartboost.showRewardedVideo(CBLocation.MainMenu);
			}
			else {
				// We don't have a cached video right now, but try to get one for next time
				Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
			}

		Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
	}

	void didCompleteRewardedVideo(CBLocation location, int reward){

		int gold = PlayerPrefs.GetInt ("Gold") + reward;

		PlayerPrefs.SetInt("Gold", gold);

		goldAmount.text = gold.ToString();

		this.gameObject.SetActive (false);

	}
	
*/
	}

	void AdCallbackhandler (ShowResult result)
	{
		switch(result)
		{
		case ShowResult.Finished:
			Debug.Log ("Ad Finished. Rewarding player...");
			break;
		case ShowResult.Skipped:
			Debug.Log ("Ad skipped. Son, I am dissapointed in you");
			break;
		case ShowResult.Failed:
			Debug.Log("I swear this has never happened to me before");
			break;
		}
	}

		public void CompletedAdEquipTest(){

		int reward = 1000;

		int gold = PlayerPrefs.GetInt ("Gold") + reward;
		
		PlayerPrefs.SetInt("Gold", gold);
		
		goldAmount.text = gold.ToString();
		
		this.gameObject.SetActive (false);

	}

}
