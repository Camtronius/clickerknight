﻿using UnityEngine;
using System.Collections;

public class SolarIncrementer : MonoBehaviour {

	public bool closeBeam = false;
	public GameObject Explosion;
	public GameObject HitGroundParticles;
	public int createdByPlayer = 0;

	public float loopTime = 0;
	public float beamPositionX = 0;
	public float beamPositionX2 = 0;

	public GameObject TopStartingPosition;
	public GameObject player1RightHandPosition;
	public GameObject player1TorsoPosition;
	public GameObject player1Head;

	public GameObject player2LegPosition;
	public GameObject player2RightHandPosition;
	public GameObject player2TorsoPosition;
	public GameObject player2Head;
//	public GameObject ExplosionForce;

	public GameObject waterColliderObj;

	public float yPosition;

	public float BeamSpeed = 4;
	public float BeamWidth = 3;
	public float scrollSpeed;

	public LineRenderer WaterBeamLine;
	public Renderer beamObject;

	public GameObject SpellCreator; //this is for the isSpellComplete condition

	// Use this for initialization
	void Start () {
		SpellCreator = GameObject.Find ("SpellListener");

//		print (Camera.main.ScreenToWorldPoint(new Vector3(0,0,0)).x + " is the world pt." );

		beamObject = this.gameObject.GetComponent<Renderer> ();

		if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){

		//	Camera.main.GetComponent<CampaignGameOrganizer>().PersistantSoundmanager.WaterBeamShot();

		}

		if (createdByPlayer == 1) {

			TopStartingPosition = GameObject.Find ("Enemy/ShotSpawnTop");

		//	waterColliderObj.transform.position = player1RightHandPosition.transform.position;

			WaterBeamLine.SetWidth (3,3);

			yPosition = 0;//TopStartingPosition.transform.position.y; //3 is the width of the beam in worldspace

			beamPositionX2 = TopStartingPosition.transform.position.x;

			WaterBeamLine.SetPosition (0, new Vector3 (0, 0, 5)); //already set to the right hand in control player 1 script
			WaterBeamLine.SetPosition (1, new Vector3 (0, 0, 5));


		}

		if (createdByPlayer == 2) {

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){

			player2RightHandPosition = GameObject.Find ("Player2Character/Player2/UpperRightArm/LowerRightArm/RightHand");
		
			player2LegPosition = GameObject.Find ("Player2Character/Player2/RightLegLower/RightFoot");
		
			player2TorsoPosition = GameObject.Find ("Player2Character/Player2/Torso");
		
			player2Head = GameObject.Find ("Player2Character/Player2/Head");

			waterColliderObj.transform.position = player2RightHandPosition.transform.position;

				WaterBeamLine.SetWidth (BeamWidth,BeamWidth);

			yPosition = player2Head.transform.position.y; //3 is the width of the beam in worldspace

			beamPositionX2 = player2RightHandPosition.transform.position.x;

			beamPositionX = player2RightHandPosition.transform.position.x;

			WaterBeamLine.SetPosition (1, new Vector3 (player2RightHandPosition.transform.position.x, 
			                                           yPosition, 5)); //already set to the right hand in control player 1 script
			WaterBeamLine.SetPosition (0, new Vector3 (player2RightHandPosition.transform.position.x, yPosition, 5));
			
			}else{ //if its an enemy in the campaign mode


				if(GameObject.Find ("Enemy/EvilGuy")!=null){
				player2RightHandPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/UpperRightArm/LowerRightArm/RightHand");
				
				player2LegPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/RightLegLower/RightFoot");
				
				player2TorsoPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/Torso");
				
				player2Head = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/Head");

				}

				if(GameObject.Find ("Enemy/FinalBoss")!=null){
					player2RightHandPosition = GameObject.Find ("Enemy/FinalBoss/ShotSpawn");//("Enemy/FinalBoss/FinalBoss/UpR_Arm/LowR_Arm/R_Hand");
					
					player2LegPosition = GameObject.Find ("Enemy/FinalBoss/ShotSpawn");
					
					player2TorsoPosition = GameObject.Find ("Enemy/FinalBoss/FinalBoss/Torso");
					
					player2Head = GameObject.Find ("Enemy/FinalBoss/FinalBoss/UpR_Arm/LowR_Arm/R_Hand");
					
				}
				
				waterColliderObj.transform.position = player2RightHandPosition.transform.position;
				
				float beamWidth = 3;
				
				WaterBeamLine.SetWidth (beamWidth,beamWidth);
				
				yPosition = player2Head.transform.position.y; //3 is the width of the beam in worldspace
				
				beamPositionX2 = player2RightHandPosition.transform.position.x;
				
				beamPositionX = player2RightHandPosition.transform.position.x;
				
				WaterBeamLine.SetPosition (1, new Vector3 (player2RightHandPosition.transform.position.x, 
				                                           yPosition, 5)); //already set to the right hand in control player 1 script
				WaterBeamLine.SetPosition (0, new Vector3 (player2RightHandPosition.transform.position.x, yPosition, 5));

			}

		}

	}

	void Update () {
	

		if (closeBeam == false) {

			if (yPosition > -4.5f && createdByPlayer==1) {
				yPosition -= Time.deltaTime * BeamSpeed;
				WaterBeamLine.SetPosition (1, new Vector3 (0, yPosition, 5));
		
			}

			if (BeamWidth<3) {
								
				BeamWidth+=Time.deltaTime*3f;
				
				WaterBeamLine.SetWidth (BeamWidth,BeamWidth);	
			}

			if (beamPositionX > Camera.main.ScreenToWorldPoint(new Vector3(0,0,0)).x && createdByPlayer==2) {
				beamPositionX -= Time.deltaTime * BeamSpeed;
				WaterBeamLine.SetPosition (0, new Vector3 (beamPositionX, yPosition, 5));
				
			}
		
		}
	
		if (closeBeam == true && createdByPlayer==1) {

			BeamWidth-=Time.deltaTime*3f;

			WaterBeamLine.SetWidth (BeamWidth,BeamWidth);

			if (BeamWidth <=0) {
				
				if(SpellCreator.GetComponent<SpellCreator>()!=null){
					
					SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
					
					SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
					
					
					
					Destroy(this.gameObject);
					
				}
				
				if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
					
					SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
					
					SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
					
					
					
					Destroy(this.gameObject);
					
				}
				
				
			}

		//	WaterBeamLine.SetPosition (0, new Vector3 (beamPositionX2, yPosition, 0));
		}

		if (closeBeam == true && createdByPlayer==2) {
			
			beamPositionX2 -= Time.deltaTime * BeamSpeed;
			
			BeamWidth-=Time.deltaTime*BeamSpeed;
			
			WaterBeamLine.SetWidth (BeamWidth,BeamWidth);	
		}

	

		loopTime += Time.deltaTime;
		
		float offset = loopTime * scrollSpeed;

		beamObject.material.SetTextureOffset ("_MainTex", new Vector2 (offset, 0));
		
		if (offset >= 0.65f) {
			
			//beamObject.material.SetTextureOffset("_MainTex", new Vector2(0.2f, 0));
			loopTime = 0f;
			offset = 0f;
		}



		if (beamPositionX2 <= Camera.main.ScreenToWorldPoint(new Vector3(0,0,0)).x && createdByPlayer==2) {

			//dont have to change this one for the enemy until there is an enemy that uses this spell..
			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			
			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;

			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
			
			}else{

				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);

			}

		

			Destroy(this.gameObject);

		}
	}

	public void createGroundParticles(Vector3 enemyLocation){

		GameObject GroundParticlesClone = (GameObject)Instantiate (HitGroundParticles, new Vector3 (enemyLocation.x, enemyLocation.y, 0), Quaternion.identity);


	}

	public void createExplosion(Vector3 enemyLocation){


		GameObject ExplosionClone = (GameObject)Instantiate (Explosion, new Vector3 (enemyLocation.x, enemyLocation.y, 0), Quaternion.identity);

		waterColliderObj.transform.position = enemyLocation; //this appears below the explosion for some reason

		waterColliderObj.gameObject.GetComponent<BoxCollider2D> ().enabled = true;

	}

}