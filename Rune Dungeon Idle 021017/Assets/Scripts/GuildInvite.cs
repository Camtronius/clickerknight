﻿using UnityEngine;
using UnityEngine.UI;

public class GuildInvite : MonoBehaviour {

    // Use this for initialization
    public Button AcceptButton;
    public Button DeclineButton;
    private string sender;
    private int rank;
    //you are the receiver of course

    public Text inviteText;

    public GuildHandler guildHandlerScript;

    //who sent the invite. So we can search for it and reject/accept it or not. 
    public void setSenderRank(string _sender, int _rank)
    {
        sender = _sender;
        rank = _rank;
    }

    public void AcceptInvite()
    {
        guildHandlerScript.AcceptGuildInv(sender, rank, this.gameObject);
    }

    public void RejectInvite()
    {
        guildHandlerScript.RejectGuildInv(sender,this.gameObject);
    }


}
