﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AutoSell : MonoBehaviour {

	public Inventory inventoryScript;
	public TextMeshProUGUI countdown;

	public int sellTime;
	public MainMenu cameraObj;

	// Use this for initialization
	void OnEnable () {
		sellTime = PlayerPrefs.GetInt ("AutoSellTime");

        countdown.text = "";

        StartCoroutine("SellCountdown");
	}

	void OnDisable(){
		sellTime = PlayerPrefs.GetInt ("AutoSellTime");
		countdown.text = "";

	}

	public void playAppearWoosh(){

		cameraObj.persistantSoundManager.GetComponent<SoundManager> ().showInventoryItem ();



	}

	public void checkTutorial(){

		//this checks if the player has completed the tutorial yet for finding an item
		if (PlayerPrefs.GetInt ("FirstItemFound") == 0) {

			cameraObj.TutorialObject.wizardSprite.sprite = cameraObj.TutorialObject.wizardSprites[3]; //surprised wiz sprite

			cameraObj.TutorialObject.equipTap = true;

			cameraObj.TutorialObject.twoPartTutorial = true;
			cameraObj.TutorialObject.explainGoldExp = false;

			cameraObj.persistantSoundManager.GetComponent<SoundManager> ().oldManSurprised ();

			cameraObj.TutorialObject.TutPartOne ("Awesome! you found a new piece of gear");
			cameraObj.TutorialObject.secondThingToSay = "Lets <color=#03c0ff>EQUIP</color> the gear to strengthen our character!";

			PlayerPrefs.SetInt ("FirstItemFound",1); //so it doesnt explain this twice

		}


	}
	
	IEnumerator SellCountdown(){ //***make sure this is only called once per game and doesnt repeatedly get called

		while (true) {

		

			yield return new WaitForSeconds(1f);
			sellTime -= 1;
		//	print ("sell coroutine running!!asdja;ldk");
			countdown.text = "Automatic Sell in " + sellTime.ToString() + " seconds";

			if (sellTime <= 0) {

				inventoryScript.sellItem ();
				sellTime = PlayerPrefs.GetInt ("AutoSellTime");

			}

		}
	}


}
