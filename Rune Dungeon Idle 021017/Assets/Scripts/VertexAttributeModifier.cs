﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class VertexAttributeModifier : MonoBehaviour {

    public enum AnimationMode { Wave, VertexColor, Jitter, Warp, Dangling, Reveal };
    public AnimationMode MeshAnimationMode = AnimationMode.Wave;
    public AnimationCurve VertexCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.25f, 2.0f), new Keyframe(0.5f, 0), new Keyframe(0.75f, 2.0f), new Keyframe(1, 0f));
    public float AngleMultiplier = 1.0f;
    public float SpeedMultiplier = 1.0f;
	public float CurveScale = 1.0f;   

	public float clockTime = 1.0f;

    private TMP_Text m_TextComponent;
    //private TextContainer m_TextContainer;
    //private TMP_TextInfo m_textInfo;

    private string textLabel = "Text <#ff8000>silliness</color> with TextMesh<#00aaff>Pro!</color>";


    private struct VertexAnim
    {
        public float angleRange;
        public float angle;
        public float speed;
    }

    void Awake()
    {
        m_TextComponent = gameObject.GetComponent<TMP_Text>();
    }


    void Start()
    {

        switch (MeshAnimationMode)
        {
            case AnimationMode.VertexColor:
                StartCoroutine(AnimateVertexColors());
                break;
            case AnimationMode.Wave:
           //     StartCoroutine(AnimateVertexPositions());
                break;
            case AnimationMode.Jitter:
                //StartCoroutine(AnimateVertexPositionsJitter());
                break;
            case AnimationMode.Warp:
                StartCoroutine(WarpText());
                break;
            case AnimationMode.Dangling:
                //StartCoroutine(AnimateVertexPositionsIV(m_TextComponent));
                break;
            case AnimationMode.Reveal:
                //StartCoroutine(AnimateVertexPositionsV(m_TextComponent));
                break;
        }
    
	
	}

	public void startWave(){

		StartCoroutine(AnimateVertexPositions());

	}



    /// <summary>
    /// Method to animate vertex colors of a TMP Text object.
    /// </summary>
    /// <returns></returns>
    IEnumerator AnimateVertexColors()
    {
        TMP_TextInfo textInfo = m_TextComponent.textInfo;
        int currentCharacter = 0;

        Color32[] newVertexColors;
        Color32 c0 = m_TextComponent.color;

        while (true)
        {
            int characterCount = textInfo.characterCount;

            // If No Characters then just yield and wait for some text to be added
            if (characterCount == 0)
            {
                yield return new WaitForSeconds(0.25f);
                continue;
            }

            // Get the index of the material used by the current character.
            int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

            // Get the vertex colors of the mesh used by this text element (character or sprite).
            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            // Get the index of the first vertex used by this text element.
            int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

            // Only change the vertex color if the text element is visible.
            if (textInfo.characterInfo[currentCharacter].isVisible)
            {
                c0 = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);

                newVertexColors[vertexIndex + 0] = c0;
                newVertexColors[vertexIndex + 1] = c0;
                newVertexColors[vertexIndex + 2] = c0;
                newVertexColors[vertexIndex + 3] = c0;

                // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
                m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                // This last process could be done to only update the vertex data that has changed as opposed to all of the vertex data but it would require extra steps and knowing what type of renderer is used.
                // These extra steps would be a performance optimization but it is unlikely that such optimization will be necessary.
            }

            currentCharacter = (currentCharacter + 1) % characterCount;

            yield return new WaitForSeconds(0.05f);
        }
    }


    /// <summary>
    /// Method to animate the position of characters using a Unity animation curve.
    /// </summary>
    /// <returns></returns>
    IEnumerator AnimateVertexPositions()
    {
        VertexCurve.preWrapMode = WrapMode.Loop;
        VertexCurve.postWrapMode = WrapMode.Loop;

        // Force an update of the text object so we can begin modifying the vertex data right away.
        // Since this function was called from Start, the text object hasn't been generated yet. This normally happens at the end of the frame when properties have changed.
        // So in this example, we force this update right away instead of waiting one frame. 
        m_TextComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = m_TextComponent.textInfo;
        int materialCount = textInfo.materialCount;

        // We use a temporary vertex buffer as to not modify the original vertex data.
        Vector3[][] temp_VertexBuffers = new Vector3[materialCount][];

        int loopCount = 0;

        while (true)
        {
            materialCount = textInfo.materialCount;


            if (materialCount != temp_VertexBuffers.Length)
                temp_VertexBuffers = new Vector3[materialCount][];

            for (int i = 0; i < materialCount; i++)
                temp_VertexBuffers[i] = textInfo.meshInfo[i].mesh.vertices;

            int characterCount = textInfo.characterCount;

            for (int i = 0; i < characterCount; i++)
            {
                if (!textInfo.characterInfo[i].isVisible)
                    continue;

                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                float offsetY = VertexCurve.Evaluate((float)i / characterCount + loopCount / 50f) * CurveScale;

                Vector3[] sourceVertexPositions = textInfo.meshInfo[materialIndex].vertices;

                temp_VertexBuffers[materialIndex][vertexIndex + 0] = new Vector3 (sourceVertexPositions[vertexIndex + 0].x, sourceVertexPositions[vertexIndex + 0].y + offsetY, 0);
                temp_VertexBuffers[materialIndex][vertexIndex + 1] = new Vector3 (sourceVertexPositions[vertexIndex + 1].x, sourceVertexPositions[vertexIndex + 1].y + offsetY, 0);
                temp_VertexBuffers[materialIndex][vertexIndex + 2] = new Vector3 (sourceVertexPositions[vertexIndex + 2].x, sourceVertexPositions[vertexIndex + 2].y + offsetY, 0);
                temp_VertexBuffers[materialIndex][vertexIndex + 3] = new Vector3 (sourceVertexPositions[vertexIndex + 3].x, sourceVertexPositions[vertexIndex + 3].y + offsetY, 0);
            }

            loopCount += 1;

            // Upload the mesh with the revised information
            //m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Vertices);

            // Push the vertex data changes onto the main and sub meshes used by the text object.
            for (int i = 0; i < textInfo.materialCount; i++)
            {
                Mesh mesh = textInfo.meshInfo[i].mesh;

                mesh.vertices = temp_VertexBuffers[i];
                m_TextComponent.UpdateGeometry(mesh, i);
            }

			//clockTime -= Time.deltaTime;
			
			//fade in
			


            yield return new WaitForSeconds(0.025f);

			//if(m_TextComponent.alpha<1){
				
		//		m_TextComponent.alpha+=0.01f;
				
		//	}
        }
    }


    /// <summary>
    /// Method to animate the position of characters to add a random jitter.
    /// </summary>
    /// <param name="textComponent"></param>
    /// <returns></returns>
    IEnumerator AnimateVertexPositionsJitter(TextMeshPro textComponent)
    {

        Matrix4x4 matrix;
        Vector3[] vertices;

        int loopCount = 0;

        // Create an Array which contains pre-computed Angle Ranges and Speeds for a bunch of characters.
        VertexAnim[] vertexAnim = new VertexAnim[1024];
        for (int i = 0; i < 1024; i++)
        {
            vertexAnim[i].angleRange = Random.Range(10f, 25f);
            vertexAnim[i].speed = Random.Range(1f, 3f);
        }

        textComponent.renderMode = TextRenderFlags.DontRender;

        while (loopCount < 10000)
        {
            textComponent.havePropertiesChanged = true;
            textComponent.ForceMeshUpdate();
            vertices = textComponent.textInfo.meshInfo[0].vertices;

            int characterCount = textComponent.textInfo.characterCount;

            for (int i = 0; i < characterCount; i++)
            {
                // Setup initial random values
                VertexAnim vertAnim = vertexAnim[i];
                TMP_CharacterInfo charInfo = textComponent.textInfo.characterInfo[i];

                // Skip Characters that are not visible
                if (!charInfo.isVisible)
                    continue;

                int vertexIndex = charInfo.vertexIndex;

                //Vector2 charMidTopline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.topRight.y);
                Vector2 charMidBasline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.baseLine);

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                //Vector3 offset = charMidTopline;
                Vector3 offset = charMidBasline;

                vertices[vertexIndex + 0] += -offset;
                vertices[vertexIndex + 1] += -offset;
                vertices[vertexIndex + 2] += -offset;
                vertices[vertexIndex + 3] += -offset;

                vertAnim.angle = Mathf.SmoothStep(-vertAnim.angleRange, vertAnim.angleRange, Mathf.PingPong(loopCount / 25f * vertAnim.speed, 1f));
                Vector3 jitterOffset = new Vector3(Random.Range(-.25f, .25f), Random.Range(-.25f, .25f), 0);

                //matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, vertexAnim[i].angle), Vector3.one);
                //matrix = Matrix4x4.TRS(jitterOffset, Quaternion.identity, Vector3.one);
                matrix = Matrix4x4.TRS(jitterOffset * CurveScale, Quaternion.Euler(0, 0, Random.Range(-5f, 5f) * AngleMultiplier), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);


                vertices[vertexIndex + 0] += offset;
                vertices[vertexIndex + 1] += offset;
                vertices[vertexIndex + 2] += offset;
                vertices[vertexIndex + 3] += offset;

                vertexAnim[i] = vertAnim;
            }

            loopCount += 1;

            textComponent.mesh.vertices = vertices;
            textComponent.mesh.uv = textComponent.textInfo.meshInfo[0].uvs0;
            textComponent.mesh.uv2 = textComponent.textInfo.meshInfo[0].uvs2;
            //m_TextMeshPro.mesh.colors32 = m_TextMeshPro.textInfo.meshInfo[0].vertexColors;

            yield return new WaitForSeconds(0.1f * SpeedMultiplier);
        }
    }


    /// <summary>
    /// Method to animate the position of characters to add a random jitter.
    /// </summary>
    /// <param name="textComponent"></param>
    /// <returns></returns>
    IEnumerator AnimateVertexPositionsJitter(TextMeshProUGUI textComponent)
    {
        CanvasRenderer uiRenderer = textComponent.canvasRenderer;
        Mesh mesh = textComponent.textInfo.meshInfo[0].mesh;
        Matrix4x4 matrix;
        Vector3[] vertices;

        int loopCount = 0;

        // Create an Array which contains precomputed Angle Ranges and Speeds for a bunch of characters.
        VertexAnim[] vertexAnim = new VertexAnim[1024];
        for (int i = 0; i < 1024; i++)
        {
            vertexAnim[i].angleRange = Random.Range(10f, 25f);
            vertexAnim[i].speed = Random.Range(1f, 3f);
        }

        while (loopCount < 10000)
        {
            // Force an update of the geometry to reset the positions otherwise we would be animating and already animated position.
            textComponent.renderMode = TextRenderFlags.Render;
            textComponent.ForceMeshUpdate();
            vertices = textComponent.textInfo.meshInfo[0].vertices;

            int characterCount = textComponent.textInfo.characterCount;

            for (int i = 0; i < characterCount; i++)
            {
                // Setup initial random values
                VertexAnim vertAnim = vertexAnim[i];
                TMP_CharacterInfo charInfo = textComponent.textInfo.characterInfo[i];

                // Skip Characters that are not visible
                if (!charInfo.isVisible)
                    continue;

                int vertexIndex = charInfo.vertexIndex;

                //Vector2 charMidTopline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.topRight.y);
                Vector2 charMidBasline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.baseLine);

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                //Vector3 offset = charMidTopline;
                Vector3 offset = charMidBasline;

                vertices[vertexIndex + 0] += -offset;
                vertices[vertexIndex + 1] += -offset;
                vertices[vertexIndex + 2] += -offset;
                vertices[vertexIndex + 3] += -offset;

                vertAnim.angle = Mathf.SmoothStep(-vertAnim.angleRange, vertAnim.angleRange, Mathf.PingPong(loopCount / 25f * vertAnim.speed, 1f));
                Vector3 jitterOffset = new Vector3(Random.Range(-.25f, .25f), Random.Range(-.25f, .25f), 0);

                matrix = Matrix4x4.TRS(jitterOffset * CurveScale, Quaternion.Euler(0, 0, Random.Range(-5f, 5f) * AngleMultiplier), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);


                vertices[vertexIndex + 0] += offset;
                vertices[vertexIndex + 1] += offset;
                vertices[vertexIndex + 2] += offset;
                vertices[vertexIndex + 3] += offset;

                vertexAnim[i] = vertAnim;
            }

            loopCount += 1;

            // Upload the mesh with the revised information
            mesh.vertices = vertices;
            mesh.uv2 = textComponent.textInfo.meshInfo[0].uvs2; // This should not be necessary so I'll investigate when I have time.
            uiRenderer.SetMesh(mesh);

            yield return new WaitForSeconds(0.1f * SpeedMultiplier);
        }
    }



    private AnimationCurve CopyAnimationCurve(AnimationCurve curve)
    {
        AnimationCurve newCurve = new AnimationCurve();

        newCurve.keys = curve.keys;

        return newCurve;
    }

    IEnumerator WarpText()
    {
        VertexCurve.preWrapMode = WrapMode.Clamp;
        VertexCurve.postWrapMode = WrapMode.Clamp;

        Mesh mesh = m_TextComponent.textInfo.meshInfo[0].mesh;

        Vector3[] vertices;
        Matrix4x4 matrix;

        m_TextComponent.havePropertiesChanged = true; // Need to force the TextMeshPro Object to be updated.
        CurveScale *= 10;
        float old_CurveScale = CurveScale;
        AnimationCurve old_curve = CopyAnimationCurve(VertexCurve);

        while (true)
        {
            if (!m_TextComponent.havePropertiesChanged && old_CurveScale == CurveScale && old_curve.keys[1].value == VertexCurve.keys[1].value)
            {
                yield return null;
                continue;
            }

            old_CurveScale = CurveScale;
            old_curve = CopyAnimationCurve(VertexCurve);

            m_TextComponent.ForceMeshUpdate(); // Generate the mesh and populate the textInfo with data we can use and manipulate.

            TMP_TextInfo textInfo = m_TextComponent.textInfo;
            int characterCount = textInfo.characterCount;


            if (characterCount == 0) continue;

            vertices = textInfo.meshInfo[0].vertices;
            //int lastVertexIndex = textInfo.characterInfo[characterCount - 1].vertexIndex;

            float boundsMinX = mesh.bounds.min.x;
            float boundsMaxX = mesh.bounds.max.x;


            for (int i = 0; i < characterCount; i++)
            {
                if (!textInfo.characterInfo[i].isVisible)
                    continue;

                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Compute the baseline mid point for each character
                Vector3 offsetToMidBaseline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, textInfo.characterInfo[i].baseLine);
                //float offsetY = VertexCurve.Evaluate((float)i / characterCount + loopCount / 50f); // Random.Range(-0.25f, 0.25f);

                // Apply offset to adjust our pivot point.
                vertices[vertexIndex + 0] += -offsetToMidBaseline;
                vertices[vertexIndex + 1] += -offsetToMidBaseline;
                vertices[vertexIndex + 2] += -offsetToMidBaseline;
                vertices[vertexIndex + 3] += -offsetToMidBaseline;

                // Compute the angle of rotation for each character based on the animation curve
                float x0 = (offsetToMidBaseline.x - boundsMinX) / (boundsMaxX - boundsMinX); // Character's position relative to the bounds of the mesh.
                float x1 = x0 + 0.0001f;
                float y0 = VertexCurve.Evaluate(x0) * CurveScale;
                float y1 = VertexCurve.Evaluate(x1) * CurveScale;

                Vector3 horizontal = new Vector3(1, 0, 0);
                //Vector3 normal = new Vector3(-(y1 - y0), (x1 * (boundsMaxX - boundsMinX) + boundsMinX) - offsetToMidBaseline.x, 0);
                Vector3 tangent = new Vector3(x1 * (boundsMaxX - boundsMinX) + boundsMinX, y1) - new Vector3(offsetToMidBaseline.x, y0);

                float dot = Mathf.Acos(Vector3.Dot(horizontal, tangent.normalized)) * 57.2957795f;
                Vector3 cross = Vector3.Cross(horizontal, tangent);
                float angle = cross.z > 0 ? dot : 360 - dot;

                matrix = Matrix4x4.TRS(new Vector3(0, y0, 0), Quaternion.Euler(0, 0, angle), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);

                vertices[vertexIndex + 0] += offsetToMidBaseline;
                vertices[vertexIndex + 1] += offsetToMidBaseline;
                vertices[vertexIndex + 2] += offsetToMidBaseline;
                vertices[vertexIndex + 3] += offsetToMidBaseline;
            }


            // Upload the mesh with the revised information
            mesh.vertices = textInfo.meshInfo[0].vertices;
            m_TextComponent.canvasRenderer.SetMesh(mesh);

            yield return new WaitForSeconds(0.025f);
        }
    }

}
