﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PrestigePoints : MonoBehaviour {
	// Use this for initialization
	public float CoolDownTimeMax;
	public float prestCountAmt  =.125f;
	public TextMeshProUGUI amountOfPrestigePoints;

	public Slider CoolDownGfx;


	void OnEnable () {

		//need to call the player pregs to set up the wheel at start

		CoolDownGfx.value = PlayerPrefs.GetFloat ("PrestigeCounter") / CoolDownTimeMax; //puts the wheel in the correct position

		amountOfPrestigePoints.text = PlayerPrefs.GetInt ("PrestigePtsEarned").ToString (); //puts in the correct amt of prestige pts
	}


	public void updatePrestigeCounter(){

		float prestPts = PlayerPrefs.GetFloat ("PrestigeCounter") + prestCountAmt;

		PlayerPrefs.SetFloat ("PrestigeCounter", prestPts); //increases the amt of prest points available


		if (prestPts >= 1) {

			PlayerPrefs.SetInt ("PrestigePtsEarned", PlayerPrefs.GetInt ("PrestigePtsEarned") + 1); //sets the total prestige points the player has to +1

			PlayerPrefs.SetFloat ("PrestigeCounter", 0);

			prestPts = 0;

			amountOfPrestigePoints.text = PlayerPrefs.GetInt ("PrestigePtsEarned").ToString (); //puts in the correct amt of prestige pts

		}

		CoolDownGfx.value = prestPts / CoolDownTimeMax;

	}

}
