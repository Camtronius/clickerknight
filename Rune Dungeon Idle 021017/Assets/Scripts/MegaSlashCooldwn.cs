﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class MegaSlashCooldwn : MonoBehaviour {

	// Use this for initialization
	public float CoolDownTimeMax;
	public float CoolDownTime;
	public Image CoolDownGfx;
	public bool isReady=false;
	public TextMeshProUGUI timeCountDown;
	public string timeString;
    public MainMenu cameraMainMenu;



    void OnEnable(){

        cameraMainMenu = Camera.main.GetComponent<MainMenu>();


        if (PlayerPrefs.GetInt ("MegaSlashTime") > 0 && PlayerPrefs.GetInt ("MegaSlashRdy") == 0) {
			CoolDownTime = PlayerPrefs.GetInt ("MegaSlashTime");
			timeCountDown.text = CoolDownTime.ToString (); 

		} else if (PlayerPrefs.GetInt ("MegaSlashRdy") == 1) {

			isReady = true; //they saved the megaslash
			timeCountDown.text = "";
			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

		} else {
			CoolDownTime =CoolDownTimeMax;
			timeCountDown.text = CoolDownTime.ToString (); 

		}

		CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

	}

	public void decreaseMegaSlashCooldown(){

		if (CoolDownTime > 0) {

			if (Mathf.CeilToInt(CoolDownTime) % 5 == 0 && CoolDownTime!=0) {
				PlayerPrefs.SetInt ("MegaSlashTime", Mathf.CeilToInt(CoolDownTime));
				PlayerPrefs.SetInt ("MegaSlashRdy", 0);


			}

			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);
			CoolDownTime -= 1* (1 + cameraMainMenu.petManager.abilityArray[1]);
			isReady = false;
			timeString = CoolDownTime.ToString ();
            if (timeString.Length > 3)
            {
                timeString = timeString.Substring(0, 3); //10B
            }
            timeCountDown.text = timeString;



		} else {
			isReady = true;
			PlayerPrefs.SetInt ("MegaSlashRdy", 1);

			timeCountDown.text = "";

		}

	}

	public void MegaSlashButtonPress(){
		if (isReady == true && Camera.main.GetComponent<MainMenu>().windowOpen==false) {
			CoolDownTime = CoolDownTimeMax;


			Camera.main.GetComponent<MainMenu> ().MegaSlashButtonPress ();
			decreaseMegaSlashCooldown ();

			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().MegaSlashActivate ();

			this.gameObject.GetComponentInParent<ActiveAbilityManager> ().unlockWeaponAnim (this.gameObject.transform, this.gameObject.GetComponentInParent<ActiveAbilityManager> ().MegaSlashImg.sprite);//this is to play the animation, the name of the method is wrong but i was too lazt to change


		}
	}



}
