﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class SoundManager : MonoBehaviour {

	public List<AudioClip> audioList = new List<AudioClip>();

	public List<AudioClip> musicList = new List<AudioClip>();

	private static SoundManager instance = null;


	public static SoundManager Instance {
		get { return instance; }
	}

	public AudioSource soundEffect;

	public AudioSource musicPlaying;

	public bool SoundSwitch = true;

	public GameObject toggleItem;

	void Awake() {
	//	print ("Enemy Stats Inheriter is awake!");

		SoundCheck ();


		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);


	}

	// Use this for initialization
	void Start () {
	
		MainMenuMusic ();

	}

	public void SoundCheck(){

		if (PlayerPrefs.GetInt ("SoundOnOff") == 0) {

			soundEffect.enabled = true;
		//	print ("audio now ON!");

		} else if (PlayerPrefs.GetInt ("SoundOnOff") == 1){

			soundEffect.enabled = false;
		//	print ("audio now OFF!");

		}

		if (PlayerPrefs.GetInt ("MusicOnOff") == 0) {

			musicPlaying.enabled = true;
			//	print ("audio now ON!");

		} else if (PlayerPrefs.GetInt ("MusicOnOff") == 1){

			musicPlaying.enabled = false;
			//	print ("audio now OFF!");

		}

	}

	public void TurnOffSound(){

		if(PlayerPrefs.GetInt ("SoundOnOff") == 0){
			PlayerPrefs.SetInt ("SoundOnOff",1);
			soundEffect.enabled = false;
	//		AudioListener.volume = 0f;
	//		print ("audio now OFF!");


			
		} else if(PlayerPrefs.GetInt ("SoundOnOff") == 1){
			PlayerPrefs.SetInt ("SoundOnOff",0);
			soundEffect.enabled = true;

	//		AudioListener.volume = 1.0f;
	//		print ("audio now ON!");


		}


	

	}

	public void TurnOffMusic(){

		if (PlayerPrefs.GetInt ("MusicOnOff") == 0) {
			PlayerPrefs.SetInt ("MusicOnOff",1);
			musicPlaying.enabled = false;

		} else if (PlayerPrefs.GetInt ("MusicOnOff") == 1){
			PlayerPrefs.SetInt ("MusicOnOff",0);
			musicPlaying.enabled = true;

		}
			
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void restorePitch(){
	//	soundEffect.pitch = 1f;
	}

	public void SwordSwish(){

		int randInt = Random.Range (0, 3);

		switch (randInt) {
		case 0:
			soundEffect.PlayOneShot (audioList [0], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [1], 1f);

			break;
		case 2:
			soundEffect.PlayOneShot (audioList [2], 1f);

			break;
		}

		restorePitch ();


	}

	public void swordHitEnemy(){

		int RandNum = Random.Range (0, 3);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [3], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [4], 1f);

			break;
		case 2:
			soundEffect.PlayOneShot (audioList [5], 1f);
			break;

		}

		restorePitch ();

	}



	public void FireballSound(){
	//	soundEffect.pitch = Random.Range (6, 11)/10f;
		soundEffect.PlayOneShot (audioList [8],1);

		restorePitch ();


	}

	public void SmallExplosionSound(){
	//	soundEffect.pitch = Random.Range (6, 11)/10f;
		soundEffect.PlayOneShot(audioList [9],0.7f);

		restorePitch ();



	}

	public void SkeleDeath(){

	//	soundEffect.pitch = Random.Range (8, 11)/10f;
		soundEffect.PlayOneShot (audioList [10],1f);
		soundEffect.pitch = 1f;
		restorePitch ();



	}

	public void goldSound(){
		soundEffect.PlayOneShot (audioList [11], 1f);
		restorePitch ();

	}

	public void BackButton(){
		soundEffect.PlayOneShot (audioList [12], 1f);
		restorePitch ();

	}

	public void openBag(){
		soundEffect.PlayOneShot (audioList [13], 1f);
		restorePitch ();

	}
		
	public void UpgradeWeapon (){
		restorePitch ();

		soundEffect.PlayOneShot (audioList [14], 1f);

	}


	public void UpgradeSpell(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [15], 1f);
	}

	public void LevelUp(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [16], 1f);

	}

	public void TalentSelect(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [17], 1f);

	}

	public void UnlockTalent(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [18], 1f);

	}

	public void UsePotion1(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [19], 1f);

	}

	public void UsePotion2(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [20], 1f);

	}

	public void BrewPotion(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [21], 1f);

	}

	public void FreezeTime(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [22], 1f);

	}

	public void SpellFlurryActivate(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [23], 1f);

	}

	public void MegaSlashActivate(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [24], 1f);

	}

	public void FlurryActivate(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [25], 1f);

	}

	public void EarthWoodBreak(){ //for chest breaking open
		restorePitch ();
		soundEffect.PlayOneShot (audioList [26], 1f);

	}

	public void atkCritSound(){

	//	soundEffect.pitch = Random.Range (8, 11)/10f;
		soundEffect.PlayOneShot (audioList [27],1f);
		soundEffect.pitch = 1f;
		restorePitch ();

	}

	public void splCritSound(){

	//	soundEffect.pitch = Random.Range (8, 11)/10f;
		soundEffect.PlayOneShot (audioList [28],1f);
		restorePitch ();

	}

	public void collectGem(){

		int RandNum = Random.Range (0, 3);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [29], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [30], 1f);

			break;
		case 2:
			soundEffect.PlayOneShot (audioList [31], 1f);
			break;

		}
		restorePitch ();

	}

	public void waterSpellCast(){

		int RandNum = Random.Range (0, 2);
	//	soundEffect.pitch = Random.Range (8, 11)/10f;


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [32], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [33], 1f);

			break;

		}
		restorePitch ();

	}

	public void waterSpellHit(){

		int RandNum = Random.Range (0, 2);
	//	soundEffect.pitch = Random.Range (8, 11)/10f;

		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [34], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [35], 1f);

			break;

		}
		restorePitch ();

	}

	public void rockHit(){
		restorePitch ();

		int RandNum = Random.Range (0, 2);
	//	soundEffect.pitch = Random.Range (8, 11)/10f;


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [36], 1f);

			break;
		case 1:
			soundEffect.PlayOneShot (audioList [37], 1f);

			break;

		}
		restorePitch ();

	}

	public void rockExplode(){
		restorePitch ();

	//	soundEffect.pitch = Random.Range (8, 11)/10f;
		soundEffect.PlayOneShot (audioList [38],1f);
		soundEffect.pitch = 1f;

		}

	public void startTornado(){

	//	soundEffect.pitch = Random.Range (8, 11)/10f;
		int RandNum = Random.Range (0, 2);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [39], 1f);

			break;
		case 1:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [39], 1f);

			break;

		}
		restorePitch ();

	}

	public void TornadoHit(){
		restorePitch ();

		//soundEffect.pitch = Random.Range (8, 11)/10f;
		soundEffect.PlayOneShot (audioList [41],1f);
		soundEffect.pitch = 1f;

	}

	public void GiveExtraCoin(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [42],1f);
		soundEffect.pitch = 1f;

	}

	public void showInventoryItem(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [43],1f);
		soundEffect.pitch = 1f;

	}

	public void twentyTapHeal(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [44],1f);
		soundEffect.pitch = 1f;

	}

	public void ResurrectionSound(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [45],1f);
		soundEffect.pitch = 1f;

	}

	public void openLevelUnlock(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [46],1f);
		soundEffect.pitch = 1f;

	}

	public void addStatsSound(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [47],1f);
		soundEffect.pitch = 1f;

	}

	public void rerollSound(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [48],1f);
		soundEffect.pitch = 1f;

	}

	public void unlockLevelSfx(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [49],1f);
		soundEffect.pitch = 1f;

	}

	public void oldManHappy(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [50],1f);
		soundEffect.pitch = 1f;

	}

	public void oldManSurprised(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [51],1f);
		soundEffect.pitch = 1f;

	}

	public void oldManConfident(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [52],1f);
		soundEffect.pitch = 1f;

	}

	public void equipItem(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [53],1f);
		soundEffect.pitch = 1f;

	}

	public void sellItem(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [54],1f);
		soundEffect.pitch = 1f;

	}

	public void slimeDeath(){

		restorePitch ();
		int RandNum = Random.Range (0, 2);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [55], 1f);

			break;
		case 1:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [59], 1f);

			break;
		case 2:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [74], 1f);

			break;
		}
	}

	public void mushroomDeath(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [56],1f);
		soundEffect.pitch = 1f;

	}

	public void skeleWarriorDeath(){

		restorePitch ();
		soundEffect.pitch = 1f;
        int RandNum = Random.Range(0, 2);


        switch (RandNum)
        {
            case 0:
                soundEffect.PlayOneShot(audioList[57], 1f);

                break;
            case 1:
                //soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
                soundEffect.PlayOneShot(audioList[84], 1f);

                break;
        }

    }

	public void skeleMageDeath(){

		restorePitch ();

        int RandNum = Random.Range(0, 2);


		soundEffect.pitch = 1f;

        switch (RandNum)
        {
            case 0:
                soundEffect.PlayOneShot(audioList[58], 1f);

                break;
            case 1:
                //soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
                soundEffect.PlayOneShot(audioList[83], 1f);

                break;
        }

    }

	public void reptileDeath(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [60],1f); 
		soundEffect.pitch = 1f;

	}

	public void werewolfDeath(){

		restorePitch ();
		int RandNum = Random.Range (0, 2);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [61], 1f);

			break;
		case 1:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [62], 1f);

			break;
		}

	}

	public void dailyCountUp(){

		restorePitch ();

		if(soundEffect.isPlaying==false){

			soundEffect.PlayOneShot (audioList [63],0.8f);
		}
	}

	public void dailyLoginBonus(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [64],0.8f); 
		soundEffect.pitch = 1f;

	}

	public void dailyCountEnd(){

		restorePitch ();
	//	soundEffect.PlayOneShot (audioList [65],0.5f); 
		soundEffect.pitch = 1f;

	}

	public void slimeBite(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [66],1f); 
		soundEffect.pitch = 1f;

	}

	public void swordSlimeAtk(){

		restorePitch ();
		int RandNum = Random.Range (0, 3);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [67], 1f);

			break;
		case 1:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [77], 1f);

			break;
		case 2:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [78], 1f);

			break;
		}
	}

	public void mushGuyAtk(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [68],1f); 
		soundEffect.pitch = 1f;

	}

	public void skelePunch(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [69],1f); 
		soundEffect.pitch = 1f;

	}

	public void enemyWoosh(){

		int RandNum = Random.Range (0, 3);


		switch (RandNum) {
		case 0:
			soundEffect.PlayOneShot (audioList [70], 1f);

			break;
		case 1:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [71], 1f);

			break;
		case 2:
			//soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
			soundEffect.PlayOneShot (audioList [72], 1f);

			break;
		}

	}

	public void skeleMageStaffHit(){

		restorePitch ();
		soundEffect.PlayOneShot (audioList [73],1f); 
		soundEffect.pitch = 1f;

	}

	public void werewolfAtk(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [75],1f); 
		soundEffect.pitch = 1f;

	}

	public void shamanAtk(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [76],1f); 
		soundEffect.pitch = 1f;

	}



	//77,78 used for other sounds of swords

	public void fireworks(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [79],1f); 
		soundEffect.pitch = 1f;

	}

	public void fanfare(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [80],1f); 
		soundEffect.pitch = 1f;

	}

	public void purchaseWithGems(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [81],1f); 
		soundEffect.pitch = 1f;

	}

	public void characterDown(){ //slime death three is used for 74

		restorePitch ();
		soundEffect.PlayOneShot (audioList [82],1f); 
		soundEffect.pitch = 1f;

	}

    //83 being used for skelemage death 2
    //84 being used for skelewarriors death 2

    public void DungeonEnemyCount()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[85], 1f);
        soundEffect.pitch = 1f;

    }

    public void DungeonCompleteCheer()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[86], 1f);
        soundEffect.pitch = 1f;

    }

    public void eggCrack()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[87], 1f);
        soundEffect.pitch = 1f;

    }

    public void eggCrack2()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[88], 1f);
        soundEffect.pitch = 1f;

    }

    public void eggBreak()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[89], 1f);
        soundEffect.pitch = 1f;

    }

    public void upgradePet()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[90], 1f);
        soundEffect.pitch = 1f;

    }

    public void closeEquipPet()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[91], 1f);
        soundEffect.pitch = 1f;

    }

    public void eventChestOpen()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[92], 1f);
        soundEffect.pitch = 1f;

    }

    public void soulChargeUse()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[93], 1f);
        soundEffect.pitch = 1f;

    }

    public void pullBowOne()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[94], 1f);
        soundEffect.pitch = 1f;

    }

    public void pullBowTwo()
    { //slime death three is used for 74

        restorePitch();
        soundEffect.PlayOneShot(audioList[95], 1f);
        soundEffect.pitch = 1f;

    }

    public void shootArrow()
    { //slime death three is used for 74

        restorePitch();
 
        int RandNum = Random.Range(0, 2);


        switch (RandNum)
        {
            case 0:
                soundEffect.PlayOneShot(audioList[96], 1f);

                break;
            case 1:
                //soundEffect.PlayOneShot (audioList [40], 1f); //not used anymore becuase it didnt sound good
                soundEffect.PlayOneShot(audioList[97], 1f);

                break;
        }

        soundEffect.pitch = 1f;

    }



    //--------------------------------------------------------------------------------------------------------------



    public void WolfDeath(){
		restorePitch ();
	//	soundEffect.PlayOneShot (audioList [3],1f);
	}



	public void SwordHit(){
		restorePitch ();
	//	soundEffect.PlayOneShot (audioList [5],1f);
	}

	public void BoneThud(){
		restorePitch ();
	//	int boneDropRand = Random.Range (50, 100);
	//	soundEffect.PlayOneShot (audioList [6],boneDropRand/100f);
	}

	public void RockOnFloor(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [7],1f);
	}

	public void RockHitEnemy(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [8], 1f);

	}

	public void RockExplosion(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [1], 1f);

	}

	public void BoneHit(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [8], 1f);

	}

	public void WolfBite(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [9], 1f);
		
	}

	public void WolfBiteGrowl(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [10], 1f);
		
	}

	public void TornadoSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [11], 1f);
		
	}



	public void openSpellBook(){
		soundEffect.pitch = 0.5f;
		soundEffect.PlayOneShot (audioList [13], 1f);
		soundEffect.pitch = 1f;

	}

	public void openEquipBag(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [14], 1f);
		
	}

	public void openStats(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [15], 1f);
		
	}

	public void WaterBeamCharge(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [16], 1f);
		
	}

	public void WaterBeamShot(){
		restorePitch ();
		soundEffect.loop = true;
		soundEffect.clip = audioList [17];
		soundEffect.Play ();
		
	}

	public void footStep1(){
		restorePitch ();
			soundEffect.PlayOneShot (audioList [18], 0.7f);
	}

	public void footStep2(){
		restorePitch ();
			soundEffect.PlayOneShot (audioList [19], 0.7f);
	}



	public void getPotion(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [21], 1f);
		
	}

	public void BriefWindSound(){ //for skele earth hit
		restorePitch ();
		soundEffect.PlayOneShot (audioList [23], 1f);
		
	}

	public void PewPewSound(){ //for skele earth hit
		restorePitch ();
		soundEffect.PlayOneShot (audioList [24], 1f);
		
	}

	public void WaterBeamShotEnd(){

		soundEffect.loop = false;

	}

	public void TreantVoice(){ //for skele earth hit
		restorePitch ();
		soundEffect.PlayOneShot (audioList [27], 1f);
		
	}

	public void TreantFootStep(){ //for skele earth hit
		restorePitch ();
		soundEffect.PlayOneShot (audioList [28], 1f);
		
	}

	public void TreantHit(){ //for skele earth hit
		restorePitch ();
		soundEffect.PlayOneShot (audioList [29], 1f);
		
	}

	public void NewItemFound(){ //for skele earth hit
		restorePitch ();
		soundEffect.reverbZoneMix = 0.5f;
		soundEffect.PlayOneShot (audioList [30], 0.6f);
		soundEffect.reverbZoneMix = 1;

	}





	public void EvilGuyDeath(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [33], 1f);
	
	}

	public void confusionSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [34], 1f);
		
	}

	public void waterExplosion(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [35], 1f);
		
	}

	public void runeTravel(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [36], 0.5f);
		
	}

	public void InvisSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [37], 0.5f);
		
	}

//	public void ReptileDeath(){
//		restorePitch ();
//		soundEffect.PlayOneShot (audioList [38], 1f);
//		
//	}

	public void FinalBossLaugh(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [39], 1f);
		
	}

	public void FinalBossDeath1(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [40], 1f);
		
	}

	public void FinalBossDeath2(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [41], 1f);
		
	}

	public void RuneSpawnSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [42], 0.5f);
		
	}

	public void RuneHitGround(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [43], 1f);
		
	}

	public void PlayerDeath(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [44], 1f);
		
	}

	public void runeSelection(){

		soundEffect.PlayOneShot (audioList [45], 1f);
		/*
		soundEffect.clip = audioList [45];
		if (soundEffect.clip == audioList [45] && !soundEffect.isPlaying) {
			soundEffect.volume=0.7f;
			soundEffect.Play();
		}
		*/
	}
	
	public void undoSelection(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [46], 1f);

	}

	public void _SolarBeamSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [47], 1f);
		
	}

	public void LightningStrike(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [48], 1f);

	}



	public void MagicJumpSfx(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [50], 1f);
		
	}

	public void EnchantPowerUp(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [51], 1f);
		
	}

	public void GolemDeathSound(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [52], 1f);
		
	}

	public void FinalBossAct2Death(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [53], 1f);
		
	}

	public void FinalBossAct2Laugh(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [54], 1f);
		
	}

	public void FinalBossAct2Appear(){
		restorePitch ();
		soundEffect.PlayOneShot (audioList [55], 1f);
		
	}


	public void MainMenuMusic(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [0];
		musicPlaying.Play ();
	}

	public void Level1Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();

	}

	public void Level2Music(){
		musicPlaying.Stop ();
		musicPlaying.volume = 1f;
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
	}

	public void Level3Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [3];
		musicPlaying.Play ();
		
	}

	public void Level4Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level5Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();
		
	}

	public void Level6Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
		
	}

	public void Level7Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [3];
		musicPlaying.Play ();
		
	}

	public void Level8Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level9Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();
		
	}

	public void Level10Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
		
	}

	public void Level11Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [3];
		musicPlaying.Play ();
		
	}

	public void Level12Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level13Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
		
	}

	public void Level14Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();
		
	}

	//lv15+ music

	public void Level15Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [6]; //happy hip hop
		musicPlaying.Play ();
		
	}

	public void Level16Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [3];
		musicPlaying.Play ();
		
	}

	public void Level17Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level18Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
		
	}

	public void Level19Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [6];
		musicPlaying.Play ();
		
	}

	public void Level20Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();
		
	}

	public void Level21Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level22Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [8];
		musicPlaying.Play ();
		
	}

	public void Level23Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [6];
		musicPlaying.Play ();
		
	}

	public void Level24Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [1];
		musicPlaying.Play ();
		
	}

	public void Level25Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void Level26Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [2];
		musicPlaying.Play ();
		
	}

	public void Level27Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [6];
		musicPlaying.Play ();
		
	}

	public void Level28Music(){
		musicPlaying.Stop ();
		defaultMusicVolume ();
		musicPlaying.clip = musicList [4];
		musicPlaying.Play ();
		
	}

	public void defaultMusicVolume(){
		musicPlaying.volume = 1f;
	}



}
