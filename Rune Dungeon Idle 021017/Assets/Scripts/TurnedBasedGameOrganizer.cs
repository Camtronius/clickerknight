﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.SocialPlatforms;
using System.Linq;
using UnityEngine.Analytics;
using TMPro;

public class TurnedBasedGameOrganizer : MonoBehaviour {

	public bool UnityMode = false;
	public bool linearRuneSelection = false;
	
	//~~Networking declarations
	//private TurnBasedMatch mMatch = null;
	private MatchDataRunes mMatchData = null; //remember to set this back to null when done    new MatchDataRunes();
	
	public GameObject netDictionaryObj;

	const int MinOpponents = 1; //makes it so you can only invite one person max
	const int MaxOpponents = 1; //makes it so you can only invite one person max
	const int Variant = 0;  // default
	private bool mWaitingForAuth = false;

	public GameObject SpellGuideWindow;//shows whats happening in the game
		public int pressCounter = 0; //for opening and closing the window
	public Text statusText;//shows whats happening in the game
	public Text networkText; //shows whats happening through network
	public Text otherText; //shows anything else
	public Text playerID; //shows the player iD

	public GameObject p1DamageAmount;
	public GameObject p2DamageAmount; //for hit point loss notification

		private string playerName;
	public Text healthRecorded; //shows the player iD
	public GameObject WhosTurnPanel;
	public GameObject WhoWon;
	public GameObject EndMatchPanel;
	public GameObject multiplayerGameMenu;
	public GameObject BlockClickMousePanel;
	public GameObject TurnTaken;

	public GameObject spellDescriptionWindow;
	public GameObject spellDescriptionEquip;
	public GameObject equipmentBackButton;
	public SpellInfo spellButtonHolder;
	public Image spellDescriptionImage;
	public TextMeshProUGUI spellDescritpionText;
	public TextMeshProUGUI spellTitleText;


	public float player1InvisValue=0;
	public float player2InvisValue=0;

	public int playerInt = 1; //int represents which players turn it is, 1 = player 1, 2 = player 2, 
	//~~NON - networking declarations

	public List <Vector2> touchList = new List<Vector2>(); //has a list of all the available meteors on the grid
	public List <GameObject> selectedBlocks = new List<GameObject> (); //4 index list of the blocks that are selected
	public List <GameObject> sentBlocks = new List<GameObject> (); //4 index list of the blocks that are selected
	public GameObject SpellListener;

	public GameObject LastSpellPlayer1;//the UI element of the last spell casted.
	public GameObject LastSpellPlayer2;
		public Sprite WindSprite; //this is so the last spell casted can be changed to this...
		public Sprite FireSprite;
		public Sprite WaterSprite;
		public Sprite EarthSprite;
		public Sprite nullsprite;

	//for meteor spawning//
	public GameObject TestMeteor; //meteor object which gets spawned
	public GameObject GhostMeteor; //meteor object which gets spawned
	public GameObject InvisParticles; //this is for making the characters invis if they are already invisible.

	public int ColumnLength; //number of columns for spawning blocks
	public int RowHeight; //number or rows for spawning blocks

	public List<MeteorCode> meteorList = new List<MeteorCode>();
	public List<float> copiedPlayerMeteorList = new List<float>();
	public List<GameObject> ghostMeteorList = new List<GameObject>();

	public List<float> player1GearMods = new List<float>(); //goes in the same order as runes 0 wind 1 fire 2 water 3 earth
	public List<float> player2GearMods = new List<float>(); //goes in the same order as runes 0 wind 1 fire 2 water 3 earth
	public List<float> player1AttributeMods = new List<float> ();
	public List<float> player2AttributeMods = new List<float> ();
	//for meteor spawning//

	public int	selectedBlocksCount =0; //to show how many blocks have been selected
	public bool mouseDown = false; //is mouse down or not bool
	private GameObject GameBoard; 
	//private GameObject Canvas; 
	
	RaycastHit2D hit; //for detecting if a meteor is selcted
	public LayerMask myLayerMask;
	public GameObject mouseObject; //for trail on mouse

	public GameObject player1;
	public GameObject player2;

	public bool nextTurnBool = false;
	public GameObject effectivenessObj;
	public GameObject critTextObj;

	//for rune fx
	public GameObject runeExplosion;
	public GameObject travelingRuneObject;
	//public bool gameOver = false; //may not be needed if we just check if a player is dead instead
	public GameObject HpGainP1;
	public GameObject HpGainP2;

	// Use this for initialization

	//for sounds:

	public SoundManager PersistantSoundManager;
	public GameObject itemObj;

	public bool QuickMatchSearching = false; //this makes it so you can only serach one game at atime
	public GameObject textForFindPlayer;


	void Start () {

		GameBoard = GameObject.Find ("GameBoardObject"); //gets gameboard object to instantiate new blocks
			
		PersistantSoundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		//	StartCoroutine ("ClickDetector"); //detects if a block is being selected

		if (Application.platform == RuntimePlatform.WindowsEditor) {
			StartCoroutine ("ClickDetector"); //detects if a block is being selected
			StartCoroutine ("TravelRuneRoutine");
		}
		
		if (Application.platform == RuntimePlatform.Android) {
			StartCoroutine ("ClickDetectorAndroid"); //detects if a block is being selected
			StartCoroutine ("TravelRuneRoutine");
		}
		//	GooglePlayGames.PlayGamesPlatform.Activate(); // authenticate user for GPGS(Google play game services):

		GooglePlayAuth ();

		//to spawn meteors when not online ***

	if (UnityMode == true) {

			mMatchData =  new MatchDataRunes();

			SetupObjects (true); //*** remove this when finished with offline mode test
		}

//		print ("start is called");

	}

	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			mouseDown=true;
		}
		
		if (Input.GetMouseButtonUp (0)) {
			mouseDown=false;
			
			touchList.Clear();
		}
	}

	public void OnClicked(Button button)
	{
				
		if (button.GetComponent<SpellInfo> () != null) {

			print(button.name);
			
			spellDescriptionWindow.SetActive (true);
			
			spellButtonHolder = button.GetComponent<SpellInfo> (); //this holds the button that was clicked so we can get the playerprefs from it etc.
			
			print(spellButtonHolder.spellPlayerPref.ToString() + " = " + PlayerPrefs.GetFloat(spellButtonHolder.spellPlayerPref));
			
			spellTitleText.text = button.GetComponent<SpellInfo>().spellTitle;
			spellDescritpionText.text = button.GetComponent<SpellInfo>().spellDescription;
			spellDescriptionImage.sprite = button.GetComponent<SpellInfo>().spellSprite;
			
		}

	}

	public void playSelectionSound(){
		
		
		//	PersistantSoundmanager.soundEffect.pitch = 0.5f + selectedBlocks.Count/4f;
		
		if (selectedBlocks.Count == 0) {
			PersistantSoundManager.runeSelection ();
		}
		//persistantsoundmanaer.
		//45 - rune select
		//46 rune undo
		
	}

	public void backFromItemDescription (){

		if (spellDescriptionWindow.activeSelf == true) {
			spellDescriptionWindow.SetActive(false);

		}
		//persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound
	}

	//for unity testing
	IEnumerator ClickDetector(){

		while (true) {
			
			Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //gets the mouse screen coordinates 
	
			if(mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && selectedBlocksCount<4){

				//mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId)
					
				//	if(Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-2])>=0 &&
				//	   Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-2]) < 0.5f){ //checks if the distance is less than 0.5 since being
						//pressed. once the list has been populated

						Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
						RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, myLayerMask);  //shhots the ray in that direction and detects whatever was hit

				if(hit.collider!=null && linearRuneSelection==true && hit.collider.GetComponent<MeteorCode>()!=null){ //this is if u want to try it with the linear rune selection
							//	print (hit.collider); 
							//this is the code to allow only adajacent blocks to be selected

					if(hit.collider.GetComponent<MeteorCode>().isSelected==false && hit.collider.GetComponent<MeteorCode>().isSelectable==true){
						playSelectionSound();
					}
							for(int i = 0; i<meteorList.Count; i++){

								if(meteorList[i].GetComponent<MeteorCode>().isSelected == true
								   && meteorList[i].GetComponent<MeteorCode>().Neighbors.Contains(hit.collider.gameObject)
								   || selectedBlocksCount==0){

									if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
									   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
									   ){
										
										hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
										
										selectedBlocks.Add(hit.collider.gameObject);
										selectedBlocksCount+=1; //counts how many blocks have been selected
									}

								}

							}

						}

					if(hit.collider!=null && linearRuneSelection==false){ //this is for testing purposes where u can select any rune
						//	print (hit.collider); 
						//this is the code to allow only adajacent blocks to be selected

								if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
								   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
								   ){
									
									hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
									
									selectedBlocks.Add(hit.collider.gameObject);
									selectedBlocksCount+=1; //counts how many blocks have been selected
								}

					}
				
			}
			/*
			if(selectedBlocksCount>=4){

			if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}

				nextTurnBool = true; //this is to check if 4 blocks has been taken or not, so tht they can recieve a spell without taking a turn

				for(int i =0; i<4; i++){
					if(selectedBlocks[i]!=null){

						mMatchData.mRuneTypes.Add(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this is the code to make it work online

						SpellListener.GetComponent<SpellCreator>().sentSpell.Add((int)selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this adds to the sent spell list
						//so the spell listener can decide which spell it is that was sent.
						
						Debug.Log("added " + selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation + " to mRunetypes at : " + i ); 

						Debug.Log("index to add is at " + meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); 
						
					//	spawnMeteor(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().columnSpawned, i);
						                                                 // meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); //spawns a block at the column of block removed

						//meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())

						yield return new WaitForSeconds(0.25f);

					//	meteorList.Remove(selectedBlocks[i].gameObject.GetComponent<MeteorCode>());

					//	selectedBlocks[i].GetComponent<SpriteRenderer>().sprite=nullsprite;

						colorOldBlocks(i);

					//	Destroy(selectedBlocks[i]);
					}
				}

				if(playerInt==1){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer1(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[0] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;

				}
				if(playerInt==2){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer2(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[1] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;
				}

				selectedBlocksCount=0;
				selectedBlocks.Clear ();

				for(int i=0; i <mMatchData.mRuneTypes.Count; i++){ 
					print(mMatchData.mRuneTypes[i]);
				}

				setPlayersLastSpell(); //this will set the players last spell type before the turn is taken.
			


			}


			*/
			yield return new WaitForSeconds(0.05f);
			
		}
	}

	//for android play
	IEnumerator ClickDetectorAndroid(){
		
		while (true) {
			
			Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //gets the mouse screen coordinates 

			if(mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId) && selectedBlocksCount<4){
				
				//mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId)
					
					//	if(Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-2])>=0 &&
					//	   Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-2]) < 0.5f){ //checks if the distance is less than 0.5 since being
					//pressed. once the list has been populated
					
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, myLayerMask);  //shhots the ray in that direction and detects whatever was hit
					
				if(hit.collider!=null && linearRuneSelection==true && hit.collider.GetComponent<MeteorCode>()!=null){ //this is if u want to try it with the linear rune selection
						//	print (hit.collider); 
						//this is the code to allow only adajacent blocks to be selected


					if(hit.collider.GetComponent<MeteorCode>().isSelected==false && hit.collider.GetComponent<MeteorCode>().isSelectable==true){
						playSelectionSound();
					}

						for(int i = 0; i<meteorList.Count; i++){
							
							if(meteorList[i].GetComponent<MeteorCode>().isSelected == true
							   && meteorList[i].GetComponent<MeteorCode>().Neighbors.Contains(hit.collider.gameObject)
							   || selectedBlocksCount==0){
								
								if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
								   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
								   ){
									
									hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
									
									selectedBlocks.Add(hit.collider.gameObject);
									selectedBlocksCount+=1; //counts how many blocks have been selected
								}
								
							}
							
						}
						
					}
					
					if(hit.collider!=null && linearRuneSelection==false){ //this is for testing purposes where u can select any rune
						//	print (hit.collider); 
						//this is the code to allow only adajacent blocks to be selected
						
						if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
						   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
						   ){
							
							hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
							
							selectedBlocks.Add(hit.collider.gameObject);
							selectedBlocksCount+=1; //counts how many blocks have been selected
						}
						
					}
				
			}

			/*
			
			if(selectedBlocksCount>=4){
				
				if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}
				
				nextTurnBool = true; //this is to check if 4 blocks has been taken or not, so tht they can recieve a spell without taking a turn
				
				for(int i =0; i<4; i++){
					if(selectedBlocks[i]!=null){
						
						mMatchData.mRuneTypes.Add(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this is the code to make it work online
						
						SpellListener.GetComponent<SpellCreator>().sentSpell.Add((int)selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this adds to the sent spell list
						//so the spell listener can decide which spell it is that was sent.
						
						Debug.Log("added " + selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation + " to mRunetypes at : " + i ); 
						
						Debug.Log("index to add is at " + meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); 
						
						//	spawnMeteor(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().columnSpawned, i);
						// meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); //spawns a block at the column of block removed
						
						//meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())
						
						yield return new WaitForSeconds(0.25f);
						
						//	meteorList.Remove(selectedBlocks[i].gameObject.GetComponent<MeteorCode>());
						
						//	selectedBlocks[i].GetComponent<SpriteRenderer>().sprite=nullsprite;
						
						colorOldBlocks(i);
						
						//	Destroy(selectedBlocks[i]);
					}
				}
				
				if(playerInt==1){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer1(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[0] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;
					
				}
				if(playerInt==2){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer2(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[1] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;
				}
				
				selectedBlocksCount=0;
				selectedBlocks.Clear ();
				
				for(int i=0; i <mMatchData.mRuneTypes.Count; i++){ 
					print(mMatchData.mRuneTypes[i]);
				}
				
				setPlayersLastSpell(); //this will set the players last spell type before the turn is taken.
				
				
				
			}
			*/
			yield return new WaitForSeconds(0.05f);
			
		}
	}

	IEnumerator TravelRuneRoutine(){
		
		while (true) {
			
			if(selectedBlocksCount>=4){

				PersistantSoundManager.runeTravel();

				
				if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}
				
				nextTurnBool = true; //this is to check if 4 blocks has been taken or not, so tht they can recieve a spell without taking a turn
				
				for(int i =0; i<4; i++){
					if(selectedBlocks[i]!=null){
						
						mMatchData.mRuneTypes.Add(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this is the code to make it work online
						
						SpellListener.GetComponent<SpellCreator>().sentSpell.Add((int)selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this adds to the sent spell list
						//so the spell listener can decide which spell it is that was sent.
						
						Debug.Log("added " + selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation + " to mRunetypes at : " + i ); 
						
						Debug.Log("index to add is at " + meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); 

						//just added this, if something doesnt work this is the reason

						GameObject RuneBreaking = (GameObject)Instantiate (runeExplosion, new Vector3 (selectedBlocks[i].transform.position.x,selectedBlocks[i].transform.position.y, 0), Quaternion.identity); //instantiates the beam offscreen

						selectedBlocks[i].GetComponent<SpriteRenderer>().sprite = null; // makes the sprite invis 
						
	
						//	spawnMeteor(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().columnSpawned, i);
						// meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())); //spawns a block at the column of block removed
						
						//meteorList.IndexOf(selectedBlocks[i].gameObject.GetComponent<MeteorCode>())
						
						yield return new WaitForSeconds(0.25f);
						
						//	meteorList.Remove(selectedBlocks[i].gameObject.GetComponent<MeteorCode>());
						
						//	selectedBlocks[i].GetComponent<SpriteRenderer>().sprite=nullsprite;
						
						colorOldBlocks(i); //this was removed but may be needed!!!!!!!!!!!!!!!!!!!!!!!!************************* 050616
						
						//	Destroy(selectedBlocks[i]);
					}
				}
				
				if(playerInt==1){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer1(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[0] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;
					
				}
				if(playerInt==2){
					SpellListener.GetComponent<SpellCreator>().sentSpellDesignatorPlayer2(); //calls through the list and determines what spell you are casting
					mMatchData.lastSpellTypeCasted[1] = SpellListener.GetComponent<SpellCreator>().getPrimaryElement;
				}
				
				selectedBlocksCount=0;
				selectedBlocks.Clear ();
				
				for(int i=0; i <mMatchData.mRuneTypes.Count; i++){ 
					print(mMatchData.mRuneTypes[i]);
				}
				
				setPlayersLastSpell(); //this will set the players last spell type before the turn is taken.
				
				
				
			}
			
			yield return new WaitForSeconds(0.1f);
			
		}
		
	}

	public void effectiveness(int effectivenessInt){
		
		if(effectivenessInt==1){//this means it is super effective
			effectivenessObj.GetComponent<TextMeshProUGUI>().text = "Super Effective!";
			effectivenessObj.gameObject.SetActive(true);
		}
		if(effectivenessInt==2){//this means it is super effective
			effectivenessObj.GetComponent<TextMeshProUGUI>().text = "Not Very Effective";
			effectivenessObj.gameObject.SetActive(true);
		}
	}

	public void colorOldBlocks (int i){ //gets the passed index of the for loop

		if (sentBlocks [i].GetComponent<Image> ().sprite == WindSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (0); 

		
				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;


			print ("WHITE DETECTED");
		}
		if (sentBlocks [i].GetComponent<Image> ().sprite == FireSprite) {

		
				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (1); 
		//	

				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;


		print ("red DETECTED");
			
		}
		if (sentBlocks [i].GetComponent<Image> ().sprite == WaterSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (2); 


			selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;

				print ("blue DETECTED");

		}
		if (sentBlocks [i].GetComponent<Image> ().sprite == EarthSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (3); 


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;

			print ("grey DETECTED");
			
		}



		selectedBlocks[i].GetComponent<MeteorCode>().isSelected=false;

		sentBlocks [i].GetComponent<Image> ().sprite = nullsprite; //this makes the blocks invis after they are spawned

			
	}

	public void createNextBlocks (){

		if (mMatchData.RunesIncoming.Count == 0) { //this is the condition if the game is just starting...

			for (int i = 0; i<4; i++) {

				int RandColor = Random.Range (0, 4);

				switch (RandColor) {
					
				case 0:
					sentBlocks [i].GetComponent<Image> ().sprite = WindSprite; //its sent blocks because we used to use this list to track which blocks the player last sente
					break;													//to cast a spell
				case 1:
					sentBlocks [i].GetComponent<Image> ().sprite = FireSprite;					
					break;
				case 2:
					sentBlocks [i].GetComponent<Image> ().sprite = WaterSprite;	
					break;
				case 3:
					sentBlocks [i].GetComponent<Image> ().sprite = EarthSprite;	
					break;
				}

			}

		} 
		 //this will set the runes received as the runes
		if (mMatchData.RunesIncoming.Count > 0) {

			for (int i = 0; i<mMatchData.RunesIncoming.Count; i++) {
			
				switch ((int)mMatchData.RunesIncoming[i]) {
					
				case 0:
					sentBlocks [i].GetComponent<Image> ().sprite = WindSprite;
					break;
				case 1:
					sentBlocks [i].GetComponent<Image> ().sprite = FireSprite;					
					break;
				case 2:
					sentBlocks [i].GetComponent<Image> ().sprite = WaterSprite;	
					break;
				case 3:
					sentBlocks [i].GetComponent<Image> ().sprite = EarthSprite;	
					break;
				}
			}
		}

		mMatchData.RunesIncoming.Clear (); //clears the list to make sure there isnt any overlap between what we are sending...

		//this is where the next players runes are created...
		for (int i =0; i<4; i++) {

			int RandColor2 = Random.Range (0, 4); //this is for the runes to be sent
		
			print ("the next incoming runes for the next player are :" + RandColor2);
		
			mMatchData.RunesIncoming.Add (RandColor2); //creates the incoming runes
		}
	}

	IEnumerator GhostMeteorList(){

		while (true) {
			
			yield return new WaitForSeconds(2f);
			
			for(int i=0; i<meteorList.Count; i++){
				
				GhostMeteor = Instantiate (GhostMeteor, new Vector3 (meteorList[i].transform.position.x, meteorList[i].transform.position.y, 0), Quaternion.identity) as GameObject;
				
				GhostMeteor.GetComponent<GhostMeteor>().ghostIndex = i;
				
				ghostMeteorList.Add(GhostMeteor);
				
				
				GhostMeteor.name = "Ghost Meteor" + i.ToString();
				
//				print ("ghost meteor being created!");

				playerID.text = "Ghost Block spawned " + i.ToString();
			}
			yield break;
		}
	}

	public void printIndex(MeteorCode MeteorObject){ //is this method ever used?
		
		print("the index of this game object is: " + meteorList.IndexOf(MeteorObject));	//begins searching at index 0	
		
		MeteorObject.isSelected = true;
		
	}
	
	public void spawnMeteorStart(){

		meteorList.Clear ();

		PersistantSoundManager.RuneSpawnSound ();


		for (int i = 0; i<RowHeight; i++) { //4

			for (int j=0; j<ColumnLength; j++) { //4
				
				GameObject Meteor = Instantiate (TestMeteor, new Vector3 (j + j*.1f, i, 0), Quaternion.identity) as GameObject;
				
				meteorList.Add (Meteor.GetComponent<MeteorCode> ()); //adds the meteor to the list
				
				//	int currentIndex = meteorList.IndexOf(Meteor.GetComponent<MeteorCode>());
				
				Meteor.GetComponent<MeteorCode> ().columnSpawned = j;
				
				Meteor.name = Meteor + meteorList.IndexOf(Meteor.GetComponent<MeteorCode>()).ToString();

				switch (i){

				case (0):

							switch (j){

							case (0):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
								break;
							case (1):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
								break;
							case (2):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
								break;
							case (3):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
								break;
									}

				break;

				case (1):
					
						switch (j){
							
						case (0):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (1):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (2):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (3):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						}
					
				break;
				
				case (2):
					
						switch (j){
							
						case (0):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (1):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (2):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (3):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						}
					
				break;
				
				case (3):
					
						switch (j){
							
						case (0):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (1):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (2):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						case (3):
							Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
							Meteor.GetComponent<MeteorCode>().LockColor=true;
							break;
						}
							
				break;

				}

			}


		}
			




	}
	
	public void ChangeBlockColor(){
		
		if(copiedPlayerMeteorList.Count>0){	
			
			print("copied meteorlist count is: " + copiedPlayerMeteorList.Count);
			
			print("meteorlist count is: " + meteorList.Count);
			
			
			for(int k=0; k<meteorList.Count; k++){
				
//				print ("k index" + k);

				meteorList[k].LockColor=false;

				meteorList[k].createRuneColor((int)copiedPlayerMeteorList[k]);
				
				meteorList[k].LockColor=true;
				
			}
			
		}
		
	}
	
	public void spawnMeteor(int MeteorColumn, int spawnNumber){  //int MeteorIndexDestroyed
		
		GameObject Meteor =  Instantiate(TestMeteor,new Vector3(MeteorColumn + MeteorColumn*.1f ,4 ,0),Quaternion.identity) as GameObject ;
		Meteor.GetComponent<MeteorCode>().columnSpawned = MeteorColumn;
		
		meteorList.Add(Meteor.GetComponent<MeteorCode> ());
		Meteor.name = Meteor + meteorList.IndexOf(Meteor.GetComponent<MeteorCode>()).ToString();

		print ("block number being spawned is " + spawnNumber);

		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == WindSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("WHITE DETECTED");
		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == FireSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("red DETECTED");

		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == WaterSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("blue DETECTED");

		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == EarthSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("grey DETECTED");

		}
		//of a take turn googleplay method

	}

	public void sortMeteorList(){

		meteorList = meteorList.OrderBy(x=>x.indexSpawned).ToList(); //this was originally in take turn but im guessing u cannot sort a list in the midd

		Debug.Log ("the count of the meteor list is: " + meteorList.Count); 

		print ("sorting finished");
		
		otherText.text = "metoerlist count is " + meteorList.Count;
		
		for (int j =0; j<meteorList.Count; j++) { //GameBoard.GetComponent<MeteorSpawn>().meteorList.Count
			
			otherText.text = "Stopped WHILE sort at " + j.ToString(); 
			
			
			Debug.Log ("index spawned is: " + (int)meteorList[j].GetComponent<MeteorCode>().indexSpawned);
			Debug.Log ("runeintdesg spawned is: " + (float)meteorList[j].GetComponent<MeteorCode>().runeIntDesignation);

			mMatchData.Player1Blocks.Add((float)meteorList[j].GetComponent<MeteorCode>().runeIntDesignation);
			
			otherText.text = "block inserted to playerlist at " + j.ToString(); 

		}
	}

	//--------------------------------------------------------this is where the network code is kept--------------------------------------------------------//

//	void OnMatchStarted(bool success, TurnBasedMatch match) {
//
//		if (success) {
//
//			multiplayerGameMenu.SetActive(false);
//		
//			mMatch = match;  //REMEMBER TO SET BACK TO UNCOMMENT WHEN done
//
//			//	networkText.text = mMatchData.networkStatus;
//
//				try {
//					// Note that mMatch.Data might be null (when we are starting a new match).
//					// MatchData.MatchData() correctly deals with that and initializes a
//					// brand-new match in that case.
//					mMatchData = new MatchDataRunes(mMatch.Data); 
//					
//				if(otherText!=null){
//					otherText.text = "Match started data is" + mMatchData; 	
//				}
//
//				if(match.SelfParticipantId=="p_1"){
//					if(playerID!=null){
//					playerID.text = "P1";
//					playerInt = 1;
//					}
//				}
//
//				if(match.SelfParticipantId=="p_2"){
//
//					if(playerID!=null){
//
//					playerID.text = "P2";
//					playerInt = 2;
//					}
//				}
//
//				player2.SetActive(true);
//
//
////				networkText.text = mMatchData.networkStatus;
//			} catch (MatchData.UnsupportedMatchFormatException ex) {
//				//	mFinalMessage = "Your game is out of date. Please update your game\n" +
//				//		"in order to play this match.";
//					Debug.LogWarning("Failed to parse board data: " + ex.Message);
//					return;
//				}
//
//			// I can only make a move if the match is active and it's my turn!
//			bool canPlay = (match.Status == TurnBasedMatch.MatchStatus.Active &&
//			                match.TurnStatus == TurnBasedMatch.MatchTurnStatus.MyTurn);
//
//			playerName =	match.PendingParticipant.DisplayName; //gets the name of the players so it can be shown before the turn, only works online
//
//			if (canPlay) {
//
//				SetupObjects(canPlay);
//
//			} else {
//
//			//sampleString = ExplainWhyICantPlay(); this explains why you cant play right now
//			}
//
//
//		
//		
//		} else {
//			Debug.Log ("error --- the match failed to start --- error");
//		}
//
////		isMatchOver();
//
//	}
	/*
	public void isMatchOver(){

		
		if (mMatch.Status == TurnBasedMatch.MatchStatus.Complete) {
			PlayGamesPlatform.Instance.TurnBased.AcknowledgeFinished(match.MatchId,
			                                                         (bool success) => {
				if (success) {
					// success!
				} else {
					// show error
				}
			});
		}

	}
*/
	public void CreateQuickMatch(){


		if (QuickMatchSearching == false) {

			textForFindPlayer.GetComponent<TextMeshProUGUI>().text = "<color=green>Searching for Player...</color> ";

			QuickMatchSearching = true;

//
//			PlayGamesPlatform.Instance.TurnBased.CreateQuickMatch (MinOpponents, MaxOpponents,
//		                                                      Variant, OnMatchStarted);
		}
	}


	public void GooglePlayAuth(){
		print ("google play being authe'd");

		if (Social.localUser.authenticated) { //this is to avoid relogging in 
			
			if (Social.localUser.image != null) {
				
				
			} else {
				
			}
			
		} else {
			//this is new above 7/4/16
			if (mWaitingForAuth) {
				return;
			}

			if (Social.localUser.authenticated) {

				if (Social.localUser.image != null) {


				} else {

				}
			} else {

			}

			if (!Social.localUser.authenticated) {
				// Authenticate
				mWaitingForAuth = true;

				Social.localUser.Authenticate ((bool success) => {
					
					mWaitingForAuth = false;

					if (success) {

					} else {

					}
				});
			} else {
				// Sign out!
			//	((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut ();
			}
		}
	}

//	public void InviteFriends(){
//
//		PlayGamesPlatform.Instance.TurnBased.CreateWithInvitationScreen (MinOpponents, MaxOpponents,
//		                                                                Variant, OnMatchStarted);
//
//	}

	public void CheckInbox(){

	//	PlayGamesPlatform.Instance.TurnBased.AcceptFromInbox(OnMatchStarted);
	}

	private class sort : IComparer<MeteorCode>{
		int IComparer<MeteorCode>.Compare(MeteorCode _objA, MeteorCode _objB) {
			float t1 = _objA.indexSpawned;
			float t2 = _objB.indexSpawned;
			print ("this is sorting the list!" + t1.ToString() + " " +  t2.ToString());
			return t1.CompareTo(t2);

		}
	}

	public void TakeTurn() {

	//	otherText.text = "Stopped after sorting... "; 

	//	Debug.Log ("the count of the player1blocks list is: " + mMatchData.Player1Blocks.Count); 

		if (UnityMode == true) {	
		mMatchData.ReadFromBytes(mMatchData.ToBytes()); //*** remove this when finished with offline mode test
		}
	//	setPlayersLastSpell();

		/*
		statusText.text = "RuneListSent " + mMatchData.mRuneTypes[0].ToString() + " " 
			+ mMatchData.mRuneTypes[1].ToString() + " "
				+ mMatchData.mRuneTypes[2].ToString() + " "
				+ mMatchData.mRuneTypes[3].ToString() + " ";
		/*
		networkText.text = "Player 1 runes sent " + mMatchData.Player1Blocks[0].ToString() + " " 
			+ mMatchData.Player1Blocks[1].ToString() + " "
				+ mMatchData.Player1Blocks[2].ToString() + " "
				+ mMatchData.Player1Blocks[3].ToString() + " ";
	*/
	//	otherText.text = "Turn Taken "; 	

		//when testing the game offline
	if (UnityMode == true) {

			SpellListener.GetComponent<SpellCreator>().resetAllCounts();

			playerInt += 1;
			if (playerInt > 2) {
				playerInt = 1;

			}

			if(BlockClickMousePanel.activeSelf==true){
				BlockClickMousePanel.SetActive(false); //if the block mouse pane is already up
			}

			nextTurnBool=false; //reset

			SetupObjects(true);
		}

		//use the code below when running online
		if (UnityMode == false) {
//			PlayGamesPlatform.Instance.TurnBased.TakeTurn (mMatch, mMatchData.ToBytes (),
//		                                              DecideNextToPlay (), (bool success) => {
//
//			});

			TurnTaken.SetActive(true);

			//add something about the turn take text
		}

	//	print("player prefs Fireball " + PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball"));
//		print("player prefs FireDrag " + PlayerPrefs.GetFloat ("NetDictionary_" + "FireDragon"));
//		print("player prefs WaterBeam " + PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBeam"));
//		print("player prefs WaterBubble " + PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBubble"));
//		print("player prefs EarthRock " + PlayerPrefs.GetFloat ("NetDictionary_" + "EarthRock"));
//		print("player prefs EarthTreant " + PlayerPrefs.GetFloat ("NetDictionary_" + "EarthTreant"));
//		print("player prefs Tornado " + PlayerPrefs.GetFloat ("NetDictionary_" + "Tornado"));
//		print("player prefs Invis " + PlayerPrefs.GetFloat ("NetDictionary_" + "Invis"));
//		print("player prefs Shield " + PlayerPrefs.GetFloat ("NetDictionary_" + "Shield"));

	//	for (int i =0; i < netDictionaryObj.GetComponent<NetDictionary>().spellDamageReference.Count; i++) {

	//		print ("spl dmg is " + netDictionaryObj.GetComponent<NetDictionary>().spellDamageReference[i]);
//		print ("trying to get a float" + netDictionaryObj.GetComponent<NetDictionary> ().GetFloat ("Fireball"));
	//	}
	}

//	
//	string DecideNextToPlay() {
//		if (mMatch.AvailableAutomatchSlots > 0) {
//			// hand over to an automatch player
//			return null;
//		} else {
//			// hand over to our (only) opponent
//			Participant opponent = UtilRunes.GetOpponent(mMatch);
//			return opponent == null ? null : opponent.ParticipantId;
//		}
//
//	}

	private void SetupObjects(bool canPlay) {

		if (mMatchData.turnCount.Count == 0) {

			mMatchData.turnCount.Add (0);

			foreach(SpriteRenderer spriteObj in player2.GetComponentsInChildren<SpriteRenderer>()){

				spriteObj.color = Color.black;
			}


		} else {

			mMatchData.turnCount[0] = mMatchData.turnCount[0] +1;

		}

		print ("setup objects called");

	//	if (UnityMode == true) { //there will be a seperate instance in the match started code
			WhosTurn (canPlay, playerName); //makes the player turn panel come out and shows who's turn it is...
	//	}
		checkForGear ();

			setupStats ();
			networkText.text = "stats setup";
			
					setupHealth (); //sets up the health bars for the players if unity is false see the code inside the method
					networkText.text = "health setup";
					
						checkForModifiers ();
						networkText.text = "mods setup";
										
							createNextBlocks ();	//creates the next four blocks to be spawned.
							networkText.text = "new 4 blocks setup";

								setupCrit (); //checks if crit list is empty
								
									
		if (mMatchData.Player1Blocks.Count > 0) {
	
			statusText.text = "player1blocks is full! not working";

			copiedPlayerMeteorList.Clear();

			for (int z=0; z<mMatchData.Player1Blocks.Count; z++) { //when reading from the data, this transfers choosable blocks stored to copiedPlayersMeteorList so the color can be changed
				
				copiedPlayerMeteorList.Add(mMatchData.Player1Blocks [z]);
				
			}

			if(UnityMode==false){ //if you are playing online...

				spawnMeteorStart (); //Just changes the color of the blocks to their starting type when the game first starts also changes the int designation

				networkText.text = "blocks setup";
				ChangeBlockColor (); 
			
				checkForBurntMod();

				checkForDrownedMod();

				checkForConfusionMod (); //this checks for confusion after the blocks are already spawned this should fix the problem and make the blocks instantly 
				//mystery blocks

				networkText.text = "condustion,drowned,burnt mods setup";

			StartCoroutine ("GhostMeteorList"); //creates feeler objects that will get the index of any meteor in that area at that time. So they can

			}
			statusText.text = "player1blocks is full! IS working";


		} else {

			statusText.text = "player1blocks is empty! not working";
			spawnMeteorStart (); //spawns without setting up new colors
			StartCoroutine ("GhostMeteorList"); //creates feeler objects that will get the index of any meteor in that area at that time. So they can
			statusText.text = "player1blocks is empty! IS working";
		}

		if (mMatchData.mRuneTypes.Count == 0) { 
				
		}

		if (mMatchData.lastSpellTypeCasted.Count == 0) {
			//mMatchData.lastSpellTypeCasted.Capacity=2;
			for (int i =0; i<2; i++) {
				mMatchData.lastSpellTypeCasted.Add (-1); //set this back to -1 when done testing...
			}

		} else {

			setPlayersLastSpell(); //this sets the blocks to the last spell the player casted
		
		}

		if (UnityMode == true) {

			checkForBurntMod ();
			
			checkForDrownedMod ();

			checkForConfusionMod (); //this checks for confusion after the blocks are already spawned this should fix the problem and make the blocks instantly 
			//mystery blocks

		}

		if (mMatchData.mRuneTypes.Count > 0) { //checks if the list contains any objects

		
			for (int i=0; i<mMatchData.mRuneTypes.Count; i++) {
			
				SpellListener.GetComponent<SpellCreator>().receivedSpell.Add((int)mMatchData.mRuneTypes [i]); //this adds to the spell creator so spells  	
			}


			if(playerInt==1 && UnityMode==false){ //for showing the last spell casted... doesnt work when using in Unity
				if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}
				SpellListener.GetComponent<SpellCreator>().recievedSpellDesignatorPlayer(playerInt);
					statusText.text = "Player 1 received the spell";

			}
			
			if(playerInt==2 && UnityMode==false ){ //for showing the last spell casted...doesnt work when using in Unity
				if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}
				SpellListener.GetComponent<SpellCreator>().recievedSpellDesignatorPlayer(playerInt);
				statusText.text = "Player 2 received the spell";

			}

			mMatchData.mRuneTypes.Clear(); //clears the list so we can write new items into it...
			mMatchData.Player1Blocks.Clear (); //clears the list so we dont add blocks to an already existing list
			//Health is cleared in the setuphealth method

		} else {
		
		//	statusText.text = "Nothing contained in mMatchData :/";
		}
	}

	public void recordHealth(){
		//this needs to go at the end of the spell recieved 
//
//		mMatchData.playersHealth[0] = (player1.GetComponent<ControlPlayer1Character>().playerHealthCurrent);
//
//			mMatchData.playersHealth[1] = (player1.GetComponent<ControlPlayer1Character>().playerHealthMax);
//
//				mMatchData.playersHealth[2] = (player2.GetComponent<ControlPlayer2Character>().playerHealthCurrent);
//				
//					mMatchData.playersHealth[3] = (player2.GetComponent<ControlPlayer2Character>().playerHealthMax);
//
//

	}

	public void BurntRunesManager(float player1burnt, float player2burnt){

		mMatchData.checkBurnt [0] = player1burnt;
		mMatchData.checkBurnt [1] = player2burnt;
	}

	public void DrownedRunesManager(float player1drowned, float player2drowned){
		
		mMatchData.checkDrowned [0] = player1drowned;
		mMatchData.checkDrowned [1] = player2drowned;
	}

	public void confusionManager(float player1confus, float player2confus){

		mMatchData.checkConfusion [0] = player1confus;
		mMatchData.checkConfusion [1] = player2confus;
		
	}

	public void invisManager(float player1invis, float player2invis){

		player1InvisValue = player1invis;
		player2InvisValue = player2invis;

		mMatchData.checkInvis [0] = player1invis;
		mMatchData.checkInvis [1] = player2invis;

	}

	public void stunManager(float player1Stun, float player2Stun){

		print ("stun manager is running");

		mMatchData.checkStun [0] = player1Stun;
		mMatchData.checkStun [1] = player2Stun;
		
	}

	public void flameDOTManager(float player1FlameDOT, float player2FlameDOT){
		
		print ("stun manager is running");
		
		mMatchData.FlameDOT [0] = player1FlameDOT;
		mMatchData.FlameDOT [1] = player2FlameDOT;
		
	}

	public void shieldManager(float player1Shield, float player2Shield){
		
		print ("shield manager is running");
		
		mMatchData.checkShield [0] = player1Shield;
		mMatchData.checkShield [1] = player2Shield;

	}

	public void setupHealth(){

//		if (mMatchData.playersHealth.Count > 0) {
//
//			if(UnityMode==false){
//
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent = mMatchData.playersHealth [0]; //this stops the health bar from moving...
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthNew = mMatchData.playersHealth [0];
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthMax = mMatchData.playersHealth [1];			
//
//				//match turn based status  = match turn status invited and player = p2 
//			
//			player2.GetComponent<ControlPlayer2Character> ().playerHealthCurrent = mMatchData.playersHealth [2];
//			player2.GetComponent<ControlPlayer2Character> ().playerHealthNew = mMatchData.playersHealth [2];
//			player2.GetComponent<ControlPlayer2Character> ().playerHealthMax = mMatchData.playersHealth [3];
//
//				if(mMatchData.turnCount[0]==1){
//					//this checks if its the players first turn
//					player2.GetComponent<ControlPlayer2Character> ().playerHealthCurrent = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//					player2.GetComponent<ControlPlayer2Character> ().playerHealthMax = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//					player2.GetComponent<ControlPlayer2Character> ().playerHealthNew = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//
//					networkText.text = "this is the 2nd players first turn!";
//
//				}
//
//				player1.GetComponent<ControlPlayer1Character> ().Player1HealthBar.GetComponent<Slider> ().value = mMatchData.playersHealth [0] / mMatchData.playersHealth [1];
//				player1.GetComponent<ControlPlayer1Character> ().Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = mMatchData.playersHealth [0] + "/" + mMatchData.playersHealth [1] + " HP";
//
//				player2.GetComponent<ControlPlayer2Character> ().Player2HealthBar.GetComponent<Slider> ().value = mMatchData.playersHealth [2] / mMatchData.playersHealth [3];
//				player2.GetComponent<ControlPlayer2Character> ().Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = mMatchData.playersHealth [2] + "/" + mMatchData.playersHealth [3] + " HP";
//
//			}
//
//		} 
//
//
//			if (mMatchData.playersHealth.Count == 0) {
//
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthMax = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthNew = (PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod")) *100 + 1000;
//
//			player1.GetComponent<ControlPlayer1Character> ().Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = 
//				player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent.ToString() + "/" + 
//					player1.GetComponent<ControlPlayer1Character> ().playerHealthMax.ToString() + " HP";
//
//
//			mMatchData.playersHealth.Add (player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent);
//			mMatchData.playersHealth.Add (player1.GetComponent<ControlPlayer1Character> ().playerHealthMax);
//				mMatchData.playersHealth.Add (player2.GetComponent<ControlPlayer2Character> ().playerHealthCurrent);
//				mMatchData.playersHealth.Add (player2.GetComponent<ControlPlayer2Character> ().playerHealthMax);
//		}
//
	}

	public void setupStats(){

		if (mMatchData.playerAttributes.Count == 0) {
			//for p1
			mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("WindItemMod"));
			mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("FireItemMod"));
			mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("WaterItemMod"));
			mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("EarthItemMod"));
			mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("atkMod")); //this is general atk mod
				mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("HPItemMod")); //this is the mod for HP from items
				mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("LuckItemMod")); //this is the luck
				mMatchData.playerAttributes.Add(PlayerPrefs.GetFloat ("luckMod")); //this is the luck


			//for p2 put empty stuff because we ddont know his items yet...
				mMatchData.playerAttributes.Add(0);
				mMatchData.playerAttributes.Add(0);
				mMatchData.playerAttributes.Add(0);
				mMatchData.playerAttributes.Add(0);
				mMatchData.playerAttributes.Add(0); //this is general atk mod
				mMatchData.playerAttributes.Add(0); //this is the mod for HP from items
				mMatchData.playerAttributes.Add(0); //this is the luck
				mMatchData.playerAttributes.Add(0); //this is the luck

		
		}

		if (mMatchData.turnCount [0] == 1) {

			mMatchData.playerAttributes[8] = PlayerPrefs.GetFloat ("WindItemMod");
			mMatchData.playerAttributes[9] = PlayerPrefs.GetFloat ("FireItemMod");
			mMatchData.playerAttributes[10] = PlayerPrefs.GetFloat ("WaterItemMod");
			mMatchData.playerAttributes[11] = PlayerPrefs.GetFloat ("EarthItemMod");
			mMatchData.playerAttributes[12] = PlayerPrefs.GetFloat ("atkMod"); //this is general atk mod
				mMatchData.playerAttributes[13] = PlayerPrefs.GetFloat ("HPItemMod"); //this is the mod for HP from items
				mMatchData.playerAttributes[14] = PlayerPrefs.GetFloat ("LuckItemMod"); //this is the luck
				mMatchData.playerAttributes[15] = PlayerPrefs.GetFloat ("luckMod"); //this is the luck

		}

		for (int i=0; i<16; i++) {
			if(i<=7){

				player1AttributeMods[i]=mMatchData.playerAttributes[i];
			}
			if(i>7){
				player2AttributeMods[i-8]=mMatchData.playerAttributes[i];
			}
		}
	}

	public void checkForGear(){

		if (mMatchData.playerGear.Count == 0) { //game has just been established and there is no gear data.
			//player 1
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("head")); //for player 1/2 all items are in this order
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("chest"));
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("arms"));
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("legs"));
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("cape"));
			mMatchData.playerGear.Add(PlayerPrefs.GetInt("Sword"));
				//player 2
				mMatchData.playerGear.Add(0); //for player 1/2 all items are in this order (head)
				mMatchData.playerGear.Add(0); //chest
				mMatchData.playerGear.Add(0); //arms
				mMatchData.playerGear.Add(0); //legs
				mMatchData.playerGear.Add(0); //cape
				mMatchData.playerGear.Add(0); //sword
		}

			if (mMatchData.turnCount [0] == 1) {

			mMatchData.playerGear[6] = PlayerPrefs.GetInt("head");
			mMatchData.playerGear[7] = PlayerPrefs.GetInt("chest");
			mMatchData.playerGear[8] = PlayerPrefs.GetInt("arms");
			mMatchData.playerGear[9] = PlayerPrefs.GetInt("legs");
			mMatchData.playerGear[10] = PlayerPrefs.GetInt("cape");
			mMatchData.playerGear[11] = PlayerPrefs.GetInt("Sword");
			}

//		player1.GetComponent<ClothePlayer> ().wearGearMultiplayer (mMatchData.playerGear);
//		player2.GetComponent<ClothePlayer> ().wearGearMultiplayer (mMatchData.playerGear);


	}

	public void checkForModifiers(){

		if (mMatchData.checkBurnt.Count == 0) {
			mMatchData.checkBurnt.Add(0);
			mMatchData.checkBurnt.Add(0);
		}

		if (mMatchData.checkDrowned.Count == 0) {
			mMatchData.checkDrowned.Add(0);
			mMatchData.checkDrowned.Add(0);
		}

		if (mMatchData.checkConfusion.Count == 0) {

			mMatchData.checkConfusion.Add(0);
			mMatchData.checkConfusion.Add(0);
		}

//THIS IS FOR INVISIBLITY!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (mMatchData.checkInvis.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkInvis.Add(0);
			mMatchData.checkInvis.Add(0);
		}


		if (mMatchData.checkInvis.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkInvis.Add(0);
			mMatchData.checkInvis.Add(0);
		}

		//invisManager (1, 0);//to test if invisstuff works.

		if (mMatchData.checkInvis [0] > 0 ) { //check if player 1 is invis if 1 that means the player is invis
		//player 1 is invis
			mMatchData.checkInvis [0] -=1;

			player1.GetComponent<ControlPlayer1Character>().player1Invis=true; //this makes it so the player doesnt become invis again

			player1.GetComponent<BoxCollider2D>().enabled=false; //this makes it so the player cannot be hit

			if(UnityMode==false){
				GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);
				InvisParticlesClone.transform.parent = player1.transform;

			}

			if(mMatchData.checkInvis[0]==0 
			   && UnityMode==true 
			   && player1.GetComponent<ControlPlayer1Character>().player1Invis==true){
	
				player1.GetComponent<ControlPlayer1Character>().player1Invis=false; //this makes it so the player doesnt become invis again
				player1.GetComponent<ControlPlayer1Character>().player1ReturnToVisible=true;
				player1.GetComponent<BoxCollider2D>().enabled=true; //this makes it so the player cannot be hit

			}

		}

		if (mMatchData.checkInvis [1] > 0 ) { //check if player 1 is invis if 1 that means the player is invis
		//player 2 is invis
			mMatchData.checkInvis [1] -=1;

			player2.GetComponent<ControlPlayer2Character>().player2Invis=true; //this makes it so the player doesnt become invis again

			player2.GetComponent<BoxCollider2D>().enabled=false; //this makes it so the player cannot be hit

			if(UnityMode==false){
				GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (player2.transform.position.x, player2.transform.position.y, 0), Quaternion.identity);
				InvisParticlesClone.transform.parent = player2.transform;
			}

			if(mMatchData.checkInvis[1]==0 
			   && UnityMode==true 
			   && player2.GetComponent<ControlPlayer2Character>().player2Invis==true){

				player2.GetComponent<ControlPlayer2Character>().player2Invis=false; //this makes it so the player doesnt become invis again
				player2.GetComponent<ControlPlayer2Character>().player2ReturnToVisible=true;
				player2.GetComponent<BoxCollider2D>().enabled=true; //this makes it so the player cannot be hit

			}
		}

	//	otherText.text = "Invi" + mMatchData.checkInvis [0].ToString() + "_" + mMatchData.checkInvis [1].ToString();


//END INVISIBLITY!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//***

//THIS IS FOR STUNS -----------------------------------------------------------------------------------
	

		if (mMatchData.checkStun.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkStun.Add(0);
			mMatchData.checkStun.Add(0);
		}

		if (mMatchData.checkStun [0] > 0 ) { //check if player 1 is stunned if > 1 that means the player is stunned
			print ("player 1 is stunned");

			mMatchData.checkStun [0] -= 1;

			if(player2.GetComponent<ControlPlayer2Character>().player2Invis!=true){
			player1.GetComponent<ControlPlayer1Character> ().player1Stun = true;
			player1.GetComponent<Animator>().SetBool("IsStunned",true);
		//	BlockClickMousePanel.SetActive(false);
			}

			if(mMatchData.checkStun[0] ==0 
			   && player1.GetComponent<ControlPlayer1Character> ().player1Stun == true){

				player1.GetComponent<ControlPlayer1Character> ().player1Stun = false;
				player1.GetComponent<Animator>().SetBool("IsStunned",false);
		//		BlockClickMousePanel.SetActive(false);

			}
		}

		if (mMatchData.checkStun [1] > 0 ) { //check if player 1 is stunned if > 1 that means the player is stunned
			mMatchData.checkStun [1] -= 1;
			if(player1.GetComponent<ControlPlayer1Character>().player1Invis!=true){
			player2.GetComponent<ControlPlayer2Character> ().player2Stun = true;
			player2.GetComponent<Animator>().SetBool("IsStunned",true);
		//		BlockClickMousePanel.SetActive(false);

			}

			if(mMatchData.checkStun[1] ==0 
			   && player2.GetComponent<ControlPlayer2Character> ().player2Stun == true){
				
				player2.GetComponent<ControlPlayer2Character> ().player2Stun = false;
				player2.GetComponent<Animator>().SetBool("IsStunned",false);
			//	BlockClickMousePanel.SetActive(false);

			}
		}
//END STUNS -----------------------------------------------------------------------------------
//this is for FLAME DOT

		if (mMatchData.FlameDOT.Count == 0) { //this sets up the Flame DOT list as 0 for both players.
			
			mMatchData.FlameDOT.Add(0);
			mMatchData.FlameDOT.Add(0);
		}

		//flameDOTManager (0,0);

		if (mMatchData.FlameDOT [0] > 0) { //detects if the player should be receiving damage over time

			mMatchData.FlameDOT[0] -=1;

			player1.GetComponent<ControlPlayer1Character>().player1FlameDOT=true;

			if(UnityMode==true){
				player1.GetComponent<ControlPlayer1Character>().justBurned=false;
				
				if(mMatchData.FlameDOT[0]==0){
					
					player1.GetComponent<ControlPlayer1Character>().player1FlameDOT=false;
					
				}
			}			
		}
		
		if (mMatchData.FlameDOT [1] > 0) { //detects if the player should be receiving damage over time
			
			mMatchData.FlameDOT[1] -=1;

			player2.GetComponent<ControlPlayer2Character>().player2FlameDOT=true;

			if(UnityMode==true){
				player2.GetComponent<ControlPlayer2Character>().justBurned=false;
			
				if(mMatchData.FlameDOT[1]==0){

					player2.GetComponent<ControlPlayer2Character>().player2FlameDOT=false;

				}
			
			}
		}

//END FLAME DOT

//CHECK FOR Shield Modifier

		if (mMatchData.checkShield.Count == 0) { //this sets up the Flame DOT list as 0 for both players.
			
			mMatchData.checkShield.Add(0);
			mMatchData.checkShield.Add(0);
		}
		
		//shieldManager (0,1);
		
		if (mMatchData.checkShield [0] > 0) { //detects if the player should be receiving damage over time
			
			mMatchData.checkShield[0] -=1;

			if(UnityMode==false){
			player1.GetComponent<ControlPlayer1Character>().isShielded=true; //this is so it wont add the shield amount again
			player1.GetComponent<Animator>().SetBool("SummonShield", true);
			}
		}
		
		if (mMatchData.checkShield [1] > 0) { //detects if the player should be receiving damage over time
			
			mMatchData.checkShield[1] -=1;

			if(UnityMode==false){
			player2.GetComponent<ControlPlayer2Character>().isShielded=true;
			player2.GetComponent<Animator>().SetBool("SummonShield", true);
			}
		}
	}

	public void InvisCounterCheckP2(){
		//this method is a special condition where both players are invis, and one player is receiving a spell
		//it is placed in this class because this method needs to reference mMatchData and I dont want to have any other class reference mMatchData
	
		if (UnityMode == false) { //makes it so the game doesnt reset the invis when played offline
			
			if (mMatchData.checkInvis [0] == 0 //if p2 is countering back p1
				&& mMatchData.checkInvis [1] == 0
				&& player2.GetComponent<ControlPlayer2Character> ().player2Invis == true
				&& player1.GetComponent<ControlPlayer1Character> ().player1Invis == true) {
				
				player1.GetComponent<ControlPlayer1Character> ().castFireSpell ();
				
				player1.GetComponent<ControlPlayer1Character> ().player1Invis = false; //this makes it so the player doesnt become invis again
				player1.GetComponent<ControlPlayer1Character> ().player1ReturnToVisible = true;
				player1.GetComponent<BoxCollider2D> ().enabled = true; //this makes it so the player cannot be hit
			}
		}
	}
	public void InvisCounterCheckP1(){		
		if(UnityMode==false){

		if (mMatchData.checkInvis [0] == 0 //if p1 is countering back p2
			&& mMatchData.checkInvis [1] == 0
			&& player1.GetComponent<ControlPlayer1Character> ().player1Invis == true
			&& player2.GetComponent<ControlPlayer2Character> ().player2Invis == true) {
				
			player2.GetComponent<ControlPlayer2Character> ().castFireSpell ();
				
			player2.GetComponent<ControlPlayer2Character> ().player2Invis = false; //this makes it so the player doesnt become invis again
			player2.GetComponent<ControlPlayer2Character> ().player2ReturnToVisible = true;
			player2.GetComponent<BoxCollider2D> ().enabled = true; //this makes it so the player cannot be hit
		}
	}
}

	public void setPlayersLastSpell(){

		switch ((int)mMatchData.lastSpellTypeCasted[0]) {
		
		case(0):
			LastSpellPlayer1.GetComponent<Image>().sprite = WindSprite; //wind 
			break;
		case(1):
			LastSpellPlayer1.GetComponent<Image>().sprite = FireSprite; //fire
			break;
		case(2):
			LastSpellPlayer1.GetComponent<Image>().sprite = WaterSprite; //water
			break;
		case(3):
			LastSpellPlayer1.GetComponent<Image>().sprite = EarthSprite; //earth
			break;
		case(4):
			LastSpellPlayer1.GetComponent<Image>().sprite = nullsprite; //earth
			break;
		}

		print ("the last spell casted is type p0 " + (int)mMatchData.lastSpellTypeCasted [0]);

		switch ((int)mMatchData.lastSpellTypeCasted[1]) {

		case(0):
			LastSpellPlayer2.GetComponent<Image>().sprite = WindSprite; //wind 
			break;
		case(1):
			LastSpellPlayer2.GetComponent<Image>().sprite = FireSprite; //wind 
			break;
		case(2):
			LastSpellPlayer2.GetComponent<Image>().sprite = WaterSprite; //wind 
			break;
		case(3):
			LastSpellPlayer2.GetComponent<Image>().sprite = EarthSprite; //wind 
			break;
		case(4):
			LastSpellPlayer2.GetComponent<Image>().sprite = nullsprite; //wind 
			break;
			
		}

		print ("the last spell casted is type p0 " + (int)mMatchData.lastSpellTypeCasted [1]);

	}

	public void checkForConfusionMod(){
		
		if (mMatchData.checkConfusion[0] > 0 && playerInt==1
		    || mMatchData.checkConfusion[0] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkConfusion [0] -= 1;
			
			if(mMatchData.checkConfusion[0]!=0){
				
			//	networkText.text= "ended before conf p1";
				
				createConfusion();
				
			//	networkText.text= "ended after  p1";
			}
			
			if(mMatchData.checkConfusion[0]==0 && mMatchData.checkBurnt[0]==0 && mMatchData.checkDrowned[0]==0){
				ChangeBlockColor();
			//	networkText.text= "block color back to normal! p1";
				
			}
		}
		
		if (mMatchData.checkConfusion[1] > 0 && playerInt==2 
		    || mMatchData.checkConfusion[1] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkConfusion [1] -= 1;
			
			if(mMatchData.checkConfusion[1]!=0){
				
			//	networkText.text= "ended before conf p2";
				
				createConfusion();
				
			//	networkText.text= "ended after  p2";
				
			}
			
			if(mMatchData.checkConfusion[1]==0 && mMatchData.checkBurnt[1]==0 && mMatchData.checkDrowned[1]==0){
				ChangeBlockColor();
			//	networkText.text= "block color back to normal! p2";
				
			}
		}
		
	}

	public void checkForBurntMod(){

		if (mMatchData.checkBurnt[0] > 0 && playerInt==1
		    || mMatchData.checkBurnt[0] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkBurnt [0] -= 1;
			if(mMatchData.checkBurnt[0]!=0){
			//	networkText.text= "ended before BurntR p1";
				createBurntRunes();
			//	networkText.text= "ended after BurntR p1";
			}
			if(mMatchData.checkBurnt[0]==0 && mMatchData.checkDrowned[0]==0 && mMatchData.checkConfusion[0]==0){
				ChangeBlockColor();
			//	networkText.text= "burnt bloks back to normal! p1";
			}
		}

		if (mMatchData.checkBurnt[1] > 0 && playerInt==2 
		    || mMatchData.checkBurnt[1] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkBurnt [1] -= 1;
			if(mMatchData.checkBurnt[1]!=0){
			//	networkText.text= "ended before BurntR p2";
				createBurntRunes();
			//	networkText.text= "ended after BurntR p2";
			}
			if(mMatchData.checkBurnt[1]==0 && mMatchData.checkDrowned[1]==0 && mMatchData.checkConfusion[1]==0){
				ChangeBlockColor();
			//	networkText.text= "burnt bloks back to normal! p2";
			}
		}
	}

	public void checkForDrownedMod(){
		if (mMatchData.checkDrowned[0] > 0 && playerInt==1
		    || mMatchData.checkDrowned[0] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkDrowned [0] -= 1;
			if(mMatchData.checkDrowned[0]!=0){
				networkText.text= "ended before drown p1";
				createDrownedRunes();
				networkText.text= "ended after drown p1";
			}
			if(mMatchData.checkDrowned[0]==0 && mMatchData.checkBurnt[0]==0 && mMatchData.checkConfusion[0]==0){
				ChangeBlockColor();
				networkText.text= "drown bloks back to normal! p1";
			}
		}
		if (mMatchData.checkDrowned[1] > 0 && playerInt==2 
		    || mMatchData.checkDrowned[1] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkDrowned [1] -= 1;
			if(mMatchData.checkDrowned[1]!=0){
				networkText.text= "ended before drown p1";
				createDrownedRunes();
				networkText.text= "ended after drown p1";
			}
			if(mMatchData.checkDrowned[1]==0 && mMatchData.checkBurnt[1]==0 && mMatchData.checkConfusion[1]==0){
				ChangeBlockColor();
				networkText.text= "drown bloks back to normal! p2";
			}
		}
	}

	public void openSpellGuideWindow(){
		if (spellDescriptionWindow.activeSelf == true) {
			spellDescriptionWindow.SetActive(false);
			
		}

		if (SpellGuideWindow.activeSelf == false && pressCounter==0) {
			SpellGuideWindow.SetActive(true);
		} 

		pressCounter+=1;
		if (pressCounter == 2) {
			SpellGuideWindow.SetActive(false);
			pressCounter=0;
		}
	}

	public void WhosTurn(bool canplay, string playerName){

		networkText.text = "Whos Turn Called";

		if (player1.GetComponent<ControlPlayer1Character> ().playerDead == true || player2.GetComponent<ControlPlayer2Character> ().playerDead == true) {
		//	networkText.text = "a player is ded!";

		}


		if (UnityMode == true && player1.GetComponent<ControlPlayer1Character>().playerDead==false
		    && player2.GetComponent<ControlPlayer2Character>().playerDead==false) {
			if(playerInt==1){
				WhosTurnPanel.SetActive(true);
				WhosTurnPanel.GetComponent<TextMeshProUGUI>().text = "Player 1's Turn";

			}
			if(playerInt==2){
				WhosTurnPanel.SetActive(true);
				WhosTurnPanel.GetComponent<TextMeshProUGUI>().text = "Player 2's Turn";

			}
		}

		if (player1.GetComponent<ControlPlayer1Character> ().playerDead == true && UnityMode == true) {
			print ("player 1 is dead!!");
			WhoWon.GetComponent<TextMeshProUGUI>().text = "Player 2 Wins!";
			if(BlockClickMousePanel.activeSelf==true){
				BlockClickMousePanel.SetActive(false); //if the block mouse pane is already up
			}
			EndMatchPanel.SetActive(true);
		}

		if (player2.GetComponent<ControlPlayer2Character> ().playerDead == true && UnityMode == true) {
			print ("player 2 is dead!!");
			WhoWon.GetComponent<TextMeshProUGUI>().text = "Player 1 Wins!";
			if(BlockClickMousePanel.activeSelf==true){
				BlockClickMousePanel.SetActive(false); //if the block mouse pane is already up
			}
			EndMatchPanel.SetActive(true);
		}

			if (player1.GetComponent<ControlPlayer1Character> ().playerDead == true && UnityMode == false) { //end match condition (i think it doesnt detect a player as dead in multiplayer mode)
					//print ("player 1 is dead!!");
			WhoWon.GetComponent<TextMeshProUGUI>().text = "Player 2 Wins!";

			networkText.text = "Someone Dead Detected";

			
//			FinishMatch();

			networkText.text = "Match Finished Called";

			if(BlockClickMousePanel.activeSelf==true){
				BlockClickMousePanel.SetActive(false); //if the block mouse pane is already up
			}
			
			EndMatchPanel.SetActive(true);

			networkText.text = "End Match Panel Called";

		}
				
			if (player2.GetComponent<ControlPlayer2Character> ().playerDead == true && UnityMode == false) {
					//print ("player 2 is dead!!");
			WhoWon.GetComponent<TextMeshProUGUI>().text = "Player 1 Wins!";

			networkText.text = "Someone Dead Detected";

			
		//	FinishMatch();

			networkText.text = "Match Finished Called";

			if(BlockClickMousePanel.activeSelf==true){
				BlockClickMousePanel.SetActive(false); //if the block mouse pane is already up
			}
			
			EndMatchPanel.SetActive(true);

			networkText.text = "End Match Panel Called";

		}


		if (UnityMode == false && canplay == true) {
			WhosTurnPanel.SetActive(true);
			WhosTurnPanel.GetComponent<TextMeshProUGUI>().text = playerName + "'s Turn";

		}

		if (UnityMode == false && canplay == false) {
			WhosTurnPanel.SetActive(true);
			WhosTurnPanel.GetComponent<TextMeshProUGUI>().text = playerName + "'s Turn";

		}

	}

//	string GetAdversaryParticipantId() {
//		foreach (Participant p in mMatch.Participants) {
//			if (!p.ParticipantId.Equals(mMatch.SelfParticipantId)) {
//				return p.ParticipantId;
//			}
//		}
//		Debug.LogError("Match has no adversary (bug)");
//		return null;
//	}
	
//	void FinishMatch() {
//
//		Analytics.CustomEvent("MatchFinished", new Dictionary<string, object>
//		                      {
//			{"MatchFinished", 0}
//			
//		});
//
//		Analytics.CustomEvent("MatchFinished", new Dictionary<string, object>
//		                      {
//			{"MatchFinished", 1}
//			
//		});
//
//		bool winnerIsMe = false;
//
////		if (mMatchData.playersHealth [0] <= 0 && mMatch.SelfParticipantId == "p_1") { //am I player 1 and my health is less than zero
////
////			winnerIsMe = false;
////
////		}
////
////		if (mMatchData.playersHealth [2] <= 0 && mMatch.SelfParticipantId == "p_1") { //am I player 1 and my health is less than zero
////			
////			winnerIsMe = true;
////			
////		}
//
//		// define the match's outcome
//		MatchOutcome outcome = new MatchOutcome();
//
//		//this is if the other playerwins
//
//		outcome.SetParticipantResult(mMatch.SelfParticipantId,
//		                             winnerIsMe ? MatchOutcome.ParticipantResult.Win : MatchOutcome.ParticipantResult.Loss);
//		outcome.SetParticipantResult(GetAdversaryParticipantId(),
//		                             winnerIsMe ? MatchOutcome.ParticipantResult.Loss : MatchOutcome.ParticipantResult.Win);
//		
//		// finish the match
//	//	SetStandBy("Sending...");
//		//SetStandBy("Sending...");
//		PlayGamesPlatform.Instance.TurnBased.Finish(mMatch, mMatchData.ToBytes(),
//		                                            outcome, (bool success) => {
//		//	EndStandBy();
//		//	mFinalMessage = success ? (winnerIsMe ? "YOU WON!" : "YOU LOST!") :
////				"ERROR finishing match.";
//		});
//	}

	public void createConfusion(){
		/*
		for (int i=0; i<meteorList.Count; i++) {


			if(i>=0 && i<4){
			if(i%2 == 0){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
				meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
				meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=4 && i<8){
				if(i%2 == 1){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=8 && i<12){
				if(i%2 == 0){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=12 && i<16){
				if(i%2 == 1){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}
		}
		*/
	//	networkText.text = "changed conf";
	}

	public void createBurntRunes(){
		/*
		for (int i=0; i<meteorList.Count; i++) {
			if(i==0 || i==5 || i==10 || i==15){
					meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(5);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
			}else{
				meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
				meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
				meteorList[i].LockColor=true;
			}
		}
		*/
	//	networkText.text = "burnt runes";
	}

	public void createDrownedRunes(){
		/*
		for (int i=0; i<meteorList.Count; i++) {
			if(i==3 || i==6 || i==9 || i==12){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
				meteorList[i].GetComponent<MeteorCode>().createRuneColor(5);
				meteorList[i].GetComponent<MeteorCode>().LockColor=true;
			}
			else{
				meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
				meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
				meteorList[i].LockColor=true;

			}
		}
		*/
//		networkText.text = "drownd runes";
	}

	public void damageP1Notification(float damage){
		
		p1DamageAmount.GetComponent<TextMeshProUGUI>().text = "- " + damage.ToString() + " HP"; //needs to be added to every spell
		p1DamageAmount.SetActive(true);
	}

	public void damageP2Notification(float damage){
		
		p2DamageAmount.GetComponent<TextMeshProUGUI>().text = "- " + damage.ToString() + " HP"; //needs to be added to every spell
		p2DamageAmount.SetActive(true);
	}

	public void setupCrit(){

		if (mMatchData.didCrit.Count == 0) {

			mMatchData.didCrit.Add(0);
			mMatchData.didCrit.Add(0);

		}
	}

	public bool criticalHit(){

		int CritDice = Random.Range (0, 100);

		if (playerInt == 1) {

			if (CritDice <= (mMatchData.playerAttributes[6]+mMatchData.playerAttributes[7])) {

			//	critTextObj.SetActive (true);	

				mMatchData.didCrit[0] = 1;
				mMatchData.didCrit[1] = 0;

				return true;

			}

		}

		if (playerInt == 2) {

			if (CritDice <= (mMatchData.playerAttributes[14]+mMatchData.playerAttributes[15])) {
				
			//	critTextObj.SetActive (true);	

				mMatchData.didCrit[0] = 0;
				mMatchData.didCrit[1] = 1;


				return true;
								
			}
		}

		return false;
	}

	public int recieveCrit(int player){

		if (player == 1) { //this is the player you are receiving the crit from
			//1 returns a crit 0 returns nothing

			if(mMatchData.didCrit[0]==1){
				mMatchData.didCrit[0] = 0 ; //resets but returns a crit value
				mMatchData.didCrit[1] = 0 ; //resets but returns a crit value
				return 1 ;
			}

		}

		if (player == 2 ) {

			if(mMatchData.didCrit[1]==1){ //did player 2 crit on his turn?
				mMatchData.didCrit[0] = 0 ; //resets but returns a crit value
				mMatchData.didCrit[1] = 0; //resets but returns a crit value
				return 1;

			}

		}

		return 0;
	}

	public void SurrenderPlayer(){

		if (playerInt == 1) {

			mMatchData.playersHealth[0] = 0;
			nextTurnBool = true;
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
		}

		if (playerInt == 2) {

			mMatchData.playersHealth[2] = 0;
			nextTurnBool = true;
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();


		}
	}

	public void EnemyBurned(){
		
		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.red, Color.red, Color.yellow, Color.yellow);
		//goes top left/right //bottom left/right
		itemObj.GetComponent<TextMeshProUGUI> ().text = "Enemy Burned!";
		itemObj.SetActive (true);
		
		//PersistantSoundmanager.NewItemFound ();
	}
	
	public void EnemyStunned(){
		
		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.blue, Color.blue, Color.white, Color.white);
		//goes top left/right //bottom left/right
		
		itemObj.GetComponent<TextMeshProUGUI> ().text = "Enemy Stunned!";
		itemObj.SetActive (true);
		
		//PersistantSoundmanager.NewItemFound ();
	}
	
	public void CounterAtkRdy(){
		
		if (itemObj.activeSelf == false) {
			
			itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.grey, Color.grey, Color.white, Color.white);
			//goes top left/right //bottom left/right
			
			itemObj.GetComponent<TextMeshProUGUI> ().text = "Counter Attack Ready!";
			itemObj.SetActive (true);
			
		}
		//PersistantSoundmanager.NewItemFound ();
	}
}
