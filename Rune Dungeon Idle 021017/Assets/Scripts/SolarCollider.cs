﻿using UnityEngine;
using System.Collections;

public class SolarCollider : MonoBehaviour {

	public GameObject ExplosionForce;
	// Use this for initialization
	void Start () {
	
	}
	
	
	void OnTriggerEnter2D (Collider2D player) {
		
		
		
		
		
		if (player.gameObject.tag == "Enemy" || player.gameObject.tag == "Player2" && Camera.main.GetComponent<CampaignGameOrganizer> () != null) { //to damage the enemy during campaign // when marked as P2
			
			damageEnemy(player);
		}
		
	}
	
	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method
		
		
		float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball") 
		                              + PlayerPrefs.GetFloat ("FireItemMod") 
		                              + PlayerPrefs.GetFloat ("EarthItemMod") 
		                              + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
		
		
		
		if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
			
			calculatedHealthLoss = 2*calculatedHealthLoss;
			print("CRITS!!!!!!!!!");
			
		}
		
		player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
		
		Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);

		
		
		if(calculatedHealthLoss >= player.GetComponent<ControlEnemy>().playerHealthCurrent){
			
			player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
			player.gameObject.GetComponent<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll
			player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll();
			
			ExplosionForce = Instantiate (ExplosionForce, new Vector3 (player.GetComponent<EnemyRagdoll>().LeftLegUpper.transform.position.x,
			                                                           player.GetComponent<EnemyRagdoll>().Torso.transform.position.y, 0), Quaternion.identity) as GameObject;
			
		}
		
		this.GetComponent<BoxCollider2D>().enabled=false;
		
	}
}
