﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ShopAndEquipScript : MonoBehaviour {


	public List<ItemInfo> ItemsToActivate = new List<ItemInfo>();


	void Start () {
	
		StartCoroutine ("ActivateItems"); 



	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public IEnumerator ActivateItems(){
		
		while (true) {

			for (int i = 0; i<ItemsToActivate.Count; i++) {
				
				if(ItemsToActivate[i].enabled == false){
				ItemsToActivate[i].enabled=true;
				}
				yield return new WaitForSeconds(0.025f);

			}

			yield break;
		}

	}
}
