﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;


public class ItemInfo : MonoBehaviour {

	public Dictionary<string, int> GearDict = new Dictionary<string, int>();

	public Dictionary<string, int> GearAttributes = new Dictionary<string, int>();

	public string itemTitle;

	public GameObject ItemCostLabelObject;
	public TextMeshProUGUI ItemCostLabel;

	public GameObject NewItemSparkle;
	public GameObject FacebookItemSparkle;
	public GameObject NewSparklingItemHolder;
	public bool sparklyFacebook = false;

	public GameObject ArmorImage;
	public GameObject ArmorBackDrop;
	
	public bool head,chest,arms,legs,cape,sword = false;

	public bool isFacebookItem = false;

	public bool owned = false;

//	public bool isEquipped = false;

	public int gearLevel;

	public int FireMod,WaterMod,WindMod,EarthMod, HpMod, LuckMod;

	public int itemCost;

	public bool newItemAvailable = false;

	void Start () {

//		print ("the player pref of this object is " + PlayerPrefs.GetInt (this.gameObject.GetComponent<Image> ().sprite.name.ToString ()));

		if (PlayerPrefs.GetInt ("LV1_Torso") == 0) {
			equipDefaults();
		}

		PlayerPrefs.SetInt ("LV1_Torso", 1);
		PlayerPrefs.SetInt ("LV1_UpperRightArm", 1);
		PlayerPrefs.SetInt ("ShortBrownHair", 1);
		PlayerPrefs.SetInt ("LV1_RightLegLower", 1);
		PlayerPrefs.SetInt ("StartingCape1", 1); 
		PlayerPrefs.SetInt ("sword", 1); //These show that these are owned...

		if (isFacebookItem == true && sparklyFacebook == false) {

			GameObject FacebookSparkly = (GameObject)Instantiate (FacebookItemSparkle, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);
			FacebookSparkly.transform.parent = this.transform; 
			sparklyFacebook=true; //so it doesnt repeat the instantiate
		}

		if (PlayerPrefs.GetInt (this.gameObject.GetComponent<Image> ().sprite.name.ToString ()) == 0) {

			Color temp = ArmorImage.GetComponent<Image> ().color;
		//	Color temp = ArmorBackDrop.GetComponent<Image> ().color;
			temp.a = 0.75f;
			temp.r = .5f;
			temp.g = .5f;
			temp.b = .5f;
			ArmorImage.GetComponent<Image> ().color = temp;
			ArmorBackDrop.GetComponent<Image> ().color = temp;

			if(ItemCostLabelObject!= null){
				ItemCostLabelObject.SetActive(true);
				if(isFacebookItem==false){
				ItemCostLabel.text = itemCost.ToString();
				}
			}

		} else if (PlayerPrefs.GetInt (this.gameObject.GetComponent<Image> ().sprite.name.ToString ()) == 2) {

			GameObject sparkly = (GameObject)Instantiate (NewItemSparkle, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

		//	sparkly.transform.position = new Vector3(0,0,0);

			if(ItemCostLabelObject!= null){
				ItemCostLabelObject.SetActive(false);
			}

			newItemAvailable = true;

			sparkly.transform.parent = this.transform; 

			NewSparklingItemHolder = sparkly;
//			GameObject NewText = (GameObject)Instantiate (NewItemText, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);
			
	//		NewText.transform.parent = this.transform;

			owned = true;

		} else {

			owned = true;
		}

	if (head == true) {
			GearDict.Add("head",gearLevel);
		}
			if (chest == true) {
				GearDict.Add("chest",gearLevel);
			}
				if (arms == true) {
					GearDict.Add("arms",gearLevel);
				}
					if (legs == true) {
						GearDict.Add("legs",gearLevel);
					}
						if (cape == true) {
							GearDict.Add("cape",gearLevel);
						}
							if (sword == true) {
								GearDict.Add("Sword",gearLevel);
							}
	
		checkEquipped(); //checks if the equip is equipped and changes color if it is
	
	
	}

	public void restoreColor(){

		Color temp = ArmorImage.GetComponent<Image> ().color;
		//	Color temp = ArmorBackDrop.GetComponent<Image> ().color;
		temp.a = 1;
		temp.r = 1f;
		temp.g = 1f;
		temp.b = 1f;
		ArmorImage.GetComponent<Image> ().color = temp;
		ArmorBackDrop.GetComponent<Image> ().color = temp;

		owned = true;

	}

	public void checkEquipped(){

		if (head == true) {

			if(PlayerPrefs.GetInt("head")==gearLevel){

				equipMsg();
				equipColor();
			}else if(owned==true){
				
				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}


		}
		if (chest == true) {

			if(PlayerPrefs.GetInt("chest")==gearLevel){
				
				equipMsg();
				equipColor();

			}else if(owned==true){
				
				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}
		}
		if (arms == true) {

			if(PlayerPrefs.GetInt("arms")==gearLevel){
				
				equipMsg();
				equipColor();

			}else if(owned==true){
				
				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}
		}
		if (legs == true) {

			if(PlayerPrefs.GetInt("legs")==gearLevel){
				
				equipMsg();
				equipColor();

			}else if(owned==true){
				
				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}
		}
		if (cape == true) {

			if(PlayerPrefs.GetInt("cape")==gearLevel){
				
				equipMsg();
				equipColor();

			}else if(owned==true){

				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}


		}
		if (sword == true) {
			
			if(PlayerPrefs.GetInt("Sword")==gearLevel){
				
				equipMsg();
				equipColor();
				
			}else if(owned==true){
				
				restoreColor();
				if(ItemCostLabelObject!= null){
					ItemCostLabelObject.SetActive(false);
				}
			}
			
			
		}
	}

	public void equipMsg(){
//		print( this.gameObject.name + " is equipped");

	}

	public void equipColor(){

		Color temp = ArmorImage.GetComponent<Image> ().color;
		//	Color temp = ArmorBackDrop.GetComponent<Image> ().color;
		temp.a = 255f;
		temp.r = 0;
		temp.g = 255f;
		temp.b = 0f;
		ArmorBackDrop.GetComponent<Image> ().color = temp;

		if(ItemCostLabelObject!= null){
			ItemCostLabelObject.SetActive(false);
		}


	}

	public void equipDefaults(){

			PlayerPrefs.SetInt("head",1);

			PlayerPrefs.SetInt("chest",1);		

			PlayerPrefs.SetInt("arms",1);

			PlayerPrefs.SetInt("legs",1);

			PlayerPrefs.SetInt("cape",1);

			PlayerPrefs.SetInt("Sword",1);

	}

		

}


