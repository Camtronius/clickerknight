﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GuildHall : MonoBehaviour {

    public List<Sprite> loadedSprites = new List<Sprite>();
    public List<Image> guildHallImages = new List<Image>();
    private List<Dictionary<string, object>> gpLvs = new List<Dictionary<string, object>>();

    public Sprite blankTransparentSprite;
    public GuildHandler guildHandler;

    public Slider expToNextSlider;
    public TextMeshProUGUI sliderText;

    private int currentLevel = 1;
    private double gpToNextLv;

    //0 = gate
    //1 = tower near
    //2 = wall near
    //3 = tower behind gate
    //4 = wall back
    //5 = back tower far
    //6 = back tower near
    //7 = far tower tall
    //8 = nearest tower tall

    // Use this for initialization
    void Start () {

        methodsToCallOnStart();

    }

    public void methodsToCallOnStart()
    {
        if (loadedSprites.Count == 0)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("GuildHall");

            foreach (Sprite sprite in sprites)
            {
                loadedSprites.Add(sprite);
                //Debug.Log(sprite.name);
            }
        }

        startingTransparency();

        loadGPData();

        loadCastleLevel();

        assignHallSprite();

        updateSlider();
    }

    public void startingTransparency()
    {
        for (int i = 0; i < guildHallImages.Count; i++)
        {
            if (i > 0) //the gate is always there
            {
                guildHallImages[i].sprite = blankTransparentSprite;
            }
        }
    }

    private void loadGPData()
    {
        if (gpLvs.Count == 0)
        {
            gpLvs = CSVReader.Read("GP");//, PlayerPrefs.GetInt("CurrentLevel"), PlayerPrefs.GetInt("CurrentLevel") + 35);
        }
    }

    public void loadCastleLevel()
    {
        //check what level we are based on total exp gained

        //check if we are at max lv
        if (checkMaxCastleLv() == false)
        {

            for (int i = 0; i < gpLvs.Count; i++)
            {
                if (guildHandler.getGuildPoints() >= (double)gpLvs[i]["TOTGPLV"])
                {
                    continue;
                }
                else
                {
                    Debug.Log("our current lv is " + i);
                    //find difference from here to next lv.
                    gpToNextLv = differenceToNextLv(guildHandler.getGuildPoints(), (double)gpLvs[i]["TOTGPLV"]);
                    Debug.Log("xp to next is " + gpToNextLv);
                    //sets the level of the castle so we can iterate through the building pieces
                    currentLevel = i;
                    break;
                }
            }

        }
        else
        {
            currentLevel = gpLvs.Count; //this is the max level which is 27 i believe
            //make it so the exp bar is full and we cant go to next lv
        }

    }

    private bool checkMaxCastleLv()
    {
        if (guildHandler.getGuildPoints() >= (double)gpLvs[gpLvs.Count - 1]["TOTGPLV"])
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public double differenceToNextLv(double smallNum, double largeNum)
    {
        double difference = largeNum - smallNum;

        return difference;
    }

    public void assignHallSprite()
    {

        //there are 3 tiers of sprites
        //8 items per castle
        //=24 sprites total
        //player earns 1 pt per day, total of 5 points/day for a full guild. Wouldnt expect all guilds to be completely full all the time so maybe 8pts/upgrade

        for (int i = 0; i < currentLevel; i++) //goes from 0-26, skip one because they will start with a gate...
        {

            if (i > 17)
            {
                guildHallImages[i - 18].sprite = loadedSprites[i];

            }
            else if (i > 8)
            {
                guildHallImages[i - 9].sprite = loadedSprites[i];

            }
            else
            {
                guildHallImages[i].sprite = loadedSprites[i];

            }
        }
    }

    public void updateSlider()
    {
        expToNextSlider.value = (float)(1-gpToNextLv/((double)gpLvs[currentLevel]["TOTGPLV"]-(double)gpLvs[currentLevel-1]["TOTGPLV"])) ;
        sliderText.text = "GP Till Next Upgrade: " + gpToNextLv.ToString();

    }

    public void testCastleGrowth()
    {
        if (currentLevel < loadedSprites.Count)
            currentLevel += 1;

        assignHallSprite();
    }

    public void getGuildHallPoints()
    {
        //this will be loaded from the guild handler script
    }

}
