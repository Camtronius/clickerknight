﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour {

	public List<Button> buttonList = new List<Button> (); //for disabling/enabling for tutorial
	public List<Sprite> wizardSprites = new List<Sprite>();
	public Image wizardSprite;
	public Image wizardHand;

	// Use this for initialization
	public TextMeshProUGUI tutorialText;
	public Animator tutAnimator; 
	public GameObject pointerFinger;
	public GameObject tapToContinueObj;
	public GameObject tutorialTextBox;

	public bool enableContinue = false; //so the player can click to go to next step of tut or cancel it.
	public bool twoPartTutorial = false; //if there is a 2 part tutorial

	public bool partTwoTutorialComp = false; //this is called when the 2nd tutorial part has completed
	public bool explainGoldExp = false; //this is called when the 2nd tutorial part has completed
	public bool clickOnMonsterTut = false; //this is for the clicking on monster to defeat it tutorial
	public bool findItemTut = false; //this is for the clicking on monster to defeat it tutorial
	public bool equipTap = false; //this is for the clicking on monster to defeat it tutorial
	public bool wpnMenuTap = false; //this is for the clicking on monster to defeat it tutorial
	public bool levelUpTutorial = false; //this is for the clicking on monster to defeat it tutorial
	public bool explainStats = false; //this is for the clicking on monster to defeat it tutorial
	public bool talentsTutorial = false;//for having a talent tutorial
	public bool unlockLevelTut = false; 
	public bool bossTut = false;
	public bool prestigeTutorial = false;
	public bool potionTutorial = false;


	public bool tutorialComplete = false;//this will end the current tutorial



	public ScrollRect wpnMenuScrollRect;
	public ScrollRect worldMenuScrollRect;




	public string secondThingToSay { get; set; }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TutPartOne(string thingToSay){ //for the first step of any tutorial

		tapToContinueObj.SetActive(false);

		tutorialText.text = thingToSay;

		if (talentsTutorial == true || explainStats==true) {
			wizardHand.sprite = wizardSprites [4];
			wizardHand.gameObject.SetActive (true);
		}

		if (potionTutorial == true) {
			wizardHand.sprite = wizardSprites [5];
			wizardHand.gameObject.SetActive (true);
		}

		this.gameObject.SetActive (true);

		Camera.main.GetComponent<MainMenu> ().windowOpen = true;
	}

	public void TutPartTwo(){ //for the 2nd step of any tutorial
	
		tapToContinueObj.SetActive(false);

		tutorialText.text = secondThingToSay;

		tutAnimator.SetBool ("TutorialStep2",true);

		if (talentsTutorial == true || explainStats==true) {
			wizardHand.sprite = wizardSprites [4];
			wizardHand.gameObject.SetActive (true);
		}

		if (potionTutorial == true) {
			wizardHand.sprite = wizardSprites [5];
			wizardHand.gameObject.SetActive (true);
		}

		Camera.main.GetComponent<MainMenu> ().windowOpen = true;

	}

	public void partTwoTutorialComplete(){ //completing the 2nd step
		partTwoTutorialComp = true;
	}

	public void enableClickNextToCont(){ //makes it so you can click to the next step of a tut

		tutAnimator.SetBool ("TapToContinue",true);
		tapToContinueObj.SetActive(true);

		enableContinue = true;
	
	}

	public void enableTapOnMonster(){ //this is a specific finger tap instruction

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;

		tutAnimator.SetBool ("MonsterTap",true);

		Camera.main.GetComponent<MainMenu> ().windowOpen = false;

		disableButtons (buttonList[11]);
	}

	public void enableTapOnEquip(){ //this is a specific finger tap instruction

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;

		tutAnimator.SetBool ("EquipFinger",true);

        disableButtons(buttonList[13]);
	}

	public void enableTapOnStats(){ //this is a specific finger tap instruction
		
		tutAnimator.SetBool ("EquipFinger",false); //for some reason equipfinger = true, so im just going to set it false so i dont have to find where it does this. only happens when the chest is found before hero lv's up
	
		wizardHand.gameObject.SetActive (false); //this remains on some times

		tutAnimator.SetBool ("StatsFinger",true);

	}

	public void enableTapTalentTabs(){ //this is a specific finger tap instruction


		wizardHand.gameObject.SetActive (false); //this remains on some times

		tutAnimator.SetBool ("TalentFingerTap",true); //researchYesNo turns this off when you click on another tab
		disableButtons (buttonList[11]);

	}

	public void enableStatsOkButton(){ //this is a specific finger tap instruction

		//tutAnimator.SetBool ("StatsFinger",false);

		tutAnimator.SetBool ("FingerTapStatsOk",true);

	}

	public void goldExpExplanation(){ //specific expplaination of gold and exp for a tutorial part 2

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;

		tutorialText.text = secondThingToSay;

		tutAnimator.SetBool ("WpnUpgradeExplain",true);	

		wpnMenuTap = true; //this is so we can go to the next part of the menu

		enableContinue = false;//this needs to be enable to allow the player to continue

		Camera.main.GetComponent<MainMenu> ().windowOpen = true;


	}

	public void pointAtWpnUpgrade(){

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;

		disableButtons(buttonList [12]);

		tutAnimator.SetBool ("PointAtWpn",true);	
		wpnMenuScrollRect.enabled = false;

	}

	public void pointAtWindowClose(){

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;


		tutAnimator.SetBool ("TapCloseMenu",true);	
		disableButtons (buttonList[11]);
	}

	public void enableTapOnLevelMenu (){

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;


		tutAnimator.SetBool ("UnlockLevelTap",true);

	}

	public void enableTapOnWorld (){

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;


		tutAnimator.SetBool ("UnlockWorldTap",true);

	}

	public void enableTapOnPotions (){

		tutorialTextBox.GetComponent<Image> ().raycastTarget = false;


		wizardHand.gameObject.SetActive (false); //this remains on some times

		tutAnimator.SetBool ("PotionTap",true);

	}

	public void enableTapOnPrestige (){


		tutAnimator.SetBool ("PrestigeButtonTap",true);
		//disable buttons? //this is done in another script... enemyragdoll when boss dies...
	}


	public void tutorialCompleted(){ //ends the current tutorial explanation

		tutorialComplete = true;
	}

	public void hasClicked(){

		if (twoPartTutorial == true && enableContinue == true && unlockLevelTut==false) { //if you are going to the 2nd step. level up tutorial has an intermediate step

			TutPartTwo ();

		} 

		if(partTwoTutorialComp == true && enableContinue == true && clickOnMonsterTut==true){ //if you completed the second step and there is nothing afterwards

			//this.gameObject.SetActive (false);

			enableTapOnMonster (); //this plays the tapping on monster anim

		}

		if(partTwoTutorialComp == true && explainGoldExp==true && wpnMenuTap==false){ //if you completed the second step and there is nothing afterwards

			//this.gameObject.SetActive (false);
			tutAnimator.SetBool ("MonsterTap",false); //to remove the finger anim

			goldExpExplanation (); //this plays the tapping on monster anim

		}

		if (wpnMenuTap == true) {

			tutAnimator.SetBool ("TapOnWeapon",true); //to remove the finger anim
			disableButtons(buttonList [2]);
		}
			
			
		if(partTwoTutorialComp == true && enableContinue == true && equipTap==true ){ //if you completed the second step and there is nothing afterwards

			enableTapOnEquip (); // a check runs in the inventory script so that when the item is equipped or sold the tutorial will automatically end. The AutoSell script holds the code for the prompt

		}

		if(partTwoTutorialComp == true && enableContinue == true && talentsTutorial==true ){ //if you completed the second step and there is nothing afterwards

			enableTapTalentTabs (); // taps on the talent tabs so the player explores other talents

		}

		if (partTwoTutorialComp == true && levelUpTutorial == true) {

			enableTapOnStats ();
		}


		if(partTwoTutorialComp == false && enableContinue == true && unlockLevelTut==true){ //tutorial start has been started, but the 2nd tutorial explanation hasnt started yet.

			enableTapOnLevelMenu ();
		}

		if(partTwoTutorialComp == true && enableContinue == true && unlockLevelTut==true){ //tutorial start has been started, but the 2nd tutorial explanation hasnt started yet.

			enableTapOnWorld ();
		}

		if(partTwoTutorialComp == true && enableContinue == true && prestigeTutorial==true){ //tutorial start has been started, but the 2nd tutorial explanation hasnt started yet.

			enableTapOnPrestige ();
		}

		if(partTwoTutorialComp == true && enableContinue == true && potionTutorial==true){ //tutorial start has been started, but the 2nd tutorial explanation hasnt started yet.

			enableTapOnPotions ();
		}

		if(partTwoTutorialComp == true && enableContinue == true && bossTut==true){ //tutorial start has been started, but the 2nd tutorial explanation hasnt started yet.

			refreshAllVariables ();
			Camera.main.GetComponent<MainMenu> ().windowOpen = false; //dont want to include this in here, because some windows are left open after the tutorial
		}

		if (tutorialComplete == true) {

			refreshAllVariables ();
		}

		tutAnimator.SetBool ("TapToContinue",false); //turns this off after each time you click

		enableContinue = false;//this needs to be enable to allow the player to continue
	}

	public void refreshAllVariables(){

		restoreButtons ();

		enableContinue = false; //so the player can click to go to next step of tut or cancel it.
		twoPartTutorial = false; //if there is a 2 part tutorial

		partTwoTutorialComp = false; //this is called when the 2nd tutorial part has completed
		explainGoldExp = false; //this is called when the 2nd tutorial part has completed
		clickOnMonsterTut = false; //this is for the clicking on monster to defeat it tutorial
		findItemTut = false; //this is for the clicking on monster to defeat it tutorial
		equipTap = false; //this is for the clicking on monster to defeat it tutorial
		wpnMenuTap = false; //this is for the clicking on monster to defeat it tutorial
		levelUpTutorial = false; //this is for the clicking on monster to defeat it tutorial
		explainStats = false; //this is for the clicking on monster to defeat it tutorial
		talentsTutorial = false;//for having a talent tutorial
		tutorialComplete = false;//this will end the current tutorial
		unlockLevelTut = false; 
		prestigeTutorial = false;
		potionTutorial = false;
		bossTut = false;


		tutAnimator.SetBool ("TutorialStep2",false);
		tutAnimator.SetBool ("TapToContinue",false);
		tutAnimator.SetBool ("MonsterTap",false);
		tutAnimator.SetBool ("GoldExpExplain",false);		
		tutAnimator.SetBool ("GoldExpBlink",false);
		tutAnimator.SetBool ("EquipFinger",false);
		tutAnimator.SetBool ("FingerTapStatsOk",false);
		tutAnimator.SetBool ("WpnUpgradeExplain",false);
		tutAnimator.SetBool ("StatsFinger",false);
		tutAnimator.SetBool ("TapCloseMenu",false);	
		tutAnimator.SetBool ("PointAtWpn",false);	
		tutAnimator.SetBool ("TapOnWeapon",false); //to remove the finger anim
		tutAnimator.SetBool ("TalentFingerTap",false);
		tutAnimator.SetBool ("UnlockLevelTap",false);
		tutAnimator.SetBool ("UnlockWorldTap",false);
		tutAnimator.SetBool ("PrestigeButtonTap",false);
		tutAnimator.SetBool ("PotionTap",false);


		tapToContinueObj.GetComponent<TextMeshProUGUI> ().alpha = 0;

		wizardHand.gameObject.SetActive (false);

		tapToContinueObj.SetActive(false);

		tutorialText.gameObject.SetActive (true);

		tutorialTextBox.GetComponent<Image> ().raycastTarget = true;


		this.gameObject.SetActive (false);

	//	Camera.main.GetComponent<MainMenu> ().windowOpen = false;

	}

	public void disableButtons(Button dontDisableThisButton){

		for (int i = 0; i < buttonList.Count; i++) {

			buttonList [i].interactable = false;


		}

		dontDisableThisButton.interactable = true;

		//this is for the tutorial, disables all butons except the one u want the player to click.

	}

	public void restoreButtons(){

		for (int i = 0; i < buttonList.Count; i++) {

			buttonList [i].interactable = true;


		}
		//this is for the tutorial, disables all butons except the one u want the player to click.
		wpnMenuScrollRect.enabled = true;
		worldMenuScrollRect.enabled = true;
	}

}
