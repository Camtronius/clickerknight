﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class ControlPlayer2Character : MonoBehaviour {

	public Animator animator;
	//spells
	public GameObject fireball;
	public int fireballCount=0;
	public GameObject firedrag;
	public GameObject FlameDOT;
		public bool player2FlameDOT = false;
			public bool justBurned = false;

	public GameObject GiantRock;
	public GameObject WaterBubble;
	public GameObject WaterBeamObj;
	public GameObject InvisParticles;
	public GameObject Tornado;
	public GameObject Treant;
	public GameObject SpellShieldObj;


	//sspell spawn locationz
	public GameObject shotSpawn;
	public GameObject shotSpawnBehind;
	public GameObject shotSpawnTop;
	public GameObject shotSpawnTopPlayer1;

	public float playerHealthMax = 100;
	public float playerHealthCurrent = 100;
	public float playerHealthNew= 100;
	public float lifeStealValue = 0;
	public bool playerDead = false;

	//modifiers to the character
	public bool player2Invis=false;
	public bool player2ReturnToVisible = false; //does what it says ;p
	public bool player2Stun=false; //tru if player is invis
	public bool lifeSteal= false;
	public bool isShielded = false;

	//end mods to the character

	public GameObject Player2HealthBar;
	public GameObject Player2Torso;

	public GameObject Player2PunchHand;
	public GameObject AutoAtkCollider;

	public Vector3 defaultLocation;

	public SoundManager soundManager;

	// Use this for initialization
	void Start () {

		defaultLocation = this.transform.position;

		animator = GetComponent<Animator>();

		StartCoroutine ("HealthBarGUI"); //detects if a block is being selected

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();


	}

	public void castFireSpell(){

		animator.SetBool ("FireSpell", true);

		undoInvis ();
	}

	public void castDragSpell(){
		
		animator.SetBool ("FireDrag", true);

		undoInvis ();

	}

	public void castRockThrowSpell(){
		
		animator.SetBool ("RockThrow", true);

		undoInvis ();

	}

	public void castWaterBeamSpell(){
		
		animator.SetBool ("WaterBeam", true);

		undoInvis ();

	}

	public void castWaterBubble(){
		
		animator.SetBool ("WaterBubble", true);

		undoInvis ();

	}

	public void castInvisSpell(){
		
		animator.SetBool ("InvisSpell", true);

	}

	public void castTornadoSpell(){
		
		animator.SetBool ("TornadoSpell", true);

		undoInvis ();

	}

	public void castTreantSpell(){
		
		animator.SetBool ("SummonTreant", true);

		undoInvis ();

	}

	public void castSpellShield(){
		
		animator.SetBool ("SummonShield", true);

		undoInvis ();

	}

	public void castDefaultAttack(){
		
		animator.SetBool ("DefaultAtkP1", true);	
		
	}


	IEnumerator HealthBarGUI(){
		
		while (true) {
			
			if(playerHealthCurrent>playerHealthNew && lifeSteal==false){ //for health loss
				playerHealthCurrent-=20f;
				
				Player2HealthBar.GetComponent<Slider> ().value = (float)playerHealthCurrent/playerHealthMax;
				Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = playerHealthCurrent.ToString() + "/" + playerHealthMax.ToString();

				if(playerHealthCurrent<playerHealthNew){ //this makes it so the hp doesnt only go down in increments of 5
					
					playerHealthCurrent=playerHealthNew;
					Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = playerHealthCurrent.ToString() + "/" + playerHealthMax.ToString();
				}
				
			}
			
			if(lifeSteal==true && playerHealthCurrent<=playerHealthMax){

				if(lifeSteal==true && playerHealthCurrent<=playerHealthMax){
					
					//if the value is less than current hp max
					if((playerHealthCurrent + lifeStealValue)<playerHealthMax && lifeSteal==true){
						playerHealthCurrent+=lifeStealValue;
						lifeSteal=false;
						
					}
					
					if((playerHealthNew+ lifeStealValue)<playerHealthMax && lifeSteal==true){
						playerHealthNew+=lifeStealValue;
						lifeSteal=false;
					}
					
					//if playerhp + lifesteal value is more than max hp, just set as max hp
					if((playerHealthCurrent + lifeStealValue)>playerHealthMax && lifeSteal==true){
						playerHealthCurrent=playerHealthMax;
						lifeSteal=false;
					}
					
					if((playerHealthNew+ lifeStealValue)>playerHealthMax && lifeSteal==true){
						playerHealthNew=playerHealthMax;
						lifeSteal=false;
					}
					
					
					
					Player2HealthBar.GetComponent<Slider> ().value = (float)playerHealthCurrent/playerHealthMax;
					Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = playerHealthCurrent.ToString() + "/" + playerHealthMax.ToString();
					
					
					lifeSteal=false;
					
				}


				
			}

			yield return new WaitForSeconds(0.01f);
		}
		
	}


	public void createFireBall(){

		fireball.gameObject.GetComponent<FireBallCollider> ().createdByPlayer = 2;

		fireball.GetComponent<ConstantForce2D> ().force = new Vector2 (-12f, 0);

		fireballCount += 1;

		GameObject FireballObj = (GameObject)Instantiate (fireball, new Vector3 (shotSpawn.transform.position.x, shotSpawn.transform.position.y, 0), Quaternion.Euler(0,180,0));

//		FireballObj.GetComponent<FireBallCollider> ().NumberFireballCreated = fireballCount;

		if (fireballCount >= 3) {
			fireballCount=0;
		}
	}

	public void createFireDrag(){
		
		firedrag.gameObject.GetComponent<DragonSpellPath> ().createdByPlayer = 2;
		
		//	fireball.gameObject.GetComponent<ConstantForce2D> ().relativeForce = new Vector2 (1, 0);
		
		Instantiate (firedrag, new Vector3 (shotSpawnBehind.transform.position.x, shotSpawnBehind.transform.position.y, 0), Quaternion.Euler(0,180,316));
	}

	public void createWaterBeam(){
		
		GameObject WaterBeam = (GameObject)Instantiate (WaterBeamObj); //instantiates the beam offscreen
		
		WaterBeam.GetComponent<BeamIncrementer> ().createdByPlayer = 2;

		WaterBeam.name = "WaterBeam";

		if (this.gameObject.name == "FinalBoss") {

			Camera.main.GetComponent<CampaignGameOrganizer> ().BurntRunesManager (2, 0);
			Camera.main.GetComponent<CampaignGameOrganizer> ().createBurntRunes ();


		}
	}

	public void createGiantRock(){
		
		GameObject GiantRock2 = (GameObject)Instantiate (GiantRock, new Vector3 (this.transform.position.x  , shotSpawnTop.transform.position.y, 0), Quaternion.identity); //instantiates the beam offscreen

		GiantRock2.gameObject.GetComponent<RockCollider> ().createdByPlayer = 2;
	}

	public void closeWaterBeam(){
		GameObject.Find ("WaterBeam").GetComponent<BeamIncrementer> ().closeBeam = true;
	}

	public void createWaterBubble(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			shotSpawnTopPlayer1 = GameObject.Find("Player1Character/Player1").GetComponent<ControlPlayer1Character>().shotSpawnTop;
		}

		GameObject WaterBubbleObj = (GameObject)Instantiate (WaterBubble, new Vector3 (shotSpawnTopPlayer1.transform.position.x, shotSpawnTopPlayer1.transform.position.y, 0), Quaternion.identity);
		WaterBubbleObj.GetComponentInChildren<WaterBubbleBehavior>().createdByPlayer2 = true;
		WaterBubbleObj.GetComponent<WaterBubbleAnimation> ().createdByPlayer = 2;

	}
	
	public void CreateInvisParticles(){
		
		GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (Player2Torso.transform.position.x, Player2Torso.transform.position.y, 0), Quaternion.identity);
		InvisParticlesClone.transform.parent = this.transform;
		InvisParticlesClone.GetComponent<InvisibleWind> ().createdByPlayer = 2;

	}

	public void checkInvisCounter(){
		
		Camera.main.GetComponent<TurnedBasedGameOrganizer> ().InvisCounterCheckP2 ();
		//this is to check if both players are invis, so a player can counter attk when they are receiving a spell.

	}

	public void CreateTornado(){
		
		GameObject TornadoClone = (GameObject)Instantiate (Tornado, new Vector3 (shotSpawn.transform.position.x, shotSpawn.transform.position.y, 0), Quaternion.identity);
		TornadoClone.GetComponentInChildren<TornadoSpellScript> ().createdByPlayer = 2;
		TornadoClone.GetComponent<DestroyTornado> ().createdByPlayer = 2;

		if (this.gameObject.name == "FinalBoss") {
			
			Camera.main.GetComponent<CampaignGameOrganizer>().createConfusion();

			
			
		}

	}

	public void CreateFlameDOT(){
		
		GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);
		FlameDOTClone.transform.parent = this.transform;


		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {

			if (playerHealthCurrent <= 150) {
				
				playerHealthNew = 1;
			} else {
				
				playerHealthNew -= 150;
				
			}

			Camera.main.GetComponent<TurnedBasedGameOrganizer> ().damageP2Notification (150);
		}


	}

	public void CreateTreant(){
		
		GameObject TreantClone = (GameObject)Instantiate (Treant, new Vector3 (shotSpawnBehind.transform.position.x, shotSpawnBehind.transform.position.y + 10 , 0), Quaternion.Euler(0,180,0));
		TreantClone.GetComponentInChildren<TreantAnimation> ().createdByPlayer = 2;
	}

	public void CreateSpellShield(){
		
		GameObject SpellShieldClone = (GameObject)Instantiate (SpellShieldObj, new Vector3 (this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightLegUpper.transform.position.x, 
		                                                                                    this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightLegUpper.transform.position.y, 0), Quaternion.identity);
		SpellShieldClone.transform.parent = this.transform;
		SpellShieldClone.GetComponentInChildren<SphereShieldScript> ().createdByPlayer = 2;
		SpellShieldClone.GetComponentInChildren<SphereShieldScript> ().createShield = true;
	}

	public void CreateSwordAtkCollider(){
		
		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
	//		ControlEnemy enemyToHit = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ();
	//		GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player2Torso.transform.position.y, 0), Quaternion.identity);
	//		AutoAtkColliderClone.GetComponentInChildren<SwordAtkCollider> ().createdByPlayer = 2;
			
		} else {
			ControlPlayer1Character enemyToHit = GameObject.Find ("Player1").GetComponentInChildren<ControlPlayer1Character> ();
			GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player1Torso.transform.position.y, 0), Quaternion.identity);
			AutoAtkColliderClone.GetComponentInChildren<SwordAtkCollider> ().createdByPlayer = 2;
			
			
			
		}
		
		
	}

	public void endFireSpellAnimation(){ //add ending keyframes to the respective animations
		
		animator.SetBool ("FireSpell", false);
		
		animator.SetBool ("FireDrag", false);
		
		animator.SetBool ("RockThrow", false);
		
		animator.SetBool ("WaterBeam", false);
		
		animator.SetBool ("WaterBubble", false);

		if (animator.GetBool ("InvisSpell") == true) { //becase this invis spell and the shield spell do not create a ragdoll
			animator.SetBool ("InvisSpell", false); //this needs to be here so the turn is taken afterwards
			
			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true){

		if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character>().player1Invis==false){

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();

				}
			}
		}

		animator.SetBool ("TornadoSpell", false);
		
		animator.SetBool ("SummonTreant", false);

		if (animator.GetBool ("SummonShield") == true) {
			animator.SetBool ("SummonShield", false);
			
			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true
			   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character>().player1Invis == false){
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
				 
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
			}
		}

		animator.SetBool ("DefaultAtkP2", false);
		animator.SetBool ("DefaultAtkP1", false);


	}

	public void undoInvis(){

		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ()!= null &&
		    Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == false) {
			
		//	player2ReturnToVisible = true;
		//	this.GetComponent<BoxCollider2D>().enabled=true; //this makes it so the player cannot be hit

		}

	}

	void Update () {
	
	}

	public void playFireballSound(){
		
		soundManager.FireballSound ();
		//explosion sound will be attached to the fireball and played with collision
	}

	public void invisSound(){

		soundManager.InvisSound ();
	}
	
	public void playSwordWhoosh(){
		
		soundManager.SwordSwish ();
	}
	
	public void playSwordHit(){
		soundManager.SwordHit ();
	}
	
	public void waterBeamChargeUp(){
		soundManager.WaterBeamCharge ();
	}
	
	public void endWaterBeamSoundLoop(){
		soundManager.WaterBeamShotEnd ();
	}
	
	public void footstep1(){
		soundManager.footStep1 ();
	}
	
	public void footstep2(){
		soundManager.footStep2 ();
	}

	public void playLastBossLaugh(){
		soundManager.FinalBossLaugh ();
	}
	public void playLastBossDeath1(){
		soundManager.FinalBossDeath1 ();
	}
	public void playLastBossDeath2(){
		soundManager.FinalBossDeath2 ();
	}
	public void runeTravelSound(){
		soundManager.runeTravel ();
	}
	public void FinalBossAct2Laugh(){
		soundManager.FinalBossAct2Laugh ();
	}
	public void FinalBossAct2Death(){
		soundManager.FinalBossAct2Death ();
	}
	public void FinalBossAct2Appear(){
		soundManager.FinalBossAct2Appear ();
	}
	public void PlayGroundThud(){
		soundManager.RockOnFloor ();
	}
}
