﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class EnemyProjectileCollider : MonoBehaviour {

	public GameObject ExplosionForce;

	public float destroyTimer =2;
	public GameObject SpellCreator;
	public int DamageToPlayer;
	public GameObject poof;

	public GameObject skeleMageWindEffect;
	public GameObject skeleMageFireEffect;
	public GameObject skeleMageWaterEffect;
	public GameObject skeleMageEarthEffect;
	public GameObject skeleMageMiscEffect;

	public bool confusePlayer = false;
	public bool stunPlayer = false;


	public int runeTypeInt = 4; //0 wind, 1 fire, 2 water , 3 earth

	public GameObject tempPlayerObj;
	// Use this for initialization

	//for sfx
	public SoundManager soundManager;
	public GameObject ScreenShaker;


	void Start () {
	
//		DamageToPlayer = GameObject.Find ("EnemyStatsInheriter");

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();
		ScreenShaker = GameObject.Find ("ScreenShakerController");

		SpellCreator = GameObject.Find ("SpellListener");
		tempPlayerObj = GameObject.Find ("Player1Character");

	}

	public void disableAllParticleEffects(){

		runeTypeInt = 4;
		skeleMageWindEffect.SetActive(false);
		skeleMageFireEffect.SetActive(false);
		skeleMageWaterEffect.SetActive(false);
		skeleMageEarthEffect.SetActive(false);
		skeleMageMiscEffect.SetActive(false);

	}

	void OnCollisionEnter2D (Collision2D player) {



		if (player.gameObject.tag == "Player1") {

			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){

				if(this.gameObject.tag=="Projectile"){
					soundManager.BoneHit ();
				}

			

			player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew-=DamageToPlayer;

				Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player

				if(DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
					
					player.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
					player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
					
					
				}

				if(this.gameObject.GetComponent<BoxCollider2D>()!=null){
					this.gameObject.GetComponent<BoxCollider2D>().enabled=false;
				}

			}

		}
	}

	void OnTriggerEnter2D(Collider2D player) {
		
		if (player.gameObject.tag == "Player1") {

		//	print ("the player is detecting the hit by the enemy");
//			print ("runeinttype is " + runeTypeInt);
		//	print ("last spell sprite is " + Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite);

			
			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){

				if(Camera.main.GetComponent<CampaignGameOrganizer>().playerDamageAmount.activeSelf==true){ //this is for the combo attack
					Camera.main.GetComponent<CampaignGameOrganizer>().playerDamageAmount.SetActive(false);
				}


				if(runeTypeInt==0 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
						
						DamageToPlayer = 2* DamageToPlayer;
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}
					else if (runeTypeInt==0 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
						
						DamageToPlayer = Mathf.FloorToInt( 0.5f* DamageToPlayer);
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
						
				}
					

				else if(runeTypeInt==1 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				 	  == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
						
						DamageToPlayer = 2* DamageToPlayer;
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}
				else if(runeTypeInt==1 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
						
						DamageToPlayer = Mathf.FloorToInt(0.5f* DamageToPlayer);
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
						
					}


				else if(runeTypeInt==2 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
						
						DamageToPlayer = 2* DamageToPlayer;
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}

				else if(runeTypeInt==2 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
						
						DamageToPlayer = Mathf.FloorToInt(0.5f* DamageToPlayer);
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
						
					}


				else if(runeTypeInt==3 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
						
						DamageToPlayer = 2* DamageToPlayer;
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

					}

				else if(runeTypeInt==3 && Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
					   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
						
						DamageToPlayer = Mathf.FloorToInt(0.5f* DamageToPlayer);
						player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
						Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
						
					}

				else{

						player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew-=DamageToPlayer;

					}

				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen


				if(confusePlayer==true){

					Camera.main.GetComponent<CampaignGameOrganizer>().createConfusion();
				}

				if(stunPlayer==true){
					Camera.main.GetComponent<CampaignGameOrganizer>().stunManager(2,0); //this should stun p1 for 1 round?
					Camera.main.GetComponent<CampaignGameOrganizer>().PlayerStunned();
					player.gameObject.GetComponent<ControlPlayer1Character>().soundManager.confusionSound();
				}

				//this is for the player dying
				if(DamageToPlayer >= player.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
					
					player.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
					player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();

					if(ExplosionForce!=null){
					ExplosionForce = Instantiate (ExplosionForce, new Vector3 (this.transform.position.x,
					                                                           player.GetComponent<TurnOnRagdoll>().RightLegUpper.transform.position.y, 0), Quaternion.identity) as GameObject;
					}
				}

				Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player

				if(this.gameObject.GetComponent<BoxCollider2D>()!=null){

					this.gameObject.GetComponent<BoxCollider2D>().enabled=false;
				}

			}
			
		}
	}
	
	void Update () {
	
		destroyTimer -= Time.deltaTime;

		if (destroyTimer <= 0) {
			if(this.gameObject.tag !="Combo"){ //combo hit doesnt make a turn be taken...

				if(tempPlayerObj.GetComponentInChildren<ControlPlayer1Character>().player1Invis==false){
				
					SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(2);

				Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();
				Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);
					Camera.main.GetComponent<CampaignGameOrganizer>().WhosTurn(true,"null"); //this shows whos  turn it is

				
				}else{
					SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(2);


				}
				if(this.gameObject.tag=="Projectile"){

					GameObject PooflObj = (GameObject)Instantiate (poof, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

				}
			}

			Destroy (this.gameObject);



		}

	}
}
