﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;

public class IngredientsMaster : MonoBehaviour {

	// Use this for initialization
	public ObscuredInt totalRedGem; //0
	public ObscuredInt totalBlueGem; // 1
	public ObscuredInt totalYellowGem; //2
	public ObscuredInt totalGreenGems; //3

	public List<TextMeshProUGUI> amountOfIngreds = new List<TextMeshProUGUI> ();


	void Start () {

		totalRedGem = ObscuredPrefs.GetInt ("totalRedGem");
		amountOfIngreds[0].text = " " + totalRedGem.ToString ();

		totalBlueGem = ObscuredPrefs.GetInt ("totalBlueGem");
		amountOfIngreds[1].text = " " + totalBlueGem.ToString ();

		totalYellowGem = ObscuredPrefs.GetInt ("totalYellowGem");
		amountOfIngreds[2].text = " " + totalYellowGem.ToString ();

		totalGreenGems = ObscuredPrefs.GetInt ("totalGreenGem");
		amountOfIngreds[3].text = " " + totalGreenGems.ToString ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setNewPrefs(){

		//set the new fvalues here and to update the values on the chart

		ObscuredPrefs.SetInt ("totalRedGem", totalRedGem);
		ObscuredPrefs.SetInt ("totalBlueGem", totalBlueGem);
		ObscuredPrefs.SetInt ("totalYellowGem", totalYellowGem);
		ObscuredPrefs.SetInt ("totalGreenGem", totalGreenGems);
	//	PlayerPrefs.SetInt ("totalBananna",totalBananna);

		amountOfIngreds[0].text = " " + totalRedGem.ToString ();
		amountOfIngreds[1].text = " " + totalBlueGem.ToString ();
		amountOfIngreds[2].text = " " + totalYellowGem.ToString ();
		amountOfIngreds[3].text = " " + totalGreenGems.ToString ();

	}
}
