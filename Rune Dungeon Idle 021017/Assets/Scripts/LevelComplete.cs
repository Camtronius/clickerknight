﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.UI;
using TMPro;

public class LevelComplete : MonoBehaviour {

	// Use this for initialization
	public TextMeshProUGUI currentLevelText;
	public GameObject closeBuyableSpellsPane; 
	public GameObject closeBuyableSpellsButton;

	public GameObject UndoButton;
	public GameObject BackButton;
	public GameObject BuyableItemsPane;

	public GameObject UpgradableStats;
	public GameObject statsButton;
	public GameObject EquipsItemButton;

	public GameObject exitLevelButton;
	public GameObject SpellbookButton;
	public GameObject LearnSpellsButton;
	public GameObject spellDisplay;

	public GameObject NextLevelButton;
	public TextMeshProUGUI NextLevelText;

	public GameObject TitleScreenButton;

	public GameObject NewSomethingParticles; //this is used if there is an unused skill point, a new item, or a new spell point that hasnt been used

	public TutorialCampaign TutorialObject;

	public ClothePlayer PlayerForDictSprites;
	public GameObject NewSpellNotification;
	public GameObject NewStatsNotificaton;
	public GameObject NewItemNotification;

	public GameObject player;
	public GameObject canvasBehindPlayer;
	public GameObject rateGameButton;

//	public GameObject canvasInFrontOfPlayer;


	void Start () {
	
		exitLevelButton.SetActive (false);
		SpellbookButton.SetActive (false);
	//	closeBuyableSpellsButton.SetActive (true);

		string LVnumComplete = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ().CurrentLevel.ToString();

		if (this.gameObject.name == "Level Complete") {

			currentLevelText.text = "Level " + LVnumComplete + "\n Complete!";

			checkNewNotifications ();

			if(GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ().CurrentLevel>=5){

				rateGameButton.SetActive(true);
			}
			//the condition to see if the player is going to have a free gold video ad is in the bottom of campaign level enemy spawner script

		} else {
			currentLevelText.text = "Level " + LVnumComplete + "\n Failed...";

		}

		if (Camera.main.GetComponent<CampaignGameOrganizer> ().enemyHealthBar.activeSelf == true) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().enemyHealthBar.SetActive(true);
		}


		//for tracking which levles the player has completed, the other part of the analytics is in Campaign Game Organizer

		Analytics.CustomEvent("Level" + LVnumComplete + "Complete", new Dictionary<string, object>
		                      {
			{"Level" + LVnumComplete + "Complete", true }
			
		});


		if (this.gameObject.name == "Level Complete" && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 9) {
			
			GameObject.Find ("SpellListener").GetComponent<SpellCreatorCampaign> ().LevelCompleteAchievement(); //this adds the achievement for getting past lv9
			

		}

		if (TutorialObject!=null && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 14) {
			
			currentLevelText.text = "Act 1 Complete!";

			NextLevelText.GetComponent<TextMeshProUGUI>().text = "<color=green>Next Act!!</color>";

			NextLevelButton.SetActive (true);
		}

		
		if (TutorialObject!=null && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 28) {
			
			currentLevelText.text = "Game Complete!!";
			
			//NextLevelText.GetComponent<TextMeshProUGUI>().text = "<color=green>Next Act!!</color>";
			
			NextLevelButton.SetActive (false);
		}

		if (TutorialObject!=null && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 15) {
			
			NewSpellNotification.SetActive(true);
			NewItemNotification.SetActive(true);
		}

		foreach (MeteorCode rune in Camera.main.GetComponent<CampaignGameOrganizer>().meteorList) {

			rune.transform.gameObject.SetActive(false);
		}

		foreach (GameObject rune in Camera.main.GetComponent<CampaignGameOrganizer>().ghostMeteorList) {
			
			rune.SetActive(false);
		}

		UndoButton.SetActive (false);


	}

	public void checkNewNotifications(){

		if (PlayerPrefs.GetFloat("SpellPoints") > 0) {
			
			NewSpellNotification.SetActive(true);
		}
		
		if(PlayerPrefs.GetFloat("SkillPoints") > 0){
			
			NewStatsNotificaton.SetActive(true);
		}
		
		Sprite[] sprites = Resources.LoadAll<Sprite> ("SpritesForArmor");
		
		foreach (Sprite sprite in sprites) {
			
			if(PlayerPrefs.GetInt(sprite.name.ToString())==2){
//				print ("-~-~_~-~-~_~~_~_ new item available!");
				NewItemNotification.SetActive(true);
			}
		}
	}


	
	// Update is called once per frame
	public void closeBuyableSpellBook(){ //also works for buyable items

		if (TutorialObject != null && TutorialObject.isTutorial == true) {

			if(TutorialObject.spellHasBeenUnlocked==false 
			&& TutorialObject.EnemyStatsInheriterObj.CurrentLevel==1){
			TutorialObject.OffBackButton(); //turns the finger off the back button
			}

			if(TutorialObject.spellHasBeenUnlocked==true 
			   && TutorialObject.ItemExplainEquipBool==false
			   && TutorialObject.EnemyStatsInheriterObj.CurrentLevel==1){
				TutorialObject.ExplainContinueLevel1();
			}

			if(TutorialObject.ItemExplainEquipBool==true
			   && TutorialObject.EnemyStatsInheriterObj.CurrentLevel==2){ //this is for the end of the equip item tutorial
				TutorialObject.ExplainContinueLevel2();

			}

		}

			if (closeBuyableSpellsPane.activeSelf == true) {
				NewSpellNotification.SetActive (false);
//			closeBuyableSpellsPane.GetComponent<Animator> ().enabled=false;
			//closeBuyableSpellsPane.GetComponent<Animator> ().SetBool ("Effectiveness", false);


			}

		closeBuyableSpellsPane.SetActive (false);
		closeBuyableSpellsButton.SetActive (false);

		closeBuyableSpellsPane.GetComponent<ScrollRect>().enabled=true;
		closeBuyableSpellsPane.GetComponent<RawImage>().enabled=true;

		spellDisplay.SetActive (false);

			if (BuyableItemsPane.activeSelf == true) {
				NewItemNotification.SetActive(false);
			}

		BuyableItemsPane.SetActive (false);
		BackButton.SetActive (false);

			if (UpgradableStats.activeSelf == true) {
				NewStatsNotificaton.SetActive(false);
			}

		UpgradableStats.SetActive (false);

	//	canvasBehindPlayer.SetActive (true);//sets the canvas behind the player active for better performance

	}

	public void openBuyableSpellBook(){

	//	canvasBehindPlayer.SetActive (false);//sets the canvas behind the player active for better performance


//		if (PlayerPrefs.GetInt ("FreeSpells") == 1 && PlayerPrefs.GetInt("FreeSpellEarned")<2) { //checks if the ad shoulod popup when equipbag is up
//			Camera.main.GetComponent<MainMenu>().FreeSpellPopUp.SetActive(true);
//		}

		Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.openSpellBook ();

		if (TutorialObject != null && TutorialObject.isTutorial == true && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 1) { //explains that you can upgrade ur stats
			
			TutorialObject.explainSpellsBuy ();

		} else {
			BackButton.SetActive(true);

		} 

		closeBuyableSpellsPane.SetActive (true); //this actually opens the spellbook window
		closeBuyableSpellsPane.GetComponent<SpellPoints> ().updatePlayerInfo (); //make sure the correct points are shown

	}

	public void openBuyableItems(){

	//	canvasBehindPlayer.GetComponent<GraphicRaycaster> ().enabled = true;

		Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.openEquipBag ();


		BuyableItemsPane.SetActive (true);
		BackButton.SetActive (true);

		if (TutorialObject.ItemExplainEquipBool == true && TutorialObject.isTutorial==true) {
			
			TutorialObject.showNewEquipmentUnlocked();
			BackButton.SetActive(false);
			print ("the new equipment is supposed to have a finger on it now.... !!!!");
		}

		if (TutorialObject.isTutorial == false && TutorialObject.TutorialFinger.activeSelf == true) {

			TutorialObject.TutorialFinger.SetActive(false);

		}

		player.GetComponent<Animator> ().SetBool ("PlayerSwordIdle", true);

//		if (PlayerPrefs.GetInt ("FreeGems") == 1) { //checks if the ad shoulod popup when equipbag is up
//
//
//			if(PlayerPrefs.GetFloat("Level14")==1){ //if its a video ad after act 1 is complete
//				
//				Camera.main.GetComponent<MainMenu>().FreeGoldPopUp.GetComponent<TestVideoAdScriptUnity>().WouldLikeToWatch.text = "Watch brief Ad for <#9CCFE0FF>2000    </color>?";
//			}else{
//				Camera.main.GetComponent<MainMenu>().FreeGoldPopUp.GetComponent<TestVideoAdScriptUnity>().WouldLikeToWatch.text = "Watch brief Ad for <#9CCFE0FF>1000    </color>?";
//				
//			}
//
//			Camera.main.GetComponent<MainMenu>().FreeGoldPopUp.SetActive(true);
//		}

	}

	public void openUpgradableStats(){

	//	canvasBehindPlayer.SetActive (false);//sets the canvas behind the player active for better performance

		Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.openStats ();

		if (TutorialObject != null && TutorialObject.isTutorial == true && TutorialObject.EnemyStatsInheriterObj.CurrentLevel == 1) { //explains that you can upgrade ur stats

			TutorialObject.explainStatsUpgrade ();

		} else {

			closeBuyableSpellsButton.SetActive (true);

		}

			UpgradableStats.SetActive (true);

	//	if (TutorialObject == null || TutorialObject.isTutorial == false) { //makes it so the back button isnt visible if tutorial is on
		
	//		closeBuyableSpellsButton.SetActive (true);
		
	//	}
	
	}

	public void restoreAllButtons(){

		LearnSpellsButton.SetActive (true);
		statsButton.SetActive(true);
		EquipsItemButton.SetActive(true);
		NextLevelButton.SetActive (true);
		TitleScreenButton.SetActive (true);
	}
}
