﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using TMPro;

public class TestVideoAdScriptUnity : MonoBehaviour 
{
	public GameObject BuyItemParticles;//this is used for the reward when they finish the ad
	public GameObject SubStatsParticles;
	public GameObject AddStatsParticles;

	public TextMeshProUGUI WouldLikeToWatch;
	public TextMeshProUGUI TotalStats;
	public TextMeshProUGUI AtkStats;
	public TextMeshProUGUI HPStats;
	public TextMeshProUGUI LuckStats;
	public TextMeshProUGUI goldAmount;
	public TextMeshProUGUI spellPoints;

	public GameObject Player1HPBar;
	public GameObject StatsPane;

	private int reward;
	
	[SerializeField] string gameID = "1065830";
	
	void Awake()
	{
		Advertisement.Initialize (gameID, true);
	}
	
	public void ShowAd(string zone = "rewardedVideoZone")
	{
		#if UNITY_EDITOR
		StartCoroutine(WaitForAd ());
		#endif
		
		if (string.Equals (zone, "rewardedVideoZone"))
			zone = null;
		
		ShowOptions options = new ShowOptions ();
		options.resultCallback = AdCallbackhandler;
		
		if (Advertisement.IsReady (zone))
			Advertisement.Show (zone, options);
	}
	
	void AdCallbackhandler (ShowResult result)
	{
		switch(result)
		{
		case ShowResult.Finished:
			Debug.Log ("Ad Finished. Rewarding player...");

			if(this.transform.name == "VideoAdPopUp"){
			WouldLikeToWatch.text = "equiped ad finished";

				CompletedAdEquip();
				closePopUpVideo();
			}

			if(this.transform.name == "ResetStatsPopUp"){
			//	WouldLikeToWatch.text = "Reset ad finished";

				CompletedAdStats();
				closePopUp();
			}

			if(this.transform.name == "VideoAdNewSpell"){

				CompletedAdSpellBook();
				closePopUpSpell();
			}

			break;
		case ShowResult.Skipped:
			Debug.Log ("Ad skipped. Son, I am dissapointed in you");

			if(this.transform.name == "VideoAdPopUp"){
			}

			if(this.transform.name == "ResetStatsPopUp"){
			}

			if(this.transform.name == "VideoAdNewSpell"){
			}

			break;
		case ShowResult.Failed:

			Debug.Log("I swear this has never happened to me before");

			if(this.transform.name == "VideoAdPopUp"){
				WouldLikeToWatch.text = "Ad View Fail";
			}

			if(this.transform.name == "ResetStatsPopUp"){
				WouldLikeToWatch.text = "Ad View Fail";
			}

			if(this.transform.name == "VideoAdNewSpell"){
				WouldLikeToWatch.text = "Ad View Fail";
			}

			break;
		}
	}

	public void closePopUp(){
		//this is if the player selects no instead
		this.gameObject.SetActive (false);
	}

	public void closePopUpVideo(){

		PlayerPrefs.SetInt ("FreeGems", 0); //if they say no they have to wait until they complete 2 levels to get the offer again
		this.gameObject.SetActive (false);

	}

	public void closePopUpSpell(){

		PlayerPrefs.SetInt ("FreeSpells",0);
		this.gameObject.SetActive (false);

	}

	public void CompletedAdEquip(){

		if (PlayerPrefs.GetFloat ("Level14") == 1) { //if its a video ad after act 1 is complete

			reward = 2000;
		
		} else {

			reward = 1000;

		}

		int gold = PlayerPrefs.GetInt ("Gold") + reward;
		
		PlayerPrefs.SetInt("Gold", gold);
		
		goldAmount.text = gold.ToString();

		GameObject BuyParticles = (GameObject)Instantiate (BuyItemParticles, new Vector3 (goldAmount.transform.position.x, goldAmount.transform.position.y, 0), Quaternion.identity) as GameObject;

		//tells us how many ppl are actually clicking on the free gold ad
		Analytics.CustomEvent("GoldAdWatched", new Dictionary<string, object>
		                      {
			{"GoldAdWatched" , true }
		});

	//	this.gameObject.SetActive (false);

	}

	public void CompletedAdSpellBook(){

		AddStatsParticles = Instantiate (AddStatsParticles, new Vector3 (spellPoints.transform.position.x, 
		                                                                 spellPoints.transform.position.y, 0), Quaternion.identity) as GameObject;

		float currentSpellPoints = PlayerPrefs.GetFloat("SpellPoints");

		PlayerPrefs.SetFloat("SpellPoints",currentSpellPoints+1); //****Need to make it so you can have more than 1 skill pts....

		//tells us how many ppl are actually clicking on the free gold ad
		spellPoints.text = PlayerPrefs.GetFloat ("SpellPoints").ToString ();

		PlayerPrefs.SetInt ("FreeSpellEarned", PlayerPrefs.GetInt("FreeSpellEarned")+1); //this is so you can only earn one new spell once you have watched it

		Camera.main.GetComponent<MainMenu>().spellWindow.GetComponent<SpellPoints>().PointsToSpend = PlayerPrefs.GetFloat("SpellPoints");

		Analytics.CustomEvent("SpellAdWatched", new Dictionary<string, object>
		                      {
			{"SpellAdWatched" , true }
		});
		
		//	this.gameObject.SetActive (false);
		
	}

	public void CompletedAdStats(){

		int AtkStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("atkMod")) - 10; 
		
		PlayerPrefs.SetFloat ("atkMod", 10);
		
		AtkStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("atkMod")).ToString ();

			SubStatsParticles = Instantiate (SubStatsParticles, new Vector3 (AtkStats.transform.position.x, 
			                                                                 AtkStats.transform.position.y, 0), Quaternion.identity) as GameObject;
		int HPStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("HealthMod")) - 10; 
		
		PlayerPrefs.SetFloat ("HealthMod", 10);
		
		HPStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("HealthMod")).ToString ();

			SubStatsParticles = Instantiate (SubStatsParticles, new Vector3 (HPStats.transform.position.x, 
			                                                                 HPStats.transform.position.y, 0), Quaternion.identity) as GameObject;
		//reset hp bar to default
		Player1HPBar.GetComponentInChildren<TextMeshProUGUI> ().text = (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString () + "/" + (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString ();

		int LuckStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")) - 10; 
		
		PlayerPrefs.SetFloat ("luckMod", 10);
		
		LuckStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")).ToString ();

			SubStatsParticles = Instantiate (SubStatsParticles, new Vector3 (LuckStats.transform.position.x, 
			                                                                 LuckStats.transform.position.y, 0), Quaternion.identity) as GameObject;

		int newSkillPoints = Mathf.FloorToInt(PlayerPrefs.GetFloat("SkillPoints")) + (AtkStatsSubtracted + HPStatsSubtracted + LuckStatsSubtracted);
		
		PlayerPrefs.SetFloat("SkillPoints", newSkillPoints);

		TotalStats.text = newSkillPoints.ToString();

			AddStatsParticles = Instantiate (AddStatsParticles, new Vector3 (TotalStats.transform.position.x, 
			                                                                 TotalStats.transform.position.y, 0), Quaternion.identity) as GameObject;

		StatsPane.GetComponent<StatsPoints> ().updatePlayerInfo ();

		//tells us how many ppl are actually clicking on the free gold ad
		Analytics.CustomEvent("StatsAdWatched", new Dictionary<string, object>
		                      {
			{"StatsAdWatched" , true }
		});

	//	this.gameObject.SetActive (false);

	}

	IEnumerator WaitForAd()
	{
		float currentTimeScale = Time.timeScale;
		Time.timeScale = 0f;
		yield return null;
		
		while (Advertisement.isShowing)
			yield return null;
		
		Time.timeScale = currentTimeScale;
	}
}