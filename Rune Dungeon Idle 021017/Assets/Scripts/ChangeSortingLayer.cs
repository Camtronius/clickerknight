﻿using UnityEngine;
using System.Collections;

public class ChangeSortingLayer : MonoBehaviour {

	public string SortingLayerName;
	// Use this for initialization
	void Start () {
		if (this.gameObject.GetComponent<Renderer> () != null) {
			this.gameObject.GetComponent<Renderer> ().sortingLayerName = SortingLayerName;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
