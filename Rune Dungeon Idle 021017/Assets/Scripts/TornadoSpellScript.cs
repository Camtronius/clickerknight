﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;


public class TornadoSpellScript : MonoBehaviour {


	public int createdByPlayer = 0;
	public float newGravityScale = -0.05f;
	
	public List<GameObject> partsList = new List<GameObject>();

	public bool BubbleFloat=false;

	public GameObject RightTriggerCollider;
	public GameObject LeftTriggerCollider;
	public GameObject tornadoAnimObj;
	public GameObject WindExplosion;

	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public GameObject ScreenShaker;

	public double spellDMG = 0;
	public bool isCrit = false;

	public SoundManager soundManager;




	// Use this for initialization
	void Start () {
	
		ScreenShaker = GameObject.Find ("ScreenShakerController");

		StartCoroutine ("FloatInNado");

		if (createdByPlayer == 2) {
			this.gameObject.GetComponentInParent<ConstantForce2D>().force = new Vector2(-3f,0);
		}

		soundManager =Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager> ();


	//	ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (3f,0.1f); //shakes the screen

//		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
//			CreateConfusion ();
//		}

		//	Camera.main.GetComponent<TurnedBasedGameOrganizer>().createConfusion();
		
		//the tornado doesnt apply immediately when the runes are spawned because the mysterysprite manager/detector is applied before the runes are spawned. 
		//therefore it just trys to change runes that dont exist yet. It doesnt occur on the next round because by that time, the int timer is already 0 so you never
		//see the runes. A quick fix is to just apply it to the other player when they recieve the spell, but it has a delay since it starts when the spell
		//is created. You could try making the ? runes mod its own method and apply after the runes are spawned?

	}
	
	// Update is called once per frame
	void Update () {


	
	}

	void OnTriggerEnter2D (Collider2D other) {
		
//		if (other.gameObject.tag == "Player2" && createdByPlayer==1) {
//			if (other.GetComponent<TurnOnRagdoll>()!=null && other.GetComponent<TurnOnRagdoll> ().enabled == false) {
//				
//				other.GetComponent<TurnOnRagdoll>().enabled = true;
//				other.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//					//where Damage is calcualted
//					if(other.GetComponent<ControlPlayer2Character>().isShielded==false){
//						
//						float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "Tornado")
//								+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[0] //adding fire item mods
//					            + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod
//						
//						//condition for super effective
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//							
//
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//							
//						}
//						
//						//condition for Not very effective
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//							
//						calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//							
//						}
//						
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					  	== Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//							
//							
//						}
//
//					//check for crit against p2
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//					   ){ //did the player crit on their turn??
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//
//
//					other.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p2 notification
//
//
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (0, 1); //this used to be 0,1
//					}
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (0, 2);
//					}
//				
//					WindExplosion = Instantiate (WindExplosion, new Vector3 (this.gameObject.transform.position.x,
//					                                                         this.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//				
//				}
//					//End where Damage is calcualted
//
//			}
//		}

//		if (other.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			if (other.GetComponent<TurnOnRagdoll>()!=null && other.GetComponent<TurnOnRagdoll> ().enabled == false) {
//				
//				other.GetComponent<TurnOnRagdoll>().enabled = true;
//				other.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//
//				//where Damage is calcualted
//				if(other.GetComponent<ControlPlayer1Character>().isShielded==false){
//					
//					float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "Tornado")
//							+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[0] //adding fire item mods
//					        + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod
//					
//					//condition for super effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//
//						calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//						
//
//					}
//
//
//
//					//check for crit against p1
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//					   ){ //did the player crit on their turn??
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//
//						other.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//	
//
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
//						Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (0, 1); //this used to be 0,1
//					}
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
//						Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (0, 2);
//					}
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p1 notification
//
//					WindExplosion = Instantiate (WindExplosion, new Vector3 (this.gameObject.transform.position.x,
//					                                                         this.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
//				}
//				//End where Damage is calcualted
//
//			}
//		}

		if (other.gameObject.tag == "Enemy" && createdByPlayer==1 ) { //enemyishitby a tornado
			damageEnemy(other);
			WindExplosion = Instantiate (WindExplosion, new Vector3 (tornadoAnimObj.transform.position.x+1,
																	tornadoAnimObj.transform.position.y, 90), Quaternion.identity) as GameObject;
		}

			if (other.gameObject.tag == "Player2" && createdByPlayer==1 ) { //enemyishitby a tornado
					damageEnemy(other);
			WindExplosion = Instantiate (WindExplosion, new Vector3 (tornadoAnimObj.transform.position.x+1,
																	tornadoAnimObj.transform.position.y, 90), Quaternion.identity) as GameObject;
				}

					if (other.gameObject.tag == "Player1"  && createdByPlayer==2) { //when the player is hit by a player2enemy
						damagePlayer(other);
			WindExplosion = Instantiate (WindExplosion, new Vector3 (tornadoAnimObj.transform.position.x+1,
																	tornadoAnimObj.transform.position.y, 90), Quaternion.identity) as GameObject;
					}
		this.transform.parent.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
	}

	public void damageEnemy(Collider2D other){ //this will pass in the object so we can just call one method instead of one giant method
//		print("tornado hitting enemy");
		
		LeftTriggerCollider.GetComponent<BoxCollider2D>().enabled = false;
		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

		soundManager.TornadoHit ();

		//where Damage is calcualted
		if(other.GetComponent<ControlEnemy>().isShielded==false){
			
			double calculatedHealthLoss = spellDMG;

			if (isCrit == false) {
				Camera.main.GetComponent<MainMenu> ().SpellDamage ((spellDMG)); //sends the spell damage to the spell dmg text thing
			} else {

				Camera.main.GetComponent<MainMenu> ().spellCritText ((spellDMG)); //sends the spell damage to the spell dmg text thing

			}
//			print("tornado hitting enemy 2");
			
			//condition for super effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
//				
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//				
//				
//			}
			
			//condition for Not very effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//
//				calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//			}
			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//				
//				print("tornado hitting enemy3");
//								
//			}


			//put crit code here
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//				
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//				print("CRITS!!!!!!!!!");
//				
//			}

					other.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss; //for damaging the enemy and possible criyz
			int randoStun = Random.Range (0, 100);

					
			if (Camera.main.GetComponent<MainMenu> ().SpellStunChance >= randoStun && Camera.main.GetComponent<MainMenu> ().SpellStunEnabled == true) {
				other.gameObject.GetComponent<ControlEnemy> ().EnemyStunned = true;
			}
			//		Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);


			if (calculatedHealthLoss >= other.GetComponent<ControlEnemy> ().playerHealthCurrent) {
			//	this.gameObject.GetComponentInParent<BoxCollider2D>().enabled=false;
				LeftTriggerCollider.GetComponent<BoxCollider2D>().enabled = true;

				//other.gameObject.GetComponent<ControlEnemy>().playerDead=true;
				//other.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
				//other.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
				
				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
				
			}else{
				//print("tornado hitting enemy");
				//	RightTriggerCollider.GetComponent<BoxCollider2D>().enabled = false;
				//	LeftTriggerCollider.GetComponent<BoxCollider2D>().enabled = false;
				
				this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
				this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
				
			}
		}
		//End where Damage is calcualted
	}

	public void damagePlayer(Collider2D other){ //this will pass in the object so we can just call one method instead of one giant method

		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

		if (other.GetComponent<ControlPlayer1Character>().isShielded==false) {


			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
		
//		
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WaterSprite) {
//			
//				DamageToPlayer = 2f * DamageToPlayer;
//				other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (1);//1 = super effective, 2 = not very effectiveness
//			
//			}
//		
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WindSprite) {
//
//				DamageToPlayer = 0.5f * DamageToPlayer;
//				other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (2);//1 = super effective, 2 = not very effectiveness
//			
//			}
//		
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().EarthSprite) {
//			
//				//	DamageToPlayer = 2* DamageToPlayer;
//				other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//				//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//			
//			}
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().FireSprite) {
//			
//				//DamageToPlayer = 2* DamageToPlayer;
//				other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//				//Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//			
//			}
//
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//			    == Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite) {
//				
//				//DamageToPlayer = 2* DamageToPlayer;
//				other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//				//Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//				
//			}

			if (DamageToPlayer >= other.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
				
				other.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
				other.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
				other.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();

			}

	//		Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player

		}
	
	}

	void OnTriggerStay2D (Collider2D other) {
		
		if (!partsList.Contains (other.gameObject)) {
			
			partsList.Add(other.gameObject);
			
		}
		BubbleFloat = true;
	//	this.GetComponentInParent<ConstantForce2D> ().force = new Vector2 (0, 0);


	}

	IEnumerator FloatInNado(){
		
		while (true) {
			
			yield return new WaitForSeconds (0.1f);
			
			if (BubbleFloat == true) {


				for (int i=0; i<partsList.Count; i++) {

					if(partsList[i]!=null && partsList[i].GetComponent<Rigidbody2D>()!=null){

					if (this.gameObject.GetComponent<BoxCollider2D> ().IsTouching(partsList[i].GetComponent<BoxCollider2D>())) {

							partsList[i].GetComponent<Rigidbody2D>().gravityScale = newGravityScale;

							partsList[i].GetComponent<Rigidbody2D>().AddForce((partsList[i].transform.position)*50f);
											
					}else{

						if(this.gameObject!=RightTriggerCollider){

							LeftTriggerCollider.GetComponent<TornadoSpellScript>().partsList[i].GetComponent<Rigidbody2D>().gravityScale = -2f;

						}

						if(this.gameObject!=LeftTriggerCollider){
							
							RightTriggerCollider.GetComponent<TornadoSpellScript>().partsList[i].GetComponent<Rigidbody2D>().gravityScale = -2f;

							}

						}

					}
				
				}
			}
		}
	}

	public void CreateConfusion(){
		
		//this is for when a player is receiving a spell online
		if (createdByPlayer == 1 //this is sent by player 1 received by player 2
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2
		    && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2Invis==false
		    && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().isShielded==false) {

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
			Camera.main.GetComponent<TurnedBasedGameOrganizer> ().createConfusion(); //this actually creates the stun
			}
		}

		if (createdByPlayer == 2 //this is sent by player 2 received by player 1
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1
		    && GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().player1Invis==false
		    && GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().isShielded==false) {

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().createConfusion(); 
			}
		}
//----------------------------------------
		
		//this is for just when the spell is first sent
		if (createdByPlayer == 1 
		    && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2Invis==false
		    && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().isShielded==false) {
			
		//	if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){ //turned this off on 9/21/15 so it would show the "?" during online play when the player sends the spell at the other player
			//	Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (0, 2);
				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().createConfusion(); 

			//}
		}
		if (createdByPlayer == 2 
		    && GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().player1Invis==false
		    && GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().isShielded==false) {

			//if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
			//	Camera.main.GetComponent<TurnedBasedGameOrganizer> ().confusionManager (2,0);
				Camera.main.GetComponent<TurnedBasedGameOrganizer> ().createConfusion(); 

			//}
		}
	}
}
