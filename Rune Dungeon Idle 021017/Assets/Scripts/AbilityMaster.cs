﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Analytics.Experimental;


public class AbilityMaster : MonoBehaviour {

    public List<GameObject> abilityGameobj = new List<GameObject>();
    public List<Transform> abilityLocation = new List<Transform>();


    public List<AbilityCard> ListOfAbilities = new List<AbilityCard>();
    public List<Sprite> AbilitySprites = new List<Sprite>();
    public List<string> AbilityNames = new List<string>();


    public int[] unlockedAbilities; //will show 1 if level is unlocked and 0 if it is not unlocked
    public int[] equippedAbilities; //will show 1 if level is unlocked and 0 if it is not unlocked



    public List<Image> equipSlot = new List<Image>();

    public GameObject abilityCardPrefab;
    public GameObject abilityCardholder;

    public Sprite blankTransparent;
    public GameObject abilEquipPage;

    public int currentSelectedIndex = -1; //this is the index given by the card script, so we know which
    
    // Use this for initialization
    void OnEnable () {

        updateAbilities();
    }

    private void OnDisable()
    {
        abilEquipPage.SetActive(false);

    }

    public void updateAbilities()///so this method can be called from the other scripts
    {

        //clears the array to make sure no values r stored fom previous playthrough

        loadAbilitiesEquipped();
        generatePetCards(); //creates the cards if they dont exist yet
        updateAbilityCards();//gets and changes the cards if there were updates
        autoEquipAbility();//this checks if there are abilities learned and they need to be in the default positions
        updateAbilityEquipIcons();//gets the images of the abils. that are equiped already
        saveAbilitiesEquipped();
    }

    public void checkAbilitiesEquipped() //loads the sprites for the pets that are equipped
    {

        //for(int i=0; i < 3; i++)// b/c there are 3 slots
        //{
        //    if (petGenScript.petIndexEquipped[i] >= 0) //because -1 means no pet is equipped in that slot
        //    {

        //        equipSlot[i].sprite = ListOfPets[petGenScript.petIndexEquipped[i]].petSprite.sprite; //gives the correspoding PETID in the list of pets to get the sprite. For example if the pet index equipped is 5, it will get the 5th pet in the list of pets and get the associated sprite
        //        petManager.petsList[i].GetComponent<Image>().sprite  = ListOfPets[petGenScript.petIndexEquipped[i]].petSprite.sprite;
        //        petManager.petsList[i].SetActive(true);
        //    }

        //}

    }

    public void autoEquipAbility()
    {

        for(int i=0; i< equippedAbilities.Length; i++)
        {

            if (equippedAbilities[i] == -1) //check if the equip slot is -1 (meaning its empty)

            {

                for (int j=0; j < unlockedAbilities.Length; j++)//iterate through the list of abilities and see which ones are unlocked
                {

                    if (unlockedAbilities[j] == 1) //if this ability has been unlocked...
                    {

                        bool abilEquipped = false; //temp bool to see if this ability has been equipped

                        for(int k=0; k < equippedAbilities.Length; k++) //iterate through equipped abilities to see if one matches the unlocked abilities
                        {

                            if(equippedAbilities[k] == j) //the index j is actually the number of the ability 0=flurry, 1 = megaslash etc.
                            {
                                //print("abilit equipped in cell " + k);
                                abilEquipped = true;
                                break;
                            }

                        }

                        if (abilEquipped == false) //if that ability is unlocked at not found, it will automatically equip it.
                        {
                            equippedAbilities[i] = j;
                            break;
                        }
                        else
                        {
                            
                        }


                    } //this will then iterate to the next unlocked ability

                }

            }

        }
        //if updateabilities() is called, then the icons will be updated in the method after this
    }

    public void equipAbility(int abilitySlot)
    {

        //check int selected (aka as the slot)
        checkForDuplicatesEquip(abilitySlot);
       

        //petManager.getNewArrayValues(); //this changes the modifiers for the abilities

    }

    public void checkForDuplicatesEquip(int index)
    {

        bool duplicateFound = false;
        int indexOfDuplicate = -2; //for placeholder

        for (int i = 0; i < 4; i++) //because 4 ability slots
        {
            if (abilityCardholder.GetComponent<AbilityCard>().index == equippedAbilities[i]) //checks if that index is anywhere else in the slots
            {
                duplicateFound = true;
                indexOfDuplicate = i;

                //swap them then break.
                equippedAbilities[i] = equippedAbilities[index];

                equippedAbilities[index] = abilityCardholder.GetComponent<AbilityCard>().index;


            }
        }

        if (duplicateFound == false)
        {
            equippedAbilities[index] = abilityCardholder.GetComponent<AbilityCard>().index;

        }

        updateAbilityEquipIcons(); //sets the ability pictures

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().closeEquipPet();


        saveAbilitiesEquipped(); //this saves the arry of equipped abilities


    }

    public void updateAbilityEquipIcons()
    {
        //first disable the gameobjects of everything so this will only show those that are equipped
        for (int i = 0; i < abilityGameobj.Count; i++)
        {
            abilityGameobj[i].SetActive(false);
        }

            for (int i=0; i<equipSlot.Count; i++)
        {
            if (equippedAbilities[i]==-1)
            {
                equipSlot[i].sprite = blankTransparent;

                abilityLocation[i].gameObject.SetActive(false);
            }
            else
            {

                if (unlockedAbilities[equippedAbilities[i]] == 0)//this is so if an ability is saved but not unlocked it shouldnt show the image **this is the most recent change so fix this if anything breaks!
                {
                    equipSlot[i].sprite = blankTransparent;
                    equippedAbilities[i] = -1;
                }
                else
                {

                    equipSlot[i].sprite = AbilitySprites[equippedAbilities[i]];

                    abilityGameobj[equippedAbilities[i]].transform.position = abilityLocation[i].transform.position;

                    abilityGameobj[equippedAbilities[i]].transform.parent = abilityLocation[i];

                    abilityGameobj[equippedAbilities[i]].SetActive(true);//enables the ability obj

                    abilityLocation[i].gameObject.SetActive(true);

                }
            }

        }

    }

    public void loadAbilitiesEquipped()
    {
        if (PlayerPrefsX.GetIntArray("AbilitiesEquipped").Length == 0) //if the array is empty it has never been called before
        {

            for (int i = 0; i < equippedAbilities.Length; i++)
            {
                equippedAbilities[i] = -1;

            }

        }
        else
        {
            equippedAbilities = PlayerPrefsX.GetIntArray("AbilitiesEquipped");
        }

    }

    public void saveAbilitiesEquipped()
    {

        PlayerPrefsX.SetIntArray("AbilitiesEquipped", equippedAbilities);

    }

    public void checkPetUnlocked() //this might be able to be used for when the player unlocks an ability but none are equipped
    {
        ////find first pet selected

        //bool petUnlocked = false; //if there is a petunlocked it will set this bool to true
        //bool noPetEquipped = true; //this is if the player owns a pet but its not equipped

        //for (int i = 0; i < unlockedAbilities.Length; i++) //check for any pets unlocked
        //{
        //    if (unlockedAbilities[i] == 1)
        //    {
        //        petUnlocked = true;
        //    }
        //}

        //if (petUnlocked == true)
        //{

        //    for (int j = 0; j < unlockedAbilities.Length; j++)//loads the first pet equipped
        //    {
        //        if (petGenScript.petIndexEquipped[j] >= 0)//-1 for  no pet equipped in that slot
        //        {
        //            ListOfPets[petGenScript.petIndexEquipped[j]].TaskOnClick();

        //            noPetEquipped = false;
        //            break;
        //        }

        //    }

        //    if (noPetEquipped == true) { 

        //        for (int i = 0; i < petGenScript.UnlockedPets.Length; i++)//if no pet equipped, load first pet unlocked
        //        {
        //            if (petGenScript.UnlockedPets[i] == 1)
        //            {
        //                ListOfPets[i].TaskOnClick();
        //                break;
        //            }
        //        }

        //    }
           
        //}
        //else //if no pets are unlocked, load the first pet, the fox as a display
        //{
        //    ListOfPets[0].TaskOnClick();//take one for the team foxy boi

        //}

        //////if you cannot find the pet, then make everything blank. This way you arent upgrading a pet that doesnt exist!
    }

    public void generatePetCards()
    {

        //NOTE MAKES SURE THE PLAYER CAN ONLY GENERATE THE PETS THAT ARE AVAILABLE. IN OTHER WORDS -- THERE SHOULD BE NO DUPLICATE NUMBER PETS! You can do this by modifying the Random.Range Values to make sure only a total
        //of 10 numbers will come up in the "Pet Generator script"!!!!!!!!!!!!

        //ALSO, IF ANYTHING HERE CHANGES, THE UPDATE PET SCRIPT ALSO NEEDS TO CHANGE

        if (ListOfAbilities.Count == 0)//there are no pets cards and they need to be generated
        {

            for(int i=0; i<7; i++)//10 amount of pets max at the moment
            {
                GameObject abilCardObj = (GameObject)Instantiate(abilityCardPrefab, abilityCardholder.gameObject.transform);

                AbilityCard abilityCard = abilCardObj.GetComponent<AbilityCard>();

                ListOfAbilities.Add(abilityCard);

                ListOfAbilities[i].abilitySprite.sprite = AbilitySprites[i];

                ListOfAbilities[i].abilityName = AbilityNames[i]; //gives the card the pet name

                ListOfAbilities[i].abilityNameTmpro.text = ListOfAbilities[i].abilityName;

                ListOfAbilities[i].index = i;

                ListOfAbilities[i].abilityMasterScript = this;
            }

        }

    }

    public void updateAbilityCards()
    {

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFF64", out myColorFaded);
        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);

        for (int i = 0; i < 7; i++)//10 amount of pets max at the moment
        {

           if (unlockedAbilities[i] == 1)
            {
                ListOfAbilities[i].isUnlocked = true;

                ListOfAbilities[i].abilitySprite.color = myColorNorm;

                ListOfAbilities[i].abilityLock.SetActive(false);

            }
            else
            {
               
                ListOfAbilities[i].abilitySprite.color = myColorFaded;

                ListOfAbilities[i].abilityLock.SetActive(true);

            }


        }

    }

    public void deselectAll() //this is called from the petcard script when a pet is selected. First it deselects all pets, then it selects the pet you clicked on. This is to make sure we dont select multiple pets
    {

        for(int i=0; i <ListOfAbilities.Count; i++)
        {
            if (ListOfAbilities[i].selected.activeSelf == true)
            {
                ListOfAbilities[i].selected.SetActive(false);
            }
        }

    }

    public void openAbilityWindow()
    {

        checkAbilitiesEquipped();

        abilEquipPage.SetActive(true);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openBag();
        
    }

    public void closeEquipWindow()
    {
        abilEquipPage.SetActive(false);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();



    }
}
