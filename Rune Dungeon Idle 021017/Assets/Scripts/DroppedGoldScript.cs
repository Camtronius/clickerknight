﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;


public class DroppedGoldScript : MonoBehaviour {

	public GameObject goldTextObj; //this needs to spawn seperately so it stays in the same place

	public double goldAwarded = 0;

	public float movingSpeed = 1f;
	public float moveTime = 0;

	public float countdown = 1.5f;


	public bool hitGround = false;

	public Vector3 hitPos;
	public Vector3 totalGoldEndPos;

	// Use this for initialization
	void Start () {
		totalGoldEndPos = Camera.main.GetComponent<MainMenu> ().gemGoldImgPos.position;
	}
	
	// Update is called once per frame
	void Update () {

		if (hitGround == true) {

			countdown -= Time.deltaTime;

			if (countdown<0){
			moveTime += Time.deltaTime * movingSpeed;

			this.gameObject.transform.position = Vector3.Lerp (hitPos, totalGoldEndPos, moveTime-1.9f); //movetime - 2 because movetime is multipled by movespeed 
			}
	
		}

	}

	void OnCollisionEnter2D (Collision2D player) {

	//	print ("colliding with" + player.gameObject);

		if (player.gameObject.tag == "Floor" && hitGround==false) {
			this.gameObject.GetComponent<Animator> ().SetTrigger ("HitGround");
		//	print ("hit ground!");
			Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().goldSound();
		//	goldTextObjSpawned = (GameObject)Instantiate (goldTextObj, new Vector3 (player.gameObject.transform.position.x, player.gameObject.transform.position.y, 90), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		//	goldTextObjSpawned.GetComponent<TextMeshPro>().text = goldAwarded.ToString ();
			GameObject goldTextObjSpawn = Instantiate (goldTextObj, this.transform.position, Quaternion.identity) as GameObject;
			goldTextObjSpawn.GetComponent<RectTransform> ().transform.position = this.transform.localPosition;
			goldTextObjSpawn.GetComponent<TextMeshPro> ().text = LargeNumConverter.setDmg(goldAwarded).ToString ();

			hitPos = this.transform.position;
			hitGround = true;

		}

		if (player.gameObject.tag == "GoldEnd" && hitGround == true) {
			//	Camera.main.GetComponent<MainMenu> ().gemGoldImgPos.gameObject.GetComponent<Animator> ().SetTrigger ("Gain");

		//	print ("hitting gold end!!!!!!!!!!!!!!!");
		//	disableThisObj ();
		}

	}

	public void setGold(double goldAmount){

		//player has both starter and chemist pack

		if (ObscuredPrefs.GetInt ("StarterPack") == 1 && ObscuredPrefs.GetInt ("ChemistPack") == 1) {

			goldAwarded = 1.5f * goldAmount * (1 + Camera.main.GetComponent<MainMenu> ().potionModList [2] + Camera.main.GetComponent<MainMenu>().potionModList[7] + Camera.main.GetComponent<MainMenu>().potionModList[12] + PlayerPrefs.GetFloat("MemsGpBonus"));


		} else if (ObscuredPrefs.GetInt ("StarterPack") == 1) {

			goldAwarded = 1.25f* goldAmount * (1 + Camera.main.GetComponent<MainMenu> ().potionModList [2] + Camera.main.GetComponent<MainMenu>().potionModList[7] + Camera.main.GetComponent<MainMenu>().potionModList[12] + PlayerPrefs.GetFloat("MemsGpBonus"));


		} else if (ObscuredPrefs.GetInt ("ChemistPack") == 1) {
			goldAwarded = 1.25f* goldAmount * (1 + Camera.main.GetComponent<MainMenu> ().potionModList [2] + Camera.main.GetComponent<MainMenu>().potionModList[7] + Camera.main.GetComponent<MainMenu>().potionModList[12] + PlayerPrefs.GetFloat("MemsGpBonus"));


		} else {

			goldAwarded = goldAmount * (1 + Camera.main.GetComponent<MainMenu> ().potionModList [2] + Camera.main.GetComponent<MainMenu>().potionModList[7] + Camera.main.GetComponent<MainMenu>().potionModList[12] + PlayerPrefs.GetFloat("MemsGpBonus"));

		}

}

	public void giveGold(){
		GameObject.FindObjectOfType<ControlPlayer1Character> ().playerGold += goldAwarded; //this is set by the SetGold Method
		
		//set control player one to add the gold amount with an iterator

		//Camera.main.GetComponent<MainMenu> ().playerGoldText.text = GameObject.FindObjectOfType<ControlPlayer1Character> ().playerGold.ToString();
	}

	public void disableThisObj(){
		
		Destroy (this.gameObject);

	}

	public void disableThis(){

		this.gameObject.SetActive (false);
	}
}
