﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EquipmentToggle : MonoBehaviour {

	public GameObject Act1Toggle;
	public GameObject Act2Toggle;

	public GameObject VerticleLayoutHolder;

	public GameObject Act1ToggleForLevelSelect;
	public GameObject Act2ToggleForLevelSelect;

	public GameObject Act1ItemList;
	public GameObject Act2ItemList;

	public GameObject Act1LevelList;
	public GameObject Act2LevelList;

	public GameObject ScrollRectHolder;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowAct1Items()
	{
		/*
		ColorBlock cb = Act2Toggle.GetComponent<Toggle> ().colors;
		cb.normalColor = new Color (255, 255, 255, 255);

		ColorBlock cb2 = Act1Toggle.GetComponent<Toggle> ().colors;
		cb2.normalColor = new Color (90, 90, 90, 255);
*/
	//	Act2Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (255, 255, 255, 255);
	//	Act1Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (90, 90, 90, 255);
	//	Act2ItemList.SetActive (false);
		Act1ItemList.SetActive (true);

	}

	public void ShowAct2ItemsTest()
	{
		/*
		ColorBlock cb = Act1Toggle.GetComponent<Toggle> ().colors;
		cb.normalColor = new Color (255, 255, 255, 255);
		
		ColorBlock cb2 = Act2Toggle.GetComponent<Toggle> ().colors;
		cb2.normalColor = new Color (90, 90, 90, 255);
*/
	//	Act1Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (255, 255, 255, 255);
	//	Act2Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (90, 90, 90, 255);
		Act1ItemList.SetActive (false);
	//	Act2ItemList.SetActive (true);

	}

	public void ShowAct1Levels()
	{
		/*
		ColorBlock cb = Act1Toggle.GetComponent<Toggle> ().colors;
		cb.normalColor = new Color (255, 255, 255, 255);
		
		ColorBlock cb2 = Act2Toggle.GetComponent<Toggle> ().colors;
		cb2.normalColor = new Color (90, 90, 90, 255);
*/
		//	Act1Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (255, 255, 255, 255);
		//	Act2Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (90, 90, 90, 255);
		ScrollRectHolder.GetComponent<ScrollRect> ().content = Act1LevelList.GetComponent<RectTransform>();
//		Act2LevelList.SetActive (false);
		Act1LevelList.SetActive (true);
		
	}

	public void ShowAct2LevelsTest()
	{
		/*
		ColorBlock cb = Act1Toggle.GetComponent<Toggle> ().colors;
		cb.normalColor = new Color (255, 255, 255, 255);
		
		ColorBlock cb2 = Act2Toggle.GetComponent<Toggle> ().colors;
		cb2.normalColor = new Color (90, 90, 90, 255);
*/
		//	Act1Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (255, 255, 255, 255);
		//	Act2Toggle.GetComponent<Toggle> ().colors.normalColor = new Color (90, 90, 90, 255);
		ScrollRectHolder.GetComponent<ScrollRect> ().content = Act2LevelList.GetComponent<RectTransform>();
		Act1LevelList.SetActive (false);
	//	Act2LevelList.SetActive (true);
		
	}
}
