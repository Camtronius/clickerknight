﻿using UnityEngine;
using System.Collections;

public class ExitLevel : MonoBehaviour {

	public GameObject exitLevelWindow; 
	public GameObject yesButton;
	public GameObject noButton;

	// Update is called once per frame
	public void openLevelQuit(){

		exitLevelWindow.SetActive (true);
	
	}

	public void quitLevel(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.MainMenuMusic ();
		}

		Application.LoadLevel ("RunesTestScene");

	}

	public void quitToMain(){
		
		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.MainMenuMusic ();
		}
		
		Application.LoadLevel ("RunesMainScene");
		
	}

	public void returnToGame(){
	
		exitLevelWindow.SetActive (false);
	
	}
}
