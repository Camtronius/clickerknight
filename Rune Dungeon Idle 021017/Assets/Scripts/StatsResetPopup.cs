﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using TMPro;

public class StatsResetPopup : MonoBehaviour {
	public TextMeshProUGUI WouldLikeToWatch;
	public TextMeshProUGUI TotalStats;
	public TextMeshProUGUI AtkStats;
	public TextMeshProUGUI HPStats;
	public TextMeshProUGUI LuckStats;

	public TextMeshProUGUI TestingText;

	public GameObject Player1HPBar;
	public GameObject StatsPane;

	void Start(){
	}

	void Awake () {
		
		if (Advertisement.isSupported) { // If the platform is supported,
			Advertisement.Initialize("113304",true); // initialize Unity Ads.
		} //the true is test mode or not MAKE SURE YOU SAY FALSE BEFORE YOU MAKE PUBLIC


	}

	public void closeStatsPopUp(){
		
		this.gameObject.SetActive (false);
	}

	/*
	public void watchVideoAdStats(){
		

		
		if (Chartboost.hasRewardedVideo(CBLocation.IAPStore)) { 
			Chartboost.showRewardedVideo(CBLocation.IAPStore);
		}
		else {
			// We don't have a cached video right now, but try to get one for next time
			Chartboost.cacheRewardedVideo(CBLocation.IAPStore);
		}



		Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;


	}

	*/

	public void watchVideoAdResetStats(){

		ShowOptions options = new ShowOptions ();
		options.resultCallback = AdCallbackhandler;

		WouldLikeToWatch.text= Advertisement.IsReady ("rewardedVideoZone") ? "Show Ad" : "Waiting...";

		if (Advertisement.IsReady("rewardedVideoZone")) //works when zone removed...
		{
			Advertisement.Show("rewardedVideoZone",options);
		}



	}

	void AdCallbackhandler (ShowResult result)
	{
		switch(result)
		{
		case ShowResult.Finished:
			Debug.Log ("Ad Finished. Rewarding player...");
			WouldLikeToWatch.text = "Ad Finished. Rewarding player...";
			break;
		case ShowResult.Skipped:
			Debug.Log ("Ad skipped. Son, I am dissapointed in you");
			WouldLikeToWatch.text = "Ad skipped. Son, I am dissapointed in you";

			break;
		case ShowResult.Failed:
			Debug.Log("I swear this has never happened to me before");
			WouldLikeToWatch.text = "I swear this has never happened to me before";

			break;
		}
	}

	/*
	
	void didCompleteRewardedVideo(CBLocation location, int reward){

		int AtkStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("atkMod")) - 10; 

			PlayerPrefs.SetFloat ("atkMod", 10);

				AtkStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("atkMod")).ToString ();

		TestingText.text = "stopped after atk";


		int HPStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("HealthMod")) - 10; 

			PlayerPrefs.SetFloat ("HealthMod", 10);

				HPStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("HealthMod")).ToString ();
			//reset hp bar to default
				Player1HPBar.GetComponentInChildren<TextMeshProUGUI> ().text = (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString () + "/" + (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString ();

		TestingText.text = "stopped after hp";

		int LuckStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")) - 5; 

			PlayerPrefs.SetFloat ("luckMod", 5);

			LuckStats.text = Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")).ToString ();

		int newSkillPoints = Mathf.FloorToInt(PlayerPrefs.GetFloat("SkillPoints")) + (AtkStatsSubtracted + HPStatsSubtracted + LuckStatsSubtracted);

		PlayerPrefs.SetFloat("SkillPoints", newSkillPoints);

		TestingText.text = "skill points";

		TotalStats.text = newSkillPoints.ToString();

		StatsPane.GetComponent<StatsPoints> ().updatePlayerInfo ();

		TestingText.text = "went through the method entirely";

		this.gameObject.SetActive (false);
		
	}

	*/

	public void testResetMethod(){

		int AtkStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("atkMod")) - 10; 
		
			PlayerPrefs.SetFloat ("atkMod", 10);

					TestingText.text = "stopped after atk";

						print ("atk stats subbd: " + AtkStatsSubtracted);
		
		
		int HPStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("HealthMod")) - 10; 
		
			PlayerPrefs.SetFloat ("HealthMod", 10);

		//reset hp bar to default
					Player1HPBar.GetComponentInChildren<TextMeshProUGUI> ().text = (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString () + "/" + (PlayerPrefs.GetFloat ("HealthMod") * 100).ToString ();
		
						TestingText.text = "stopped after hp";

		print ("hp stats subbd: " + HPStatsSubtracted);

		print ("luck player pref rounded is : " + Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")));
		
		int LuckStatsSubtracted = Mathf.FloorToInt(PlayerPrefs.GetFloat("luckMod")) - 5; 
		
			PlayerPrefs.SetFloat ("luckMod", 5);

		print ("luck stats subbd: " + LuckStatsSubtracted);


		float newSkillPoints = Mathf.FloorToInt(PlayerPrefs.GetFloat("SkillPoints")) + (AtkStatsSubtracted + HPStatsSubtracted + LuckStatsSubtracted);
		
			PlayerPrefs.SetFloat("SkillPoints", newSkillPoints);

		TestingText.text = "skill points";

			StatsPane.GetComponent<StatsPoints> ().updatePlayerInfo (); //will update all text and points able to spend as new skill pts.
		
		TestingText.text = "went through the method entirely";


	}

}
