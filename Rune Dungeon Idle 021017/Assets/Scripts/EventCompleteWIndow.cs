﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EventCompleteWIndow : MonoBehaviour {

    public TextMeshProUGUI eventCompleteText;

    public GameObject plusOneEventPtText;
    public GameObject plusOneGemText;


    public Button campaignButton;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void closeEventPopUp()
    {
        Camera.main.GetComponent<MainMenu>().backFromItemDescription(); //this window hasnt been added to this yet.
        campaignButton.onClick.Invoke();
    }

    public void closeThis()
    {
        Camera.main.GetComponent<MainMenu>().backFromItemDescription(); //this window hasnt been added to this yet.
    }

    public void eventWindowOpenWoosh()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().LevelUp(); //this window hasnt been added to this yet.
    }
}
