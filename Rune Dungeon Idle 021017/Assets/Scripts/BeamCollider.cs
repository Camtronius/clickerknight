﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;


public class BeamCollider : MonoBehaviour {

	public int createdByPlayerCollider = 0;
	public Vector3 collidedPosition;
	public bool resetCollision = false;
	public GameObject ScreenShaker;
	//public GameObject waterBurstParticles;
	public double spellDMG = 0;
	public bool isCrit = false;


	// Use this for initialization
	void Start () {
	
		ScreenShaker = GameObject.Find ("ScreenShakerController");

//		print ("beam collider on!!");

		if (this.gameObject.GetComponentInParent<BeamIncrementer> ().createdByPlayer == 1) {

			GameObject player1ShotSpawn = GameObject.Find ("ShotSpawnLeft");

			this.gameObject.transform.position = player1ShotSpawn.transform.position;

			createdByPlayerCollider = 1;
			this.gameObject.GetComponent<ConstantForce2D>().force = new Vector2(150,0);
		}

//		if (this.gameObject.GetComponentInParent<BeamIncrementer> ().createdByPlayer == 2) {
//
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
//			GameObject player2ShotSpawn = GameObject.Find ("Player2Character/ShotSpawn");
//				this.gameObject.transform.position = player2ShotSpawn.transform.position;
//			}else{
//				//if campaign mode
//				if(GameObject.Find ("Enemy/EvilGuy")!=null){
//				GameObject player2ShotSpawn = GameObject.Find ("Enemy/EvilGuy/ShotSpawn");
//				this.gameObject.transform.position = player2ShotSpawn.transform.position;
//				}
//				if(GameObject.Find ("Enemy/FinalBoss")!=null){
//					GameObject player2ShotSpawn = GameObject.Find ("Enemy/FinalBoss/ShotSpawn");
//					this.gameObject.transform.position = player2ShotSpawn.transform.position;
//				}
//			}
//
//		
//			createdByPlayerCollider = 2;
//			this.gameObject.GetComponent<ConstantForce2D>().force = new Vector2(-150,0);
//
//		}
	}

	void OnTriggerEnter2D (Collider2D player) {


//		if (player.gameObject.tag == "Player2" && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			print ("there is a collision with player2!!!");
//
//
//			//where Damage is calcualted
//			if(player.gameObject.GetComponent<ControlPlayer2Character>().isShielded==false){
//
//				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBeam") 
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[2] //adding fire item mods
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod
//				
//				//condition for super effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				//condition for Not very effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//
//					calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//					
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//				}
//
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//					
//
//				}
//				
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createDrownedRunes();
//
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().DrownedRunesManager (0, 1); //this used to be 0,1
//				}
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().DrownedRunesManager (0, 2);
//				}
//			
//				//hitting player 2
//
//
//				//check for crit against p2
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1
//				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
//				   ){ //did the player crit on their turn??
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//
//				player.gameObject.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p2 notification
//
//				waterBurstParticles = Instantiate (waterBurstParticles, new Vector3 (this.transform.position.x, 
//				                                                                 this.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//
//
//			}
//			//End where Damage is calcualted
//
//
//
//	// this is for multiple collisions in online gameplay
////				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
////							//checks if campaign mode or not
////					player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
////						player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
////
////					collidedPosition = player.gameObject.transform.position;
////					//}		
////						if (resetCollision == false) {
////							StartCoroutine("CollisionReset");
////						}
////					resetCollision = true;
////
////				}
//
//		} //end of checking is player is player 2

//		if (player.gameObject.tag == "Player1" && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			print ("there is a collision with player1!!!");
//
//			//where Damage is calcualted
//			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){
//
//				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//
//				
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBeam") 
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[2] //adding fire item mods
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod
//				
//				//condition for super effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
//
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				//condition for Not very effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite){
//
//					calculatedHealthLoss = (0.5f)*calculatedHealthLoss;
//					
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//					
//
//				}
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createDrownedRunes();
//
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().DrownedRunesManager (0, 1); //this used to be 0,1
//				}
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
//					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().DrownedRunesManager (0, 2);
//				}
//
//
//					//check for crit against p1
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//					   ){ //did the player crit on their turn??
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//
//				player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p2 notification
//
//			}
//			//End where Damage is calcualted
//
//		//	player.gameObject.GetComponentInChildren<BoxCollider2D> ().enabled = false;
//	
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){ //checks if colliding with player 1
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//			
//			collidedPosition = player.gameObject.transform.position;
//			}		
//
//			if (resetCollision == false) {
//				
//				StartCoroutine("CollisionReset");
//			}
//			
//			resetCollision = true;
//
//			waterBurstParticles = Instantiate (waterBurstParticles, new Vector3 (this.transform.position.x, 
//			                                                                 this.transform.position.y, 0), Quaternion.identity) as GameObject;
//
//		
//		}


			if (player.gameObject.tag == "Player2" ) { //to damage the enemy during campaign // when marked as P2
				
				damageEnemy(player);
			}

				if (player.gameObject.tag == "Enemy" ) { //to damage the enemy during campaign // when marked as P2
					
					damageEnemy(player);
				}

					if (player.gameObject.tag == "Player1" ) { //to damage the enemy during campaign // when marked as P2
						
						damagePlayer(player);
					}
	}

	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method

//		print ("colling with player2 enemy guy");
		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().waterSpellHit ();


		if (player.gameObject.GetComponent<ControlEnemy> ().isShielded == false) {
			
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

			double calculatedHealthLoss = spellDMG;

			if (isCrit == false) {
				Camera.main.GetComponent<MainMenu> ().SpellDamage ((spellDMG)); //sends the spell damage to the spell dmg text thing
			} else {

				Camera.main.GetComponent<MainMenu> ().spellCritText ((spellDMG)); //sends the spell damage to the spell dmg text thing

			}
			
			//condition for super effective
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().FireSprite) {
//
//				calculatedHealthLoss = 2 * calculatedHealthLoss;
//								
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (1);//1 = super effective, 2 = not very effectiveness
//
//			}
//			
//			//condition for Not very effective
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WaterSprite) {
//				
//				calculatedHealthLoss = (.5f) * calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (2);//1 = super effective, 2 = not very effectiveness
//
//			}
//			
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().EarthSprite ||
//				Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WindSprite ||
//				Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer2.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite) {
//
//			}

//				if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() == true){ //calls the method no further code needed ;D
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					print("CRITS!!!!!!!!!");
//				}
				
					player.gameObject.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
			int randoStun = Random.Range (0, 100);


			if (Camera.main.GetComponent<MainMenu> ().SpellStunChance >= randoStun && Camera.main.GetComponent<MainMenu> ().SpellStunEnabled == true) {
				player.gameObject.GetComponent<ControlEnemy> ().EnemyStunned = true;
			}
				//		Camera.main.GetComponent<CampaignGameOrganizer> ().damageEnemyNotification (calculatedHealthLoss);

			if (calculatedHealthLoss >= player.gameObject.GetComponent<ControlEnemy> ().playerHealthCurrent) {
				//player.gameObject.GetComponent<ControlEnemy> ().playerDead = true;
				this.gameObject.GetComponent<CircleCollider2D> ().isTrigger = false;
				//player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
				//player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
			}
		
			//waterBurstParticles = Instantiate (waterBurstParticles, new Vector3 (this.transform.position.x, 
			//                                                                 this.transform.position.y, 85), Quaternion.identity) as GameObject;


		//	this.gameObject.GetComponentInChildren<CircleCollider2D>().enabled=false; //so it doesnt dmg enemy twice
		
		}
	}

	public void damagePlayer(Collider2D player){
//		
//			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//			
//		if (player.gameObject.GetComponent<ControlPlayer1Character> ().isShielded == false) {
//			
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WaterSprite) {
//
//				DamageToPlayer = 0.5f * DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (2);//1 = super effective, 2 = not very effectiveness
//				player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//
//				
//			}
//			
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().WindSprite) {
//				
//				//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//
//			}
//			
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().EarthSprite
//			    || Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//			    == Camera.main.GetComponent<CampaignGameOrganizer> ().nullsprite) {
//
//				player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//
//				
//			}
//			
//			if (Camera.main.GetComponent<CampaignGameOrganizer> ().LastSpellPlayer1.GetComponent<Image> ().sprite 
//				== Camera.main.GetComponent<CampaignGameOrganizer> ().FireSprite) {
//				
//				DamageToPlayer = 2 * DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer> ().effectiveness (1);//1 = super effective, 2 = not very effectiveness
//				player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//
//				
//			}
//
//			
//
//			if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
//			
//				player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
//				player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//				player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//	
//			
//			}
//			
//
//			//	player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//			Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//
//
//			waterBurstParticles = Instantiate (waterBurstParticles, new Vector3 (this.transform.position.x, 
//			                                                                     this.transform.position.y, 85), Quaternion.identity) as GameObject;
//
//		
//		}
		
	}

	IEnumerator CollisionReset(){
		
		while (true) {

			yield return new WaitForSeconds(0.5f);
			this.gameObject.transform.position = collidedPosition;
		}

	}


}
