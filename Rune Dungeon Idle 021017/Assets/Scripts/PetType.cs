﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetType : MonoBehaviour {

	public enum petType {Cat,Rabbit,Penguin,Fox};
	public petType TypeOfPet;
	public Button buttonObj; //for this pet
	public GameObject lockObj;

	// Use this for initialization
	void OnEnable () {

		//checks if the pet is unlocked
		if (PlayerPrefs.GetInt (TypeOfPet + "Unlocked") == 0) {
			//not unlocked
			buttonObj.interactable = false;
		} else {
			buttonObj.interactable = true;

			lockObj.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
