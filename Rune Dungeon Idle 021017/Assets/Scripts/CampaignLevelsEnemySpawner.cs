﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;


public class CampaignLevelsEnemySpawner : MonoBehaviour {

	public List<GameObject> enemyList = new List<GameObject>();


	//public SpriteRenderer travelingSky;

	public GameObject travelingFrontMostGround;
	public GameObject travelingFrontGround;
	public GameObject travelingGround;
	public GameObject travelingForeground;
	public GameObject travelingBackDrop;


	public GameObject dimensionalFXobj;

	public float GroundSpeed = 0;
	public float CloudSpeed = 0;


	public int enemyNumber = 0;
	public int enemyTypeNumber = 0;
	public double bossMultiplier = 0; //how many enemies the boss is worth
	public double bossHPMultiplier = 0; //how many enemies the boss is worth

	public float enemyMultiplier = 0; //how many enemies the boss is worth


	public int FinalEnemyNumber = 1;

	public GameObject Player1;
	public Vector2 Player1StartingPos;
	public Vector2 endPos;

	public GameObject ItemChest; //same as above
    public GameObject EventChest; //same as above


    public GameObject PinkSlime; //1 
	public GameObject Skeleton;  //2 
	public GameObject TinyMushGuyRedHat; //3
	public GameObject SkeletonWarriorShield; //4 
	public GameObject SkeletonWarriorDualSword;  //5
	public GameObject SkeletonMageStaff; //6
	public GameObject SkeletonMageFireHands; //7 
	public GameObject SkeletonMageFireHandsAndStaff; //8
	public GameObject SkeletonMageBlueFireHands; //9
	public GameObject SkeletonMageKing; //10
	public GameObject SkeletonMageGreenFireHands; //11
	public GameObject SkeletonKing; //12
	public GameObject PinkSlimeWarrior; //13
	public GameObject GreenJelloSlime; //14
	public GameObject PurpleSlime; //15
	public GameObject DarkSlime; //16
	public GameObject GoldSlime; //17
	public GameObject DarkPinkSlime; //18
	public GameObject MushSlime; //19
	public GameObject SpikyMushSlime; //20
	public GameObject GreenToxicMushSlime; //21
	public GameObject YellowToxicMushSlime; //22
	public GameObject SpearReptile; //23
	public GameObject ShamanReptile; //24
	public GameObject SwordReptile; //25
	public GameObject TinyMushGuyGreenHat; //26
	public GameObject TinyMushGuyYellowHat; //27
	public GameObject TinyMushGuyPurpleHat; //28
	public GameObject evilChest; //29
	public GameObject WhiteWolf; //30
	public GameObject BrownWolf; //31
	public GameObject BlackWolf; //32
    public GameObject Egg; //33

    public Sprite blankTransparentSprite;
	public Sprite strawberry;
	public Sprite blueRose;
	public Sprite leafCraft;
	public Sprite banannaCraft;

    public GameObject eventPotionSprite;
    public float eventPotionSpeedMod =1f;
    public bool eventPotionSpeedUp = false;

    public List<GameObject> craftLocationEndUnchanging = new List<GameObject> ();
	public List<GameObject> craftHolder = new List<GameObject>();
	public List<GameObject> craftLocationEnd = new List<GameObject>();
    	
	public GameObject FinalBoss; //same as above
	public GameObject DeadSoldier; //for the tutorial
	public GameObject Portal; //for the tutorial

	public GameObject EnemySpawned;
		public GameObject TutorialBossHolder;

	public GameObject EnemyHealthBar;
	public GameObject EndOfLevelPane;
	public GameObject EnemyTurnPane;
	public GameObject ExitButton;
	public GameObject WhosTurnPane;

	public Vector3 EnemyStartingPos;
	public float EnemyStartingPosZ;
	public Vector3 EnemyEndPos;
	public RectTransform EnemyEndPositionObj;
		
	public float movingSpeed = 0.9f;
	public float moveTime = 0;
	public float chestTimer = 0;
    public float eventTimer = 0;
    public float eventTimerMax = 120;

    public Slider eventSlider;

    public TextMeshProUGUI eventSliderText;
    public bool treasureFound = false;



    public bool playerTraveling = false;
	public bool enemyDefeated = false;

	public EnemyStatsInheriter InheritStats;
	// Use this for initialization

	public bool chestSpawned = false;
	public MainMenu cameraObj;


	void OnAwake(){

	}

	void Start () {

		InheritStats = GameObject.Find("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter>();
		cameraObj = Camera.main.GetComponent<MainMenu> ();
			//sets the background based on what the stats inheriters specifies
	//		travelingGround.GetComponent<Image> ().material.mainTexture = InheritStats.pathTexture;
	//		travelingForeground.GetComponent<Image> ().material.mainTexture = InheritStats.foregroundTexture;
//			travelingBackDrop.GetComponent<Image> ().material.mainTexture = InheritStats.backgroundTexture;

			if (InheritStats.dimensionalFX == true) {
			dimensionalFXobj.SetActive(true);
		//	travelingForeground.GetComponent<Image>().enabled=false;
			travelingBackDrop.GetComponent<Image>().enabled=false;

			}

        //	Player1.transform.GetComponent<Animator> ().SetBool ("isRunning", true);
        if (InheritStats.dungeonMode == false)
        {

            setMoveTime(0);
        }
        else
        {

            setMoveTime(-4); //just to give the illusion of traveling farther
        }

        if (InheritStats.eventMode == true)
        {
            eventSlider.gameObject.SetActive(true);

            //this is for event potions stuff
            if (PlayerPrefs.GetInt("EventSpeed") == 1)
            {
                eventPotionSpeedUp = true;
                eventPotionSpeedMod = 1.25f;
            }
            else
            {
                eventPotionSpeedUp = false;

            }

        }

        //	print ("start is being called");    

       

    }


	// Update is called once per frame
	void Update () {

		if (chestTimer >= 0 && cameraObj.windowOpen==false) {

			chestTimer -= Time.deltaTime;

		}

        if(InheritStats.eventMode == true && cameraObj.windowOpen==false)
        {
            if (eventTimer < eventTimerMax)
            {

                if (eventPotionSpeedUp == true && eventPotionSprite.activeSelf==false)
                {
                    eventPotionSprite.SetActive(true);
                    eventPotionSpeedMod = 1.5f;
                }

                eventTimer += Time.deltaTime*eventPotionSpeedMod;//normally its one, but if the potion was used, its now 1.25f

                eventSlider.value = eventTimer / eventTimerMax;

            }else if (treasureFound==false)
            {
                treasureFound = true;
                eventSliderText.text = "Treasure Found!";
            }

        }

        

	//	CloudSpeed -= movingSpeed * Time.deltaTime;


	//	travelingSky.material.SetTextureOffset ("_MainTex", new Vector2 (CloudSpeed/50f, 0));


		if (playerTraveling == true && Time.deltaTime<0.1f) {

			if(Player1.transform.GetComponent<Animator> ().GetBool ("isRunning")==false){


				Player1.transform.GetComponent<Animator> ().SetBool ("isRunning", true);

                cameraObj.resetBowAnimBools(false);

            }

            GroundSpeed += movingSpeed * Time.deltaTime;

			moveTime += Time.deltaTime * movingSpeed;

			//in order of that which is shown
			travelingBackDrop.GetComponent<SpriteRenderer>().material.SetTextureOffset ("_MainTex", new Vector2 (GroundSpeed/15, 1));

			travelingForeground.GetComponent<SpriteRenderer>().material.SetTextureOffset ("_MainTex", new Vector2 (GroundSpeed/3.5f, 0));

			travelingGround.GetComponent<SpriteRenderer>().material.SetTextureOffset ("_MainTex", new Vector2 (GroundSpeed/3f, 1)); //was grnd/2 and 1.02f

			if (travelingFrontGround.activeSelf == true) {
				travelingFrontGround.GetComponent<SpriteRenderer> ().material.SetTextureOffset ("_MainTex", new Vector2 (GroundSpeed / 2.5f, 0));
			}

			if (travelingFrontMostGround.activeSelf == true) { //this is for example the bush that moves in front of the player
				travelingFrontMostGround.GetComponent<SpriteRenderer> ().material.SetTextureOffset ("_MainTex", new Vector2 (GroundSpeed / 2.5f, 0));
			}



			if (moveTime < 1 && enemyDefeated==false) {

				blockPlayerClicks(true); //this will block the player cliks until its his turn again.

//				Player1.transform.parent.position = Vector2.Lerp (Player1StartingPos, endPos, moveTime);
			}

			if (moveTime > 1 && moveTime < 2) {

				EnemySpawned.transform.position = Vector3.Lerp (EnemyStartingPos, EnemyEndPos, moveTime-1f); //movetime - 2 because movetime is multipled by movespeed 

				//craft lerp range
			

			}

			if (moveTime >= 2) {

				playerTraveling = false;

				EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=true; //so the spells only hit when the enemy has arrived at the location

				EnemySpawned.GetComponentInChildren<ControlEnemy>().isHittable = true;
				Player1.transform.GetComponent<Animator> ().SetBool ("isRunning", false);
			
				//make a seperate health bar animation so it slides in 
				if(EnemyHealthBar.activeSelf==false){
				EnemyHealthBar.SetActive(true); //sets the enemy health bar to active so we can see it 

				

				}else{

				}


				

				blockPlayerClicks(false); //this will block the player cliks until its his turn again.

				//this is to move the final boos into position for the tutorial

				for (int i = 0; i < craftHolder.Count; i++) {

					craftHolder[i].transform.position =craftLocationEnd[i].transform.position; //movetime - 2 because movetime is multipled by movespeed 
					craftHolder[i].SetActive(true);

					if (craftHolder [i].GetComponent<Button> ().interactable == true) {
						
						craftHolder [i].GetComponent<IndividualIngredient> ().checkAutoCollect ();
					}
				}
			
			}
		}


	}


	public void setMoveTime(int moveTimeSet){

		enemyNumber += 1;

	//	enemyDefeated = true;
		moveTime = moveTimeSet;
	//	print ("the movetime is " + moveTime);
		playerTraveling = true;
		nextSpawn ();
	//	print ("set move time is being called");
	}

	public void nextSpawn(){
        //this will eventually go through the list to detemine what the next spawn is, for now its just skels
        if(InheritStats.dungeonMode==true)
        {
            dailyDungeonSpawn();
        }
        else if(InheritStats.eventMode==true)
        {
            eventSpawn();//for limited timed events
        }
        else
        {
            campaignSpawn();

        }
    }

    public  void campaignSpawn()
    {

        int enemySpawnType = 0; //just to set it so i can use it again

        if (chestTimer > 0)
        {

            int chestChanceSpawn = Random.Range(1, 101); //initial chance to spawn a chest is 14%

            if (chestChanceSpawn < 12 && enemyNumber != 1 && chestSpawned == false)
            { //was 14
                //to make sure tge chests are spawnbed with correct stats
                bossMultiplier = 1;
                bossHPMultiplier = 1;
                enemyMultiplier = Random.Range(4, 8) * 0.25f;

                spawnItemChest();
                enemyTypeNumber = enemySpawnType; //sets so we know what enemy type is playing
                                                  //	chestTimer = 150;
                chestSpawned = true;

            }
            else if (enemyNumber == 1 && PlayerPrefs.GetInt("Boss" + (InheritStats.CurrentLevel - 1).ToString()) == 0 && InheritStats.CurrentLevel != 1)
            { //means the first enemy and the boss hasnt been beaten yet

                //		print ("the current level for the boss to fight according to inheristats : " + InheritStats.CurrentLevel.ToString ());

                enemyMultiplier = 1;
                bossMultiplier = (double)InheritStats.data[1]["BOSSDMGX"];
                bossHPMultiplier = (double)InheritStats.data[1]["BOSSHPX"]; // this was 1.5
                spawnEnemies(3); //3 is the boss number in the array of monsters 
                EnemySpawned.GetComponentInChildren<ControlEnemy>().isBoss = true; //makes the enemy a boss - this is for the death screen at a boss
                EnemySpawned.GetComponentInChildren<EnemyRagdoll>().isBoss = true; //makes the enemy a boss - this is for the death screen at a boss

                enlargeBoss();//this may have to change if the enemies dont get reset back to normal afterwards (including shadows)


                chestSpawned = false;

                if (PlayerPrefs.GetInt("FoughtFirstBoss") == 0)
                {
                    //	disabling this for now to update unity and fix IAP issue
                    TutorialController tutObj = Camera.main.GetComponent<MainMenu>().TutorialObject;

                    //add wizard surprised face
                    tutObj.wizardSprite.sprite = tutObj.wizardSprites[3];

                    tutObj.bossTut = true;
                    tutObj.twoPartTutorial = true;



                    tutObj.TutPartOne("Watch Out! Every new world has a <color=#dd480a>Boss Guardian</color>!");
                    tutObj.secondThingToSay = "Bosses are very strong, and <color=#dd480a>you can't pass until they are defeated</color>! Good Luck!";
                    cameraObj.persistantSoundManager.GetComponent<SoundManager>().oldManConfident();

                    //player pref is set to 1 

                }


            }
            else
            {
                bossMultiplier = 1;
                bossHPMultiplier = 1;
                enemyMultiplier = Random.Range(4, 8) * 0.25f;
                enemySpawnType = Random.Range(0, 3); //random 0,1,2 which corresponds to an array of enemy types

                //	print("enemy spawn type is : "+ enemySpawnType);
                spawnEnemies(enemySpawnType);
                chestSpawned = false;

            }

        }
        else if (chestTimer <= 0 && chestSpawned == false)
        {
            //to make sure tge chests are spawnbed with correct stats
            bossMultiplier = 1;
            bossHPMultiplier = 1;
            enemyMultiplier = Random.Range(4, 8) * 0.25f;

            spawnItemChest();
            enemyTypeNumber = enemySpawnType; //sets so we know what enemy type is playing
                                              //	chestTimer = 150;
            chestSpawned = true; //so yo cant have more than 1 chest spawned at a time

        }
        else
        {

            bossMultiplier = 1;
            bossHPMultiplier = 1;
            enemyMultiplier = Random.Range(4, 8) * 0.25f;
            enemySpawnType = Random.Range(0, 3); //random 0,1,2 which corresponds to an array of enemy types

            //	print("enemy spawn type is : "+ enemySpawnType);
            spawnEnemies(enemySpawnType);
            chestSpawned = false;

        }

        decideCraftsToSpawn();

    }

    public void dailyDungeonSpawn()
    {

        int enemySpawnType = 0; //just to set it so i can use it again     

            if (enemyNumber <= 3 ) //b/c enemy number starts at 1 (1-3 will be bosses, 4 is egg)
            {
                //		print ("the current level for the boss to fight according to inheristats : " + InheritStats.CurrentLevel.ToString ());

                enemyMultiplier = 1;

                int[] dungeonArray = PlayerPrefsX.GetIntArray("DungeonArray"); //this is to get the stored dungeon data. 0 is the enemy type (unsed in enemy statsinherit) 1 is the level of monster to spawn
                int[] bonusDungeonArray = PlayerPrefsX.GetIntArray("BonusDungeonArray"); //this is to get the stored dungeon data. 0 is the enemy type (unsed in enemy statsinherit) 1 is the level of monster to spawn


                if (InheritStats.dungeonModeBonus == false)
                {
                    bossMultiplier = (double)InheritStats.dungeonData[0]["BOSSDMGX"];

                    //note: because usually we have (current lv-1), this will be like being on the next row. For world 2 we have 2.50X HP. So total HP multiplier is 3*2.5=7.5. 
                    bossHPMultiplier = (double)InheritStats.dungeonData[0]["BOSSHPX"] * 3f; // makes the dungeon boss 3x as strong as the current boss of that level. //this can be adjusted as needed later
                                                                                                   //for example on level 5, the multiplier is 4*3 = 12. 4 from the spreadsheet for BossHPX row 7 (just add 2 to the level to get the row) and multiply this by the bass HP 362591 for lv5,row 7 to get the boss HP
               // print("the bonus dungone is " + InheritStats.data[dungeonArray[3]]["BOSSDMGX"].ToString());

                }
                else
                {
                    bossMultiplier = (double)InheritStats.dungeonData[2]["BOSSDMGX"];
                    bossHPMultiplier = (double)InheritStats.dungeonData[2]["BOSSHPX"] * 3f;

                }
                    spawnEnemies(enemyNumber); //1-3 are bosses. enemy num starts at 1 so this will generate 3 bosses in a row

                    EnemySpawned.GetComponentInChildren<ControlEnemy>().isBoss = true; //makes the enemy a boss - this is for the death screen at a boss
                    EnemySpawned.GetComponentInChildren<EnemyRagdoll>().isBoss = true; //makes the enemy a boss - this is for the death screen at a boss

                    enlargeBoss();
            }

             else if (enemyNumber == 4)
            {

              //  print("spawning egg!");
        
                    bossMultiplier = 1;
                   // int[] dungeonArray = PlayerPrefsX.GetIntArray("DungeonArray"); //too make the egg a bit difficult to destroy

                    bossHPMultiplier = (double)3f;
                    enemyMultiplier = Random.Range(4, 8) * 0.25f;
                    enemySpawnType = 0; //this is what makes the egg spawn. 0 in the list is the egg

                
                    spawnEnemies(enemySpawnType);


            }
            else
            {


            }

        decideCraftsToSpawn();

    }

    public void eventSpawn()
    {

        int enemySpawnType = 0; //just to set it so i can use it again     

        bossMultiplier = 1;
        bossHPMultiplier = 1;

        if (eventTimer < eventTimerMax ) //b/c enemy number starts at 1 (1-3 will be bosses, 4 is egg)
        {
            //		print ("the current level for the boss to fight according to inheristats : " + InheritStats.CurrentLevel.ToString ());

            enemyMultiplier = 1; //the enemies hp and atk are already set in enemystatsinheriter 

            enemySpawnType = Random.Range(0, 12); //random 0,1,2 which corresponds to an array of enemy types


            spawnEnemiesEvent(enemySpawnType); //1-3 are bosses. enemy num starts at 1 so this will generate 3 bosses in a row

        }

        else //this is where u spawn the chest and its completed
        {


            Camera.main.GetComponent<MainMenu>().dungeonCelebration.setDungeonCompleteText("Treasure Found!");

            Camera.main.GetComponent<MainMenu>().dungeonCelebration.gameObject.SetActive(true);

            Camera.main.GetComponent<MainMenu>().dungeonCelebration.GetComponent<Animator>().SetTrigger("TreasureFound");



            enemyTypeNumber = enemySpawnType; //sets so we know what enemy type is playing

            spawnEventChest();



        }


        decideCraftsToSpawn();

    }

    public void enlargeBoss()
    {

        Transform bossTransform = EnemySpawned.GetComponentInParent<Transform>();

        float xScale = bossTransform.localScale.x * 1.3f;
        float yScale = bossTransform.localScale.y * 1.3f;

        float yPos = bossTransform.position.y * 0.25f;

        float xPos = bossTransform.position.x;
        // float zPos = bossTransform.position.z;

        bossTransform.transform.localScale = new Vector3(xScale, yScale, 1);
        EnemyEndPos.y = EnemyEndPos.y + yPos; //so the boss gets raised up because he was scaled up. Next need to scale the shadow too...

        //defaults are 8,3 for the shadow
        Camera.main.GetComponent<MainMenu>().shadowObj.GetComponent<CircleShadow>().xInitialScale = 13;
        Camera.main.GetComponent<MainMenu>().shadowObj.GetComponent<CircleShadow>().xScaleMultiplier = 3;
        //apply to all bosses once u find out it works

    }

    public void resetEnemyShadow()
    {

        Camera.main.GetComponent<MainMenu>().shadowObj.GetComponent<CircleShadow>().xInitialScale = 8;
        Camera.main.GetComponent<MainMenu>().shadowObj.GetComponent<CircleShadow>().xScaleMultiplier = 3;
    }



    public void spawnEnemies(int enemySpawnNum){


        resetEnemyShadow(); //in case this was modified for a boss


        switch (InheritStats.enemyArray[enemySpawnNum]) { //gets the array that is prefilled with 3 enemy types per level and one boss which is index 3


		case(0):
			EndOfLevel();
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(1):
			spawnSlime (PinkSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(2):
			spawnSkele(); //spawns skelee fighter
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(3):
			spawnMushBoi(TinyMushGuyRedHat); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(4):
			spawnSkeleWarrior (SkeletonWarriorShield);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(5):
			spawnSkeleWarrior (SkeletonWarriorDualSword);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(6):
			spawnSkeleMage (SkeletonMageStaff);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(7):
			spawnSkeleMage (SkeletonMageFireHands);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(8):
			spawnSkeleMage (SkeletonMageFireHandsAndStaff);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(9):
			spawnSkeleMage (SkeletonMageBlueFireHands);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(10):
			spawnSkeleMage (SkeletonMageKing);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;

		case(11):
			spawnSkeleMage (SkeletonMageGreenFireHands);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
//		case(12):
//			spawnItemChest();
//			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
//			break;
		case(12):
			spawnSkeleWarrior (SkeletonKing);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(13):
			spawnSlime (PinkSlimeWarrior);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(14):
			spawnSlime (GreenJelloSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(15):
			spawnSlime (PurpleSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(16):
			spawnSlime (DarkSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(17):
			spawnSlime (GoldSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(18):
			spawnSlime (DarkPinkSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(19):
			spawnSlime (MushSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(20):
			spawnSlime (SpikyMushSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(21):
			spawnSlime (GreenToxicMushSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(22):
			spawnSlime (YellowToxicMushSlime);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(23):
			spawnReptillian(SpearReptile);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(24):
			spawnReptillian(ShamanReptile);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(25):
			spawnReptillian(SwordReptile);
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(26):
			spawnMushBoi(TinyMushGuyGreenHat); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(27):
			spawnMushBoi(TinyMushGuyYellowHat); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(28):
			spawnMushBoi(TinyMushGuyPurpleHat); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(29):
			spawnEvilChest(evilChest); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(30):
			spawnWerewolf(WhiteWolf); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(31):
			spawnWerewolf(BrownWolf); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
		case(32):
			spawnWerewolf(BlackWolf); //spawns slime
			enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
			break;
        case (33):
            spawnEgg(Egg); //spawns slime
            enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
            break;

        }

	//	print("the inheristats enemy to be spawned is.... " + InheritStats.enemyArray[enemySpawnNum]);


	}



	public void decideCraftsToSpawn(){

		for (int i = 0; i < craftHolder.Count; i++) {
			craftHolder[i].SetActive(false);

			craftHolder [i].GetComponent<Image> ().sprite = blankTransparentSprite;
			craftHolder [i].GetComponentInChildren<ParticleSystemRenderer> ().enabled = false;
			craftHolder [i].GetComponent<Image> ().raycastTarget = false;
			craftHolder [i].GetComponent<Button> ().interactable = false;

			craftLocationEnd[i].transform.position = new Vector3 (craftLocationEndUnchanging[i].transform.position.x + Random.Range (2, 4),craftLocationEndUnchanging[i].transform.position.y,craftLocationEndUnchanging[i].transform.position.z);
		}

		if (PlayerPrefs.GetInt ("CurrentLevel") >= 3) { //spawn gems only if you are on level 3 or more
			
				int numOfCrafts = Random.Range (1, 3);
			

					for (int i = 0; i < numOfCrafts; i++) {

						spawnCrafts(Random.Range(0,4)); //randomizes from 0,1,2 depending on the amount of crafts to spawn, 3 is a no spawn
					}

		}
			
	}

	public void spawnCrafts(int craftSpawn){

		switch (craftSpawn) { //gets the array that is prefilled with 3 enemy types per level and one boss which is index 3



		case(0):
			spawnStrawberry();

			break;

		case(1):
			spawnRose();

			break;

		case(2):
			spawnLeaf();


			break;
		case(3):
		//	spawnBananna();


			break;

		}

	}

	public void spawnStrawberry(){ //normal skele spawn
	
		for (int i = 0; i < craftHolder.Count; i++) {

			if (craftHolder [i].GetComponent<Image> ().sprite == blankTransparentSprite) {

				craftHolder [i].GetComponent<Image> ().sprite = strawberry;
				craftHolder [i].GetComponent<Image> ().raycastTarget = true; //this is so clicks wont get blocked
				craftHolder [i].GetComponent<Button> ().interactable = true;
				craftHolder [i].GetComponent<IndividualIngredient> ().IngredientTypeSelect = IndividualIngredient.IngredientType.Strawberry;
				craftHolder [i].GetComponentInChildren<ParticleSystemRenderer> ().enabled = true;

				break;
			}

		}

	}

	public void spawnRose(){ //normal skele spawn
	
		for (int i = 0; i < craftHolder.Count; i++) {

			if (craftHolder [i].GetComponent<Image> ().sprite == blankTransparentSprite) {

				craftHolder [i].GetComponent<Image> ().sprite = blueRose;
				craftHolder [i].GetComponent<Image> ().raycastTarget = true;
				craftHolder [i].GetComponent<Button> ().interactable = true;
				craftHolder [i].GetComponent<IndividualIngredient> ().IngredientTypeSelect = IndividualIngredient.IngredientType.BlueRose;
				craftHolder [i].GetComponentInChildren<ParticleSystemRenderer> ().enabled = true;


				break;

			}

		}


	}

	public void spawnLeaf(){ //normal skele spawn
	
		for (int i = 0; i < craftHolder.Count; i++) {

			if (craftHolder [i].GetComponent<Image> ().sprite == blankTransparentSprite) {

				craftHolder [i].GetComponent<Image> ().sprite = leafCraft;
				craftHolder [i].GetComponent<Image> ().raycastTarget = true;
				craftHolder [i].GetComponent<Button> ().interactable = true;
				craftHolder [i].GetComponent<IndividualIngredient> ().IngredientTypeSelect =  IndividualIngredient.IngredientType.GreenLeaf;
				craftHolder [i].GetComponentInChildren<ParticleSystemRenderer> ().enabled = true;


				break;

			}

		}


	}

	public void spawnBananna(){ //normal skele spawn

		for (int i = 0; i < craftHolder.Count; i++) {

			if (craftHolder [i].GetComponent<Image> ().sprite == blankTransparentSprite) {

				craftHolder [i].GetComponent<Image> ().sprite = banannaCraft;
				craftHolder [i].GetComponent<Image> ().raycastTarget = true;
				craftHolder [i].GetComponent<Button> ().interactable = true;
				craftHolder [i].GetComponent<IndividualIngredient> ().IngredientTypeSelect =  IndividualIngredient.IngredientType.Bananna;
				break;

			}

		}


	}

	public void blockPlayerClicks(bool activateWindow){

	}

	public void resetStartEndPos(){
	//	EnemyStartingPos = EnemyEndPositionObj.transform.position;
	//	EnemyStartingPos.x = EnemyStartingPos.x + 10;
		EnemyEndPos = EnemyEndPositionObj.transform.position;
		EnemyEndPos.z = EnemyStartingPosZ; 
	}

	public void addAndReplace (GameObject nextSpawned){

		

		if(enemyList.Count>=5){

            if (enemyList[0] != null)//this null check didnt used to exist but was giving me erros. So i removed it
            {
                enemyList[0].gameObject.SetActive(false);
                DestroyImmediate(enemyList[0].gameObject);
                enemyList.RemoveAt(0);

            }




        }
        enemyList.Add(nextSpawned);


    }

    public void spawnItemChest(){ //normal skele spawn

		resetStartEndPos (); //was skeleton
		EnemySpawned = (GameObject)Instantiate (ItemChest, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "ItemChest"; //this is so the counter attack can find it

		setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned);

	}

    public void spawnEventChest()
    { //normal skele spawn

        resetStartEndPos(); //was skeleton
        EnemySpawned = (GameObject)Instantiate(EventChest, new Vector3(EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0, 0, 0)); //instantiates the beam offscreen
        EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled = false; //so the enemy doesnt get hit by a spell early
        EnemySpawned.transform.parent = GameObject.Find("Enemy").transform;
        EnemySpawned.gameObject.name = "EventChest"; //this is so the counter attack can find it

        setEnemyStats(EnemySpawned);
        addAndReplace(EnemySpawned);

    }

    public void spawnSkele(){ //normal skele spawn

		resetStartEndPos (); //was skeleton
		EnemySpawned = (GameObject)Instantiate (Skeleton, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,180,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "Skele"; //this is so the counter attack can find it

        setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned);

	}

	public void spawnSkeleWarrior(GameObject warrior){ //aka skelewarrior

		resetStartEndPos ();
		EnemySpawned = (GameObject)Instantiate (warrior, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "SkeleWarrior"; //this is so the counter attack can find it

        setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned);


	}

	public void spawnSlime( GameObject slimeType){ //regular wolf

		resetStartEndPos ();
		EnemyEndPos.z = 92f; //this is unique for the wolf
		EnemySpawned = (GameObject)Instantiate (slimeType, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "Slime"; //this is so the counter attack can find it

        setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big

		
	}

    public void spawnEgg(GameObject slimeType)
    { //regular egg

        resetStartEndPos();
        EnemyEndPos.z = 92f; //this is unique for the wolf
        EnemySpawned = (GameObject)Instantiate(slimeType, new Vector3(EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0, 0, 0)); //instantiates the beam offscreen
        EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled = false; //so the enemy doesnt get hit by a spell early
        EnemySpawned.transform.parent = GameObject.Find("Enemy").transform;
        EnemySpawned.gameObject.name = "Egg"; //this is so the counter attack can find it

        setEnemyStats(EnemySpawned);
        addAndReplace(EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big


    }

    public void spawnEvilChest( GameObject chest){ //regular wolf

		resetStartEndPos ();
		EnemyEndPos.z = 92f; //this is unique for the wolf
		EnemySpawned = (GameObject)Instantiate (chest, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "Slime"; //this is so the counter attack can find it

        setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big


	}

	public void spawnMushBoi( GameObject mushguy){ //regular wolf

		resetStartEndPos ();
		EnemyEndPos.z = 92f; //this is unique for the wolf
		EnemySpawned = (GameObject)Instantiate (mushguy, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "Mush"; //this is so the counter attack can find it

        setEnemyStats (EnemySpawned);
		addAndReplace (EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big


	}

	public void spawnSkeleMage(GameObject SkeleMage){ //

		resetStartEndPos ();
		EnemySpawned = (GameObject)Instantiate (SkeleMage, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "SkeleMage"; //this is so the counter attack can find it
		checkIfBoss();

        setEnemyStats (EnemySpawned);
        addAndReplace(EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big

    }

    public void spawnReptillian(GameObject Reptillian){ //aka skelewarrior
		
		resetStartEndPos ();
		EnemySpawned = (GameObject)Instantiate (Reptillian, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "Reptillian"; //this is so the counter attack can find it
		checkIfBoss();

		setEnemyStats (EnemySpawned);
        addAndReplace(EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big


    }

    public void spawnWerewolf(GameObject WereWolf){ //This will have to have its sounds changed in EnemyRagdoll to reflect werewolf death

		resetStartEndPos ();
		EnemySpawned = (GameObject)Instantiate (WereWolf, new Vector3 (EnemyStartingPos.x, EnemyStartingPos.y, EnemyStartingPosZ), Quaternion.Euler(0,0,0)); //instantiates the beam offscreen
		EnemySpawned.GetComponentInChildren<BoxCollider2D>().enabled=false; //so the enemy doesnt get hit by a spell early
		EnemySpawned.transform.parent = GameObject.Find ("Enemy").transform;
		EnemySpawned.gameObject.name = "WereWolf"; //this is so the counter attack can find it
		checkIfBoss();

		setEnemyStats (EnemySpawned);
        addAndReplace(EnemySpawned); //this adds to the enemy list and deletes the enemy if the list is getting too big


    }


    public void checkIfBoss(){

		if (bossMultiplier >1) { //quick check to see if this is the boss object

			EnemySpawned.GetComponentInChildren<EnemyRagdoll> ().isBoss = true;
		} else {

			EnemySpawned.GetComponentInChildren<EnemyRagdoll> ().isBoss = false;


		}
	}

	public void EndOfLevel(){


		
	}



	public void setEnemyStats(GameObject enemyObject){

		float instaKill = 2 * PlayerPrefs.GetInt ("InstaKill" + "pointsInvested"); //for some reason it would kill all enemies instantly so im just replacing what worked with this. This script is called before Main Menu because reasons..

		enemyObject.GetComponentInChildren<ControlEnemy>().playerHealthMax = InheritStats.Enemy1Health * bossHPMultiplier * enemyMultiplier;

		//print ("the instakill % is " + cameraObj.InstaKillChance + " the player pref is " + PlayerPrefs.GetInt("InstaKill"+"pointsInvested").ToString());

		enemyObject.GetComponentInChildren<ControlEnemy>().playerHealthNew = InheritStats.Enemy1Health * bossHPMultiplier * enemyMultiplier * ((100-instaKill)/100f); 

		enemyObject.GetComponentInChildren<ControlEnemy>().playerHealthCurrent = (InheritStats.Enemy1Health+10) * bossHPMultiplier * enemyMultiplier; //to account for subtracting initial hp for some reason...

		enemyObject.GetComponentInChildren<ControlEnemy>().enemyDamage = InheritStats.Enemy1Damage * bossMultiplier; 

		enemyObject.GetComponentInChildren<ControlEnemy>().enemyRank = InheritStats.Enemy1Rank * bossMultiplier; 

		enemyObject.GetComponentInChildren<EnemyRagdoll> ().goldReward = InheritStats.Enemy1Gold;

	}

    public void spawnEnemiesEvent(int enemySpawnNum)
    {


        resetEnemyShadow(); //in case this was modified for a boss


        switch (enemySpawnNum)
        { //gets the array that is prefilled with 3 enemy types per level and one boss which is index 3

            case (0):
                spawnSkeleMage(SkeletonMageGreenFireHands);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (1):
                spawnSlime(GreenJelloSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (2):
                spawnSlime(GoldSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (3):
                spawnSlime(MushSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (4):
                spawnSlime(SpikyMushSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (5):
                spawnSlime(GreenToxicMushSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (6):
                spawnSlime(YellowToxicMushSlime);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (7):
                spawnReptillian(SpearReptile);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (8):
                spawnReptillian(ShamanReptile);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (9):
                spawnReptillian(SwordReptile);
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (10):
                spawnMushBoi(TinyMushGuyGreenHat); //spawns slime
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;
            case (11):
                spawnMushBoi(TinyMushGuyPurpleHat); //spawns slime
                enemyTypeNumber = enemySpawnNum; //sets so we know what enemy type is playing
                break;

        }

        //	print("the inheristats enemy to be spawned is.... " + InheritStats.enemyArray[enemySpawnNum]);


    }

}
