﻿using UnityEngine;
using System.Collections;

public class GPGSAutoLogin : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == true) {
			
			this.enabled=false;
		}

		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ().UnityMode == false) {
			Camera.main.GetComponent<TurnedBasedGameOrganizer> ().GooglePlayAuth ();
		} 



	}
	
}
