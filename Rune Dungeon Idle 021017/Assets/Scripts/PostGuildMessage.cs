﻿using UnityEngine;
using UnityEngine.UI;

public class PostGuildMessage : MonoBehaviour {

    public float timeSinceLastPost = 0;
    public Button postMessageButton;
    public Image postCooldownImg;

    public InputField message;


    public GuildHandler guildHandlerScript;

	void Update () {

        if (timeSinceLastPost >=0)
        {
            timeSinceLastPost -= Time.deltaTime;
            postCooldownImg.fillAmount = timeSinceLastPost / 30f; //3 is the max time wait for message
        }
    }

    public void postMessage()
    {
        if (timeSinceLastPost > 0 || message.text.Length < 5)
            return;

        guildHandlerScript.PostGuildMessage(message.text);
        message.textComponent.text = "";
        timeSinceLastPost = 60f;
    }
}
