﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


using TMPro;

public class CheckLevelsComplete : MonoBehaviour {

	public List<GameObject> ListOfLevels = new List<GameObject> ();

	public int[] unlockedLevelArray; //will show 1 if level is unlocked and 0 if it is not unlocked
    public int[] eventLevels; //will show 1 if level is unlocked and 0 if it is not unlocked

    public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>();
	public GameObject unlockLevelsPane;
	public TextMeshProUGUI unlockLevelsText;
	public TextMeshProUGUI unlockLevelsGold;
	public TextMeshProUGUI LevelCostButtonText;

	public GameObject playerObj;
    public GameObject levelSelected;
    public GameObject levelHolder;
    public MainMenu cameraObj;

    public GameObject QuestList;
    public GameObject QuestToggle;

    public TextMeshProUGUI questToggleText;
    public TextMeshProUGUI dungeonToggleText;
    public TextMeshProUGUI eventToggleText;
    public TextMeshProUGUI enterEventX;//popup text



    public GameObject DungeonList;
    public GameObject EventList;
    public TextMeshProUGUI eventTimeText;
    public GameObject limitedEventPopup;


    public GameObject goToEventWindow;
    public GameObject goToDungeonWindow;
    public GameObject watchDungonAdWindow;

    public TextMeshProUGUI dungeonText;
    public TextMeshProUGUI bonusDungeonText;

    public GameObject dungeonAdUnlockButton;
    public GameObject dungeonAdlock;

    public GameObject regDungeonFinished; //for when the dungeons are completed
    public GameObject bonusDungeonFinished;
    public GameObject eventFinishedBashed;


    public AdManager adManagerScript;

    public DateTime oldDate;
    public DateTime newDate;

    public bool loadBonusDungeon = false;
    public bool loadNormalDungeon = false;

    public GameObject ProTipEventObj;
    public GameObject PotionUsedEventObj;
    public GameObject resetDungeonNotification;



    void Start () {

        //for testing purposes only
       // PlayerPrefs.SetInt("EventLevel", 9);
       //PlayerPrefs.SetInt("EventPoints", 10);


    }

    void OnEnable(){

        

        refreshEventCountDown();

        closeGoToDungeon();

        updateLevelCost();
        //	updateGems (); //gets the current players gems
        updateLevelAvailability();

        checkDungeonsAvailable(); //checks if the dungeons can be played (based on if its a new day or not and if the old dungeon has been completed)

        //for the event dungeons tips
        if (PlayerPrefs.GetInt("EventSpeed") == 1)
        {
            resetDungeonNotification.SetActive(false);
            ProTipEventObj.SetActive(false);
            PotionUsedEventObj.SetActive(true);

        }
        else
        {
            resetDungeonNotification.SetActive(false);
            ProTipEventObj.SetActive(true);
            PotionUsedEventObj.SetActive(false);
        }

        //updateEventSlider();
    }



	public void updateLevelAvailability(){

        if (PlayerPrefsX.GetIntArray("LevelsUnlocked").Length == 0) //if the array is empty it has never been called before
        {

            //    //dont need to do anything because the array is already populated to 300 with 0's

        }
        else
        {
            if (PlayerPrefsX.GetIntArray("LevelsUnlocked").Length < 200) //we need to repair this array
            {
                int[] oldArry = PlayerPrefsX.GetIntArray("LevelsUnlocked");

                Array.Resize<int>(ref oldArry, 300);

                unlockedLevelArray = oldArry;//PlayerPrefsX.GetIntArray("PetsUnlocked");
            }
            else //array already exists...
            {
                //array has alreay been repaired!
                unlockedLevelArray = PlayerPrefsX.GetIntArray("LevelsUnlocked");

            }

        }

  //      if (PlayerPrefsX.GetIntArray ("LevelsUnlocked").Length>1){ //this checks if at least one level has been unlocked. if not it defaults to level 1,2,3
		//unlockedLevelArray = PlayerPrefsX.GetIntArray ("LevelsUnlocked");
		//}


		for (int i = 0; i < unlockedLevelArray.Length; i++) {

			if (unlockedLevelArray [i] == 1) {
				//do nothing, the level has been unlocked
			} else if (unlockedLevelArray [i] == 0 && i >2) { //this means the level is greater than level 1 and has no been unlocked. [0] will be set to 1 in the inspector
				//if the level hasnt been completed
				ListOfLevels [0].GetComponent<LevelUpgrade> ().levelNumber = i - 2; //0+1 = 1
				ListOfLevels [0].GetComponent<LevelUpgrade> ().levelUnlocked = 1; //=3

				ListOfLevels [1].GetComponent<LevelUpgrade> ().levelNumber = i - 1; //=2
				ListOfLevels [1].GetComponent<LevelUpgrade> ().levelUnlocked = 1; //=3

				ListOfLevels [2].GetComponent<LevelUpgrade> ().levelNumber = i ; //=3
				ListOfLevels [2].GetComponent<LevelUpgrade> ().levelUnlocked = 0; //=3

                //ListOfLevels[0].GetComponent<LevelUpgrade>().levelIndex = 1; //=3
                //ListOfLevels[1].GetComponent<LevelUpgrade>().levelIndex = 2; //=2
                //ListOfLevels[2].GetComponent<LevelUpgrade>().levelIndex = 3; //=3

                break;

			} else if (unlockedLevelArray [i] == 0 && i <= 2) { //if player is just startin the game

				ListOfLevels [0].GetComponent<LevelUpgrade> ().levelNumber = i-1; //0+1 = 1
				ListOfLevels [0].GetComponent<LevelUpgrade> ().levelUnlocked = 1; //=3
				ListOfLevels [1].GetComponent<LevelUpgrade> ().levelNumber = i; //=2
				ListOfLevels [2].GetComponent<LevelUpgrade> ().levelNumber = i+1; //=3

               // ListOfLevels[0].GetComponent<LevelUpgrade>().levelIndex = 0; //=0
               // ListOfLevels[1].GetComponent<LevelUpgrade>().levelIndex = 1; //=1
               // ListOfLevels[2].GetComponent<LevelUpgrade>().levelIndex = 2; //=2

                break;
			}


		}
	}

	public void updateLevelCost(){

		if (data.Count==0) {
           // print("lv complete running data");
          //  data = cameraObj.enemyStatsObj.data;
        }
        else {
		//	print("data for Levels is not null anymore...");
		}
		// Enemy1Gold=(int)data[CurrentLevel-1]["GOLD"];  just as a referemce


		for (int i = 0; i < ListOfLevels.Count; i++) { //changed this from i=0; to i=1 and also made enemystatobj.data[i-1] instead of just i (0 is the starting index and prices should start at index 1 since level index 0 will always be unlocked

			LevelUpgrade LvUpgrade = ListOfLevels [i].GetComponent<LevelUpgrade> ();

                if (cameraObj.reduceLevelCost == false)
                {
                    LvUpgrade._levelCost = (double)(cameraObj.enemyStatsObj.data[LvUpgrade.levelIndex]["LVCOST"]) * (1 - cameraObj.BarginBinReductionPrestige);
                }
                else
                {
                    LvUpgrade._levelCost = (double)(cameraObj.enemyStatsObj.data[LvUpgrade.levelIndex]["LVCOST"]) * (cameraObj.LevelCostReduction - cameraObj.BarginBinReductionPrestige); //subtracting bcos levelcostred. is 1-x so we need to subtract on top of this
                }
            
		//	LvUpgrade.levelUnlocked = PlayerPrefs.GetInt (ListOfLevels [i].gameObject.name);
			LvUpgrade.updateLevelCosts (); //just changes if there is a lock or not on the level 

		}
	}


	public void openUnlockLevels(GameObject level){

		levelSelected = level; //sets the temp obj levl selected and sets the data for unlocking the level

		LevelUpgrade levelinfo = level.GetComponent<LevelUpgrade> ();

		unlockLevelsText.text = "Unlock World " + levelinfo.levelNumber +"?";

		LevelCostButtonText.text = LargeNumConverter.setDmg (levelinfo._levelCost);

		unlockLevelsPane.SetActive (true);

	}

	public void unlockTheLevel(){

		levelSelected.GetComponent<LevelUpgrade> ().purchaseLevel ();

	}

	public void updateLevelsUnlocked(){

		//closeUnlockLevel (); //not needed since already included in the leveupgrade -- purchaselevel()

		unlockedLevelArray [levelSelected.GetComponent<LevelUpgrade> ().levelNumber] = 1;

		PlayerPrefsX.SetIntArray ("LevelsUnlocked", unlockedLevelArray);//sets the unlocked levels as a mod

		//updateLevelAvailability ();


	}

	public void updateAllNames(){

		for (int i=0; i < ListOfLevels.Count; i++) {

			ListOfLevels [i].GetComponent<LevelUpgrade> ().updateLevelName ();
		}

	}

	public void closeUnlockLevel(){

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();


		unlockLevelsPane.SetActive (false);

	}

    public void pressQuestToggle()
    {

        this.gameObject.GetComponent<ScrollRect>().content = QuestList.GetComponent<RectTransform>();

        QuestList.SetActive(true);
        EventList.SetActive(false);
        DungeonList.SetActive(false);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn); 
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        eventToggleText.color = myToggleOff;
        dungeonToggleText.color = myToggleOff;
        questToggleText.color = myToggleOn;


    }

    public void pressDungeonToggle()
    {

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        if (PlayerPrefs.GetInt("PrestigeLv")>=1)
        {
            this.gameObject.GetComponent<ScrollRect>().content = DungeonList.GetComponent<RectTransform>();

            EventList.SetActive(false);
            DungeonList.SetActive(true);
            QuestList.SetActive(false);
            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

            questToggleText.color = myToggleOff;
            eventToggleText.color = myToggleOff;
            dungeonToggleText.color = myToggleOn;

        }
        else
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You must Ascend to unlock Daily Dungeons!");
            QuestToggle.GetComponent<Toggle>().isOn = true;

            questToggleText.color = myToggleOn;
            eventToggleText.color = myToggleOff;
            dungeonToggleText.color = myToggleOff;

        }

    }

    public void pressEventToggle()
    {

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        if (PlayerPrefs.GetInt("CurrentLevel") >= 5)
        {
            this.gameObject.GetComponent<ScrollRect>().content = EventList.GetComponent<RectTransform>();
            DungeonList.SetActive(false);
            QuestList.SetActive(false);
            EventList.SetActive(true);

            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

            questToggleText.color = myToggleOff;
            dungeonToggleText.color = myToggleOff;
            eventToggleText.color = myToggleOn;


        }
        else
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You must get to World 5 to begin the Event!");
            QuestToggle.GetComponent<Toggle>().isOn = true;
            questToggleText.color = myToggleOn;
            eventToggleText.color = myToggleOff;
            dungeonToggleText.color = myToggleOff;
        }

        if (PlayerPrefs.GetInt("EventSpeed") == 1)
        {
            resetDungeonNotification.SetActive(false);
            ProTipEventObj.SetActive(false);
            PotionUsedEventObj.SetActive(true);

        }
        else
        {
            resetDungeonNotification.SetActive(false);
            ProTipEventObj.SetActive(true);
            PotionUsedEventObj.SetActive(false);
        }

        if (PlayerPrefs.GetInt("EventLevel") == 10)
        {
            ProTipEventObj.SetActive(false);
            PotionUsedEventObj.SetActive(false);
            resetDungeonNotification.SetActive(true);
        }



    }

    public void closeGoToDungeon()
    {
        loadNormalDungeon = false;
        loadBonusDungeon = false;

        goToDungeonWindow.SetActive(false);
        watchDungonAdWindow.SetActive(false);
        goToEventWindow.SetActive(false);

    }

    public void loadDungeon()
    {

        if (loadNormalDungeon == true) //this is the normal dungeon 
        {
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false; //So the game doesnt think we are going into the bonus dungeon
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = true; //so a dungeon is loaded with the next lv. 

            Camera.main.GetComponent<MainMenu>().loadLevel(PlayerPrefs.GetInt("CurrentLevel"));
        //    print("load norm dung");
        }


    }

    public void loadEvent()
    {

        Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false; //So the game doesnt think we are going into the bonus dungeon
        Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = false; //so a dungeon is NOT loaded with the next lv. 
        Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = true;

        Camera.main.GetComponent<MainMenu>().loadLevel(PlayerPrefs.GetInt("CurrentLevel"));
 
    }

    public void openEventPage()
    {
        if(PlayerPrefs.GetInt("EventLevel") == 10)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You have completed this event!");

        }
        else if(PlayerPrefs.GetInt("EventComplete") == 1)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("A new Temple Event Starts Tomorrow!");

        }
        else
        {
            enterEventX.text = "Enter Event Temple " + (PlayerPrefs.GetInt("EventLevel") + 1).ToString() + "?";
            goToEventWindow.SetActive(true);
            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openLevelUnlock();
        }

            
        

    }

    public void openDungeonPage(bool bonusDungeon)
    {
        if (bonusDungeon == false) //normal dungeon
        {
            if (PlayerPrefs.GetInt("DungeonComp") == 0) {
                loadBonusDungeon = false;
                loadNormalDungeon = true;

                dungeonText.text = "Enter Dungeon?";
                Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openLevelUnlock();
                goToDungeonWindow.SetActive(true);
            }
            else
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("Dungeon Complete! More Dungeons Tomorrow!");
            }
        }

        if (bonusDungeon == true) //bonus dungeon
        {
            if (PlayerPrefs.GetInt("BonusDungeonComp") == 0)
            {

                loadBonusDungeon = true;
                loadNormalDungeon = false;

                Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openLevelUnlock();

                if (PlayerPrefs.GetInt("DungeonAdWatched") == 1)
                {
                    //say enter the bonus dungeon?
                    bonusDungeonText.text = "Enter EXTREME Dungeon?";
                }
                else
                {
                    //say watch ad to unlock dungeon
                    bonusDungeonText.text = "Watch Ad to Unlock Bonus Dungeon?";

                }

                watchDungonAdWindow.SetActive(true);
            }
            else
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("Bonus Dungeon Complete! More Dungeons Tomorrow!");


            }




        }
        

    }

    public void watchDungeonAd()
    {

        if(PlayerPrefs.GetInt("DungeonAdWatched") == 1)//if the ad has already been watched this day, load the dungeon
        {
            //  print("load bonus dung");
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = true; //So the game doesnt think we are going into the bonus dungeon
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = true; //so a dungeon is loaded with the next lv. 

            Camera.main.GetComponent<MainMenu>().loadLevel(PlayerPrefs.GetInt("CurrentLevel"));


        }
        else
        {
            adManagerScript.setDungeonType();
            adManagerScript.showUnityAd("rewardedVideo", false);

        }



    }

    public void unlockBonusDungeon()
    {

        PlayerPrefs.SetInt("DungeonAdWatched", 1);//make it so if this is already watched, to remove the icon to watch an ad. (OnEnable)
        
        if (watchDungonAdWindow.activeSelf == true)
        {
            watchDungonAdWindow.SetActive(false);
        }

        this.gameObject.GetComponentInParent<Animator>().SetTrigger("UnlockDungeon");
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().unlockLevelSfx();

    }

    public void checkDungeonsAvailable()//called from onenable
    {

      

        if (PlayerPrefs.GetInt("DungeonComp") == 1) //if reg dungon is completed
        {
            regDungeonFinished.SetActive(true);
        }
            else
            {
                regDungeonFinished.SetActive(false);
            }
        
        //bonus dungeon available, ad watched
        if (PlayerPrefs.GetInt("DungeonAdWatched") == 1)//if the bonus dungeon is available
        {
            dungeonAdUnlockButton.SetActive(false);
            dungeonAdlock.SetActive(false);
        }
            else //bonus dungeon available, ad not watched
            {
                dungeonAdUnlockButton.SetActive(true);
                dungeonAdlock.SetActive(true);
            }

        //bonus dugeon completed

        if (PlayerPrefs.GetInt("BonusDungeonComp") == 1) //if bonus dungon is completed
        {
            bonusDungeonFinished.SetActive(true);
        }
            else //bonus dungeon not completed yet
            {
                bonusDungeonFinished.SetActive(false);
            }

        if (PlayerPrefs.GetInt("EventLevel") == 10)
        {
            eventFinishedBashed.SetActive(true);
        }
        else
        {
            eventFinishedBashed.SetActive(false);
        }
    }

    public void refreshEventCountDown()
    {



            if (PlayerPrefsX.GetIntArray("EventLevels").Length != 0)
            {
                eventLevels = PlayerPrefsX.GetIntArray("EventLevels");
            }

            if (PlayerPrefs.GetString("LimitedEvent") != "")
            { //checks if a time has been saved previouslyt

                long temp = Convert.ToInt64(PlayerPrefs.GetString("LimitedEvent")); //gets the last time the shop was opened 

                newDate = System.DateTime.Now; //stores the new date of the system at time of game start

                oldDate = DateTime.FromBinary(temp); //gets the stored last date

                TimeSpan difference = newDate.Subtract(oldDate);

                double _Totalhours = difference.TotalHours; //total mins since last opening the shop

                PlayerPrefs.SetFloat("LimitedEventHours", (float)_Totalhours + PlayerPrefs.GetFloat("LimitedEventHours")); //this is the amount of hours stored since last opening the shop

                limitedEventPopup.SetActive(true);
            }
            else
            {
                //if the notification has not been opened yet



            }

            float totalHours = PlayerPrefs.GetFloat("LimitedEventHours"); //temp hours are the hours since last opening the shop. If >2 then display new cards

        

            if (totalHours <= 72 && totalHours > 0)
            {

            //      print("total hrs btwn 0 and 72");
                
                    float days = (72 - totalHours) / 24f;

                    string amtHrs = days.ToString();
                    amtHrs = amtHrs.Substring(0, 1); //gives the amt of hours

                    float amtMinDecim = days - (int)days; //this gives u the leftover decimal place
                    int minsLeft = Mathf.FloorToInt(amtMinDecim * 24);

                    eventTimeText.text = amtHrs.ToString() + " Days and " + minsLeft.ToString() + " Hours Left!";

            }

            else if (totalHours == 0)
            {
                eventTimeText.text = "Limited Event Available!!!";

                //gives the initial event levels
                for (int i = 0; i < eventLevels.Length; i++)
                {
                    eventLevels[i] = PlayerPrefs.GetInt("CurrentLevel") + i;
                }

                PlayerPrefsX.SetIntArray("EventLevels", eventLevels);

                PlayerPrefs.SetInt("EventLevel", 0); //sop the player starts at the first event index
            }
            else //total hours is >72
            {

            eventTimeText.text = "New Event Tomorrow!";


            PlayerPrefs.SetInt("EventComplete", 1); //this is so we can reset the next event after 24hrs




   //         PlayerPrefs.SetFloat("LimitedEventHours", 0); //resets this value so we are starting from another 72hr  value
      



            }

            if (PlayerPrefs.GetInt("CurrentLevel") < 5)
            {
                limitedEventPopup.SetActive(false);
            }
            else
            {
                limitedEventPopup.SetActive(true);

                PlayerPrefs.SetString("LimitedEvent", System.DateTime.Now.ToBinary().ToString()); //this is so we know how much time has passed since the player last opened the shop

                //for creatinjg the event notification for the player to notify a new event has started. Total hours are the hours passed since the event has started
                cameraObj.notificationScript.NewEventNotification(72f - totalHours);

        }


        }


   



}