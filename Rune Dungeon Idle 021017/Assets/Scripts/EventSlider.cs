﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;


public class EventSlider : MonoBehaviour {

    public Slider eventProgressSlider;
    public float barGrowthRate;
    public float totalBarGrowth; //when this reaches 1, stop the coroutine
    public TextMeshProUGUI plusEventPointFromSliderText;
    public SoundManager soundManager;


    // Use this for initialization
    void Start () {

        soundManager = Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>();

    }

    void OnEnable()
    {
        updateEventSlider();
        //for testing
        //growEventBar();
        //PlayerPrefs.SetInt("EventLevel", 8);
    }

    public void updateEventSlider()
    {
        eventProgressSlider.value = (PlayerPrefs.GetInt("EventLevel")) / 10f;

    }

    public void growEventBar()
    {

        StartCoroutine("GrowBar");
    }

    public void playAppearSound()
    {

        soundManager.dailyLoginBonus();
    }

    public void openEventSlider()
    {

        soundManager.showInventoryItem();
    }

    public void checkEventPoint()
    {

        if (PlayerPrefs.GetInt("EventLevel") == 5 || PlayerPrefs.GetInt("EventLevel") == 10)
        {
            //show text showing the player earned an event point

            if (ObscuredPrefs.GetInt("EventPack") == 1) //dbl points if event pack is bought
            {
                //give double points
                plusEventPointFromSliderText.text = "+2 Event Points!";


            }
            else
            {
                plusEventPointFromSliderText.text = "+1 Event Point!";


            }

            this.gameObject.GetComponent<Animator>().SetTrigger("PointEarned");

        }

    }



    public void openEventPopUp()
    {

        if(PlayerPrefs.GetInt("EventLevel") < 10)
        {
            Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().eventCompleteText.text = "Event Temple " + PlayerPrefs.GetInt("EventLevel").ToString() + "/10 Completed!";
        }
        else
        {
            Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().eventCompleteText.text = "Event Completed!";
        }


        if (PlayerPrefs.GetInt("EventLevel") == 5 || PlayerPrefs.GetInt("EventLevel") == 10)
        {
            //show text showing the player earned an event point
            if (Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneEventPtText.activeSelf == false)
            {
                Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneEventPtText.SetActive(true);

                if (ObscuredPrefs.GetInt("EventPack")==1) //dbl points if event pack is bought
                {
                    //give double points
                    Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneEventPtText.GetComponent<TextMeshProUGUI>().text = "+2 Event Points!";
                    PlayerPrefs.SetInt("EventPoints", PlayerPrefs.GetInt("EventPoints") + 2);

                }
                else
                {
                    Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneEventPtText.GetComponent<TextMeshProUGUI>().text = "+1 Event Point!";
                    PlayerPrefs.SetInt("EventPoints", PlayerPrefs.GetInt("EventPoints") + 1);

                }

            }
        }
        else
        {
            if (Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneGemText.activeSelf==false)
            {

                Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneGemText.SetActive(true);

                IngredientsMaster ingredObj = GameObject.Find("IngedientsHolder").GetComponent<IngredientsMaster>();

                if (ObscuredPrefs.GetInt("EventPack") == 1) //dbl points if event pack is bought
                {
                    //give double gems
                    Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneGemText.GetComponentInChildren<TextMeshProUGUI>().text = "+2 Gems!";
                    ingredObj.totalGreenGems += 2; //100 is the default for now

                }
                else
                {
                    Camera.main.GetComponent<MainMenu>().eventWindow.GetComponent<EventCompleteWIndow>().plusOneGemText.GetComponentInChildren<TextMeshProUGUI>().text = "+1 Gem!";

                    ingredObj.totalGreenGems += 1; //100 is the default for now

                }


                ingredObj.setNewPrefs();
            }
        }

        Camera.main.GetComponent<MainMenu>().eventWindow.SetActive(true);

        PlayerPrefs.SetInt("EventSpeed", 0);//this is set to one if a potion is used for an event. if the level is completed the potion is one.
    }

    public void pointEarnedSfx()
    {
        soundManager.UnlockTalent();
    }

    IEnumerator GrowBar()
    { //***make sure this is only called once per game and doesnt repeatedly get called //this also checks for potions as well

        while (true)
        {
            if (totalBarGrowth < .1f)
            {
                eventProgressSlider.value += barGrowthRate;
                totalBarGrowth += barGrowthRate;
                soundManager.dailyCountUp();
            }
            else
            {
                StopCoroutine("GrowBar");
                if (PlayerPrefs.GetInt("EventLevel") < 10)
                {
                    PlayerPrefs.SetInt("EventLevel", PlayerPrefs.GetInt("EventLevel") + 1);
                }
            }
           yield return new WaitForSeconds(0.05f);

            //print("coroutine running!");
        }
    }

    public void closeThis()
    {

        this.gameObject.SetActive(false);
    }
}
