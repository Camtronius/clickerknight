﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.Analytics.Experimental;


public class BrewPotion : MonoBehaviour {

	// Use this for initialization

	public int redGemReq;
	public int blueGemReq;
	public int yellowGemReq;

	public TextMeshProUGUI _redAmtReq;
	public TextMeshProUGUI _blueAmtReq;
	public TextMeshProUGUI _yellowAmtReq;

	public TextMeshProUGUI currentLv;
	public TextMeshProUGUI potionDescription;
	public TextMeshProUGUI watchAdText;

	public DateTime oldDate;
	public DateTime newDate;

	public Image buttonImage;

	public Slider potionExpSlider;
	public Slider potionGiftSlider;

	public float currentExp;
	public float PotionExpMax;
	public int potionLevel;
    public int maxPotionLevel = 94;


    public Image potionIcon;

	public Sprite potionLevel1;
	public Sprite potionLevel2;
	public Sprite potionLevel3;

	public float timeToBrew;

	public PotionMasterScript potionMaster;
	public AdManager adMngrScrpt;

	public enum PotionType {AtkDmgIncr,MagDmgIncr,AtkRateIncr,SplRateIncr,HPregen,AdGold,AdSilver,AdExp,AdRefresh,Event};
	public PotionType _potionType;

	public int minutesToWait = 2;
	public bool adPotion = false;
	public bool adWatchableBool = true;

	public adRewardsGift adGiftScript;



	void Start () {
		if (_potionType != PotionType.AdGold 
			&& _potionType != PotionType.AdSilver
			&& _potionType != PotionType.AdRefresh
			&& _potionType != PotionType.AdExp
            && _potionType != PotionType.Event) {

			_redAmtReq.text = "X " + redGemReq.ToString ();
			_blueAmtReq.text = "X " + blueGemReq.ToString ();
			_yellowAmtReq.text = "X " + yellowGemReq.ToString ();
		
		}

	}

	void OnEnable(){

        potionStart(); //this is so we can call this from other scripts

    }

    public void potionStart() //basically made this so we can call it from other scripts
    {
        potionMaster.loadPotionData();//in case it hasnt been loaded and we need data


        if (getPotionLevel() == 0)
        {
            setPotionLevel(); //this will make the potion level automatically 1
        }
        potionLevel = getPotionLevel();

        setSprite(potionLevel);

        setDescription();

        //	print ("this potion level is " + potionLevel);

        PotionExpMax = Convert.ToSingle(potionMaster.data[potionLevel - 1]["POTION"]);

        //	print ("this potion expMax is " + PotionExpMax);

        currentExp = getExp();

        potionExpSlider.value = getExp() / PotionExpMax;

        currentLv.text = "LV " + potionLevel.ToString();

        //tell me how long since last brew for an ad pot
        if (adPotion == true && getLastPotionMinutes() > 0)
        {
            adWatchable();
        }

        checkTotalAdsViewed();//checks how many ads are needed to unlock the ad view bonus

    }


	public void adWatchable(){

	//	print ("the amount of seconds since last brew is " + getLastPotionMinutes ());

		if (adPotion == true && (getLastPotionMinutes()) < minutesToWait ) { //

		double timeLeft = minutesToWait - getLastPotionMinutes ();

		watchAdText.text = "Wait " + Mathf.RoundToInt((float)(minutesToWait-getLastPotionMinutes())).ToString() + " min";

		adWatchableBool = false;
        //watchAdText.text = "Watch Ad?";


        }
        else
        {
			
			adWatchableBool = true;

			watchAdText.text = "Watch Ad?";

            getLastPotionMinutes ();
		}

		checkTotalAdsViewed ();
	}

	public void checkTotalAdsViewed(){

		if (adPotion == true) {
			
		int totalVidsViewed = PlayerPrefs.GetInt ("vidAdsViewed");

		//FOR TESTING
	//	totalVidsViewed = 5;
		//remove after testing


		if (totalVidsViewed/4 == 1) { //this checks if its divisible by 5 (so for every 5 ads watch recieve a reward

		//	print ("5 ads have been watched!!");
			PlayerPrefs.SetInt ("vidAdsViewed",0);
			setAdReward ();
			totalVidsViewed = 0;
			//give reward
			//open some congrats screen
			//set ads viewed to 0 again
		}

		//print ("ttl vid ads views is after checking is " + totalVidsViewed);

				for (int i = 0; i < potionMaster.potionsList.Count; i++) {
					
					if (potionMaster.potionsList [i].adPotion == true) {
					potionMaster.potionsList [i]._redAmtReq.text = "Brew " + (4 - totalVidsViewed).ToString () + " for a Gift!";
					potionMaster.potionsList [i].potionGiftSlider.value = (float)totalVidsViewed / 4f;


					}//change requirements text to something
				}

		}

	}

	public void setAdReward(){

	//	potionMaster.watchAdObject.GetComponent<adRewardsGift>().setReward();//this will apply the most recent reward

		potionMaster.watchAdObject.SetActive (true); //this will trigger getting the ad reward

	}

	public void setSprite(int potionLevel){

		if (potionLevel >= 0 && potionLevel < 4) {

			potionIcon.sprite = potionLevel1;
		}

		if (potionLevel >= 4 && potionLevel < 8) {

			potionIcon.sprite = potionLevel2;
		}

		if (potionLevel >= 8 && potionLevel < 100) {

			potionIcon.sprite = potionLevel3;

		}

	}

	public void setDescription(){

		if (_potionType == PotionType.AtkDmgIncr) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases Attack by " + (Convert.ToSingle((potionMaster.data [potionLevel - 1] ["ATKPOT"]))*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)).ToString () +
				"% for " + (potionMaster.data [potionLevel - 1] ["POTTIME"]).ToString () + " seconds";
		}

		if (_potionType == PotionType.MagDmgIncr) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases Magic Damage by " + (Convert.ToSingle((potionMaster.data [potionLevel - 1] ["MAGPOT"]))*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)).ToString () +
				"% for " + (potionMaster.data [potionLevel - 1] ["POTTIME"]).ToString () + " seconds";
		}

		if (_potionType == PotionType.HPregen) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases HP Regeneration by " + (potionMaster.data [potionLevel - 1] ["HPPOT"]).ToString () +
				"% for " + (potionMaster.data [potionLevel - 1] ["POTTIME"]).ToString () + " seconds";
		}

		if (_potionType == PotionType.AdRefresh) { //refreshes ability cooldowns
			potionDescription.text = "Refreshes your ability cooldowns when used!";
		}

        if (_potionType == PotionType.Event)
        { //refreshes ability cooldowns
            potionDescription.text = "Complete your Event World 50% Faster!";
        }

        if (_potionType == PotionType.AdExp) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases EXP by " + (potionMaster.data [potionLevel - 1] ["EXPPOT"]).ToString () +
				"% for " + ((double)(potionMaster.data [potionLevel - 1] ["POTTIME"])*12/60f).ToString () + " minutes";
		}

		if (_potionType == PotionType.AdGold) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases Gold by " + (potionMaster.data [potionLevel - 1] ["GOLDPOT"]).ToString () +
				"% for " + ((double)(potionMaster.data [potionLevel - 1] ["POTTIME"])*12/60f).ToString () + " minutes";
		}

		if (_potionType == PotionType.AdSilver) { //you need to give these  values to the potions so they are applied
			potionDescription.text = "Increases Atk and Mag Damage +" + (Convert.ToSingle((potionMaster.data [potionLevel - 1] ["SILVPOT"]))*(1+PlayerPrefs.GetInt("PrestigeLv")/2f)).ToString () +
				"% for " + ((double)(potionMaster.data [potionLevel - 1] ["POTTIME"])*12/60f).ToString () + " minutes";
		}
	}

	public float getExp(){

		float potionExpSaved = PlayerPrefs.GetFloat (this.gameObject.name + "Exp");

		if (potionExpSaved == 0) {
			potionExpSaved = 0.02f;
		}

		return potionExpSaved;
	}

	public void setExp(float Exp){

		PlayerPrefs.SetFloat (this.gameObject.name + "Exp", Exp); //sets the EXP as this..."Exp"

	}

	public int getPotionLevel(){

		int potionLvSaved = PlayerPrefs.GetInt (this.gameObject.name + "Lv");

        if (potionLvSaved > maxPotionLevel)
        {
            potionLvSaved = maxPotionLevel;
        }

		return potionLvSaved;

	}

	public void setPotionLevel(){

        if (getPotionLevel() + 1 > maxPotionLevel)
        {
            potionLevel = maxPotionLevel;
        }

		PlayerPrefs.SetInt (this.gameObject.name + "Lv", getPotionLevel() + 1); //sets the potion to the next level

		potionLevel = getPotionLevel ();


	}


	public void addPotionExp(){

		potionExpSlider.value = (getExp () + 0.34f) / PotionExpMax;

		if ((getExp () + 0.34f) >= PotionExpMax) {

			setPotionLevel (); //increases potion lv by 1
		
			setExp (0); //resets exp

			potionExpSlider.value = 0;

			currentLv.text = "LV " + getPotionLevel().ToString ();

			PotionExpMax = Convert.ToSingle(potionMaster.data[potionLevel-1]["POTION"]); 

			//do cool lv up fx

		} else {

			setExp((getExp () + 0.34f)); //sets EXP to new level
		}

		setSprite (potionLevel);
		setDescription ();
	}

	public void brewPotions(){

		bool ableToBrew = false; //you are able to brew unless there is a potion existing

		for (int i = 0; i < potionMaster.maxPotionBrewed; i++) {

			if (potionMaster.potionsListToDrink [i].spriteHolder.sprite == potionMaster.potionsListToDrink [i].nullSprite) {
				ableToBrew = true;
			}
		}

		if (redGemReq <= potionMaster.masterIngredientsHolder.totalRedGem &&
		    blueGemReq <= potionMaster.masterIngredientsHolder.totalBlueGem &&
		    yellowGemReq <= potionMaster.masterIngredientsHolder.totalYellowGem
		    && ableToBrew == true) { //do you have the gem reqs and are there not 2 potions?

	//		print ("Brewing Potion!");

			addPotionExp ();


			potionMaster.masterIngredientsHolder.totalRedGem -= redGemReq;
			potionMaster.masterIngredientsHolder.totalBlueGem -= blueGemReq;
			potionMaster.masterIngredientsHolder.totalYellowGem -= yellowGemReq;

			potionMaster.masterIngredientsHolder.setNewPrefs (); //saves new materials


			if (_potionType == PotionType.AtkDmgIncr) {
				potionMaster.setRedPotActive (potionLevel);
				AnalyticsEvent.Custom("RedPotionBrewed");

			}
			if (_potionType == PotionType.MagDmgIncr) {
				potionMaster.setBluePotActive (potionLevel);
				AnalyticsEvent.Custom("BluePotionBrewed");

			}
			if (_potionType == PotionType.SplRateIncr) {
				potionMaster.setPurpPotionActive (potionLevel);

			}
			if (_potionType == PotionType.HPregen) {
				potionMaster.setGreenPotionActive (potionLevel);
				AnalyticsEvent.Custom("GreenPotionBrewed");

			}

            if (potionMaster.gameObject.activeSelf == true) //so it doesnt play the animation of a brewed potion if brewed from the popup
            {
                potionMaster.unlockWeaponAnim(this.gameObject.GetComponentInChildren<Button>().transform, potionIcon.sprite, potionIcon.gameObject.transform);
            }
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BrewPotion ();


		} else if (ableToBrew == false) {
			
			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Potion Inventory Full!");


			//print ("you cannot afford this potion:");
		} else {

			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("You need to meet the gem requirements!");

		}


	}

	public void checkUnityAdWatchLimit(bool brewable){



		if (brewable == false && adWatchableBool==true) {

			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Potion Inventory Full!");

		}else

		if (adWatchableBool == false && brewable==false) {
			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Please Wait " + Mathf.RoundToInt ((float)(minutesToWait - getLastPotionMinutes ())).ToString () + " min to watch another ad");
		}



	}

	public void showUnityAdGoldPotion(){


		bool ableToBrew = false; //you are able to brew unless there is a potion existing

		for (int i = 0; i < potionMaster.maxPotionBrewed; i++) { // was potionMaster.potionsListToDrink.Count

			if (potionMaster.potionsListToDrink [i].spriteHolder.sprite == potionMaster.potionsListToDrink [i].nullSprite && adWatchableBool==true) {
				ableToBrew = true;
			}
		}

		checkUnityAdWatchLimit (ableToBrew);


		if (ableToBrew == true && adMngrScrpt.showingAd == false) {

			adMngrScrpt.showingAd = true;
			adMngrScrpt.setGoldType ();

			adMngrScrpt.showUnityAd ("rewardedVideo", false);
	//		print ("button to show ad pressed");
		}


	}

	public void showUnityAdSilverPotion(){

		bool ableToBrew = false; //you are able to brew unless there is a potion existing

		for (int i = 0; i < potionMaster.maxPotionBrewed; i++) {

			if (potionMaster.potionsListToDrink [i].spriteHolder.sprite == potionMaster.potionsListToDrink [i].nullSprite && adWatchableBool==true) {
				ableToBrew = true;
			}
		}

		checkUnityAdWatchLimit (ableToBrew);


		if (ableToBrew == true && adMngrScrpt.showingAd == false) {
		
			adMngrScrpt.showingAd = true;
			adMngrScrpt.setSilverType ();

			adMngrScrpt.showUnityAd ("rewardedVideo", false);
//			print ("button to show ad pressed");
		}


	}

	public void showUnityAdPurplePotion(){

		bool ableToBrew = false; //you are able to brew unless there is a potion existing

		for (int i = 0; i < potionMaster.maxPotionBrewed; i++) {

			if (potionMaster.potionsListToDrink [i].spriteHolder.sprite == potionMaster.potionsListToDrink [i].nullSprite && adWatchableBool==true) {
				ableToBrew = true;
			}
		}

		checkUnityAdWatchLimit (ableToBrew);


		if (ableToBrew == true && adMngrScrpt.showingAd == false) {

			adMngrScrpt.showingAd = true;
			adMngrScrpt.setPurpleType ();

			adMngrScrpt.showUnityAd ("rewardedVideo", false);
	//		print ("button to show ad pressed");
		}
			
	}

    public void showUnityAdWhitePotion()
    {

        bool ableToBrew = false; //you are able to brew unless there is a potion existing

        for (int i = 0; i < potionMaster.maxPotionBrewed; i++)
        {

            if (potionMaster.potionsListToDrink[i].spriteHolder.sprite == potionMaster.potionsListToDrink[i].nullSprite && adWatchableBool == true)
            {
                ableToBrew = true;
            }
        }

        checkUnityAdWatchLimit(ableToBrew);


        if (ableToBrew == true && adMngrScrpt.showingAd == false)
        {

            adMngrScrpt.showingAd = true;
            adMngrScrpt.setWhiteType();

            adMngrScrpt.showUnityAd("rewardedVideo", false);
            //		print ("button to show ad pressed");
        }

    }

    public void showUnityAdOrangePotion(){

		bool ableToBrew = false; //you are able to brew unless there is a potion existing

		for (int i = 0; i < potionMaster.maxPotionBrewed; i++) {

			if (potionMaster.potionsListToDrink [i].spriteHolder.sprite == potionMaster.potionsListToDrink [i].nullSprite && adWatchableBool==true) {
				ableToBrew = true;
			}
		}

		checkUnityAdWatchLimit (ableToBrew);


		if (ableToBrew == true && adMngrScrpt.showingAd == false) {

			adMngrScrpt.showingAd = true;
			adMngrScrpt.setOrangeType ();

			adMngrScrpt.showUnityAd ("rewardedVideo", false);
	//		print ("button to show ad pressed");
		}


	}

	public void brewGoldSilvPot(){

	//		print ("Brewing gld Potion!");

			addPotionExp ();

			if (_potionType == PotionType.AdGold) {
				potionMaster.setGoldPotActive (potionLevel);
			AnalyticsEvent.Custom("GoldPotionBrewed");

			}
			if (_potionType == PotionType.AdSilver) {
				potionMaster.setSilvPotActive (potionLevel);
			AnalyticsEvent.Custom("SilverPotionBrewed");

			}
			if (_potionType == PotionType.AdRefresh) {
				potionMaster.setPurpPotionActive (potionLevel);
			AnalyticsEvent.Custom("PurpPotionBrewed");


			}
            if (_potionType == PotionType.Event)
            {
                potionMaster.setWhitePotionActive(potionLevel);
                AnalyticsEvent.Custom("EventPotionBrewed");


            }
            if (_potionType == PotionType.AdExp) {
				    potionMaster.setOrangePotActive (potionLevel);
			    AnalyticsEvent.Custom("OrangePotionBrewed");



            }

        potionMaster.unlockWeaponAnim (this.gameObject.GetComponentInChildren<Button>().transform, potionIcon.sprite, potionIcon.gameObject.transform);
		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BrewPotion ();


        PlayerPrefs.SetString (this.gameObject.name+"SavedTime", System.DateTime.Now.ToBinary ().ToString ()); //this is so we know how much time has passed since the player last played

		//this in crements the total ads watched
		PlayerPrefs.SetInt ("vidAdsViewed", PlayerPrefs.GetInt ("vidAdsViewed") + 1);

    //    print("amt of ads watched is: " + PlayerPrefs.GetInt("vidAdsViewed"));


        adWatchable();

	}

	public double getLastPotionMinutes(){
		//temp disable this so there is no time restriction
		if (PlayerPrefs.GetString (this.gameObject.name + "SavedTime") != "") {
			newDate = System.DateTime.Now; //stores the new date of the system at time of game start

			long temp = Convert.ToInt64 (PlayerPrefs.GetString (this.gameObject.name + "SavedTime"));

			oldDate = DateTime.FromBinary (temp);

			TimeSpan difference = newDate.Subtract (oldDate);

			// difference.Minutes + " Minutes";
			//	print ("the amount of seconds since last brew is " + difference.Seconds );

			return difference.TotalMinutes;
		} else {

		return 0;
	}
	}

//	public float setLastPotionMinutes(){
//
//	}

}