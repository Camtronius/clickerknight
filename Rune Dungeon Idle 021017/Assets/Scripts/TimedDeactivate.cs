﻿using UnityEngine;
using System.Collections;

public class TimedDeactivate : MonoBehaviour {

	public float TimeToDeactivateIn;
	private float savedTime;
	public bool isDestroyed=false;

	// Use this for initialization
	void Start () {
		savedTime = TimeToDeactivateIn;
	}
	
	// Update is called once per frame
	void Update () {

		TimeToDeactivateIn -= Time.deltaTime;

		if (TimeToDeactivateIn <= 0) {
			TimeToDeactivateIn = savedTime;
				this.gameObject.SetActive (false);
			}


	}
}
