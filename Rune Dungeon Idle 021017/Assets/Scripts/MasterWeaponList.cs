﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;



public class MasterWeaponList : MonoBehaviour {

	public List<GameObject> WeaponUnlockAnims = new List<GameObject> ();
	public List<WeaponUpgrades> ListOfWeapons = new List<WeaponUpgrades> ();
	public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>();
	public Dictionary<string, Sprite> dictSprites = new Dictionary<string, Sprite>();

	public int[] unlockedWpnArray; //will show 1 if level is unlocked and 0 if it is not unlocked

	public Sprite[] arrayOfSwords;
	public Sprite[] arrayOfSwordBkgs;

    public GameObject multiplierObj;
    public int multiplier = 1;
    public TextMeshProUGUI multiplierText;
    public MainMenu cameraObj;


    public GameObject weaponDisplay;//this is what is shown if u click on spelll toggle
    public TextMeshProUGUI weaponDisplayText;
    public TextMeshProUGUI bowDescriptionText;


    public GameObject bowDisplay;//this is what is shown if u click on spelll toggle
    public TextMeshProUGUI bowDisplayText;

    //	public TextMeshProUGUI gemAmount;
    public GameObject playerObj;

	public ClothePlayerButton clothePlayerButton;
	public GameObject animatedPlayer;

    public Toggle wpnToggle;
    public Toggle bowToggle;

    public EnemyStatsInheriter enemyStatsObj;

    // Use this for initialization
    void Start () {

	//	loadWeaponData(); //this is automatically called from the main menu

	}

	void OnEnable(){

        if (PlayerPrefs.GetInt("PrestigeLv") >= 1)
        {
            multiplierObj.SetActive(true);
        }
        else
        {
            multiplierObj.SetActive(false);

        }
        multiplier = 1;
        multiplierText.text = multiplier.ToString() + "x";

        updateWeaponAvailability();
        loadWeaponData();//so when the WQeaponUpgrade scripts check for things affordable, they will have data
        updatePrices();

        bowDescriptionText.text = "The Bow does " + (4.1f + Camera.main.GetComponent<MainMenu>().bowMasteryMod / 10f).ToString() + "X your sword damage, but takes 4 taps to fire an arrow.";
    }

	public void loadWeaponData(){


        //for this one, we need only load the weapon levels if they are greater than a certain amount for example, if we are on wpn lv 3 and the are upgrading, then load the next 10. Dont need to load them all...
       // data.Clear();
        data = CSVReaderShort.Read("WpnSpl",ListOfWeapons[0].itemNumber+1, ListOfWeapons[4].itemNumber+2); //because index 0 is the header
       // print("weapon data being loaded");
       // updatePrices (); //moved to method abovce
       //this method is also called by the inventory script if the game just starts and it doesnt know what default spell damage is

	}

	public void unlockWeaponAnim(Transform location, Sprite wpnSprite, Transform swordSpritePos){

		for (int i = 0; i < WeaponUnlockAnims.Count; i++) {

			if (!WeaponUnlockAnims [i].activeInHierarchy) {

				WeaponUnlockAnims [i].GetComponent<Image> ().color = Color.red;
				WeaponUnlockAnims [i].GetComponent<SpriteHolder> ().imageHeld.sprite = wpnSprite;

				WeaponUnlockAnims [i].transform.position = new Vector3 (location.position.x, location.position.y, 90); //instantiates the beam offscre
                WeaponUnlockAnims[i].GetComponent<SpriteHolder>().imageHeld.transform.position = new Vector3(swordSpritePos.position.x, swordSpritePos.position.y, 90); //instantiates the beam offscre

                //	magicCritDmgList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

                WeaponUnlockAnims[i].SetActive (true);

				break;
			}

		}

	}

	public void updatePrices(){

		for (int i = 0; i < ListOfWeapons.Count; i++) {

			ListOfWeapons [i].updateItemCosts ();
		}
	}

	public double getWpnCost(int WpnNum){

		double WpnCost = (double)data[WpnNum]["WPNCOST"];

		return WpnCost;
	}

	public double getWpnDmg(int WpnNum){

		double WpnDmg = (double)data[WpnNum]["WPNDMG"];

		return WpnDmg;

	}

	public string getWpnName(int WpnNum){

		string WpnName = (string)data[WpnNum]["WPNNAME"];

		return WpnName;

	}

    public void addMultiplier()
    {
        if (multiplier == 1)
        {
            multiplier = 5;
        } else if(multiplier == 5){
            multiplier = 10;
        } else if (multiplier == 10)
        {
            multiplier = 1;
        }
        multiplierText.text = multiplier.ToString() + "x";

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton(); //plays back btrn sound

    }

    public void loadSword1Dict(){ //called from mainmenu row1130

		//arrayOfSwords = Resources.LoadAll<Sprite>("Backgrounds");

	//	arrayOfSwords = Resources.LoadAll<Sprite> ("Swords");

	}

	public void setCharacterIconBkg(int BkgLv){

		PlayerPrefs.SetInt ("setCharacterIconBkg", BkgLv);

		clothePlayerButton.setIconBkg ();

	}

	public void updateWeaponAvailability(){

        if (PlayerPrefsX.GetIntArray("WpnsUnlocked").Length == 0) //if the array is empty it has never been called before
        {

            //    //dont need to do anything because the array is already populated to 300 with 0's

        }
        else
        {
            if (PlayerPrefsX.GetIntArray("WpnsUnlocked").Length < 200) //we need to repair this array
            {
                int[] oldArry = PlayerPrefsX.GetIntArray("WpnsUnlocked");

                Array.Resize<int>(ref oldArry, 300);

                unlockedWpnArray = oldArry;//PlayerPrefsX.GetIntArray("PetsUnlocked");
            }
            else //array already exists...
            {
                //array has alreay been repaired!
                unlockedWpnArray = PlayerPrefsX.GetIntArray("WpnsUnlocked");

            }

        }

        //if (PlayerPrefsX.GetIntArray("WpnsUnlocked").Length > 1)
        //{ //this checks if at least one level has been unlocked. if not it defaults to level 1,2,3
        //    unlockedWpnArray = PlayerPrefsX.GetIntArray("WpnsUnlocked");
        //}





        for (int i = 0; i < unlockedWpnArray.Length; i++) {

			if (unlockedWpnArray [i] == 0 && i < 3) {//game has just started so nothing should be unlocked 

				ListOfWeapons [0].GreyScreen.enabled = false;

			ListOfWeapons [0].itemNumber = i - 2;

				if (PlayerPrefs.GetInt ("Wpn" + (ListOfWeapons [1].itemNumber-1).ToString ()) < 5) {
					ListOfWeapons [1].GreyScreen.enabled = true;
				} else {
					ListOfWeapons [1].GreyScreen.enabled = false;

				}

			ListOfWeapons [1].itemNumber = i - 1;


				if (PlayerPrefs.GetInt ("Wpn" + (ListOfWeapons [2].itemNumber-1).ToString ()) < 5) {
					ListOfWeapons [2].GreyScreen.enabled = true;
				} else {
					ListOfWeapons [2].GreyScreen.enabled = false;

				}

			ListOfWeapons [2].itemNumber = i;

			ListOfWeapons [3].itemNumber = i+1;

			ListOfWeapons [4].itemNumber = i+2;

				//because it will first its first 0 at 5???

				break;

			} else if (unlockedWpnArray [i] == 0 && i >= 3) {

				ListOfWeapons [0].itemNumber = i - 2;
				ListOfWeapons [0].GreyScreen.enabled = false;
//				ListOfWeapons [0].SwordBkgSprite.sprite = getSwordBkg(i-2);
//				ListOfWeapons [0].SwordSprite.sprite = getSwordIcon(i-2);

				ListOfWeapons [1].itemNumber = i - 1;
				ListOfWeapons [1].GreyScreen.enabled = false;
//				ListOfWeapons [1].SwordBkgSprite.sprite = getSwordBkg(i-1);
//				ListOfWeapons [1].SwordSprite.sprite = getSwordIcon(i-1); 


				ListOfWeapons [2].itemNumber = i ;
				ListOfWeapons [2].GreyScreen.enabled = false;
//				ListOfWeapons [2].SwordBkgSprite.sprite = getSwordBkg(i);
//				ListOfWeapons [2].SwordSprite.sprite = getSwordIcon(i);


				ListOfWeapons [3].itemNumber = i + 1;
				ListOfWeapons [3].GreyScreen.enabled = true;
//				ListOfWeapons [3].SwordBkgSprite.sprite = getSwordBkg(i+1);
//				ListOfWeapons [3].SwordSprite.sprite = getSwordIcon(i+1);


				ListOfWeapons [4].itemNumber = i + 2;
				ListOfWeapons [4].GreyScreen.enabled = true;
//				ListOfWeapons [4].SwordBkgSprite.sprite = getSwordBkg(i+2);
//				ListOfWeapons [4].SwordSprite.sprite = getSwordIcon(i+2);


				break;
			}



		}
	}

	public void writeWpnArray(){

		PlayerPrefsX.SetIntArray ("WpnsUnlocked", unlockedWpnArray);

	}

	public int getCharSword(int listNumber){

		double SwrdNum = (double)data[listNumber]["WPNSPRITE"];

		//	print ("the list number is " + listNumber + " and the SwrdNum is " + SwrdNum.ToString () + " converted to int its..." + ((int)SwrdNum).ToString ());

		return (int)SwrdNum;


	}

	public Sprite getSwordIcon(int listNumber){

		double SwrdNum = (double)data[listNumber]["WPNSPRITE"];

	//	print ("the list number is " + listNumber + " and the SwrdNum is " + SwrdNum.ToString () + " converted to int its..." + ((int)SwrdNum).ToString ());

		return arrayOfSwords[(int)SwrdNum];


	}

	public Sprite getSwordBkg(int listNumber){

		double BkgNum = (double)data[listNumber]["WPNBKG"];

	//	print ("the list number is " + listNumber + " and the BkgNum is " + BkgNum.ToString () + " converted to int its..." + ((int)BkgNum).ToString ());


		return arrayOfSwordBkgs[(int)BkgNum];


	}

	public int getSwordBkgNum(int listNumber){

		double BkgNum = (double)data[listNumber]["WPNBKG"];

		//	print ("the list number is " + listNumber + " and the BkgNum is " + BkgNum.ToString () + " converted to int its..." + ((int)BkgNum).ToString ());


		return (int)BkgNum;


	}

    public void pressWpnToggle()
    {

        weaponDisplay.SetActive(true);
        bowDisplay.SetActive(false);

        //Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        bowDisplayText.color = myToggleOff;
        weaponDisplayText.color = myToggleOn;


    }

    public void pressBowToggle()
    {
        if (Camera.main.GetComponent<MainMenu>().bowUnlocked == true)
        {

            bowDisplay.SetActive(true);
            weaponDisplay.SetActive(false);

            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

            Color myToggleOn = new Color();
            ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
            Color myToggleOff = new Color();
            ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

            weaponDisplayText.color = myToggleOff;
            bowDisplayText.color = myToggleOn;

        }
        else
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You must Max the Bow Ability in the Event Research Tree");

        }



    }


    public void updateGems(){

	//	gemAmount.text =  playerObj.GetComponent<ControlPlayer1Character> ().playerGold.ToString();
	}
	

}
