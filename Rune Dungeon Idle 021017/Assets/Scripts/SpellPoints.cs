﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class SpellPoints : MonoBehaviour {

	public List<GameObject> ListOfSpells = new List<GameObject>();

	public TextMeshProUGUI SpellPointsText;
	public float PointsToSpend;

	public float tornadoLearned;
	public float rockThrowLearned;
	public float waterBeamLearned;
	
	public float ShieldLearned;
	public float FireDragLearned;
	public float WaterBubbleLearned;
	public float TreantLearned;
	public float InvisibilityLearned;

	public float LightningBoltLearned;
	public float SolarBeamLearned;
	public float EnchantSwordLearned;
	public float VampireAttackLearned;


	// Use this for initialization
	void Start () {

		updatePlayerInfo ();

		for (int i = 0; i< ListOfSpells.Count; i++) {
			if(ListOfSpells[i]!=null){
			ListOfSpells[i].GetComponent<SpellInfo>().checkIfLocked();
			}
		}
	}

	public void updatePlayerInfo(){

		//thois is mostly for my reference
		PlayerPrefs.SetFloat ("default", 1);//this just makes sure the buy spell button doesnt come up for already owned spells


		tornadoLearned = PlayerPrefs.GetFloat ("TornadoLearned");

		rockThrowLearned = PlayerPrefs.GetFloat ("BoulderThrowLearned");

		waterBeamLearned = PlayerPrefs.GetFloat ("WaterBeamLearned");

		ShieldLearned = PlayerPrefs.GetFloat ("ShieldLearned");

		FireDragLearned = PlayerPrefs.GetFloat ("FireDragLearned");

		WaterBubbleLearned = PlayerPrefs.GetFloat ("WaterBubbleLearned");

		TreantLearned = PlayerPrefs.GetFloat ("TreantLearned");

		InvisibilityLearned = PlayerPrefs.GetFloat ("InvisibilityLearned");

	//	PlayerPrefs.SetFloat("SpellPoints",10); //comment this out after testing complete 4/11/16

		//new spells 09/11/16
		LightningBoltLearned = PlayerPrefs.GetFloat ("LightningBoltLearned");

		SolarBeamLearned = PlayerPrefs.GetFloat ("SolarBeamLearned");

		EnchantSwordLearned = PlayerPrefs.GetFloat ("EnchantSwordLearned");

		VampireAttackLearned = PlayerPrefs.GetFloat ("LifeStealLearned");


		PointsToSpend = PlayerPrefs.GetFloat("SpellPoints");

		SpellPointsText.text = PlayerPrefs.GetFloat ("SpellPoints").ToString ();

	
	}
	// Update is called once per frame


}
