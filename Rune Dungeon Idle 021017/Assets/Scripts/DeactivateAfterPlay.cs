﻿using UnityEngine;
using System.Collections;

public class DeactivateAfterPlay : MonoBehaviour {

public void Deactivate(){

		this.gameObject.SetActive (false);

	}

	public void refreshAnimBool(){

		this.gameObject.GetComponentInParent<Animator> ().SetBool ("ResearchIconAnim",false);
	}
}
