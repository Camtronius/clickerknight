﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockSelector : MonoBehaviour {

	public List <Vector2> touchList = new List<Vector2>(); //has a list of all the available meteors on the grid
	public List <GameObject> selectedBlocks = new List<GameObject> (); //4 index list of the blocks that are selected
	public List <GameObject> sentBlocks = new List<GameObject> (); //4 index list of the blocks that are selected

	public int	selectedBlocksCount =0; //to show how many blocks have been selected
	public bool mouseDown = false; //is mouse down or not bool
	private GameObject GameBoard; 

	private GameObject Canvas; 
	
	RaycastHit2D hit; //for detecting if a meteor is selcted

	public GameObject mouseObject; //for trail on mouse

	private MatchDataRunes mMatchData = new MatchDataRunes();

	void Start () {
		/*
		mouseObject.GetComponent<TrailRenderer> ().sortingOrder = 1;

		GameBoard = GameObject.Find ("GameBoardObject"); //gets gameboard object to instantiate new blocks

		StartCoroutine ("ClickDetector"); //detects if a block is being selected
*/
		}
	
	// Update is called once per frame
	void Update () {
		/*
		Vector2 mouseTrailPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mouseObject.transform.position = mouseTrailPos;

		if (Input.GetMouseButtonDown (0)) {
			mouseDown=true;
		}
		
		if (Input.GetMouseButtonUp (0)) {
			mouseDown=false;

			touchList.Clear();
		}
*/
	}

	IEnumerator ClickDetector(){
		
		while (true) {

			Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			touchList.Add(mousePos);

			if(mouseDown==true){

				if(touchList.Count>=5){ //.05* time is 1/2 sec

					if(Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-4])>=0 &&
					   Vector2.Distance(touchList[touchList.Count-1],touchList[touchList.Count-4]) < 0.5f){
					//	GameObject Meteor =  Instantiate(angleCheck,touchList[touchList.IndexOf(mousePos)-1],Quaternion.identity) as GameObject ;				
					//	Meteor.GetComponent<SpriteRenderer>().color = Color.red;

						Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
						RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
						
						if(hit.collider!=null){
//							print (hit.collider);
							if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
							   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected ==false ){

							hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;

								selectedBlocks.Add(hit.collider.gameObject);
								selectedBlocksCount+=1;
							}
						}
				}
				}

			}

			if(selectedBlocksCount>=4){

				for(int i =0; i<4; i++){
					if(selectedBlocks[i]!=null){
				//	selectedBlocks[i].gameObject.GetComponent<MeteorCode>().isSelected=false;
						sentBlocks[i].gameObject.GetComponent<CanvasRenderer>().SetColor(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().DefaultColor);


						//this passes the runes directly into the MatchDataRunes Class

						mMatchData.mRuneTypes.Add(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation);


						Debug.Log("added " + selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation + " to mRunetypes at : " + i ); 

						/*
						GameBoard.GetComponent<MeteorSpawn>().spawnMeteor(
						selectedBlocks[i].gameObject.GetComponent<MeteorCode>().columnSpawned); //spawns a block at the column of block removed
*/
						yield return new WaitForSeconds(0.5f);

					GameBoard.GetComponent<MeteorSpawn>().meteorList.Remove(selectedBlocks[i].gameObject.GetComponent<MeteorCode>());

					Destroy(selectedBlocks[i]);
					}
				}

			 //this stores the sent blocks color in the file that will be accessed by the other players

				selectedBlocksCount=0;
				selectedBlocks.Clear ();

				for(int i=0; i <mMatchData.mRuneTypes.Count; i++){ 
				print(mMatchData.mRuneTypes[i]);
				}

				mMatchData.ReadFromBytes(mMatchData.ToBytes());

			}

		yield return new WaitForSeconds(0.1f);

		}
	}
}

