﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class FireBallCollider : MonoBehaviour {

	public GameObject ParticleExplosion;
	public GameObject SmallExplosion;
	public GameObject VeryLargeExplosion;
//	public GameObject ExplosionForce;
	public GameObject ScreenShaker;

	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public GameObject ModGameObject;

	public float timeAlive = 0;
	public int createdByPlayer = 0;
	public double spellDMG = 0;
	public bool isCrit = false;
	// Use this for initialization

	//for SFX
	public SoundManager soundManager;

	void Start () {
	
		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		ScreenShaker = GameObject.Find ("ScreenShakerController");

		SpellCreator = GameObject.Find ("SpellListener");

		soundManager.FireballSound ();
	}
	
	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime;

		if(timeAlive>5){

//			if(NumberFireballCreated==3){
//
//				if(SpellCreator.GetComponent<SpellCreator>()!=null){
//
//				SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
//
//				SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
//
//				//this makes the players reappear as visible after the spell has  been casted (Assuming this is the counter atk spell...)
//				if(createdByPlayer==1 && GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().player1Invis==true){
//					GameObject.Find("Player1Character").GetComponentInChildren<BoxCollider2D>().enabled=true;
//						GameObject.Find("Player1Character").GetComponentInChildren<ControlPlayer1Character>().player1ReturnToVisible=true;
//
//					}
//				if(createdByPlayer==2 && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2Invis==true){
//					GameObject.Find("Player2Character").GetComponentInChildren<BoxCollider2D>().enabled=true;
//						GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2ReturnToVisible=true;
//
//					}
//				}
//
//			}

			Destroy(this.gameObject);
			
		}

//		if (timeAlive > 2 && SpellCreator.GetComponent<SpellCreatorCampaign> () != null) {
//
//			if (NumberFireballCreated == 3) {
//
//				if (SpellCreator.GetComponent<SpellCreatorCampaign> () != null) {
//				
//					SpellCreator.GetComponent<SpellCreatorCampaign> ().spellCompleted = true;
//				
//					if (GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == false && createdByPlayer == 1) { 
//						SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer);
//					}
//
//					//this makes the players reappear as visible after the spell has  been casted (Assuming this is the counter atk spell...)
//					if (createdByPlayer == 1 && GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == true) { //this is for the player counter attackin
//					
//						GameObject.Find ("Player1Character").GetComponentInChildren<BoxCollider2D> ().enabled = true;
//						GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1ReturnToVisible = true;
//						SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer);
//					
//					
//					}
//				
//					if (GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == true && createdByPlayer == 2) { 
//						SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer);
//					}
//
//					if (GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character> ().player1Invis == false && createdByPlayer == 2) { 
//						SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer);
//					}
//					/*
//					if(createdByPlayer==2 && GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2Invis==true){
//						GameObject.Find("Player2Character").GetComponentInChildren<BoxCollider2D>().enabled=true;
//						GameObject.Find("Player2Character").GetComponentInChildren<ControlPlayer2Character>().player2ReturnToVisible=true;
//						
//					}
//					*/
//				}
//
//			}
//
//			Destroy(this.gameObject);
//
//		}
	}

	void OnTriggerEnter2D (Collider2D player) {

		if (player.tag == "Player2" && createdByPlayer==1) {

		//	print ("fireball is hitting player!");

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

			ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (player.GetComponent<ControlPlayer2Character>().Player2Torso.transform.position.x, 
			                                                                 player.GetComponent<ControlPlayer2Character>().Player2Torso.transform.position.y, 90), Quaternion.identity) as GameObject;

			SmallExplosion = Instantiate (SmallExplosion, new Vector3 (this.transform.position.x, 
			                                                                 this.transform.position.y, 90), Quaternion.identity) as GameObject;

			explosionSound ();

			//where Damage is calcualted


			if(player.GetComponent<ControlPlayer2Character>().isShielded==false && Camera.main.GetComponent<CampaignGameOrganizer>()==null){
				
				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball") +
				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[1] //adding fire item mods
				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){ //this is to check if its a campaign enemy
				//condition for super effective
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
					
							calculatedHealthLoss = 2*calculatedHealthLoss;
			
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}
				
				//condition for Not very effective
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
					
							calculatedHealthLoss = (.5f)*calculatedHealthLoss;

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness

				}
				
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
					
					
				}

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createBurntRunes();

				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 1); //this used to be 0,1
				}
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 2);
				}

						//check for crit against p2
						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1
						   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true //is player 1 critting player 2?
						   ){ //and is it player 1's turn
							calculatedHealthLoss = 2*calculatedHealthLoss;
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);

						}
						
						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && //is it player 2's turn?
						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
							calculatedHealthLoss = 2*calculatedHealthLoss;
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
							
						}

							player.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
								
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p2 notification

						
				player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
				player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();

				
					}
			}

			//if its an enemy in campaign mode....

				if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){

					damageEnemy(player); //this is just an abbreviated way instead of having one gigantic method


				}

		
			this.GetComponent<SpriteRenderer>().enabled = false; //this is for the fireball not the player....
			this.GetComponent<CircleCollider2D>().enabled=false;


	}

		if (player.tag == "Player1" && createdByPlayer==2) {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

			ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.x,
			                                                                 player.GetComponent<ControlPlayer1Character>().Player1Torso.transform.position.y, 90), Quaternion.identity) as GameObject;

			SmallExplosion = Instantiate (SmallExplosion, new Vector3 (this.transform.position.x, 
			                                                           this.transform.position.y, 90), Quaternion.identity) as GameObject;

			explosionSound ();
			//where Damage is calcualted



			if(player.GetComponent<ControlPlayer1Character>().isShielded==false && Camera.main.GetComponent<CampaignGameOrganizer>()==null){
			
				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball") +
				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[1] //adding fire item mods
				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){ //this is to check if its a campaign enemy

				//condition for super effective
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){

							calculatedHealthLoss = 2*calculatedHealthLoss;

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness

				}

				//condition for Not very effective
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite){
					
							calculatedHealthLoss = (.5f)*calculatedHealthLoss;

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness

				}

				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite ||
				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
					
				}
			
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createBurntRunes();

				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 1); //this used to be 0,1
				}
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 2);
				}

			
						//check for crit against p1
						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
						   ){ //did the player crit on their turn??
							calculatedHealthLoss = 2*calculatedHealthLoss;
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
						}

						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
							calculatedHealthLoss = 2*calculatedHealthLoss;
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);

						}

							player.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p1 notification



			
				if(player.gameObject.GetComponent<TurnOnRagdoll>()!=null){
					player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
			}
			

				}


			}
			

				if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){
					
					damagePlayer(player); 	//***damages the player if in a campaign level
				}


		

			this.GetComponent<SpriteRenderer>().enabled = false; //this is for the fireball not the player....
			this.GetComponent<CircleCollider2D>().enabled=false;

		
		}


		if (player.gameObject.tag == "Enemy") { //to damage the enemy during campaign // when marked as P2
			
			damageEnemy(player);
		}

	}
	public void damagePlayer(Collider2D player){

//	
//			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//
//
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
//					
//					player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//					
//					player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//					
//					DamageToPlayer = 2* DamageToPlayer;
//					player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
//					
//					DamageToPlayer = Mathf.FloorToInt(0.5f* DamageToPlayer);
//					player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//					Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//					
//				}
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//					
//					player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//					//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//					
//				}
//
//		//	player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthNew -= DamageToPlayer;
//			Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//
//					if(DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
//						
//						player.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
//						player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//						player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//
//
//
//					}
//
//			Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmount.SetActive (true);


	}

	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method

		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		
//		ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (this.gameObject.transform.position.x, 
//		                                                                 this.gameObject.transform.position.y, 90), Quaternion.identity) as GameObject;
		
		SmallExplosion = Instantiate (SmallExplosion, new Vector3 (this.transform.position.x, 
		                                                           this.transform.position.y, 90), Quaternion.identity) as GameObject;

		explosionSound ();
		//where Damage is calcualted
		//		if(player.GetComponent<ControlPlayer2Character>().isShielded==false){
		
		double calculatedHealthLoss = spellDMG;
//
		if (isCrit == false) {
			Camera.main.GetComponent<MainMenu> ().SpellDamage ((spellDMG)); //sends the spell damage to the spell dmg text thing
		} else {

			Camera.main.GetComponent<MainMenu> ().spellCritText ((spellDMG)); //sends the spell damage to the spell dmg text thing

		}
			//		if(NumberFireballCreated==3){
//			
//			//player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
//			
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//				
//
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//			}
			
			//condition for Not very effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
//				
//				calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				
//			}
			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//				
//			//	player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
//				
//			}

//			if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//				print("CRITS!!!!!!!!!");
//
//			}

			player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
		int randoStun = Random.Range (0, 100);

		if (Camera.main.GetComponent<MainMenu> ().SpellStunChance >= randoStun && Camera.main.GetComponent<MainMenu> ().SpellStunEnabled == true) {
			player.GetComponent<ControlEnemy> ().EnemyStunned = true;
           // print("fireball stun!");
		}

		if (calculatedHealthLoss >= player.gameObject.GetComponent<ControlEnemy> ().playerHealthCurrent) {
			//player.gameObject.GetComponent<ControlEnemy> ().playerDead = true;
			//player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll

			this.GetComponent<SpriteRenderer>().enabled = false; //this is for the fireball not the player....
			this.GetComponent<CircleCollider2D>().isTrigger=false;

		//	player.gameObject.GetComponent<EnemyRagdoll> ().createExplosion ();

			//player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
		} else {

			this.GetComponent<SpriteRenderer>().enabled = false; //this is for the fireball not the player....
			this.GetComponent<CircleCollider2D>().enabled=false;
		}
	//		Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
						
//		}	
		/*
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createBurntRunes();
				
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 1); //this used to be 0,1
				}
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 2);
				}
				*/
		//	}
		//End where Damage is calcualted



	}


	public void explosionSound(){

		soundManager.SmallExplosionSound ();
	}



}
