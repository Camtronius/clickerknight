﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginBonusAnim : MonoBehaviour {

	public bool AnimComplete = false;

	public void CompleteAnim(){

		AnimComplete = true;

	}

	public void playLoginBonusSound(){

		this.gameObject.GetComponentInParent<IdleTimeAway> ().soundManager.dailyLoginBonus ();
	}
}
