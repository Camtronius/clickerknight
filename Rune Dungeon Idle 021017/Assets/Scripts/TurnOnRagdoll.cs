﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class TurnOnRagdoll : MonoBehaviour {

	// Use this for initialization
	public GameObject Head;
	public GameObject Hair;
	public GameObject Torso;

	public float gravityScale = 0.2f;

	public GameObject UpperLeftArm;
	public GameObject LowerLeftArm;
	public GameObject LeftHand;

	public GameObject LeftLegUpper;
	public GameObject LeftLegLower;
	public GameObject LeftFoot;

	public GameObject UpperRightArm;
	public GameObject LowerRightArm;
	public GameObject RightHand;

	public GameObject RightLegUpper;
	public GameObject RightLegLower;
	public GameObject RightFoot;

	public GameObject Cape;
	public GameObject SpellCreatorObject;

	public GameObject explosiveForce;

	public bool healthRecordable=false;
	public bool stunCasted=false;
	public bool isDown=false;

	public bool StartCountDown=false; //this starts a 10 second timer to make sure the body isnt down too long


	public float elapsedTimeDown = 10;
	public float sloMoTimer = 1f;

	//this is to show the player is burned after the fire spell
	public GameObject FlameDOT;

	public List <GameObject> partsList = new List<GameObject>();


	void Start () {




		partsList.Add (Head);
		if (Hair != null) {

			partsList.Add(Hair);
		}
		partsList.Add (Torso);

		partsList.Add (UpperLeftArm);
		partsList.Add (LowerLeftArm);
		partsList.Add (LeftHand);

		partsList.Add (LeftLegUpper);
		partsList.Add (LeftLegLower);
		partsList.Add (LeftFoot);

		partsList.Add (UpperRightArm);
		partsList.Add (LowerRightArm);
		partsList.Add (RightHand);

		partsList.Add (RightLegUpper);
		partsList.Add (RightLegLower);
		partsList.Add (RightFoot);
	}

	public void createRagDoll(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {

			this.gameObject.GetComponent<ControlPlayer1Character> ().soundManager.PlayerDeath ();
		}

		if (this.gameObject.GetComponentInParent<ControlPlayer1Character> ()!= null &&
		    this.gameObject.GetComponentInParent<ControlPlayer1Character> ().isShielded == true) {

			this.enabled = false;

		}

		if (this.gameObject.GetComponentInParent<ControlPlayer2Character> ()!= null &&
		    this.gameObject.GetComponentInParent<ControlPlayer2Character> ().isShielded == true) {
			
			this.enabled = false;
			
		}

		if (this.gameObject.tag == "Player1" && this.gameObject.GetComponent<ControlPlayer1Character> ().isShielded == false
			|| this.gameObject.tag == "Player2" && this.gameObject.GetComponent<ControlPlayer2Character> ().isShielded == false) {



			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			this.gameObject.GetComponent<Animator> ().enabled = false;
		
			Head.GetComponent<BoxCollider2D> ().enabled = true;
			Head.GetComponent<HingeJoint2D> ().enabled = true;
			Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			Torso.GetComponent<BoxCollider2D> ().enabled = true;
			Torso.GetComponent<HingeJoint2D> ().enabled = true;
			Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			UpperLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
			UpperLeftArm.GetComponent<HingeJoint2D> ().enabled = true;
			UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			LowerLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
			LowerLeftArm.GetComponent<HingeJoint2D> ().enabled = true;
			LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			LeftLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
			LeftLegUpper.GetComponent<HingeJoint2D> ().enabled = true;
			LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			LeftLegLower.GetComponent<BoxCollider2D> ().enabled = true;
			LeftLegLower.GetComponent<HingeJoint2D> ().enabled = true;
			LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		//	LeftFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			UpperRightArm.GetComponent<BoxCollider2D> ().enabled = true;
			UpperRightArm.GetComponent<HingeJoint2D> ().enabled = true;
			UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			LowerRightArm.GetComponent<BoxCollider2D> ().enabled = true;
			LowerRightArm.GetComponent<HingeJoint2D> ().enabled = true;
			LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			RightLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
			RightLegUpper.GetComponent<HingeJoint2D> ().enabled = true;
			RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
			RightLegLower.GetComponent<BoxCollider2D> ().enabled = true;
			RightLegLower.GetComponent<HingeJoint2D> ().enabled = true;
			RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		//	RightFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	RightFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){

				GameObject BodyMover = (GameObject)Instantiate (explosiveForce, new Vector3 (Torso.transform.position.x  , Torso.transform.position.y, 0), Quaternion.identity); //instantiates the beam offscreen
			
				BodyMover.GetComponent<ExplosiveForce>().explosionMultiplier = 150;
			}

			StartCoroutine ("detectStillBody");
			StartCoroutine ("detectDownDelay");

		}
	}

	// Update is called once per frame
	void Update () {

		if(elapsedTimeDown > 0f && StartCountDown==true){

			elapsedTimeDown-=Time.deltaTime;

		}


		if (this.gameObject.GetComponent<ControlPlayer2Character> () != null 
			&& this.gameObject.GetComponent<ControlPlayer2Character> ().playerHealthCurrent <= 0
			|| this.gameObject.GetComponent<ControlPlayer1Character> () != null 
			&& this.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent <= 0) {

			if(sloMoTimer>0){
				sloMoTimer-=Time.deltaTime;
			Time.timeScale = 0.4f;
			}else{

				Time.timeScale=1.0f;
			
			}
		}
	
	}

	public void redoGravity(){

		Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
	//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
	//	RightFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

	}

	IEnumerator detectDownDelay(){
		
		while (true) {

			yield return new WaitForSeconds(2.0f);
		
			for(int i=0; i<partsList.Count; i++){ //this is to protect some spells for respanwing the character eearly
				if(partsList[i].GetComponent<isDownDetector>()!=null){
					
					partsList[i].GetComponent<isDownDetector>().enabled=true;
					
				}
				
			}

			StartCountDown=true; //this starts a 10 second timer to make sure the body isnt down too long

			yield break;

		}
	}

	IEnumerator detectStillBody(){
		
		while (true) {
	
			Vector3 CurrentHeadPosition = Head.transform.position;
		
			yield return new WaitForSeconds(0.25f);

			if(isDown==true || elapsedTimeDown<=0){

				yield return new WaitForSeconds(4f);

				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer2Character>().playerHealthCurrent > 0
				   ||this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent > 0 ){

			

				Cape.GetComponent<SkinnedMeshRenderer>().enabled=false;
				Cape.GetComponent<MeshCollider>().enabled=false;

				this.enabled=false;

				this.gameObject.GetComponent<Animator>().enabled=true;

				this.gameObject.GetComponent<BoxCollider2D>().enabled=true;

				Head.GetComponent<BoxCollider2D>().enabled=false;
				Head.GetComponent<HingeJoint2D>().enabled=false;
				Head.GetComponent<Rigidbody2D> ().gravityScale = 0;
				
					Torso.GetComponent<BoxCollider2D>().enabled=false;
					Torso.GetComponent<HingeJoint2D>().enabled=false;
					Torso.GetComponent<Rigidbody2D> ().gravityScale = 0;
				
						UpperLeftArm.GetComponent<BoxCollider2D>().enabled=false;
						UpperLeftArm.GetComponent<HingeJoint2D>().enabled=false;
						UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = 0;
					
							LowerLeftArm.GetComponent<BoxCollider2D>().enabled=false;
							LowerLeftArm.GetComponent<HingeJoint2D>().enabled=false;
							LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = 0;
								
									LeftLegUpper.GetComponent<BoxCollider2D>().enabled=false;
									LeftLegUpper.GetComponent<HingeJoint2D>().enabled=false;
									LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = 0;
								
										LeftLegLower.GetComponent<BoxCollider2D>().enabled=false;
										LeftLegLower.GetComponent<HingeJoint2D>().enabled=false;
										LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = 0;

											UpperRightArm.GetComponent<BoxCollider2D>().enabled=false;
											UpperRightArm.GetComponent<HingeJoint2D>().enabled=false;
											UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = 0;
												
												LowerRightArm.GetComponent<BoxCollider2D>().enabled=false;
												LowerRightArm.GetComponent<HingeJoint2D>().enabled=false;
												LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = 0;
												
													RightLegUpper.GetComponent<BoxCollider2D>().enabled=false;
													RightLegUpper.GetComponent<HingeJoint2D>().enabled=false;
													RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = 0;
					
														RightLegLower.GetComponent<BoxCollider2D>().enabled=false;
														RightLegLower.GetComponent<HingeJoint2D>().enabled=false;
														RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = 0;



					this.gameObject.GetComponent<BoxCollider2D>().enabled=true;

			//		SpellCreatorObject.GetComponent<SpellCreator>().resetAllCounts();

				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null && 
				   this.gameObject.GetComponent<ControlPlayer1Character>().player1Stun==true &&
				   stunCasted==false){ //stuncasted checks if the player justrecently casted a stun spell
					//because the player shouldnt come off stun if the player casts a stun spell twice.

					this.gameObject.GetComponent<Animator>().SetBool("IsStunned",false);
					this.gameObject.GetComponent<ControlPlayer1Character>().player1Stun=false;


					print ("changed is stunned to true play 1");

					}

				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null &&
				   this.gameObject.GetComponent<ControlPlayer2Character>().player2Stun==true &&
				   stunCasted==false){//stuncasted checks if the player justrecently casted a stun spell
					//because the player shouldnt come off stun if the player casts a stun spell twice.

					this.gameObject.GetComponent<Animator>().SetBool("IsStunned",false);
					this.gameObject.GetComponent<ControlPlayer2Character>().player2Stun=false;

					print ("changed is stunned to true play 1");

						
					}

				//WARNING ~&~&~&~&~&~&~&~&~& if this is on it wont send the correct spell to the other player during online mode...
				//SpellCreatorObject.GetComponent<SpellCreator>().sentSpell.Clear();

					Cape.GetComponent<SkinnedMeshRenderer>().enabled=true;
					Cape.GetComponent<MeshCollider>().enabled=true;

				//this makes it so when the player responds it shows the fire like he is burned and taking dmg
				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null && 
				   this.gameObject.GetComponent<ControlPlayer1Character>().justBurned==true){ 

					GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

					print ("P1 burned after ragdoll");
					
				}

				//this makes it so when the player responds it shows the fire like he is burned and taking dmg
				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null &&
				   this.gameObject.GetComponent<ControlPlayer2Character>().justBurned==true){

					GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

					print ("P2 burned after ragdoll");

				}

				if(this.gameObject.tag=="Player2"){
				this.gameObject.transform.rotation = Quaternion.Euler(0,180,0);
				}
				if(this.gameObject.tag=="Player1"){
					this.gameObject.transform.localScale = new Vector3 (1, 1, 1);

				this.gameObject.transform.rotation = Quaternion.Euler(0,0,0);
				}

				if(healthRecordable==true){
				Camera.main.gameObject.GetComponent<TurnedBasedGameOrganizer>().recordHealth(); //will it not record
					//the hp fast enuff before the ragdoll is repapplied?
				}

				} //this is the end of the detection if the players health is above zero (0)

				//assigns death to a player if they are dead
				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer2Character>().playerHealthCurrent <= 0){

					this.gameObject.GetComponent<ControlPlayer2Character>().playerDead=true;


				}

				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent <= 0){
					
					this.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;

				//	Camera.main.GetComponent<TurnedBasedGameOrganizer>().networkText.text = "Player1 dead";


						if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){ //detect if player is dead during the campaign

						print ("the player has been detected as dead and the dead player screen is now active");
						Camera.main.GetComponent<CampaignGameOrganizer>().playerHasDied();


						}
				}






				for(int i=0; i<partsList.Count; i++){ //this is to protect some spells for respanwing the character eearly
					if(partsList[i].GetComponent<isDownDetector>()!=null){
						
						partsList[i].GetComponent<isDownDetector>().enabled=false;
						
					}
					
				}

				

				StartCountDown=false; //stops the countdown
				elapsedTimeDown=10; //resets the time for the countdown;
				
				

				isDown=false;

				takeTurnProper();

	//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().networkText.text = "SHould have taken turn";



				yield break;

			}
		
		}

	}


	public void takeTurnProper(){

		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> ()!=null) {
			//wont end game on spell reception because it doesnt take a turn when the player respawns on his own turn

			//checks if players health is less than or equal to zero and if its their turn...
			if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
			   && this.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent <= 0 
			   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 ||
			   this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
			   && this.gameObject.GetComponent<ControlPlayer2Character>().playerHealthCurrent <= 0
			   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2){

					Camera.main.GetComponent<TurnedBasedGameOrganizer>().WhosTurn(true,"x");

			}else if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool == true){

				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character>().playerHealthCurrent <= 0 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2){

					Camera.main.GetComponent<TurnedBasedGameOrganizer>().WhoWon.GetComponent<Text>().text = "You Win!";
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().EndMatchPanel.SetActive(true);
				}
				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character>().playerHealthCurrent <= 0 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1){
					
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().WhoWon.GetComponent<Text>().text = "You Win!";
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().EndMatchPanel.SetActive(true);
				}
			

			Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
			
				}else{ //next turn bool is == false meaning the player recieved a spell and its their turn to pick runes now.
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);

			}

			}
		}

	}


