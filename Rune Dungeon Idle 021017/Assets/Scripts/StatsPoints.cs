﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class StatsPoints : MonoBehaviour {

	public TextMeshProUGUI TotalPoints;

	public TextMeshProUGUI AtkPoints;
	public GameObject AtkSlider;

	public TextMeshProUGUI HealthPoints;
	public GameObject HPSlider;

	public TextMeshProUGUI LuckPoints;
	public GameObject LuckSlider;

	public TextMeshProUGUI youAreNowLvX;


	public GameObject statsReset;

	public float PointsToSpend;

	public float atkMod;
	public float startingAtkMod;

	public float luckMod;
	public float startingLuckMod;

	public float HealthMod;
	public float startingHealthMod;

	public float maxStatsValue;

	public GameObject Player1HPBar;
	public GameObject OpenAdObject;
	public GameObject AddStatsParticles;
	public GameObject SubStatsParticles;

	public TextMeshProUGUI AtkIncrement;
	public TextMeshProUGUI HPIncrement;
	public TextMeshProUGUI MagIncrement;


	public GameObject ResearchTalentButton;

	public TutorialCampaign TutorialScript;

	public GameObject redStatsFx;
	public GameObject yelStatsFx;
	public GameObject blueStatsFx;

	public GameObject redPartFx;
	public GameObject yelPartFx;
	public GameObject bluePartFx;

    public GameObject multiplierObj;
    public int multiplier = 1;
    public TextMeshProUGUI multiplierText;
    // Use this for initialization
    void Start () {



		//PlayerPrefs.DeleteAll ();

		//	PlayerPrefs.SetFloat("SkillPoints",5); //test to see if atk mod works

	//	updatePlayerInfo ();
	}

	void OnEnable(){

		if (PlayerPrefs.GetInt ("StatsAdd") == 0) { //this is for prestige lv0

			PlayerPrefs.SetInt ("StatsAdd", 2);
		}

		if (PlayerPrefs.GetInt ("StatsMax") == 0) { //this is for prestige lv0
			PlayerPrefs.SetInt ("StatsMax", 100); //this is the max slider value

		}

		maxStatsValue = PlayerPrefs.GetInt ("StatsMax")*(PlayerPrefs.GetInt("PrestigeLv")+2);

		ResearchTalentButton.SetActive (false);
		updatePlayerInfo ();
		youAreNowLvX.text = "You are now Level " + PlayerPrefs.GetInt("Rank").ToString() + "!"; 

		AtkIncrement.text = "Sword Atk +" + PlayerPrefs.GetInt ("StatsAdd").ToString() + "%";
		HPIncrement.text = "Player HP +" + PlayerPrefs.GetInt ("StatsAdd").ToString() + "%";
		MagIncrement.text = "Magic Atk +" + PlayerPrefs.GetInt ("StatsAdd").ToString() + "%";

        if (PlayerPrefs.GetInt("PrestigeLv") >= 2)
        {
            multiplierObj.SetActive(true);
        }
        else
        {
            multiplierObj.SetActive(false);

        }

        multiplier = 1;
        multiplierText.text = multiplier.ToString() + "x";
    }

	public void updatePlayerInfo(){

		PointsToSpend = PlayerPrefs.GetFloat("SkillPoints");

		//for testing purposes	
		//PointsToSpend = 10;

		TotalPoints.GetComponent<TextMeshProUGUI>().text = PointsToSpend.ToString();

			atkMod = PlayerPrefs.GetFloat("atkMod") + (Camera.main.GetComponent<MainMenu>().petManager.abilityArray[10] * 100f)* PlayerPrefs.GetInt("StatsAdd");
			startingAtkMod = atkMod;
        AtkPoints.GetComponent<TextMeshProUGUI>().text =  "+ " + atkMod.ToString() + "%";// + "/" + maxStatsValue.ToString();
			AtkSlider.GetComponent<Slider> ().value = (atkMod) / (float)maxStatsValue;

			
			HealthMod = PlayerPrefs.GetFloat("HealthMod") + (Camera.main.GetComponent<MainMenu>().petManager.abilityArray[10] * 100f) * PlayerPrefs.GetInt("StatsAdd"); 
			startingHealthMod = HealthMod;
        HealthPoints.GetComponent<TextMeshProUGUI>().text = "+ " + (HealthMod).ToString() + "%";// + "/" + maxStatsValue.ToString(); //its health mod - 10 b/c you start with 10 mod to make your starting hp 1k
			HPSlider.GetComponent<Slider> ().value = (HealthMod) / (float)maxStatsValue;

			
			luckMod = PlayerPrefs.GetFloat("luckMod") + (Camera.main.GetComponent<MainMenu>().petManager.abilityArray[10] * 100f) * PlayerPrefs.GetInt("StatsAdd"); 
			startingLuckMod = luckMod;
        LuckPoints.GetComponent<TextMeshProUGUI>().text = "+ " +  luckMod.ToString() + "%";// + "/" + maxStatsValue.ToString();
			LuckSlider.GetComponent<Slider> ().value = (luckMod) / (float)maxStatsValue;


	}

	public void openResetStatsAd(){

		OpenAdObject.SetActive (true);
	}

	public void incrHP(){

        for (int i = 0; i < multiplier; i++)
        {
            if (PointsToSpend >= 1)
            {
                HealthMod += PlayerPrefs.GetInt("StatsAdd");
                //button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
                HealthPoints.GetComponent<TextMeshProUGUI>().text = "+ " + (HealthMod).ToString() + "%";// + "/" + maxStatsValue.ToString();
                HPSlider.GetComponent<Slider>().value = (HealthMod) / (float)maxStatsValue;


                yelStatsFx.SetActive(true);
                yelPartFx.SetActive(true);

                subTotalPoints();
                PlayerPrefs.SetFloat("HealthMod", HealthMod);

                if (Player1HPBar != null)
                {
                    Camera.main.GetComponent<MainMenu>().playerObj.quickHPUpdate(); //updates the players HP
                }
            }
        }
        playStatsAddSfx();

    }

    public void dcrHP(){

		if (startingHealthMod < HealthMod) {
			HealthMod -= 1;
			//button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
			HealthPoints.GetComponent<TextMeshProUGUI> ().text = HealthMod.ToString ();

			playStatsAddSfx ();


			addTotalPoints ();
			PlayerPrefs.SetFloat ("HealthMod", HealthMod);
			Player1HPBar.GetComponentInChildren<TextMeshProUGUI> ().text = ((PlayerPrefs.GetFloat ("HealthMod") + PlayerPrefs.GetFloat("HPItemMod"))*100).ToString() + "/" + ((PlayerPrefs.GetFloat ("HealthMod") +PlayerPrefs.GetFloat("HPItemMod"))*100).ToString();
		}


	}

	public void incrAtk(){

        for (int i = 0; i < multiplier; i++)
        {
            if (PointsToSpend >= 1)
            {
                atkMod += PlayerPrefs.GetInt("StatsAdd");
                //button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
                AtkPoints.GetComponent<TextMeshProUGUI>().text = "+ " + atkMod.ToString() + "%";//  + "/" + maxStatsValue.ToString();

                AtkSlider.GetComponent<Slider>().value = (atkMod) / (float)maxStatsValue;


                redStatsFx.SetActive(true);
                redPartFx.SetActive(true);


                subTotalPoints();
                PlayerPrefs.SetFloat("atkMod", atkMod);
            }
        }
        playStatsAddSfx();

    }

    public void dcrAtk(){
		
		if (startingAtkMod < atkMod) {
			atkMod -= 1;
			//button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
			AtkPoints.GetComponent<TextMeshProUGUI> ().text = atkMod.ToString ();

			playStatsAddSfx ();

			addTotalPoints ();
			PlayerPrefs.SetFloat ("atkMod", atkMod);
		}
		
	}

	public void incrLuck(){

        for (int i = 0; i < multiplier; i++)
        {
            if (PointsToSpend >= 1)
            {
                luckMod += PlayerPrefs.GetInt("StatsAdd");
                //button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
                LuckPoints.GetComponent<TextMeshProUGUI>().text = "+ " + luckMod.ToString() + "%";// + "/" + maxStatsValue.ToString();

                LuckSlider.GetComponent<Slider>().value = (luckMod) / (float)maxStatsValue;

                blueStatsFx.SetActive(true);
                bluePartFx.SetActive(true);



                subTotalPoints();
                PlayerPrefs.SetFloat("luckMod", luckMod);
            }
        }
        playStatsAddSfx();

    }

    public void dcrLuck(){
		
		if (startingLuckMod < luckMod) {
			luckMod -= 1;
			//button.gameObject.transform.FindChild ("Points").GetComponent<Text> ().text = HealthMod.ToString;
			LuckPoints.GetComponent<TextMeshProUGUI> ().text = luckMod.ToString ();

			playStatsAddSfx ();

			addTotalPoints ();
			PlayerPrefs.SetFloat ("luckMod", luckMod);

	//		print ("luck mod is set to :" + luckMod);

		}
		
	}

	public void addTotalPoints(){

		PointsToSpend += 1;
		PlayerPrefs.SetFloat("SkillPoints",PointsToSpend);
		TotalPoints.GetComponent<TextMeshProUGUI> ().text = PointsToSpend.ToString ();
	}
	public void subTotalPoints(){

		PointsToSpend -= 1;

		if (PointsToSpend == 0) {

			ResearchTalentButton.SetActive (true);

			if(Camera.main.GetComponent<MainMenu>().TutorialObject.explainStats==true){


				Camera.main.GetComponent<MainMenu> ().TutorialObject.enableStatsOkButton ();

			}

		}

		if (TutorialScript!=null && PointsToSpend == 0 && TutorialScript.isTutorial==true && TutorialScript.EnemyStatsInheriterObj.CurrentLevel==1) {

			TutorialScript.LevelCompleteObj.BackButton.SetActive(true);

			TutorialScript.StatsBackButton();
			
		}




		PlayerPrefs.SetFloat("SkillPoints",PointsToSpend);
		TotalPoints.GetComponent<TextMeshProUGUI> ().text = PointsToSpend.ToString ();
	}

	public void playStatsAddSfx(){

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().addStatsSound ();
	}

    public void addMultiplier()
    {
        if (multiplier == 1)
        {
            multiplier = 5;
        }
        else if (multiplier == 5)
        {
            multiplier = 10;
        }
        else if (multiplier == 10)
        {
            multiplier = 1;
        }
        multiplierText.text = multiplier.ToString() + "x";

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton(); //plays back btrn sound

    }

}
