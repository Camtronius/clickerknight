﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Analytics.Experimental;


public class GemPerDay : MonoBehaviour {

    // Use this for initialization

    public AdManager adManagerScript;
    public TextMeshProUGUI watchAdText;
    public PotionMasterScript potionMaster;
    public Image potionIcon;



    void OnEnable() {

        if (PlayerPrefs.GetInt("FreeGemAd") == 1) //gem ad already watched
        {

            watchAdText.text = "1 Day";
  

        }
        else //gem ad not watched
        {

            watchAdText.text = "Watch Ad?";

        }

    }

    public void watchFreeGemAd()
    {

        if (PlayerPrefs.GetInt("FreeGemAd") == 1) //gem ad already watched
        {

            Camera.main.GetComponent<MainMenu>().showErrorMsg("Limit one per day!");


        }
        else //gem ad not watched
        {
            AnalyticsEvent.Custom("FreeGemAd");

            adManagerScript.setFreeGemType();
            adManagerScript.showUnityAd("rewardedVideo", false);
            PlayerPrefs.SetInt("FreeGemAd", 1);
            watchAdText.text = "1 Day";



        }

    }

    public void rewardAnim()
    {

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().collectGem();
        potionMaster.unlockWeaponAnim(this.gameObject.GetComponentInChildren<Button>().transform, potionIcon.sprite, potionIcon.gameObject.transform);

    }

}
