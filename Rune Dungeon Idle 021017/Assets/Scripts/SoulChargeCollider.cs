﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class SoulChargeCollider : MonoBehaviour {

	public int createdByPlayer=0;
	public float timeAlive = 0;
	public bool EnemyDead = false;
	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public GameObject ScreenShaker;
    public Collider2D playerColliderHolder;
	public SoundManager soundManager;
	public double spellDMG = 0;
	public bool isCrit = false;


	// Use this for initialization
	void Start () {

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		ScreenShaker = GameObject.Find ("ScreenShakerController");

		SpellCreator = GameObject.Find ("SpellListener");

        soundManager.soulChargeUse();

	}
	
	// Update is called once per frame
	void Update () {

		timeAlive += Time.deltaTime;

        if (timeAlive>3f){

		Destroy(this.gameObject);


		}

	}

    public void applySoulChargeDmg()
    {
       // print("collision detected with soul collider!");
        if (playerColliderHolder != null)
        {
            damageEnemy(playerColliderHolder); //this is to damage a non player enemy 
        }
        soundManager.rockHit();

    }

	void OnTriggerEnter2D(Collider2D player) {


		if (player.tag == "Floor") {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();
		}

		if (player.tag == "Player1" && createdByPlayer == 1) {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();
		}

		if (player.tag == "Player2" && createdByPlayer == 2) {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();

		}

		
		if (player.gameObject.tag == "Enemy") {

            playerColliderHolder = player;
            //damageEnemy(player); //this is to damage a non player enemy 
            //print("collision detected with soul collider!");
        }


		if (player.gameObject.tag == "Player2" && createdByPlayer == 1 && Camera.main.GetComponent<CampaignGameOrganizer> () != null) { //this is for a player based enbemy
			
			damageEnemy(player); //this is to damage a player based enemy
			soundManager.rockHit();

		}

		//need damage player enemy 

		//need damage p1

	}

	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method

        //		print ("there is a collision with enemy!!!");

        MainMenu cameraObj = Camera.main.GetComponent<MainMenu>();

        double calculatedSpellDmg = cameraObj.playerObj.spellDamageCalc() * (4f + 2 * (cameraObj.soulChargeMod));
        double calculatedWeaponDmg = cameraObj.CalcPlayerDmg() * (15f + 5 * (cameraObj.soulChargeMod));

        double totalDMg = calculatedSpellDmg + calculatedWeaponDmg;


            Camera.main.GetComponent<MainMenu> ().spellCritText (totalDMg); //sends the spell damage to the spell dmg text thing

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

		
		player.gameObject.GetComponent<ControlEnemy>().playerHealthNew -= totalDMg;


		this.gameObject.GetComponent<CircleCollider2D>().enabled=false;


		//player.gameObject.GetComponentInChildren<BoxCollider2D> ().enabled = false;
		if (totalDMg >= player.GetComponent<ControlEnemy> ().playerHealthCurrent) {
			
			//player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
			//player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
			//player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
			
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

			EnemyDead=true; //activates the timer to destroy the rock
			
		}
	}

	public void damagePlayer(Collider2D player){
		

			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;

			

		if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
					
			//player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
			//player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
			//player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();

			EnemyDead=true; //activates the timer to destroy the rock

					
					
		} else {

		}

//		Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player


	}



//	IEnumerator ClickDetector(){
//		
//		while (true) {
//
//			yield return new WaitForSeconds(0.75f);
//
//			if(this.gameObject.GetComponent<ConstantForce2D>()!=null){
//				this.gameObject.GetComponent<ConstantForce2D>().enabled=true;
//
//			}
//
//			yield break;
//
//		}
//
//	}

//	IEnumerator LifeStealRoutine(Collider2D playerObject){
//
//		while (true) {
//			
//			yield return new WaitForSeconds(0.5f);
//			
//			GameObject LifeStealObjClone = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//
//			yield return new WaitForSeconds(0.5f);
//
//			GameObject LifeStealObjClone2 = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone2.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone2.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone2.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//			yield return new WaitForSeconds(0.5f);
//
//			GameObject LifeStealObjClone3 = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone3.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone3.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone3.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//
//			
//			yield break;
//
//			
//		}
//		
//	}

}
