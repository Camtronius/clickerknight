﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class DroppedDmgScript : MonoBehaviour {

	public TextMeshPro dmgAmountText;
	public int dmgGiven = 0;
	public string[] suffixArray = new string[]{

		"K","M","B","T","Q","Qu","Sx","Sp","Oc","Nn","Dc","Un"
	};
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

//	void OnCollisionEnter2D (Collision2D player) {
//
//		//	print ("colliding with" + player.gameObject);
//
//		if (player.gameObject.tag == "Floor") {
//			this.gameObject.GetComponent<Animator> ().SetTrigger ("HitGround");
//
//		}
//	}

	public void setDmg(double dmgAmount){
		
		string dmgString = dmgAmount.ToString ();

		int numZeros = (int)Mathf.Log10 ((float)dmgAmount); 

	//	print("Number of zeros is " + numZeros.ToString());

		int suffixIndex = (int)(numZeros/3); //this changes the suffix based on how many zeros: 3 zeros = thousands, 6 zeros = millions etc

		suffixIndex -=1;  //makes it so it starts at thousands, if you have three zeros and the int is at 1, then it will start at millions. Subtracting one makes it start at index 0 which is thousands

		if (suffixIndex >= 0) { //if we are past the thousands place...

			double newDmg = dmgAmount / Mathf.Pow (10, (suffixIndex + 1) * 3); //this is what moves the decimal place. 
			//Divides damage by 10^X. For example if we have 1000, our suffix index is 1, then we subtract 1 which would make it zero. We add one then and multiple by 3 which gives us 1000. 
			//Therefor 1000dmg/1000 = 1.000K

			dmgString = newDmg.ToString (); //making a temp string for newDmg so i can replace the , with a .

			dmgString = dmgString.Replace (",", "."); //replaces , with . 

			//	print("New Dmg is " + dmgString + suffixArray[suffixIndex]); //writes the string

			if(dmgString.Length>6){
				dmgString = dmgString.Substring (0, 4); //10B
				}

			dmgAmountText.text = dmgString + " "+suffixArray [suffixIndex];

		} else {
			dmgAmountText.text = dmgString;
		}

			
	
//		if(dmgString.Length>6){
//			dmgString = dmgString.Substring (0, 6); //10B
//		}

	
	}

	public void activateScreenShake(){ //this is for megaslash to use

		Camera.main.GetComponent<ScreenShake> ().ShakeAmount = 0.1f;

		Camera.main.GetComponent<ScreenShake> ().ShakeTimer = 0.4f;

		//	UIbackground.GetComponent<ScreenShake> ().ShakeAmount = intensity;

		//		UIbackground.GetComponent<ScreenShake> ().ShakeTimer = duration;
	}

	public void disableThisObj(){

		this.gameObject.SetActive (false);
	}
}
