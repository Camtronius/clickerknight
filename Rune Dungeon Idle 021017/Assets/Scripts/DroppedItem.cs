﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DroppedItem : MonoBehaviour {

	public List <GameObject> ItemList = new List<GameObject>();

	public Sprite healthPot;
	public Sprite goldBag;
	//public Sprite expScroll; 

	public int goldInside = 0;
	public int healthInside = 0;

	public bool isItem = false;
	public GameObject itemObject;

	public bool itemForTutorial=false;
	public bool dontDropItem=false;

	private int randNum2;

	void Start () {

		assignSprite ();


	}
	
	// Update is called once per frame
	public void assignSprite(){
	
		int randNum = Random.Range(0,4);//Random.Range (0,4);

		//randNum = 0;

		if (GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().enemyNumber < 3 
		    && GameObject.Find ("Player1Character/Player1").GetComponent<ControlPlayer1Character>().playerHealthCurrent 
		    <  GameObject.Find ("Player1Character/Player1").GetComponent<ControlPlayer1Character>().playerHealthMax * 0.4f) {


//			print ("Default to health pot");
			randNum = 0;
		}

		if (GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().enemyNumber == 3) {

			int randNumChanger = Random.Range(1,3);//Random.Range (0,4);
			
//			print ("--- " + randNumChanger);

			randNum = randNumChanger; //this is so the player doesnt get a health pot as the last item of the level

//			print ("Default to item or gold");


		}

	//	randNum = 3;

		if (itemForTutorial == true) {

			randNum=3; //this will force an item to be dropped if the tutorial calls for it...

		}

		switch (randNum) {

		case (0):

			this.gameObject.transform.localScale = new Vector3(0.5f,0.5f,1);
			this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(.96f,.96f);
			this.gameObject.GetComponent<SpriteRenderer>().sprite = healthPot;
			break;

		case (1):
			this.gameObject.transform.localScale = new Vector3(0.5f,0.5f,1);
			this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(.96f,.96f);

			this.gameObject.GetComponent<SpriteRenderer>().sprite = goldBag;
			break;
		
		case (2):
			this.gameObject.transform.localScale = new Vector3(0.5f,0.5f,1);
			this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(.96f,.96f);

			this.gameObject.GetComponent<SpriteRenderer>().sprite = goldBag;
			break;
		
		case (3):

			isItem = true;

			if(PlayerPrefs.GetFloat ("Level14")==1 && GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ().CurrentLevel>14){
				 randNum2 = Random.Range(0,ItemList.Count); //if you are in act 2 u have the other items available
			}else {
				 randNum2 = Random.Range(0,19);
			}

			GameObject DroppedItem = (GameObject)Instantiate (ItemList[randNum2], new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.Euler(0,0,0));

			DroppedItem.transform.parent = this.transform;

			itemObject = DroppedItem; //sets it to an object so i can easily reference it

			if (GameObject.Find ("Level Complete") != null) {
				GameObject.Find ("Level Complete").GetComponent<LevelComplete> ().NewItemNotification.SetActive (true);
			}

			break;
	
		}
	
	}

	void OnCollisionEnter2D (Collision2D floor) {
		
		if (floor.gameObject.tag == "Floor") {
			
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2,0);	
			
		}

		if (floor.gameObject.tag == "Player1") {

			if(this.GetComponent<SpriteRenderer>().sprite == healthPot){

				Camera.main.GetComponent<CampaignGameOrganizer>().addHP(healthInside);
			}

			if(this.GetComponent<SpriteRenderer>().sprite == goldBag){
				Camera.main.GetComponent<CampaignGameOrganizer>().addGold(goldInside);
			}

			if(isItem == true){		

//				print ("the item recieved is" + itemObject.GetComponent<SpriteRenderer>().sprite.name);

				if(PlayerPrefs.GetInt(itemObject.GetComponent<SpriteRenderer>().sprite.name.ToString())==1 || 
				   PlayerPrefs.GetInt(itemObject.GetComponent<SpriteRenderer>().sprite.name.ToString())==2){
					
					Camera.main.GetComponent<CampaignGameOrganizer>().addGold(50);
					
				}

				if(PlayerPrefs.GetInt(itemObject.GetComponent<SpriteRenderer>().sprite.name.ToString())==0){

					PlayerPrefs.SetInt(itemObject.GetComponent<SpriteRenderer>().sprite.name.ToString(), 2);
					Camera.main.GetComponent<CampaignGameOrganizer>().addItem();

				}



			}

//			print ("gem or potion is colliding with player 1");
			Destroy(this.gameObject);
		
		}

			
			
			
		}



}
