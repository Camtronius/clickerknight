﻿using UnityEngine;
using System.Collections;

public class BeamIncrementer : MonoBehaviour {

	public bool closeBeam = false;

	public int createdByPlayer = 0;

	public float loopTime = 0;
	public float beamPositionX = 0;
	public float beamPositionX2 = 0;

	public GameObject player1LegPosition;
	public GameObject player1RightHandPosition;
	public GameObject player1TorsoPosition;
	public GameObject player1Head;

	public GameObject player2LegPosition;
	public GameObject player2RightHandPosition;
	public GameObject player2TorsoPosition;
	public GameObject player2Head;

	public GameObject currentEnemyChest;

	public GameObject waterColliderObj;
	public GameObject waterSplashObj;


	public float yPosition;

	public float beamTime = 0;
	public float BeamSpeed = 1;
	public float BeamWidth = 3;
	public float TempBeamWidth = 3;

	public float scrollSpeed;

	public LineRenderer WaterBeamLine;
	public Renderer beamObject;

	public GameObject SpellCreator; //this is for the isSpellComplete condition

	// Use this for initialization
	void Start () {
		SpellCreator = GameObject.Find ("SpellListener");
//		beamObject.sortingOrder = -40;
//		beamObject.sortingLayerName = "Scenery";
//		WaterBeamLine.material = WaterMaterial;
//		print ("the sorting order is :" + WaterBeamLine.sortingOrder);
//		print ("the sorting name is :" + WaterBeamLine.sortingLayerName);

		TempBeamWidth = BeamWidth;

		currentEnemyChest = Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<EnemyRagdoll>().Torso;

//		print (Camera.main.ScreenToWorldPoint(new Vector3(0,0,0)).x + " is the world pt." );

		beamObject = this.gameObject.GetComponent<Renderer> ();

//		if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().PersistantSoundmanager.WaterBeamShot();
//
//		}

		if (createdByPlayer == 1) {

			player1RightHandPosition = GameObject.Find ("ShotSpawnLeft");

//			player1LegPosition = GameObject.Find ("Player1Character/Player1/RightLegLower/RightFoot");
//
//			player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso");
//
//			player1Head = GameObject.Find ("Player1Character/Player1/Head");

			waterColliderObj.transform.position = player1RightHandPosition.transform.position;

			WaterBeamLine.SetWidth (BeamWidth,BeamWidth);

			yPosition = player1RightHandPosition.transform.position.y; //3 is the width of the beam in worldspace

			beamPositionX2 = player1RightHandPosition.transform.position.x;

			WaterBeamLine.SetPosition (0, new Vector3 (player1RightHandPosition.transform.position.x, yPosition, 91)); //already set to the right hand in control player 1 script
			WaterBeamLine.SetPosition (1, new Vector3 (0, yPosition, 91));

			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().waterSpellCast ();


		}

//		if (createdByPlayer == 2) {
//
//			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
//
//			player2RightHandPosition = GameObject.Find ("Player2Character/Player2/UpperRightArm/LowerRightArm/RightHand");
//		
//			player2LegPosition = GameObject.Find ("Player2Character/Player2/RightLegLower/RightFoot");
//		
//			player2TorsoPosition = GameObject.Find ("Player2Character/Player2/Torso");
//		
//			player2Head = GameObject.Find ("Player2Character/Player2/Head");
//
//			waterColliderObj.transform.position = player2RightHandPosition.transform.position;
//
//				WaterBeamLine.SetWidth (BeamWidth,BeamWidth);
//
//			yPosition = player2Head.transform.position.y; //3 is the width of the beam in worldspace
//
//			beamPositionX2 = player2RightHandPosition.transform.position.x;
//
//			beamPositionX = player2RightHandPosition.transform.position.x;
//
//			WaterBeamLine.SetPosition (1, new Vector3 (player2RightHandPosition.transform.position.x, 
//			                                           yPosition, 5)); //already set to the right hand in control player 1 script
//			WaterBeamLine.SetPosition (0, new Vector3 (player2RightHandPosition.transform.position.x, yPosition, 5));
//			
//			}else{ //if its an enemy in the campaign mode
//
//
//				if(GameObject.Find ("Enemy/EvilGuy")!=null){
//				player2RightHandPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/UpperRightArm/LowerRightArm/RightHand");
//				
//				player2LegPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/RightLegLower/RightFoot");
//				
//				player2TorsoPosition = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/Torso");
//				
//				player2Head = GameObject.Find ("Enemy/EvilGuy/EvilEnemy/Head");
//
//				}
//
//				if(GameObject.Find ("Enemy/FinalBoss")!=null){
//					player2RightHandPosition = GameObject.Find ("Enemy/FinalBoss/ShotSpawn");//("Enemy/FinalBoss/FinalBoss/UpR_Arm/LowR_Arm/R_Hand");
//					
//					player2LegPosition = GameObject.Find ("Enemy/FinalBoss/ShotSpawn");
//					
//					player2TorsoPosition = GameObject.Find ("Enemy/FinalBoss/FinalBoss/Torso");
//					
//					player2Head = GameObject.Find ("Enemy/FinalBoss/FinalBoss/UpR_Arm/LowR_Arm/R_Hand");
//					
//				}
//				
//				waterColliderObj.transform.position = player2RightHandPosition.transform.position;
//				
//				float beamWidth = 3;
//				
//				WaterBeamLine.SetWidth (beamWidth,beamWidth);
//				
//				yPosition = player2Head.transform.position.y; //3 is the width of the beam in worldspace
//				
//				beamPositionX2 = player2RightHandPosition.transform.position.x;
//				
//				beamPositionX = player2RightHandPosition.transform.position.x;
//				
//				WaterBeamLine.SetPosition (1, new Vector3 (player2RightHandPosition.transform.position.x, 
//				                                           yPosition, 5)); //already set to the right hand in control player 1 script
//				WaterBeamLine.SetPosition (0, new Vector3 (player2RightHandPosition.transform.position.x, yPosition, 5));
//
//			}
//
//		}

	}

	void Update () {
	
		beamTime += Time.deltaTime;

		if (beamTime >= 1) {
			closeBeam = true;
		}

//		if (currentEnemyChest.transform.position.x < 0) {
//			print ("chest less than 0!");
//			currentEnemyChest = Camera.main.GetComponent<MainMenu> ().campaignLevelEnemySpawner.EnemyEndPositionObj.gameObject;
//
//		} else {
//			currentEnemyChest = Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<EnemyRagdoll>().Torso;
//
//		}

	//	if (closeBeam == false) {

				if (beamPositionX != currentEnemyChest.transform.position.x && createdByPlayer == 1) {
				//	beamPositionX += Time.deltaTime * BeamSpeed;
				WaterBeamLine.SetPosition (1, new Vector3 (currentEnemyChest.transform.position.x, yPosition, 91));
						

				}


			if (waterSplashObj.activeSelf == false) {
				waterSplashObj.SetActive (true);
			}
				waterSplashObj.transform.position = new Vector3 (currentEnemyChest.transform.position.x-0.6f, yPosition, 91); //the -0.2f is to correct it from going to far in front of the bearm


//			if (beamPositionX > Camera.main.ScreenToWorldPoint(new Vector3(0,0,0)).x && createdByPlayer==2) {
//				beamPositionX -= Time.deltaTime * BeamSpeed;
//				WaterBeamLine.SetPosition (0, new Vector3 (beamPositionX, yPosition, 5));
//				
//			}
		
	//	}
	
		if (closeBeam == true && createdByPlayer==1) {

			if (waterSplashObj.activeSelf == true && BeamWidth<TempBeamWidth/5f) { //temp beam width is just to check when the beam is half its size so the splash can go away
				waterSplashObj.SetActive (false);
			}
			if (BeamWidth <= 0) {
				beamPositionX2 += Time.deltaTime * 6;
			}
			BeamWidth-=Time.deltaTime*6;

			WaterBeamLine.SetWidth (BeamWidth,BeamWidth);


		//	WaterBeamLine.SetPosition (0, new Vector3 (beamPositionX2, yPosition, 0));
		}

//		if (closeBeam == true && createdByPlayer==2) {
//			
//			beamPositionX2 -= Time.deltaTime * BeamSpeed;
//			
//			BeamWidth-=Time.deltaTime*BeamSpeed;
//			
//			WaterBeamLine.SetWidth (BeamWidth,BeamWidth);	
//		}

	

		loopTime += Time.deltaTime;
		
		float offset = loopTime * scrollSpeed;

		beamObject.material.SetTextureOffset ("_MainTex", new Vector2 (offset, 0));
		
		if (offset >= 0.65f) {
			
			//beamObject.material.SetTextureOffset("_MainTex", new Vector2(0.2f, 0));
			loopTime = 1f;
			offset = .15f;
		}

		if (BeamWidth<=0 && createdByPlayer==1) {

			if(SpellCreator.GetComponent<SpellCreator>()!=null){

			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;

//			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
				//
			

			Destroy(this.gameObject);
			
			}

			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
	//			SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);



				Destroy(this.gameObject);
				
			}


		}

//		if (beamPositionX2 <= Camera.main.ScreenToWorldPoint(new Vector3(0,0,91)).x && createdByPlayer==2) {
//
//			//dont have to change this one for the enemy until there is an enemy that uses this spell..
//			if(SpellCreator.GetComponent<SpellCreator>()!=null){
//			
//			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
//
//	//		SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
//			
//			}else{
//
//				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
//				
//		//		SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
//
//			}
//
//		
//
//			Destroy(this.gameObject);
//
//		}
	}
}