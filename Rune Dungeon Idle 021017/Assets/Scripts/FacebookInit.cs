﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using Facebook.Unity;
using UnityEngine.Analytics;
using TMPro;

public class FacebookInit : MonoBehaviour {

	public GameObject ShareOnFBButton; //this is for the the main menu

	public GameObject ShareOnFBInventoryItem;
	public GameObject LoginToFBInventoryItem;
	public GameObject purchaseItemFromDescription;

	public GameObject KingTorso;
	public GameObject KingArms;
	public GameObject KingHelm;
	public GameObject KingLegs;
	public GameObject KingCape;

	void Awake ()
	{
//		if (!FB.IsInitialized) {
//			// Initialize the Facebook SDK
//			FB.Init(InitCallback, OnHideUnity);
//		} else {
//			// Already initialized, signal an app activation App Event
//			FB.ActivateApp();
//		}
	}
	// Use this for initialization
	void Start () {

//		if(FB.IsLoggedIn==true){
//			//show the sharebutton
//			this.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "Logged In!";
//		
//			LoginToFBInventoryItem.SetActive(false);
//
//			ShareOnFBButton.SetActive(true);
//			ShareOnFBInventoryItem.SetActive(true);
//		}
	
	}
	
//	public void LoginToFacebook(){
//
//		var perms = new List<string>(){"public_profile", "email", "user_friends"};
//
//		FB.LogInWithReadPermissions(perms, AuthCallback);
//
//	}

//	public void Share()
//	{
//		FB.FeedShare (
//			string.Empty,
//			new Uri("https://play.google.com/store/apps/details?id=com.Company.RuneDungeonFinal"),
//			"Rune Dungeon!",
//			"~Burnt Games",
//			"Rune Dungeon is a FREE Action Packed! Puzzle RPG for Android! Customize your character, Upgrade your Hero, Learn Powerful Magic, and save the World!",
//			new Uri("http://www.burnt-games.com/wp-content/uploads/FacebookIcon.png"),
//			string.Empty,
//			ShareCallback
//			);
//	}
	
//	void ShareCallback(IResult result)
//	{
//		if (result.Cancelled) {
//			Debug.Log ("Share Cancelled");
//		} else if (!string.IsNullOrEmpty (result.Error)) {
//			Debug.Log ("Error on share!");
//		} else if (!string.IsNullOrEmpty (result.RawResult)) {
//			Debug.Log ("Success on share");
//
//		//	ShareOnFBButton.GetComponentInChildren<TextMeshProUGUI>().text = "Shared_FB! :)";
//
//
//			PlayerPrefs.SetInt("FacebookShare",1); //this means it has been shared
//
//			PlayerPrefs.SetInt ("LV1_KingTorso", 1);
//			PlayerPrefs.SetInt ("LV1_KingUpperRightArm", 1);
//			PlayerPrefs.SetInt ("LV1_KingHead", 1);
//			PlayerPrefs.SetInt ("LV1_KingLowerRightLeg", 1);
//			PlayerPrefs.SetInt ("KingCape 1", 1); 
//
//			KingTorso.GetComponent<ItemInfo>().owned = true;
//			KingTorso.GetComponent<ItemInfo>().checkEquipped();
//			KingArms.GetComponent<ItemInfo>().owned = true;
//			KingArms.GetComponent<ItemInfo>().checkEquipped();
//			KingHelm.GetComponent<ItemInfo>().owned = true;
//			KingHelm.GetComponent<ItemInfo>().checkEquipped();
//			KingLegs.GetComponent<ItemInfo>().owned = true;
//			KingLegs.GetComponent<ItemInfo>().checkEquipped();
//			KingCape.GetComponent<ItemInfo>().owned= true;
//			KingCape.GetComponent<ItemInfo>().checkEquipped();
//
//			purchaseItemFromDescription.SetActive(true);
//
//			Analytics.CustomEvent("FBShare", new Dictionary<string, object>
//			                      {
//				{"FBShare" , true }
//			});
//
//		}
//	}
//
//	private void InitCallback ()
//	{
//		if (FB.IsInitialized) {
//			// Signal an app activation App Event
//			FB.ActivateApp();
//			// Continue with Facebook SDK
//			// ...
//		} else {
//			Debug.Log("Failed to Initialize the Facebook SDK");
//		}
//	}

	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}



	
//	private void AuthCallback (ILoginResult result) {
//		if (FB.IsLoggedIn) {
//			// AccessToken class will have session details
//			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
//			// Print current access token's User ID
//			Debug.Log(aToken.UserId);
//			// Print current access token's granted permissions
//			foreach (string perm in aToken.Permissions) {
//				Debug.Log(perm);
//			}
//
//			this.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "Logged In!";
//
//
//			LoginToFBInventoryItem.SetActive(false);
//
//			ShareOnFBButton.SetActive(true);
//			ShareOnFBInventoryItem.SetActive(true);
//
//			Analytics.CustomEvent("FBLogIn", new Dictionary<string, object>
//			                      {
//				{"FBLogIn" , true }
//			});
//
//		} else {
//			Debug.Log("User cancelled login");
//		}
//	}
	/*
	public void TestShareButton(){

		PlayerPrefs.SetInt("FacebookShare",1); //this means it has been shared
		
		PlayerPrefs.SetInt ("LV1_KingTorso", 1);
		PlayerPrefs.SetInt ("LV1_KingUpperRightArm", 1);
		PlayerPrefs.SetInt ("LV1_KingHead", 1);
		PlayerPrefs.SetInt ("LV1_KingLowerRightLeg", 1);
		PlayerPrefs.SetInt ("KingCape 1", 1); 

		KingTorso.GetComponent<ItemInfo>().owned = true;
		KingTorso.GetComponent<ItemInfo>().checkEquipped();
		KingArms.GetComponent<ItemInfo>().owned = true;
		KingArms.GetComponent<ItemInfo>().checkEquipped();
		KingHelm.GetComponent<ItemInfo>().owned = true;
		KingHelm.GetComponent<ItemInfo>().checkEquipped();
		KingLegs.GetComponent<ItemInfo>().owned = true;
		KingLegs.GetComponent<ItemInfo>().checkEquipped();
		KingCape.GetComponent<ItemInfo>().owned= true;
		KingCape.GetComponent<ItemInfo>().checkEquipped();
		
		purchaseItemFromDescription.SetActive(true);

	}
	*/
}



