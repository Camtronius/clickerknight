﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PetFlurryCooldown : MonoBehaviour {

	// Use this for initialization
	public float CoolDownTimeMax;
	public float CoolDownTime;
	public float ThreeSecCount  =3 ;

	public Image CoolDownGfx;
	public bool isReady=false;
	public TextMeshProUGUI timeCountDown;
	public string timeString;
	public MainMenu cameraMainMenu;

	void Start () {

		cameraMainMenu = Camera.main.GetComponent<MainMenu>();

		if (PlayerPrefs.GetFloat (this.gameObject.name) > 0) {
			CoolDownTime = PlayerPrefs.GetFloat (this.gameObject.name);
		}


	}

	// Update is called once per frame
	void Update () {

		if (CoolDownTime >= 0 && cameraMainMenu.windowOpen==false) {
			CoolDownGfx.fillAmount = CoolDownTime / CoolDownTimeMax;
			CoolDownTime -= Time.deltaTime * (1+cameraMainMenu.petManager.abilityArray[1]);
			isReady = false;

		

			timeString = CoolDownTime.ToString ();



			if (CoolDownTime >= 100) {
				timeString = timeString.Substring (0, 3); //10B
			} else if (CoolDownTime > 10 && CoolDownTime < 100) {
				timeString = timeString.Substring (0, 2); //10B
			} else {//less tha n10?
				timeString = timeString.Substring (0, 1); //10B
			}

			timeCountDown.text = timeString;

			if (CoolDownTime <= 0) {
				timeCountDown.text = "";
				PlayerPrefs.SetFloat (this.gameObject.name, CoolDownTime);// this is to save the time so the potion can be reused with less time in another level...

			}


			//this is for saving the time
			if(ThreeSecCount>=0){
			ThreeSecCount -= Time.deltaTime; //just an interval so im not constantly saving to playerprefs
			}
			if(ThreeSecCount<=0){ //writes to the player prefs every 3 seconds so im not continually writing to it

				PlayerPrefs.SetFloat (this.gameObject.name, CoolDownTime);// this is to save the time so the potion can be reused with less time in another level...
				ThreeSecCount = 3;
			}




		} else {
			isReady = true;
		//timeCountDown.text = "";

		}

	}

	public void FlurryButtonPress(){
		if (isReady == true) {
			CoolDownTime = CoolDownTimeMax;

			if (this.gameObject.name == "PetFlurryButton") {
				Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().FlurryActivate ();

				this.gameObject.GetComponentInParent<ActiveAbilityManager> ().unlockWeaponAnim (this.gameObject.transform, this.gameObject.GetComponentInParent<ActiveAbilityManager> ().petFlurryImg.sprite);//this is to play the animation, the name of the method is wrong but i was too lazt to change
			}


		}
	}
}
