﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DungeonCelebration : MonoBehaviour {

    // Use this for initialization
    public TextMeshProUGUI dungeonCompleteTxt;

    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void setDungeonCompleteText(string newTxt)
    {
        dungeonCompleteTxt.text = newTxt;

    }

    public void DungeonEnemyCountSFX(){
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().DungeonEnemyCount();
    }

    public void CelebrationSFX()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().DungeonCompleteCheer();
    }

    public void TreasureFoundSFX()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();
    }

    public void deactivateGameobject()
    {
        this.gameObject.SetActive(false);
    }
}
