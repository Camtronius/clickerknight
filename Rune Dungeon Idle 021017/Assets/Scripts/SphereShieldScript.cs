﻿using UnityEngine;
using System.Collections;

public class SphereShieldScript : MonoBehaviour {

	public float SpellShieldTimer = 0f;
	public int createdByPlayer = 0;
	public bool spellComplete; //this is so we know when the spell is finished and if a counter attk is possible on it
	public bool createShield=false;
	public bool closeShield=false;

	public GameObject SpellCreator; //this is for the isSpellComplete condition and for counteratks

	// Use this for initialization
	void Start () {
	
		SpellCreator = GameObject.Find ("SpellListener");

		if (this.transform.parent.tag == "Player1" 
		    //this will be triggered when the player first starts the spell in ControlPlayer1 etc.
		    //fireball collider returns the player to visible after the counter attk spll is casted
		    && GetComponentInParent<ControlPlayer1Character>().isShielded==false) { //set player to invis
			
			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().shieldManager(2,0);
			}
			   

			GetComponentInParent<ControlPlayer1Character>().isShielded=true;

		}

		if (this.transform.parent.tag == "Player2" 
		    //this will be triggered when the player first starts the spell in ControlPlayer1 etc.
		    //fireball collider returns the player to visible after the counter attk spll is casted
		    && GetComponentInParent<ControlPlayer2Character>().isShielded==false) { //set player to invis
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().shieldManager(0,2);

			GetComponentInParent<ControlPlayer2Character>().isShielded=true;

		}

	}
	
	// Update is called once per frame
	void Update () {
		SpellShieldTimer += Time.deltaTime / 4f;

		if (createShield == true) { //&& SpellShieldTimer < 2
			if (this.gameObject.GetComponent<Renderer> ().material.mainTextureScale.y < 1.3f) {
				this.gameObject.GetComponent<Renderer> ().material.mainTextureScale += new Vector2 (0, 0.05f);
				//makes the shield come down like curtains..?
			}

			if (this.gameObject.GetComponent<Renderer> ().material.mainTextureScale.y > 1.3f && spellComplete == false) {

				if(SpellCreator.GetComponent<SpellCreator> ()!=null){
				SpellCreator.GetComponent<SpellCreator> ().spellCompleted = true;
				SpellCreator.GetComponent<SpellCreator> ().CounterAttackSpell (createdByPlayer);

				spellComplete = true;

					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool == false){ //this is so if someone recieves the spell it will unblock da runes since they are only unblocked after the ragdoll runs

						Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);


					}

				}

				if(SpellCreator.GetComponent<SpellCreatorCampaign> ()!=null){
					SpellCreator.GetComponent<SpellCreatorCampaign> ().spellCompleted = true;
				//	SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer); //this is for a player counter attacking the main player during campaign
					
					spellComplete = true;
				}
			}
		}

		if (closeShield == true && spellComplete==true) {
			createShield=false;

			if (this.gameObject.GetComponent<Renderer> ().material.mainTextureScale.y >=0) {
				this.gameObject.GetComponent<Renderer> ().material.mainTextureScale -= new Vector2 (0, 0.05f);
				//makes the shield come down like curtains..?
				if(this.gameObject.GetComponent<Renderer>().material.mainTextureScale.y<=0.1f ){

					if(createdByPlayer==1){
						this.gameObject.GetComponentInParent<ControlPlayer1Character>().isShielded=false;

						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null && 
						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true){ //this is to take a turn after shield has been closed

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
						}

						if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null && 
						   Camera.main.GetComponent<CampaignGameOrganizer>().nextTurnBool==true){ //this is to take a turn after shield has been closed
							
							Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();
							Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);
						}

						Destroy(this.gameObject);
					}

					if(createdByPlayer==2){
						this.gameObject.GetComponentInParent<ControlPlayer2Character>().isShielded=false;

						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true){ //this is to take a turn after shield has been closed

							Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
							Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
						}

						Destroy(this.gameObject);

					}

				}
			}


		}
	}
}
