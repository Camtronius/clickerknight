﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MeteorCode : MonoBehaviour {

	public List<GameObject> Neighbors = new List<GameObject>();

	public bool isSelected = false;
	public bool LockColor=false;

	//for tutorial//
	public bool isSelectable = true;
	
	public int columnSpawned;
	public int rowSpawned;

	public float indexSpawned;
	public float runeIntDesignation; //0 is white 1 is red 2 is blue 3 is gray

	public Color DefaultColor = Color.white;
	public Sprite WaterSprite;
	public Sprite FireSprite;
	public Sprite EarthSprite;
	public Sprite WindSprite;
	public Sprite MysterySprite;
	public Sprite BurntRune;

	public GameObject RuneDustRight;
	public GameObject RuneDustLeft;

	public GameObject FlameSystem;
	public GameObject FlameSystem2;
	public GameObject FlameSystem3;


	public GameObject WaterSystem;
	public GameObject WaterSystem2;
	public GameObject WaterSystem3;

	public GameObject WindSystem;
	public GameObject WindSystem2;
	public GameObject WindSystem3;

	public GameObject LeafSystem;
	public GameObject LeafSystem2;
	public GameObject LeafSystem3;

	public GameObject MysterySystem;

//	public LayerMask layerOneToIgnore;
//	public LayerMask layerTwoToIgnore;

	// Use this for initialization
	void Start () {

		GetIndexAndRow ();

			int RandColor =  Random.Range(0, 4);

				createRuneColor (RandColor);


		//this is the original color of the block
	}

	void OnCollisionEnter2D (Collision2D player) {

		if (player.gameObject.name == "Floor") { 
			if (indexSpawned == 3) {

				GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ().RuneHitGround();


				RuneDustRight = Instantiate (RuneDustRight, new Vector3 (this.transform.position.x, 
			                                                           this.transform.position.y, 0), Quaternion.identity) as GameObject;

			}
			if (indexSpawned == 0) {
				RuneDustLeft = Instantiate (RuneDustLeft, new Vector3 (this.transform.position.x, 
			                                                         this.transform.position.y, 0), Quaternion.identity) as GameObject;
			
			}
		}
	}

	public void createRuneColor (int RandColor){

		if (LockColor == false) {

			switch (RandColor) {
			case(0):
				this.GetComponent<SpriteRenderer> ().sprite = WindSprite; //wind
				runeIntDesignation = 0;
		//		print ("color changed to white!");
				break;
			case(1):
				this.GetComponent<SpriteRenderer> ().sprite = FireSprite; //fire
				runeIntDesignation = 1;
		//		print ("color changed to red!");
				break;
			case(2):
				this.GetComponent<SpriteRenderer> ().sprite = WaterSprite; //water
				runeIntDesignation = 2;
		//		print ("color changed to blue!");
				break;
			case(3):
				this.GetComponent<SpriteRenderer> ().sprite = EarthSprite; //earth
				runeIntDesignation = 3;
		//		print ("color changed to grey!");
				break;
			case(4):
				this.GetComponent<SpriteRenderer> ().sprite = MysterySprite; //unknown Rune
				//runeIntDesignation = 3;
				//print ("color changed to my!");
				break;
			case(5):

				print (this.gameObject + " rune is still burnt!???"); //does it have to do with the ghost runes?
				this.GetComponent<SpriteRenderer>().sprite = BurntRune; //unusuable rune
				isSelectable=false;
				break;
			}

			//print ("color changed!");

		
			DefaultColor = this.GetComponent<SpriteRenderer> ().color;

		}
	}

	public void RestoreBurntRunes(){ //do i need this??

		if (this.GetComponent<SpriteRenderer> ().sprite == BurntRune) { //unusuable rune
		
			LockColor=false;
			createRuneColor(Mathf.FloorToInt(runeIntDesignation)); //changes burnt runes back
			LockColor=true;
		}
	}

	public void GetIndexAndRow(){


		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
		//	print ("the index of this object is : " + Camera.main.GetComponent<TurnedBasedGameOrganizer> ().meteorList.IndexOf (this));

			indexSpawned = Camera.main.GetComponent<TurnedBasedGameOrganizer> ().meteorList.IndexOf (this);
		}

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
	//		print ("the index of this object is : " + Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList.IndexOf (this));

			indexSpawned = Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList.IndexOf (this);
		}

		if (indexSpawned / 4 < 1) {
			rowSpawned=0;
		}
		if (indexSpawned / 4 < 2 && indexSpawned / 4 >=1) {
			rowSpawned=1;
		}
		if (indexSpawned / 4 < 3 && indexSpawned / 4 >=2) {
			rowSpawned=2;
		}
		if (indexSpawned / 4 < 4 && indexSpawned / 4 >=3) {
			rowSpawned=3;
		}

		//the idea is to be able to calculate what the index of the new blocks will be based on the block that was removed. This also needs to take into account
		//all other affected blocks in the column. 

	}

	// Update is called once per frame
	void Update () {

	//	Physics2D.IgnoreLayerCollision(8, 9, true);


		if (isSelected == true) {

			switch((int)runeIntDesignation){

			case (0):
				if(this.GetComponent<SpriteRenderer>().sprite!=MysterySprite){
					WindSystem.GetComponent<ParticleSystem>().enableEmission = true;
					WindSystem2.GetComponent<ParticleSystem>().enableEmission = true;
					WindSystem3.GetComponent<ParticleSystem>().enableEmission = true;


				}else{
					MysterySystem.SetActive(true);

				}

			break;

			case (1):
				if(this.GetComponent<SpriteRenderer>().sprite!=MysterySprite){
					FlameSystem.GetComponent<ParticleSystem>().enableEmission = true;
					FlameSystem2.GetComponent<ParticleSystem>().enableEmission = true;
					FlameSystem3.GetComponent<ParticleSystem>().enableEmission = true;
				}else{
					MysterySystem.SetActive(true);

				}

			break;
				
			case (2):
				if(this.GetComponent<SpriteRenderer>().sprite!=MysterySprite){
					WaterSystem.GetComponent<ParticleSystem>().enableEmission = true;
					WaterSystem2.GetComponent<ParticleSystem>().enableEmission = true;
					WaterSystem3.GetComponent<ParticleSystem>().enableEmission = true;				
				}else{
					MysterySystem.SetActive(true);

				}

			break;

			case (3):
				if(this.GetComponent<SpriteRenderer>().sprite!=MysterySprite){
					LeafSystem.GetComponent<ParticleSystem>().enableEmission = true;
					LeafSystem2.GetComponent<ParticleSystem>().enableEmission = true;
					LeafSystem3.GetComponent<ParticleSystem>().enableEmission = true;		
				}else{
					MysterySystem.SetActive(true);
				}

			break;
			}


		} else {

			this.GetComponent<SpriteRenderer> ().color = DefaultColor;

			WindSystem.GetComponent<ParticleSystem>().enableEmission = false;
			WindSystem2.GetComponent<ParticleSystem>().enableEmission = false;
			WindSystem3.GetComponent<ParticleSystem>().enableEmission = false;

			FlameSystem.GetComponent<ParticleSystem>().enableEmission = false;
			FlameSystem2.GetComponent<ParticleSystem>().enableEmission = false;
			FlameSystem3.GetComponent<ParticleSystem>().enableEmission = false;

			WaterSystem.GetComponent<ParticleSystem>().enableEmission = false;
			WaterSystem2.GetComponent<ParticleSystem>().enableEmission = false;
			WaterSystem3.GetComponent<ParticleSystem>().enableEmission = false;

			LeafSystem.GetComponent<ParticleSystem>().enableEmission = false;
			LeafSystem2.GetComponent<ParticleSystem>().enableEmission = false;
			LeafSystem3.GetComponent<ParticleSystem>().enableEmission = false;			

			MysterySystem.SetActive(false);

		}

	}

public void AddNeighbors(GameObject r){

		Neighbors.Add (r);

	}

public void RemoveNeighbors(GameObject r){
		
		Neighbors.Remove (r);
		
	}

}
