﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AbilityCard : MonoBehaviour {

    public Image petCardSprite;
    public Image abilitySprite;
    public TextMeshProUGUI abilityNameTmpro;

    public List<Sprite> cardRaritySprites = new List<Sprite>();
    public AbilityMaster abilityMasterScript;

    public int index;//this is the index of the card in the list of cards of PetMaster script

    public int rarity; //0 is common //1 is uncommon //2 is rare //3 is epic //4 is Unique (only bought)

    public string abilityName;

    public bool isUnlocked = false;

    public GameObject abilityLock;
    public GameObject selected;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void loadRarityCard() //the card is assigned a rarity in petsmaster, then this is called from petsmaster
    {
        switch (rarity)
        {
            case 0:
                petCardSprite.sprite = cardRaritySprites[0];//common
                break;
            case 1:
                petCardSprite.sprite = cardRaritySprites[1];//uncommon
                break;
            case 2:
                petCardSprite.sprite = cardRaritySprites[2];//rare
                break;
            case 3:
                petCardSprite.sprite = cardRaritySprites[3];//epic
                break;
            case 4:
                petCardSprite.sprite = cardRaritySprites[4];//unique
                break;

        }

    }

    
    public void abilitySpriteMeth()
    {

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#00000096", out myColorFaded);

        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);

       
            if (isUnlocked == true)
            {
            abilitySprite.color = myColorNorm;
            }
            else
            {
            abilitySprite.color = myColorFaded;
            }

    }

    public void TaskOnClick()
    {

        if (abilityMasterScript.unlockedAbilities[index] == 0)
        {

            Camera.main.GetComponent<MainMenu>().showErrorMsg("This ability has not been unlocked yet");

        }
        else
        {
            
            abilityMasterScript.abilityCardholder = this.gameObject;
            abilityMasterScript.openAbilityWindow();
        }

 
    }
}
