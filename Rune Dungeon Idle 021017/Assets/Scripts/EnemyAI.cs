using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class EnemyAI : MonoBehaviour
{
	public int enemyIntelligence; //1 is the base int
	public int inapplicableRunesCount=0;
	public int desiredRuneType;
	public int lowestIndex;
	public int lastSpellPlayer1;
	//public Dictionary<int, List<GameObject>> DictOfLists = new Dictionary<int, List<GameObject>>();
	List<List<GameObject>> listOfPaths = new List<List<GameObject>>();
	public  List<int> runeCountList= new List<int>();

	public int dictKey = 0;

	// Use this for initialization
	void Start ()
	{
	//	decideDesiredRune (); //this will decide which rune to choose


	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

//	public void decideDesiredRune(){
//
////		if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite
////		   ==Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
////			lastSpellPlayer1=0;
////		}
////
////			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite
////			   ==Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
////				lastSpellPlayer1=1;
////			}
////
////				if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite
////				   ==Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
////					lastSpellPlayer1=2;
////				}
////
////					if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite
////					   ==Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
////						lastSpellPlayer1=3;
////					}
////
////						if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite
////						   ==Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
////							lastSpellPlayer1=4;
////						}
//
////		for (int i=0; i<4; i++) {
////
////		runeCountList.Add(Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count(gameObject => gameObject.GetComponent<MeteorCode>().runeIntDesignation == i));
////		
////			switch(i){
////
////			case 0:
////			//	print ("*there are this many wind runes " + runeCountList[i]);
////				break;
////			case 1:
////			//	print ("**there are this many fire runes " + runeCountList[i]);
////				break;
////			case 2:
////			//	print ("***there are this many water runes " + runeCountList[i]);
////				break;
////			case 3:
////			//	print ("****there are this many earth runes " + runeCountList[i]);
////				break;
////
////			}
////
////		}
//
//
//		if (enemyIntelligence >= 11) { 
//
//			//the enemy is pretty stupid, he will pick at random a new element each turn, 
//			///then pick the worst possible combination. usually containing about 1 rune of that type.
//
//			//this shows which type of rune is the lowest, we want this int to pick the lowest amt of a rune type.
//			for(int i=0; i<runeCountList.Count(); i++){
//
//				for(int j=0; j<runeCountList.Count(); j++){
//					
//					if(runeCountList[i]<runeCountList[j] && runeCountList[i]>0){
//
//						lowestIndex=i;
//					}
//				}
//			}
//
////			print ("~_~_~_~_~__~__~_~~_~_~__~ the lowest index is " + lowestIndex);
//
//			desiredRuneType = lowestIndex; //if the enemy is dumb enough they will just choose runes at random
//
//		}
//
//		if (enemyIntelligence < 11 && enemyIntelligence >= 7) { //this would be good for the skeletonWarrior
//
//			//this enemy is semi intelligent it will pick a rune at random then pick a middle of the pack number
//			//probably around 3 of the same type of rune and rarely 4
//
//			int runeChoice = Random.Range (0, 4);
//
//			desiredRuneType = runeChoice; //if the enemy is dumb enough they will just choose runes at random
//
//
//		}
//
//
//		if (enemyIntelligence <= 7 && enemyIntelligence > 6) { //this would be good for the skeleMage
//			
//			//this enemy is more greedy than smart, it will pick the rune with the most type on the board
//			//then is will pick 3-4 of them
//
//			lowestIndex = runeCountList.IndexOf(runeCountList.Max());
//
//			//use lowest index so i dont have to create another variable
//			print ("/ x/ x/ x/ x/ x/ x/ x/ the Highest index is " + lowestIndex);
//			
//			desiredRuneType = lowestIndex; //if the enemy is dumb enough they will just choose runes at random
//			
//		}
//
//		if (enemyIntelligence == 6) { //this would be good for a smarter mage or maybe a Skele King or something
//			
//			//this enemy will try to counter the player if possible, but it will not check if the 
//			//counter to the spell its trying to cast is on the board.
//			//it also doesnt check the amount of the type of runes on the board...except if its zero then it will
//			//pick the lowest type of rune
//
//			//0 is wind, 1 fire, 2 water, 3 earth
//			if(lastSpellPlayer1==0 && runeCountList[3]>0){
//
//				desiredRuneType = 3;
//			}else if(lastSpellPlayer1==1 && runeCountList[2]>0){
//					
//					desiredRuneType = 2;
//				} else if(lastSpellPlayer1==2 && runeCountList[0]>0){
//						
//						desiredRuneType = 0;
//					}else if(lastSpellPlayer1==3 && runeCountList[1]>0){
//							
//							desiredRuneType = 1;
//						}else{
//						//if nothing to counter it will choose rune of the highest count
//							lowestIndex = runeCountList.IndexOf(runeCountList.Max()); 
//									desiredRuneType = lowestIndex; 
//							}
//
//			
//			//use lowest index so i dont have to create another variable
//			print ("/ o/ o/ o/ o/ o/ o/ o/ the Counter index is " + desiredRuneType);
//
//		}
//
//		if (enemyIntelligence < 6 && enemyIntelligence >= 4) { //this would be good for a smarter mage or maybe a Skele King or something
//			
//			//this enemy will try to counter the player if possible, and it will check the amount runes on the board
//			//in case it will be counter attacked
//			
//			//0 is wind, 1 fire, 2 water, 3 earth
//			if(lastSpellPlayer1==0 && runeCountList[3]>0 && runeCountList[1]<5){ //checks if the fire runes are less than 5
//				
//				desiredRuneType = 3;
//			}else if(lastSpellPlayer1==1 && runeCountList[2]>0 && runeCountList[0]<5){ //checks if wind runes are less than 5
//				
//				desiredRuneType = 2;
//			} else if(lastSpellPlayer1==2 && runeCountList[0]>0 && runeCountList[3]<5){
//				
//				desiredRuneType = 0;
//			}else if(lastSpellPlayer1==3 && runeCountList[1]>0 && runeCountList[2]<5){
//				
//				desiredRuneType = 1;
//			}else{
//				//if nothing to counter it will choose rune of the Lowest count
//				//will this work if the rune of lower count is zero????
//				lowestIndex = runeCountList.IndexOf(runeCountList.Max()); 
//				desiredRuneType = lowestIndex; 
//			}
//			
//			
//			//use lowest index so i dont have to create another variable
//	//		print ("/ o/ o/ o/ o/ o/ o/ o/ the Counter index is " + desiredRuneType);
//			
//		}
//
//		//for intelligences 3/2/1
//
//		//3 will check for any other runes that contain all of one type and pick a 4 match if the counter isnt around
//		//maybe have a list of lists for each rune type
//
//		//1-2 will maybe plan on the placement of the runes and do what 1-2 do as well
//
//
////		print ("the enemy wants " + desiredRuneType + " types of runes");
//
//
//		runeCountList.Clear ();
//	}

//	public IEnumerator ChooseRunes(List<GameObject> RunesSelected, List<GameObject> Path){
//		
//		while (true) {
//		
//			//	print ("choosing runes!");
//
//				//this checks if its the enemy's turn and if their health is greater than zero
//				if (Camera.main.GetComponent<CampaignGameOrganizer> ().playerInt == 2 && this.gameObject.GetComponent<ControlEnemy> ().playerHealthNew > 0) { //checks if its player 2's turn
//
//				//	int desiredRuneType = 3; //1 is fire rune
//
//					int pathLength = 3; //amount of times the loop is iterated through
//
//					//**one difficulty point is the neighbors is a gameobject, but the meteorlist contains a list of meteorcode scripts
//
//					if(RunesSelected == null){ //creates a list of the runes that have already been iterated through
//				
//					decideDesiredRune();//this will count runes and decide which rune type the enemy will try to pick
//
//						RunesSelected = new List<GameObject>();
//
//						//do something to make it not null?
//					}
//
//
//					if(Path == null){ //this is the current path of runes that the loop is on, it will eventually store the best path- the ones with most runes of a same type
//
//				//		print ("a new path is being created");
//						Path = new List<GameObject>();
//
//					}
//
//					//if the path is empty add the first rune that matches the given type
//
//
//					//this needs to
//					if(Path.Count==0){
//
//				//		print ("path count is zero");
//
//						for (int i=0; i<Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.Count; i++) { //this iterates through the entire list of runes
//							
//					//		print ("iterating at " + Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList [i].gameObject);
//							
//							//checks if the rune type is the type we want and if the rune is not in the list of runes that are not related or already checked
//							if (!RunesSelected.Contains (Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList [i].gameObject)) {
//								
//								Path.Add (Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList [i].gameObject); //adds the desired rune to the selectd path
//
//								RunesSelected.Add(Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList [i].gameObject); //adds to the runes selected list so its not added twice
//
//							//	print (Camera.main.GetComponent<CampaignGameOrganizer> ().meteorList [i].gameObject + "has been added to paths");
//								
////								print("the current path count is " + Path.Count);
//
//								break;
//
//								
//							}
//
//						}
//
//					}
//
//					int initialPathCount = Path.Count;
//
//					//for (int j=0; j<Path[Path.Count-1].GetComponent<MeteorCode>().Neighbors.Count; j++) { use this is things get cray
//						for (int j=0; j<Path.Count; j++) {
//
//						//	print("the amount of neighbors are " + Path[Path.Count-1].GetComponent<MeteorCode>().Neighbors.Count);
//								//gets the last rune in the current path list and checks if its equal to the desired rune type 
//						//iterates through all the neighbors
//
//
//							for (int k=0; k<Path[j].GetComponent<MeteorCode>().Neighbors.Count; k++){
//
//							if(Path[j].GetComponent<MeteorCode>().Neighbors[k].GetComponent<MeteorCode> ().runeIntDesignation == desiredRuneType
//							   && !Path.Contains(Path[j].GetComponent<MeteorCode>().Neighbors[k]) && Path.Count<4){
//
//								//print ("this has been added as a DESIRED neighbor " + Path[Path.Count-1].GetComponent<MeteorCode>().Neighbors[j]);
//
//								Path.Add (Path[j].GetComponent<MeteorCode>().Neighbors[k]); //this adds the current neighbor of the rune type we want
//
//								inapplicableRunesCount=0; //this needs to reset since the list will be iterating from the newly added rune and a new set of neighbors
//
//							}
//						}
//				
//
//
//
//				//		print ("current count of path " + Path.Count + " inital count is" + initialPathCount);
//						//this is to check if there is only one neighbor with a fire rune designation
//
//						//supposedly "continue" breaks the current loop so this will have the function run through the body and start again with the next path index
//						//	break;
//						continue;
//
//						//110215*** Have a final check to see if the path has no available neighbors that are of the matching type
//						// if there are no blocks of the matching type left, and the blocks that are matching are contained in the path list
//						// and/or if the path is greater than the path length, then add the list to the List of Lists in the new created class.
//
//				
//										// keep this old code because this is how you choose the runes and will be used once we have a good path.
//										//	Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocks [tempIntSelectedBlocksCount - 1].GetComponent<MeteorCode> ().Neighbors [j].GetComponent<MeteorCode> ().isSelected = true;
//										//	Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocks.Add (Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocks [tempIntSelectedBlocksCount - 1].GetComponent<MeteorCode> ().Neighbors [j]);
//										//	Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocksCount += 1; //counts how many blocks have been selected
//										
//						} 
//
//					//this checks if the path length has stayed the same after the iteration, if so, a random rune needs to be added
//					if(initialPathCount==Path.Count){
//
//					for (int l=0; l<Path.Count; l++) {
//
//						for (int m=0; m<Path[l].GetComponent<MeteorCode>().Neighbors.Count; m++){
//							
//							if(Path[l].GetComponent<MeteorCode>().Neighbors[m].GetComponent<MeteorCode> ().runeIntDesignation != desiredRuneType
//							   && !Path.Contains(Path[l].GetComponent<MeteorCode>().Neighbors[m]) && Path.Count<4){
//								
//					//			print ("this has been added as a DESIRED neighbor " + Path[l].GetComponent<MeteorCode>().Neighbors[m]);
//								
//								Path.Add (Path[l].GetComponent<MeteorCode>().Neighbors[m]); //this adds the current neighbor of the rune type we want
//
//									break; //so only one new rune is added
//							//	inapplicableRunesCount=0; //this needs to reset since the list will be iterating from the newly added rune and a new set of neighbors
//								
//							}
//							
//						}
//
//					}
//				}	
//							if (Path.Count == pathLength+1) { //why do i keep seeing the same lists over and over in the list that contins them
//																//path length + 1 is another way of saying 4
//
////								print ("Adding path to list of paths" + Path);
//
//										dictKey+=1;
//						
//											listOfPaths.Add(Path);
//
//												
//													Path =null;
//
//							}
//						
//
////							print ("end of enemy counter atk intell loop reacherd");
//
//
//				if(dictKey==16){ //if every block has been run through once
//
//
//					sortListofLists(desiredRuneType);
//
////					print ("paths have been sorted from 0-15 by best move, 0 being the best");
//
//					//the dumbest enemy would pick the middle lists or more of the lower middle since those would
//					//contain some runes but not all, //they would also pick runes at random or those that would not be effective
//
//
//					for(int i=0; i<listOfPaths[enemyIntelligence].Count; i++){
//						
////						print ("running through the dict");
//						yield return new WaitForSeconds (0.1f);
//						listOfPaths[enemyIntelligence][i].GetComponent<MeteorCode>().isSelected=true;
//						Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocks.Add (listOfPaths[enemyIntelligence][i]);
//						Camera.main.GetComponent<CampaignGameOrganizer> ().selectedBlocksCount += 1; //counts how many blocks have been selected
//						
//						
//					}
//					
//					dictKey=0;
//					listOfPaths.Clear();
//
//					yield break; //stops the coroutine
//
//				}	
//
//						
//					}
//
//					
//
//					//	}//checks if the player int is ==2
//
//					//this checks if the player is dead so it doesnt attack when the next enemy comes. gives the turn back to the player
//					if (this.gameObject.GetComponent<ControlEnemy> ().playerHealthNew <= 0 
//						&& Camera.main.GetComponent<CampaignGameOrganizer> ().playerInt == 2) {
//				
//					//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerInt = 1;
//
//					print ("turn is changing");
//				yield break; //stops the coroutine
//
//					}
//					//	} //this ends the loop for checking the runes
//
//			yield return new WaitForSeconds (0.01f);
//
//			}
//
//
//		}
//
//	public void sortListofLists ( int desiredRuneType){
//
//	listOfPaths =	listOfPaths.OrderByDescending(c => c.Count(gameObject => gameObject.GetComponent<MeteorCode>().runeIntDesignation == desiredRuneType)).ToList();
//		
//		for (int i=0; i<listOfPaths.Count; i++) {
//			
////			print ("the current list is : " + listOfPaths[i]);
//			
//			for(int j =0; j<listOfPaths[i].Count; j++){
//
////				print ("it contains : " + listOfPaths[i][j]);
////				print ("it contains : " + listOfPaths[i][j].GetComponent<MeteorCode>().runeIntDesignation);
//			}
//		}
//
//	}


}