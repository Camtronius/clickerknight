using UnityEngine;
using System.Collections;

public class iceTowerScript : MonoBehaviour
{

	public Texture2D timeTexture = null; 

	private int closestIndex=0; //this is the closest enemy index. This is the unit that player unit will atk
	private float distance=float.MaxValue; //this is the distance to that unit
	private float closestDistance = float.MaxValue; //this is the closest distance out of all the units
	public float elapse_time;

	public Vector3 targetPosition;
	public bool lightningAttack = false;

	private int attackRangeIceTower = 10;


	public LineRenderer _LineRenderer;
	public LineRenderer _LineRenderer2;

	public Vector3 currentPosition;



		
	void Awake()
	{
	//	currentPosition = this.transform.position;
		_LineRenderer = GetComponent<LineRenderer> ();
	}


	void Start ()
		{
	
		StartCoroutine ("ShootLightning");
		}
	
		// Update is called once per frame

	void Update ()
		{
/*
		//lightning attack thingie

		if (lightningAttack == true) {

		

			_LineRenderer.enabled = true;
					//	_LineRenderer.SetPosition (0, this.transform.position + new Vector3(0,2,0));
		
						for (int i=0; i<4; i++) {
								Vector3 pos = Vector3.Lerp (this.transform.position, targetPosition, i / 4.0f);
			
								pos.x += Random.Range (-0.9f, 0.9f);
								pos.y += Random.Range (-0.9f, 0.9f);
			
				_LineRenderer.SetPosition (i, pos);
						}
		
						_LineRenderer.SetPosition (3, targetPosition);
				}
		if (lightningAttack == false) {
			_LineRenderer.enabled = false;


		}
		//end lightning attack thingie
		
		*/
	}

	IEnumerator ShootLightning(){
		
		while (true) {
			
			if (lightningAttack == true) {
				
				
				
				_LineRenderer.enabled = true;
				//	_LineRenderer.SetPosition (0, this.transform.position + new Vector3(0,2,0));
				
				for (int i=0; i<4; i++) {
					Vector3 pos = Vector3.Lerp (this.transform.position, targetPosition, i / 4.0f);
					
					pos.x += Random.Range (-0.5f, 0.5f);
					pos.y += Random.Range (-0.5f, 0.5f);
					
					_LineRenderer.SetPosition (i, pos);
				}
				
				_LineRenderer.SetPosition (3, targetPosition);
			
			}
			if (lightningAttack == false) {
				_LineRenderer.enabled = false;
				
				
			}
			
			yield return new WaitForSeconds(0.05f);
		}
		
	}





	
}

