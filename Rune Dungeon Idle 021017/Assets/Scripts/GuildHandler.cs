﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;


public class GuildHandler : MonoBehaviour {

    public List<GuildMemberInfo> guildMembers = new List<GuildMemberInfo>();
    public List<GameObject> guildInvites = new List<GameObject>();
    public List<GameObject> guildMessages = new List<GameObject>();
    public List<GameObject> guildLeaderboard = new List<GameObject>();

    public InputField guildField;
    public InputField usernameField;
    public InputField passwordField;

    public GameObject guildInvObject;
    public GameObject guildMsgObject;
    public GameObject guildLeaderBoardObject;

    //list of holder objects. They show different parts of the guild menu.
    public GameObject menuHolder;
    public GameObject chatroomHolder;
    public GameObject loginHolder;
    public GameObject createGuildHolder;
    public GameObject guildInfoHolder;
    public GameObject guildInvitesHolder;
    public GameObject guildMessagesHolder;
        public GameObject guildMessagesLayout; //this is what controls the vert layout of the messages
    public GameObject guildLeaderboardHolder;

    public bool leader, officier, member = false;

    public Client chatroomClient;

    private string username;
    public int score;

    //public bool Loggedin { get { return username != null; } }
    private bool loggedin = false;

    public Button submitRegBtn;
    public Button loginBtn;

    private double guildPoints;
    public GuildHall guildHallScript;

    //prevents spamming the same query over and over
    public float guildDelayTime = 0f;
    public float invitesDelayTime = 0f;
    public float guildMsgDelayTime = 0f;
    public float guildRankDelayTime = 0f;

    public Image guildInfoCooldown;
    public Image guildInvitesCooldown;
    public Image guildMsgCooldown;
    public Image guildRankCooldown;

    public Text goldBonusAMt;


    // Use this for initialization

    private void Start()
    {
        //this was just to test that we can spliot data
        //StartCoroutine(WebTest()); //for testing the phpo file
           usernameField.text= PlayerPrefs.GetString("UserName");
           passwordField.text = PlayerPrefs.GetString("Password");
    }

    private void Update()
    {
        if (guildDelayTime > 0)
        {
            guildDelayTime -= Time.deltaTime;
            guildInfoCooldown.fillAmount = guildDelayTime / 20f;
        }
        else
        {
            checkActiveCooldownImage(guildInfoCooldown);
        }

        if (invitesDelayTime > 0)
        {
            invitesDelayTime -= Time.deltaTime;
            guildInvitesCooldown.fillAmount = invitesDelayTime / 20f;
        }
        else
        {
            checkActiveCooldownImage(guildInvitesCooldown);
        }

        if (guildMsgDelayTime > 0)
        {
            guildMsgDelayTime -= Time.deltaTime;
            guildMsgCooldown.fillAmount = guildMsgDelayTime / 20f;
        }
        else
        {
            checkActiveCooldownImage(guildMsgCooldown);
        }

        if (guildRankDelayTime > 0)
        {
            guildRankDelayTime -= Time.deltaTime;
            guildRankCooldown.fillAmount = guildRankDelayTime / 20f;
        }
        else
        {
            checkActiveCooldownImage(guildRankCooldown);
        }
    }

    private void checkActiveCooldownImage(Image imageToCheck)
    {
        if (imageToCheck.enabled == true)
        {
            imageToCheck.enabled = false;
        }

    }

    public void CallRegister()
    {
        StartCoroutine(Register());
    }

    public void CallLogin()
    {
        StartCoroutine(LoginPlayer()); //also checks if they are in a clan
        //check clan and check clan invites are now only called on success of LoginPlayer above
        //we did this because we need to see if they are in a clan before we log in because it adds the clan tag to their Username
        
        //keep these here so we know they were added to LoginPlayer
        //StartCoroutine(CheckClan()); //also checks if they are in a clan
        //StartCoroutine(CheckClanInvites());

    }

    public void CallSaveData()
    {
        StartCoroutine(SavePlayerData());
    }

    public void CallCreateGuild()
    {
        StartCoroutine(CreateGuild());
    }

    public void SendGuildInv(GuildMemberInfo memberInfo)
    {
        StartCoroutine(GuildInv(memberInfo, guildMembers[0].memberName.text)); //GuildInv(memberInfo.memberName.text, guildMembers[0].memberName.text, memberInfo.memberRank)); //0 is the clan name
    }

    public void AcceptGuildInv(string sender, int rank, GameObject invite)
    {
        StartCoroutine(AcceptClanInvites(sender, rank, invite));
    }

    public void RejectGuildInv(string sender, GameObject invite)
    {
        StartCoroutine(RejectClanInvites(sender, invite));
    }

    public void KickGuildMem(GuildMemberInfo memberInfo)
    {

        memberInfo.kickCount += 1;

        if (memberInfo.kickCount < 5)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Tap 5 times to kick member!");
        }
        else
        { //if they confirm
            memberInfo.kickCount = 0;
            StartCoroutine(KickGuildMember(memberInfo, guildMembers[0].memberName.text));
        }              
    }

    public void PostGuildMessage(string msg)
    {
        StartCoroutine(PostGuildMessageRoutine(msg));
    }

    public void sendGuildPoints()
    {
        if (ObscuredPrefs.GetFloat("GP") < 30)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You need 30 GP to submit");
        }
        else
        {
            StartCoroutine(submitGuildPoints(guildMembers[0].memberName.text));
        }
    }

    IEnumerator Register()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", usernameField.text);
        form.AddField("password", passwordField.text);
        WWW www = new WWW("http://localhost/sqlconnect/register.php",form);
        yield return www;

        if (www.text == "0") //returns with no errors. It works.
        {
            Debug.Log("user successfully created");
            PlayerPrefs.SetString("UserName", usernameField.text);
            PlayerPrefs.SetString("Password", passwordField.text);
        }
        else
        {
            Debug.Log("user create failed error# " + www.text);
        }
    }

    IEnumerator CreateGuild()
    {
        if (guildField.text.Length == 4)
        {
            IngredientsMaster ingredObj = GameObject.Find("IngedientsHolder").GetComponent<IngredientsMaster>();

            if (ingredObj.totalGreenGems >= 50)
            {
                WWWForm form = new WWWForm();
                form.AddField("name", guildField.text.ToUpper());
                form.AddField("leader", username);

                WWW www = new WWW("http://localhost/sqlconnect/createguild.php", form);
                yield return www;
                print("guild field text is " + guildField.text);

                if (www.text == "0") //returns with no errors. It works.
                {
                    Debug.Log("guild successfully created");
                    ingredObj.totalGreenGems -= 50;
                    ingredObj.setNewPrefs();
                    ShowGuildInfo(); //this will refresh the page so we dont have the create clan screen anymore
                }
                else if (www.text == "1")
                {
                    Camera.main.GetComponent<MainMenu>().showErrorMsg("Cannot create, guild already exists");
                }
                else
                {
                    Debug.Log("guild create failed error# " + www.text);
                }
            }
            else
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("You need 50 gems to create a guild");
            }
        }
    }

    IEnumerator submitGuildPoints(string guildName)
    {

            WWWForm form = new WWWForm();
            form.AddField("guild", guildName);
            form.AddField("pointsAdd", ObscuredPrefs.GetFloat("GP").ToString());

            WWW www = new WWW("http://localhost/sqlconnect/addclanpoint.php", form);
            yield return www;
            print("clan points submitted " +www.text);

            if (www.text == "0") //returns with no errors. It works.
            {
                Debug.Log("successfully submitted pts!");
                ObscuredPrefs.SetFloat("GP", 0);
                StartCoroutine(CheckClan());//this will get our points update
            }
            else
            {
                Debug.Log("couldnt submit clan pts " + www.text);
            }        
    }

    public double getGuildPoints()
    {
        return guildPoints; //gives the user the guild points currentlyu owns
    }

    IEnumerator GuildInv(GuildMemberInfo memberinfo, string guildName)
    {
        WWWForm form = new WWWForm();
        form.AddField("sender", username); //they will have logged in and have a stored username
        form.AddField("receiver", memberinfo.memberName.text); //whatever they typed in
        form.AddField("guild", guildName); //get this from the guild name input field row
        form.AddField("rank", memberinfo.memberRank);

        WWW www = new WWW("http://localhost/sqlconnect/claninvite.php", form);
        yield return www;

        if (www.text == "0") //returns with no errors. It works.
        {
            //Debug.Log("clan invite successfully sent");
            Camera.main.GetComponent<MainMenu>().showErrorMsg("invite to: " + memberinfo.memberName.text + " sent!");
            memberinfo.memberNameInputField.text = ""; //erase the text and turn off invite button so they know its sent
            memberinfo.gameObject.SetActive(false);
        }
        else if (www.text == "1")
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Sorry, User: " + memberinfo.memberName.text +  " not found...");
        }
        else if (www.text == "2")
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("invite already sent to: " + memberinfo.memberName.text);
            memberinfo.memberNameInputField.text = ""; //erase the text and turn off invite button so they know its sent
            memberinfo.gameObject.SetActive(false);
        }
    }

    IEnumerator LoginPlayer()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", usernameField.text);
        form.AddField("password", passwordField.text);
        WWW www = new WWW("http://localhost/sqlconnect/login.php", form);
        yield return www;
        //Debug.Log(www.text);
        if (www.text[0] == '0')
        {
            username = usernameField.text; //this should be the same as we logged in with
           //score =  int.Parse(www.text.Split('\t')[1]);
            Debug.Log("Logged In Successful!");

            StartCoroutine(CheckClan()); //also checks if they are in a clan
            //StartCoroutine(CheckClanInvites());
        }
        else
        {
            Debug.Log("User login failed: Error #" + www.text);
        }
    }

    public void InitialLogIn()
    {
        loginHolder.SetActive(false); //so the player doesnt have to login again
        chatroomHolder.SetActive(true);
        menuHolder.SetActive(true);
        chatroomClient.ConnectToServer();//connects to the server with the Client script. and using the username from the field...
        loggedin = true;//logged into the SQL database, not necessarily the Chatroom. That is a different script.
    }

    public void ShowChat()
    {
        CloseWindows();
        chatroomHolder.SetActive(true);
    }

    public void ShowGuildInfo()
    {
        CloseWindows();
        StartCoroutine(CheckClan()); //may need cooldown timer so there arent to many requests for the server to handle. Maybe allow to check once every 3 seconds?
        guildDelayTime = 20f;
        guildInfoCooldown.enabled = true;
    }

    public void ShowGuildInvites()
    {
        CloseWindows();
        StartCoroutine(CheckClanInvites()); //may need cooldown timer so there arent to many requests for the server to handle. Maybe allow to check once every 3 seconds?
        invitesDelayTime = 20f;
        guildInvitesCooldown.enabled = true;
    }

    public void showLeaderboard()
    {
        CloseWindows();
        StartCoroutine(checkLeaderBoard());
        guildRankDelayTime = 20f;
        guildRankCooldown.enabled = true;

    }

    public void ShowGuildMessages()
    {
        CloseWindows();
        if (leader == true || officier == true || member == true)
        {
            guildMessagesHolder.SetActive(true); //change to the coroutine later

            StartCoroutine(CheckClanMessages());
        }
        else
        {
            guildMessagesHolder.SetActive(false); //change to the coroutine later

            Camera.main.GetComponent<MainMenu>().showErrorMsg("Sorry, you are not in a guild.");
        }
        guildMsgDelayTime = 20f;
        guildMsgCooldown.enabled = true;

    }

    public void CloseWindows()
    {
    clearInvites();
    clearMessages();
    clearLeaderboard();

    chatroomHolder.SetActive(false); //add in later
    loginHolder.SetActive(false);
    guildInfoHolder.SetActive(false);
    guildInvitesHolder.SetActive(false);
    createGuildHolder.SetActive(false);
    guildMessagesHolder.SetActive(false); //add in lkater
    guildLeaderboardHolder.SetActive(false);

    }

    public void clearInvites()//clears all the invites for a player (not in DB, but the gameobjects)
    {
        for (int i = 0; i < guildInvites.Count; i++) //destroy all the guild invites
        {
            Destroy(guildInvites[i].gameObject);
        }

        guildInvites.Clear(); //clear the list of invites
    }

    public void clearMessages()//clears all the invites for a player (not in DB, but the gameobjects)
    {
        for (int i = 0; i < guildMessages.Count; i++) //destroy all the guild invites
        {
            Destroy(guildMessages[i].gameObject);
        }

        guildMessages.Clear(); //clear the list of invites
    }

    public void clearLeaderboard()//clears all the invites for a player (not in DB, but the gameobjects)
    {
        for (int i = 0; i < guildLeaderboard.Count; i++) //destroy all the guild invites
        {
            Destroy(guildLeaderboard[i].gameObject);
        }

        guildLeaderboard.Clear(); //clear the list of invites
    }

    public void LogOut()
    {
        username = null;
    }

    IEnumerator CheckClan()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", usernameField.text);
        WWW www = new WWW("http://localhost/sqlconnect/checkclan.php", form);
        yield return www;
        Debug.Log("clan identifier is : " + www.text);

        int numGuildMembers = 0;

        if (www.text[0] == '0' || www.text[0] == '1' || www.text[0] == '2')//checks if the player is the leader, officier, or member ranks
        {
            if (www.text.Split('\t')[1] != "") //this is the name of the clan
            {
               chatroomClient.clanTag = "[" + www.text.Split('\t')[1] + "] ";

               string[] clanMembers = www.text.Split('\t');

               //enable clan privledges here:
               if (www.text[0] == '0')
               {
                    //leader privledges, kick/invite all
                    member = false;
                    officier = false;
                    leader = true;
               }
               if (www.text[0] == '1')
               {
                    //leader privledges, kick/invite all, leader can kick officier though
                    member = false;
                    leader = false;
                    officier = true;

               }
               if (www.text[0] == '2')
               {
                    //member privledges,  CANNOT kick/invite at all
                    officier = false;
                    leader = false;
                    member = true;
               }

               for (int i=0; i< guildMembers.Count; i++)
               {                    
                    Debug.Log("clan mems : " + clanMembers[i+1]);
                    if (clanMembers[i+1] != "") //if there is a clan member in that spot
                    {
                        numGuildMembers += 1; //this is so we know how many ppl are in the game and we can add the 1%GP bonus


                        guildMembers[i].memberName.text = clanMembers[i+1];
                        if (guildMembers[i].memberNameInputField != null)
                        {
                            if (leader || officier == true)
                            {
                                guildMembers[i].memberNameInputField.enabled = false;
                                guildMembers[i].kickButton.SetActive(true);
                            }
                            else //if they are members do not enable kick button
                            {
                                guildMembers[i].memberNameInputField.enabled = false;
                            }

                        }
                        if (clanMembers[i + 1] == username && leader==false) //if its you, then you can kick yourself, unless you are a leader
                        {
                            guildMembers[i].kickButton.SetActive(true);
                        }
                    }
                    else
                    {
                        if (member == true)
                        {
                            guildMembers[i].memberNameInputField.enabled = false;
                        }
                    }
               }


                if (leader == true || officier == true || member == true)
                {
                    chatroomClient.startGPEarn();

                    if (loggedin == true)//so it doesnt show this page at start
                    {
                        guildInfoHolder.SetActive(true); 
                    }
                }
            }
            else
            {
                //for some reason there was no clan tag...
            }

            //this is for getting guild points
            if(int.Parse(www.text.Split('\t')[7])>0)
            {
                Debug.Log("clan pts: " + www.text.Split('\t')[7].ToString());
                guildPoints = double.Parse(www.text.Split('\t')[7]); //sets the points so we can use them to determine lv in the guild hall scrpt

            }

            guildHallScript.methodsToCallOnStart();//this is so if we are submitting points it will automatically refresh the pts we have submitted

            PlayerPrefs.SetFloat("MemsGpBonus", (numGuildMembers-1)/100f);//we sub one because i think it counts the clan points or clan name as a person
            print("you have : " + (numGuildMembers-1) + " and you get a " + PlayerPrefs.GetFloat("MemsGpBonus").ToString() + " bonus");
            goldBonusAMt.text = (numGuildMembers - 1).ToString() + " Members gives<color=#FFEC00> + " + (numGuildMembers - 1).ToString() + "% gold</color> to all members!";

        }
        else if(www.text[0] == '3')
        {
            if (loggedin == true)
            {
                createGuildHolder.SetActive(true);//they arent in a guild
            }
        }
        else
        {
            Debug.Log("there was an unexpected error when checking if player is in a guild");
        }

        Debug.Log("Initial Log in was run after the coroutine was returned..this is a test");

        if (loggedin == false)
        {
            InitialLogIn(); //this logs user into chatroom with their clan tag if they are in a clan
        }
    }

    IEnumerator CheckClanInvites()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", usernameField.text);
        WWW www = new WWW("http://localhost/sqlconnect/checkclaninvite.php", form);
        yield return www;
        Debug.Log(www.text);
        if (www.text[0] == '0')//checks if the first character is 0
        {         
            if (www.text.Split('\t').Length>1) //you have a clan invite notification!
            {
                string[] claninvinfo = www.text.Split('\t');
                //each invite will contain 2 pieces of data Which Guild, and What Rank. It goes like 0-rank-guild-rank-guild-rank-guild etc.
                //instantiate a max of 3 invites at a time
                for (int i = 1; i < claninvinfo.Length; i++)
                {
                    Debug.Log(claninvinfo[i]);
                    if (i % 3 == 1 && claninvinfo[i] != "")//if i is divisible by 3 --> guild/rank/sender
                    {
                        if (guildInvites.Count < 6)
                        {
                            GameObject GuildInvite = (GameObject)Instantiate(guildInvObject, guildInvitesHolder.gameObject.transform) as GameObject;
                            GuildInvite.GetComponent<GuildInvite>().guildHandlerScript = this.gameObject.GetComponent<GuildHandler>();
                            GuildInvite.GetComponent<GuildInvite>().setSenderRank(claninvinfo[i + 2], int.Parse(claninvinfo[i + 1]));
                            guildInvites.Add(GuildInvite);//adds to the list so we can clear this when they click on another menu object. This prevents duplicate invites being shown

                            string tempRankString = "rank?";

                            switch (int.Parse(claninvinfo[i + 1]))
                            {
                                case 1:
                                    tempRankString = "General";
                                    break;
                                case 2:
                                    tempRankString = "Member#1";
                                    break;

                                case 3:
                                    tempRankString = "Member#2";
                                    break;
                                case 4:
                                    tempRankString = "Member#3";
                                    break;
                            }
                            GuildInvite.GetComponent<GuildInvite>().inviteText.text = claninvinfo[i] + " invited you to join as: " + tempRankString;
                        }
                    }
                }
            }
            else
            {
                Debug.Log("You do not have any invites");
            }

            if (loggedin == true)//so it doesnt open on start
            {
                guildInvitesHolder.SetActive(true);
            }


        }
        else
        {
            Debug.Log("Guild Check failed: Error #" + www.text);
        }

    }

    IEnumerator AcceptClanInvites(string sender, int rank, GameObject invite)
    {
        WWWForm form = new WWWForm();
        form.AddField("sender", sender);
        form.AddField("receiver", usernameField.text);
        form.AddField("rank", "rank" +rank.ToString()); //convert from rank int to rankX string so we can use this in our PHP Query

        Debug.Log("accepting invite from : " + sender + " for rank " + "rank" + rank.ToString());

        WWW www = new WWW("http://localhost/sqlconnect/acceptinvite.php", form);
        yield return www;
        Debug.Log("the invite result is " + www.text);
        if (www.text[0] == '0')
        {
            //success, you joined the clan!
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You joined guild [" + www.text.Split('\t')[1] + "]!");
            Destroy(invite);//destroys the invite so you cant see it anymore

        }else if (www.text[0] == '1')
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Sorry, you can only join one guild.");
        }
        else if (www.text[0] == '2')
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Sorry, that spot has been filled.");
        }
        else
        {
            //error
        }
    }

    IEnumerator RejectClanInvites(string sender, GameObject invite)
    {
        WWWForm form = new WWWForm();
        form.AddField("sender", sender);
        form.AddField("receiver", usernameField.text);

        Debug.Log("rejecting and deleting invite from : " + sender);

        WWW www = new WWW("http://localhost/sqlconnect/rejectinvite.php", form);
        yield return www;
        Debug.Log("the reject inv result is " + www.text);
        if (www.text[0] == '0')
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Invite rejected");
            Destroy(invite);//destroys the invite so you cant see it anymore

        }
        else
        {
            Debug.Log("there was an error rejecting the invite...");
        }
    }

    IEnumerator KickGuildMember(GuildMemberInfo memberinfo, string guildName) //just have it modify the player in the guild, nothing else
    {
        WWWForm form = new WWWForm();

        form.AddField("rank", "rank" + memberinfo.memberRank.ToString());
        form.AddField("guild", guildName);
        
        Debug.Log("kicking user : " + memberinfo.memberName.text + " from rank "  + memberinfo.memberRank.ToString() + " from guild: " + guildName);

        WWW www = new WWW("http://localhost/sqlconnect/kickmember.php", form);
        yield return www;
        Debug.Log("the kick result is " + www.text);
        if (www.text[0] == '0')
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("You kicked " + memberinfo.memberName.text + " from the guild");
            if (leader || officier == true)
            {
                memberinfo.gameObject.GetComponentInParent<InputField>().enabled = true; //reenable the input field to invite
            }
            memberinfo.memberName.text = "Type name here to invite Member";
            memberinfo.gameObject.SetActive(false);            
        }
        else
        {
            //error
        }
    }

    IEnumerator PostGuildMessageRoutine(string message)
    {
        WWWForm form = new WWWForm();

        form.AddField("guild", guildMembers[0].memberName.text);
        form.AddField("sender", username);
        form.AddField("msg", message);

        Debug.Log("message to be posted is : " + message);

        WWW www = new WWW("http://localhost/sqlconnect/postclanmessage.php", form);
        yield return www;
        if (www.text == "0")
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Message Posted!");
        }
        else
        {
            Debug.Log("message post error" + www.text);
        }
    }

    IEnumerator CheckClanMessages()
    {
        WWWForm form = new WWWForm();
        form.AddField("guild", guildMembers[0].memberName.text);
        WWW www = new WWW("http://localhost/sqlconnect/loadclanmessages.php", form);
        yield return www;
        Debug.Log(www.text);
        if (www.text[0] == '0')//checks if the first character is 0
        {
            if (www.text.Split('\t').Length > 1) //you have a clan invite notification!
            {
                string[] clanmsginfo = www.text.Split('\t');
                //each invite will contain 2 pieces of data Which Guild, and What Rank. It goes like 0-rank-guild-rank-guild-rank-guild etc.
                //instantiate a max of 3 invites at a time
                for (int i = 1; i < clanmsginfo.Length; i++)
                {
                    Debug.Log(clanmsginfo[i]);
                    if (i % 3 == 1 && clanmsginfo[i] != "")//if i is divisible by 3 --> guild/rank/sender
                    {
                        if (guildMessages.Count < 10)
                        {
                            GameObject GuildMsg = (GameObject)Instantiate(guildMsgObject, guildMessagesLayout.gameObject.transform) as GameObject;
                            GuildMsg.GetComponentInChildren<Text>().text = clanmsginfo[i] + ": " + clanmsginfo[i + 1] + " -" + clanmsginfo[i + 2];
                            guildMessages.Add(GuildMsg);//adds to the list so we can clear this when they click on another menu object. This prevents duplicate invites being shown
                        }
                    }
                }
            }
            else
            {
                Debug.Log("You do not have any messages");
            }
        }
        else
        {
            Debug.Log("Clan Msg Check failed: Error #" + www.text);
        }
    }

    IEnumerator SavePlayerData()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", username);
        form.AddField("score", score);
        WWW www = new WWW("http://localhost/sqlconnect/savedata.php", form);
        yield return www;
        if(www.text == "0")
        {
            Debug.Log("game saved");
        }
        else
        {
            Debug.Log("save failed error #" + www.text);
        }

    }

    IEnumerator checkLeaderBoard()
    {
        WWWForm form = new WWWForm();
        WWW www = new WWW("http://localhost/sqlconnect/guildleaderboard.php", form);
        yield return www;
        if (www.text[0]=='0')
        {

            guildLeaderboardHolder.SetActive(true);

            string[] leaderboardInfo = www.text.Split('\t');

            for (int i = 1; i < leaderboardInfo.Length-1; i++)
            {
                if (i % 2 == 0)//if its 1 this is the guild name, if 
                {                    
                    GameObject guildRankObj = (GameObject)Instantiate(guildLeaderBoardObject, guildLeaderboardHolder.gameObject.transform) as GameObject;
                    guildRankObj.GetComponentInChildren<Text>().text =  "Rank " + (i/2).ToString() + ": [" + leaderboardInfo[i] + "] = " + leaderboardInfo[i + 1] + " points";
                    guildLeaderboard.Add(guildRankObj);//adds to the list so we can clear this when they click on another menu object. This prevents duplicate invites being shown                    }                   
                }
                else
                {

                }
            }
        }
        else
        {
            Debug.Log("leaderboard couldnt be called " + www.text);
        }
    }

    public void VerifyInputs()
    {
        if(usernameField.text.Length>=7 && passwordField.text.Length >= 7)
        {
            submitRegBtn.interactable = true;
            loginBtn.interactable = true;
        }
        else
        {
            submitRegBtn.interactable = false;
            loginBtn.interactable = false;
        }
    }

    public void VerifyInvites(Text memberNameText)
    {
        if (memberNameText.text.Length >= 7 && memberNameText.gameObject.GetComponentInParent<InputField>().enabled == true)
        {
            memberNameText.transform.parent.GetComponentInChildren<Button>(true).gameObject.SetActive(true);
        }
        else
        {

        }
    }
}
