﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRoutineIdleTime : MonoBehaviour {

	public void startTheRoutine(){ //start the routine in the idle time away script because anim attached to wrong object

		if (this.gameObject.name == "IdleTimeAwayPane") {
			this.gameObject.GetComponentInParent<IdleTimeAway> ().startRoutine ();
		}

	}
}
