﻿using UnityEngine;
using System.Collections;
using TMPro;

public class WaterBubbleAnimation : MonoBehaviour {

	public bool hasCollided = false;
	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public int createdByPlayer;
	public GameObject ScreenShaker;
	public SoundManager soundManager;
	public GameObject WaterExplosion;

	public AudioClip confusion;
		public AudioClip waterexplosion;



	// Use this for initialization
	void Start () {
	
		ScreenShaker = GameObject.Find ("ScreenShakerController");
		SpellCreator = GameObject.Find ("SpellListener");

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();


	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Floor")
		
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (1.0f,0.1f); //shakes the screen

//			print ("colliding! with" + coll.gameObject.name);
			
			if (hasCollided == false) {	

			Animator animator = GetComponent<Animator> () as Animator;
			animator.SetBool ("BubbleBounce", true);
		
			hasCollided=true;
		}
		
	}

	public void endBubbleBounce(){
		Animator animator = GetComponent<Animator> () as Animator;
		animator.SetBool ("BubbleBounce", false);

	}

	public void makeWaterBurst (){

		WaterExplosion = Instantiate (WaterExplosion, new Vector3 (this.gameObject.transform.position.x,
		                                                         this.gameObject.transform.position.y, 5), Quaternion.identity) as GameObject;

	}

	public void destroyThisObject(){

		if (SpellCreator.GetComponent<SpellCreator> () != null) {
			SpellCreator.GetComponent<SpellCreator> ().CounterAttackSpell (createdByPlayer);
		}
		if (SpellCreator.GetComponent<SpellCreatorCampaign> () != null) {
			SpellCreator.GetComponent<SpellCreatorCampaign> ().CounterAttackSpell (createdByPlayer);
		}
		print ("spell creator sent with bubble power");

		Destroy (this.gameObject);
	}

	public void returnToNormalGrav(){

		this.gameObject.GetComponentInChildren<WaterBubbleBehavior> ().returnToNormalGravity ();

	}

	public void playConfusionSound(){

		soundManager.confusionSound();


	}

	public void playExplosionSound(){

		soundManager.waterExplosion ();


	}
}
