﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class TwoHourShopRotation : MonoBehaviour {

	// Use this for initialization
	public TextMeshProUGUI textTillNextRotation;
	public GameObject dailyGemReward;
	public List <GameObject> StatsRotation = new List <GameObject>();
	public List <GameObject> CrystalRotation = new List <GameObject>();
	public List <int> oneThruSeven = new List <int>();


	public DateTime oldDate;
	public DateTime newDate;

	public double _TotalMins;

	public TextMeshProUGUI GoldCardAmt;
	public TextMeshProUGUI TapToCollect;

	public EnemyStatsInheriter EnemyStatsObj;
	public ControlPlayer1Character playerObjHolder;

	public GameObject goldNotification;

	public SoundManager soundManager;


	void Start () {
		
	}
	
	void OnEnable(){

		if (soundManager == null) {

			soundManager = Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ();

		}

		refreshSavedShopTime (); //this is a seperate method so i can call it from main menu and it will refresh notification

	}

	public void refreshSavedShopTime(){

		if (EnemyStatsObj == null) {

			EnemyStatsObj = Camera.main.GetComponent<MainMenu> ().enemyStatsObj; 
		}

		if(PlayerPrefs.GetString ("SavedTimeShop")!=""){ //checks if a time has been saved previouslyt

			long temp = Convert.ToInt64 (PlayerPrefs.GetString ("SavedTimeShop")); //gets the last time the shop was opened 

			newDate = System.DateTime.Now; //stores the new date of the system at time of game start

			oldDate = DateTime.FromBinary (temp);

			TimeSpan difference = newDate.Subtract (oldDate);

			_TotalMins = difference.TotalMinutes; //total mins since last opening the shop

			PlayerPrefs.SetFloat ("ShopMins", (float)_TotalMins + PlayerPrefs.GetFloat ("ShopMins")); //this is the amount of hours stored since last opening the shop

		}

		float totalMins = PlayerPrefs.GetFloat ("ShopMins"); //temp hours are the hours since last opening the shop. If >2 then display new cards

		if(totalMins<=119 && totalMins>0){

			float hours = (120- totalMins) / 60f;

			string amtHrs = hours.ToString ();
			amtHrs = amtHrs.Substring (0, 1); //gives the amt of hours

			float amtMinDecim = hours - (int)hours;
			int minsLeft = Mathf.FloorToInt( amtMinDecim*60);

			textTillNextRotation.text = "New deals appear in " + amtHrs.ToString() + " hr and " + minsLeft.ToString() + " min!";


		}else {

			PlayerPrefs.SetFloat ("ShopMins", 0); //resets this value so we are starting from another 2hr  value

			textTillNextRotation.text = "New deals appear in 2 hours!";

			loadNewStatsCards ();//loads and selects a new stats rotation card
			loadNewStatsCards2 (); //loads the 2nd stats card
			loadNewCrystalCards();//loads and selects a new crystal rotation card
			loadNewCrystalCards2();//loads the 2nd crys card


			//gold card will refresh every 2 hrs.
			PlayerPrefs.SetInt("GoldCardCollection",0);// 0 == not collected, 1 == collected
			goldNotification.SetActive(true);

		}

		activateCards ();//gets the saved cards
		SetupGoldCard(); //adds how much gold will be on gold card and checks if it has been used today

		PlayerPrefs.SetString ("SavedTimeShop", System.DateTime.Now.ToBinary ().ToString ()); //this is so we know how much time has passed since the player last opened the shop


	}

	public void activateCards(){

		//initiall set them all to inactive, so that only 4 can be active at a time. this is to prevent duplicates that happen if player is playing when the 2hrs is up
		for (int i = 0; i < StatsRotation.Count; i++) {

			StatsRotation [i].SetActive (false);

		}

		for (int j = 0; j < CrystalRotation.Count; j++) {

			CrystalRotation [j].SetActive (false);

		}

		StatsRotation [PlayerPrefs.GetInt ("StatsRotationCard")].SetActive (true);
		StatsRotation [PlayerPrefs.GetInt ("StatsRotationCard2")].SetActive (true);

		CrystalRotation [PlayerPrefs.GetInt ("CrystalRotationCard")].SetActive (true);
		CrystalRotation [PlayerPrefs.GetInt ("CrystalRotationCard2")].SetActive (true);

		//this is to protect the player from seeing duplicates if they are playing when the 2hr rotation comes around. If this happens, usually the player sees too many cards on the screen

	
	}


	public void SetupGoldCard(){

		if (PlayerPrefs.GetInt ("GoldCardCollection") == 0) {

            if (PlayerPrefs.GetInt("CurrentLevel") == 1) //because this list eventually moves after level 1. dont want to give them a ton of gold early 
            {
                GoldCardAmt.text = LargeNumConverter.setDmg((double)350f);

            }
            else
            {
                GoldCardAmt.text = LargeNumConverter.setDmg((double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.data[2]["LVCOST"] * 0.1f);

            }

            TapToCollect.text = "Tap to Collect!";

			goldNotification.SetActive (true);


		} else {

			GoldCardAmt.text =  "Collected!";
			TapToCollect.text = "";

			goldNotification.SetActive (false);

		}


	}

	public void collectGoldCard(){

			if(PlayerPrefs.GetInt("GoldCardCollection")==0){


            if (PlayerPrefs.GetInt("CurrentLevel") == 1) //because this list eventually moves after level 1. dont want to give them a ton of gold early 
            {
                playerObjHolder.playerGold += 350; //this is also set in SetupGoldCard(). If you change it here, change it there too.

            }
            else
            {
                playerObjHolder.playerGold += (double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.data[2]["LVCOST"] * 0.1f; //this is also set in SetupGoldCard(). If you change it here, change it there too.

            }

            PlayerPrefs.SetInt("GoldCardCollection",1);// 0 == not collected, 1 == collected

			goldNotification.SetActive (false);

			soundManager.purchaseWithGems ();

			}

		SetupGoldCard (); //to change the text to what it should be

	}

	public void loadNewStatsCards(){

		int randomCard1 = UnityEngine.Random.Range (0, StatsRotation.Count); //returns a random index in the stats rotation list

		if (StatsRotation [randomCard1].activeInHierarchy==true) {//if an already selected object is active 

			if(randomCard1==0){
			//StatsRotation [randomCard1].SetActive (false); //disable the previous card
				disableAllStatsCards();

				unprefCard (StatsRotation [randomCard1 + 1]);

					StatsRotation [randomCard1+1].SetActive(true); //move one index above if you land on the same card and its 0

						//add something here to save the new cards index for 2 hrs
						PlayerPrefs.SetInt("StatsRotationCard",randomCard1+1);

			}else{

				//StatsRotation [randomCard1].SetActive (false); //disable the previous card
				disableAllStatsCards();

					unprefCard (StatsRotation [randomCard1 - 1]);

						StatsRotation [randomCard1-1].SetActive(true); //move one index below if you land on the same card and its not = 0

							PlayerPrefs.SetInt("StatsRotationCard",randomCard1-1);

			}

		}else{ //if the object is not already active

			//disable all previous cards
			disableAllStatsCards();

			unprefCard (StatsRotation [randomCard1]);
				
				StatsRotation [randomCard1].SetActive (true);

					//set a the new card to active and save
					PlayerPrefs.SetInt("StatsRotationCard",randomCard1);

		}

	}

	public void disableAllStatsCards(){
		for (int i = 0; i < StatsRotation.Count; i++) {
			if(StatsRotation[i].activeInHierarchy==true){
				StatsRotation [i].SetActive (false);
			}
		}

	}


	public void loadNewStatsCards2(){

		oneThruSeven.Clear ();

		for (int i = 0; i < StatsRotation.Count; i++) {

			if (StatsRotation [i].activeSelf == false) {
				oneThruSeven.Add (i); 
			}

		}

		int indexChosen = UnityEngine.Random.Range (0,oneThruSeven.Count); //the number contained in this will be the number of the index available

		int indexAssigned = oneThruSeven [indexChosen];


		unprefCard (StatsRotation [indexAssigned]); //so it can be rebought on a refresh (using the hax button to gen new cards_)


		StatsRotation [indexAssigned].SetActive (true); //disable the previous card

		PlayerPrefs.SetInt("StatsRotationCard2",indexAssigned);

	}

	public void loadNewCrystalCards(){

		int randomCard2 = UnityEngine.Random.Range (0, CrystalRotation.Count); //returns a random index in the stats rotation list

		if (CrystalRotation [randomCard2].activeInHierarchy==true) {//if an already selected object is active 

			if(randomCard2==0){
				disableAllCrysCards();

				unprefCard (CrystalRotation [randomCard2 + 1]);//removes the playerpref if it has been bought before

					CrystalRotation [randomCard2+1].SetActive(true); //move one index above if you land on the same card and its 0

						//add something here to save the new cards index for 2 hrs
						PlayerPrefs.SetInt("CrystalRotationCard",randomCard2+1);

			}else{

				disableAllCrysCards();

				unprefCard (CrystalRotation [randomCard2 - 1]);

					CrystalRotation [randomCard2-1].SetActive(true); //move one index below if you land on the same card and its not = 0
			
						PlayerPrefs.SetInt("CrystalRotationCard",randomCard2-1);

			}

		}else{ //if the object is not already active

			//disable the previous card
			disableAllCrysCards();

			unprefCard (CrystalRotation [randomCard2]);

				CrystalRotation [randomCard2].SetActive (true);

					//set a the new card to active and save
					PlayerPrefs.SetInt("CrystalRotationCard",randomCard2);

		}

	}

	public void loadNewCrystalCards2(){

		oneThruSeven.Clear ();

		for (int i = 0; i < CrystalRotation.Count; i++) {

			if (CrystalRotation [i].activeSelf == false) {
				oneThruSeven.Add (i); 
			}

		}

		int indexChosen = UnityEngine.Random.Range (0,oneThruSeven.Count); //the number contained in this will be the number of the index available

		int indexAssigned = oneThruSeven [indexChosen];


		unprefCard (CrystalRotation [indexAssigned]); //so it can be rebought on a refresh (using the hax button to gen new cards_)


		CrystalRotation [indexAssigned].SetActive (true); //disable the previous card

		PlayerPrefs.SetInt("CrystalRotationCard2",indexAssigned);

	}

	public void disableAllCrysCards(){
		for (int i = 0; i < CrystalRotation.Count; i++) {
			if(CrystalRotation[i].activeInHierarchy==true){
				CrystalRotation [i].SetActive (false);
			}
		}

	}

	public void unprefCard(GameObject unprefThis){

		PlayerPrefs.SetInt (unprefThis.name, 0); //0 value will make any previous purchase of this card reset
	}


}
