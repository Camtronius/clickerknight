﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeteorSpawn : MonoBehaviour {

	public GameObject TestMeteor; //meteor object which gets spawned
	public GameObject GhostMeteor; //meteor object which gets spawned
	
	public int ColumnLength; //number of columns
	public int RowHeight; //number or rows
	RaycastHit hit; //for detecting if a meteor is selcted


	public List<MeteorCode> meteorList = new List<MeteorCode>();

	public List<float> copiedPlayerMeteorList = new List<float>();

	public List<GameObject> ghostMeteorList = new List<GameObject>();

	// Use this for initialization
	void Start () {

		StartCoroutine ("GhostMeteorList"); //creates feeler objects that will get the index of any meteor in that area at that time. So they can
		//easily be copied and reproduced

	}
	
	// Update is called once per frame
	void Update () {
	

	}
	IEnumerator GhostMeteorList(){
		while (true) {

			yield return new WaitForSeconds(3f);

			for(int i=0; i<meteorList.Count; i++){

				GhostMeteor = Instantiate (GhostMeteor, new Vector3 (meteorList[i].transform.position.x, meteorList[i].transform.position.y, 0), Quaternion.identity) as GameObject;

				GhostMeteor.GetComponent<GhostMeteor>().ghostIndex = i;

				ghostMeteorList.Add(GhostMeteor);


				GhostMeteor.name = "Ghost Meteor" + i.ToString();

				print ("ghost meteor being created!");
			}
			yield break;
		}
	}

	public void printIndex(MeteorCode MeteorObject){

		print("the index of this game object is: " + meteorList.IndexOf(MeteorObject));	//begins searching at index 0	

		MeteorObject.isSelected = true;

	}

	public void spawnMeteorStart(){

		meteorList.Clear ();

		for (int i = 0; i<RowHeight; i++) {

			for (int j=0; j<ColumnLength; j++) {

				GameObject Meteor = Instantiate (TestMeteor, new Vector3 (j, i, 0), Quaternion.identity) as GameObject;

					meteorList.Add (Meteor.GetComponent<MeteorCode> ()); //adds the meteor to the list

					//	int currentIndex = meteorList.IndexOf(Meteor.GetComponent<MeteorCode>());

							Meteor.GetComponent<MeteorCode> ().columnSpawned = j;
							
				Meteor.name = Meteor + meteorList.IndexOf(Meteor.GetComponent<MeteorCode>()).ToString();

			}
			
		}


	}

	public void ChangeBlockColor(){

		if(copiedPlayerMeteorList.Count>0){	
			
			print("copied meteorlist count is: " + copiedPlayerMeteorList.Count);
			
			print("meteorlist count is: " + meteorList.Count);
			
			
			for(int k=0; k<meteorList.Count; k++){
				
				print ("k index" + k);

				meteorList[k].createRuneColor((int)copiedPlayerMeteorList[k]);

				meteorList[k].LockColor=true;
			
				//	meteorList[k].runeIntDesignation=((int)copiedPlayerMeteorList[k]);
				
				//				this.gameObject.GetComponent<TurnedBasedGameOrganizer>().networkText.text = "color change for loop run!" + k.ToString() ;
			}
			
		}

	}

	public void spawnMeteor(int MeteorColumn){  //int MeteorIndexDestroyed

		GameObject Meteor =  Instantiate(TestMeteor,new Vector3(MeteorColumn ,4 ,0),Quaternion.identity) as GameObject ;
		Meteor.GetComponent<MeteorCode>().columnSpawned = MeteorColumn;
	
		meteorList.Add(Meteor.GetComponent<MeteorCode> ());
		Meteor.name = Meteor + meteorList.IndexOf(Meteor.GetComponent<MeteorCode>()).ToString();


	}
	
}
