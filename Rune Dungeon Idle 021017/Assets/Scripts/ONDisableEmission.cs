﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ONDisableEmission : MonoBehaviour {

	private ParticleSystem ps;


	void OnDisable(){

		ps = this.gameObject.GetComponent<ParticleSystem>();
		ParticleSystem.EmissionModule emis1 = ps.emission;
		emis1.enabled = false;
	
	}
}
