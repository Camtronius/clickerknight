﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum Upgrade {
	Spells,
	IncreasedCastSpeed,
	SpellFlurry, 
	IncreasedSpellDmg,
	SpellCrit,
	FreezeTime,
	SpellStun,
	Curse,
	ShallowGrave,
	FindRing,

	AutoAtk, 
	CriticalHit,
	IncreasedCritHit, 
	IncreaseAtkDmg,
	AtkFlurry,
	MegaSlash,
	DmgBelowHP,
	InstaKill,
	RupturedWound, //maybe make it so the next enemy attk does less dmg? instead of dmg over time?
	LifestealHit,

	GoldMiner,
	GoldCrit,
	ExtraGemDrop,
	ReduceUpgradeCostWeapon,
	ReduceUpgradeCostSpell, 
	ReduceUpgradeCostLevel, 
	IncreasedItemRate,
	PotionMastery, 
	GemShards, 
	GemEssence,

	ATKImprovedCrit,
	ATKImprovedFlurry,
	ATKUpgradePrest,
	ATKImprovedUltraSlash,
	ATKReturnDmg,

	MAGImprovedCrit,
	MAGImprovedFlurry,
	MAGUpgradePrest,
	MAGImprovedTimeFreeze,
	MAGReturnDmg,

	GLDImprovedChem,
	GLDImprovedSellPrice,
	GLDCritUpgradePrest,
	GLDBargainBin,
	GLDGemCollector,

    EVNTSpellShield,
    EVNTPetFlurry,
    EVNTPetCrit,
    EVNTSoulCharge,
    EVNTUseBow,
    EVNTBowMastery



};

	//add ring item?

public class ResearchUpgrade : MonoBehaviour {

	public List<ResearchUpgrade> prereqList = new List<ResearchUpgrade> ();

	public Upgrade UpgradeLevel;
	public GameObject relatedUpgradeLine;

	public int _newItemCost; //this is the cost after the level is factored in
	//public Slider researchSlider; //these are in the main char page

	public DateTime startingTime;
	public DateTime endingTime;

	public DateTime currentTime;
	public TimeSpan differenceTime;

	[Header("Time in Seconds")]
	public int _requiredTime; //this is in seconds

	//public TextMeshProUGUI researchTime;
	public int lvReqInt;
	public TextMeshProUGUI lvReq;

	public GameObject researchWindow; //opens the window to start research
	public ResearchYesNo researchStartScript; //the script attached to the window to actually start the research

	public TextMeshProUGUI researchTitle;
	public TextMeshProUGUI researchDescription;
	public string upgradeDescription;

	public TextMeshProUGUI pointCapText;
	public int pointCap;
    public int pointCapOvercharge; //extended point cap for those abilities that can be upgraded when the tree is full

    public int pointsInvested;

	public bool unlockable = false;
	public bool PrestigeTalent = false;
    public bool eventTalent = false; //this is for when to check if the trees are maxed, and also for research y.n



    void Awake(){


	}

    void Start()
    {
        checkOvercharge();
    }

        void OnEnable () {

        updateTalentUpgrade ();

	//	checkPrereqs (); //in researchYesNo
	}

    public void checkOvercharge()
    {
        if (researchStartScript.talentsMaxed == true && pointCapOvercharge > 0)
        {
            pointCapText.text = pointsInvested.ToString() + "/" + "<color=#FF0000>" + (pointCap + pointCapOvercharge).ToString() + "! </color>";
        }
        else if (researchStartScript.talentsMaxed == false && PrestigeTalent == false)
        {
            pointCapText.text = pointsInvested.ToString() + "/" + pointCap.ToString();

        }

        if (researchStartScript.prestigeTalentsMaxed == true && pointCapOvercharge > 0 && PrestigeTalent==true)
        {
            pointCapText.text = pointsInvested.ToString() + "/" + "<color=#FBFF00>" + (pointCap + pointCapOvercharge).ToString() + "! </color>";
        }
        else if(researchStartScript.prestigeTalentsMaxed == false && PrestigeTalent == true)
        {
            pointCapText.text = pointsInvested.ToString() + "/" + pointCap.ToString();

        }

        if (eventTalent == true)
        {
            pointCapText.text = pointsInvested.ToString() + "/" + pointCap.ToString();

        }

    }

	public void updateTalentUpgrade(){

	//	researchStartScript.UnlockTalentButton.SetActive (true);




		if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") >= 0) { //if the talent has been upgraded once

            //event talents

            if (UpgradeLevel == Upgrade.EVNTSpellShield)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Spell Shield";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Upgrade to increase Spell Shield HP% from " + (10f + 5*PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% to " +
                                                (10f + 5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%"; //increases up to 30%

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Creates a Shield that has " + (10f + 5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% of your HP to block enemy damage";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Creates a Shield that has " + (10f + 5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% of your HP to block enemy damage";

                }

            }

            if (UpgradeLevel == Upgrade.EVNTPetFlurry)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Pet Flurry";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Upgrade to Increase Pet Attack Speed from " + (25f * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% to " +
                                                (25f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% for 8 seconds"; //increases up to 30%

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Use this ability to Increase Pet Attack Speed by " + (25f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% for 8 seconds";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Use this ability to Increase Pet Attack Speed by " + (25f * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% for 8 seconds";

                }

            }

            if (UpgradeLevel == Upgrade.EVNTPetCrit)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Pet Critical Hit";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Upgrade to Increase Pet Critical Hit Chance from " + (3f * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% to " +
                                                (3f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%"; //increases up to 30%

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Gives all pets a " + (3f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+1)).ToString() + "% chance to critical hit for 2X damage";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Gives all pets a " + (3f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% chance to critical hit for 2X damage";

                }

            }

            if (UpgradeLevel == Upgrade.EVNTSoulCharge)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Soul Charge";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Upgrade damage to " + (4f + 2*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)) + "X Magic Damage and "
                        + (15f +5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X Atk Damage.";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Deals " + (4f + 2*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)) + "X Magic Damage and "
                        + (15f +5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X Atk Damage. Charge the ability by defeating enemies.";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Deals " + (4f + 2*PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "X Magic Damage and "
                                           + (15f +5*PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "X Atk Damage. Charge the ability by defeating enemies.";

                }

            }

            if (UpgradeLevel == Upgrade.EVNTUseBow)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Learn Bow";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Learn how to use a bow! <color=#dd480a>Max this Talent to Enable the Bow</color> You have " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " points so far.";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Learn how to use a bow! <color=#dd480a>Max this Talent to Enable the Bow</color>";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Learn how to use a bow!";

                }

            }

            if (UpgradeLevel == Upgrade.EVNTBowMastery)
            { //also check if it can be upgraded more and what your current upgrade is

                //3.0 is the default spell cast speed
                researchTitle.text = "Bow Mastery";

                if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
                {

                    researchDescription.text = "Upgrades bow damage from " + (4.1f + 3*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))/10f).ToString() + "X Atk Damage to "
                        + (4.1f + 3*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+1)/10f).ToString() + "X Atk Damage.";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") == 0)
                {

                    researchDescription.text = "Upgrades bow damage from " + (4.1f + 3*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))/10f).ToString() + "X Atk Damage to "
                        + (4.1f + 3*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)/10f).ToString() + "X Atk Damage.";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
                {

                    researchDescription.text = "Upgrades bow damage to " + (4.1f + 3 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) / 10f).ToString() + "X Atk Damage.";
                }

            }




            //end of event talents



            //Spell Talents

            if (UpgradeLevel == Upgrade.Spells) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Learn Magic";
				researchDescription.text = "Enables you to automatically cast spells and upgrade them";
				upgradeDescription = "";
				//	(8 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " seconds";
			}


			if (UpgradeLevel == Upgrade.IncreasedCastSpeed) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Magic Finesse";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text ="Upgrade to increase Cast Speed from " + (0.2f * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") / 4.0f * 100).ToString () + "% to " +
												(0.2f * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1) / 4.0f * 100).ToString () + "%"; //increases up to 30%

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {

					researchDescription.text = "Increases Spell Cast Rate by " + (0.2f * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1) / 4.0f * 100).ToString () + "%";

				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Increases Spell Cast Rate by " + (0.2f * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")) / 4.0f * 100).ToString () + "%";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Spell Cast Rate by " + (0.2f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) / 4.0f * 100).ToString() + "%";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Spell Cast Rate by " + (0.2f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) / 4.0f * 100).ToString() + "%";
                }

            }

			if (UpgradeLevel == Upgrade.SpellFlurry) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Rapid Casting";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase Rapid Casting time from " + (5+PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " seconds to " +
					(5+PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+ 1).ToString () + " seconds";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
					researchDescription.text = "Activate to Rapid Cast spells for " + (5+PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1).ToString () + " seconds";

                } else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Activate to Rapid Cast spells for " + (5+PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " seconds";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Activate to Rapid Cast spells for " + (5 + PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1).ToString() + " seconds";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Activate to Rapid Cast spells for " + (5 + PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " seconds";
                }



            }

					if (UpgradeLevel == Upgrade.MAGImprovedFlurry) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Rapid Casting Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
								researchDescription.text = "Upgrade to increase Rapid Cast Time from " + ((0.5f*PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " second(s) to " +
								(0.5f*((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1))).ToString () + " second(s)";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
								researchDescription.text = "Increases total time of Rapid Cast talent by " + (0.5f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";
						} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
								researchDescription.text = "Increases total time of Rapid Cast talent by " + (0.5f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " second(s)";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Increases total time of Rapid Cast talent by " + (0.5f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + " seconds";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Increases total time of Rapid Cast talent by " + (0.5f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + " second(s)";
                        }

            }

			if (UpgradeLevel == Upgrade.IncreasedSpellDmg) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Magic Mastery";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Upgrade to increase Spell Damage from " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% to " +
                        ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
					
					researchDescription.text = "Increases Spell Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";

				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed==false){
					
					researchDescription.text = "Increases Spell Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
				}
                //for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Spell Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap+pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Spell Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
                }

            }

			//for prestige
					if (UpgradeLevel == Upgrade.MAGUpgradePrest) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Magic Mastery Prestige";
					
						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
							researchDescription.text = "Upgrade to further increase Magic Damage from " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") * 33f).ToString () + "% to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1) * 33f).ToString () + "%";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0){
							researchDescription.text = "Further increases Magic Damage by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)* 33f).ToString () + "%";

						}else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
							researchDescription.text = "Further increases Magic Damage by " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")*33f).ToString () + "%";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases Magic Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * 33f).ToString() + "%";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases Magic Damage by " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * 33f).ToString() + "%";
                        }

                    }


			if (UpgradeLevel == Upgrade.SpellCrit) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Potent Magic";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Gives a " + (13 + 2 *(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% chance for a Critical Spell Hit";

				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=1){

					researchDescription.text = "Increases Critical Spell hit chance by " + (13 + 2 *(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";
				}
					
			}

					if (UpgradeLevel == Upgrade.MAGImprovedCrit) { //also check if it can be upgraded more and what your current upgrade is

							//3.0 is the default spell cast speed
							researchTitle.text = "Potent Magic Prestige";

							if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
								researchDescription.text = "Upgrade to increase Potent Magic multiplier from " + (2 + 0.15f*PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "X to " +
									(2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "X";

							} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
								researchDescription.text = "Increases Potent Magic multiplier to " + (2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "X";
							
							} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                            {
								researchDescription.text = "Increases Potent Magic multiplier by " + (2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "X";

							}
                            //for overcharge talents
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                            {
                                researchDescription.text = "Increases Potent Magic multiplier to " + (2 + 0.15f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X";
                            }
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                            {
                                researchDescription.text = "Increases Potent Magic multiplier by " + (2 + 0.15f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "X";
                            }


            }

			if (UpgradeLevel == Upgrade.FreezeTime) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Time Break";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Increase time frozen from " + (4 + PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " seconds to " +
					(4 + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " seconds after casting 26 spells";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "After casting 26 spells, you can Freeze Time for " + (4 + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))+1).ToString () + " seconds";
				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "After casting 26 spells, you can Freeze Time for " + (4 + PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " seconds";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "After casting 26 spells, you can Freeze Time for " + (4 + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) + 1).ToString() + " seconds";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "After casting 26 spells, you can Freeze Time for " + (4 + PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " seconds";
                }

            }

					if (UpgradeLevel == Upgrade.MAGImprovedTimeFreeze) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Time Break Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

							researchDescription.text = "Reduces amount of spells needed to Freeze time from " + (26 - 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " spells to " +
							(26 - 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " spells";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

							researchDescription.text = "Reduces amount of spells needed to Freeze time from " + (26 - 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " spells to " +
							(26 - 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " spells";
							
						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") >= 3 && researchStartScript.prestigeTalentsMaxed == false) {

							researchDescription.text = "Reduces amount of spells needed to Freeze time to " + (26 - 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " spells";

						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Reduces amount of spells needed to Freeze time from " + (26 - 2 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " spells to " +
                                                        (26 - 2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + " spells";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Reduces amount of spells needed to Freeze time to " + (26 - 2 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " spells";
                        }

            }

			if (UpgradeLevel == Upgrade.ShallowGrave) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Resurrection";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Increase health gained after resurrection from " + (5+5*(PlayerPrefs.GetInt((this.gameObject.name +"pointsInvested")))).ToString() + "% HP to " 
						+ (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% HP";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "At death, gives a 30% chance you will come back to life with " + (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% HP";
				
				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "At death, gives a 30% chance you will come back to life with " + (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() + "% HP";
				}
                //for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "At death, gives a 30% chance you will come back to life with " + (5 + 5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% HP";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "At death, gives a 30% chance you will come back to life with " + (5 + 5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% HP";
                }

            }

			if (UpgradeLevel == Upgrade.SpellStun) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Concussive Magic";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Increase spell stun chance for 1 second from " + (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() + "% to " 
						+ (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "%";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Gives any spell a " + (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% chance to stun the enemy for 1 second";

				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Gives any spell a " + (5+5*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() + "% chance to stun the enemy for 1 second";
                }
                //for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives any spell a " + (5 + 5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% chance to stun the enemy for 1 second";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives any spell a " + (5 + 5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% chance to stun the enemy for 1 second";
                }

            }

			if (UpgradeLevel == Upgrade.Curse) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Fatal Curse";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Increase the amount of total HP the enemy loses to the curse from " + (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")-1)/2f).ToString() + "% to " 
						+ (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))/2f).ToString() + "% per second";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Curses the Enemy to lose " + (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))/2f).ToString() + "% of their current HP every second";

				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3){

					researchDescription.text = "Curses the Enemy to lose " + (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")-1)/2f).ToString() + "% of their current HP every second";
				}

			}

			if (UpgradeLevel == Upgrade.FindRing) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Magic Ring";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

					researchDescription.text = "Increase the amount of attributes from " + (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")).ToString() + " to " 
						+ (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1).ToString() + " when finding a ring";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Enables you to find and equip Rings in chests with  " + (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1).ToString() + " random attribute";

				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Enables you to find and equip Rings in chests with  " + (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")).ToString() + " random attributes";
				}
                //for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Enables you to find and equip Rings in chests with  " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1).ToString() + " random attribute";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Enables you to find and equip Rings in chests with  " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + " random attributes";
                }

            }




			//Weapon Talents ********************************************************************************************************************************************
			if (UpgradeLevel == Upgrade.IncreasedCritHit) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Increased Critical Hit";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
					researchDescription.text = "Upgrade to increase Critical Hit chance from " + (1 + 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "% to " +
					(1 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";
				
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
					researchDescription.text = "Increases Critical Hit chance by " + (1 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "%";
				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Increases Critical Hit chance by " + (1 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

                } //for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Critical Hit chance by " + (1 + 2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Critical Hit chance by " + (1 + 2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "%";
                }
            }
						//for prestige
						if (UpgradeLevel == Upgrade.ATKImprovedCrit) { //also check if it can be upgraded more and what your current upgrade is

							//3.0 is the default spell cast speed
							researchTitle.text = "Critical Hit Prestige";

							if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
								researchDescription.text = "Upgrade to increase Critical Hit multiplier from " + (2 + 0.15f*PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "X to " +
									(2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "X";

							} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
								researchDescription.text = "Increases Critical Hit multiplier to " + (2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "X";
							} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                            {
								researchDescription.text = "Increases Critical Hit multiplier by " + (2 + 0.15f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "X";

							}
                            //for overcharge talents
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                            {
                                researchDescription.text = "Increases Critical Hit multiplier to " + (2 + 0.15f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X";
                            }
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                            {
                                researchDescription.text = "Increases Critical Hit multiplier by " + (2 + 0.15f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "X";
                            }
                        }

            // PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv")) / 100f; //need to add upgraded stats to this too

            if (UpgradeLevel == Upgrade.IncreaseAtkDmg) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Weapon Mastery";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
					researchDescription.text = "Upgrade to increase Weapon Damage from " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString () + "% to " +
                        ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
				
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0){
					researchDescription.text = "Increases Weapon Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
				
				}else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Increases Weapon Damage by " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Weapon Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Weapon Damage by " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%";

                }

            }

					if (UpgradeLevel == Upgrade.ATKUpgradePrest) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Weapon DMG Prestige";
						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
							researchDescription.text = "Upgrade to further increase Weapon Damage from " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") * 33f).ToString () + "% to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1) * 33f).ToString () + "%";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0){
							researchDescription.text = "Further increases Weapon Damage by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)* 33f).ToString () + "%";

						}else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
							researchDescription.text = "Further increases Weapon Damage by " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")*33f).ToString () + "%";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases Weapon Damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * 33f).ToString() + "%";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases Weapon Damage by " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * 33f).ToString() + "%";
                        }


            }

			if (UpgradeLevel == Upgrade.AtkFlurry) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Ambush";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
					researchDescription.text = "Upgrade to increase Ambush Time from " + (8 + 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " seconds to " +
						(8 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " seconds";
				
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
					researchDescription.text = "Unleashes a flurry of attacks for " + (8 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";
				} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Unleashes a flurry of attacks for " + (8 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Unleashes a flurry of attacks for " + (8 + 2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + " seconds";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Unleashes a flurry of attacks for " + (8 + 2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + " seconds";

                }

            }

			if (UpgradeLevel == Upgrade.ATKImprovedFlurry) { //also check if it can be upgraded more and what your current upgrade is

							//3.0 is the default spell cast speed
							researchTitle.text = "Ambush Prestige";

							if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
								researchDescription.text = "Upgrade to increase total Ambush Time by " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + " second(s) to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " second(s)";

							} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
								researchDescription.text = "Increases total time of Ambush talent by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";
							} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                            {
								researchDescription.text = "Increases total time of Ambush talent by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " second(s)";
							}
                            //for overcharge talents
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                            {
                                researchDescription.text = "Increases total time of Ambush talent by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + " seconds";
                            }
                            else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                                        {
                                researchDescription.text = "Increases total time of Ambush talent by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + " second(s)";
                            }

                        }

			if (UpgradeLevel == Upgrade.AutoAtk && researchStartScript.talentsMaxed == false) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Automatic Attack";
				researchDescription.text = "Automatically attack the enemy once every second";
				upgradeDescription = "";
				//	(8 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + " seconds";
			}else if (UpgradeLevel == Upgrade.AutoAtk && researchStartScript.talentsMaxed==true && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < 3)
            {
                researchTitle.text = "Automatic Attack";
                researchDescription.text = "Reduce Auto Attack Time by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))/20f*100).ToString() + "%";
                upgradeDescription = "";
            }
            else if(UpgradeLevel == Upgrade.AutoAtk && researchStartScript.talentsMaxed == true && PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= 3)
            {
                researchTitle.text = "Automatic Attack";
                researchDescription.text = "Reduce Auto Attack Time by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")-1)/20f*100).ToString() + "%";
                upgradeDescription = "";
            }


					if (UpgradeLevel == Upgrade.ATKReturnDmg) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Counter Attack Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
							researchDescription.text = "Upgrade from " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")*3).ToString () + " Attack(s) to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)*3).ToString () + " returned Attacks";
						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
							researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)*3).ToString () + " Attacks when attacked by an enemy";
						} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
							researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))*3).ToString () + " Attacks when attacked by an enemy";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                           researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * 3).ToString() + " Attacks when attacked by an enemy";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                           researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * 3).ToString() + " Attacks when attacked by an enemy";
                        }
                    }

					if (UpgradeLevel == Upgrade.MAGReturnDmg) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Counter Spell Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
							researchDescription.text = "Upgrade from " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")*20f).ToString () + "% returned Spell Damage to " +
						((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)*20f).ToString () + "% returned Spell Damage";
						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
							researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)*20f).ToString () + "% Spell Damage when attacked by an enemy";
						} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
							researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))*20f).ToString () + "% Spell Damage attacked by an enemy";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * 20f).ToString() + "% Spell Damage when attacked by an enemy";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Automatically returns " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * 20f).ToString() + "% Spell Damage attacked by an enemy";
                        }
                    }

			if (UpgradeLevel == Upgrade.CriticalHit) { //upgrade to increase dmg more %????

				//3.0 is the default spell cast speed
				researchTitle.text = "Critical Hit";
				researchDescription.text = "Enables you to hit the enemy for critical hits (Double Damage)";
//				upgradeDescription = "Upgrade to increase Critical Hit chance from " + (1 + 2 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "% to " +
//					(1 + 2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";
			}

			if (UpgradeLevel == Upgrade.MegaSlash) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Ulta Slash";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

                    researchDescription.text = "Upgrade to increase the special attack from " + (5*PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+10).ToString () + "X damage to "
						+ (5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+1)+10).ToString () + "X damage";

                } else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {

					researchDescription.text = "For every 250 attacks gain a special attack that does " + (5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1) + 10).ToString () + "X damage";

				}else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "For every 250 attacks gain a special attack that does " + (5 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 10).ToString () + "X damage";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "For every 250 attacks gain a special attack that does " + (5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) + 10).ToString() + "X damage";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "For every 250 attacks gain a special attack that does " + (5 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 10).ToString() + "X damage";

                }

            }

					if (UpgradeLevel == Upgrade.ATKImprovedUltraSlash) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Ulta Slash Prestige";
						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
							researchDescription.text = "Upgrade to increase the Ultra Slash Multiplier from " + (5*PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString () + "X to "
								+ (5*(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")+1)).ToString () + "X damage";
						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
					        researchDescription.text = "Upgrade to increase the Ultra Slash Multiplier by " + (5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "X damage";

						}else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {
					        researchDescription.text = "Increases the Ultra Slash Multiplier by " + (5 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "X damage";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                             researchDescription.text = "Upgrade to increase the Ultra Slash Multiplier by " + (5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X damage";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                             researchDescription.text = "Increases the Ultra Slash Multiplier by " + (5 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "X damage";
                        }

            }

			if (UpgradeLevel == Upgrade.DmgBelowHP) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Adrenaline";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
					researchDescription.text = "Increases Atk damage from " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% to " +
                        ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "%, when your HP is less than 40%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
					researchDescription.text = "Increases Atk damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% when your HP is less than 40%";
	
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Increases Atk damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% when your HP is less than 40%";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Atk damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% when your HP is less than 40%";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Increases Atk damage by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")) * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv"))).ToString() + "% when your HP is less than 40%";

                }

            }

			if (UpgradeLevel == Upgrade.RupturedWound) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Hemorrhage";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
					researchDescription.text = "Increases chance to wound from " + (1+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString () + "% to " +
						(1+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString () + "% for 5 seconds";
			
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0 ) {
					researchDescription.text = "Gives a " + (1+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString () + "% chance to cause a wound that reduces enemy" +
						" damage by  15% for 5 seconds";
				
				}else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {
					researchDescription.text = "Gives a " + (1+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString () + "% chance to cause a wound that reduces enemy" +
						" damage by  15% for 5 seconds";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives a " + (1 + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% chance to cause a wound that reduces enemy" +
                                            " damage by  15% for 5 seconds";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives a " + (1 + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% chance to cause a wound that reduces enemy" +
                                            " damage by  15% for 5 seconds";
                }

            }

			if (UpgradeLevel == Upgrade.InstaKill) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Preemptive Strike";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Increase enemy starting HP loss from -" + (2*(float)PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "% HP to -" +
						(2*(float)(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% HP";
					
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Enemies start with -" + (2*(float)(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% HP";
					
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3){

					researchDescription.text = "Enemies start with -" + (2*(float)(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% HP";
				}

			}

			if (UpgradeLevel == Upgrade.LifestealHit) {

				//3.0 is the default spell cast speed
				researchTitle.text = "Healing Blade";
				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "increase healing from "+ (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() + "% to " +
						(2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% of your HP after 30 total attack hits"; //goes 1-->6-->11


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Each time your attack totals more than 30 hits, heal "+ (2+((PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1))).ToString() + "% of your HP"; //goes 1-->6-->11


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Each time your attack totals more than 30 hits, heal "+ (2+(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() + "% of your HP"; //goes 1-->6-->11

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Each time your attack totals more than 30 hits, heal " + (2 + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1))).ToString() + "% of your HP"; //goes 1-->6-->11

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Each time your attack totals more than 30 hits, heal " + (2 + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% of your HP"; //goes 1-->6-->11

                }

            }


			//wealth talents
			if (UpgradeLevel == Upgrade.GoldMiner) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Gold Miner";
				researchDescription.text = "Gain a gold bounty every 5 seconds";
				upgradeDescription = "";

//				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {
//
//					researchDescription.text = "Decrease time required to gain a coin from "+ (8 - (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString ()  + " seconds to " +
//						(8 - (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds"; //goes 1-->6-->11
//
//
//				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {
//
//					researchDescription.text = "Gain a coin every " + (8 - (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";
//
//
//				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3){
//
//					researchDescription.text = "Gain a coin every " + (8 - (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds";
//
//				}
//
			}

			if (UpgradeLevel == Upgrade.GoldCrit) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Critical Coin Find";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase the chance for a critical coin (1.5X value) from " + (5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
						(5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";
					
				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Gives an " + (5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% chance for a critical coin (1.5X value)";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Gives an " + (5 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% chance for a critical coin (1.5X value)";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives an " + (5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% chance for a critical coin (1.5X value)";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gives an " + (5 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% chance for a critical coin (1.5X value)";

                }

            }

					if (UpgradeLevel == Upgrade.GLDCritUpgradePrest) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Critical Coin Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

							researchDescription.text = "Upgrade to increase the critical coin multiplier from " + (1.5f + 0.1f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "X to " +
							(1.5f + 0.1f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "X";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

							researchDescription.text = "Upgrade to increase the critical coin multiplier from " + (1.5f + 0.1f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "X to " +
							(1.5f + 0.1f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "X";

						} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {

							researchDescription.text = "Increases the critical coin multiplier to " + (1.5f + 0.1f*(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "X";

						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Upgrade to increase the critical coin multiplier from " + (1.5f + 0.1f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "X to " +
                            (1.5f + 0.1f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Increases the critical coin multiplier to " + (1.5f + 0.1f * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "X";
                        }

                    }

					if (UpgradeLevel == Upgrade.GLDImprovedSellPrice) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Sell Item Prestige";

						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) { //

							researchDescription.text = "Upgrade to increase Sell Price Bonus from " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") * 4f).ToString () + "% to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1) * 4f).ToString () + "%";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

							researchDescription.text = "Gives a " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1) * 4f).ToString () + "% Gold Price Bonus for items sold";

						} else if ( PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {

					        researchDescription.text = "Gives a " + (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") * 4f).ToString () + "% Gold Bonus to items sold ";
						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Gives a " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1) * 4f).ToString() + "% Gold Price Bonus for items sold";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Gives a " + (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") * 4f).ToString() + "% Gold Bonus to items sold ";
                        }

                    }

							if (UpgradeLevel == Upgrade.GLDGemCollector) { //also check if it can be upgraded more and what your current upgrade is

								//3.0 is the default spell cast speed
								researchTitle.text = "Gem Collect Prestige!";

								if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

									researchDescription.text = "Upgrade to increase the chance to auto collect a gem from " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
										(4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

								} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

									researchDescription.text = "Any gem has a " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% chance to be automatically collected";


								} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                                {

									researchDescription.text = "Any gem has a " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% chance to be automatically collected";

								}
                                //for overcharge talents
                                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                                {
                                    researchDescription.text = "Any gem has a " + (4 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% chance to be automatically collected";
                                }
                                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                                                {
                                    researchDescription.text = "Any gem has a " + (4 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "% chance to be automatically collected";
                                }

                            }

			if (UpgradeLevel == Upgrade.ExtraGemDrop) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Bigger Pockets";

				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase the chance for an extra coin drop from " + (10 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
						(10 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Gives a " + (10 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "% chance the enemy will drop an extra coin";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3){

					researchDescription.text = "Gives a " + (10 * PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")).ToString () + "% chance the enemy will drop an extra coin";

				}

			}



			if (UpgradeLevel == Upgrade.ReduceUpgradeCostWeapon) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Weapon Bargain";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to reduce the price of a weapon from " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
						(2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Reduces the price of weapon upgrades by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "%";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Reduces the price of weapon upgrades by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Reduces the price of weapon upgrades by " + (2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Reduces the price of weapon upgrades by " + (2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "%";

                }

            }

					if (UpgradeLevel == Upgrade.GLDBargainBin) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Bargain Bin Prestige!";


						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

							researchDescription.text = "Upgrade to reduce the price of Weapons / Spells / Worlds from " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
								((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

							researchDescription.text = "Reduce the price of Weapons / Spells / Worlds by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "%";


						} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {

							researchDescription.text = "Reduces the price of Weapons / Spells / Worlds by " + ((PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Reduce the price of Weapons / Spells / Worlds by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%";

                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Reduces the price of Weapons / Spells / Worlds by " + ((PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "%";
                        }

            }

			if (UpgradeLevel == Upgrade.ReduceUpgradeCostSpell) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Magic Efficiency";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to reduce the price of a spell from " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
						(2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Reduces the price of spell upgrades by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "%";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Reduces the price of spell upgrades by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Reduces the price of spell upgrades by " + (2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
					researchDescription.text = "Reduces the price of spell upgrades by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

                }

            }

				
		
			if (UpgradeLevel == Upgrade.PotionMastery) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Chemistry";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase the duration of all potions from " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds to " +
						(4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Increases the duration of all potions by " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3){

					researchDescription.text = "Increases the duration of all potions by " + (4 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds";

				}

			}

					if (UpgradeLevel == Upgrade.GLDImprovedChem) { //also check if it can be upgraded more and what your current upgrade is

						//3.0 is the default spell cast speed
						researchTitle.text = "Chemistry Prestige";


						if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

							researchDescription.text = "Upgrade to further increase the duration of all potions from " + (3 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds to " +
								(3 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";

						} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

							researchDescription.text = "Further increases the duration of all potions by " + (3 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + " seconds";


						} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.prestigeTalentsMaxed == false)
                        {

							researchDescription.text = "Further increases the duration of all potions by " + (3 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + " seconds";

						}
                        //for overcharge talents
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases the duration of all potions by " + (3 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + " seconds";
                        }
                        else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true)
                        {
                            researchDescription.text = "Further increases the duration of all potions by " + (3 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + " seconds";
                        }

                    }
		
		
			if (UpgradeLevel == Upgrade.GemShards) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Gem Shards";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Increase the damage to " + (2+(float)(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString () + "X your ATK and " +
                        ((2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)) * 10f).ToString() + "% your MAG damage when collecting gems"; 

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Collect a gem and damage the enemy equal to " + (2+(float)(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString () + "X your ATK and " +
                        ((2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1))*10f).ToString()+"% your MAG damage";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Collect a gem and damage the enemy equal to " + (2+(float)(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString () + "X your ATK and " +
                        ((2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))) * 10f).ToString() + "% your MAG damage";
                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Collect a gem and damage the enemy equal to " + (2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "X your ATK and " +
                        ((2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)) * 10f).ToString() + "% your MAG damage";
                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Collect a gem and damage the enemy equal to " + (2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "X your ATK and " +
                                        ((2 + (float)(PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))) * 10f).ToString() + "% your MAG damage";
                }

            }

			if (UpgradeLevel == Upgrade.GemEssence) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Gem Essence";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase the HP gained when collecting a gem from " + (3*PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")).ToString() + "% to " +
						(3*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% your max HP";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Every time you collect a gem, heal " + (3*(PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% of your HP";
                    
				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Every time you collect a gem, heal " + (3*PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")).ToString() + "% of your HP";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Every time you collect a gem, heal " + (3 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% of your HP";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Every time you collect a gem, heal " + (3 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% of your HP";

                }

            }

			if (UpgradeLevel == Upgrade.ReduceUpgradeCostLevel) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Trail Blazer";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to reduce the price of unlocking a new area from " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "% to " +
						(2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") + 1)).ToString () + "%";

				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Reduces the price of unlocking new areas by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")+1)).ToString () + "%";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Reduces the price of unlocking new areas by " + (2 * (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested"))).ToString () + "%";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Reduces the price of unlocking new areas by " + (2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "%";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Reduces the price of unlocking new areas by " + (2 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested"))).ToString() + "%";

                }

            }
		
		
			if (UpgradeLevel == Upgrade.IncreasedItemRate) { //also check if it can be upgraded more and what your current upgrade is

				//3.0 is the default spell cast speed
				researchTitle.text = "Item Reroll";


				if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") > 0 && PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") < 3) {

					researchDescription.text = "Upgrade to increase  your chance for a Item Reroll from " + (17 * (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested"))).ToString() 
						+ "% to " + (17 * (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "%";


				} else if (PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested") == 0) {

					researchDescription.text = "Gain a " + (17 * (PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")+1)).ToString() + "% chance to reroll an item for a slight gold cost";


				} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")>=3 && researchStartScript.talentsMaxed == false)
                {

					researchDescription.text = "Gain a " + (17 * PlayerPrefs.GetInt(this.gameObject.name +"pointsInvested")).ToString() + "% chance to reroll an item for a slight gold cost";

                }//for overcharge talents
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") < pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gain a " + (17 * (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") + 1)).ToString() + "% chance to reroll an item for a slight gold cost";

                }
                else if (PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested") >= pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
                {
                    researchDescription.text = "Gain a " + (17 * PlayerPrefs.GetInt(this.gameObject.name + "pointsInvested")).ToString() + "% chance to reroll an item for a slight gold cost";

                }

            }
		
		
		
		
		
		} else if(PlayerPrefs.GetInt (this.gameObject.name + "pointsInvested")== 0 && pointCap>1) {
			//	upgradeDescription = "This Talent is upgradable!";
						
		}
		if (pointsInvested == pointCap) {

		//	upgradeDescription = "This talent cannot be upgraded further";
		//	researchTime.text = "";

		//	researchStartScript.UnlockTalentButton.SetActive (false); //this doesnt need to be disabled here, it is disabled when clicked on
			upgradeDescription = ""; //this shouldnt say anything if it is maxed

			if (relatedUpgradeLine != null) {

		//		print ("the name of this object is " + this.gameObject.name);
		//		print ("the name of the white line for this object is " + relatedUpgradeLine.transform.parent.name);

				relatedUpgradeLine.SetActive (true);
			}

		} else {

		}

	}

	public void checkPrereqs (){

		if (prereqList.Count > 0) {

			for (int i = 0; i < prereqList.Count; i++) {

				//print (" I is " + i + " for " + this.gameObject.name);
				if (prereqList [i].pointsInvested == 0) {

					//		researchStartScript.UnlockTalentButton.SetActive (false);

					ColorBlock tmp = this.gameObject.GetComponentInChildren<Button> ().colors;

					tmp.normalColor = new Color (1f, 1f, 1f, 0.35f); 
					tmp.highlightedColor = new Color (1f, 1f, 1f, 0.35f); 
					tmp.pressedColor = new Color (1f, 1f, 1f, 0.35f); 

					this.gameObject.GetComponentInChildren<Button> ().colors = tmp;

			//		print ("preregs of " + this.gameObject.name + " not met");

					unlockable = false;

				} else if (prereqList [i].pointsInvested >= prereqList [i].pointCap) {

					//			researchStartScript.UnlockTalentButton.SetActive (true);

					ColorBlock tmp2 = this.gameObject.GetComponentInChildren<Button> ().colors;

					tmp2.normalColor = new Color (1f, 1f, 1f, 1f); 
					tmp2.highlightedColor = new Color (1f, 1f, 1f, 1f); 
					tmp2.pressedColor = new Color (1f, 1f, 1f, 1f); 
					this.gameObject.GetComponentInChildren<Button> ().colors = tmp2;

					unlockable = true;
//					if (relatedUpgradeLine != null) {
//
//						print ("the name of this object is " + this.gameObject.name);
//						print ("the name of the white line for this object is " + relatedUpgradeLine.transform.parent.name);
//
//						relatedUpgradeLine.SetActive (true);
//					}
					break;

				}
					
			}
				
		} else {

		//	researchStartScript.UnlockTalentButton.SetActive (true);

			ColorBlock tmp = this.gameObject.GetComponentInChildren<Button> ().colors;

			tmp.normalColor = new Color(1f,1f,1f,1f); 
			tmp.highlightedColor = new Color(1f,1f,1f,1f); 
			tmp.pressedColor = new Color(1f,1f,1f,1f); 

			this.gameObject.GetComponentInChildren<Button> ().colors= tmp;
			unlockable = true;
				
		}

	}

	public void clearTalentData(){

	//	PlayerPrefs.SetInt (this.gameObject.name + "pointsInvested", 0); //resets points invested
	//	PlayerPrefs.SetInt (this.gameObject.name + "isResearched", 0); //clears if this GO has been researched

	}

	void OnDisable(){

	//	StopCoroutine ("SliderUpdate");
	

	}

	public void refreshTalentPoints(){
        checkOvercharge();


    }

	public void researchButtonPress(){

		//the player will be presented with the yes/no research option

		researchWindow.SetActive (true); //opens the yes/no window
        researchStartScript.talentBkg.SetActive(true); //the backdrop for showing a talent icon
        researchStartScript.treeDescription.text = "";


        updateTalentUpgrade();


		researchStartScript.ResearchDetails (this.gameObject, _requiredTime, upgradeDescription); //gives the research window the necessary information

        if (pointsInvested == pointCap && researchStartScript.talentsMaxed == false || pointsInvested == pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
        {

            researchStartScript.amountSeconds.gameObject.SetActive(false);
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else if (unlockable == false)
        { //meaning the prereqs arent met, this is from prereqs method
            researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else if (PlayerPrefs.GetInt("ResearchPoints") == 0)
        {
            researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else if (researchStartScript.talentsMaxed == true && pointsInvested < pointCap + pointCapOvercharge)
        {
            researchStartScript.amountSeconds.gameObject.SetActive(true);
            researchStartScript.UnlockTalentButton.SetActive(true);

        }
        else
        {

            researchStartScript.amountSeconds.gameObject.SetActive(true);
            researchStartScript.UnlockTalentButton.SetActive(true);
        }

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().TalentSelect ();
	}

    public void eventButtonPress()
    {

        //the player will be presented with the yes/no research option

        researchWindow.SetActive(true); //opens the yes/no window
        researchStartScript.talentBkg.SetActive(true); //the backdrop for showing a talent icon
        researchStartScript.treeDescription.text = "";


        updateTalentUpgrade();


        researchStartScript.ResearchDetails(this.gameObject, _requiredTime, upgradeDescription); //gives the research window the necessary information

        if (pointsInvested == pointCap && researchStartScript.talentsMaxed == false || pointsInvested == pointCap + pointCapOvercharge && researchStartScript.talentsMaxed == true)
        {

            researchStartScript.amountSeconds.gameObject.SetActive(false);
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else if (unlockable == false)
        { //meaning the prereqs arent met, this is from prereqs method
            researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else if (PlayerPrefs.GetInt("EventPoints") == 0)
        {
            researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
            researchStartScript.UnlockTalentButton.SetActive(false);

        }
        else
        {

            researchStartScript.amountSeconds.gameObject.SetActive(true);
            researchStartScript.UnlockTalentButton.SetActive(true);
        }

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().TalentSelect();
    }

    public void researchPrestigeButtonPress(){

        //the player will be presented with the yes/no research option

        researchWindow.SetActive(true); //opens the yes/no window
        researchStartScript.talentBkg.SetActive(true); //the backdrop for showing a talent icon
        researchStartScript.treeDescription.text = "";

        updateTalentUpgrade ();


		researchStartScript.ResearchDetails (this.gameObject, _requiredTime, upgradeDescription); //gives the research window the necessary information

		if (pointsInvested == pointCap && researchStartScript.prestigeTalentsMaxed == false || pointsInvested == pointCap + pointCapOvercharge && researchStartScript.prestigeTalentsMaxed == true) {

			researchStartScript.amountSeconds.gameObject.SetActive (false);
			researchStartScript.UnlockTalentButton.SetActive (false);

		}else if(unlockable == false ){ //meaning the prereqs arent met, this is from prereqs method
			researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
			researchStartScript.UnlockTalentButton.SetActive (false);

		}else if(PlayerPrefs.GetInt ("PrestigePts")==0){
			researchStartScript.amountSeconds.gameObject.SetActive(true); //this means it takes x times to unlock etc.
			researchStartScript.UnlockTalentButton.SetActive (false);

		}
        else if (researchStartScript.prestigeTalentsMaxed == true && pointsInvested < pointCap + pointCapOvercharge)
        {
            researchStartScript.amountSeconds.gameObject.SetActive(true);
            researchStartScript.UnlockTalentButton.SetActive(true);

        }else {
			researchStartScript.amountSeconds.gameObject.SetActive(true);
			researchStartScript.UnlockTalentButton.SetActive (true);
		}

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().TalentSelect ();

	}

    //IEnumerator SliderUpdate(){

    //	while (true) {

    //		yield return new WaitForSeconds(1);

    //		startingTime = DateTime.FromBinary (Convert.ToInt64 (PlayerPrefs.GetString (this.gameObject.name + "StartTime")));
    //		currentTime = System.DateTime.Now;

    //		differenceTime = currentTime.Subtract (startingTime);

    //		if (differenceTime.TotalSeconds < _requiredTime) { //should this be less than or = to

    //			researchSlider.value = (float)(differenceTime.TotalSeconds / _requiredTime);

    //			researchTime.text = (Mathf.CeilToInt ((float)(_requiredTime - differenceTime.TotalSeconds))).ToString () + " sec";

    //		} else {
    //			//make the slider bar full
    //			researchSlider.value = 0;
    //			researchTime.text = "";

    //			researchSlider.gameObject.SetActive (false);//resch completed already
    //		//	updateTalentUpgrade ();
    //		//	checkPrereqs ();

    //			StopCoroutine ("SliderUpdate");
    //		}

    //	//	print ("corutnine running!");


    //	}
    //}


}


