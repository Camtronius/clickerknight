﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveAbilityManager : MonoBehaviour {

	// Use this for initialization

	public Image atkFlurryImg;
	public Image MegaSlashImg;
	public Image splFlurryImg;
	public Image FreezeTimeImg;
    public Image ShieldImg;
    public Image petFlurryImg;
    public Image soulChargeImg;




    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void unlockWeaponAnim(Transform location, Sprite wpnSprite){

		for (int i = 0; i < Camera.main.GetComponent<MainMenu>().talentAnimations.Count; i++) {

			if (! Camera.main.GetComponent<MainMenu>().talentAnimations [i].activeInHierarchy) {

				Camera.main.GetComponent<MainMenu>().talentAnimations [i].GetComponent<SpriteHolder> ().imageHeld.sprite = wpnSprite;

				Camera.main.GetComponent<MainMenu>().talentAnimations [i].transform.position = new Vector3 (location.position.x, location.position.y, 90); //instantiates the beam offscre

				//	magicCritDmgList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

				Camera.main.GetComponent<MainMenu>().talentAnimations [i].SetActive (true);

				break;
			}

		}

	}
}
