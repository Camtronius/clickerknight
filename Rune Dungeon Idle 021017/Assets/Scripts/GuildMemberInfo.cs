﻿using UnityEngine;
using UnityEngine.UI;

public class GuildMemberInfo : MonoBehaviour {

    public Text memberName;
    public int memberRank;
    public int kickCount=0; //you have to tap Kick 2x to kick someone

    public GameObject inviteButton;
    public GameObject kickButton;
    public InputField memberNameInputField;
    // Use this for initialization
    public void OnEnable()
    {
        kickCount = 0; //so you cant accidentally kick someone after 1 tap
    }

    public void VerifyInput(Text inviteName)
    {
        if (inviteName.text.Length >= 7 && memberNameInputField.enabled == true)
        {
            inviteButton.SetActive(true);
        }
        else
        {
            inviteButton.SetActive(false);
        }
    }
}
