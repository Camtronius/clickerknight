﻿using UnityEngine;
using System.Collections;

public class LayerOrderModifier : MonoBehaviour {

	public GameObject trail1;
	public GameObject trail2;
	// Use this for initialization
	void Start () {
	
		trail1.GetComponent<TrailRenderer> ().sortingOrder = 20;
		trail2.GetComponent<TrailRenderer> ().sortingOrder = 20;
		trail1.GetComponent<TrailRenderer> ().sortingLayerName = "FinalSort";
		trail2.GetComponent<TrailRenderer> ().sortingLayerName = "FinalSort";



	}

}
