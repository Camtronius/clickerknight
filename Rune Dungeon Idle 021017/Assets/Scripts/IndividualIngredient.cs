﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IndividualIngredient : MonoBehaviour {

	// Use this for initialization
	public enum IngredientType {GreenLeaf,BlueRose,Strawberry,Bananna};
	public IngredientType IngredientTypeSelect;

	public List<GameObject> ingredientGainPool = new List<GameObject>();

	public GameObject ingredientAdd;

	public IngredientsMaster masterIngredients;

	public float yCompOffset;



	public void onButtonPress(){

		this.gameObject.GetComponent<Button> ().interactable = false;
		this.gameObject.GetComponent<Animator> ().SetTrigger ("Clicked");
		this.gameObject.GetComponent<Image> ().raycastTarget = false;
        bool granikBonus = false;

        if (Camera.main.GetComponent<MainMenu>().petManager.abilityArray[19] > 0 
            && Random.Range(0, 100) < Camera.main.GetComponent<MainMenu>().petManager.abilityArray[19]*100) //if granik is active, and his ability rolls true
        {
            granikBonus = true;
        }

        for (int i = 0; i < ingredientGainPool.Count; i++) {

			if (ingredientGainPool [i].activeSelf == false) {

				switch ((int)IngredientTypeSelect) {

				case 0:
                        //ingredientGainPool [i].GetComponent<TextMeshProUGUI> ().color = Color.red;

                        if (granikBonus == true)
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#E2000BFF>" + "+2" + "</color>";

                            masterIngredients.totalRedGem += 2;
                        }
                        else
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#E2000BFF>" + "+1" + "</color>";

                            masterIngredients.totalRedGem += 1;
                        }

					break;

				case 1:

                        if (granikBonus == true)
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#4AF3FFFF>" + "+2" + "</color>";

                            masterIngredients.totalBlueGem += 2;
                        }
                        else
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#4AF3FFFF>" + "+1" + "</color>";

                            masterIngredients.totalBlueGem += 1;
                        }

					break;

				case 2:

                        if (granikBonus == true)
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#FBB024FF>" + "+2" + "</color>";

                            masterIngredients.totalYellowGem += 2;
                        }
                        else
                        {
                            ingredientGainPool[i].GetComponent<TextMeshProUGUI>().text = "<color=#FBB024FF>" + "+1" + "</color>";

                            masterIngredients.totalYellowGem += 1;
                        }
                        
					break;

				case 3:
				//	ingredientGainPool[i].GetComponent<TextMeshProUGUI> ().color = Color.yellow;
				//	masterIngredients.totalBananna += 1;
					//no longer used

					break;

				}

				masterIngredients.setNewPrefs (); //saves
				ingredientGainPool [i].gameObject.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y+ yCompOffset ,  this.transform.position.z); //instantiates the beam offscre
				ingredientGainPool [i].GetComponentInChildren<Image> ().sprite = this.gameObject.GetComponent<Image> ().sprite;

				Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().collectGem ();

				ingredientGainPool [i].SetActive (true);



				break;

			}

		}

		if (PlayerPrefs.GetInt ("FirstGem")==0) { //for how to bew potions

			//boss has been killed, que prestige explanation
			TutorialController tutObj = Camera.main.GetComponent<MainMenu>().TutorialObject;

			tutObj.refreshAllVariables ();//resets everything so we can restart the tutorial. Tutpart one will enable the obj

			tutObj.potionTutorial = true;
			tutObj.twoPartTutorial = true;

			tutObj.wizardSprite.sprite = tutObj.wizardSprites [2]; //laughing

			tutObj.disableButtons (tutObj.buttonList[4]);

			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().oldManConfident ();

			tutObj.TutPartOne ("Ho ho! I see you have collected a <color=#20fff2>Gem</color>...");
			tutObj.secondThingToSay = "<color=#20fff2>Gems are used to make potions</color>. They give you a boost! But, you need to collect all the requirements to brew one.";

			PlayerPrefs.SetInt ("FirstGem", 1);


		}

        if (granikBonus == false)
        {
            Camera.main.GetComponent<MainMenu>().gemEssenceActivate(1.0f); //if granik bonus false
            Camera.main.GetComponent<MainMenu>().gemShardsActivate(1.0f);
        }
        else
        {
            Camera.main.GetComponent<MainMenu>().gemEssenceActivate(2.0f); ////if granik bonus true
            Camera.main.GetComponent<MainMenu>().gemShardsActivate(2.0f);
        }

		
	
	}

	public void checkAutoCollect(){
		if (Camera.main.GetComponent<MainMenu> ().AutoGemCollector > 0) {
			StartCoroutine ("AutoCollectCheck");
		}
	}

	IEnumerator AutoCollectCheck(){ //***make sure this is only called once per game and doesnt repeatedly get called //this also checks for potions as well

		while (true) {

	//		print ("auto collect gem is running!");

			yield return new WaitForSeconds(2.5f);

			if (Camera.main.GetComponent<MainMenu> ().AutoGemCollector > Random.Range (0, 100)) {

				onButtonPress (); //autocollect the gem
			}

			StopCoroutine ("AutoCollectCheck");
		}
	}

}
