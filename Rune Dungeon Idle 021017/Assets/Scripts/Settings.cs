﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

	// Use this for initialization

	public TextMeshProUGUI secondsPlayed;
	//public TextMeshProUGUI windowsOpenText;

    public Image OnOffImage;
    public Sprite OnToggle;
    public Sprite OffToggle;

	void OnEnable () {

		secondsPlayed.text = PlayerPrefs.GetFloat ("TimePlayed").ToString ();	

		if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {

			//windowsOpenText.text = "Yes";
            OnOffImage.sprite = OnToggle;

		} else { //if ==1

			//windowsOpenText.text = "No";
            OnOffImage.sprite = OffToggle;

        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void pressRedditButton(){
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Application.OpenURL ("https://www.reddit.com/r/clickerknight");
	}

	public void pressDiscordButton(){
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Application.OpenURL ("https://discord.gg/2gyQ2vG");
	}

	public void pressWindowsOpenButton(){

		if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {

			PlayerPrefs.SetInt ("WindowsOpenPausing", 1);

			//windowsOpenText.text = "No";
            OnOffImage.sprite = OffToggle;


        }
        else { //if ==1

			PlayerPrefs.SetInt ("WindowsOpenPausing", 0);

			//windowsOpenText.text = "Yes";
            OnOffImage.sprite = OnToggle;

        }

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

    }
}
