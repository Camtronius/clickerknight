﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;


using TMPro;

public class ControlPlayer1Character : MonoBehaviour {

	public Animator animator;
	//spells

	public GameObject fireball;
		public int fireballCount=0;
			public bool player1FlameDOT = false;
				public bool justBurned = false;
	public GameObject firedrag;
	public GameObject WaterBeamObj;
	public GameObject GiantRock;
	public GameObject WaterBubble;
	public GameObject InvisParticles;
	public GameObject FlameDOT;
	public GameObject Tornado;
	public GameObject Treant;
	public GameObject Lightning; //new spell!
	public GameObject LightningHolder;
	public GameObject SolarBeam;
	public GameObject SpellShieldObj;
	public GameObject levelUpExplosion;
	public GameObject resurrectionAnim;


	public GameObject shotSpawn;
	public GameObject shotSpawnBehind;
	public GameObject shotSpawnTop;
	public GameObject shotSpawnTopPlayer2;


//	public GameObject player1RightHand;//for spawning the waterbeam

	public bool castFireBall = false;
	public bool castFireDragon = false;
	
	public double playerHealthMax = 100;
	public double playerHealthCurrent = 100;
	public double playerHealthNew= 100;
	public double playerGold = 0;
	public float increasedSpellRateTalent = 0;
	public float IncreasedSpellDmgTalent = 0;
	public float IncreasedSpellDmgPrestige = 0;


	public bool refreshSpellTimer = false;
	public float spellCastTimer = -1f;
	public TextMeshProUGUI castTimeText;
	public string castTimeString;

	//for gold miner
//	public float goldCastTimer = -1f;
//	public TextMeshProUGUI goldTimeText;
//	public string goldTimeString;


	public int playerRank = 0;
	public TextMeshProUGUI playerRankText;
	public TextMeshProUGUI playerGoldText;
	public GameObject playerGoldObj;

	public float lifeStealValue = 0;
	public bool playerDead = false;

	public double playerExpMax = 1000;
	public double playerExpCurrent = 0;
	public double playerExpNew= 0;

	//modifiers to the character
	public bool player1Invis=false; //tru if player is invis
	public bool player1ReturnToVisible = false; //does what it says ;p
	public bool lifeSteal= false;
	public bool isShielded = false;
	public bool isEnchanted = false;
	public bool LifeStealLearned = false;

	public bool player1Stun=false; //tru if player is invis

	//end mods to character

	public GameObject Player1HealthBar;
	public GameObject Player1ExpBar;

	public GameObject Player1Torso;
	public GameObject Player1Sword;

	public GameObject Player1PunchHand;
	public GameObject AutoAtkCollider;
	public GameObject enchantExplosion;

	public Vector3 defaultLocation;

	//for SFX
	public SoundManager soundManager;
	//for stats of everything
	public bool enableAutoSpells = false;

	public double playerGoldOld;

	public double playerAtkDmg = 10;
	//public double spellDmg = 0;



	public GameObject lvUpButton;
	public GameObject lvUpRaysGfx;
	public GameObject lvUpCountdown;

	public FreezeTimeCooldown freezeTimeSkill;

	public MainMenu CameraMenu;
	public Inventory InventoryScript;

	public float spellFlurryTimer = 0;
	//public TextMeshProUGUI spellFlurryTimerText;
	public int spellCritChance = 0;

    public double shieldHp;
    public double shieldHpMax;
    public bool shieldOn;
    public Slider shieldSlider;


    // Use this for initialization
    void Start () {

		increasedSpellRateTalent = 0; //so they arent loaded at start
		IncreasedSpellDmgTalent = 0;
		IncreasedSpellDmgPrestige = 0;

		CameraMenu = Camera.main.GetComponent<MainMenu> ();

		defaultLocation = this.transform.position;

		animator = GetComponent<Animator>();

        //StartCoroutine ("HealthBarGUI"); 

        StartCoroutine("SpellTest"); 

		loadPlayerPrefs (); //this needs to be before  the exp is loaded

		StartCoroutine ("ExpBarGUI"); 

		StartCoroutine ("GoldGUI");

		soundManager = CameraMenu.persistantSoundManager.GetComponent<SoundManager> ();



	}



    void Update(){

		if (refreshSpellTimer == true && spellCastTimer >= 0) {

			spellCastTimer -= Time.deltaTime;

			castTimeString =  spellCastTimer.ToString ();

            if (castTimeString.Length > 3)
            {
                castTimeString = castTimeString.Substring(0, 3);
            }
            else
            {

            }

            castTimeText.text = "Spell Cast: " + castTimeString;
		}

//		if (CameraMenu.goldMiningEnabled == true && goldCastTimer >= 0) {
//
//			goldCastTimer -= Time.deltaTime;
//
//
//			goldTimeString = goldCastTimer.ToString ();
//			goldTimeString = goldTimeString.Substring (0, 3);
//			goldTimeText.text = "Gold Miner: " + goldTimeString;
//
//		} else {
//			goldCastTimer = 5f;
//		}

	}
	public void loadPlayerPrefs(){


		//PlayerPrefs.SetString ("Gold", "123456");
		//for getting the player gold
		//playerGold = PlayerPrefs.GetFloat ("Gold"); //gets the current gold of the player
	//	print("The playerprefs string for  gold is " + PlayerPrefs.GetString ("Gold"));
		if (PlayerPrefs.GetString ("Gold") == "") {
			playerGold = 250; //so the player can buy an item right away in the tutorial
		} else {
			playerGold = double.Parse (PlayerPrefs.GetString ("Gold"));
		}
		playerGoldOld = playerGold;


		playerGoldText.text = LargeNumConverter.setDmg(playerGold).ToString(); //sets the players current gold

		//for setting the player rank
		if (PlayerPrefs.GetInt ("Rank") == 0) { //sets a default rank of 1 if there is no rank
			PlayerPrefs.SetInt("Rank",1);
        }

        CameraMenu.enemyStatsObj.loadPlayerExpMaxData(); //this is so it loads the correct rank if the rank is 0


        playerRank = PlayerPrefs.GetInt ("Rank");
		playerRankText.GetComponent<TextMeshProUGUI> ().text = "LV " + playerRank.ToString();

		//for setting current player exp
		playerExpMax = (double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.playerExpMaxData[0]["EXPMAX"];//PlayerPrefs.GetFloat ("ExpMax");//this is the max exp value for the player

		//playerExpNew = PlayerPrefs.GetFloat ("Exp"); //gets the current exp of the player

		if (PlayerPrefs.GetString ("Exp") == "") {
			playerExpNew = 0;
		} else {
			playerExpNew = double.Parse (PlayerPrefs.GetString ("Exp"));
		}

		playerExpCurrent = playerExpNew; //so it doesnt lv up automatically

		if (playerExpMax == 0) { //if the player is starting out fresh
			playerExpMax = (double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.playerExpMaxData[0]["EXPMAX"];
		}

		if (playerExpCurrent == 0) { //if the player is starting out fresh
			playerExpCurrent = 1f;
		}


		//slider values cannot be doubles, but this should be okay, because those will both be large enough values to be below the float num limit
		Player1ExpBar.GetComponent<Slider> ().value = (float)(playerExpCurrent/playerExpMax);


		if (PlayerPrefs.GetFloat ("HPItemMod") == 0) { //this is for lv1 hp starting a new game
		
			PlayerPrefs.SetFloat ("HPItemMod",10);
		
		Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = "1000/1000 HP";
		
		}

        playerHealthMax = returnPlayerMaxHP();
        //print ("should be adding " + Mathf.FloorToInt (playerHealthMax * (InventoryScript.totalListOfAttributes [0] / 100f)).ToString() + "to phpmax"); 

        InventoryScript.totalMods(); //so we not the HP totals before we start the gm

        playerHealthCurrent = playerHealthMax;
        playerHealthNew = playerHealthCurrent;

		Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = playerHealthCurrent.ToString() + "/" + playerHealthMax.ToString() + " HP";


		if (PlayerPrefs.GetFloat ("LifeStealLearned") == 1) {
			LifeStealLearned = true;
		};

		//if the first time ever playing the game the default settings will be
		//PlayerPrefs.GetFloat ("playerAtkDmg") == 0
		if (PlayerPrefs.GetString ("playerAtkDmg") == "") {

			playerAtkDmg = 100;

		} else {
			playerAtkDmg = double.Parse (PlayerPrefs.GetString ("playerAtkDmg"));
		}


		if (PlayerPrefs.GetString ("playerSplDmg")=="") {

            //this is defined in the inventory for some reason
            //	print ("this is running at the beginning of the game");
            //		PlayerPrefs.SetString ("playerSplDmg","413");
		}


//		if (PlayerPrefs.GetFloat ("playerSplDmg") == 0) {
//
//			PlayerPrefs.SetFloat ("playerSplDmg",400);
//
//		}


	}



	public void AutoSpells(Button button){

		if (enableAutoSpells == true) {

			enableAutoSpells = false;
			button.GetComponent<Animator> ().SetBool ("OpenBook", false);

		} else {
			enableAutoSpells = true;
			button.GetComponent<Animator> ().SetBool ("OpenBook", true);
		}
	}

	public void playerHit(){
		
		animator.SetBool ("PlayerHit", true);
		
		
	}

    public void resetBowAnims()
    {

        CameraMenu.resetBowAnimBools(false);
    }

    public void pullBow1()
    {

        soundManager.pullBowOne();
    }

    public void pullBow2()
    {

        soundManager.pullBowTwo();
    }

    public void shootArrow()
    {

        soundManager.shootArrow();
    }

    public void castFireSpell(){

		animator.SetBool ("FireSpell", true);


	}

	public void castDragSpell(){
		
		animator.SetBool ("FireDrag", true);


	}

	public void castWaterBeamSpell(){
		
		animator.SetBool ("WaterBeam", true);


	}

	public void castRockThrowSpell(){
		
		animator.SetBool ("RockThrow", true);


	}

	public void castWaterBubble(){
		
		animator.SetBool ("WaterBubble", true);


	}

	public void castInvisSpell(){
		
		animator.SetBool ("InvisSpell", true);


	}

	public void castTornadoSpell(){

		animator.SetBool ("TornadoSpell", true);


	}

	public void castTreantSpell(){
		
		animator.SetBool ("SummonTreant", true);


	}
	
	public void castSpellShield(){
		
		animator.SetBool ("SummonShield", true);

	}

	public void castLightningSpell(){
		
		animator.SetBool ("Lightning", true);
			
	}

	public void castSolarBeamSpell(){
		
		animator.SetBool ("SolarBeam", true);
		
		
	}

	public void castDefaultAttack(){
		if(isEnchanted==true){
			if(Player1Sword!=null){
			Player1Sword.SetActive(true);
			}
		}else{
			if(Player1Sword!=null){
			Player1Sword.SetActive(false);
			}
		}
		animator.SetBool ("DefaultAtkP1", true);	

	}

	public void castEnchantSword(){
		
		animator.SetBool ("EnchantSword", true);	
		
	}

	public void LevelUpMethod(){

		if (animator.GetBool ("isRunning") == true) {
			animator.SetBool ("isRunning",false);
		}
		animator.SetBool ("LevelUp", true);	

		
	}

	public void LevelUpExplosion(){

		GameObject LvUpExplsn = (GameObject)Instantiate (levelUpExplosion, new Vector3 (this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightHand.transform.position.x, this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightHand.transform.position.y, 0), Quaternion.identity);
	
	}

	public void undoSpellBools(){

		castFireBall = false;
		castFireDragon = false;

	}

	public void quickHPUpdate(){ //is this if the max hp changes?

        
        double _playerHealthMax = returnPlayerMaxHP();
        playerHealthMax = _playerHealthMax;

        if (playerHealthCurrent > _playerHealthMax) {
			playerHealthCurrent = _playerHealthMax;
            playerHealthNew = _playerHealthMax;

        }

        if (playerHealthNew < playerHealthCurrent)
        {
            playerHealthCurrent = playerHealthNew;
        }

        Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(_playerHealthMax) + " HP";
		Player1HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/_playerHealthMax);

    }

    public void createShield(double shieldHP)
    {
        shieldOn = true;
        shieldHpMax = shieldHP;
        shieldHp = shieldHP;
        shieldSlider.gameObject.SetActive(true);
        shieldSlider.value = (float)(shieldHpMax / shieldHpMax);


    }

    public void shieldHPUpdate()
    { //is this if the max hp changes?

        shieldSlider.value = (float)(shieldHp / shieldHpMax);

        if (shieldHp <= 0)
        {
            //destroy shield
            shieldOn = false;
            shieldSlider.gameObject.SetActive(false);

            //   PlayerPrefs.SetInt("ShieldActivated", 0);
        }

    }

    public void resetPlayerHP(){ //after player dies or a boss fight

		double _playerHealthMax = returnPlayerMaxHP();

        playerHealthCurrent = _playerHealthMax;
		playerHealthNew = _playerHealthMax;

		Player1HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/_playerHealthMax);
		Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = (playerHealthCurrent) + "/" + (_playerHealthMax); //where does it do this for the players hp?

	}

	IEnumerator HealthBarGUI(){
		
		while (true) {
		


			//if(lifeSteal==true && playerHealthCurrent<=_playerHealthMax){

			//	//if the value is less than current hp max
			//	if((playerHealthCurrent + lifeStealValue)<_playerHealthMax 
			//		&& (playerHealthNew+ lifeStealValue)<_playerHealthMax
			//	   && lifeSteal==true){

			//		playerHealthCurrent+=lifeStealValue;
			//		playerHealthNew+=lifeStealValue;

			//		lifeSteal=false;

			//	}

			//	//if playerhp + lifesteal value is more than max hp, just set as max hp
			//	if((playerHealthCurrent + lifeStealValue)>_playerHealthMax 
			//		&& (playerHealthNew+ lifeStealValue)>_playerHealthMax
			//	   && lifeSteal==true){

			//		playerHealthCurrent=_playerHealthMax;
			//		playerHealthNew=_playerHealthMax;

			//		lifeSteal=false;
			//	}


			//	Player1HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/_playerHealthMax);
			//	Player1HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(_playerHealthMax) + " HP";


			//	lifeSteal=false;

			//}

			yield return new WaitForSeconds(0.2f);
		}

	}


    public double returnPlayerMaxHP()
    {
        float additionalTemplePetGain = ((CameraMenu.petManager.abilityArray[10] + CameraMenu.petManager.abilityArray[11]) * 100f) * PlayerPrefs.GetInt("StatsAdd");//the ability array gives .02-4-6 times 100 gives 2-4-6 and then times the stats ad give the increase $ 

        float tempPlayerHealthMax = PlayerPrefs.GetFloat("HPItemMod") * 100 + ((PlayerPrefs.GetFloat("HealthMod")+ additionalTemplePetGain) / 100f) * PlayerPrefs.GetFloat("HPItemMod") * 100 + PlayerPrefs.GetFloat("TempHPBoost"); //this stays the same

        double _playerHealthMax = (tempPlayerHealthMax + (InventoryScript.totalListOfAttributes[0]))*(1+CameraMenu.petManager.abilityArray[18]);

        //print("hp item mod is " + PlayerPrefs.GetFloat("HPItemMod").ToString() + " health mod is " + PlayerPrefs.GetFloat("HealthMod").ToString() + " temp hp boost is " + PlayerPrefs.GetFloat("TempHPBoost").ToString());
        //print("p hp max is: " + playerHealthMax.ToString() + "inventiory attribs is: " + InventoryScript.totalListOfAttributes[0].ToString() + "pet ability array is : " + CameraMenu.petManager.abilityArray[18].ToString());

        return _playerHealthMax;

    }

	IEnumerator ExpBarGUI(){
		
		while (true) {
			
			if(playerExpCurrent<playerExpNew){ //for exp gain

				playerExpCurrent = playerExpNew;

				Player1ExpBar.GetComponent<Slider> ().value = (float)(playerExpCurrent/playerExpMax);

				PlayerPrefs.SetString ("Exp", playerExpCurrent.ToString()); //sets the current exp of the player


			}

			if(playerExpCurrent>playerExpMax){ //this resets the exp bar , plays the lvup, and adds the new goal

				double leftOverExp = (playerExpCurrent-playerExpMax)+1; //to save the leftover exp (the +1 in case its 0)

				if(playerRank<MainMenu.LVCAP){ //so the player cant go over the cap
					
				playerRank+=1;

				PlayerPrefs.SetInt("Rank",playerRank); //sets players rank as the new rank in player prefs

				playerRankText.GetComponent<TextMeshProUGUI>().text = "LV " + playerRank.ToString(); //sets the rank txt

                    //	playerExpNew = playerExpNew-playerExpMax; //this is so the exp doesnt start really high when you level up?

                    //important code line
                CameraMenu.enemyStatsObj.loadPlayerExpMaxData();//based on the players new rank this will shift the dictionary to increase his expmax

                playerExpMax = (double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.playerExpMaxData[0]["EXPMAX"]; 

				playerExpNew = leftOverExp;

				playerExpCurrent = 1; //remain exp to add
				

			//	PlayerPrefs.SetString ("ExpMax", playerExpMax.ToString());//this is the max exp value for the player

				PlayerPrefs.SetFloat("SkillPoints",PlayerPrefs.GetFloat("SkillPoints")+3);


				PlayerPrefs.SetInt ("ResearchPoints", PlayerPrefs.GetInt ("ResearchPoints") + 1);

				PlayerPrefs.SetString ("Exp", leftOverExp.ToString()); //sets the current exp of the player

				CameraMenu.windowOpen = true;//so stuff doesnt keep happening when the screen levels up
				lvUpButton.SetActive (true);
				lvUpRaysGfx.SetActive (true);
				lvUpCountdown.SetActive (true);

                    checkForManager();

                    soundManager.LevelUp();//plays rank up sound


                }
                else //if the player is over the level cap
                {
                    playerExpCurrent = playerExpMax; //this is so the player cannot level up past where he is intended to lv
                    playerExpNew = playerExpMax; //this is so the player cannot level up past where he is intended to lv

                }


                Player1ExpBar.GetComponent<Slider> ().value = (float)(playerExpCurrent/playerExpMax);
		//		print ("player is leveling up!!! aslkdjsalkjd");
				

			}
				
			yield return new WaitForSeconds(0.3f);
		}
		
	}

	IEnumerator GoldGUI(){

		while (true) {

			if(playerGoldOld<playerGold){ //for exp gain

			//	playerGold - playerGoldOld;

				playerGoldOld = playerGold;

				playerGoldText.text = LargeNumConverter.setDmg(playerGoldOld).ToString();

			//	PlayerPrefs.SetFloat ("Gold", (float)playerGold); //put this here so the game remembers gold more frequently

				PlayerPrefs.SetString ("Gold", playerGold.ToString ()); //this converts the Double value to a string so we can store larger numbers
				playerGoldObj.GetComponent<Animator> ().SetTrigger ("GoldBarAnim");


			//	print("updating p;layer gold");
			}

			if(playerGoldOld>playerGold){ //i think this is if something takes the amount of gold down

				playerGoldOld = playerGold;
				playerGoldText.text = LargeNumConverter.setDmg(playerGoldOld).ToString();

			//	PlayerPrefs.SetFloat ("Gold", (float)playerGold); //put this here so the game remembers gold more frequently

				PlayerPrefs.SetString ("Gold", playerGold.ToString ()); //this converts the Double value to a string so we can store larger numbers


		//		print("updating p;layer gold");


			}

			yield return new WaitForSeconds(0.5f);
		}

	}

	IEnumerator SpellTest(){

		while (true) {

			refreshSpellTimer = true;

            float spellRateModifier = (float)InventoryScript.totalListOfAttributes[6] + increasedSpellRateTalent;

            if (spellRateModifier > 2.5) //so the max cap on spell rate is 1.5 seconds. 3.5f - 2.5f = 1 (and then add the extra 0.5 seconds to check for double cast)
            {
                //print("capping spell rate at 1.5 seconds!");
                spellRateModifier = 2.5f;
            }
            else
            {
                //do nut'n
            }


            yield return new WaitForSeconds(3.5f - spellRateModifier);

		//	print ("the cast rate is: " + (4.00f - InventoryScript.totalListOfAttributes[6]/100f - increasedSpellRateTalent).ToString ());

//			PlayerPrefs.SetFloat ("Gold", playerGold); //put this here so the game remembers gold more frequently
//			PlayerPrefs.SetFloat ("Exp",playerExpCurrent); //gets the current exp of the player

			if (CameraMenu.MainCharacterPage.activeSelf == false
				&& CameraMenu.windowOpen ==false
				&& lvUpButton.activeSelf == false
			    && CameraMenu.spellsEnabled == true) {

                //	print ("Spell Dmg should be " + (PlayerPrefs.GetInt ("playerSplDmg") + InventoryScript.totalListOfAttributes [2]).ToString ());


                if (CameraMenu.petManager.abilityArray[3]*100 >= Random.Range(1,101))//rabbit chance is true..
                {
                    castSpell();

                }

                yield return new WaitForSeconds(0.5f); //combined with above this makes 4 seconds. this is to check if we are double casting a spell from Rabbit ability

                //	refreshSpellTimer = true;
                spellCastTimer = 4.00f - spellRateModifier;

                castSpell();


            }
            else {
				refreshSpellTimer = false;
			}
		
		}

	}

    public void castSpell() //b/c spell cast and spell flurry use the same code...
    {

        				double SpellDamage = spellDamageCalc ();

				bool spellCrit = false;

				if (Random.Range (0, 100) <= (spellCritChance + CameraMenu.petManager.abilityArray[8]*100) && CameraMenu.spellCritUnlocked) { //change this to ==1 after testing

					spellCrit = true;
					SpellDamage = SpellDamage * (2+CameraMenu.MagPrestCritMult); //this can be increased to more if spellcrit is upgraded

                    if (CameraMenu.petManager.abilityArray[5] > 0)
                    {
                        CameraMenu.spellCritHeal();
                    }

				}

				switch (PlayerPrefs.GetInt ("CurrentSpell")) {

				case 0:
					createFireBall (SpellDamage, spellCrit);
					break;

				case 1:
					createWaterBeam (SpellDamage, spellCrit); //add damages for these spells below later
					break;

				case 2:
					createGiantRock (SpellDamage, spellCrit);
					break;

				case 3:
					CreateTornado (SpellDamage, spellCrit);
					break;

				}
                freezeTimeSkill.decreaseFreezeCooldown(); //this is so the counter goes down for freeze time


    }

	IEnumerator SpellFlurry(){

		while (true) {

			yield return new WaitForSeconds(0.2f);

			if (CameraMenu.MainCharacterPage.activeSelf == false 
				&& CameraMenu.windowOpen ==false
				&& lvUpButton.activeSelf==false
				&& CameraMenu.spellsEnabled==true
				&& spellFlurryTimer>0) {

                //	print ("Spell Dmg should be " + (PlayerPrefs.GetInt ("playerSplDmg") + InventoryScript.totalListOfAttributes [2]).ToString ());

                //	print ("spell flurry timer is " + spellFlurryTimer);

                castSpell();

                yield return new WaitForSeconds(0.3f); //combined with above this makes a 0.5s delay, which is how fast we want flurry to be

                if (CameraMenu.petManager.abilityArray[3] * 100 >= Random.Range(1, 101))//rabbit chance is true..
                {
                    castSpell();

                }


                //				spellFlurryTimerText.text = spellFlurryTimer.ToString ();
                spellFlurryTimer -= 0.5f;

				if (spellFlurryTimer <= 0) {

					StopCoroutine ("SpellFlurry");
				}

			}



		}

	}

	public void playDeathSound(){

        checkForManager();

        soundManager.characterDown ();

	}

	public double spellDamageCalc(){ //so we only have one calc

        float additionalTemplePetGain = ((CameraMenu.petManager.abilityArray[10] + CameraMenu.petManager.abilityArray[11]) * 100f) * PlayerPrefs.GetInt("StatsAdd");//the ability array gives .02-4-6 times 100 gives 2-4-6 and then times the stats ad give the increase $ 


        double SpellDamage = (double.Parse(PlayerPrefs.GetString ("playerSplDmg")) + double.Parse(PlayerPrefs.GetString ("playerSplDmg"))*InventoryScript.totalListOfAttributes [2]/100 + 
			double.Parse(PlayerPrefs.GetString ("playerSplDmg"))*((PlayerPrefs.GetFloat ("luckMod") + additionalTemplePetGain) / 100f+IncreasedSpellDmgTalent+IncreasedSpellDmgPrestige))*(1+CameraMenu.potionModList[1]+CameraMenu.potionModList[6] + CameraMenu.potionModList[11]);

		if (ObscuredPrefs.GetInt ("StarterPack") == 1) {

			SpellDamage = 2 * SpellDamage; //this doubles player dmg
		}

		if (ObscuredPrefs.GetInt ("FuryPack") == 1) {

			SpellDamage = 2 * SpellDamage; //this doubles player dmg
		}

        if (CameraMenu.petManager.abilityArray[31] > 0)
        {
            SpellDamage = SpellDamage * (1 + CameraMenu.petManager.abilityArray[31] * CameraMenu.statsPointsScript.HealthMod / 100f);
        }

        if (CameraMenu.petManager.abilityArray[23] > 0)
        {
            SpellDamage = SpellDamage + (100*CameraMenu.petManager.abilityArray[23]*CameraMenu.CalcPlayerDmg()); //need to mult by 100 because it is listed as .01-->.02-->.03
        }

        return SpellDamage;

	}

	public void returnEnemyDmgMag(){

		if(CameraMenu.windowOpen == false){

			GameObject EnemyObj = CameraMenu.campaignLevelEnemySpawner.EnemySpawned;

			double SpellDamage = spellDamageCalc ()*(CameraMenu.returnMagPrestige*0.2f);

			CameraMenu.SpellDamage ((SpellDamage)); //sends the spell damage to the spell dmg text thing

			EnemyObj.GetComponentInChildren<ControlEnemy>().playerHealthNew -= SpellDamage;

		}
	}


	public void createFireBall(double spellDmg, bool isCrit){

		fireball.gameObject.GetComponent<FireBallCollider> ().createdByPlayer = 1;

		fireball.GetComponent<ConstantForce2D> ().force = new Vector2 (70f, 0);

		fireballCount += 1;
		
		GameObject FireballObj = (GameObject)Instantiate (fireball, new Vector3 (shotSpawn.transform.position.x, shotSpawn.transform.position.y, 90), Quaternion.identity);

		FireballObj.GetComponent<FireBallCollider> ().spellDMG = spellDmg;
		if (isCrit == true) {
			FireballObj.GetComponent<FireBallCollider> ().isCrit = true;
		} else {
			FireballObj.GetComponent<FireBallCollider> ().isCrit = false;
		}
		if (fireballCount >= 3) {
			fireballCount=0;
		}
	
	}

	public void createFireDrag(){
		
		firedrag.gameObject.GetComponent<DragonSpellPath> ().createdByPlayer = 1;
		
	//	print ("creating fire DRAGON");
		Instantiate (firedrag, new Vector3 (shotSpawnBehind.transform.position.x, shotSpawnBehind.transform.position.y, 0), Quaternion.Euler(0,0,316));
	}

	public void createWaterBeam(double spellDmg, bool isCrit){

		GameObject WaterBeam = (GameObject)Instantiate (WaterBeamObj); //instantiates the beam offscreen

		WaterBeam.GetComponentInChildren<BeamCollider> ().spellDMG = spellDmg;

		if (isCrit == true) {
			WaterBeam.GetComponentInChildren<BeamCollider> ().isCrit = true;
		} else {
			WaterBeam.GetComponentInChildren<BeamCollider> ().isCrit = false;
		}

		WaterBeam.GetComponent<BeamIncrementer> ().createdByPlayer = 1;

		WaterBeam.name = "WaterBeam";
	}

	public void createGiantRock(double spellDmg, bool isCrit){

		GameObject GiantRock2 = (GameObject)Instantiate (GiantRock, new Vector3 (shotSpawnTop.transform.position.x  , shotSpawnTop.transform.position.y, 90), Quaternion.identity); //instantiates the beam offscreen

		GiantRock2.GetComponent<RockCollider> ().spellDMG = spellDmg;

		if (isCrit == true) {
			GiantRock2.GetComponent<RockCollider> ().isCrit = true;
		} else {
			GiantRock2.GetComponent<RockCollider> ().isCrit = false;
		}

		GiantRock2.gameObject.GetComponent<RockCollider> ().createdByPlayer = 1;
	}

	public void closeWaterBeam(){
		GameObject.Find ("WaterBeam").GetComponent<BeamIncrementer> ().closeBeam = true;
	}

	public void closeSolarBeam(){
		GameObject SolarBeamReference = GameObject.Find ("SolarBeamObj");

		SolarBeamReference.GetComponent<SolarIncrementer> ().closeBeam = true;

		SolarBeamReference.GetComponent<SolarIncrementer> ().createExplosion (GameObject.Find("EnemySpawn").transform.position);

	}

	public void createWaterBubble(){

		GameObject WaterBubbleObj = (GameObject)Instantiate (WaterBubble, new Vector3 (shotSpawnTopPlayer2.transform.position.x, shotSpawnTopPlayer2.transform.position.y, 0), Quaternion.identity);
		WaterBubbleObj.GetComponentInChildren<WaterBubbleBehavior> ().createdByPlayer1 = true;
		WaterBubbleObj.GetComponent<WaterBubbleAnimation> ().createdByPlayer = 1;
	}

	public void CreateInvisParticles(){
		
		GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (Player1Torso.transform.position.x, Player1Torso.transform.position.y, 0), Quaternion.identity);
		InvisParticlesClone.transform.parent = this.transform;
		InvisParticlesClone.GetComponent<InvisibleWind> ().createdByPlayer = 1;
	}

	public void checkInvisCounter(){
	
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
			Camera.main.GetComponent<TurnedBasedGameOrganizer> ().InvisCounterCheckP1 (); 
		}
		//this is to check if both players are invis, so a player can counter attk when they are receiving a spell.
	}

	public void CreateTornado(double spellDmg, bool isCrit){
		
		GameObject TornadoClone = (GameObject)Instantiate (Tornado, new Vector3 (shotSpawn.transform.position.x, shotSpawn.transform.position.y, 90), Quaternion.identity);
		TornadoClone.GetComponentInChildren<TornadoSpellScript> ().createdByPlayer = 1;

		TornadoClone.GetComponentInChildren<TornadoSpellScript> ().spellDMG = spellDmg;

		if (isCrit == true) {
			TornadoClone.GetComponentInChildren<TornadoSpellScript> ().isCrit = true;
		} else {
			TornadoClone.GetComponentInChildren<TornadoSpellScript> ().isCrit = false;
		}

		TornadoClone.GetComponent<DestroyTornado> ().createdByPlayer = 1;
	}

	//this are a pair
	public void CreateLightning(){
		
		GameObject LightningClone = (GameObject)Instantiate (Lightning, new Vector3 (Player1PunchHand.transform.position.x, Player1PunchHand.transform.position.y, 0), Quaternion.identity);
	//sets the starting position to the players hand?
		LightningHolder = LightningClone;

		Vector3 EnemyTorso = GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().EnemySpawned.GetComponentInChildren<EnemyRagdoll>().Torso.transform.position; //this is show the amror wolf shows up level with the player (used to be skewed)

		LightningClone.GetComponent<LightningSpell> ().Beam1.transform.position = Player1PunchHand.transform.position;
		LightningClone.GetComponent<LightningSpell> ().Beam2.transform.position = Player1PunchHand.transform.position;

		LightningClone.GetComponent<LightningSpell> ().Beam1.GetComponent<iceTowerScript> ().targetPosition = EnemyTorso; //this is show the amror wolf shows up level with the player (used to be skewed)
		GameObject LightningFxClone1 = (GameObject)Instantiate (LightningHolder.GetComponent<LightningSpell>().LightningFx, new Vector3 (Player1PunchHand.transform.position.x, Player1PunchHand.transform.position.y, 0), Quaternion.identity);

		LightningClone.GetComponent<LightningSpell> ().Beam2.GetComponent<iceTowerScript> ().targetPosition = EnemyTorso; //this is show the amror wolf shows up level with the player (used to be skewed)
		GameObject LightningFxClone2 = (GameObject)Instantiate (LightningHolder.GetComponent<LightningSpell>().LightningFx, new Vector3 (EnemyTorso.x, EnemyTorso.y, 0), Quaternion.identity);

		LightningClone.GetComponent<LightningSpell> ().LightningCollider.transform.position = GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().EnemySpawned.transform.position;
		LightningClone.GetComponent<LightningSpell> ().LightningCollider.SetActive (true);
	}

	public void CreateSolarBeam(){

		GameObject SolarBeamLocation = GameObject.Find ("Enemy/ShotSpawnTop");

		GameObject SolarBeamObj = (GameObject)Instantiate (SolarBeam, new Vector3 (SolarBeamLocation.transform.position.x, SolarBeamLocation.transform.position.y, 0), Quaternion.identity);

		SolarBeamObj.name = "SolarBeamObj"; //so i can gind it later

		SolarBeamObj.GetComponent<SolarIncrementer> ().createGroundParticles (GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().EnemySpawned.transform.position);
	}

	public void StopLightning(){

		LightningHolder.GetComponent<LightningSpell> ().disableLightningAndDestroy ();
	}

		//this is a pair

	public void TornadoSpellSound(){

        checkForManager();
        soundManager.TornadoSound ();

	}

	public void swordEnchantText(){

		Camera.main.GetComponent<CampaignGameOrganizer> ().EnchantmentRdy ();
	}

	public void CreateFlameDOT(){
		
		GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);
		FlameDOTClone.transform.parent = this.transform;

		if (playerHealthCurrent <= 150) {

			playerHealthNew = 1;
		} else {

			playerHealthNew -= 150;

		}

		Camera.main.GetComponent<TurnedBasedGameOrganizer> ().damageP1Notification (150);
			

	}

	public void CreateTreant(){
		
		GameObject TreantClone = (GameObject)Instantiate (Treant, new Vector3 (shotSpawnBehind.transform.position.x, shotSpawnBehind.transform.position.y + 10 , 0), Quaternion.identity);
		TreantClone.GetComponentInChildren<TreantAnimation> ().createdByPlayer = 1;
	}

	public void CreateSpellShield(){
		
		GameObject SpellShieldClone = (GameObject)Instantiate (SpellShieldObj, new Vector3 (this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightLegUpper.transform.position.x, 
		                                                                                    this.gameObject.GetComponentInChildren<TurnOnRagdoll>().RightLegUpper.transform.position.y, 0), Quaternion.identity);
		SpellShieldClone.transform.parent = this.transform;
		SpellShieldClone.GetComponentInChildren<SphereShieldScript> ().createdByPlayer = 1;
		SpellShieldClone.GetComponentInChildren<SphereShieldScript> ().createShield = true;
	}

	public void CreateAutoAtkCollider(){
		
	//	GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (Player1Torso.transform.position.x, this.gameObject.GetComponentInChildren<TurnOnRagdoll>().LeftHand.transform.position.y, 0), Quaternion.identity);

	//	AutoAtkColliderClone.GetComponentInChildren<AutoAtk> ().createdByPlayer = 1;

	//	AutoAtkColliderClone.transform.parent = Player1PunchHand.transform;
	}

	public void CreateSwordAtkCollider(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
		

			ControlEnemy enemyToHit = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ();
			if(isEnchanted==true){

				GameObject enchantClone = (GameObject)Instantiate (enchantExplosion, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player2Torso.transform.position.y, 0), Quaternion.identity);


			}
			GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player2Torso.transform.position.y, 0), Quaternion.identity);
			AutoAtkColliderClone.GetComponentInChildren<SwordAtkCollider> ().createdByPlayer = 1;

		} else {
			ControlPlayer2Character enemyToHit = GameObject.Find ("Player2").GetComponentInChildren<ControlPlayer2Character> ();
			if(isEnchanted==true){
				GameObject enchantClone = (GameObject)Instantiate (enchantExplosion, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player2Torso.transform.position.y, 0), Quaternion.identity);

				
			}
			GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player2Torso.transform.position.y, 0), Quaternion.identity);
			AutoAtkColliderClone.GetComponentInChildren<SwordAtkCollider> ().createdByPlayer = 1;



		}

		Camera.main.GetComponent<CampaignGameOrganizer>().swordEnch [0] -=1;

	}

	public void CreateEnchantment(){

		//not sure what to add here yet...
		//add to camerea.main campaign game organizer --> turnbased gm organizer

		if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null && 
		   Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true){ //this is to take a turn after shield has been closed
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
		}
		
		if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null && 
		   Camera.main.GetComponent<CampaignGameOrganizer>().nextTurnBool==true){ //this is to take a turn after shield has been closed

			Camera.main.GetComponent<CampaignGameOrganizer>().enchManager(2,0); //adds 3 to the player one enchantment? Maybe make it so another enemy can enchant?

			Camera.main.GetComponent<CampaignGameOrganizer>().SpellListener.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
			
			Camera.main.GetComponent<CampaignGameOrganizer>().SpellListener.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(1); //created by P1
		}
		
	}
	
	public void endFireSpellAnimation(){ //add ending keyframes to the respective animations

		animator.SetBool ("PlayerHit", false);

		animator.SetBool ("LevelUp", false);
		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().LevelUpBool = false;
		}
		animator.SetBool ("FireSpell", false);

		animator.SetBool ("FireDrag", false);

		animator.SetBool ("RockThrow", false);

		animator.SetBool ("WaterBeam", false);

		animator.SetBool ("WaterBubble", false);

		animator.SetBool ("Lightning", false);

		animator.SetBool ("SolarBeam", false);

		animator.SetBool ("EnchantSword", false);	



		if (animator.GetBool ("InvisSpell") == true) {
			animator.SetBool ("InvisSpell", false);


			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null && Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true){

				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character>().player2Invis==false){

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
			
				}
			}
		}

		animator.SetBool ("TornadoSpell", false);

		animator.SetBool ("SummonTreant", false);

		if (animator.GetBool ("SummonShield") == true) {
			animator.SetBool ("SummonShield", false);
			
			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null 
			   && Camera.main.GetComponent<CampaignGameOrganizer>().nextTurnBool==true){ //player has selected enough blocks to take a turn


				Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();

				Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);
			}

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null 
			   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool==true
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character>().player2Invis == false){ //player has selected enough blocks to take a turn
					
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
					
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();
				}
	
		}

		animator.SetBool ("DefaultAtkP1", false);


	}

	public void playFireballSound(){

        checkForManager();
        soundManager.FireballSound ();
		//explosion sound will be attached to the fireball and played with collision
	}

	public void playSwordWhoosh(){
        checkForManager();
        soundManager.SwordSwish ();
	}

	public void playSwordHit(){
        checkForManager();
        soundManager.SwordHit ();
	}

	public void waterBeamChargeUp(){
        checkForManager();
        soundManager.WaterBeamCharge ();
	}

	public void endWaterBeamSoundLoop(){
        checkForManager();
        soundManager.WaterBeamShotEnd ();
	}

	public void footstep1(){
        checkForManager();
        soundManager.footStep1 ();
	}

	public void footstep2(){
        checkForManager();
        soundManager.footStep2 ();
	}

	public void invisSound(){
        checkForManager();
        soundManager.FinalBossAct2Death ();
	}

	public void lightningSound(){
        checkForManager();
        soundManager.LightningStrike ();
	}

	public void solarBeamSFX(){
        checkForManager();
        soundManager._SolarBeamSound();
	}

	public void swordHitEnemy(){
        checkForManager();
        soundManager.swordHitEnemy ();
	}

	public void MagicJumpSound(){
        checkForManager();

        soundManager.GolemDeathSound ();
	}
	public void EnchantRingSound(){

        checkForManager();

        soundManager.EnchantPowerUp ();
	}

	public void checkForManager(){

        if (soundManager == null)
        {

            soundManager = CameraMenu.persistantSoundManager.GetComponent<SoundManager>();

        }

    }
}