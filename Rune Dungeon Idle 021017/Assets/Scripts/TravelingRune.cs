﻿using UnityEngine;
using System.Collections;

public class TravelingRune : MonoBehaviour {

	public Sprite WaterSprite;
	public Sprite FireSprite;
	public Sprite EarthSprite;
	public Sprite WindSprite;

	public Vector3 endPosition;

	public int runeIndex;
	// Use this for initialization

	// Update is called once per frame

	public void setSprite(Sprite sentRuneSprite){

		this.GetComponent<SpriteRenderer> ().sprite = sentRuneSprite;

	}

	public void setEndPosition(Vector3 endPositionRecieved){

		endPosition = endPositionRecieved;
	}

	void Update () {
	

		this.transform.position = Vector2.Lerp(this.transform.position, endPosition ,Time.deltaTime*10);

		if (Mathf.Abs(this.transform.position.x - endPosition.x)<0.01f ) {

			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null){
			Camera.main.GetComponent<CampaignGameOrganizer>().colorOldBlocks(runeIndex, this.gameObject.GetComponent<SpriteRenderer>().sprite);
			}
			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
			//	Camera.main.GetComponent<TurnedBasedGameOrganizer>().colorOldBlocks(runeIndex, this.gameObject.GetComponent<SpriteRenderer>().sprite);
			}

			Destroy(this.gameObject);
		}
	}


}
