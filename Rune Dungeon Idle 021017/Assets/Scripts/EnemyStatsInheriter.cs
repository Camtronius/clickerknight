﻿using UnityEngine;
using System;
using System.Collections.Generic;
//using System.Collections;
using CodeStage.AntiCheat.Detectors;


public class EnemyStatsInheriter : MonoBehaviour {

	private static EnemyStatsInheriter instance = null;

	public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> playerExpMaxData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> prestigeData = new List<Dictionary<string, object>>(); //so we can read data for when the player prestige's back to lv5 or 10 etc.
    public List<Dictionary<string, object>> dungeonData = new List<Dictionary<string, object>>(); //so we can read data for when the player prestige's back to lv5 or 10 etc.



    public List<Dictionary<string, object>> dataInventory = new List<Dictionary<string, object>>();

    public List<Sprite> backgroundsList = new List<Sprite>();
	public List<Sprite> pathList = new List<Sprite>();
	public List<Sprite> foregroundList = new List<Sprite>();

	public List<Sprite> dirtgroundList = new List<Sprite>();

	public int[] enemyArray = new int[5]; //3 enemies (index 0-2), 1 boss (index 3)

	
	public static EnemyStatsInheriter Instance {
		get { return instance; }
	}

	public GameObject IdleTimeAway;

	public SpriteRenderer pathTexture;

	public SpriteRenderer scrollingDirtGroundTexture;
	public SpriteRenderer scrollingForemostDirtGroundTexture;

	public SpriteRenderer backgroundTexture;
	public SpriteRenderer foregroundTexture;
	public Sprite transparentTexture;

	public GameObject scrollingGround;
	public GameObject scrollingFrontMostGround;

	public int CurrentLevel; //int for type of enemy 1=skele 2=skelearcher 3=? etc.

	public int Enemy1Type; //int for type of enemy 1=skele 2=skelearcher 3=? etc.
	public double Enemy1Health;
	public double Enemy1Damage;
	public double Enemy1Rank; //Level for the enemy?
	public int Enemy1Int; //int
	public double Enemy1Gold; //int


	public int Enemy2Type; //int for type of enemy 1=skele 2=skelearcher 3=? etc.
	public int Enemy2Health;
	public int Enemy2Damage;
	public int Enemy2Rank; //Level for the enemy?
	public int Enemy2Int; //int


	public int Enemy3Type; //int for type of enemy 1=skele 2=skelearcher 3=? etc.
	public int Enemy3Health;
	public int Enemy3Damage;
	public int Enemy3Rank; //Level for the enemy?
	public int Enemy3Int; //int

	public bool dimensionalFX = false;

    public bool dungeonMode = false;
    public bool dungeonModeBonus = false;

    public bool eventMode = false;
    public bool noCheat = false; //so we can tell if there is an erro with timesync. Mainly used to reset event

    void Awake() {

		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);



		if (PlayerPrefs.GetInt ("CurrentLevel") == 0) {

			PlayerPrefs.SetInt ("CurrentLevel", 1);
		}

		CurrentLevel = PlayerPrefs.GetInt ("CurrentLevel"); //this gets the current level you shouod be on - not the lv you have compleyed

		loadLevel (CurrentLevel);

	//	print ("levels are being loaded, current LV is LV " + CurrentLevel);

	}

     void Start()
    {
        //this is for if the idletimeaway window will show

        Camera.main.GetComponent<MainMenu>().LevelCompleteObject.refreshEventCountDown();

        if (PlayerPrefs.GetInt("FoughtFirstBoss") == 0) //makes it so the player cannot receive bonuses if they are at the end of game
        {
            //used to check if its the players firs ttime playing. now it checks if the tutorial has been completed past the prestige point
            //PlayerPrefs.SetInt ("FirstPlay", 1);

        }
        else //if (PlayerPrefs.GetInt("CurrentLevel") < MainMenu.LVCAP - 1)
        {

            ////newcode
            //double d = TimeCheatingDetector.GetOnlineTime("pool.ntp.org"); //was scarybee.com pool.ntp.org
            //var dateTime = new DateTime(1900, 1, 1).AddMilliseconds(d);

            //print ("the date from the server is: " + dateTime);

            //print("the amount of millis from server is " + d);

            //print ("my time converted to UTC time is: " + DateTime.Now.ToUniversalTime ().ToString ());

            //TimeSpan OnlineDifference = DateTime.Now.ToUniversalTime().Subtract(dateTime);

            //print ("difference between online and local time in minutes is: " + OnlineDifference.TotalMinutes.ToString());
            //new code
            //if (Math.Abs(OnlineDifference.TotalMinutes) >0) //used to be less than 120......... < 120 // changed to >0 so we can test dungeons. 12/12/18 disabled this permanently because it only causes problems
            //{ //was 60

                // print("the level cap is: " + MainMenu.LVCAP.ToString());
                // print("the current level is: " + PlayerPrefs.GetInt("CurrentLevel").ToString());

                IdleTimeAway.SetActive(true); //show the daily idle because it hasnt been seen today
                noCheat = true;
            //}
            //else
            //{
                //do nutin
              //  noCheat = false;

            //}
        }

    }

    public void assignEnemyStats(){

        //reads the data and gives it back to us from 1-20 rows

        //did have data.coiunt = 0, but want to reload this every level now, POTIONS WILL HAVE TO BE THEIR OWN LIST NOW
        //Also need to make sure we have the data loaded for Dungeons
        //Also need to make sure we have the data loaded for Events ex: levels 5-15 loaded for events, make sure if player goes to lv 20 and wants to play the event, the data is read from the beginning of event
        //   or beginning of dungon. whichever one is smaller...

        int tempLevel = CurrentLevel; //we use templevel so currentlevel doesnt get changed and modify any other code that relies on CurrentLevel

        if (CurrentLevel > 1)
        {
            tempLevel = CurrentLevel - 1;
        }

            data = CSVReaderShort.Read("RuneIdleClicker", tempLevel, tempLevel + 11);//, //adding 11 so we can see the prestige data for double prestige (if they open the window through tutor.)

            loadPlayerExpMaxData();


        //   print("enemy stats running data");


        //the "0" used to be currentlevel-1, not sure why we had -1


	}

    public void loadPlayerExpMaxData()
    {
        //this is so we always know what the players maxEXP is regardless of what world they are on. The player will always be on the zero index of this maxExp, but his rank will change based on player prefs
        playerExpMaxData = CSVReaderShort.Read("RuneIdleClicker", PlayerPrefs.GetInt("Rank"), PlayerPrefs.GetInt("Rank")+3);//, //adding 11 so we can see the prestige data for double prestige 


    }

    public void loadPrestigeData(int startingPrestWorld)
    {
        prestigeData = CSVReaderShort.Read("RuneIdleClicker", startingPrestWorld, startingPrestWorld+7);//, //adding 6 so we can see the data for double prestige


    }

    public void loadDungeonData(int regDungeonLv, int extremeDungeonLv)
    {
        dungeonData = CSVReaderShort.Read("RuneIdleClicker", regDungeonLv, extremeDungeonLv + 1);//, //adding 6 so we can see the data for double prestige


    }

    public string getItemName(int ItemType){

        //the int corresponds to the type of item you are naming
        //	List<Dictionary<string,object>> data = CSVReader.Read ("RuneIdleClicker");
        dataInventory = CSVReader.Read("InventoryData");//, PlayerPrefs.GetInt("CurrentLevel"), PlayerPrefs.GetInt("CurrentLevel") + 35);

        //if chest int
        string armorType = "";
		string adj1 = "";
		string adj2 = "";

		int rand05 = UnityEngine.Random.Range (0, 5);

		int rand06 = UnityEngine.Random.Range (0, 6);

		int rand07 = UnityEngine.Random.Range (0, 7);

		int rand08 = UnityEngine.Random.Range (0, 8);

		int rand11 = UnityEngine.Random.Range (0, 8);

		int rand028 = UnityEngine.Random.Range (0, 28);

        int rand028again = UnityEngine.Random.Range(0, 28);


      //  print("The rand nums are: " + rand05.ToString () + "/" + rand06.ToString () + "/" + rand07.ToString () + "/" + rand08.ToString () + "/" + rand11.ToString () + "/" + rand028.ToString () + "/");


//		if (data == null) {
//			print ("the data for the name gen IS null");
//		} else {
//
//			print ("the data for the name gen IS NOT null");
//		}

		if (ItemType == 0) {

			armorType = (string)dataInventory[rand08] ["HEAD"];
			adj1 = (string)dataInventory[rand028] ["ADD1"];
			adj2 = (string)dataInventory[rand028again] ["ADD2"];

		}

		if (ItemType == 1) {

			armorType = (string)dataInventory[rand11] ["CHEST"];
			adj1 = (string)dataInventory[rand028] ["ADD1"];
			adj2 = (string)dataInventory[rand028again] ["ADD2"];

		}

		if (ItemType == 2) {
			//said argument out of range at randomrange 1-7 for ARM. code line 127
			armorType = (string)dataInventory[rand07] ["ARM"]; //Random.Range (1, 7)
			adj1 = (string)dataInventory[rand028] ["ADD1"]; //Random.Range (1, 28)
			adj2 = (string)dataInventory[rand028again] ["ADD2"]; //Random.Range (1, 28)

		}

		if (ItemType == 3) { 

			armorType = (string)dataInventory[rand06] ["LEGS"];
			adj1 = (string)dataInventory[rand028] ["ADD1"];
			adj2 = (string)dataInventory[rand028again] ["ADD2"];

		}

		if (ItemType == 4) { 

			armorType = (string)dataInventory[rand05] ["CAPE"];
			adj1 = (string)dataInventory[rand028] ["ADD1"];
			adj2 = (string)dataInventory[rand028again] ["ADD2"];

		}

		if (ItemType == 5) { 

			armorType = "Ring";
			adj1 = (string)dataInventory[rand028] ["ADD1"];
			adj2 = (string)dataInventory[rand028again] ["ADD2"];

		}

		string itemTitle = armorType + " of " + adj1 + " " + adj2;

		return itemTitle;
	}

	public void loadLevel(int Level){



		dimensionalFX = false; //just to make sure it doesnt keep...
		if (backgroundTexture == null) {
			backgroundTexture = GameObject.Find ("ScrollingBackground").GetComponent<SpriteRenderer>();
		}

		if (foregroundTexture == null) {
			foregroundTexture = GameObject.Find ("ScrollingForeground").GetComponent<SpriteRenderer>();
		}

		if (pathTexture == null) {
			pathTexture = GameObject.Find ("ScrollingPath").GetComponent<SpriteRenderer>();
		}

		if (scrollingDirtGroundTexture == null) {
			scrollingDirtGroundTexture = GameObject.Find ("Ground").GetComponent<SpriteRenderer>();
		}

		if (scrollingForemostDirtGroundTexture == null) {
			scrollingForemostDirtGroundTexture = GameObject.Find ("ForefrontGround").GetComponent<SpriteRenderer>();
		}

//		switch (Level) {
//
		CurrentLevel = Level; //this needs to be before the assign enemy stats method and before the enemy array code etc.

		assignEnemyStats ();

        //1 skele/2 warrior skele/ 3 slime/ 4 skele king/ 5skele mage/ 6 reptile
        //	print("the current lv is :" + CurrentLevel);

        if (Level == 1)
        {

            enemyArray[0] = (int)(double)data[0]["E1"]; //skele is 1
            enemyArray[1] = (int)(double)data[0]["E2"]; //WarriorSkele
            enemyArray[2] = (int)(double)data[0]["E3"]; //Slime

            Enemy1Health = (double)data[0]["HP"];
            Enemy1Damage = (double)data[0]["ENMYDMG"];
            Enemy1Rank = (double)data[0]["RANK"];
            Enemy1Gold = (double)data[0]["GOLD"];

        }
        else //if level = 2 then the data will be loaded for level 1 to keep the prices correct etc. This is because we move indexes in the list.
        {
            enemyArray[0] = (int)(double)data[1]["E1"]; //skele is 1
            enemyArray[1] = (int)(double)data[1]["E2"]; //WarriorSkele
            enemyArray[2] = (int)(double)data[1]["E3"]; //Slime

            Enemy1Health = (double)data[1]["HP"];
            Enemy1Damage = (double)data[1]["ENMYDMG"];
            Enemy1Rank = (double)data[1]["RANK"];
            Enemy1Gold = (double)data[1]["GOLD"];
        }

        if (Level > 1) {

            enemyArray[3] = (int)(double)data[1]["BOSSM"]; //we need to go a level ahread because after level 1 the script moves back one level to accodomdate prices and information for moving back one level

            }

        if (dungeonMode == true) //if its a dungeon and if the dungeons have been unlocked (level 10 or greater)... this will prevent the player from getting the level 1 dungeon when they hit lv10
        {


            enemyArray[0] = 33;//33 is the code for the dungeon egg at this point!

            int[] dungeonArray = PlayerPrefsX.GetIntArray("DungeonArray"); //get the int array DungeonArray. Dungeon Array is made in Idle Time Away Daily Login Checker
            int[] bonusDungeonArray = PlayerPrefsX.GetIntArray("BonusDungeonArray"); //get the int array DungeonArray. Dungeon Array is made in Idle Time Away Daily Login Checker



            if (dungeonArray.Length == 0)//if its null, then make your own dungeon array and set it equal to something. This happens on first time entering a dungon
            {
              //  print("dungeon array is null, making a new array now");

                int bossInt1 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
                int bossInt2 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
                int bossInt3 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)

                int currentLevelInt = PlayerPrefs.GetInt("CurrentLevel");

                //randomly selects a boss enemy, and then
                dungeonArray = new int[] {bossInt1, bossInt2, bossInt3, currentLevelInt };
                PlayerPrefsX.SetIntArray("DungeonArray", dungeonArray); //Setting it as the new array so the dungeon is the same every time

            }

                if (bonusDungeonArray.Length == 0)//if its null, then make your own dungeon array and set it equal to something. This happens on first time entering a dungon
                {
                 //   print("bonus dungeon array is null, making a new array now");

                    int bossInt1 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
                    int bossInt2 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
                    int bossInt3 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)

                    int currentLevelInt = PlayerPrefs.GetInt("CurrentLevel");

                    //randomly selects a boss enemy, and then
                    bonusDungeonArray = new int[] {bossInt1, bossInt2, bossInt3, currentLevelInt+2};//add 2 to the current level int. This makes the player have to play more to unlock the next pet. 
                    PlayerPrefsX.SetIntArray("BonusDungeonArray", bonusDungeonArray); //Setting it as the new array so the dungeon is the same every time

                }

            //assign new enemy stats so it doesnt just keep replacing the old stats with the current lev

            //this will load the data for the special dungeons, for example, if the dungeon is created and starts on level 5 but we are on level 100, it will still get data for level 5
            loadDungeonData(dungeonArray[3], bonusDungeonArray[3]);

            if (dungeonModeBonus == false) //if its a normal dungeon
            {
                Enemy1Health = (double)dungeonData[0]["HP"]; //because 3 is the current lv. *** this used to be dungeonArray[3]
                Enemy1Damage = (double)dungeonData[0]["ENMYDMG"];
                Enemy1Rank = (double)dungeonData[0]["RANK"];
                Enemy1Gold = (double)dungeonData[0]["GOLD"];

                enemyArray[1] = dungeonArray[0]; //1st dungeon boss
                enemyArray[2] = dungeonArray[1]; //2nd dungeon boss
                enemyArray[3] = dungeonArray[2]; //3rd dungeon boss... // was (int)(double)data[dungeonArray[3]]["BOSSM"];

                //to make it look like a dungeon
                pathTexture.sprite = pathList[5]; //this is the path the player runs on
                backgroundTexture.sprite = backgroundsList[0];  //this is the sky bkg
                foregroundTexture.sprite = foregroundList[8]; //this is the trees/bushes/fence etc
                scrollingDirtGroundTexture.sprite = dirtgroundList[5]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                scrollingForemostDirtGroundTexture.sprite = dirtgroundList[5]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                applyLevelSettings(3);//this changes the sorting orders and whether or not the dirt ground is upper or normal

            }

            if (dungeonModeBonus==true) // if its a bonus dungeon
            {
                Enemy1Health = (double)dungeonData[2]["HP"];
                Enemy1Damage = (double)dungeonData[2]["ENMYDMG"];
                Enemy1Rank = (double)dungeonData[2]["RANK"]; //so bonus dungeons dont give crazy exp and gold
                Enemy1Gold = (double)dungeonData[2]["GOLD"];

                enemyArray[1] = bonusDungeonArray[0]; //1st dungeon boss
                enemyArray[2] = bonusDungeonArray[1]; //2nd dungeon boss
                enemyArray[3] = bonusDungeonArray[2]; //3rd dungeon boss


                //to make it look like a dungeon
                pathTexture.sprite = pathList[5]; //this is the path the player runs on
                backgroundTexture.sprite = backgroundsList[0];  //this is the sky bkg
                foregroundTexture.sprite = foregroundList[10]; //this is the trees/bushes/fence etc
                scrollingDirtGroundTexture.sprite = dirtgroundList[5]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                scrollingForemostDirtGroundTexture.sprite = dirtgroundList[6]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                applyLevelSettings(1);//this changes the sorting orders and whether or not the dirt ground is upper or normal

            }

            //end stats injection

            //5 path, 0 bkg, 8frg, 5 grnd, 5grnd makes a really cool desert look!


        }//end of dungeon code

        //EVENT MODE
        if (eventMode == true) 
        {
            Camera.main.GetComponent<MainMenu>().LevelCompleteObject.refreshEventCountDown();//this is so the array of check level complete events is populated
            enemyArray[0] = 33;//33 is the code for the (egg) EVENT CHEST at this point!

            CheckLevelsComplete levelsCompleteObj = Camera.main.GetComponent<MainMenu>().LevelCompleteObject;

            int currentEventLvIndex = levelsCompleteObj.eventLevels[PlayerPrefs.GetInt("EventLevel")];
            //an array of 10 ints. It is generated from the players current world at the start of the event. Enemies will have x2 HP and player needs to defeat 10 of them
            //when player completes and event the eventLevel int will +1. UNless its 9 which means the event is completed


            loadDungeonData(currentEventLvIndex, currentEventLvIndex + 2);//loads data for the dungeon level you are on and the next 2 levels just in case...
            //continuted from previous line....this is reloaded each time a new event level is played, so there shouldnt be much of an issue...

                Enemy1Health = 2*(double)dungeonData[0]["HP"]; //used to be 2x and dungdata[1]
                Enemy1Damage = 2*(double)dungeonData[0]["ENMYDMG"]; //used to be dungdata[1]
                Enemy1Rank = (double)dungeonData[0]["RANK"]/20f; //was 2
                Enemy1Gold = (double)dungeonData[0]["GOLD"]/20f; //was 50

                //enemies in Event Dungeon will be assigned at random

                //to make it look like a dungeon
                pathTexture.sprite = pathList[7]; //this is the path the player runs on
                backgroundTexture.sprite = backgroundsList[5];  //this is the sky bkg
                foregroundTexture.sprite = foregroundList[14]; //this is the trees/bushes/fence etc
                scrollingDirtGroundTexture.sprite = dirtgroundList[9]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                scrollingForemostDirtGroundTexture.sprite = dirtgroundList[9]; //this is the bush that can cover the dirt and the path and the gems also appear in it
                applyLevelSettings(3);//this changes the sorting orders and whether or not the dirt ground is upper or normal

            

        }
        //END EVENTMODE

        //default layer orders are going to be this for the 1st level, and maybe others too
        if (dungeonMode == false && eventMode==false)
        {

            if (Level == 1)
            {
                pathTexture.sprite = pathList[(int)(double)data[0]["PATH"]]; //this is the path the player runs on
                backgroundTexture.sprite = backgroundsList[(int)(double)data[0]["BKG"]];  //this is the sky bkg
                foregroundTexture.sprite = foregroundList[(int)(double)data[0]["FRG"]]; //this is the trees/bushes/fence etc

                scrollingDirtGroundTexture.sprite = dirtgroundList[(int)(double)data[0]["GRND"]]; //this is the bush that can cover the dirt and the path and the gems also appear in it

                scrollingForemostDirtGroundTexture.sprite = dirtgroundList[(int)(double)data[0]["GRND"]]; //this is the bush that can cover the dirt and the path and the gems also appear in it

                applyLevelSettings((int)(double)data[0]["STNG"]);//this changes the sorting orders and whether or not the dirt ground is upper or normal

            }
            else //if level = 2 then the data will be loaded for level 1 to keep the prices correct etc. This is because we move indexes in the list.
            {
                pathTexture.sprite = pathList[(int)(double)data[1]["PATH"]]; //this is the path the player runs on
                backgroundTexture.sprite = backgroundsList[(int)(double)data[1]["BKG"]];  //this is the sky bkg
                foregroundTexture.sprite = foregroundList[(int)(double)data[1]["FRG"]]; //this is the trees/bushes/fence etc

                scrollingDirtGroundTexture.sprite = dirtgroundList[(int)(double)data[1]["GRND"]]; //this is the bush that can cover the dirt and the path and the gems also appear in it

                scrollingForemostDirtGroundTexture.sprite = dirtgroundList[(int)(double)data[1]["GRND"]]; //this is the bush that can cover the dirt and the path and the gems also appear in it

                applyLevelSettings((int)(double)data[1]["STNG"]);//this changes the sorting orders and whether or not the dirt ground is upper or normal
            }



        }
		//applyLevelSettings (1);//this changes the sorting orders and whether or not the dirt ground is upper or normal

	}

	public void applyLevelSettings(int levelSetting){


		switch (levelSetting) { //gets the array that is prefilled with 3 enemy types per level and one boss which is index 3


		case(0): //for the first grassy level

			//level setting 1
			//Path = -39
			//ground = -49
			//foreground = -43
			//bkg = -50

			//ForefrontGround = off
			//ground = on

			pathTexture.sortingOrder = -39;
			scrollingDirtGroundTexture.sortingOrder = -49;
			foregroundTexture.sortingOrder = -43;
			backgroundTexture.sortingOrder = -50;

			//scrollingFrontMostGround.SetActive (false);
			scrollingForemostDirtGroundTexture.sprite = transparentTexture;

		//	scrollingGround.SetActive (true);
		//	scrollingDirtGroundTexture.sprite = transparentTexture;
			break;

		case(1):
			
			//level setting 2
			//for Outside Castle Wall
			//Path = -43*
			//ground = -49
			//foreground = -39*
			//bkg = -50

			//ForefrontGround = on* //this will need to offset texture in campaign level enemy spawner
			//ground = off

			pathTexture.sortingOrder = -43;
			scrollingDirtGroundTexture.sortingOrder = -49;
			foregroundTexture.sortingOrder = -39;
			backgroundTexture.sortingOrder = -50;

		//	scrollingFrontMostGround.SetActive(true);
		//	scrollingForemostDirtGroundTexture.sprite = transparentTexture;

		//	scrollingGround.SetActive(false);
			scrollingDirtGroundTexture.sprite = transparentTexture;


			break;

		case(2):
			
			//level setting 2
			//for tower
			//Path = -43*
			//ground = -49
			//foreground = -39*
			//bkg = -50

			//ForefrontGround = on* //this will need to offset texture in campaign level enemy spawner
			//ground = off

			pathTexture.sortingOrder = -39;
			scrollingDirtGroundTexture.sortingOrder = -49;
			foregroundTexture.sortingOrder = -43;
			backgroundTexture.sortingOrder = -50;

		//	scrollingFrontMostGround.SetActive(true);
		//	scrollingForemostDirtGroundTexture.sprite = transparentTexture;

		//	scrollingGround.SetActive(false);
			scrollingDirtGroundTexture.sprite = transparentTexture;


			break;

		case(3):


			//level setting 2
			//for tower
			//Path = -43*
			//ground = -49
			//foreground = -39*
			//bkg = -50

			//ForefrontGround = on* //this will need to offset texture in campaign level enemy spawner
			//ground = off

			pathTexture.sortingOrder = -43;
			scrollingDirtGroundTexture.sortingOrder = -49;
			foregroundTexture.sortingOrder = -39;
			backgroundTexture.sortingOrder = -50;

			//	scrollingFrontMostGround.SetActive(true);
				scrollingForemostDirtGroundTexture.sprite = transparentTexture;

			//	scrollingGround.SetActive(false);
		//	scrollingDirtGroundTexture.sprite = transparentTexture;

			break;

	
		}


	}

}
