﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.Analytics.Experimental;


public class ShopCard : MonoBehaviour {

	// Use this for initialization
	public ObscuredInt gemAmount;
	public Button purchaseButton;
	public TextMeshProUGUI RewardAmt;

	public ObscuredFloat ascendPts;
	//expGain will be dependent on the players max exp for that level;
	public ObscuredInt talentPts;
	public ObscuredInt HPboost;
	public ObscuredInt statGain;

	public ObscuredInt allCrysBonus;

	public ObscuredInt redCrysBonus;
	public ObscuredInt bluCrysBonus;
	public ObscuredInt yelCrysBonus;

	public ObscuredInt goldBonusMultiplier;

	public ShopCard arcanePkg;
	public ShopCard furyPkg;
	public ShopCard chemistPkg;

	public ObscuredInt gemPrice;
	public TextMeshProUGUI gemPriceText;
	public IngredientsMaster ingredientsScript;
	public PetManager petManager;

	public SoundManager soundManager;
	public GameObject fireworks;

    public GameObject promoPurchaseWindow;
    public GameObject promoFireWorks;
    public GameObject buttonForStarterPromo;
    public GameObject buttonForFuryPromo;
    public GameObject buttonForArcanePromo;
    public GameObject buttonForComboPromo;
    public GameObject buttonForEventPromo;


    public GameObject starterOfferIcon;
    public GameObject furyOfferIcon;
    public GameObject arcaneOfferIcon;
    public GameObject comboOfferIcon;
    public GameObject EventOfferIcon;

    //gold bonus to be determined based on currently

    public bool packageOrGem = false;

	public enum CardType {p1Ascend,InstantLvUp,p1TalentPt,
							p2TalentPt,HpBoostSmall,HpBoostLarge,
							StatGainSmall,StatGainLarge,AllCrysBonusSmall,
							AllCrysBonusLarge,RedCrysBonus,BlueCrysBonus,
							YelCrysBonus,GoldBonusSmall,GoldBonusMed,
							GoldBonusLarge,ResetEvent,StarterPack,ArcanePack,
							FuryPack,ChemistPack,ComboPack,EventPack, UtilityPack,
                            FistOfGems, PileOfGems, SackOfGems,
							CrateOfGems, ChestOfGems, TonOfGems};

	public CardType _CardType;

	void OnEnable(){

		if (soundManager == null) {

			soundManager = Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ();
		}

		runOnStart (); //this is so if a package is restored the objects will be initialized. Really only needed for packages

        //this is so the HP boost scales with prestige lebel

    }

	public void runOnStart (){ //this is so the script is initialized when its called from another script. Needed for packaged being confirmed by the store

		if (ingredientsScript == null) {
			ingredientsScript = Camera.main.GetComponent<MainMenu> ()._potionMasterScript.masterIngredientsHolder;
		}
		checkOnEnabled (); //so it can be called from another script;

	}

	public void checkOnEnabled(){
		
		purchaseButton = this.gameObject.GetComponentInChildren<Button> (true);
		RewardAmt = this.gameObject.GetComponentInChildren<TextMeshProUGUI> (true);

		if (packageOrGem == false) {
			gemPriceText.text = gemPrice.ToString ();
		}
		checkPurchase ();//checks if this object should be purchasable
	}

	public void checkPurchase(){

		if (PlayerPrefs.GetInt (this.gameObject.name) == 1) { //1 is purchased, 0 is unpurchased for the 2 hr window
		
			hasPurchased ();

		} else {
			assignValues (); //wont make a diff if this is a purchase
			purchaseButton = this.gameObject.GetComponentInChildren<Button> (true);
			purchaseButton.gameObject.SetActive (true);

		}
	}

	public void hasPurchased(){

		//print (this.gameObject.name + " is being tested for a purchase!");

		if (purchaseButton == null) {
			purchaseButton = this.gameObject.GetComponentInChildren<Button> (true);
		}
		if (RewardAmt == null) {
			RewardAmt = this.gameObject.GetComponentInChildren<TextMeshProUGUI> (true);
		}
		purchaseButton.gameObject.SetActive (false);
		RewardAmt.text = "Purchased!";
		PlayerPrefs.SetInt (this.gameObject.name, 1);

        //to restore purchases to accounts that have already purchased:

        //for starter
        if(this.gameObject.name == "StarterPackUnlock" && PlayerPrefs.GetInt(this.gameObject.name) == 1)
        {
           // print("restoring starter pack");

            unlockPetsAndEquip(0); //equips the rabbit by default if he is the only one bought

        }

        //for arcane
        if (this.gameObject.name == "ArcanePackUnlock" && PlayerPrefs.GetInt(this.gameObject.name) == 1)
        {
           // print("restoring arcane pack");

            unlockPetsAndEquip(3); //equips the rabbit by default if he is the only one bought

        }

        //for fury
        if (this.gameObject.name == "FuryPackUnlock" && PlayerPrefs.GetInt(this.gameObject.name) == 1)
        {
           // print("restoring fury pack");

            unlockPetsAndEquip(2); //equips the rabbit by default if he is the only one bought

        }

        if (this.gameObject.name == "ChemistPackUnlock" && PlayerPrefs.GetInt(this.gameObject.name) == 1)
        {
           // print("restoring chemist pack");

            unlockPetsAndEquip(1); //equips the penguin by default if he is the only one bought

        }

        if (this.gameObject.name == "EventPackUnlock" && PlayerPrefs.GetInt(this.gameObject.name) == 1)
        {
            // print("restoring chemist pack");

            unlockPetsAndEquip(10); //equips the penguin by default if he is the only one bought
            unlockPetsAndEquip(11); //equips the penguin by default if he is the only one bought

        }


    }

    public int getHPBoost()
    {

        int returnedHP = (HPboost * (PlayerPrefs.GetInt("PrestigeLv") + 1));

        return returnedHP;


    }

    public double getGoldBoost()
    {

        double goldBoost = 0;

        goldBoost = (double)Camera.main.GetComponent<MainMenu>().enemyStatsObj.data[2]["LVCOST"] * goldBonusMultiplier;

        return goldBoost;
    }

    public void assignValues(){

		//for gold and exp
		if (packageOrGem == false) {
			
			ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().playerObjHolder;
			EnemyStatsInheriter statsObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().EnemyStatsObj;
		
		//


		switch (_CardType) {

			case CardType.p1Ascend://
				
			RewardAmt.text = "+" + ascendPts.ToString() + " point";
		break;

		case CardType.InstantLvUp://
			RewardAmt.text = "+1 Lv";

		break;

		case CardType.p1TalentPt://
			RewardAmt.text = "+" + talentPts.ToString() + " point";

		break;

		case CardType.p2TalentPt://
			RewardAmt.text = "+" + talentPts.ToString() + " points";

		break;

		case CardType.HpBoostSmall://
			RewardAmt.text = "+" + getHPBoost().ToString() + " HP";

		break;

		case CardType.HpBoostLarge://
			RewardAmt.text = "+" + getHPBoost().ToString() + " HP";

		break;

		case CardType.StatGainSmall://
			RewardAmt.text = "+" + statGain.ToString() + " points";

		break;

		case CardType.StatGainLarge://
			RewardAmt.text = "+" + statGain.ToString() + " points";

		break;

		case CardType.AllCrysBonusSmall://
			RewardAmt.text = "x" + allCrysBonus.ToString() + " all";

		break;

		case CardType.AllCrysBonusLarge://
			RewardAmt.text = "x" + allCrysBonus.ToString() + " all";

		break;

		case CardType.RedCrysBonus://
			RewardAmt.text = "x" + redCrysBonus.ToString() + " red";

		break;

		case CardType.BlueCrysBonus://
			RewardAmt.text = "x" + bluCrysBonus.ToString() + " blue";

		break;

		case CardType.YelCrysBonus://
			RewardAmt.text = "x" + yelCrysBonus.ToString() + " yel";

		break;

		case CardType.GoldBonusSmall://

			RewardAmt.text =	LargeNumConverter.setDmg(getGoldBoost());

		break;

		case CardType.GoldBonusMed://

			RewardAmt.text =	LargeNumConverter.setDmg(getGoldBoost());
		break;

		case CardType.GoldBonusLarge://

			RewardAmt.text =	LargeNumConverter.setDmg(getGoldBoost());
		break;

		//packages will be handled seperated once i figure out how to do that...
		//same with gem packs
			}

		}

	}

	public bool checkEnoughGreenGems(int itemGemPrice){

		if (ingredientsScript.totalGreenGems >= itemGemPrice) {

			ingredientsScript.totalGreenGems -= itemGemPrice;

			ingredientsScript.setNewPrefs ();

			return true;
		} else {

			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("You need more Green Gems!");


			return false;
		}

	}


	public void purchaseAscendPt(){

		if(checkEnoughGreenGems(gemPrice)==true){
			
			for(int i=0; i<1/Camera.main.GetComponent<MainMenu> ().prestPointsAmt.prestCountAmt; i++){
				Camera.main.GetComponent<MainMenu> ().prestPointsAmt.updatePrestigeCounter ();//this will add 1 tick of prest pts
			}

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}
	}

	public void purchaseLvUp(){

		if(checkEnoughGreenGems(gemPrice)==true){
			
			//purchase 1 talent pt
			ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().playerObjHolder;

			playerObj.playerExpNew = playerObj.playerExpMax+1; //this will auto lv up the hero
			//puchase 2 talent pt

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}
	}

	public void purchaseTalentPt(){

		if(checkEnoughGreenGems(gemPrice)==true){
			
			//purchase 1 talent pt
			if (_CardType == CardType.p1TalentPt) {
				PlayerPrefs.SetInt ("ResearchPoints", PlayerPrefs.GetInt ("ResearchPoints") + 1);

			}
			//puchase 2 talent pt
			if (_CardType == CardType.p2TalentPt) {
				PlayerPrefs.SetInt ("ResearchPoints", PlayerPrefs.GetInt ("ResearchPoints") + 2);

			}
			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		
		}
	}

	public void purchaseHpBoost(){ //needs to be added to the players HP to work...

		//******THIS BOOST IS CLEARED WITH PRESTIGE ON PURPOSE IN THE MAIN MENU SCRIPT setPrestige()

		if(checkEnoughGreenGems(gemPrice)==true){

			if (_CardType == CardType.HpBoostSmall) {
				
				PlayerPrefs.SetFloat ("TempHPBoost", PlayerPrefs.GetFloat ("TempHPBoost")+getHPBoost()); //adds 200 base HP permanently I tihnk....
				}

			if (_CardType == CardType.HpBoostLarge) {

				PlayerPrefs.SetFloat ("TempHPBoost", PlayerPrefs.GetFloat ("TempHPBoost")+ getHPBoost());
			}
			//purchase HP boost large

			ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().playerObjHolder;

			playerObj.quickHPUpdate ();//so the HP is updated automatically

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});

		}
	}

	public void purchaseStatsBoost(){

		//purchase stat small +3

		if(checkEnoughGreenGems(gemPrice)==true){
			
			if (_CardType == CardType.StatGainSmall) {
				
			//	PlayerPrefs.SetFloat ("StatsBoost", PlayerPrefs.GetFloat ("StatsBoost") + statGain); //adds for prestige purposes

				PlayerPrefs.SetFloat ("SkillPoints", PlayerPrefs.GetFloat ("SkillPoints") + statGain);

			}
			//purchase stat large +6
			if (_CardType == CardType.StatGainLarge) {
				
			//	PlayerPrefs.SetFloat ("StatsBoost", PlayerPrefs.GetFloat ("StatsBoost") + statGain); //adds for prestige purposes

				PlayerPrefs.SetFloat ("SkillPoints", PlayerPrefs.GetFloat ("SkillPoints") + statGain);


			}

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text
		}
	}

	public void purchaseMultiCrys(){

		if(checkEnoughGreenGems(gemPrice)==true){
				
			//purchase multi crys med
			if (_CardType == CardType.AllCrysBonusSmall) {
				
			ingredientsScript.totalRedGem +=allCrysBonus;

				ingredientsScript.totalBlueGem += allCrysBonus;

				ingredientsScript.totalYellowGem += allCrysBonus;

			}

			//purchase mutli crys large

			if (_CardType == CardType.AllCrysBonusLarge) {

				ingredientsScript.totalRedGem +=allCrysBonus;

				ingredientsScript.totalBlueGem += allCrysBonus;

				ingredientsScript.totalYellowGem += allCrysBonus;

			}
			ingredientsScript.setNewPrefs ();

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}
	}

	public void purchaseRedCrys(){

		if(checkEnoughGreenGems(gemPrice)==true){
			
			ingredientsScript.totalRedGem +=redCrysBonus;
			ingredientsScript.setNewPrefs ();

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		
		}
	}

	public void purchaseBlueCrys(){
		
		if(checkEnoughGreenGems(gemPrice)==true){
			
			ingredientsScript.totalBlueGem +=bluCrysBonus;
			ingredientsScript.setNewPrefs ();

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}

	}

	public void purchaseYelCrys(){
		
		if(checkEnoughGreenGems(gemPrice)==true){
			
			ingredientsScript.totalYellowGem +=yelCrysBonus;
			ingredientsScript.setNewPrefs ();

			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			soundManager.purchaseWithGems ();//plays cash register reward sound

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}
	}

    public void purchaseEventReset()
    {

        if (checkEnoughGreenGems(gemPrice) == true)
        {

            //ingredientsScript.totalYellowGem += yelCrysBonus;
            //ingredientsScript.setNewPrefs();

            //hasPurchased();//this removes the button (plays animation) and changes the AMT text

            purchaseButton.gameObject.SetActive(false);
            RewardAmt.text = "Purchased!";

            PlayerPrefs.SetInt("EventComplete", 1); //this is so we can reset the next event after 24hrs
            Camera.main.GetComponent<MainMenu>().IdleTimeAway.GetComponent<IdleTimeAway>().checkEventRefresh();

            soundManager.purchaseWithGems();//plays cash register reward sound

            AnalyticsEvent.Custom("ShopCards", new Dictionary<string, object> {

                {this.gameObject.name,1},
            });
        }
    }

    public void purchaseGoldBonus(){

		if(checkEnoughGreenGems(gemPrice)==true){
			
			EnemyStatsInheriter statsObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().EnemyStatsObj;
			ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<TwoHourShopRotation> ().playerObjHolder;

			//gold bonus small
			if (_CardType == CardType.GoldBonusSmall) {
				playerObj.playerGold += getGoldBoost(); //this is also set in SetupGoldCard(). If you change it here, change it there too.

			}

			//gold bonus med

			if (_CardType == CardType.GoldBonusMed) {
				playerObj.playerGold += getGoldBoost(); //this is also set in SetupGoldCard(). If you change it here, change it there too.

			}

			//gold bonus large

			if (_CardType == CardType.GoldBonusLarge) {
				playerObj.playerGold += getGoldBoost(); //this is also set in SetupGoldCard(). If you change it here, change it there too.

			}


			hasPurchased ();//this removes the button (plays animation) and changes the AMT text

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});

			soundManager.purchaseWithGems ();//plays cash register reward sound
		}

	}


	public void purchaseStarterPack(){


		ObscuredPrefs.SetInt ("StarterPack", 1);
        ObscuredPrefs.SetInt("StarterPackPromo", 1);
        PlayerPrefs.SetFloat("LimitedOfferHours", 0); //resets this value so we are starting from another 72hr  value for promotions


        runOnStart (); //this is so if a package is restored the objects will be initialized. Really only needed for packages


		if (PlayerPrefs.GetInt (this.gameObject.name) == 0) {
			

			ingredientsScript.totalRedGem += allCrysBonus; 
			ingredientsScript.totalBlueGem += allCrysBonus;
			ingredientsScript.totalYellowGem += allCrysBonus;

			ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

			ingredientsScript.setNewPrefs ();

			//unlock fox
			petManager.disableOthers(); //in case other pets were on at this time

			//PlayerPrefs.SetInt ("FoxUnlocked",1);
			//PlayerPrefs.SetInt ("FoxOn", 1);

			petManager.checkPetOn ();

            if (promoPurchaseWindow.activeSelf == true)
            {

                promoFireWorks.SetActive(true);
                buttonForStarterPromo.SetActive(false);
                starterOfferIcon.SetActive(false);
            }
            else
            {
                fireworks.SetActive(true);

            }
            promoPurchaseWindow.GetComponentInParent<LimitedTimeOffers>().checkBought(); //this removes icons if its bought

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});

		}

            hasPurchased ();//this removes the button (plays animation) and changes the AMT text

            unlockPetsAndEquip(0); //equips the rabbit by default if he is the only one bought



    }

    public void purchaseFuryPack(){

		ObscuredPrefs.SetInt ("FuryPack", 1);
        ObscuredPrefs.GetInt("FuryPackPromo",1);
        PlayerPrefs.SetFloat("LimitedOfferHours", 0); //resets this value so we are starting from another 72hr  value for promotions



        runOnStart(); //this is so if a package is restored the objects will be initialized. Really only needed for packages


		if (PlayerPrefs.GetInt (this.gameObject.name) == 0) {
			
			ingredientsScript.totalRedGem += allCrysBonus; 
			ingredientsScript.totalBlueGem += allCrysBonus;
			ingredientsScript.totalYellowGem += allCrysBonus;

			ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

			ingredientsScript.setNewPrefs ();


		//unlock cat
			petManager.disableOthers(); //in case other pets were on at this time

			//PlayerPrefs.SetInt ("CatUnlocked",1);
			//PlayerPrefs.SetInt ("CatOn", 1);

			petManager.checkPetOn ();

            if (promoPurchaseWindow.activeSelf == true)
            {

                promoFireWorks.SetActive(true);
                buttonForFuryPromo.SetActive(false);
                furyOfferIcon.SetActive(false);
            }
            else
            {
                fireworks.SetActive(true);

            }

            promoPurchaseWindow.GetComponentInParent<LimitedTimeOffers>().checkBought(); //this removes icons if its bought


            AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}

        
        hasPurchased ();//this removes the button (plays animation) and changes the AMT text

        unlockPetsAndEquip(2); //equips the rabbit by default if he is the only one bought


    }

    public void purchaseArcanePack(){

		ObscuredPrefs.SetInt ("ArcanePack", 1);
        ObscuredPrefs.GetInt("ArcanePackPromo", 1);
        PlayerPrefs.SetFloat("LimitedOfferHours", 0); //resets this value so we are starting from another 72hr  value for promotions

        runOnStart (); //this is so if a package is restored the objects will be initialized. Really only needed for packages


		if (PlayerPrefs.GetInt (this.gameObject.name) == 0) {
			
			ingredientsScript.totalRedGem += allCrysBonus; 
			ingredientsScript.totalBlueGem += allCrysBonus;
			ingredientsScript.totalYellowGem += allCrysBonus;

			ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

			ingredientsScript.setNewPrefs ();

			//unlock rabbit
			petManager.disableOthers(); //in case other pets were on at this time

			//PlayerPrefs.SetInt ("RabbitUnlocked",1);
			//PlayerPrefs.SetInt ("RabbitOn",1);

			petManager.checkPetOn ();

            if (promoPurchaseWindow.activeSelf == true)
            {
                promoFireWorks.SetActive(true);
                buttonForArcanePromo.SetActive(false);
                arcaneOfferIcon.SetActive(false);
            }
            else
            {
                fireworks.SetActive(true);
            }

            promoPurchaseWindow.GetComponentInParent<LimitedTimeOffers>().checkBought(); //this removes icons if its bought


            AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}

        hasPurchased ();//this removes the button (plays animation) and changes the AMT text

        unlockPetsAndEquip(3); //equips the rabbit by default if he is the only one bought
        
    }

    public void purchaseChemistPack(){

			
		ObscuredPrefs.SetInt ("ChemistPack", 1);

		runOnStart (); //this is so if a package is restored the objects will be initialized. Really only needed for packages



		if(PlayerPrefs.GetInt (this.gameObject.name)==0){
			
			ingredientsScript.totalRedGem +=allCrysBonus; 
			ingredientsScript.totalBlueGem += allCrysBonus;
			ingredientsScript.totalYellowGem += allCrysBonus;

			ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

			ingredientsScript.setNewPrefs ();

			//unlock chemist
			petManager.disableOthers(); //in case other pets were on at this time

			//PlayerPrefs.SetInt ("PenguinUnlocked",1);
			//PlayerPrefs.SetInt ("PenguinOn", 1);

			petManager.checkPetOn ();

			fireworks.SetActive (true);

			AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

				{this.gameObject.name,1},
			});
		}

		hasPurchased ();//this removes the button (plays animation) and changes the AMT text

        unlockPetsAndEquip(1); //equips the penguin by default if he is the only one bought

	}

	public void purchaseComboPack(){

        //will have to assign seperate GOs for this
        ObscuredPrefs.SetInt("ComboPackPromo", 1);


        chemistPkg.purchaseChemistPack();
		arcanePkg.purchaseArcanePack();
		furyPkg.purchaseFuryPack();

		//PlayerPrefs.SetInt ("Glasses", 1); //dont need this anymore b/c there are no glasses in the game
		
		//petManager.checkGlasses ();//this will equip glasses on the player

		hasPurchased ();//this removes the button (plays animation) and changes the AMT text

        if (promoPurchaseWindow.activeSelf == true)
        {

            promoFireWorks.SetActive(true);
            buttonForComboPromo.SetActive(false);
            comboOfferIcon.SetActive(false);
        }

        promoPurchaseWindow.GetComponentInParent<LimitedTimeOffers>().checkBought(); //this removes icons if its bought


        AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

			{this.gameObject.name,1},
		});

	}

    public void purchaseEventPack()
    {


        ObscuredPrefs.SetInt("EventPack", 1);

        runOnStart(); //this is so if a package is restored the objects will be initialized. Really only needed for packages


        if (PlayerPrefs.GetInt(this.gameObject.name) == 0)
        {

            ingredientsScript.totalRedGem += allCrysBonus;
            ingredientsScript.totalBlueGem += allCrysBonus;
            ingredientsScript.totalYellowGem += allCrysBonus;

            ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

            ingredientsScript.setNewPrefs();

            //unlock fox
            petManager.disableOthers(); //this actuallyu doesnt do anything anymore

            //PlayerPrefs.SetInt ("FoxUnlocked",1);
            //PlayerPrefs.SetInt ("FoxOn", 1);

            petManager.checkPetOn(); //this actuallyu doesnt do anything anymore

            if (promoPurchaseWindow.activeSelf == true)
            {

                promoFireWorks.SetActive(true);
                buttonForEventPromo.SetActive(false);
                EventOfferIcon.SetActive(false);
            }
            else
            {
                fireworks.SetActive(true);

            }
            promoPurchaseWindow.GetComponentInParent<LimitedTimeOffers>().checkBought(); //this removes icons if its bought

            AnalyticsEvent.Custom("ShopCards", new Dictionary<string, object> {

                {this.gameObject.name,1},
            });

        }

        hasPurchased();//this removes the button (plays animation) and changes the AMT text

        unlockPetsAndEquip(10); //equips the rabbit by default if he is the only one bought
        unlockPetsAndEquip(11); //equips the rabbit by default if he is the only one bought

    }

    public void purchaseUtilityPack()
    {

        ObscuredPrefs.SetInt("UtilityPack", 1);

        runOnStart(); //this is so if a package is restored the objects will be initialized. Really only needed for packages


        if (PlayerPrefs.GetInt(this.gameObject.name) == 0)
        {

            ingredientsScript.totalRedGem += allCrysBonus;
            ingredientsScript.totalBlueGem += allCrysBonus;
            ingredientsScript.totalYellowGem += allCrysBonus;

            ingredientsScript.totalGreenGems += gemAmount; //100 is the default for now

            ingredientsScript.setNewPrefs();

            //unlock fox
            //petManager.disableOthers(); //this actuallyu doesnt do anything anymore

            //PlayerPrefs.SetInt ("FoxUnlocked",1);
            //PlayerPrefs.SetInt ("FoxOn", 1);

           // petManager.checkPetOn(); //this actuallyu doesnt do anything anymore

            fireworks.SetActive(true);

            AnalyticsEvent.Custom("ShopCards", new Dictionary<string, object> {

                {this.gameObject.name,1},
            });

        }

        hasPurchased();//this removes the button (plays animation) and changes the AMT text

        Camera.main.GetComponent<MainMenu>()._potionMasterScript.thirdPotionLock.SetActive(false);//removes the lock icon

    }

    public void giveGems(){

		ingredientsScript.totalGreenGems += gemAmount;

		ingredientsScript.setNewPrefs ();

		fireworks.SetActive (true);
//		hasPurchased ();//this removes the button (plays animation) and changes the AMT text

		AnalyticsEvent.Custom ("ShopCards", new Dictionary<string, object> {

			{this.gameObject.name,1},
		});


	}

    public void afPurchaseNonConsume(string packageName, string packageCost)
    {
        //print("purchases called first?! with appsflyer?");

        if (PlayerPrefs.GetInt(this.gameObject.name) == 0) //so it doesnt run this more than once. Make sure this code goes BEFORE the shop card code in the purchase manager. because the player pref will be set to ==1 after the purchase
        {
            //print("apps flyer testing woprking.this should onlu display 1 time");

            AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, new Dictionary<string, string>(){

            { AFInAppEvents.CONTENT_ID, packageName}, //for example "fury06" or "1234567"

            { AFInAppEvents.CONTENT_TYPE, "NonConsume"}, //this was changed from "category_a" to "NonConsume"

            { AFInAppEvents.REVENUE, packageCost},//must be like "1.99"

            { AFInAppEvents.CURRENCY, "USD"},

            { AFInAppEvents.QUANTITY, "1"}

            });

        }
    }

    public void afPurchaseIsConsumable(string packageName, string packageCost)
    {
            //because this is a consumable it can be called multiple times by the same person.
            //print("apps flyer testing woprking.this can be displayed many times");

            AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, new Dictionary<string, string>(){

            { AFInAppEvents.CONTENT_ID, packageName}, //for example "fury06" or "1234567"

            { AFInAppEvents.CONTENT_TYPE, "Consume"}, //this was changed from "category_a" to "default"

            { AFInAppEvents.REVENUE, packageCost},//must be like "1.99"

            { AFInAppEvents.CURRENCY, "USD"},

            { AFInAppEvents.QUANTITY, "1"}

            });

    }

    public void unlockPetsAndEquip(int indexToUnlock)
    {

      //  print("unlock pets is being called for index " + indexToUnlock.ToString());


        PetsMaster petMasterScript = Camera.main.GetComponent<MainMenu>().petManager.petMasterScript;
        PetGenerator petGenerator = Camera.main.GetComponent<MainMenu>().petManager.petMasterScript.petGenScript;

        //make sure the arrays are loaded

        petMasterScript.loadPetsAtStart(); //this loads all the arrays

        //write new pets to the array

        petGenerator.UnlockedPets[indexToUnlock] = 1;

        //check if any pets are equipped, if they are not, automatically equip the last one u bought

        bool petEquipped = false;

        for(int i=0; i<petGenerator.petIndexEquipped.Length; i++)
        {

            if (petGenerator.petIndexEquipped[i] != -1) //-1 means nothing equipped
            {
                petEquipped = true;
            }

        }

        if (petEquipped == true)
        {
            //do nothing
        }
        else
        {
            petGenerator.petIndexEquipped[0] = indexToUnlock; //if no pet is equipped, equip the newly bought pet in the first slow
        }

        //

        petMasterScript.checkPetEquipped(); //this loads the pets into the petsmanager scripts if they have been unlocked

        //get new ability arrays - via petmanager

        petMasterScript.petManager.getNewArrayValues();

        //save the array (make sure old arrays are loaded so we dont overwrite them)

        petGenerator.savePetsUnlocked(); //saves pets unlocked but also pets equipped and the pets lv


    }


}
