﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReviewGame : MonoBehaviour {

    // Use this for initialization
    void OnEnable()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().showInventoryItem();
        //do i need a windowopen = true here? It shouldnt be saying its false...
    }

    public void reviewNow()
    {
        PlayerPrefs.SetInt("StoreReview", 1);
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.Company.ClickerKnightIncrementalIdleRPG");
        Camera.main.GetComponent<MainMenu>().windowOpen = false;
        PlayerPrefs.SetInt("ResearchPoints", PlayerPrefs.GetInt("ResearchPoints") +1);
        this.gameObject.SetActive(false);

    }

    public void reviewLater()
    {
        PlayerPrefs.SetInt("StartupSinceReview", 0);
        Camera.main.GetComponent<MainMenu>().windowOpen = false;
        this.gameObject.SetActive(false);

    }

    public void reviewNever()
    {
        PlayerPrefs.SetInt("StoreReview", 1);
        Camera.main.GetComponent<MainMenu>().windowOpen = false;
        this.gameObject.SetActive(false);

    }
}
