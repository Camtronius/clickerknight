﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetManager : MonoBehaviour {

	public List<GameObject> petsList = new List<GameObject>();
    public float[] abilityArray = new float[32];
    public float[] abilityArray2 = new float[32]; //for pets that have dual abilities


    public Button petsButton;
    public PetsMaster petMasterScript;
    public MainMenu cameraObj;
	// Cat; -0
	// Fox; -1
	// Penguin; -2
	// Rabbit; -3

	// Use this for initialization
	void Start () {
		//for testing
//		PlayerPrefs.SetInt ("FoxUnlocked",1);
//		PlayerPrefs.SetInt ("CatUnlocked",1);
//		PlayerPrefs.SetInt ("RabbitUnlocked",1);
//		PlayerPrefs.SetInt ("PenguinUnlocked",1);


	}

	void OnEnable(){

        cameraObj = Camera.main.GetComponent<MainMenu>();

        petMasterScript.loadPetsAtStart();//this makes sure the pets equipped will be loaded correctly

        getNewArrayValues(); //this makes sure we get the array values for the pets special abilities

        StartCoroutine("PetAutoAtk");

    }

    public void getNewArrayValues()
    {
        //first reset array
        for(int i=0; i<abilityArray.Length; i++)
        {
            abilityArray[i] = 0; //make sure u -1 from the abilities unless they are being multiplied
            abilityArray2[i] = 0;
        }

        for(int j=0; j<petMasterScript.petGenScript.petIndexEquipped.Length; j++) //int of this is always going to be 3 basically
        {

            int indexOfAbility = petMasterScript.petGenScript.petIndexEquipped[j]; //gets the 0,1,2 positions of the list which tell us the index of the pet equipped

            int petLv = 0;//temp designation

            if (indexOfAbility >= 0)
            {
                petLv = petMasterScript.petGenScript.UnlockedPetsLevel[indexOfAbility]; //gets the pets level based on their equipped index
            }

            int evoLv = 0; //1,2,3 based on pet lv

            if (petLv < 30)
            {
                evoLv = 1;
            }

            if(petLv >=30 && petLv < 60)
            {
                evoLv = 2;
            }

            if (petLv >= 60)
            {
                evoLv = 3;
            }

            //add one to this value because it is going to be multiplicative

            if (indexOfAbility >= 0) //need this because slots with no pets are -1 index.
            {
                abilityArray[indexOfAbility] = petMasterScript.petGenScript.PetAbilityModifier[indexOfAbility]*evoLv; //copies the ability over so we can use it. We dont want to have abilities being used that arent supposed to be
                 //evoLv is what scales this ability
                abilityArray2[indexOfAbility] = petMasterScript.petGenScript.PetAbilityModifier2[indexOfAbility]*evoLv; //copies the ability over so we can use it. We dont want to have abilities being used that arent supposed to be

            }
        }



    }

    IEnumerator PetAutoAtk()
    {

        while (true)
        {
            float petFlurrymod = 0;//this is from the cameraobj, to check if the player is using pet flurry
            if (cameraObj.petFlurryTime > 0)
            {
                petFlurrymod = cameraObj.petFlurrymodifier;
            }

            yield return new WaitForSeconds(0.67f*(1-abilityArray[0] - petFlurrymod)); //pet flurry mod is normally zero unless the ability is used

            if (petsList[0].activeSelf == true)
            {

                if (cameraObj.windowOpen == false)
                {
                    this.gameObject.GetComponent<Animator>().SetTrigger("PetAtk");
                }

            }

            yield return new WaitForSeconds(0.67f * (1 - abilityArray[0] - petFlurrymod));

            if (petsList[1].activeSelf == true)
            {
                if (cameraObj.windowOpen == false)
                {
                    this.gameObject.GetComponent<Animator>().SetTrigger("PetAtk1");
                }
            }

            yield return new WaitForSeconds(0.67f * (1 - abilityArray[0] - petFlurrymod));

            if (petsList[2].activeSelf == true)
            {
                if (cameraObj.windowOpen == false)
                {
                    this.gameObject.GetComponent<Animator>().SetTrigger("PetAtk2");
                }
            }


        }
    }

    public void petAttack(int equippedIndex)
    {

        switch (equippedIndex) //this is the index of the pet equpped. for example Pet#1,2,3
        {

            case 0:
                //add spell type attacking
                int petIndex = petMasterScript.petGenScript.petIndexEquipped[0]; //this is the index of the pet in the list of 30 pets

                if (petMasterScript.petGenScript.PetType[petIndex] == 1)
                { //if the pet is attack type
                float petAtkMod = petMasterScript.petGenScript.PetDescModifier[petIndex] + (petMasterScript.petGenScript.DescGrowth[petIndex] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex] - 1));
                cameraObj.petPhysicalAttack(petAtkMod);
                }
                else//the pet is magic type
                {
                float petAtkMod = petMasterScript.petGenScript.PetDescModifier[petIndex] + (petMasterScript.petGenScript.DescGrowth[petIndex] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex] - 1));
                cameraObj.petMagicAttack(petAtkMod);
                }

                break;

            case 1:
                //add spell type attacking

                int petIndex2 = petMasterScript.petGenScript.petIndexEquipped[1]; //this is the index of the pet in the list of 30 pets

                if (petMasterScript.petGenScript.PetType[petIndex2] == 1)
                {
                    float petAtkMod2 = petMasterScript.petGenScript.PetDescModifier[petIndex2] + (petMasterScript.petGenScript.DescGrowth[petIndex2] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex2] - 1));
                    cameraObj.petPhysicalAttack(petAtkMod2);
                }
                else
                {
                    float petAtkMod2 = petMasterScript.petGenScript.PetDescModifier[petIndex2] + (petMasterScript.petGenScript.DescGrowth[petIndex2] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex2] - 1));
                    cameraObj.petMagicAttack(petAtkMod2);
                }

                break;

            case 2:
                //add spell type attacking

                int petIndex3 = petMasterScript.petGenScript.petIndexEquipped[2]; //this is the index of the pet in the list of 30 pets

                if (petMasterScript.petGenScript.PetType[petIndex3] == 1)
                {
                    float petAtkMod3 = petMasterScript.petGenScript.PetDescModifier[petIndex3] + (petMasterScript.petGenScript.DescGrowth[petIndex3] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex3] - 1));
                    cameraObj.petPhysicalAttack(petAtkMod3);
                }
                else
                {
                    float petAtkMod3 = petMasterScript.petGenScript.PetDescModifier[petIndex3] + (petMasterScript.petGenScript.DescGrowth[petIndex3] * (petMasterScript.petGenScript.UnlockedPetsLevel[petIndex3] - 1));
                    cameraObj.petMagicAttack(petAtkMod3);
                }

                break;


        }

    }

    public void checkPetOn(){//checks if there should be a pet on at start



		//if (PlayerPrefs.GetInt ("CatOn") == 1) {
		//	petsList [0].SetActive (true);

		//} else {
		//	petsList [0].SetActive (false);
		//}

		//if (PlayerPrefs.GetInt ("FoxOn") == 1) {
		//	petsList [1].SetActive (true);

		//}else {
		//	petsList [1].SetActive (false);

		//}

		//if (PlayerPrefs.GetInt ("PenguinOn") == 1) {
		//	petsList [2].SetActive (true);

		//}else {
		//	petsList [2].SetActive (false);

		//}

		//if (PlayerPrefs.GetInt ("RabbitOn") == 1) {
		//	petsList [3].SetActive (true);

		//}else {
		//	petsList [3].SetActive (false);

		//}

		//for (int i = 0; i < petsList.Count; i++) {

		//	if (petsList [i].activeSelf == true) {

		//		petsButton.interactable = true;
		//		break;
		//	}

		//}


	}

	// Update is called once per frame
	void Update () {
		
	}

	public void equipPet(PetType pressedObj){

		////
		//disableOthers();//disables all the pets right now

		//if (pressedObj.TypeOfPet == PetType.petType.Cat) {

		//	print ("cat activate");
		////	petsList [0].SetActive (true);
		//	PlayerPrefs.SetInt (pressedObj.TypeOfPet + "On",1);

		//}

		//if (pressedObj.TypeOfPet == PetType.petType.Rabbit) {

		//	print ("rab activate");
		////	petsList [3].SetActive (true);
		//	PlayerPrefs.SetInt (pressedObj.TypeOfPet + "On",1);

		//}

		//if (pressedObj.TypeOfPet == PetType.petType.Fox) {

		//	print ("fox activate");
		////	petsList [1].SetActive (true);
		//	PlayerPrefs.SetInt (pressedObj.TypeOfPet + "On",1);
				
		//}

		//if (pressedObj.TypeOfPet == PetType.petType.Penguin) {

		//	print ("guin activate");
		////	petsList [2].SetActive (true);
		//	PlayerPrefs.SetInt (pressedObj.TypeOfPet + "On",1);


		//}

	//	checkPetOn ();
	}

	public void disableOthers(){

		//PlayerPrefs.SetInt ("CatOn", 0);


		//PlayerPrefs.SetInt ("FoxOn", 0);
	

		//PlayerPrefs.SetInt ("PenguinOn", 0);
	

		//PlayerPrefs.SetInt ("RabbitOn",0);
	

		//for (int i = 0; i < petsList.Count; i++) {

		//	petsList [i].SetActive (false);

		//}


	}

    public void openPetsPage()
    {
        Camera.main.GetComponent<MainMenu>().OpenMainCharPage();

        petMasterScript.pressPetsToggle();

    }





}
