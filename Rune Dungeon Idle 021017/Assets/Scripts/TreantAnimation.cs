﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;


public class TreantAnimation : MonoBehaviour {

	public int createdByPlayer = 0;
	public float timeAlive = 0; //this is for destroying the treant after some time and making a counter spell effect possible

	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public GameObject FloorObj; //this is for the destruction of the sprite when it goes below this y pos

	public bool IsShieldOrInvis = false;

	public GameObject rockExplosion;

	public GameObject treantRightHand;
	public GameObject placeholderPlayerObj;

	public GameObject player1Transform;
	public GameObject player2Transform;

	public GameObject triggerObj;
	public GameObject lifeStealObj;
	public GameObject ScreenShaker;

	public Collider2D enemyObjHolder;
	public GameObject ExplosionForce;

	public GameObject HealingObj;


	// Use this for initialization
	void Start () {
	
		ScreenShaker = GameObject.Find ("ScreenShakerController");
		SpellCreator = GameObject.Find ("SpellListener");
		FloorObj = GameObject.Find ("FloorSprite");


		if (createdByPlayer == 1) {

			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(2f,0);
		}
		if (createdByPlayer == 2) {
			this.gameObject.transform.localScale = new Vector3((-1)*(this.gameObject.transform.localScale.x),
			                                                   (this.gameObject.transform.localScale.y), 
			                                                   (this.gameObject.transform.localScale.z));

			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2f,0);
		}

		player1Transform = GameObject.Find ("Player1Character");
		player2Transform = GameObject.Find ("Player2Character");



	}


	void OnTriggerEnter2D (Collider2D player) {

//		if (player.tag == "Player2" && createdByPlayer == 1 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			
//			print ("treant is colliding");
//
//			if(player.GetComponent<ControlPlayer2Character>().isShielded==false){
//
//			this.gameObject.GetComponentInParent<Rigidbody2D>().velocity= new Vector2(0,0); //makes the treant stop
//
//			this.gameObject.GetComponentInParent<Animator>().SetBool("GrabPlayer",true); //sets his anim to grabbing
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().gravityScale = 0.0f;
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().redoGravity(); //applies the gravity to all the parts of the player
//
//			player.gameObject.transform.parent = treantRightHand.transform; 
//
//			player.gameObject.transform.position = treantRightHand.transform.position;
//
//			if(player.gameObject.tag=="Player2"){
//				player.gameObject.transform.rotation = Quaternion.Euler(0,180,0);
//
//			}
//
//					//where Damage is calcualted
//					if(player.gameObject.GetComponent<ControlPlayer2Character>().isShielded==false){
//						
//						float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "EarthTreant")
//						+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[3] //adding fire item mods
//					    + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod
//
//						
//						//condition for super effective
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//							
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//						}
//						
//						//condition for Not very effective
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//							
//						calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//						}
//						
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//						   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					 	  == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//							
//							
//						}
//
//					//hitting p2
//					//check for crit against p2
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//					   ){ //did the player crit on their turn??
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//
//						player.gameObject.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p2 notification
//
//				}
//
//
//					//End where Damage is calcualted
//	
//
//			placeholderPlayerObj = player.gameObject;
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll (); //puts the player in that ragdoll mode...
//			}
//		}
//
//		if (player.tag == "Player1" && createdByPlayer == 2 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//
//			if(player.GetComponent<ControlPlayer1Character>().isShielded==false){
//
//			print ("treant is colliding");
//
//			this.gameObject.GetComponentInParent<Rigidbody2D>().velocity= new Vector2(0,0); //makes the treant stop
//			
//			this.gameObject.GetComponentInParent<Animator>().SetBool("GrabPlayer",true); //sets his anim to grabbing
//			
//			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//			
//			player.gameObject.GetComponent<TurnOnRagdoll> ().gravityScale = 0.0f;
//			
//			player.gameObject.GetComponent<TurnOnRagdoll> ().redoGravity(); //applies the gravity to all the parts of the player
//			
//			player.gameObject.transform.parent = treantRightHand.transform; 
//			
//			player.gameObject.transform.position = treantRightHand.transform.position;
//			
//	
//			
//
//				//where Damage is calcualted
//				if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){
//					
//					float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "EarthTreant")
//						+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[3] //adding fire item mods
//					    + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod
//
//					
//					//condition for super effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//
//
//						calculatedHealthLoss = 2*calculatedHealthLoss;
//
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//
//
//						calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//						
//
//					}
//
//
//					//hit player 1
//					//check for crit against p1
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//						   ){ //did the player crit on their turn??
//							calculatedHealthLoss = 2*calculatedHealthLoss;
//							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//						}
//						
//						if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//						   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//							calculatedHealthLoss = 2*calculatedHealthLoss;
//							Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//							
//						}
//
//					player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p2 notification
//
//				}
//				//End where Damage is calcualted
//			
//			placeholderPlayerObj = player.gameObject;
//			
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll (); //puts the player in that ragdoll mode...
//			
//
//			
//			}
//		}
//
//		if (createdByPlayer == 2 && player.tag == "Player1" && Camera.main.GetComponent<CampaignGameOrganizer> () != null) { //this is for campaign for final Boss Act 2
//			
//				
//			//where Damage is calcualted
//
//					
//				if (player.GetComponent<ControlPlayer1Character> ().isShielded == false) {
//						
//					this.gameObject.GetComponentInParent<Rigidbody2D> ().velocity = new Vector2 (0, 0); //makes the treant stop
//						
//					this.gameObject.GetComponentInParent<Animator> ().SetBool ("SmashPlayer", true); //sets his anim to grabbing
//						
//				print ("should be adding p1 to enemy holder");
//
//					enemyObjHolder = player; //puts the enemy in the temp object holder
//
//
//			}
//		}
//
//		// for attacking the enemy units
//
//		if (createdByPlayer == 1 && player.tag == "Enemy" || createdByPlayer == 1 && player.tag=="Player2" && Camera.main.GetComponent<CampaignGameOrganizer>()!=null) {
//			
//			print ("treant with enemy colliding");
//		
//			if(player.GetComponent<ControlEnemy>().isShielded==false){
//				
//				this.gameObject.GetComponentInParent<Rigidbody2D>().velocity= new Vector2(0,0); //makes the treant stop
//				
//				this.gameObject.GetComponentInParent<Animator>().SetBool("SmashPlayer",true); //sets his anim to grabbing
//
//				enemyObjHolder = player; //puts the enemy in the temp object holder
//
//
//		//		player.gameObject.GetComponent<TurnOnRagdoll> ().gravityScale = 0.0f;
//				
//				//player.gameObject.GetComponent<TurnOnRagdoll> ().redoGravity(); //applies the gravity to all the parts of the player
//				
//		//		player.gameObject.transform.parent = treantRightHand.transform; 
//				
//		//		player.gameObject.transform.position = treantRightHand.transform.position;
//
//				
//				//where Damage is calcualted
//	
//				//End where Damage is calcualted
//				
//				
//			//	placeholderPlayerObj = player.gameObject;
//				
//			//	player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll (); //puts the player in that ragdoll mode...
//			}
//		}
//


	}

	public void RockExplosion( Collider2D enemy){

		rockExplosion = Instantiate (rockExplosion, new Vector3 (enemy.gameObject.transform.position.x,
		                                                         enemy.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;

	}

	public void stepShake(){

		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen

	}

	// Update is called once per frame
	void Update () {

		if (this.transform.position.y< FloorObj.transform.position.y) {

			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
			
			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);

			Destroy(this.gameObject);
			}

			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
				
				Destroy(this.gameObject);
			}
		}



	}

	public void dropThePlayer(){

		if (createdByPlayer == 1) {
			placeholderPlayerObj.transform.parent = player2Transform.transform; //sets the parent back to default
		}

		if (createdByPlayer == 2) {
			placeholderPlayerObj.transform.parent = player1Transform.transform; //sets the parent back to default


				print ("player scale being changed");
				player1Transform.GetComponentInChildren<ControlPlayer1Character>().transform.rotation = Quaternion.Euler(0,0,0);
			player1Transform.GetComponentInChildren<ControlPlayer1Character>().transform.localScale = new Vector3(1,1,1);

		}

		placeholderPlayerObj.gameObject.GetComponent<TurnOnRagdoll> ().gravityScale = 0.4f;

		placeholderPlayerObj.gameObject.GetComponent<TurnOnRagdoll> ().redoGravity(); //applies the gravity to all the parts of the player


	}

	public void setGrabFalse(){

		triggerObj.GetComponent<BoxCollider2D> ().enabled = false;

		if (createdByPlayer == 1) {
			
			this.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (2f, 0);
			
		}

		if (createdByPlayer == 2) {

			this.gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-2f, 0);
		
		}

		this.gameObject.GetComponentInParent<Animator>().SetBool("GrabPlayer",false);
		this.gameObject.GetComponentInParent<Animator>().SetBool("SmashPlayer",false);


	}

	public void hitEnemy(){


//		if(createdByPlayer == 1 && enemyObjHolder.gameObject.GetComponent<ControlEnemy>().isShielded==false){
//			
//			float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "EarthTreant")
//			                             + PlayerPrefs.GetFloat ("EarthItemMod") 
//			                             + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
//			
//			//condition for super effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//				
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//				Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
//
//			}
//			
//			//condition for Not very effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//
//				calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//			}
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//				
//
//			}
//
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//				
//				calculatedHealthLoss = 2*calculatedHealthLoss;
//				print("CRITS!!!!!!!!!");
//				
//			}
//			//crit stuff here
//			enemyObjHolder.gameObject.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
//
//
//
//			if(calculatedHealthLoss >= enemyObjHolder.GetComponent<ControlEnemy>().playerHealthCurrent){
//				
//				enemyObjHolder.gameObject.GetComponent<ControlEnemy>().playerDead=true;
//				enemyObjHolder.gameObject.GetComponent<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll
//				enemyObjHolder.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll();
//				
//				ExplosionForce = Instantiate (ExplosionForce, new Vector3 (enemyObjHolder.gameObject.GetComponent<EnemyRagdoll>().LeftLegUpper.transform.position.x,
//				                                                           enemyObjHolder.gameObject.GetComponent<EnemyRagdoll>().Torso.transform.position.y, 0), Quaternion.identity) as GameObject;
//				
//			}
//
//
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.2f); //shakes the screen
//
//
//			playTreantHit();
//			RockExplosion(enemyObjHolder);
//
//			//adds the hp of the players current atk mod
//			Camera.main.GetComponent<CampaignGameOrganizer>().addHP(Mathf.FloorToInt(PlayerPrefs.GetFloat("EarthItemMod")+ PlayerPrefs.GetFloat ("atkMod"))*10);
//
//		}
//
//		if(createdByPlayer ==2 && enemyObjHolder.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){
//			
//			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//
//			
//			//condition for super effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//				
//				DamageToPlayer = 2*DamageToPlayer;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//				Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(DamageToPlayer);
//				
//			}
//			
//			//condition for Not very effective
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//				
//				DamageToPlayer = (.5f)*DamageToPlayer;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				
//			}
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite ||
//			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//				
//				
//			}
//
//			enemyObjHolder.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//			Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//			
//			
//			
//			if(DamageToPlayer >= enemyObjHolder.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent){
//				
//				enemyObjHolder.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
//				enemyObjHolder.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//				enemyObjHolder.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//				
//			//	ExplosiveForce.GetComponent<ExplosiveForce>().explosionColliderRadius = 1.0f;
//				
////ExplosiveForce = Instantiate (ExplosiveForce, new Vector3 (enemyObjHolder.transform.position.x, enemyObjHolder.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity) as GameObject;
//				
//			}
//			
//			
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.2f); //shakes the screen
//			
//			
//			playTreantHit();
//			RockExplosion(enemyObjHolder);
//			
//			//adds the hp of the players current atk mod
//			Camera.main.GetComponent<CampaignGameOrganizer>().addHPEnemy(350);
//			
//		}

	}

	public void createLifeStealFX(){


//		if (placeholderPlayerObj.gameObject.tag == "Player2") {
//
//			int HPadd = Mathf.FloorToInt(Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character> ().playerHealthMax/10);
//			
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().HpGainP1.GetComponent<TextMeshProUGUI> ().text = "+ " + HPadd + " HP"; 
//
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().HpGainP1.SetActive (true);
//			
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character> ().lifeStealValue = HPadd;
//			
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.GetComponent<ControlPlayer1Character> ().lifeSteal = true;
//
//			GameObject HealingObjClone = (GameObject)Instantiate (HealingObj, new Vector3 (Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.transform.position.x, 
//			                                                                               Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1.transform.position.y, 0), Quaternion.identity);
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().PersistantSoundManager.getPotion();
//
//		}
//
//		if (placeholderPlayerObj.gameObject.tag == "Player1") {
//
//			int HPadd = Mathf.FloorToInt(Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character> ().playerHealthMax/10);
//
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().HpGainP2.GetComponent<TextMeshProUGUI> ().text = "+ " + HPadd + " HP"; 
//			
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().HpGainP2.SetActive (true);
//
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character> ().lifeStealValue = HPadd;
//
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.GetComponent<ControlPlayer2Character> ().lifeSteal = true;
//
//			GameObject HealingObjClone = (GameObject)Instantiate (HealingObj, new Vector3 (Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.transform.position.x, 
//			                                                                               Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2.transform.position.y, 0), Quaternion.identity);
//
//			Camera.main.GetComponent<TurnedBasedGameOrganizer>().PersistantSoundManager.getPotion();
//
//		//	PersistantSoundmanager.getPotion();
//
//		}

	}

	public void playTreantVoice(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.TreantVoice ();
		}

		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
		//	Camera.main.GetComponent<TurnedBasedGameOrganizer> ().PersistantSoundmanager.TreantVoice ();
		}
	}

	public void playTreantFootSound(){

		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.TreantFootStep ();
		}
		
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
			//	Camera.main.GetComponent<TurnedBasedGameOrganizer> ().PersistantSoundmanager.TreantVoice ();
		}

	}

	public void playTreantHit(){
		
		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {
			Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.TreantHit ();
		}
		
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {
			//	Camera.main.GetComponent<TurnedBasedGameOrganizer> ().PersistantSoundmanager.TreantVoice ();
		}
		
	}

}
