﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;


public class LimitedTimeOffers : MonoBehaviour {

    public MainMenu cameraObj;

    public GameObject fadedBlackScreen;

    public GameObject saleDisplay;

    public GameObject starterOfferPage;
    public TextMeshProUGUI starterOfferText;

    public GameObject furyOfferPage;
    public TextMeshProUGUI furyOfferText;

    public GameObject arcaneOfferPage;
    public TextMeshProUGUI arcaneOfferText;

    public GameObject comboOfferPage;
    public TextMeshProUGUI comboOfferText;

    public GameObject eventOfferPage;

    //chemist not included
    public GameObject chemistOfferPage;

 



    public GameObject starterOfferIcon;
    public GameObject furyOfferIcon;
    public GameObject arcaneOfferIcon;
    public GameObject chemistOfferIcon;
    public GameObject comboOfferIcon;
    public GameObject eventOfferIcon;



    public DateTime oldDate;
    public DateTime newDate;

    public TextMeshProUGUI textTillNextOffer;
    

    // Use this for initialization
    void Start()
    {

        StartCoroutine("OfferTimeUpdate"); //detects if a block is being selected

    }

    void OnEnable () {


        // checkWhichOffer();
	}

    public void checkBought()
    {
        if (ObscuredPrefs.GetInt("StarterPack") == 1 || ObscuredPrefs.GetInt("StarterPackPromo") == 1)
        {
            starterOfferIcon.SetActive(false);
        }

        if (ObscuredPrefs.GetInt("FuryPack") == 1 || ObscuredPrefs.GetInt("FuryPackPromo") == 1)
        {
            furyOfferIcon.SetActive(false);
        }

        if (ObscuredPrefs.GetInt("ArcanePack") == 1 || ObscuredPrefs.GetInt("ArcanePackPromo") == 1)
        {
            arcaneOfferIcon.SetActive(false);
        }

        if (ObscuredPrefs.GetInt("FuryPack") == 1 && ObscuredPrefs.GetInt("ArcanePack") == 1)
        {
            comboOfferIcon.SetActive(false);
        }

        if(ObscuredPrefs.GetInt("EventPack") == 1)
        {
            eventOfferIcon.SetActive(false);
        }

    }
	
    public void checkWhichOffer()
    {

        if (Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode == true && ObscuredPrefs.GetInt("EventPack") == 0)
        {

            eventOfferIcon.SetActive(true);
            textTillNextOffer.text = "";

        }
        else
        {

            checkBought();//removes icons if they are already purchased items

            //starter pack offer (this is always first)
            if (PlayerPrefs.GetInt("CurrentLevel") >= 4 && ObscuredPrefs.GetInt("StarterPack") == 0 && ObscuredPrefs.GetInt("StarterPackPromo") == 0)//if the current level >3 and the starter pack has not been purchased, show the limited offer for starter pack if the promo hasnt been purchased either
            {
                //   print("starter offer being called");
                starterOfferIcon.SetActive(true);

                if (ObscuredPrefs.GetInt("StarterPackPromoStarted") == 0)
                {
                    ObscuredPrefs.SetInt("StarterPackPromoStarted", 1); //the starter pack promo has started

                }

                refreshDealCountDown();

            }

            if (ObscuredPrefs.GetInt("StarterPackPromo") == 1) //the starter pack promo has been completed... check what the player is activeky doing in the game

            {

                //    print("starter pack promo ==1");

                if (ObscuredPrefs.GetInt("FuryPack") == 0 && ObscuredPrefs.GetInt("ArcanePack") == 0 && ObscuredPrefs.GetInt("ChemistPack") == 0) //if the fury or arcane pack or chemist have not been purchased
                {

                    float AtkDifference = PlayerPrefs.GetFloat("atkMod") - PlayerPrefs.GetFloat("luckMod"); //for checking if the player primarily uses atk
                    float MagDifference = PlayerPrefs.GetFloat("luckMod") - PlayerPrefs.GetFloat("atkMod"); //for checking if the player primarily uses mag

                    //    print("atk diff is " + AtkDifference + "mag diff is" + MagDifference);

                    //show fury pack x2 dmg/tap/hold for dmg
                    if (ObscuredPrefs.GetInt("FuryPack") == 0 && ObscuredPrefs.GetInt("FuryPackPromo") == 0 && AtkDifference >= 1 && checkIfStarted("Fury") == true || ObscuredPrefs.GetInt("FuryPackPromoStarted") == 1 && ObscuredPrefs.GetInt("FuryPackPromo") == 0)
                    //they have not purchased the fury pack yet, the fury pack promo has not been completed, the atk to mag difference is >100, and other packages have not been started
                    //OR the fury promo has already been started  and not completed
                    {

                        //        print("fury offer being called");
                        furyOfferIcon.SetActive(true);

                        if (ObscuredPrefs.GetInt("FuryPackPromoStarted") == 0)
                        {
                            ObscuredPrefs.SetInt("FuryPackPromoStarted", 1); //the starter pack promo has started

                        }

                        refreshDealCountDown();


                    }


                    //show arcane pack

                    if (ObscuredPrefs.GetInt("ArcanePack") == 0 && ObscuredPrefs.GetInt("ArcanePackPromo") == 0 && MagDifference >= 1 && checkIfStarted("Arcane") == true || ObscuredPrefs.GetInt("ArcanePackPromoStarted") == 1 && ObscuredPrefs.GetInt("ArcanePackPromo") == 0)
                    //they have not purchased the Arcane pack yet, the Arcane pack promo has not been completed, the mag to atk difference is >100, and other packages have not been started
                    //OR the fury promo has already been started  and not completed
                    {

                        //     print("Arcane offer being called");
                        arcaneOfferIcon.SetActive(true);

                        if (ObscuredPrefs.GetInt("ArcanePackPromoStarted") == 0)
                        {
                            ObscuredPrefs.SetInt("ArcanePackPromoStarted", 1); //the starter pack promo has started

                        }

                        refreshDealCountDown();


                    }

                }
                else if (ObscuredPrefs.GetInt("FuryPack") == 1 &&
                        ObscuredPrefs.GetInt("ArcanePack") == 0 &&
                        ObscuredPrefs.GetInt("ChemistPack") == 0
                        ||
                        ObscuredPrefs.GetInt("FuryPack") == 0 &&
                        ObscuredPrefs.GetInt("ArcanePack") == 1 &&
                        ObscuredPrefs.GetInt("ChemistPack") == 0
                        ||
                        ObscuredPrefs.GetInt("FuryPack") == 0 &&
                        ObscuredPrefs.GetInt("ArcanePack") == 0 &&
                        ObscuredPrefs.GetInt("ChemistPack") == 1)
                {//at least one purchase has been made, but no others...

                    if (ObscuredPrefs.GetInt("ComboPackPromo") == 0 || ObscuredPrefs.GetInt("ComboPackPromoStarted") == 1 && ObscuredPrefs.GetInt("ComboPackPromo") == 0)
                    //they have not purchased the Arcane pack yet, the Arcane pack promo has not been completed, the mag to atk difference is >100, and other packages have not been started
                    //OR the fury promo has already been started  and not completed
                    {

                        //       print("Combo offer being called");
                        comboOfferIcon.SetActive(true);

                        if (ObscuredPrefs.GetInt("ComboPackPromoStarted") == 0)
                        {
                            ObscuredPrefs.SetInt("ComboPackPromoStarted", 1); //the starter pack promo has started

                        }

                        refreshDealCountDown();


                    }


                }



            }
            //
        }
    }

    public bool checkIfStarted(string pkgName) //checks if another promo has been started
    {

        if(pkgName=="Fury")
        {

            if(ObscuredPrefs.GetInt("ArcanePackPromoStarted") == 1)
            {

                return false;
            }
            else
            {
                return true; //returns true if those promos have not been started
            }

        }

        if (pkgName == "Arcane")
        {

            if (ObscuredPrefs.GetInt("FuryPackPromoStarted") == 1)
            {

                return false;
            }
            else
            {
                return true; //returns true if those promos have not been started
            }

        }

        if (pkgName == "Chemist")
        {

            if (ObscuredPrefs.GetInt("FuryPackPromoStarted") == 1 || ObscuredPrefs.GetInt("ArcanePackPromoStarted") == 1)
            {

                return false;
            }
            else
            {
                return true; //returns true if those promos have not been started
            }

        }

        return true; //if the pkgname is entered incorrectly returns that a pkg has been started as a failsafe


    }

    public void refreshDealCountDown()
    {

            if (PlayerPrefs.GetString("LimitedOffer") != "")
            { //checks if a time has been saved previouslyt

                long temp = Convert.ToInt64(PlayerPrefs.GetString("LimitedOffer")); //gets the last time the shop was opened 

                newDate = System.DateTime.Now; //stores the new date of the system at time of game start

                oldDate = DateTime.FromBinary(temp); //gets the stored last date

                TimeSpan difference = newDate.Subtract(oldDate);

                double _Totalhours = difference.TotalHours; //total mins since last opening the shop

                PlayerPrefs.SetFloat("LimitedOfferHours", (float)_Totalhours + PlayerPrefs.GetFloat("LimitedOfferHours")); //this is the amount of hours stored since last opening the shop

        //    print("the total amt of hours is " + _Totalhours);

        }
        else
        {
            //if the notification has not been opened yet


        }

            float totalHours = PlayerPrefs.GetFloat("LimitedOfferHours"); //temp hours are the hours since last opening the shop. If >2 then display new cards

            if (totalHours <= 72 && totalHours > 0)
            {

      //      print("total hrs btwn 0 and 72");

                float days = (72 - totalHours) / 24f;

                string amtHrs = days.ToString();
                amtHrs = amtHrs.Substring(0, 1); //gives the amt of hours

                float amtMinDecim = days - (int)days; //this gives u the leftover decimal place
                int minsLeft = Mathf.FloorToInt(amtMinDecim * 24);

                textTillNextOffer.text = "Offer Exipres in " + amtHrs.ToString() + " Days and " + minsLeft.ToString() + " Hours!";

            limitedOfferText(amtHrs.ToString() + " Days," + minsLeft.ToString() + " Hours!");
            }
            else if(totalHours==0)
            {

     //       print("totalHours is 0!");


            textTillNextOffer.text = "Offer Exipres in 3 Days!";
            limitedOfferText("Special Offer!");


        }
        else //total hours is >72
        {
       //     print("total hrs >72");
            
            if(ObscuredPrefs.GetInt("StarterPackPromoStarted") == 1)
            { 
                ObscuredPrefs.SetInt("StarterPackPromo", 1); //the starter pack promo has ended!
                starterOfferIcon.SetActive(false);
            }

            if (ObscuredPrefs.GetInt("FuryPackPromoStarted") == 1)
            {
                ObscuredPrefs.SetInt("FuryPackPromoStarted", 0);//need to set to zero because the other promos need to know one is not in progress
                ObscuredPrefs.SetInt("FuryPackPromo", 1); //the starter pack promo has ended!
                furyOfferIcon.SetActive(false);

            }

            if (ObscuredPrefs.GetInt("ArcanePackPromoStarted") == 1)
            {
                ObscuredPrefs.SetInt("ArcanePackPromoStarted", 0);//need to set to zero because the other promos need to know one is not in progress
                ObscuredPrefs.SetInt("ArcanePackPromo", 1); //the starter pack promo has ended!
                arcaneOfferIcon.SetActive(false);

            }

            if (ObscuredPrefs.GetInt("ComboPackPromoStarted") == 1)
            {
                ObscuredPrefs.SetInt("ComboPackPromo", 1); //the starter pack promo has ended!
                comboOfferIcon.SetActive(false);

            }

            PlayerPrefs.SetFloat("LimitedOfferHours", 0); //resets this value so we are starting from another 72hr  value


        }

        PlayerPrefs.SetString("LimitedOffer", System.DateTime.Now.ToBinary().ToString()); //this is so we know how much time has passed since the player last opened the shop


        

    }

    public void limitedOfferText(string offer) //this method updates the "Limited Offer" To show the time remaining.
    {
         starterOfferText.text = offer;

         furyOfferText.text = offer;

        arcaneOfferText.text = offer;

        comboOfferText.text = offer;



}

public void openStarterOffer()
    {
        fadedBlackScreen.SetActive(true);
        saleDisplay.SetActive(true);
        starterOfferPage.SetActive(true);
        cameraObj.windowOpen = true;
        cameraObj.persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void openFuryOffer()
    {
        fadedBlackScreen.SetActive(true);
        saleDisplay.SetActive(true);
        furyOfferPage.SetActive(true);
        cameraObj.windowOpen = true;
        cameraObj.persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void openEventOffer()
    {
        fadedBlackScreen.SetActive(true);
        saleDisplay.SetActive(true);
        eventOfferPage.SetActive(true);
        cameraObj.windowOpen = true;
        cameraObj.persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void openArcaneOffer()
    {
        fadedBlackScreen.SetActive(true);
        saleDisplay.SetActive(true);
        arcaneOfferPage.SetActive(true);
        cameraObj.windowOpen = true;
        cameraObj.persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void openChemistOffer()
    {

    }

    public void openComboOffer()
    {
        fadedBlackScreen.SetActive(true);
        saleDisplay.SetActive(true);
        comboOfferPage.SetActive(true);
        cameraObj.windowOpen = true;
        cameraObj.persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void closeOffers()
    {

    fadedBlackScreen.SetActive(false);

    saleDisplay.SetActive(false);

    starterOfferPage.SetActive(false);
    furyOfferPage.SetActive(false);
    arcaneOfferPage.SetActive(false);
    chemistOfferPage.SetActive(false);
    comboOfferPage.SetActive(false);
    //    cameraObj.persistantSoundManager.GetComponent<SoundManager>().BackButton();

       // cameraObj.windowOpen = false;

        cameraObj.checkIfEggAlive(); //this checks if there is an egg. if its not alive it disables all attacking...
    }

    public void closeOffersPage()//just for the sfx
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();
        cameraObj.windowOpen = false;
        cameraObj.checkIfEggAlive(); //this checks if there is an egg. if its not alive it disables all attacking...


    }

    IEnumerator OfferTimeUpdate()
    {

        while (true)
        {

            checkWhichOffer(); //this method contains code to also refresh the timers
      //      print("timed offer being updated");

            yield return new WaitForSeconds(60f);//updates once every minute

        }
    }

}
