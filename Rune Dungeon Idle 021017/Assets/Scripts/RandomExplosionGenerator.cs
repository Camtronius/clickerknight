﻿using UnityEngine;
using System.Collections;

public class RandomExplosionGenerator : MonoBehaviour {

	public GameObject smallExplosions;
	public Vector3 ThisPosition;
	public float countdownTime = 10f;
	// Use this for initialization
	void Start () {
		ThisPosition = this.gameObject.transform.position;

		StartCoroutine ("generateRandomExplosions");
	}

	IEnumerator generateRandomExplosions(){
		
		while (true) {

			if(countdownTime>0){
			
			int RandX =  Random.Range(-2, 2);

			int RandY =  Random.Range(-2, 2);

			GameObject smallExplosionsClone = (GameObject)Instantiate (smallExplosions, new Vector3 (ThisPosition.x + RandX, ThisPosition.y + RandY, 0), Quaternion.identity);

			}

			yield return new WaitForSeconds(0.2f);
		}
		
	}


	// Update is called once per frame
	void Update () {
	
		countdownTime -= Time.deltaTime;

		if (countdownTime < 0) {
			StopCoroutine ("generateRandomExplosions");
			Destroy(this.gameObject);
		}

	}
}
