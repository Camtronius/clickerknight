﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour {

	public bool xPosPlus=false;
	public bool xPosMinus=false;
	public bool yPosPlus=false;
	public bool yPosMinus=false;

	public float ShakeAmount = 0.2f;
	public float ShakeTimer =3f;

	public Vector3 defaultPosition;

	// Use this for initialization
	void Start () {
	
		defaultPosition = this.transform.position;

		StartCoroutine ("ScreenShaker");

	}
	
	// Update is called once per frame
	void Update () {

		if (ShakeTimer > 0) {

			ShakeTimer-=Time.deltaTime;
		
		}
	
	}

	IEnumerator ScreenShaker(){
		
		while (true) {

			if(ShakeTimer>0){

			int RandColor =  Random.Range(0, 4);

			switch (RandColor) {
			
			case(0):
				this.transform.position += new Vector3(ShakeAmount,0,0);
				 xPosPlus=true;
//					print ("!!!");

			break;

			case(1):
				this.transform.position -= new Vector3(ShakeAmount,0,0);
				 xPosMinus=true;
//					print ("!!!");

			break;

			case(2):
				this.transform.position += new Vector3(0,ShakeAmount,0);
				 yPosPlus=true;
//					print ("!!!");

			break;

			case(3):
				this.transform.position -= new Vector3(0,ShakeAmount,0);
				 yPosMinus=true;
//					print ("!!!");

			break;

			}

				yield return new WaitForSeconds (.05f);


			

			if(xPosPlus==true){
				this.transform.position -= new Vector3(ShakeAmount,0,0);
				 xPosPlus=false;

			}
				if(xPosMinus==true){
				this.transform.position += new Vector3(ShakeAmount,0,0);
					xPosMinus=false;
					
				}
					if(yPosPlus==true){
				this.transform.position -=new Vector3(0,ShakeAmount,0);
						yPosPlus=false;
						
					}
						if(yPosMinus==true){
				this.transform.position +=new Vector3(0,ShakeAmount,0);
							yPosMinus=false;
							
						}


			}

			if(ShakeTimer<0){

			//	this.transform.position = defaultPosition;
			}

			yield return new WaitForSeconds (.05f);

		}
	
	}


}
