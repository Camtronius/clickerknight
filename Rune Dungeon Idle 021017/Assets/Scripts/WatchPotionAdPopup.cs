﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WatchPotionAdPopup : MonoBehaviour {

    public List<Sprite> potionSprites = new List<Sprite>(); //0 gold //1 silver //2 EXP-orange //3 purple //4 ebent
    public PotionMasterScript potionMaster;

    public GameObject potionAdPopUp; //the actual gameobject that IS the popup. Used for animation purposes

    public TextMeshProUGUI potionDesc;
    public Image potionImage;

    public BrewPotion goldPotionScript; //gold pot
    public BrewPotion silverPotionScript; //dmg pot
    public BrewPotion orangePotionScript; //exp pot
    public BrewPotion purplePotionScript; //abil refresh
    public BrewPotion whitePotionScript; //event speedup

    public Button watchAdButton;

    private MainMenu cameraObj;


    // Use this for initialization
    void Start() {

        if (PlayerPrefs.GetInt("CurrentLevel") >= 6 && PlayerPrefs.GetInt("CurrentLevel")%2==0)
        {
            //print("we are on an even level so pop ups are a go!");

            potionMaster.masterLoadOnEnable();//loads all the potion data and other stuff so we can brew potions! :D

            cameraObj = Camera.main.GetComponent<MainMenu>();

            StartCoroutine(popUpChecker());

        }

    }

    IEnumerator popUpChecker()
    { //***make sure this is only called once per game and doesnt repeatedly get called //this also checks for potions as well

        while (true)
        {
            //if a potion slot is available 

            bool ableToBrew = false;

            //this checks if there is an available potion slot...

            for (int i = 0; i < potionMaster.getMaxPotionBrewed(); i++)
            {

                if (potionMaster.potionsListToDrink[i].spriteHolder.sprite == potionMaster.potionsListToDrink[i].nullSprite)
                {
                    ableToBrew = true;
                }
            }

            //we can brew a potion!

            if (ableToBrew == true) {

                //if it is not a Daily Dungeon or an Event..

                if (cameraObj.enemyStatsObj.dungeonMode == false && cameraObj.enemyStatsObj.eventMode == false)
                {
                    //if player has around 50% of the cost of a level but less than 100%, show gold popup (make sure they are not in dungeon or event)

                    goldPotionScript.potionStart(); //this will update the time since the last potion was viewed.
                    orangePotionScript.potionStart();
                    silverPotionScript.potionStart();
                    purplePotionScript.potionStart();

                    //wait 60 seconds
                    if (PlayerPrefs.GetInt("DefeatedAtBoss") == 1)//show silver potion ad
                    {
                        if (silverPotionScript.adWatchableBool == true)
                        {
                            //print("show silver pot!");
                            yield return new WaitForSeconds(3f); //how long to wait until we see the silver ad

                            setAdParameters(silverPotionScript, 1);//uses gold potion script to design the ad and also makes the ad show
                            watchAdButton.onClick.RemoveAllListeners();
                            watchAdButton.onClick.AddListener(addSilvPotClickerMethod);

                            yield return new WaitForSeconds(30f);
                            potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                            //this will auto deactivate after it has been shown.
                        }

                        if (purplePotionScript.adWatchableBool == true)
                        {
                            //print("show purple pot!");
                            yield return new WaitForSeconds(15f); //time to wait till purp pot

                            setAdParameters(purplePotionScript, 3);//uses gold potion script to design the ad and also makes the ad show
                            watchAdButton.onClick.RemoveAllListeners();
                            watchAdButton.onClick.AddListener(addPurpPotClickerMethod);

                            yield return new WaitForSeconds(30f);
                            potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                            //this will auto deactivate after it has been shown.
                        }

                        PlayerPrefs.SetInt("DefeatedAtBoss", 0); //set it as 0 so that if this routine is run again, it will only show 

                        //show silver potion ad if available

                    }
                    else if (PlayerPrefs.GetInt("DefeatedAtBoss") == 0)
                    {

                        if (goldPotionScript.adWatchableBool == true)
                        {
                            //print("gold pot step 2!");

                            yield return new WaitForSeconds(60f); //how long do we wait before we see a gold pot ad?

                            setAdParameters(goldPotionScript,0);//uses gold potion script to design the ad and also makes the ad show
                            watchAdButton.onClick.RemoveAllListeners();
                            watchAdButton.onClick.AddListener(addGoldPotClickerMethod);

                            yield return new WaitForSeconds(30f);
                            potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                            //this will auto deactivate after it has been shown.
                        }


                        if (orangePotionScript.adWatchableBool==true)
                        {
                            //print("orange pot step 2!");

                            yield return new WaitForSeconds(120f); //change this to be arouynd 4 mins after gold potion, then the cycle restarts

                            setAdParameters(orangePotionScript, 2);//uses gold potion script to design the ad and also makes the ad show
                            watchAdButton.onClick.RemoveAllListeners();
                            watchAdButton.onClick.AddListener(addOrangePotClickerMethod);

                            yield return new WaitForSeconds(30f);
                            potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                            //this will auto deactivate after it has been shown.
                        }

                    }

                }
                else if(cameraObj.enemyStatsObj.dungeonMode == true)
                {
                    silverPotionScript.potionStart();

                    if (silverPotionScript.adWatchableBool == true)
                    {
                        yield return new WaitForSeconds(3f); //delay for the ad when playing a lv

                        setAdParameters(silverPotionScript, 1);//uses gold potion script to design the ad and also makes the ad show
                        watchAdButton.onClick.RemoveAllListeners();
                        watchAdButton.onClick.AddListener(addSilvPotClickerMethod);

                        yield return new WaitForSeconds(30f); //how long the ad is shown for
                        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                    }

                }
                else if(cameraObj.enemyStatsObj.eventMode == true)
                {
                    whitePotionScript.potionStart();

                    if (whitePotionScript.adWatchableBool == true)
                    {
                        yield return new WaitForSeconds(3f); //delay for the ad when playing a lv

                        setAdParameters(whitePotionScript, 4);//uses gold potion script to design the ad and also makes the ad show
                        watchAdButton.onClick.RemoveAllListeners();
                        watchAdButton.onClick.AddListener(addEventPotClickerMethod);

                        yield return new WaitForSeconds(30f);
                        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
                    }
                }
            }

            yield return new WaitForSeconds(120f);

        }
    }

    public void setAdParameters(BrewPotion potionScript, int spriteInt)
    {
        potionScript.potionStart(); //updates all the potions info

        potionScript.setDescription(); //updates the brew potion with the most current discription
        potionImage.sprite = potionSprites[spriteInt];//this is the sprite of the actual ad
        potionDesc.text = potionScript.potionDescription.text;
        potionAdPopUp.SetActive(true); //the advert will become active
        //print("gold pot step 3!");

    }

    public void addGoldPotClickerMethod()
    {
        goldPotionScript.showUnityAdGoldPotion();
        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
    }

    public void addSilvPotClickerMethod()
    {
        silverPotionScript.showUnityAdSilverPotion();
        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
    }

    public void addOrangePotClickerMethod()
    {
        orangePotionScript.showUnityAdOrangePotion();
        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
    }

    public void addPurpPotClickerMethod()
    {
        purplePotionScript.showUnityAdPurplePotion();
        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
    }

    public void addEventPotClickerMethod()
    {
        whitePotionScript.showUnityAdWhitePotion();
        potionAdPopUp.GetComponent<Animator>().SetTrigger("HidePopUp");
    }
}
