﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelUpgrade : MonoBehaviour {

	public double _levelCost; //this is what the playere sees when he is unlocking a lev

	public GameObject _levelCostButton; //this is what the player sees when he is scrolling through the levels
    public GameObject youAreHere; //this is what the player sees when he is scrolling through the levels

    public TextMeshProUGUI levelCostButtonText;

    public int levelIndex; //this is for loading the correct scenery values,level prices, etc

    public int levelNumber;
	public int levelUnlocked; //0 is locked, 1 is unlocked

	public RawImage Bkg;
	public RawImage Frg;
	public RawImage Path;

	public TextMeshProUGUI levelCost;
	public TextMeshProUGUI levelNumberText;

	public GameObject lockObject;
	public EnemyStatsInheriter enemyStatsScript;

	public GameObject bossIcon;

    public bool isEvent = false;
    public bool isDungeon = false;
    public bool bonusDungeon = false; //tells checklevelcomplete if this is a bonus chage

    public TextMeshProUGUI DungeonText; //for the window when you ask to go to the dungon or not (will say go to dungeon or bonus dungeon)
    public GameObject goToDungeonWindow;


	void Start () {
       // print("lebels comp is : " + PlayerPrefs.GetInt("EventLevel"));

    }

    void OnEnable(){

		if (enemyStatsScript == null) {
			enemyStatsScript = Camera.main.GetComponent<MainMenu> ().enemyStatsObj;
		}

        if (isDungeon == false) {

            if (isEvent == false) //so the event textures dont get changed on the level selct screen
            {
                updateLevelName();
            }
            updateLevelCosts();

            youAreHere.SetActive(false);//just so they are all still inactive

            if (levelNumber == PlayerPrefs.GetInt("CurrentLevel") && enemyStatsScript.dungeonMode == false && enemyStatsScript.eventMode == false)
            {
                youAreHere.SetActive(true);
            }

            if (enemyStatsScript.eventMode == true && isEvent==true)
            {
                youAreHere.SetActive(true);

            }

            if (isEvent == true)
            {


                if (PlayerPrefs.GetInt("EventLevel") == 10)
                {
                    levelNumberText.text = "Event Completed"; //the +1 is because this will start at 0
                }
                else if (PlayerPrefs.GetInt("EventComplete") == 1)
                {
                    levelNumberText.text = "Event Ended"; //the +1 is because this will start at 0
                }
                else
                {
                    levelNumberText.text = "Temple Event " + (PlayerPrefs.GetInt("EventLevel") + 1).ToString() + "/10"; //the +1 is because this will start at 0

                }




            }
            //dont need an else statement because they are disabled by default
        }

        if (isDungeon == true && bonusDungeon == false && enemyStatsScript.dungeonMode == true && enemyStatsScript.dungeonModeBonus == false) //you are in the first dungeon
        {
            youAreHere.SetActive(true);

        }

        if (isDungeon == true && bonusDungeon == true && enemyStatsScript.dungeonModeBonus == true) //you are in the first dungeon
        {
            youAreHere.SetActive(true);

        }

        

    }

	public void updateLevelName(){

        if (levelNumber != MainMenu.LVCAP)
        {
            levelNumberText.text = "World " + levelNumber;
        }
        else
        {
            levelNumberText.text = "Coming Soon! ";
        }
            //levelindex was levelnumber-1
		Bkg.texture = enemyStatsScript.backgroundsList [(int)(double) enemyStatsScript.data[levelIndex]["BKG"]].texture;
		Frg.texture = enemyStatsScript.foregroundList [(int)(double) enemyStatsScript.data[levelIndex]["FRG"]].texture;

		Path.texture = enemyStatsScript.pathList [(int)(double) enemyStatsScript.data[levelIndex]["PATH"]].texture;

	//	print("the index of BKG is " + Bkg.transform.GetSiblingIndex ()); // 0
	//	print("the index of FRG is " + Frg.transform.GetSiblingIndex ()); // 1
	//	print("the index of Path is " + Path.transform.GetSiblingIndex ()); // 2


		if ((int)(double)enemyStatsScript.data [levelIndex] ["PATH"] == 6 || (int)(double)enemyStatsScript.data [levelIndex] ["PATH"] == 5) {
			Path.transform.SetSiblingIndex (1);
		} else if(Path.transform.GetSiblingIndex()!=2){
			//Path.transform.SetSiblingIndex (2);
		}

		if(levelCostButtonText!=null){
		levelCostButtonText.text = LargeNumConverter.setDmg (_levelCost).ToString (); //test if this works
		}
		//get the path texture number and rearrange based on that



		if (PlayerPrefs.GetInt ("Boss" + (levelNumber-1).ToString ()) == 0 && levelNumber!=1) { //checks if the boss has not been completed and we are not on lv1

			bossIcon.SetActive (true);
		} else {
			bossIcon.SetActive (false);
		}
	}

	public void updateLevelCosts(){

		//have level 1 be defauly unlocked
		if (levelNumber == 1) {
		//	PlayerPrefs.SetInt (this.gameObject.name, 1);
			levelUnlocked = 1;
		}

//		if (levelUnlocked == 1 || PlayerPrefs.GetInt (this.gameObject.name)==1) { //lv is unlcokd
//			lockObject.SetActive (false);
//		} else {
//	//		lockObject.SetActive (true);
//		}

		if (levelUnlocked == 1) { //lv is unlcokd

			lockObject.SetActive (false);
			if (_levelCostButton != null) {
				_levelCostButton.SetActive (false);
			}
		} else {
				lockObject.SetActive (true);
                _levelCostButton.SetActive(true);

        }



    }

	public void purchaseLevel(){

		ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<CheckLevelsComplete> ().playerObj.GetComponent<ControlPlayer1Character> ();

		CheckLevelsComplete lvCompObj = this.gameObject.GetComponentInParent<CheckLevelsComplete> ();

		if (playerObj.playerGold >= _levelCost) { //0 means this is unlocked

		//	print ("level unlocked");
		
			playerObj.playerGold -= _levelCost;

			playerObj.playerGoldText.text = LargeNumConverter.setDmg (playerObj.playerGold).ToString ();

			levelUnlocked = 1; //this means the lv is now unlocked, i think this is so the level can be loaded?

			///
			lvCompObj.updateLevelsUnlocked (); //this updates the completed level arrays
			/// 

//			lvCompObj.updateLevelAvailability (); //this goes through the array and assigns the new level numbers

//			this.gameObject.GetComponentInParent<CheckLevelsComplete> ().unlockLevelsGold.text = LargeNumConverter.setDmg (playerObj.playerGold).ToString (); //updates the players god...
			PlayerPrefs.SetString ("Gold", playerObj.playerGold.ToString ()); //this converts the Double value to a string so we can store larger numbers

//			lvCompObj.updateLevelCost (); //this will update the costs of the levels and apply a lock if needed.
//
//			lvCompObj.updateAllNames ();

			//FOR THE ANIMATION TESTING THIS WAS DISABLKED DATE: 051818
			//Camera.main.GetComponent<MainMenu> ().loadLevel (levelNumber); //loads the next lev
			this.gameObject.GetComponentInParent<CheckLevelsComplete> ().closeUnlockLevel();

			if (this.gameObject.name == "Level2") {
				this.gameObject.GetComponentInParent<Animator> ().SetTrigger ("UnlockLv2");
			} 

			if(this.gameObject.name == "Level3"){
				this.gameObject.GetComponentInParent<Animator> ().SetTrigger ("UnlockLv3");
			}
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().unlockLevelSfx ();


		} else {
			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Not Enough Gold!");
		}
			
	}

	public void openLevelPurchase(){ //if the level is not unlocked open the unlock level window

	//	print ("level unlocked is: " + levelUnlocked);
	//	print ("the current level is: " + PlayerPrefs.GetInt ("CurrentLevel"));
	//	print ("the level number is: " + levelNumber);
		if (levelNumber < MainMenu.LVCAP) {

			if (levelUnlocked == 0 && levelNumber == PlayerPrefs.GetInt ("CurrentLevel") + 1) { //check if the level before it is

				ControlPlayer1Character playerObj = this.gameObject.GetComponentInParent<CheckLevelsComplete> ().playerObj.GetComponent<ControlPlayer1Character> ();

				if (playerObj.playerGold >= _levelCost) {
					this.gameObject.GetComponentInParent<CheckLevelsComplete> ().openUnlockLevels (this.gameObject);

					Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().openLevelUnlock ();

				} else {
					Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Not Enough Gold!");

				}


				if (this.gameObject.GetComponentInParent<CheckLevelsComplete> ().cameraObj.TutorialObject.unlockLevelTut == true) {

					this.gameObject.GetComponentInParent<CheckLevelsComplete> ().cameraObj.TutorialObject.refreshAllVariables (); //this should end the current tutorial

				}


			} else if (PlayerPrefs.GetInt ("CurrentLevel") != levelNumber && levelUnlocked == 1) {

                    Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
                    Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = false;
                    Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false;

                    Camera.main.GetComponent<MainMenu> ().loadLevel (levelNumber); //loads the next lev
			
			} else if (PlayerPrefs.GetInt("CurrentLevel") == levelNumber && levelUnlocked == 1 && enemyStatsScript.dungeonMode==true || PlayerPrefs.GetInt("CurrentLevel") == levelNumber && levelUnlocked == 1 && enemyStatsScript.eventMode == true) //so you can load a level in the campaign when u in the dungeon
            {
                Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
                Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = false;
                Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false;

                Camera.main.GetComponent<MainMenu>().loadLevel(levelNumber); //loads the next lev
            }
            else if( levelNumber> PlayerPrefs.GetInt("CurrentLevel") + 1)
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("You must go to World " + (PlayerPrefs.GetInt("CurrentLevel") + 1).ToString() + " to unlock World " + (PlayerPrefs.GetInt("CurrentLevel") + 2).ToString());
            }
            else
            {

            }

		}

	}

    public void openDungeon()
    { //if the level is not unlocked open the unlock level window

        this.gameObject.GetComponentInParent<CheckLevelsComplete>().openDungeonPage(bonusDungeon);//if its a bonus dungeon if will be set to true from the inspector

    }

    public void openEvent()
    { //if the level is not unlocked open the unlock level window

        this.gameObject.GetComponentInParent<CheckLevelsComplete>().openEventPage();//if its a bonus dungeon if will be set to true from the inspector

    }

    public void closeGoToDungeon()
    {
        goToDungeonWindow.SetActive(true);

    }
}