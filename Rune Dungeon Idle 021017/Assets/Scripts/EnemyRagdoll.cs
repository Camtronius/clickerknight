﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.Analytics.Experimental;


public class EnemyRagdoll : MonoBehaviour {

	public bool isBoss = false;
	// Use this for initialization
	public GameObject Head;
	public GameObject Torso;
		public GameObject Pelvis; //if the pelvis exists

	public float gravityScale = 0.2f;

	public GameObject UpperLeftArm;
	public GameObject LowerLeftArm;
	public GameObject LeftHand;

	public GameObject LeftLegUpper;
	public GameObject LeftLegLower;
	public GameObject LeftFoot;

	public GameObject UpperRightArm;
	public GameObject LowerRightArm;
	public GameObject RightHand;

	public GameObject RightLegUpper;
	public GameObject RightLegLower;
	public GameObject RightFoot;

	public GameObject miscPart1;
	public GameObject miscPart2;
	public GameObject miscPart3;
	public GameObject miscPart4;
	public GameObject miscPart5;
	public GameObject miscPart6;
	public GameObject miscPart7;
	public GameObject bodyPartForShadow;


	public GameObject Sword;

	public GameObject Cape;

	public bool healthRecordable=false;
	public bool stunCasted=false;
	public bool isDown=false;

	public bool StartCountDown=false; //this starts a 10 second timer to make sure the body isnt down too long


	public float elapsedTimeDown = 0;
	public float sloMoTimer = 1f;

	//this is to show the player is burned after the fire spell
	public GameObject FlameDOT;

	public List <GameObject> partsList = new List<GameObject>();

	public GameObject itemObject;
	public GameObject itemDropped;

	public bool moveOffScreenBool = false;
	public float lerpTimer=0;

	public SoundManager soundManager;

	public GameObject rockExplosion;

	public GameObject VeryLargeExplosion;
	public GameObject ExplosiveForce;
	public GameObject ScreenShaker;

	public Inventory InventoryScript;
	public GameObject goldRewarded;
	public GameObject goldRewardedExtra;

	public double goldReward;

	public ControlEnemy ControlEnemyScript;
	public EnemyStatsInheriter enemyStatsObj;

    public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>();

	public CircleShadow shadowObj;
	public MainMenu CameraMainMenu;

    public GameObject petRewarded; //only used for pet
    public GameObject eventSliderProgressBar;

    void Start () {

	//	data = CSVReader.Read ("RuneIdleClicker");
		CameraMainMenu = Camera.main.GetComponent<MainMenu>();

		InventoryScript = CameraMainMenu.inventoryWindow.GetComponentInChildren<Inventory> ();

		ControlEnemyScript = this.GetComponent<ControlEnemy> ();
		enemyStatsObj = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ();

		partsList.Add (Head);
		partsList.Add (Torso);

		if (Pelvis != null) {
			partsList.Add (Pelvis);
		}

		if (Sword != null) {
			partsList.Add (Sword);
		}

		partsList.Add (UpperLeftArm);
		partsList.Add (LowerLeftArm);
		partsList.Add (LeftHand);

		partsList.Add (LeftLegUpper);
		partsList.Add (LeftLegLower);
		partsList.Add (LeftFoot);

		partsList.Add (UpperRightArm);
		partsList.Add (LowerRightArm);
		partsList.Add (RightHand);

		partsList.Add (RightLegUpper);
		partsList.Add (RightLegLower);
		partsList.Add (RightFoot);

		partsList.Add (miscPart1);
		partsList.Add (miscPart2);
		partsList.Add (miscPart3);
		partsList.Add (miscPart4);
		partsList.Add (miscPart5);
		partsList.Add (miscPart6);
		partsList.Add (miscPart7);



		if(Cape!=null && this.gameObject.name != "Chest" && this.gameObject.name != "EventChest")
        {
		Cape.gameObject.GetComponent<Renderer>().sortingOrder = -50; //was -10 then -4
		}

		for(int i = 0; i<partsList.Count;i++){ //doing this so i dont have to redo all the orders for every character.
		
			if (partsList [i] != null && this.gameObject.name != "Chest" && this.gameObject.name != "EventChest") {
				partsList [i].GetComponent<SpriteRenderer> ().sortingOrder = partsList [i].GetComponent<SpriteRenderer> ().sortingOrder - 20;
			}
		}

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		ScreenShaker = GameObject.Find ("ScreenShakerController");
	}

	public void createRagDoll(){

		if (PlayerPrefs.GetInt ("ClickToKillTutorial") == 0) {

			CameraMainMenu.TutorialObject.wizardSprite.sprite = CameraMainMenu.TutorialObject.wizardSprites [0];

			CameraMainMenu.TutorialObject.clickOnMonsterTut = false; //ends the monster tutorial
			CameraMainMenu.TutorialObject.explainGoldExp = true;

			CameraMainMenu.persistantSoundManager.GetComponent<SoundManager> ().oldManHappy ();



			CameraMainMenu.TutorialObject.secondThingToSay ="Great work! Lets use our gold to upgrade our <color=#dd480a>Weapon</color>!";
			CameraMainMenu.TutorialObject.goldExpExplanation(); //hoping this closes the window...

			PlayerPrefs.SetInt ("ClickToKillTutorial",1); //so it doesnt explain this twice

		}


		if (this.gameObject.name != "Chest" && this.gameObject.name!="Egg") {

			shadowObj.enemyDead = true;


			InventoryScript.totalMods (); //just to make sure the mods are added together before enemy is spawned

			goldReward=goldReward * (1+InventoryScript.totalListOfAttributes [3]/100) ; 

			MainMenu CameraScript = CameraMainMenu;

			if (isBoss == false) {
				for (int p = 0; p < Random.Range (1, 4); p++) { //randomly spawns 1-3 golds // add + some variable for the additional gold drop

					GameObject	_goldRewarded = (GameObject)Instantiate (goldRewarded, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen

					if (CameraScript.goldCritUnlocked == true && Random.Range (1, 100)<CameraScript.goldCritChanceIncrease*5) {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward*(1.5f+CameraScript.goldCritChanceIncreasePrestige));
						//and activate some gold crit thingy

					} else {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward);

					}

					_goldRewarded.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-25, 25) / 10f, Random.Range (5, 9));

				}

				if (CameraScript.extraGemUnlocked == 1 && Random.Range (1, 100)<CameraScript.extraGemChance*10) { //25% chance an extra  gem is spawned

						GameObject	_goldRewarded = (GameObject)Instantiate (goldRewardedExtra, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen
						
					if (CameraScript.goldCritUnlocked == true && Random.Range (1, 100)<CameraScript.goldCritChanceIncrease*5) {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward*(1.5f+CameraScript.goldCritChanceIncreasePrestige));
							//and activate some gold crit thingy

						} else {
							_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward);

						}

					_goldRewarded.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-25, 25) / 10f, Random.Range (5, 9));
				//	soundManager.GiveExtraCoin ();
					}

			} else {


				if (PlayerPrefs.GetInt ("FoughtFirstBoss")==0) {

					//boss has been killed, que prestige explanation
					CameraMainMenu.TutorialObject.refreshAllVariables ();//resets everything so we can restart the tutorial. Tutpart one will enable the obj

					CameraMainMenu.TutorialObject.prestigeTutorial = true;
					CameraMainMenu.TutorialObject.twoPartTutorial = true;

					CameraMainMenu.TutorialObject.wizardSprite.sprite = CameraMainMenu.TutorialObject.wizardSprites [1]; //laughing

					CameraMainMenu.TutorialObject.disableButtons (CameraMainMenu.TutorialObject.buttonList[8]);

					CameraMainMenu.persistantSoundManager.GetComponent<SoundManager> ().oldManSurprised ();


					CameraMainMenu.TutorialObject.TutPartOne ("<color=#ffc119>Oh Wow</color>! You defeated a Boss Guardian!");
					CameraMainMenu.TutorialObject.secondThingToSay = "Defeat Bosses to get part of an Ascend Point. <color=#B450D7FF>Earn 2 Ascend Points to Ascend!</color>";

					PlayerPrefs.SetInt ("FoughtFirstBoss", 1);

					AnalyticsEvent.Custom("BossDefeated");



//					if (Camera.main.GetComponent<MainMenu> ().playerObj.lvUpRaysGfx.activeSelf == true) { //get rid of lvup notification, or make it so boss 1 gives 1 less than max exp
//
//						Camera.main.GetComponent<MainMenu> ().playerObj.lvUpRaysGfx.GetComponent<LevelUpCountdwn> ().closeTime = 0; //this cleanly closes it
//					}

				}

				//add wizard surprised face






				for (int p = 0; p <6; p++) { //spawns 5 golds (this is for the bosses)

					GameObject	_goldRewarded = (GameObject)Instantiate (goldRewarded, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen

					if (CameraScript.goldCritUnlocked == true && Random.Range (1, 100)<CameraScript.goldCritChanceIncrease*5) {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward*(1.5f+CameraScript.goldCritChanceIncreasePrestige));
						//and activate some gold crit thingy

					} else {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward);

					}					
					_goldRewarded.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-25, 25) / 10f, Random.Range (5, 9));

				}

				if (CameraScript.extraGemUnlocked == 1 && Random.Range (1, 100)<CameraScript.extraGemChance*10) { //25% chance an extra  gem is spawned

					GameObject	_goldRewarded = (GameObject)Instantiate (goldRewardedExtra, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen

					if (CameraScript.goldCritUnlocked == true && Random.Range (1, 100)<CameraScript.goldCritChanceIncrease*5) {
						_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward*(1.5f+CameraScript.goldCritChanceIncreasePrestige));
								//and activate some gold crit thingy

							} else {
								_goldRewarded.GetComponent<DroppedGoldScript> ().setGold (goldReward);

							}
					_goldRewarded.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-25, 25) / 10f, Random.Range (5, 9));
				//	soundManager.GiveExtraCoin ();

						}

                //BOSS DEATH MEMORY
                PlayerPrefs.SetInt("DefeatedAtBoss", 0);//so we do not continue to show the player Silver Damage Potion ads when they have defeated a boss. This is set to =1 in the MainMenu, at loadpreviouslevel

                PlayerPrefs.SetInt ("Boss" + (enemyStatsObj.CurrentLevel-1).ToString(),1); //this means the boss has been beaten
                                                                                           //	print("the boss has been killed and the curerent level is : " +  (enemyStatsObj.CurrentLevel-1).ToString() + " the boss int is 0/1 = " + PlayerPrefs.GetInt ("Boss" + (enemyStatsObj.CurrentLevel-1).ToString()));

                if (enemyStatsObj.dungeonMode == false)
                {
                    CameraMainMenu.prestPointsAmt.updatePrestigeCounter();
                }
                else //dungeon mode is true
                {

                    if (Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.enemyNumber < 3)
                    {
                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.setDungeonCompleteText(Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.enemyNumber.ToString() + "/3");

                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.gameObject.SetActive(true);

                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.GetComponent<Animator>().SetTrigger("MonsterCount");
                    }
                    else
                    {
                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.setDungeonCompleteText("Dungeon Complete!");

                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.gameObject.SetActive(true);

                        Camera.main.GetComponent<MainMenu>().dungeonCelebration.GetComponent<Animator>().SetTrigger("DungeonComplete");

                    }


                }
			}


		}

        if(this.gameObject.name == "Egg")
        {
            //run the script for generating the pet
            //get the sprite and apply it to petrewarded
            //open the pet page afterwards

            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

                GameObject _petRewarded = (GameObject)Instantiate(petRewarded, new Vector3(Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen

                Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().generatedPetObj = _petRewarded;

                Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().giveRandomPet();

                _petRewarded.GetComponentInChildren<SpriteRenderer>().sprite = Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().petSprite.sprite;
            //set Pet

                _petRewarded.GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f, 7);

            Camera.main.GetComponent<MainMenu>().windowOpen = true;//to prevent clicking attacking/spells

            //something will set the pet page open after this
            if (enemyStatsObj.dungeonModeBonus == false)
            {
                if (PlayerPrefs.GetInt("DungeonComp") == 0)
                {
                    PlayerPrefs.SetInt("DungeonComp", 1);
                }

            }

            else

            {
                if (PlayerPrefs.GetInt("BonusDungeonComp") == 0)
                {
                    PlayerPrefs.SetInt("BonusDungeonComp", 1);
                }

            }

                this.enabled = false; //disable the enemy ragdoll script so it doesnt give another pet!
                this.gameObject.GetComponent<ControlEnemy>().isHittable = false;

        }

        if (this.gameObject.name == "EventChest")
        {
            //run the script for generating the pet
            //get the sprite and apply it to petrewarded
            //open the pet page afterwards

            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

            eventChestExplosion();//this will make the chest open and show the Orb

            //GameObject _petRewarded = (GameObject)Instantiate(petRewarded, new Vector3(Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity); //instantiates the beam offscreen

            //Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().generatedPetObj = _petRewarded;

            //Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().giveRandomPet();

            //_petRewarded.GetComponentInChildren<SpriteRenderer>().sprite = Camera.main.GetComponent<MainMenu>().PetGenerator.GetComponent<PetGenerator>().petSprite.sprite;
            //set Pet

            //_petRewarded.GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f, 7);

            Camera.main.GetComponent<MainMenu>().windowOpen = true;//to prevent clicking attacking/spells

            //something will set the pet page open after this
            if (enemyStatsObj.eventMode == true)
            {
                //this used to give the event mode point,. but now its given in the coroutine
                
            }


            this.enabled = false; //disable the enemy ragdoll script so it doesnt give another pet!
            this.gameObject.GetComponent<ControlEnemy>().isHittable = false;

            //instantiate the event slider progress bar

            Vector3 barPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height-Screen.height/5.5f, 0));


            GameObject sliderBar = (GameObject)Instantiate(eventSliderProgressBar, new Vector3(barPos.x, barPos.y, 89), Quaternion.identity); //instantiates the beam offscreen


            GameObject canvasObj = GameObject.Find("CanvasItemDescrip");

            sliderBar.transform.parent = canvasObj.transform;

            sliderBar.GetComponent<Animator>().enabled = true;

}
        //end event chest 



        if (this.gameObject.GetComponentInParent<ControlPlayer1Character> ()!= null &&
		    this.gameObject.GetComponentInParent<ControlPlayer1Character> ().isShielded == true) {

			this.enabled = false;

		}
		/*
		if (this.gameObject.GetComponentInParent<ControlPlayer2Character> ()!= null &&
		    this.gameObject.GetComponentInParent<ControlPlayer2Character> ().isShielded == true) {
			
			this.enabled = false;
			
		}
		*/
		if (this.gameObject.tag =="Enemy" ) { //for skeleton

			this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			this.gameObject.GetComponent<Animator> ().enabled = false;
		
			if(this.gameObject.name == "Skele"){

				createExplosion ();
				SkeleDeath();
			
				skeleDeathSound();

			StartCoroutine ("detectStillBody");
			StartCoroutine ("detectDownDelay");

			}

			if(this.gameObject.name == "SkeleMage"){

				createExplosion ();
				SkeleDeath();

				skeleMageDeathSound();

				StartCoroutine ("detectStillBody");
				StartCoroutine ("detectDownDelay");

			}

			if(this.gameObject.name == "SkeleWarrior"){

				createExplosion ();
				SkeleDeath();

				skeleWarriorDeathSound();

				StartCoroutine ("detectStillBody");
				StartCoroutine ("detectDownDelay");

			}

			if(this.gameObject.name == "Slime"){

				createExplosion ();
				WolfDeath();

				slimeDeathSound();
				
				StartCoroutine ("detectStillBody");
				StartCoroutine ("detectDownDelay");
				
			}

			if(this.gameObject.name == "Mush"){

				createExplosion ();
				WolfDeath();

				mushroomDeathSound();

				StartCoroutine ("detectStillBody");
				StartCoroutine ("detectDownDelay");

			}

			if(this.gameObject.name == "Golem"){

				createExplosion ();
				GolemDeath();
				
			//	WolfDeathSound();
				
				StartCoroutine ("detectStillBody");
				StartCoroutine ("detectDownDelay");
				
			}

			if(this.gameObject.name == "Werewolf"){

				createExplosion ();
				PlayerEnemyDeath(); //this is the ragdoll for the reptile and evilguy
				werewolfDeathSound();
			}

			if(this.gameObject.name == "Chest"){
			//	ChestDeath ();
				chestExplosion ();

		//		Destroy(this.gameObject); //for now since an animation doesnt exist
			//	this.gameObject.SetActive(false);
			}

		}

		if (this.gameObject.tag == "Player2" || this.gameObject.name == "Reptillian" || this.gameObject.name =="PurpleReptillian") { //for skeleton
		
			if(this.gameObject.name!="FinalBoss" && this.gameObject.name!="FinalBossAct2"){

				PlayerEnemyDeath(); //this is the ragdoll for the reptile and evilguy

				//this is for sounds
				if(this.gameObject.name != "PurpleReptillian" && this.gameObject.name != "Reptillian"){ //check if they are not reptils
				
					EvilGuyDeath();//this is for the death sound
				}else{
					createExplosion ();

					soundManager.reptileDeath();
				}
				/*
				if(this.gameObject.name == "Reptillian"){
					soundManager.ReptileDeath();


				}
				*/


		//		print ("the player based enemy rag doll has been called");
			}else{
				//this is if the enemy is the final boss
				if(this.gameObject.name == "FinalBoss"){
				this.gameObject.GetComponent<Animator>().SetBool("FinalBossDeadStart",true);
				GameObject.Find("TutorialCampaign").GetComponent<TutorialCampaign>().TurnBackNowDialogue2();
				}

				if(this.gameObject.name == "FinalBossAct2"){
					this.gameObject.GetComponent<Animator>().SetBool("StartDeathFinalBoss",true);
					GameObject.Find("TutorialCampaign").GetComponent<TutorialCampaign>().FinalBossAct2FirstDeath(); //this is gonna b the cant be happening w/e dialogue
				}

			}
		}

        if (CameraMainMenu.soulChargeButton.activeSelf == true)//decreasing soul charge cooldown
        {
            CameraMainMenu.soulChargeButton.GetComponentInChildren<SoulChargeCooldown>().decreaseFreezeCooldown();
        }
	}

	public void chestExplosion(){


		soundManager.EarthWoodBreak ();
		Sword.SetActive (false); //disables the top of the chest
		Cape.SetActive (true); //enables the top of the chest open sprite
		Pelvis.SetActive (true); //enables the chest FX

		//this was openitem chest but it lags a lot
		CameraMainMenu.chestHolder = this.gameObject;
		CameraMainMenu.OpenItemChest ();

	}

    public void eventChestExplosion()
    {


        soundManager.eventChestOpen();
        Sword.SetActive(false); //disables the top of the chest
        Cape.SetActive(true); //enables the top of the chest open sprite
        Pelvis.SetActive(true); //enables the chest FX
        miscPart1.SetActive(true);
       // print("stone chest open");
        //this was openitem chest but it lags a lot
        //CameraMainMenu.chestHolder = this.gameObject;
        //CameraMainMenu.OpenItemChest();

    }

    public void skeleDeathSound(){

		soundManager.SkeleDeath ();


	}

	public void skeleMageDeathSound(){

		soundManager.skeleMageDeath ();


	}

	public void skeleWarriorDeathSound(){

		soundManager.skeleWarriorDeath ();


	}

	public void WolfDeathSound(){
		
		soundManager.WolfDeath ();
		
		
	}

	public void slimeDeathSound(){

		soundManager.slimeDeath ();
	}

	public void mushroomDeathSound(){

		soundManager.mushroomDeath ();

	}


	public void werewolfDeathSound(){

		soundManager.werewolfDeath ();

	}

	public void EvilGuyDeath(){
		
		soundManager.EvilGuyDeath ();
		
		
	}

	public void BoneDropSound(){
		
		soundManager.BoneThud ();

	}



	// Update is called once per frame
	void Update () {

		if (ControlEnemyScript.playerHealthNew <= 0 && ControlEnemyScript.playerDead == false) {
		
			ControlEnemyScript.playerDead = true;

			if (CameraMainMenu.woundBlood.transform.parent = this.gameObject.transform) {

				ParticleSystem woundParticles = CameraMainMenu.woundBlood.GetComponent<ParticleSystem> ();
				ParticleSystem.EmissionModule emis1 = woundParticles.emission;
				emis1.enabled = false;

				CameraMainMenu.woundBlood.transform.parent = null;


			}

			createRagDoll ();
		}

		if(elapsedTimeDown > 0f && StartCountDown==true){

			elapsedTimeDown-=Time.deltaTime;

		}

//
//		if (this.gameObject.GetComponent<ControlEnemy> () != null 
//		    && this.gameObject.GetComponent<ControlEnemy>().playerDead==true) {
//
//			if(sloMoTimer>0){
//				sloMoTimer-=Time.deltaTime;
//			Time.timeScale = 0.4f;
//			}else{
//
//				Time.timeScale=1.0f;
//			
//			}
//		}

		if (moveOffScreenBool == true) {


			if(this.gameObject.name!="FinalBossAct2"){
			for(int i=0; i<partsList.Count; i++){

				if(partsList[i]!=null && partsList[i].gameObject.GetComponent<SpriteRenderer>().color.a > 0){

					partsList[i].gameObject.GetComponent<SpriteRenderer>().color -= new Color(0,0,0,.02f);

					if(partsList[i].gameObject.GetComponent<SpriteRenderer>().color.a <=0.01){

						this.gameObject.SetActive(false);
					//	Destroy(this.gameObject); //add this back if a difference isnt noticable
					}
				}


			}
			}
			//
		}
	
	}

	public void createExplosion(){

		GameObject explosionForce = Instantiate (ExplosiveForce, new Vector3 (Torso.transform.position.x, RightLegLower.transform.position.y, 92), Quaternion.identity) as GameObject;

	}

	public void SkeleDeath(){

		Head.GetComponent<BoxCollider2D> ().enabled = true;
		Head.GetComponent<HingeJoint2D> ().enabled = false; //makes it so limbs detach
		Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		Torso.GetComponent<BoxCollider2D> ().enabled = true;
		Torso.GetComponent<HingeJoint2D> ().enabled = false;
		Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperLeftArm.GetComponent<HingeJoint2D> ().enabled = false;
		UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LowerLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
		LowerLeftArm.GetComponent<HingeJoint2D> ().enabled = false;
		LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LeftLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegUpper.GetComponent<HingeJoint2D> ().enabled = false;
		LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LeftLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegLower.GetComponent<HingeJoint2D> ().enabled = false;
		LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		//	LeftFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperRightArm.GetComponent<HingeJoint2D> ().enabled = false;
		UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LowerRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		LowerRightArm.GetComponent<HingeJoint2D> ().enabled = false;
		LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		RightLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegUpper.GetComponent<HingeJoint2D> ().enabled = false;
		RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		RightLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegLower.GetComponent<HingeJoint2D> ().enabled = false;
		RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		//	RightFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	RightFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;


	}

	public void FinalBossAct2Explode(){



		this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		this.gameObject.GetComponent<Animator> ().enabled = false;

		Head.GetComponent<BoxCollider2D> ().enabled = true;
		Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		Torso.GetComponent<BoxCollider2D> ().enabled = true;
		Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		//LowerLeftArm.GetComponent<BoxCollider2D> ().enabled = true; //its messing up for some reason
		LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LeftLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		LeftLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		//	LeftFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LowerRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		RightLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		RightLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		VeryLargeExplosion = Instantiate (VeryLargeExplosion, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 0), Quaternion.identity) as GameObject;


		GameObject explosionForce = Instantiate (ExplosiveForce, new Vector3 (Torso.transform.position.x, Torso.transform.position.y, 89), Quaternion.identity) as GameObject;

		explosionForce.GetComponent<ExplosiveForce> ().explosionMultiplier = 500;

		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2f,0.4f); //shakes the screen

		StartCoroutine ("detectStillBody");
		StartCoroutine ("detectDownDelay");
		
	}

	public void WolfDeath(){

		for (int i =0; i<partsList.Count; i++) {

			if(partsList[i]!=null){

				if(partsList[i].GetComponent<BoxCollider2D>()!=null){

					partsList[i].GetComponent<BoxCollider2D> ().enabled = true;

				}

				if(partsList[i].GetComponent<BoxCollider2D>()!=null){

					partsList[i].GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

				}

				if(partsList[i].GetComponent<HingeJoint2D>()!=null && partsList[i].GetComponent<HingeJoint2D>().enabled==false){
					partsList[i].GetComponent<HingeJoint2D>().enabled=true;
				}

			}


		}


	}

	public void ChestDeath(){
	
	
	}

	public void GolemDeath(){
		
		Head.GetComponent<BoxCollider2D> ().enabled = true;
		Head.GetComponent<HingeJoint2D> ().enabled = false; //makes it so limbs detach
		Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		Torso.GetComponent<BoxCollider2D> ().enabled = true;
		Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LowerLeftArm.GetComponent<BoxCollider2D> ().enabled = true;
		LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LeftLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LeftLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		//	LeftFoot.GetComponent<BoxCollider2D>().enabled = true;
		//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		UpperRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		LowerRightArm.GetComponent<BoxCollider2D> ().enabled = true;
		LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		RightLegUpper.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		
		RightLegLower.GetComponent<BoxCollider2D> ().enabled = true;
		RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		soundManager.GolemDeathSound ();
	
	}

	public void PlayerEnemyDeath(){
		this.gameObject.GetComponent<Animator>().enabled = false;

		this.gameObject.GetComponent<BoxCollider2D> ().enabled = false;

		for (int i =0; i<partsList.Count; i++) {
			
			if(partsList[i]!=null){
				
				if(partsList[i].GetComponent<BoxCollider2D>()!=null){
					
					partsList[i].GetComponent<BoxCollider2D> ().enabled = true;
					
				}
				

				
				if(partsList[i].GetComponent<HingeJoint2D>()!=null && partsList[i].GetComponent<HingeJoint2D>().enabled==false){
					partsList[i].GetComponent<HingeJoint2D>().enabled=true;
				}

				if(partsList[i].GetComponent<BoxCollider2D>()!=null){
					
					partsList[i].GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
					
				}
				
			}
			
			
		}
			StartCoroutine ("detectStillBody");
			StartCoroutine ("detectDownDelay");



	}

	public void redoGravity(){

		Head.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		Torso.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		UpperLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LowerLeftArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LeftLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LeftLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
	//	LeftFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		UpperRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		LowerRightArm.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		RightLegUpper.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
		RightLegLower.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
	//	RightFoot.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

	}

	IEnumerator detectDownDelay(){
		
		while (true) {

			yield return new WaitForSeconds(0.25f);
		
			for(int i=0; i<partsList.Count; i++){ //this is to protect some spells for respanwing the character eearly
				if(partsList[i]!=null && partsList[i].GetComponent<isDownDetector>()!=null){
					
					partsList[i].GetComponent<isDownDetector>().enabled=true;
					
				}
				
			}

			StartCountDown=true; //this starts a 10 second timer to make sure the body isnt down too long

			yield break;

		}
	}

	IEnumerator detectStillBody(){
		
		while (true) {
	
			Vector3 CurrentHeadPosition = Head.transform.position;
		
			yield return new WaitForSeconds(0.25f);

			if(isDown==true || elapsedTimeDown<=0){

				yield return new WaitForSeconds(0.5f);

				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer2Character>().playerHealthCurrent > 0
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null
				   ||this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent > 0 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){

				Cape.GetComponent<SkinnedMeshRenderer>().enabled=false;
				Cape.GetComponent<MeshCollider>().enabled=false;

				this.enabled=false;

				this.gameObject.GetComponent<Animator>().enabled=true;

				this.gameObject.GetComponent<BoxCollider2D>().enabled=true;

			//		SpellCreatorObject.GetComponent<SpellCreator>().resetAllCounts();

				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null && 
				   this.gameObject.GetComponent<ControlPlayer1Character>().player1Stun==true &&
				   stunCasted==false){ //stuncasted checks if the player justrecently casted a stun spell
					//because the player shouldnt come off stun if the player casts a stun spell twice.

					this.gameObject.GetComponent<Animator>().SetBool("IsStunned",false);
					this.gameObject.GetComponent<ControlPlayer1Character>().player1Stun=false;


				//	print ("changed is stunned to true play 1");

					}

				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null &&
				   this.gameObject.GetComponent<ControlPlayer2Character>().player2Stun==true &&
				   stunCasted==false){//stuncasted checks if the player justrecently casted a stun spell
					//because the player shouldnt come off stun if the player casts a stun spell twice.

					this.gameObject.GetComponent<Animator>().SetBool("IsStunned",false);
					this.gameObject.GetComponent<ControlPlayer2Character>().player2Stun=false;

			//		print ("changed is stunned to true play 1");

						
					}

				//WARNING ~&~&~&~&~&~&~&~&~& if this is on it wont send the correct spell to the other player during online mode...
				//SpellCreatorObject.GetComponent<SpellCreator>().sentSpell.Clear();

			//		Cape.GetComponent<SkinnedMeshRenderer>().enabled=true; //not sure why this is here
			//		Cape.GetComponent<MeshCollider>().enabled=true; //not sure why this is here....6/22/16

				//this makes it so when the player responds it shows the fire like he is burned and taking dmg
				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null && 
				   this.gameObject.GetComponent<ControlPlayer1Character>().justBurned==true){ 

					GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

			//		print ("P1 burned after ragdoll");
					
				}

				//this makes it so when the player responds it shows the fire like he is burned and taking dmg
				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null &&
				   this.gameObject.GetComponent<ControlPlayer2Character>().justBurned==true){

					GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (this.transform.position.x, this.transform.position.y, 0), Quaternion.identity);

				//	print ("P2 burned after ragdoll");

				}

				if(this.gameObject.tag=="Player2"){
				this.gameObject.transform.rotation = Quaternion.Euler(0,180,0);
				}
				if(this.gameObject.tag=="Player1"){
					this.gameObject.transform.localScale = new Vector3 (1, 1, 1);

				this.gameObject.transform.rotation = Quaternion.Euler(0,0,0);
				}

				
				
				

				} //this is the end of the detection if the players health is above zero (0)

				//assigns death to a player if they are dead
				if(this.gameObject.GetComponent<ControlPlayer2Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer2Character>().playerHealthCurrent <= 0){

					this.gameObject.GetComponent<ControlPlayer2Character>().playerDead=true;
				}

				if(this.gameObject.GetComponent<ControlPlayer1Character>()!=null 
				   && this.gameObject.GetComponent<ControlPlayer1Character>().playerHealthCurrent <= 0){
					
					this.gameObject.GetComponent<ControlPlayer1Character>().playerDead=true;
				}

				for(int i=0; i<partsList.Count; i++){ //this is to protect some spells for respanwing the character eearly
					if(partsList[i]!=null && partsList[i].GetComponent<isDownDetector>()!=null){
						
						partsList[i].GetComponent<isDownDetector>().enabled=false;
						
					}
					
				}

				

				StartCountDown=false; //stops the countdown
				elapsedTimeDown=5; //resets the time for the countdown; //changed from 10 to 5   062216
				
//				Camera.main.gameObject.GetComponent<CampaignGameOrganizer>().recordHealth(); //will it not record
				//the hp fast enuff before the ragdoll is repapplied?

					// or make the primary element -1?


			//	takeTurnProper();
                if (this.gameObject.name != "Egg" && this.gameObject.name!="EventChest") {
                    MakePlayerTravel();
                }
				

				this.gameObject.GetComponent<ControlEnemy>().StopCoroutine("HealthBarGUI");

				moveOffScreenBool = true; //this will make the enemy fade

				dropItem();

				giveExp();

				/*
				 * //not used
				for(int i=0; i<partsList.Count; i++){

				//	partsList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(-4,0);

				}
				*/


				//this.gameObject.SetActive(false);

				//Destroy(this.gameObject);

				yield break;

			}
		
		}

	}

	public void takeTurnProper(){

//		if (Camera.main.GetComponent<CampaignGameOrganizer> ().nextTurnBool == true) {
//
//	//		Camera.main.GetComponent<CampaignGameOrganizer>().playerInt=1;
//			if(this.gameObject.GetComponentInParent<ControlEnemy>().playerHealthNew >0){ 
//			
//		//	print ("the taketurnproper has been called");
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(false);
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().WhosTurn(true,"null"); //this shows whos  turn it is
//
//			//	print ("enemy health is " + this.gameObject.GetComponentInParent<ControlEnemy>().playerHealthNew);
//			//	print ("enemy health is not parent? " + this.gameObject.GetComponent<ControlEnemy>().playerHealthNew);
//
//			
//			}else if(this.gameObject.GetComponentInParent<ControlEnemy>().playerHealthNew<=0)
//			{
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().sortMeteorList();
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().TakeTurn(true);
//		
//
//			}
//
//		}

	}

	public void MakePlayerTravel(){ //this makes the player travel, spawns the next enemy, and also resets the health bars

		GameObject TravelingObj = GameObject.Find ("EnemySpawn");

        if (enemyStatsObj.dungeonMode == true)
        {

            TravelingObj.GetComponent<CampaignLevelsEnemySpawner>().setMoveTime(-4);
        }
        else
        {

            TravelingObj.GetComponent<CampaignLevelsEnemySpawner>().setMoveTime(0);
        }
		

//		//make a sliding in health bar animation
////		TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyHealthBar.GetComponent<Animator>().SetBool("DisappearHealth",true);
//
//		//itereates through the enemyspawn list by 1 to create the next enemy
//		TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().enemyNumber += 1; //iterates to the next obj in the list
//		TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().nextSpawn ();
//
//
//		TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().enemyDefeated = true; //this makes it so it doesnt make blocks
//		//and move the player again
//
////		if (TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().enemyNumber < 
////		    TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().FinalEnemyNumber) {
//

//
//		//	TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().Player1.GetComponent<Animator> ().SetBool ("isRunning", true);
//
//			TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().playerTraveling = true;

		//}

	}

	public void dropItem(){

//		GameObject TravelingObj = GameObject.Find ("EnemySpawn");
//		GameObject TutorialCampaignObj = GameObject.Find ("TutorialCampaign");
//
//		if (TutorialCampaignObj != null) { //is the tutorial on or not..
//
//			if (TutorialCampaignObj.GetComponent<TutorialCampaign> ().EnemyStatsInheriterObj.CurrentLevel == 1 
//				&& TutorialCampaignObj.GetComponent<TutorialCampaign> ().isTutorial == true) {
//
//				//dont spawn any item...
//
//			} else if (TutorialCampaignObj.GetComponent<TutorialCampaign> ().EnemyStatsInheriterObj.CurrentLevel == 2 
//				&& TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().enemyNumber == 2
//				&& TutorialCampaignObj.GetComponent<TutorialCampaign> ().isTutorial == true) {
//	
//				itemObject.GetComponent<DroppedItem> ().itemForTutorial = true;
//				itemDropped = (GameObject)Instantiate (itemObject, new Vector3 (TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.x, TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.y, 0), Quaternion.identity); //instantiates the beam offscreen
//				itemDropped.GetComponent<DroppedItem> ().goldInside = (this.gameObject.GetComponent<ControlEnemy> ().enemyRank * 25 + Random.Range(1,100));
//				itemDropped.GetComponent<DroppedItem> ().healthInside = this.gameObject.GetComponent<ControlEnemy> ().enemyRank*60; //the -5 is because the luck mod starts out at 5
//
//			} else if (TutorialCampaignObj.GetComponent<TutorialCampaign> ().EnemyStatsInheriterObj.CurrentLevel == 2 
//				&& TutorialCampaignObj.GetComponent<TutorialCampaign> ().isTutorial == true) {
//
//				//dont spawn items
//			} else {
//
//				itemObject.GetComponent<DroppedItem> ().itemForTutorial = false;
//
//				itemDropped = (GameObject)Instantiate (itemObject, new Vector3 (TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.x, TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.y, 0), Quaternion.identity); //instantiates the beam offscreen
//				itemDropped.GetComponent<DroppedItem> ().goldInside = (this.gameObject.GetComponent<ControlEnemy> ().enemyRank * 25 + Random.Range(1,100));
//				itemDropped.GetComponent<DroppedItem> ().healthInside = this.gameObject.GetComponent<ControlEnemy> ().enemyRank*60; //the -5 is because the luck mod starts out at 5
//
//
//			}
//		} else {
//
//			itemDropped = (GameObject)Instantiate (itemObject, new Vector3 (TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.x, TravelingObj.GetComponent<CampaignLevelsEnemySpawner> ().EnemyEndPos.y, 0), Quaternion.identity); //instantiates the beam offscreen
//			itemDropped.GetComponent<DroppedItem> ().goldInside = (this.gameObject.GetComponent<ControlEnemy> ().enemyRank * 25 + Random.Range(1,100));
//			itemDropped.GetComponent<DroppedItem> ().healthInside = this.gameObject.GetComponent<ControlEnemy> ().enemyRank*60; //the -5 is because the luck mod starts out at 5
//
//
//		}
	}

	public void giveExp(){

		ControlPlayer1Character playerScript = Object.FindObjectOfType<ControlPlayer1Character>();
	//	print ("exp gained is " + this.gameObject.GetComponent<ControlEnemy> ().enemyRank + " plus an extra " + (this.gameObject.GetComponent<ControlEnemy> ().enemyRank * (InventoryScript.totalListOfAttributes [4]/100f+CameraMainMenu.potionModList[3]/100f)).ToString ());

		if (isBoss == true && PlayerPrefs.GetInt("CurrentLevel")==2) {//this just needs to be here for the prestige explanation

			playerScript.playerExpNew = playerScript.playerExpMax - 1;

			//print ("the boss died and we are executing this code!");

		} else { 
			//start of exp give
			double expGained = this.gameObject.GetComponent<ControlEnemy> ().enemyRank + (this.gameObject.GetComponent<ControlEnemy> ().enemyRank * (InventoryScript.totalListOfAttributes [4]/100f + CameraMainMenu.potionModList [3]/100f + CameraMainMenu.potionModList[8]/100f + CameraMainMenu.potionModList[13]/100f)); 

			if (ObscuredPrefs.GetInt ("ArcanePack") == 1) {
			
				expGained = 1.25f * expGained;
		
			} else {
			
			}

			playerScript.playerExpNew += expGained;

			CameraMainMenu.expAmount (expGained);
			//end
		
		}

	}

}
