﻿using UnityEngine;
using System.Collections;

public class LightningSpell : MonoBehaviour {

	public GameObject Beam1;
	public GameObject Beam2;
	public GameObject LightningFx;
	public GameObject LightningCollider;
	public float timeAlive = 0;

	//add a timer here to destroy this object after like 5 seconds or something...// check if solar beam needs one too

	// Use this for initialization
	void Start () {
	
	}

	public void disableLightningAndDestroy(){

		Beam1.GetComponent<iceTowerScript> ().lightningAttack = false;
		Beam2.GetComponent<iceTowerScript> ().lightningAttack = false;

		Beam1.GetComponent<LineRenderer> ().enabled = false;
		Beam2.GetComponent<LineRenderer> ().enabled = false;

		Beam1.GetComponent<iceTowerScript> ().StopCoroutine ("ShootLightning");
		Beam2.GetComponent<iceTowerScript> ().StopCoroutine ("ShootLightning");

	}


	void Update () {
	
		timeAlive += Time.deltaTime;

		if (timeAlive > 5) {
			Destroy(this.gameObject);
		}

	}
}
