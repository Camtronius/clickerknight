﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PrestigeHolder : MonoBehaviour {

	// Use this for initialization
	public TextMeshProUGUI PrestigeDescription;
	public TextMeshProUGUI PrestigeStatsGiven;
	public TextMeshProUGUI amtPoints;

	public GameObject prestigeRing;
	public GameObject premiumPrestigeRing;

	public int prestigeTapCount = 0;
	public int premPrestTapCount = 0;
	public int premiumGemPrice;

	public IngredientsMaster ingredMaster;
	public Animator prestigeAnimatorObj;

	public List<TextMeshProUGUI> AscendList = new List<TextMeshProUGUI>();
	public List<TextMeshProUGUI> PremiumAscendList = new List<TextMeshProUGUI>();


	void Start () {
		
	}

	void OnDisable(){

		prestigeTapCount = 0;
		premPrestTapCount = 0;
		prestigeAnimatorObj.SetBool ("ConfirmPremium", false);
		prestigeAnimatorObj.SetBool ("Confirm", false);
		prestigeRing.SetActive(false);
		premiumPrestigeRing.SetActive(false);
	}

	void Update () {
		
	}

	public void tapPrestigeButton(){

		if (PlayerPrefs.GetInt ("PrestigePtsEarned") >= 2) {
		
				prestigeTapCount += 1;

				if (prestigeTapCount < 2) {

					premPrestTapCount = 0;
					prestigeAnimatorObj.SetBool ("ConfirmPremium", false); //resets the other button if it is activated
					premiumPrestigeRing.SetActive (false);

					prestigeAnimatorObj.SetBool ("Confirm", true);

					Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Tap Again to Confirm!");


				} else { //if they confirm

					Camera.main.GetComponent<MainMenu> ().setPrestige (); //actually prestiges the player

				}
		}else{

			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("You need 2 Ascend Points to unlock Ascend");
		}

	}

	public void tapPremiumPrestigeButton(){

		if (PlayerPrefs.GetInt ("PrestigePtsEarned") >= 2) {
			
				if (ingredMaster.totalGreenGems >= premiumGemPrice) {
				
					premPrestTapCount += 1;

					if (premPrestTapCount < 2) {
						
						prestigeTapCount = 0;
						prestigeAnimatorObj.SetBool ("Confirm", false); //resets the other button if it is activated
						prestigeRing.SetActive(false);

						prestigeAnimatorObj.SetBool ("ConfirmPremium", true);

						Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Tap Again to Confirm!");


					} else { //if they confirm the prestige

						ingredMaster.totalGreenGems -= premiumGemPrice; //this subtracts the players gems

						ingredMaster.setNewPrefs (); //this will update the gems

						Camera.main.GetComponent<MainMenu> ().setPremiumPrestige (); //actually prestiges the player

					}

				} else { //if they cannot afford it (not enuf premium)

					Camera.main.GetComponent<MainMenu> ().showErrorMsg ("Not enough Green Gems!");
				}

		}else{

			Camera.main.GetComponent<MainMenu> ().showErrorMsg ("You need 2 Ascend Points to unlock Ascend");
		}

	}
}
