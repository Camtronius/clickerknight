﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class MasterSpellList : MonoBehaviour {

	public List<GameObject> WeaponUnlockAnims = new List<GameObject> ();
	public List<SpellUpgrades> ListOfSpells = new List<SpellUpgrades> ();
	public List<Sprite> SpellSpriteIcons = new List<Sprite> ();
	public int[] unlockedSplArray; //will show 1 if level is unlocked and 0 if it is not unlocked


	public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>();
//	public TextMeshProUGUI gemAmount;
	public GameObject playerObj;
	public MainMenu cameraObj;

    public GameObject multiplierObj;
    public int multiplier = 1;
    public TextMeshProUGUI multiplierText;

    public GameObject spellsDisplay;//this is what is shown if u click on spelll toggle
    public TextMeshProUGUI spellsDisplayText;

    public GameObject abilityDisplay;//this is what is shown if u click on spelll toggle
    public TextMeshProUGUI abilityDisplayText;

    public Toggle spellToggle;
    public Toggle abilityToggle;

    public EnemyStatsInheriter enemyStatsObj;



    // Use this for initialization
    void Start () {

//		loadSpellUpgrades ();

//		updateGems (); //gets the current players gems

	}

	void OnEnable(){

        updateSpellAvailability();
        loadSpellUpgrades();
		updatePrices ();

        if (PlayerPrefs.GetInt("PrestigeLv")>=1)
        {
            multiplierObj.SetActive(true);
        }
        else
        {
            multiplierObj.SetActive(false);

        }

        multiplier = 1;
        multiplierText.text = multiplier.ToString() + "x";

        abilityToggle.isOn = false;
        spellToggle.isOn = true;
        pressSpellToggle();//so it always starts on spell page

    }

	public void loadSpellUpgrades(){

        data = CSVReaderShort.Read("WpnSpl", ListOfSpells[0].itemNumber+1, ListOfSpells[4].itemNumber+2); //because index 0 is the header

      //  print("spl running data 1 os " + ListOfSpells[0].itemNumber.ToString() + " 2nd is " + ListOfSpells[4].itemNumber.ToString());

	}

	public void updatePrices(){

		for (int i = 0; i < ListOfSpells.Count; i++) {

		//	SplUpgrades._itemLV = PlayerPrefs.GetInt (SplUpgrades.gameObject.name);
			ListOfSpells [i].updateSpellCosts ();

		}

	}

	public void unlockWeaponAnim(Transform location, Sprite wpnSprite, Transform swordSpritePos)
    {

		for (int i = 0; i < WeaponUnlockAnims.Count; i++) {

			if (!WeaponUnlockAnims [i].activeInHierarchy) {

				WeaponUnlockAnims [i].GetComponent<Image> ().color = Color.cyan;
				WeaponUnlockAnims [i].GetComponent<SpriteHolder> ().imageHeld.sprite = wpnSprite;

				WeaponUnlockAnims [i].transform.position = new Vector3 (location.position.x, location.position.y, 90); //instantiates the beam offscre
                WeaponUnlockAnims[i].GetComponent<SpriteHolder>().imageHeld.transform.position = new Vector3(swordSpritePos.position.x, swordSpritePos.position.y, 90); //instantiates the beam offscre


                //	magicCritDmgList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

                WeaponUnlockAnims[i].SetActive (true);

				break;
			}

		}

	}

    public void addMultiplier()
    {
        if (multiplier == 1)
        {
            multiplier = 5;
        }
        else if (multiplier == 5)
        {
            multiplier = 10;
        }
        else if (multiplier == 10)
        {
            multiplier = 1;
        }
        multiplierText.text = multiplier.ToString() + "x";

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton(); //plays back btrn sound

    }

    public double getSplCost(int splNum){

		double splCost = (double)data[splNum]["SPELLCOST"];

		return splCost;
	}

	public string getSplName(int splNum){

		string splName = (string)data[splNum]["SPLNAME"];

		return splName;
	}

	public double getSplDmg(int splNum){

		double splDmg = (double)data[splNum]["SPELLDMG"];

		return splDmg;
	}

	public Sprite getSpellIcon(int listNumber){

		double SplType = (double)data[listNumber]["SPLTYPE"];

		//	print ("the list number is " + listNumber + " and the SwrdNum is " + SwrdNum.ToString () + " converted to int its..." + ((int)SwrdNum).ToString ());

		return SpellSpriteIcons[(int)SplType];

	}

	public int getSplType(int listNumber){

		double SplType = (double)data[listNumber]["SPLTYPE"];

		//	print ("the list number is " + listNumber + " and the SwrdNum is " + SwrdNum.ToString () + " converted to int its..." + ((int)SwrdNum).ToString ());

		return (int)SplType;

	}

	public void writeSplArray(){

		PlayerPrefsX.SetIntArray ("SplsUnlocked", unlockedSplArray);


	}

	public void updateSpellAvailability(){

        if (PlayerPrefsX.GetIntArray("SplsUnlocked").Length == 0) //if the array is empty it has never been called before
        {

            //    //dont need to do anything because the array is already populated to 300 with 0's

        }
        else
        {
            if (PlayerPrefsX.GetIntArray("SplsUnlocked").Length < 200) //we need to repair this array
            {
                int[] oldArry = PlayerPrefsX.GetIntArray("SplsUnlocked");

                Array.Resize<int>(ref oldArry, 300);

                unlockedSplArray = oldArry;//PlayerPrefsX.GetIntArray("PetsUnlocked");
            }
            else //array already exists...
            {
                //array has alreay been repaired!
                unlockedSplArray = PlayerPrefsX.GetIntArray("SplsUnlocked");

            }

        }

        //this first part needs to be reviewed before going to the 2nd part
  //      if (PlayerPrefsX.GetIntArray ("SplsUnlocked").Length>1){ //this checks if at least one spell has been unlocked
		//	unlockedSplArray = PlayerPrefsX.GetIntArray ("SplsUnlocked");
		//}

		for (int i = 0; i < unlockedSplArray.Length; i++) {

			if (unlockedSplArray [i] == 0 && i < 3 && cameraObj.spellsEnabled == true) {//game has just started so nothing should be unlocked 

				ListOfSpells [0].GreyScreen.enabled = false;
				ListOfSpells [0].itemNumber = i - 2;
		//		ListOfSpells [0].updateSpellCosts ();
		//		ListOfSpells [0].ItemSpellType = (SpellUpgrades.SpellType)(0); //fireball

				if (PlayerPrefs.GetInt ("Spl" + (ListOfSpells [1].itemNumber - 1).ToString ()) < 5) {
					ListOfSpells [1].GreyScreen.enabled = true;
				} else {
					ListOfSpells [1].GreyScreen.enabled = false;

				}

				ListOfSpells [1].itemNumber = i - 1;
		//		ListOfSpells [1].updateSpellCosts ();
		//		ListOfSpells [1].ItemSpellType = (SpellUpgrades.SpellType)(1);//waterbeam


				if (PlayerPrefs.GetInt ("Spl" + (ListOfSpells [2].itemNumber - 1).ToString ()) < 5) {
					ListOfSpells [2].GreyScreen.enabled = true;
				} else {
					ListOfSpells [2].GreyScreen.enabled = false;

				}

				ListOfSpells [2].itemNumber = i;
	
				ListOfSpells [3].itemNumber = i + 1;

				ListOfSpells [4].itemNumber = i + 2;

				break;

			} else if (unlockedSplArray [i] == 0 && i >= 3 && cameraObj.spellsEnabled == true) {

				ListOfSpells [0].itemNumber = i - 2;
				ListOfSpells [0].GreyScreen.enabled = false;

		//		ListOfSpells [0].updateSpellCosts (); //do this here so that it doesnt run afterwards. its slow when it runs afterwards i dont know why

				ListOfSpells [1].itemNumber = i - 1;
				ListOfSpells [1].GreyScreen.enabled = false;

		//		ListOfSpells [1].updateSpellCosts();

				ListOfSpells [2].itemNumber = i;
				ListOfSpells [2].GreyScreen.enabled = false;

		//		ListOfSpells [2].updateSpellCosts ();

				ListOfSpells [3].itemNumber = i + 1;
				ListOfSpells [3].GreyScreen.enabled = true;

		//		ListOfSpells [3].updateSpellCosts ();

				ListOfSpells [4].itemNumber = i + 2;
				ListOfSpells [4].GreyScreen.enabled = true;

	//			ListOfSpells [4].updateSpellCosts ();

				break;

			} else if(cameraObj.spellsEnabled == false){

				ListOfSpells [0].itemNumber = 0;
				ListOfSpells [0].GreyScreen.enabled = true;
	//			ListOfSpells [0].updateSpellCosts ();


				ListOfSpells [1].itemNumber = 1;
				ListOfSpells [1].GreyScreen.enabled = true;
	//			ListOfSpells [1].updateSpellCosts ();


				ListOfSpells [2].itemNumber = 2;
				ListOfSpells [2].GreyScreen.enabled = true;
	//			ListOfSpells [2].updateSpellCosts ();


				ListOfSpells [3].itemNumber =3;
				ListOfSpells [3].GreyScreen.enabled = true;
	//			ListOfSpells [3].updateSpellCosts ();


				ListOfSpells [4].itemNumber = 4;
				ListOfSpells [4].GreyScreen.enabled = true;
	//			ListOfSpells [4].updateSpellCosts ();


				break;
			}



		}

	}

    public void pressSpellToggle()
    {

        spellsDisplay.SetActive(true);
        abilityDisplay.SetActive(false);

        //Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        abilityDisplayText.color = myToggleOff;
        spellsDisplayText.color = myToggleOn;


    }

    public void pressAbilityToggle()
    {

        abilityDisplay.SetActive(true);
        spellsDisplay.SetActive(false);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        spellsDisplayText.color = myToggleOff;
        abilityDisplayText.color = myToggleOn;


    }



    public void updateGems(){

	//	gemAmount.text =  LargeNumConverter.setDmg(playerObj.GetComponent<ControlPlayer1Character> ().playerGold).ToString();
	}


}
