﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SoulChargeCooldown : MonoBehaviour {

	// Use this for initialization
	private float CoolDownTimeMax;
	public float CoolDownTime;

	public float FreezeTimeMax;
	public float FreezeTime;

	//public bool enemyFrozen = false;

	public Image CoolDownGfx;
	public bool isReady=false;
	public TextMeshProUGUI timeCountDown;
//	public TextMeshProUGUI abilityCountDown;

	public string timeString;
	public ControlEnemy enemyObj;
	public MainMenu cameraObj;

	void OnEnable(){

		updateCooldownTimeMax ();

		if (PlayerPrefs.GetInt ("SoulTime") > 0 && PlayerPrefs.GetInt ("SoulTimeRdy") == 0) {
			CoolDownTime = (float)PlayerPrefs.GetInt ("SoulTime");
			timeCountDown.text = CoolDownTime.ToString (); 

		} else if (PlayerPrefs.GetInt ("SoulTimeRdy") == 1) {
			isReady = true; //they saved the megaslash
			timeCountDown.text = "";
			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

		} else {
			CoolDownTime = CoolDownTimeMax;
			timeCountDown.text = CoolDownTime.ToString (); 
		}

		CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

	}

	public void updateCooldownTimeMax(){
        //this is in case the prestige talent is taken
        CoolDownTimeMax = 20f;


	}

	public void decreaseFreezeCooldown(){

		if (CoolDownTime > 0) {

			if (CoolDownTime>0) {
				PlayerPrefs.SetInt ("SoulTime", Mathf.CeilToInt(CoolDownTime));
				PlayerPrefs.SetInt ("SoulTimeRdy", 0);


			}

			CoolDownTime -= 1.00f * (1 + cameraObj.petManager.abilityArray[1]); 



			isReady = false;
			timeString = CoolDownTime.ToString ();

            if (timeString.Length > 2)
            {
                timeString = timeString.Substring(0, 2); //10B
            }

            timeCountDown.text = timeString;
			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

			if (CoolDownTime <= 0) {
				isReady = true;
				PlayerPrefs.SetInt ("SoulTimeRdy", 1);

				timeCountDown.text = "";
			}


		}
		updateCooldownTimeMax ();

	}

	public void FreezeTimePress(){
		
		if (isReady == true) {

			//freezeAnimation.SetActive (true);

			//enemyObj = Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();

			CoolDownTime = CoolDownTimeMax;

			//cameraObj.persistantSoundManager.GetComponent<SoundManager> ().FreezeTime ();

			this.gameObject.GetComponentInParent<ActiveAbilityManager> ().unlockWeaponAnim (this.gameObject.transform, this.gameObject.GetComponentInParent<ActiveAbilityManager> ().soulChargeImg.sprite);//this is to play the animation, the name of the method is wrong but i was too lazt to change

            isReady = false;

            timeCountDown.text = CoolDownTime.ToString();

            CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

            PlayerPrefs.SetInt("SoulTime", Mathf.CeilToInt(CoolDownTime));
            PlayerPrefs.SetInt("SoulTimeRdy", 0);
        }
    }

}
