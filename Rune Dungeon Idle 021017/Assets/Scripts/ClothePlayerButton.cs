﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ClothePlayerButton : MonoBehaviour {

	// Use this for initialization
	public GameObject Head, Torso, 
					UpperLeftArm,LeftHand,
					UpperRightArm,
					Cape, Sword;

	public Inventory _InventoryScript;

	public Image RarityBkg;

	public WeaponUpgrades wpnUpgradeForBkg;

	public Dictionary<string, Sprite> dictSprites = new Dictionary<string, Sprite>();

	public List <Texture2D> CapeTextures = new List<Texture2D> (); //4 index list of the blocks that are selected

	public List <Sprite> RarityBkgList = new List<Sprite> (); //4 index list of the blocks that are selected

	public List <GameObject> NeckList = new List<GameObject> ();  //0 is RoyalNeck, 1 is Knight Neck, 2 is DragonNeck, 3 is wolf

	public GameObject animatedPlayer;


	public bool TurnOnClothes=false;


	void OnEnable(){
		if (dictSprites.Count == 0) {
			Sprite[] sprites = Resources.LoadAll<Sprite> ("NewArmor");

			foreach (Sprite sprite in sprites) {
				dictSprites.Add (sprite.name, sprite);
			}
		}
		putOnClothes ();
		setIconBkg ();

	}

	public void setIconBkg(){

		RarityBkg.sprite = RarityBkgList [PlayerPrefs.GetInt ("setCharacterIconBkg")];

	}

	public void refreshAnimatedPlayerClothes(){

		animatedPlayer.GetComponent<ClothePlayer> ().enableClothes (); //puts clothes on the animated player

	}

	public void putOnClothes(){

		_InventoryScript.loadDict (); //preloads the sprites for the hero.



		//you need to load all the values for the clothes so when you load the sprites it gets what the player actually has instead of the placeholder
		for (int i = 0; i < _InventoryScript.InventoryList.Count; i++) {

			_InventoryScript.InventoryList[i].loadMods();

			//these armor level things will have to be colored eventually
		}

		if (TurnOnClothes == true && Camera.main.GetComponent<CampaignGameOrganizer> () != null 
		    || TurnOnClothes == true && Camera.main.GetComponent<MainMenu>()!=null) {


			for (int z = 0; z < NeckList.Count; z++) {

				NeckList [z].SetActive (false);

			}

//			print ("head case is " + PlayerPrefs.GetInt ("head").ToString ());
			
			switch (_InventoryScript.InventoryList[0].armorLevel) { //head
				
			case 0:
				break;
			case 1:
				Head.gameObject.GetComponent<Image> ().sprite = dictSprites ["NoviceHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
			//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["ShortBrownHair"]; //hair sprite?
				
				break;
			case 2:
				Head.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				break;
			case 3:
				Head.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				
				break;
			case 4:
				Head.gameObject.GetComponent<Image> ().sprite = dictSprites ["KnightHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				
				break;
			case 5:
				Head.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
			//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				
				break;
//			case 6: //for king
//				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_KingHead"]; //use this to set a sprite to a specific sprite in the dict.
//				Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
//				
//				break;
//			case 7: //for Wolf Armor
//				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WolfHead"]; //use this to set a sprite to a specific sprite in the dict.
//				Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
//				
//				break;
//			case 8: //for Wolf Armor
//				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_PurpHead"]; //use this to set a sprite to a specific sprite in the dict.
//				Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
//				
//				break;
//			case 9: //for Invo Armor
//				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_InvoHead"]; //use this to set a sprite to a specific sprite in the dict.
//				Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
//				
//				break;
			}
			
			switch (_InventoryScript.InventoryList[1].armorLevel) { //says test bescause the torsos had to be adjusted to fit the icon correctly
			case 0:
				break;
			case 1:
				Torso.gameObject.GetComponent<Image> ().sprite = dictSprites ["NoviceTest"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				Torso.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[3].SetActive(true);
				break;
			case 3:
				Torso.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[0].SetActive(true);
				break;
			case 4:
				Torso.gameObject.GetComponent<Image> ().sprite = dictSprites ["KnightTest"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[1].SetActive(true);

				break;
			case 5:
				Torso.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonTest"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[2].SetActive(true);

				break;

			}
			
			switch (_InventoryScript.InventoryList[2].armorLevel) { //remeber the upper arms are switched right is left and vise versa
			case 0:
				break;
			case 1:
				UpperLeftArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["NoviceLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
			//	LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<Image> ().sprite = dictSprites ["NoviceLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["NoviceRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
		//		RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				UpperLeftArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
		//		RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				UpperLeftArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
		//		RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				UpperLeftArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["KnightLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<Image> ().sprite = dictSprites ["KnightLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["KnightRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
		//		RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				UpperLeftArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
		//		RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}
			
			switch (_InventoryScript.InventoryList[3].armorLevel) {
			case 0:
				break;
			case 1:
		//		LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeggings"]; //use this to set a sprite to a specific sprite in the dict.
				//LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegLower"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceBoots"]; //use this to set a sprite to a specific sprite in the dict.

//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegUpper"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegLower"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_RightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
		//		LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
		//		LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
		//		LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
		//		LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeggings"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}




			switch (PlayerPrefs.GetInt ("Sword")) { //for the sword

			case 0:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfSwordLv1"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 1:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfSwordLv2"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 2:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["WolfSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["RoyalSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 6:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 7:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 8:
				Sword.gameObject.GetComponent<Image> ().sprite = dictSprites ["DragonSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			}


		}
	}

}
