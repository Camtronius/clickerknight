﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WeaponUpgrades : MonoBehaviour {

	public double _itemCost; //this is the original item cost
	public double _newItemCost; //this is the cost after the level is factored in
	public double _itemDMG;
	public int _itemLV;
	public int bkgLv;

	public string _itemName;

	public int itemNumber; //this is the number this item corresponds to the data loaded depending on the wpn unlocked
    public int indexNumber; //this is the number this item corresponds to its place in the list of wpnUpgrades (0-5)


    public TextMeshProUGUI itemCost;
	public TextMeshProUGUI itemLv;
	public TextMeshProUGUI itemDMG;
	public TextMeshProUGUI itemNameText;


	public Image GreyScreen;
	public Image SwordBkgSprite;
	public Image SwordSprite;

	public Sprite upgradeSprite;
	public Sprite cantUpgradeSprite;
	public Sprite nullSprite;

	public Image buttonSprite;


	public MasterWeaponList masterWpnList;

	public MainMenu cameraObj;

	void Start () {
		cameraObj = Camera.main.GetComponent<MainMenu> ();
		updateItemCosts ();
	}
	
	// Update is called once per frame
	public void updateItemCosts(){

		if (itemNumber < MainMenu.LVCAP+3) { //add three so players should be able to complete daily dungeons if they are at the end
			_itemLV = PlayerPrefs.GetInt ("Wpn" + itemNumber.ToString ()); //gets the item of the wpn

			_itemCost = masterWpnList.getWpnCost (indexNumber); // gets the cost of the wpn

			_itemName = masterWpnList.getWpnName (indexNumber);

			_itemDMG = masterWpnList.getWpnDmg (indexNumber);

			itemNameText.text = _itemName;

			_newItemCost = _itemCost + _itemLV * _itemCost;

			SwordBkgSprite.sprite = masterWpnList.getSwordBkg (indexNumber);

			bkgLv = masterWpnList.getSwordBkgNum (indexNumber);

			SwordSprite.sprite = masterWpnList.getSwordIcon (indexNumber);


			if (_itemLV < 5) {
				
				if (Camera.main.GetComponent<MainMenu> ().reduceWeaponCost == true) {
				
					_newItemCost = _newItemCost * (Camera.main.GetComponent<MainMenu> ().weaponCostReduction-Camera.main.GetComponent<MainMenu> ().BarginBinReductionPrestige);

					itemCost.text = LargeNumConverter.setDmg (_newItemCost); //10% discount for test
				
				
				} else {
				
					_newItemCost = _newItemCost * (1-Camera.main.GetComponent<MainMenu> ().BarginBinReductionPrestige);

					itemCost.text = LargeNumConverter.setDmg (_newItemCost);

				}

                checkButtonSprite();

                itemDMG.text = LargeNumConverter.setDmg ((_itemDMG + (_itemLV) * (_itemDMG / 5))) + " DMG";

			} else {
				
				itemCost.text = "";
				itemDMG.text = LargeNumConverter.setDmg ((_itemDMG + (_itemLV) * (_itemDMG / 5))) + " DMG";

				buttonSprite.sprite = nullSprite;
			}

			itemLv.text = "Item LV: " + _itemLV.ToString ();
		}else{
			this.gameObject.SetActive(false);
		}

	}

	public void checkButtonSprite(){

		ControlPlayer1Character playerObj = masterWpnList.playerObj.GetComponent<ControlPlayer1Character> ();

		if (playerObj.playerGold >= _newItemCost) {

			buttonSprite.sprite = upgradeSprite;

		} else {

			buttonSprite.sprite = cantUpgradeSprite;

		}

	}

	public void setSwordSpriteCharacterMain(){

		PlayerPrefs.SetInt ("Sword", masterWpnList.getCharSword (indexNumber));


	}

	public void purchaseItem(){

        if (buttonSprite.sprite == upgradeSprite)
        {
            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UpgradeWeapon();//plays upg spoell sound
        }

        for (int i=0; i<masterWpnList.multiplier; i++) //buys amt of item depending on multiplier
            {
                buyTheItem();
            }
        
    }

    private void buyTheItem()
    {
        ControlPlayer1Character playerObj = masterWpnList.playerObj.GetComponent<ControlPlayer1Character>();

        if (playerObj.playerGold < _newItemCost)
        {
            Camera.main.GetComponent<MainMenu>().showErrorMsg("Not Enough Gold!");

        }

        if (playerObj.playerGold >= _newItemCost && PlayerPrefs.GetInt("Wpn" + itemNumber.ToString()) < 5)
        { //so level cant go above 5

            //		print ("item purchased");


            masterWpnList.unlockWeaponAnim(this.gameObject.GetComponentInChildren<Button>().transform, SwordSprite.sprite, SwordSprite.gameObject.transform);

            PlayerPrefs.SetInt("Wpn" + itemNumber.ToString(), _itemLV + 1); //levels up the item

            //itemlv+1 and masterwpnlist.writearray used to be here in that order

            if (_itemLV + 1 >= 5)
            {

                masterWpnList.unlockedWpnArray[itemNumber] = 2;
                //print("new wpn unlocked");
                masterWpnList.writeWpnArray();

            }


            playerObj.playerGold -= _newItemCost;

            //this needs to be changed to a string that is saved, then parsed later

            //	PlayerPrefs.SetFloat ("playerAtkDmg", _itemDMG + PlayerPrefs.GetInt ("Wpn" + itemNumber.ToString())*(_itemDMG/5));//the only other place this is used is in ControlPlayer1 and referencfed in Main Menu

            PlayerPrefs.SetString("playerAtkDmg", (_itemDMG + PlayerPrefs.GetInt("Wpn" + itemNumber.ToString()) * (_itemDMG / 5)).ToString());

            setSwordSpriteCharacterMain(); //updates the sword for the maincharacter page.

            playerObj.playerAtkDmg = double.Parse(PlayerPrefs.GetString("playerAtkDmg"));

            masterWpnList.setCharacterIconBkg(bkgLv);
            masterWpnList.clothePlayerButton.putOnClothes();
            masterWpnList.clothePlayerButton.refreshAnimatedPlayerClothes();


            //			masterWpnList.updatePrices (); //this automatically happens in mainmenu but this will make it  faster

            if (_itemLV + 1 >= 5)
            {

               // masterWpnList.unlockedWpnArray[itemNumber] = 2;
                //   print("new wpn unlocked");
                masterWpnList.updateWeaponAvailability();

                masterWpnList.loadWeaponData(); //after the update wpn availability method is run, new item numbers will be assigned. Then we can load the new CSV values.
            }
           



            masterWpnList.updatePrices();

        }



        if (cameraObj.TutorialObject.wpnMenuTap == true)
        {

            cameraObj.TutorialObject.pointAtWindowClose();
        }

    }


}
