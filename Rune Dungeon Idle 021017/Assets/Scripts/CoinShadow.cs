﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinShadow : MonoBehaviour {

	// Use this for initialization
	//public GameObject shadowObj;
	public LayerMask chooseLayer;
	public GameObject shadowObj;

	public float xInitialScale;
	public float xScaleMultiplier;


	public float initialAlpha;
	public float currentAlpha;
	public SpriteRenderer spriteAlpha;

    public bool isPet = false;
    public float petCountDown = 2f;

	void Start () {
		initialAlpha = shadowObj.gameObject.GetComponent<SpriteRenderer> ().color.a;
	}

	// Update is called once per frame
	void Update () {

        //for pet appearance only!

        if (isPet == true && petCountDown>=0)
        {
            petCountDown -= Time.deltaTime;

                if (petCountDown < 0)
                {
                    if (this.gameObject.GetComponent<Animator>().enabled == false)
                    {
                        this.gameObject.GetComponent<Animator>().enabled = true;
                    }

                    
                }
        }

        //end pet appearance
		
			RaycastHit2D hit =	Physics2D.Raycast (this.transform.position, Vector2.down, 100, LayerMask.GetMask ("Floor"));

	//		Debug.DrawRay (torsoObj.transform.localPosition, Vector2.down);

			if (hit.collider != null) {

				float tempX = (float)hit.point.x;
				float tempY = (float)hit.point.y;

				shadowObj.transform.position = new Vector3 (tempX, tempY, 87.8f);

			//	print ("the distance is " + hit.distance);

				float hitDist = (float)hit.distance;
		
				if (hitDist < xScaleMultiplier) {

				shadowObj.gameObject.transform.localScale = new Vector3 ((float)(xInitialScale - hitDist), xScaleMultiplier - hitDist, 1f); 
				} else {
				shadowObj.gameObject.transform.localScale = new Vector3 (0, 1, 1f); 

				}
				Color tmpColor = spriteAlpha.color;
				tmpColor.a = initialAlpha - (initialAlpha * hitDist);
				currentAlpha = tmpColor.a;
				spriteAlpha.color = tmpColor;
			}
		
	}

    public void openPetFound() //this is only if a pet is dropped
    {
        Camera.main.GetComponent<MainMenu>().PetGenerator.SetActive(true);

    }


}
