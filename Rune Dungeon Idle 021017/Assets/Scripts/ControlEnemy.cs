﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class ControlEnemy : MonoBehaviour {

	public Animator animator;

	public GameObject FlameDOT;
		public bool EnemyFlameDOT = false;
			public bool justBurned = false;
				public bool enemyWounded=false;

	public GameObject BoneThrow;
	public GameObject WolfBiteEffect;
	public GameObject SkeleWarriorAtkEffect;
	public GameObject SkeleWarriorComboEffect;
	public GameObject SkeleMagicHit;
		public int SpellTypeInt;
	public GameObject PurpleExplosion;

	
	private Vector3 player1TorsoPosition;

	//sspell spawn locationz
	public GameObject shotSpawn;
	public GameObject damageUIElement;

	public double playerHealthMax = 100;
	public double playerHealthCurrent = 100;
	public double playerHealthNew= 100;
	public double lifeStealValue = 0;
	public double enemyDamage = 0; //need to apply this to enemy attacks
	public double enemyRank = 0; //not sure what to do with this yet

	public bool playerDead = false;

	//modifiers to the character
	public bool player2Invis=false;
	public bool player2ReturnToVisible = false; //does what it says ;p
	public bool player2Stun=false; //tru if player is invis
	public bool lifeSteal= false;
	public bool isShielded = false;
	public bool isHittable = false; //this is so we know if the box collider is on or not
	public bool isBoss = false;
	public bool EnemyStunned = false;
	public float StunTime = 0;
	public bool isFrozen = false;
	public float WoundTime = 0;

		

	//end mods to the character
	public GameObject Player2HealthBar;
	public GameObject Player2Torso;

	public GameObject Player2PunchHand;
	public GameObject AutoAtkCollider;

	public Vector3 defaultLocation;
	public GameObject SpellCreatorObject;
	public MainMenu CameraMenu;

	public ControlPlayer1Character playerObj;
	public CircleShadow shadowObj;

	public Color enemyBldColor;
	// Use this for initialization

	public bool isSkele;
	public bool isChest;
	public bool bigEnemy = false; //so the shadow can use another object instead of torso

    public Sprite eggSprite;
    public EnemyRagdoll ragdollScript;
    public Sprite egg0;
    public Sprite egg1;
    public Sprite egg2;
    public Sprite egg3;



    void Start () {

		shadowObj = Camera.main.GetComponent<MainMenu> ().shadowObj.GetComponent<CircleShadow> (); //assigns the shadow script to this obj (shadow obj is to make things simpler)

        ragdollScript = this.gameObject.GetComponent<EnemyRagdoll>();

		if (bigEnemy == false) {
			shadowObj.torsoObj = ragdollScript.Torso; //assigns the torso so it knows where to make the shadow
		} else {
			shadowObj.torsoObj = ragdollScript.bodyPartForShadow; //assigns the torso so it knows where to make the shadow


		}
		shadowObj.enemyDead = false;

        ragdollScript.shadowObj = shadowObj; //gives the object to the enemy ragdoll so it can call methods when the enemy dies etc. (doesnt work well for treasure chest)




		CameraMenu = Camera.main.GetComponent<MainMenu> ();

		playerObj = CameraMenu.playerObj;

		defaultLocation = this.transform.position;

		animator = GetComponent<Animator>();

		Player2HealthBar = GameObject.Find ("Player2Health");

		Player2HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/playerHealthMax);

		Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(playerHealthMax);
	
	//	Camera.main.gameObject.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite = Camera.main.gameObject.GetComponent<CampaignGameOrganizer>().nullsprite;

		StartCoroutine ("HealthBarGUI"); //detects if a block is being selected

		StartCoroutine ("enemyAttacking");

		shotSpawn = GameObject.Find ("ShotSpawn");

		damageUIElement = GameObject.FindGameObjectWithTag ("ScreenDmg");

	}

	void Update(){

		if (EnemyStunned == true && StunTime>0 && playerHealthNew>0) { //this is for spell stun
		
						if (animator.enabled == true) {
		
							animator.enabled = false;
						}
		
						StunTime -= Time.deltaTime;

					} else if(StunTime<=0){

					if (animator.enabled == false && playerHealthNew > 0 && isFrozen==false) { //the health condition is in case the enemy dies while stunned. see what happens when you remove is froze == false
							animator.enabled = true;
						}
		
						EnemyStunned = false;
						StunTime = 1;
					}

		if (WoundTime >= 0) {
			WoundTime -= Time.deltaTime;
			enemyWounded = true;

		} else if(enemyWounded==true && WoundTime < 0) {
			enemyWounded = false;

			ParticleSystem woundParticles = Camera.main.GetComponent<MainMenu>().woundBlood.GetComponent<ParticleSystem> ();
			ParticleSystem.EmissionModule emis1 = woundParticles.emission;
			emis1.enabled = false;

		}


	}


	void OnDisable(){

		StopAllCoroutines ();
	}

	void OnDestroy(){

		StopAllCoroutines ();

	}

	IEnumerator enemyAttacking(){

		while (true) {

			yield return new WaitForSeconds (Random.Range (2,5));

			if (CameraMenu.MainCharacterPage.activeSelf == false 
				&& CameraMenu.windowOpen ==false
				&& CameraMenu.playerObj.lvUpButton.activeSelf==false
				) {
				
				if (HasParameter ("SkeleAttack", animator) == true) {

					animator.SetBool ("SkeleAttack", true);
				}

				if (HasParameter ("SkeleAttackCombo", animator) == true) {

					animator.SetBool ("SkeleAttackCombo", true);
				}

				if (HasParameter ("castWolfAttack", animator) == true) {

					animator.SetBool ("castWolfAttack", true);
				}

				if (HasParameter ("castWolfAttack2", animator) == true) {

					animator.SetBool ("castWolfAttack2", true);
				}

				if (HasParameter ("DefaultAtkP1", animator) == true) {

					animator.SetBool ("DefaultAtkP1", true);
				}
				if (HasParameter ("RegAtk", animator) == true) {

					animator.SetBool ("RegAtk", true);
				}
				if (HasParameter ("StunAtk", animator) == true) {

					animator.SetBool ("StunAtk", true);
				}
				if (HasParameter ("Atk", animator) == true) {

					animator.SetBool ("Atk", true);
				}
			}
		}
	}

	public void castFireSpell(){

		animator.SetBool ("SkeleAttack", true);

	}

	//for evil person enemy
	public void evilGuyFireball(){
		animator.SetBool ("FireSpell", true);
//		print ("==================================firebal called");
	}
	public void castWaterBeamSpell(){
		animator.SetBool ("WaterBeam", true);
//		print ("==================================waterb called");

	}
	public void castRockThrowSpell(){
		animator.SetBool ("RockThrow", true);
//		print ("==================================castr called");

	}
	public void castTornadoSpell(){	
		animator.SetBool ("TornadoSpell", true);
//		print ("==================================castt called");

	}
	public void castPlayerEnemySwordAtk(){
		
		animator.SetBool ("DefaultAtkP1", true);	
		
	}
	//end evil enemy spells

	public void castComboSpell(){
		
		animator.SetBool ("SkeleAttackCombo", true);
		
	}

	public void castWolfAttack(){
		
		animator.SetBool ("castWolfAttack", true);
		
	}

	public void castWolfAttack2(){
		
		animator.SetBool ("castWolfAttack2", true);
		
	}

	public void castSkeleMageSpell(){
		
		animator.SetBool ("SkeleAttack", true);
		
	}

	public void castGolemRegAtk(){
		
		animator.SetBool ("RegAtk", true);
		
	}

	public void castGolemStunAtk(){
		
		animator.SetBool ("StunAtk", true);
		
	}

	public void castPurpReptileAtk(){
		
		animator.SetBool ("Atk", true);
		
	}

	public void wolfDamage(){

		activateScreenDmg ();
//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//
//		WolfBiteEffect.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//
//		GameObject WolfBite = (GameObject)Instantiate (WolfBiteEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//
//		WolfBiteSound ();	
//	
	}

	public void purpReptileAtk(){
		activateScreenDmg ();

		//
//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//		
//		SkeleWarriorAtkEffect.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//		
//		GameObject SkeleWarriorAtk = (GameObject)Instantiate (SkeleWarriorAtkEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//		GameObject PurpExpl = (GameObject)Instantiate (PurpleExplosion, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//
	}

	public void wolfDamageAndBurn(){
		activateScreenDmg ();

//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//		
//		WolfBiteEffect.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//		
//		
//		GameObject WolfBite = (GameObject)Instantiate (WolfBiteEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//		
//		Camera.main.GetComponent<CampaignGameOrganizer> ().BurntRunesManager (2, 0);
//		Camera.main.GetComponent<CampaignGameOrganizer> ().createBurntRunes ();
//
//
//		WolfBiteSound ();	
//		
	}

	public void wolfGrowlForAttack(){
        //		activateScreenDmg ();

        //
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().WolfBiteGrowl ();
//
	}

	public void SkeleWarriorDamage(){
		activateScreenDmg ();

//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//
//		SkeleWarriorAtkEffect.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//
//		GameObject SkeleWarriorAtk = (GameObject)Instantiate (SkeleWarriorAtkEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//		
	}

	public void SkeleWarriorComboDamage(){
		activateScreenDmg ();

//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//
//		if(this.gameObject.name=="Reptillian" || this.gameObject.name == "FinalBossAct2"){
//		SkeleWarriorComboEffect.GetComponent<EnemyProjectileCollider> ().confusePlayer = true; //this makes it so the confusion runes come up
//
//		}
//
//		SkeleWarriorComboEffect.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//
//		GameObject SkeleWarriorComboHit = (GameObject)Instantiate (SkeleWarriorComboEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//		
	}

	public void SkeleMageHit(){
		activateScreenDmg ();


//
//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().disableAllParticleEffects (); //makes it so there are no spell types active on start
//
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().stunPlayer = false;
//
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//
//		GameObject SkeleMagic = (GameObject)Instantiate (SkeleMagicHit, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//
//		SkeleMagic.GetComponent<EnemyProjectileCollider> ().runeTypeInt = getSpellType ();
//
//
//		switch(getSpellType()){
//			
//		case 0:
//
//			GameObject ParticleMagic = Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageWindEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//
//			ParticleMagic.SetActive(true);
//			//SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageWindEffect.SetActive(true);
//
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.BriefWindSound ();
//
//			break;
//		case 1:
//
//			GameObject ParticleMagic2 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageFireEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//
//			ParticleMagic2.SetActive(true);
//
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.SmallExplosionSound ();
//
//		
//			break;
//		case 2:
//
//			GameObject ParticleMagic3 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageWaterEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//
//			ParticleMagic3.SetActive(true);
//
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.WaterBeamShot ();
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.soundEffect.loop=false; //waterbeamshot loops by default
//			//beam sound
//			break;
//		case 3:
//
//			GameObject ParticleMagic4 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageEarthEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//
//			ParticleMagic4.SetActive(true);
//
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.EarthWoodBreak ();
//
//			//earth wood break sound
//			break;
//		case 4:
//			GameObject ParticleMagic5 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageMiscEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//
//
//			ParticleMagic5.SetActive(true);
//
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.PewPewSound ();
//
//			//PEWPEW
//			break;
//			
//		}

	//	SkeleMagic.SetActive (true); //the prefab is originally inactive so i can set the rune int;

	}

	public void CreateSwordAtkCollider(){
		activateScreenDmg ();

		
//		ControlPlayer1Character enemyToHit = GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character>();
//		
//		GameObject AutoAtkColliderClone = (GameObject)Instantiate (AutoAtkCollider, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player1Torso.transform.position.y, 0), Quaternion.identity);
//		
//		AutoAtkColliderClone.GetComponentInChildren<SwordAtkCollider> ().createdByPlayer = 2;
//
	}

	public void createConfustionLastBoss(){
		activateScreenDmg ();


//		Camera.main.GetComponent<CampaignGameOrganizer>().createConfusion();
//		ControlPlayer1Character enemyToHit = GameObject.Find ("Player1Character").GetComponentInChildren<ControlPlayer1Character>();
//
//		GameObject PurplePuffObj = (GameObject)Instantiate (PurpleExplosion, new Vector3 (enemyToHit.transform.position.x, enemyToHit.Player1Torso.transform.position.y, 0), Quaternion.identity);
//
	}

	public void activateScreenDmg(){

        if (playerObj.shieldOn == true)
        {

            if (playerObj.shieldHp > enemyDamage)
            {
                playerObj.shieldHp -= enemyDamage;
            }
            else
            {
                damagePlayer(enemyDamage- playerObj.shieldHp);
                playerObj.shieldHp -= enemyDamage;
                
                
                //break the shield and give player remaining damage
            }

            playerObj.shieldHPUpdate(); //updates the shield slider and current HP

        }
        else
        {

            damagePlayer(enemyDamage);
        }

	
	}

    public void damagePlayer(double damageToPlayer)
    {
        damageUIElement.GetComponent<Animator>().SetTrigger("Hit");

        if (damageToPlayer >= playerObj.playerHealthCurrent && isBoss == false)
        { //if player dies to an enemy and has resurrection

            if (Camera.main.GetComponent<MainMenu>().ShallowGrave == true && Random.Range(0, 100) <= 30)
            {

                //to restore a percentage of HP
                playerObj.playerHealthNew = (CameraMenu.GraveHP / 100f) * playerObj.playerHealthMax;

                playerObj.playerHealthCurrent = (CameraMenu.GraveHP / 100f) * playerObj.playerHealthMax;

                playerObj.quickHPUpdate();

                playerObj.resurrectionAnim.SetActive(true);

                Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().ResurrectionSound();

            }
            else if (Camera.main.GetComponent<MainMenu>().ShallowGrave == false)
            {  //if player dies to an enemy and DOES NOT HAVE resurrection

                playerObj.playerHealthCurrent = 0;
                playerObj.playerHealthNew = 0;

                playerObj.quickHPUpdate();


                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.GetComponent<CountDownToAttack>().fromBoss = false; //enables the death screen difference
                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.SetActive(true);
                Camera.main.GetComponent<MainMenu>().resetBowAnimBools(true);

                Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool("charDown", true);
                //Camera.main.GetComponent<MainMenu> ().animatedPlayerObj.playDeathSound ();

                Camera.main.GetComponent<MainMenu>().windowOpen = true;

            }
            else
            { //not sure what this is for but too scared to remove it

                playerObj.playerHealthCurrent = 0;
                playerObj.playerHealthNew = 0;

                playerObj.quickHPUpdate();


                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.GetComponent<CountDownToAttack>().fromBoss = false; //enables the death screen difference
                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.SetActive(true);
                Camera.main.GetComponent<MainMenu>().resetBowAnimBools(true);

                Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool("charDown", true);
                //Camera.main.GetComponent<MainMenu> ().animatedPlayerObj.playDeathSound ();


                Camera.main.GetComponent<MainMenu>().windowOpen = true;
            }



        }
        else if (damageToPlayer >= playerObj.playerHealthCurrent && isBoss == true)
        { //if the enemy IS a boss
          //du stuff

            if (Camera.main.GetComponent<MainMenu>().ShallowGrave == true && Random.Range(0, 100) <= 30)
            { //resurrection at a boss

                //to restore a percentage of HP
                playerObj.playerHealthNew = (CameraMenu.GraveHP / 100f) * playerObj.playerHealthMax;

                playerObj.playerHealthCurrent = (CameraMenu.GraveHP / 100f) * playerObj.playerHealthMax;

                playerObj.quickHPUpdate();

                playerObj.resurrectionAnim.SetActive(true);

                Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().ResurrectionSound();

            }
            else if (Camera.main.GetComponent<MainMenu>().ShallowGrave == false)
            { //no resurraction at boss

                playerObj.playerHealthCurrent = 0;
                playerObj.playerHealthNew = 0;
                playerObj.quickHPUpdate();

                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.GetComponent<CountDownToAttack>().fromBoss = true; //enables the death screen difference
                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.SetActive(true);
                Camera.main.GetComponent<MainMenu>().resetBowAnimBools(true);

                Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool("charDown", true);
                //Camera.main.GetComponent<MainMenu> ().animatedPlayerObj.playDeathSound ();


                Camera.main.GetComponent<MainMenu>().windowOpen = true;
            }
            else
            {

                playerObj.playerHealthCurrent = 0;
                playerObj.playerHealthNew = 0;
                playerObj.quickHPUpdate();

                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.GetComponent<CountDownToAttack>().fromBoss = true; //enables the death screen difference
                Camera.main.GetComponent<MainMenu>().PlayerDeadScreen.SetActive(true);
                Camera.main.GetComponent<MainMenu>().resetBowAnimBools(true);

                Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool("charDown", true);
                //Camera.main.GetComponent<MainMenu> ().animatedPlayerObj.playDeathSound ();

                Camera.main.GetComponent<MainMenu>().windowOpen = true;

            }





        }
        else
        { //for just damaging the player

            float woundPct = 0; //if the enemy is wounded this will change

            if (enemyWounded == false)
            {
            }
            else
            {
                woundPct = 0.15f;

            }

            double hitPlayerDMG = (damageToPlayer + Random.Range(0, 10)) * (1 - CameraMenu.petManager.abilityArray[24] - woundPct);


            playerObj.playerHealthNew -= hitPlayerDMG;

            Camera.main.GetComponent<MainMenu>().dmgPlayerAmount(hitPlayerDMG);


            if (Camera.main.GetComponent<MainMenu>().returnAtkPrestige > 0)
            { //return the damage to enemy 

                Camera.main.GetComponent<MainMenu>().returnEnemyDmgAtk();

                //	print ("atk damage returned!");
            }

            if (Camera.main.GetComponent<MainMenu>().returnMagPrestige > 0)
            { //return the damage to enemy 

                playerObj.returnEnemyDmgMag();

                //	print ("mag damage returned!");
            }

            playerObj.quickHPUpdate();


        }





    }


    IEnumerator HealthBarGUI(){
		
		while (true) {
			
			if(playerHealthCurrent>playerHealthNew && lifeSteal==false){ //for health loss
		//		playerHealthCurrent-=5f;
				playerHealthCurrent=playerHealthNew;
				
				Player2HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/playerHealthMax);

				if (playerHealthNew > 0) {
					Player2HealthBar.GetComponentInChildren<TextMeshProUGUI> ().text = LargeNumConverter.setDmg (playerHealthCurrent) + "/" + LargeNumConverter.setDmg (playerHealthMax); //where does it do this for the players hp?
				} else {
					Player2HealthBar.GetComponentInChildren<TextMeshProUGUI> ().text = "0/" + LargeNumConverter.setDmg (playerHealthMax); //where does it do this for the players hp?
				}

                eggCheck(); //checks to see if its the egg and to change its sprite as its breaking

            }

            if (playerHealthCurrent<playerHealthNew){ //this makes it so the hp doesnt only go down in increments of 5

				playerHealthCurrent=playerHealthNew;
				Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(playerHealthMax);


            }
			
			if(lifeSteal==true && playerHealthCurrent<=playerHealthMax){

				if((playerHealthCurrent + lifeStealValue)<playerHealthMax){
					playerHealthCurrent+=lifeStealValue;
				}
				
				if((playerHealthNew+ lifeStealValue)<playerHealthMax){
					playerHealthNew+=lifeStealValue;
				}
				
				//if playerhp + lifesteal value is more than max hp, just set as max hp
				if((playerHealthCurrent + lifeStealValue)>playerHealthMax){
					playerHealthCurrent=playerHealthMax;
				}
				
				if((playerHealthNew+ lifeStealValue)>playerHealthMax){
					playerHealthNew=playerHealthMax;
				}

				Player2HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/playerHealthMax);
				Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(playerHealthMax) + " HP";

				lifeSteal=false;
				
			}

//			if (EnemyStunned == true && StunTime>0) {
//
//				if (animator.enabled == true) {
//
//					animator.enabled = false;
//				}
//
//				StunTime -= Time.deltaTime;
//			} else {
//
//				if (animator.enabled == false) {
//					animator.enabled = true;
//				}
//
//				EnemyStunned = false;
//				StunTime = 1;
//			}

			yield return new WaitForSeconds(0.1f);
		}
		
	}

    public void eggCheck()
    {
        if(this.gameObject.name == "Egg")
        {

            if(playerHealthCurrent<= 0) //25%HP Left
            {
                if (ragdollScript.Head.GetComponent<SpriteRenderer>().sprite != egg3)
                {
                    ragdollScript.Head.GetComponent<SpriteRenderer>().sprite = egg3; //cracked open
                    Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().eggBreak();
                }
            }

            if (playerHealthCurrent > 0 && playerHealthCurrent < (double)(playerHealthMax * 0.33f)) //55%HP Left
            {
                //kinda 
                if (ragdollScript.Head.GetComponent<SpriteRenderer>().sprite != egg2)
                {
                    ragdollScript.Head.GetComponent<SpriteRenderer>().sprite = egg2; //cracked open
                    Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().eggCrack2();
                }
            }

            if (playerHealthCurrent > (double)(playerHealthMax * 0.33f) && playerHealthCurrent < (double)(playerHealthMax * 0.66f)) //75%HP Left
            {
                //kinda 
                if (ragdollScript.Head.GetComponent<SpriteRenderer>().sprite != egg1)
                {
                    ragdollScript.Head.GetComponent<SpriteRenderer>().sprite = egg1; //cracked open
                    Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().eggCrack();
                }
            }

            if (playerHealthCurrent > (double)(playerHealthMax * 0.66f) && playerHealthCurrent < playerHealthMax) //100%HP
            {
                //start egg
                ragdollScript.Head.GetComponent<SpriteRenderer>().sprite = egg0; //not cracked

            }
        }

    }

	public void resetBossHP(){


		playerHealthCurrent = playerHealthMax;
		playerHealthNew = playerHealthMax;

		Player2HealthBar.GetComponent<Slider> ().value = (float)(playerHealthCurrent/playerHealthMax);
		Player2HealthBar.GetComponentInChildren<TextMeshProUGUI>().text = LargeNumConverter.setDmg(playerHealthCurrent) + "/" + LargeNumConverter.setDmg(playerHealthMax); //where does it do this for the players hp?

	}

	public void createBone(){
		activateScreenDmg ();


//		if (BoneThrow != null) {
//			BoneThrow.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt (enemyDamage);
//
//			GameObject BoneObj = (GameObject)Instantiate (BoneThrow, new Vector3 (shotSpawn.transform.position.x, shotSpawn.transform.position.y, 0), Quaternion.Euler (0, 180, 0));
//
//			BoneObj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10, 0);
//		}
	}

	public void CreateFlameDOT(){
		
//		GameObject FlameDOTClone = (GameObject)Instantiate (FlameDOT, new Vector3 (Player2Torso.transform.position.x, Player2Torso.transform.position.y, 0), Quaternion.identity);
//		FlameDOTClone.transform.parent = this.transform;
//
//		if (playerHealthCurrent <= (Mathf.FloorToInt ((PlayerPrefs.GetFloat ("FireItemMod") + PlayerPrefs.GetFloat ("atkMod"))*10)/2)) {
//			playerHealthNew = 1;
//		}
//		if (playerHealthCurrent > (Mathf.FloorToInt ((PlayerPrefs.GetFloat ("FireItemMod") + PlayerPrefs.GetFloat ("atkMod"))*10)/2)) {
//			playerHealthNew -= (Mathf.FloorToInt ((PlayerPrefs.GetFloat ("FireItemMod") + PlayerPrefs.GetFloat ("atkMod"))*10)/2);
//		}
//
//		Camera.main.GetComponent<CampaignGameOrganizer> ().damageEnemyNotification (Mathf.FloorToInt ((PlayerPrefs.GetFloat ("FireItemMod") + PlayerPrefs.GetFloat ("atkMod"))*10)/2);
//		
	}

	public void initiateStun(){
//		player2Stun = true;
//		this.gameObject.GetComponent<Animator> ().SetBool ("IsStunned", true);
		
	}

	public void disableStun(){

//		this.gameObject.GetComponent<Animator>().SetBool("IsStunned", false);
//		player2Stun = false;
	}

	public void GolemRegularAtk(){
		activateScreenDmg ();

//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//		
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().disableAllParticleEffects (); //makes it so there are no spell types active on start
//		
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().stunPlayer = false;
//
//		
//		GameObject SkeleMagic = (GameObject)Instantiate (SkeleMagicHit, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//
//			GameObject ParticleMagic4 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageEarthEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//			
//			ParticleMagic4.SetActive(true);
//			
//			this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.EarthWoodBreak ();
//
//		this.gameObject.GetComponent<EnemyAI> ().enemyIntelligence = 4;

		}
		
	public void GolemStunAtk(){
		activateScreenDmg ();

//		player1TorsoPosition = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
//		
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().disableAllParticleEffects (); //makes it so there are no spell types active on start
//		
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().DamageToPlayer = Mathf.CeilToInt(enemyDamage);
//		
//		SkeleMagicHit.GetComponent<EnemyProjectileCollider> ().stunPlayer = true;
//
//		GameObject SkeleMagic = (GameObject)Instantiate (SkeleMagicHit, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity); //instantiates the beam offscreen
//		
//		GameObject ParticleMagic4 =Instantiate (SkeleMagic.GetComponent<EnemyProjectileCollider>().skeleMageEarthEffect, new Vector3 (player1TorsoPosition.x  , player1TorsoPosition.y, 0), Quaternion.identity) as GameObject; //instantiates the beam offscreen
//		
//		ParticleMagic4.SetActive(true);
//		
//		this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.EarthWoodBreak ();
//
//		this.gameObject.GetComponent<EnemyAI> ().enemyIntelligence = 14; //gives the stupid int after a stun so chain stuns less likely
//
		
	}	


	public void endFireSpellAnimation(){ //add ending keyframes to the respective animations

	if (HasParameter ("SkeleAttack", animator)==true) {

		animator.SetBool ("SkeleAttack", false);
		}

		if (HasParameter ("SkeleAttackCombo", animator) == true) {

			animator.SetBool ("SkeleAttackCombo", false);
			}
			
				if (HasParameter ("castWolfAttack", animator) == true) {

				animator.SetBool ("castWolfAttack", false);
				}
				
				if (HasParameter ("castWolfAttack2", animator) == true) {

					animator.SetBool ("castWolfAttack2", false);
					}
					
					if (HasParameter ("DefaultAtkP1", animator) == true) {

						animator.SetBool ("DefaultAtkP1", false);
						}
							if (HasParameter ("RegAtk", animator) == true) {
													
								animator.SetBool ("RegAtk", false);
							}
								if (HasParameter ("StunAtk", animator) == true) {
															
									animator.SetBool ("StunAtk", false);
								}
									if (HasParameter ("Atk", animator) == true) {
										
										animator.SetBool ("Atk", false);
									}
	}

	public bool HasParameter(string paramName, Animator animator)
	{
		foreach (AnimatorControllerParameter param in animator.parameters)
		{
			if (param.name == paramName) return true;
		}
		return false;
	}

	public int getSpellType()
	{ 
		return SpellTypeInt; 
	}

	public void setSpellType(int SpellType) 
	{
		SpellTypeInt = SpellType; 


	}

	public void BoneWhish(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().SwordSwish ();

	}

	public void FlameHandsWhish(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().FireballSound ();

	}

	public void enemySwooshes(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().enemyWoosh ();

	}
	
	public void BontHit(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().skelePunch ();
		
	}

	public void WolfBiteSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().WolfBite ();

	}

	public void skeleMageHitSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().skeleMageStaffHit ();
	}

	public void slimeBiteSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().slimeBite ();
	}

	public void swordSlimeAtkSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().swordSlimeAtk ();
	}

	public void mushGuyAtkSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().mushGuyAtk ();
	}

	public void SwordSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().SwordHit ();
		
	}

	public void werewolfAtkSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().werewolfAtk ();

	}

	public void ShamanAtkSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().shamanAtk ();

	}

	//misc sounds taken from the control player script

	public void playFireballSound(){

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().FireballSound ();
		//explosion sound will be attached to the fireball and played with collision
	}
	/*
	public void playSwordWhoosh(){
		
		this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.SwordSwish ();
	}
	
	public void playSwordHit(){
		this.gameObject.GetComponent<EnemyRagdoll> ().soundManager.SwordHit ();
	}
	*/
	public void waterBeamChargeUp(){
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().WaterBeamCharge ();
	}
	
	public void endWaterBeamSoundLoop(){
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().WaterBeamShotEnd ();
	}

	public void endTheGame(){
		GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().EndOfLevel ();
	}
}
