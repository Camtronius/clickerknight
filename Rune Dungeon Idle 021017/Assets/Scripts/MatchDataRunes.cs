﻿/*
 * Copyright (C) 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MatchDataRunes {
    
	private const int Header = 600673; // sanity check for serialization

	public List<float> RunesIncoming = new List<float>(); //this is for generating the next blocks incoming

	public List<float> mRuneTypes = new List<float>();

	public List<float> Player1Blocks = new List<float>();
	
	public List<float> playersAwake = new List<float>(); //not sure what this does...

	public List<float> playersHealth = new List<float>(); //list which holds player health

	public List<float> lastSpellTypeCasted = new List<float> (); //determines the spell type the player last casted

	//player effects
	public List<float> checkInvis = new List<float> (); //checks if the player is invis or not

	public List<float> checkStun = new List<float> (); //checks if the player is stunned or not

	public List<float> FlameDOT = new List<float> (); //checks if the player has the Flame DOT applied to him or not

	public List<float> checkShield = new List<float> ();

	public List<float> checkConfusion = new List<float> ();

	public List<float> checkBurnt = new List<float> ();

	public List<float> checkDrowned = new List<float> ();

	public List<float> turnCount = new List<float> ();

	public List<float> playerGear = new List <float> ();

	public List<float> playerAttributes = new List <float> ();

	public List<float> didCrit = new List <float> ();

	public List<float> swordEnchnt = new List <float> ();
	
	public string networkStatus = "";

	public MatchDataRunes() {

    }

    public MatchDataRunes(byte[] b) : this() {
        if (b != null) {

            ReadFromBytes(b); //this calls the read from bytes method

			networkStatus = networkStatus + " has read MatchDR byte b"; //lets me know whats happening

        }
    }

    public byte[] ToBytes() { //write my data to mem

        MemoryStream memStream = new MemoryStream(); //opens up file writing abilities
        BinaryWriter w = new BinaryWriter(memStream); //so we can write to memory
        
		w.Write(Header); //writes header ID to memory

		w.Write( mRuneTypes.Count 
		        + Player1Blocks.Count
		        + playersHealth.Count
		        + lastSpellTypeCasted.Count
		        + checkInvis.Count
		        + checkStun.Count
		        + FlameDOT.Count
		        + checkShield.Count
		        + RunesIncoming.Count
		        + checkConfusion.Count
		        + checkBurnt.Count
		        + checkDrowned.Count
		        + turnCount.Count
		        + playerGear.Count
		        + playerAttributes.Count
		        + didCrit.Count
		        + swordEnchnt.Count); //writes the amount of runes contained in the list

	//	Debug.Log ("the count of mRuneTypes  is : " + mRuneTypes.Count);

	//	Debug.Log ("the count of player 1 blocks  is : " + Player1Blocks.Count);

	//	Debug.Log ("the count of playerHealth is : " + playersHealth.Count);

	//	Debug.Log ("the count of last spell type casted is : " + lastSpellTypeCasted.Count);

	//	Debug.Log ("the count of checkinvis  is : " + checkInvis.Count);
		
	//	Debug.Log ("the count of checkstun is : " + checkStun.Count);

	//	Debug.Log ("the count of flamedot  is : " + FlameDOT.Count);
		
	//	Debug.Log ("the count of checkshield  is : " + checkShield.Count);

//		Debug.Log ("the count of RunesIncoming is : " + RunesIncoming.Count);

//		Debug.Log ("the count of checkConfusion is : " + checkConfusion.Count);

//		Debug.Log ("the count of checkBurnt is : " + checkBurnt.Count);

//		Debug.Log ("the count of checkBurnt is : " + checkDrowned.Count);

//		Debug.Log ("amount of Turn Count is : " + turnCount.Count);

//		Debug.Log ("amount of Playeer Gear is : " + playerGear.Count);

//		Debug.Log ("player attributes count is : " + playerAttributes.Count);

//		Debug.Log ("player crit count is : " + didCrit.Count);



		for (int i =0; i< mRuneTypes.Count; i++) { //iterates throught the list and writes to memory

			w.Write(mRuneTypes[i]);  //writing the runes to mem

	//		Debug.Log( "(Write) color val is: " + mRuneTypes[i]); //shows that it is being writed

		}

		for(int j=0; j<Player1Blocks.Count; j++){

			w.Write(Player1Blocks[j]);

	//		Debug.Log ("(Write) the player 1 blocks!");

		}

		for(int k=0; k<playersHealth.Count; k++){
			
			w.Write(playersHealth[k]);
			
	//		Debug.Log ("(Write) the players health!");
			
		}

		for(int l=0; l<lastSpellTypeCasted.Count; l++){
			
			w.Write(lastSpellTypeCasted[l]);
			
	//		Debug.Log ("(Write) Last spell type casted is" + lastSpellTypeCasted[l]);

		}

		for(int m=0; m<checkInvis.Count; m++){
			
			w.Write(checkInvis[m]);
			
	//		Debug.Log ("(Write) isinvis is" + checkInvis[m]);
			
		}

		for(int n=0; n<checkStun.Count; n++){
			
			w.Write(checkStun[n]);
			
	//		Debug.Log ("(Write) checkStun is" + checkStun[n]);
			
		}

		for(int o=0; o<FlameDOT.Count; o++){
			
			w.Write(FlameDOT[o]);
			
	//		Debug.Log ("(Write) FlameDOT is" + FlameDOT[o]);
			
		}

		for(int p=0; p<checkShield.Count; p++){
			
			w.Write(checkShield[p]);
			
//			Debug.Log ("(Write) CheckShield is" + checkShield[p]);

		}

		for(int q=0; q<RunesIncoming.Count; q++){
			
			w.Write(RunesIncoming[q]);
			
	//		Debug.Log ("(Write) Incoming Runes are: " + RunesIncoming[q]);

		}

		for(int r=0; r<checkConfusion.Count; r++){
			
			w.Write(checkConfusion[r]);

//			Debug.Log ("(Write) is Confused are: " + checkConfusion[r]);

		}

		for(int s=0; s<checkBurnt.Count; s++){

			w.Write(checkBurnt[s]);
			
	//		Debug.Log ("(Write) is Burnt are: " + checkBurnt[s]);
			
		}

		for(int t=0; t<checkDrowned.Count; t++){
			
			w.Write(checkDrowned[t]);
			
	//		Debug.Log ("(Write) is Drowned are: " + checkDrowned[t]);

		}

		for(int u=0; u<turnCount.Count; u++){
			
			w.Write(turnCount[u]);
			
//			Debug.Log ("(Write) (turn count ) is --------->: " + turnCount[u]);
			
		}

		for(int v=0; v<playerGear.Count; v++){
			
			w.Write(playerGear[v]);
			
	//		Debug.Log ("(Write) (Player Gear Amount is " + playerGear[v]);
			
		}

		for(int x=0; x<playerAttributes.Count; x++){
			
			w.Write(playerAttributes[x]);
			
	//		Debug.Log ("(Write) (Player Attributes are " + playerAttributes[x]);
			
		}

		for(int y=0; y<didCrit.Count; y++){
			
			w.Write(didCrit[y]);
			
//			Debug.Log ("(Write) (Player Crit are " + didCrit[y]);
			
		}

		for(int z=0; z<swordEnchnt.Count; z++){
			
			w.Write(swordEnchnt[z]);
			
			//			Debug.Log ("(Write) (Player Crit are " + didCrit[y]);
			
		}

		networkStatus = "Tobytes() wrote to mem";

        w.Close(); //closes the write stream

        byte[] buf = memStream.GetBuffer(); //removes fluff from the byte array

        memStream.Close(); //closes mem stream

        return buf; //returned the buffered array
    }

    public void ReadFromBytes(byte[] b) { //why is the "b" there? //change this back to public

        BinaryReader r = new BinaryReader(new MemoryStream(b));
        
		int header = r.ReadInt32(); //reads the header to make sure its the same version?
       
		if (header != Header) {
            // we don't know how to parse this version; user has to upgrade game
            throw new UnsupportedMatchFormatException("Board data header " + header +
                    " not recognized.");
        }

		mRuneTypes.Clear(); //clears the list so if there are any existing items in the list, they wont interfere with what is being added
		Player1Blocks.Clear ();
		playersHealth.Clear ();
		lastSpellTypeCasted.Clear ();
		checkStun.Clear (); //these were added 7/25/15 so they may be messing stuff up if something doesnt work...
		checkInvis.Clear (); //these were added 7/25/15 so they may be messing stuff up if something doesnt work...
		FlameDOT.Clear (); //these were added 7/25/15 so they may be messing stuff up if something doesnt work...
		checkShield.Clear ();
		RunesIncoming.Clear ();
		checkConfusion.Clear ();
		checkBurnt.Clear ();
		checkDrowned.Clear ();
		turnCount.Clear ();
		playerGear.Clear ();
		playerAttributes.Clear ();
		didCrit.Clear ();
		swordEnchnt.Clear ();

//		Debug.Log ("mrunetypes/Player1blocks/playerhealths/lastspellcasted has been cleared...");

		int byteCounter = r.ReadInt32(); //reads the amount of bytes contained in the byte array 32 worked, lets see if we can add more

//		Debug.Log ("bytecounter is : " + byteCounter);

		for (int i = 0; i < byteCounter; i++) {
			//Debug.Log("running for loop");

			if(i<4){
			float runeIntDesignation;

			runeIntDesignation = r.ReadSingle(); //reads the runes stored

	//		Debug.Log( "(Read) the bytes the G val " + runeIntDesignation);

			mRuneTypes.Add(runeIntDesignation); //adds them to mRuneTypes

			}
			if(i>=4 && i<20){

				float player1BlocksDesignation;
				
				player1BlocksDesignation = r.ReadSingle(); //reads the runes stored
				
//				Debug.Log( "(Read) the player1BlocksDesignation " + player1BlocksDesignation);
				
				Player1Blocks.Add(player1BlocksDesignation); //adds them to mRuneTypes

			}

			if(i>=20 && i<24){

				float playerHealths;
				
				playerHealths = r.ReadSingle(); //reads the runes stored
				
//				Debug.Log( "(Read) playerHealths are " + playerHealths);

				playersHealth.Add(playerHealths); //adds them to mRuneTypes


			}

			if(i>=24 && i<26){ //this is for the last spell type casted
				
				float primarySpellType;
				
				primarySpellType = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) primarySpellTypes are " + primarySpellType);
				
				lastSpellTypeCasted.Add(primarySpellType); //adds them to mRuneTypes
				
				
			}

			if(i>=26 && i<28){ //this is for the last spell type casted
				
				float isPlayerInvis; //1 is invis, 0 not invis
				
				isPlayerInvis = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) Is invis? " + isPlayerInvis);
				
				checkInvis.Add(isPlayerInvis); //adds them to mRuneTypes

			}

			if(i>=28 && i<30){ //this is for the last spell type casted
				
				float isPlayerStunned; //1 is invis, 0 not invis
				
				isPlayerStunned = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read) Is Player Stunned? " + isPlayerStunned);
				
				checkStun.Add(isPlayerStunned); //adds them to mRuneTypes
				
			}

			if(i>=30 && i<32){ //this is for the last spell type casted
				
				float isPlayerFlameDOT; //1 is invis, 0 not invis
				
				isPlayerFlameDOT = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) Does player have a Flame DOT? " + isPlayerFlameDOT);
				
				FlameDOT.Add(isPlayerFlameDOT); //adds them to mRuneTypes
				
			}

			if(i>=32 && i<34){ //this is for the last spell type casted
				
				float isPlayerShield; //1 is invis, 0 not invis
				
				isPlayerShield = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read) Does player have a SHIELD? " + isPlayerShield);
				
				checkShield.Add(isPlayerShield); //adds them to mRuneTypes
				
			}

			if(i>=34 && i<38){ //this is for the random runes creator
				
				float incomingRune; //numbering follows that of the runes
				
				incomingRune = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read) The incoming rune is? " + incomingRune);
				
				RunesIncoming.Add(incomingRune); //adds them to mRuneTypes
				
			}

			if(i>=38 && i<40){ //this is for the random runes creator
				
				float isConfused; //numbering follows that of the runes
				
				isConfused = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) The confusion is? " + isConfused);
				
				checkConfusion.Add(isConfused); //adds them to mRuneTypes
				
			}

			if(i>=40 && i<42){ //this is for checking if the blocks have been burnt
				
				float isBurnt; //numbering follows that of the runes
				
				isBurnt = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) The Burnt val is? " + isBurnt);
				
				checkBurnt.Add(isBurnt); //adds them to burnt list
				
			}

			if(i>=42 && i<44){ //this is for checking if the blocks have been burnt
				
				float isDrowned; //numbering follows that of the runes
				
				isDrowned = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read) The Drowned val is? " + isDrowned);
				
				checkDrowned.Add(isDrowned); //adds them to burnt list
				
			}
			if(i>=44 && i<45){ //this is for checking if the blocks have been burnt
				
				float turnNumber; //numbering follows that of the runes
				
				turnNumber = r.ReadSingle(); //reads the runes stored
				
		//		Debug.Log( "(Read)  Turn Number is ------>" + turnNumber);
				
				turnCount.Add(turnNumber); //adds them to burnt list
				
			}

			if(i>=45 && i<57){ //this is for checking if the blocks have been burnt
				
				float gearNumber; //numbering follows that of the runes
				
				gearNumber = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read)  Player gear number is" + gearNumber);
				
				playerGear.Add(gearNumber); //adds them to burnt list
				
			}

			if(i>=57 && i<73){ //this is for checking if the blocks have been burnt
				
				float attributeNumber; //numbering follows that of the runes
				
				attributeNumber = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read)  Player attribute is" + attributeNumber);

				playerAttributes.Add(attributeNumber); //adds them to burnt list
				
			}

			if(i>=73 && i<75){ //this is for checking if the blocks have been burnt
				
				float critNum; //numbering follows that of the runes
				
				critNum = r.ReadSingle(); //reads the runes stored
				
	//			Debug.Log( "(Read)  Player crit is" + critNum);
				
				didCrit.Add(critNum); //adds them to burnt list
				
			}

			if(i>=75 && i<76){ //this is for checking if the blocks have been burnt
				
				float enchNum; //numbering follows that of the runes
				
				enchNum = r.ReadSingle(); //reads the runes stored
				
				//			Debug.Log( "(Read)  Player crit is" + critNum);
				
				swordEnchnt.Add(enchNum); //adds them to burnt list
				
			}

		}

	//	Debug.Log ("running for each loop");

		networkStatus = "ReadFromBytes complete";
    }

    public class UnsupportedMatchFormatException : System.Exception {
        public UnsupportedMatchFormatException(string message) : base(message) {}
    }
}
