﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class FreezeTimeCooldown : MonoBehaviour {

	public GameObject freezeAnimation;
	// Use this for initialization
	private float CoolDownTimeMax;
	public float CoolDownTime;

	public float FreezeTimeMax;
	public float FreezeTime;

	public bool enemyFrozen = false;

	public Image CoolDownGfx;
	public bool isReady=false;
	public TextMeshProUGUI timeCountDown;
//	public TextMeshProUGUI abilityCountDown;

	public string timeString;
	public ControlEnemy enemyObj;
	public MainMenu cameraObj;

	void OnEnable(){

		updateCooldownTimeMax ();

		if (PlayerPrefs.GetInt ("FreezeTime") > 0 && PlayerPrefs.GetInt ("FreezeTimeRdy") == 0) {
			CoolDownTime = (float)PlayerPrefs.GetInt ("FreezeTime");
			timeCountDown.text = CoolDownTime.ToString (); 

		} else if (PlayerPrefs.GetInt ("FreezeTimeRdy") == 1) {
			isReady = true; //they saved the megaslash
			timeCountDown.text = "";
			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

		} else {
			CoolDownTime = CoolDownTimeMax;
			timeCountDown.text = CoolDownTime.ToString (); 
		}

		CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

	}
	
	// Update is called once per frame
	void Update () {


		if (FreezeTime > 0 && enemyFrozen==true) {

	//		abilityCountDown.text = FreezeTime.ToString ();
			FreezeTime -= Time.deltaTime;


		} else if (FreezeTime<=0 && enemyFrozen==true) {

			enemyObj = Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();
			if (enemyObj != null) {
				enemyObj.gameObject.GetComponent<Animator> ().enabled = true;
				enemyObj.isFrozen = false;
			}
			enemyFrozen = false;

		}


	}

	public void updateCooldownTimeMax(){
		 //this is in case the prestige talent is taken
		CoolDownTimeMax = 26f - (float)cameraObj.FreezeTimePrestige;


	}

	public void decreaseFreezeCooldown(){

		if (CoolDownTime > 0) {

			if (CoolDownTime>0) {
				PlayerPrefs.SetInt ("FreezeTime", Mathf.CeilToInt(CoolDownTime));
				PlayerPrefs.SetInt ("FreezeTimeRdy", 0);


			}

			CoolDownTime -= 1.00f * (1 + cameraObj.petManager.abilityArray[1]); 



			isReady = false;
			timeString = CoolDownTime.ToString ();

            if (timeString.Length > 2)
            {
                timeString = timeString.Substring(0, 2); //10B
            }

            timeCountDown.text = timeString;
			CoolDownGfx.fillAmount = ((float)CoolDownTime / (float)CoolDownTimeMax);

			if (CoolDownTime <= 0) {
				isReady = true;
				PlayerPrefs.SetInt ("FreezeTimeRdy", 1);

				timeCountDown.text = "";
			}


		}
		updateCooldownTimeMax ();

	}

	public void FreezeTimePress(){
		
		if (isReady == true) {

			freezeAnimation.SetActive (true);

			enemyObj = Camera.main.GetComponent<MainMenu>().campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();

			enemyObj.gameObject.GetComponent<Animator> ().enabled = false;

			enemyObj.isFrozen = true;

			FreezeTime = cameraObj.FreezeTimeTime; //amount of freeze time given by the camera

			enemyFrozen = true;

			CoolDownTime = CoolDownTimeMax;

			cameraObj.persistantSoundManager.GetComponent<SoundManager> ().FreezeTime ();

			this.gameObject.GetComponentInParent<ActiveAbilityManager> ().unlockWeaponAnim (this.gameObject.transform, this.gameObject.GetComponentInParent<ActiveAbilityManager> ().FreezeTimeImg.sprite);//this is to play the animation, the name of the method is wrong but i was too lazt to change

		}
	}

}
