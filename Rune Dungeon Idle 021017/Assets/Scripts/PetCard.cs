﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PetCard : MonoBehaviour {

    public List<Sprite> petSpriteList = new List<Sprite>();

    public Image petCardSprite;
    public Image petSprite;
    public TextMeshProUGUI petNameTmpro;

    public List<Sprite> cardRaritySprites = new List<Sprite>();
    public PetsMaster petMasterScript;

    public int index;//this is the index of the card in the list of cards of PetMaster script

    public int petType = 0; //1 if attak typ, 2 if magic typ, 3 is hybrid or health type??

    public int petLvInt;
    public int evoLvInt;
    public int rarity; //0 is common //1 is uncommon //2 is rare //3 is epic //4 is Unique (only bought)

    public string petName;
    public string ability;
    public string description;

    public bool isUnlocked = false;

    public GameObject petLock;
    public GameObject selected;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void loadRarityCard() //the card is assigned a rarity in petsmaster, then this is called from petsmaster
    {
        switch (rarity)
        {
            case 0:
                petCardSprite.sprite = cardRaritySprites[0];//common
                break;
            case 1:
                petCardSprite.sprite = cardRaritySprites[1];//uncommon
                break;
            case 2:
                petCardSprite.sprite = cardRaritySprites[2];//rare
                break;
            case 3:
                petCardSprite.sprite = cardRaritySprites[3];//epic
                break;
            case 4:
                petCardSprite.sprite = cardRaritySprites[4];//unique
                break;

        }

            if (rarity == 4 && isUnlocked == false)//these are the only cards that should have a lock
            {
                petLock.SetActive(true);

            }else if (isUnlocked == true)
            {
                petLock.SetActive(false);

            }

    }

    public void loadEvoBar()
    {
        //faded color is #00000055
        //normal color is #FFFFFFFF

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#00000096", out myColorFaded);

        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);


        if (petMasterScript.petGenScript.UnlockedPetsLevel[index] < 30)//if pets level is <30
        {
            evoLvInt = 1;
            petMasterScript.evo1.sprite = petSpriteList[0];
            if (isUnlocked == true || rarity==4)
            {
                petMasterScript.evo1.color = myColorNorm;
            }
            else
            {
            petMasterScript.evo1.color = myColorFaded;
            }

            petMasterScript.evo2.sprite = petSpriteList[1];
            petMasterScript.evo2.color = myColorFaded;

            petMasterScript.evo3.sprite = petSpriteList[2];
            petMasterScript.evo3.color = myColorFaded;



        }
        else if(petMasterScript.petGenScript.UnlockedPetsLevel[index] >= 30 && petMasterScript.petGenScript.UnlockedPetsLevel[index]<60) //if pets level is between 60 and 30
        {
            evoLvInt = 2;
            petMasterScript.evo1.sprite = petSpriteList[0];

            petMasterScript.evo2.sprite = petSpriteList[1];
            petMasterScript.evo2.color = myColorNorm;

            petMasterScript.evo3.sprite = petSpriteList[2];
            petMasterScript.evo3.color = myColorFaded; //because evo form 3 wouldnt have been unlocked



        }
        else if (petMasterScript.petGenScript.UnlockedPetsLevel[index] >= 60) //if pets lev > 60
        {
            evoLvInt = 3;
            petMasterScript.evo1.sprite = petSpriteList[0];

            petMasterScript.evo2.sprite = petSpriteList[1];
            petMasterScript.evo2.color = myColorNorm;

            petMasterScript.evo3.sprite = petSpriteList[2];
            petMasterScript.evo3.color = myColorNorm;



        }

    }

    public void assignPetSprite()
    {

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#00000096", out myColorFaded);

        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);

        if (petMasterScript.petGenScript.UnlockedPetsLevel[index] < 30)//if pets level is <30
        {
            evoLvInt = 1;

            if (isUnlocked == true || rarity==4)
            {
                petSprite.color = myColorNorm;
            }
            else
            {
                petSprite.color = myColorFaded;

            }

            petSprite.sprite = petSpriteList[0];
        }
        else if (petMasterScript.petGenScript.UnlockedPetsLevel[index] >= 30 && petMasterScript.petGenScript.UnlockedPetsLevel[index] < 60) //if pets level is between 60 and 30
        {
            evoLvInt = 2;
            petSprite.sprite = petSpriteList[1];

        }
        else if (petMasterScript.petGenScript.UnlockedPetsLevel[index] >= 60) //if pets lev > 60
        {
            evoLvInt = 3;
            petSprite.sprite = petSpriteList[2];

        }

    }

    public void TaskOnClick()
    {

        //FOR TESTING PURPOSES

        //END TESTIN PURPOSES
        loadEvoBar(); //loads the evolutuon stages of the pets. Is needed for petmasterevobar and also for assigning pet sprites. if evonum is not know, u cant assign the pet

        petMasterScript.deselectAll();//makes sure no other cards are shown as selected
        selected.SetActive(true); //shows the pet as selected

        petMasterScript.petNameAndLv.text = petMasterScript.petGenScript.PetName[index] + " LV" + petMasterScript.petGenScript.UnlockedPetsLevel[index]; // gives the pets name and its current level

            //this modifies the string to replace it with the current pet ability mod 
            string PetAbilityText = petMasterScript.petGenScript.PetAbility[index];

            //this will be changed once evolving pets becomes a thing *************NOTE:EVO LV IS AT 1 from the start method
            float totalAbilValue = petMasterScript.petGenScript.PetAbilityModifier[index]*evoLvInt*100;
            float totalAbilValue2 = petMasterScript.petGenScript.PetAbilityModifier2[index]*evoLvInt*100;

            string output = PetAbilityText.Replace("KEY", "<color=#FFF935FF>" + "+" + (totalAbilValue).ToString() + "%</color>");
            string outputExtra = output.Replace("LEE", "<color=#F20000FF>" + (totalAbilValue2).ToString() + "%</color>"); //runs a 2nd replace function in case there is a 2nd factor of this ability
            string outputExtraExtra = outputExtra.Replace("ZEE", "<color=#FFF935FF>" + "+" + (totalAbilValue).ToString() + "</color>");


        petMasterScript.abilityText.text = outputExtraExtra;



            //this modifies the string to replace it with the current pet desc mod (it should eventually multiply it by the pet LV too
            string PetDescriptionText = petMasterScript.petGenScript.PetDesc[index];

            //because pets will start at lv1 we have to subtract one
            float totalDescValue = (petMasterScript.petGenScript.PetDescModifier[index] + petMasterScript.petGenScript.DescGrowth[index]*(petMasterScript.petGenScript.UnlockedPetsLevel[index]-1)) * 100;

            string output2 = ""; //this is a placeholder so we can modify it below

            if (petType == 1) //if its atk typ
            {
                output2 = PetDescriptionText.Replace("KEY", "<color=#FF0000FF>" + "+" + (totalDescValue).ToString() + "%</color>");

            }
            else //if its mag typ
            {
                output2 = PetDescriptionText.Replace("KEY", "<color=#4AF3FFFF>" + "+" + (totalDescValue).ToString() + "%</color>");

            }

            petMasterScript.descriptionText.text = output2;

            petMasterScript.currentSelectedIndex = index; //gives the petmaster script our index so we know what we are upgrading

        //grwoth rate

        petMasterScript.growthText.text = "Gains + " + ((petMasterScript.petGenScript.DescGrowth[index] * 100)).ToString() + "% Damage per Pet Level";

            //the index finds the level of this pet, and the upgrade cost is determined by the pet level. Need -1 because the pets will default start at level 1
            petMasterScript.upgradeGoldCost = petMasterScript.petGenScript.petUpgradeCost[petMasterScript.petGenScript.UnlockedPetsLevel[index]-1]; 
            petMasterScript.petUpgradeCost.text = LargeNumConverter.setDmgShort(petMasterScript.upgradeGoldCost);

            //if player cannot afford switch upgrade button sprite

            if(Camera.main.GetComponent<MainMenu>().playerObj.playerGold<petMasterScript.upgradeGoldCost) //if they cannot afoord the upgrade make it red
            {

                petMasterScript.upgradeButtonImage.sprite = petMasterScript.cantUpgradeSprite;

            }
            else //they can afford so make it blue
            {

                petMasterScript.upgradeButtonImage.sprite = petMasterScript.UpgradeSprite;

            }

            if (isUnlocked == false)
            {
            //if you havent found this pet
            petMasterScript.equipButton.SetActive(false);
            petMasterScript.upgradeButton.SetActive(false);

                if (rarity == 4)
                {
                petMasterScript.premiumPetDisclaimer.SetActive(true);
                petMasterScript.notUnlcokedPetDisclaimer.SetActive(false);

                }
                else
                {
                petMasterScript.premiumPetDisclaimer.SetActive(false);
                petMasterScript.notUnlcokedPetDisclaimer.SetActive(true);
            }


            }
            else //if the pet is unlocked
            {
            petMasterScript.equipButton.SetActive(true);
            petMasterScript.upgradeButton.SetActive(true);
            petMasterScript.premiumPetDisclaimer.SetActive(false);
            petMasterScript.notUnlcokedPetDisclaimer.SetActive(false);

            if (petLvInt >= MainMenu.LVCAP) //so they cant go over the cap with upgrading pets
            {
                petMasterScript.upgradeButton.SetActive(false);
            }

        }

        //add growth/lv


        //this needs to stay here

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#00000096", out myColorFaded);

        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);

        if (isUnlocked == true || rarity == 4)
        {
            petMasterScript.petSprite.color = myColorNorm;
        }
        else
        {
            petMasterScript.petSprite.color = myColorFaded;

        }

        switch (evoLvInt)
        {
            case 1:
                petMasterScript.petSprite.sprite = petSpriteList[0];

                break;
            case 2:
                petMasterScript.petSprite.sprite = petSpriteList[1];

                break;
            case 3:
                petMasterScript.petSprite.sprite = petSpriteList[2];

                break;

        }

            petMasterScript.petCardBkg.sprite = petMasterScript.cardRarityBkgs[rarity];
            //end (this needs to stay here)


            switch (petType){

                case 0:

                    //0 is not a type so dont do anything
                break;

                case 1:
                    petMasterScript.petTypeText.text = "Type: <color=#FF0000FF>ATK</color>"; //atk type

                break;

                case 2:
                    petMasterScript.petTypeText.text = "Type: <color=#4AF3FFFF>MAG</color>"; //magic type

                break;


            }




        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();
       
    }
}
