// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIDS
{
        public const string achievement_wind_one = "CgkIvsac4ekTEAIQCw"; // <GPGSID>
        public const string achievement_fire_master = "CgkIvsac4ekTEAIQAw"; // <GPGSID>
        public const string achievement_water_blessed = "CgkIvsac4ekTEAIQBA"; // <GPGSID>
        public const string achievement_water_master = "CgkIvsac4ekTEAIQBg"; // <GPGSID>
        public const string achievement_earth_guided = "CgkIvsac4ekTEAIQBw"; // <GPGSID>
        public const string achievement_earth_master = "CgkIvsac4ekTEAIQCQ"; // <GPGSID>
        public const string achievement_wind_master = "CgkIvsac4ekTEAIQDA"; // <GPGSID>
        public const string achievement_story_driven = "CgkIvsac4ekTEAIQDg"; // <GPGSID>
        public const string achievement_earth_spirit = "CgkIvsac4ekTEAIQCA"; // <GPGSID>
        public const string achievement_fire_wielder = "CgkIvsac4ekTEAIQAg"; // <GPGSID>
        public const string achievement_wind_chosen = "CgkIvsac4ekTEAIQCg"; // <GPGSID>
        public const string achievement_water_weaver = "CgkIvsac4ekTEAIQBQ"; // <GPGSID>
        public const string achievement_fire_touched = "CgkIvsac4ekTEAIQAQ"; // <GPGSID>
        public const string achievement_merchant_master = "CgkIvsac4ekTEAIQDQ"; // <GPGSID>

}

