﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Analytics.Experimental;


public class PetsMaster : MonoBehaviour {


    public List<PetCard> ListOfPets = new List<PetCard>();
    public List<Sprite> cardRarityBkgs = new List<Sprite>();

    public List<Image> equipSlot = new List<Image>();
    public List<GameObject> equipSlotLock = new List<GameObject>();

    public SpriteToggle2 petsToggle; //b/c unity's toggle system blows
    public SpriteToggle2 heroToggle;

    public TextMeshProUGUI petsToggleText; //b/c unity's toggle system blows
    public TextMeshProUGUI heroToggleText;

    public Image petSprite;
    public Image petCardBkg;

    public Image evo1;
    public Image evo2;
    public Image evo3;

    public GameObject petCardHolder; //this is the scrollrect
    public GameObject petCardPrefab;
    public PetGenerator petGenScript; //this holds all the Pet dictionary stuff

    public GameObject heroPage;
    public GameObject heroItemDescription;

    public GameObject petsPage;
    public GameObject petsEquipPage;
    public CheckLevelsComplete levelsCompleteScript;

    public TextMeshProUGUI petNameAndLv;
    public TextMeshProUGUI abilityText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI petTypeText;
    public TextMeshProUGUI growthText;


    public TextMeshProUGUI petUpgradeCost;

    public Sprite UpgradeSprite;
    public Sprite cantUpgradeSprite;
    public Sprite blankTransparentSprite;

    public Image upgradeButtonImage;

    public GameObject equipButton;
    public GameObject upgradeButton;
    public GameObject premiumPetDisclaimer;
    public GameObject notUnlcokedPetDisclaimer;


    public IngredientsMaster ingredientsScript;
    public PetManager petManager;
    public double upgradeGoldCost;
    public int currentSelectedIndex = -1; //this is the index given by the card script, so we know which
    
    // Use this for initialization
    void OnEnable () {

        loadPetsAtStart();
    }

    public void loadPetsAtStart()//need this to be called from other methods
    {
        petGenScript.loadPetsUnlocked(); //this will load the arrays of PetsUnlocked, and PetsUnlockedLevels, and pets equpped
        petGenScript.loadPetsText(); //this loads the text name, ability, description, and rarity
        petGenScript.loadPetSprites(); //loads the pet sprites into a dictionary of sprites.

        generatePetCards(); //it will not generate if they already exist!

        updatePetCards();//this will be to add the data to each individual pet card

        checkPetUnlocked(); //this is where the currently selected index will be modified at start. For example, if a pet is automatically selected to represent the page. It will be give this index

        checkPetEquipped(); //this checks which pets are currently equoipped, so you can select the slot to put your pet

    }

    public void checkPetEquipped() //loads the sprites for the pets that are equipped
    {

        for(int i=0; i < 3; i++)// b/c there are 3 slots
        {
            if (petGenScript.petIndexEquipped[i] >= 0) //because -1 means no pet is equipped in that slot
            {

                equipSlot[i].sprite = ListOfPets[petGenScript.petIndexEquipped[i]].petSprite.sprite; //gives the correspoding PETID in the list of pets to get the sprite. For example if the pet index equipped is 5, it will get the 5th pet in the list of pets and get the associated sprite
                petManager.petsList[i].GetComponent<Image>().sprite  = ListOfPets[petGenScript.petIndexEquipped[i]].petSprite.sprite;
                petManager.petsList[i].SetActive(true);
            }

        }

    }

    public void equipPet(int petSlot)
    {

        //check int selected (aka as the slot)
        if (petSlot == 0)
        {
            checkForDuplicatesEquip(petSlot);
        }
        else if (petSlot == 1) //need to have completed lv30
        {

            if (levelsCompleteScript.unlockedLevelArray[30] == 1)//this means the level has been achieved
            {
                checkForDuplicatesEquip(petSlot);

            }
            else
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("You must get to World 30 to unlock this slot!");

            }

        }
        else if (petSlot == 2) //need to have completed lv60
        {

            if (levelsCompleteScript.unlockedLevelArray[60] == 1)//this means the level has been achieved
            {
                checkForDuplicatesEquip(petSlot);

            }
            else
            {
                Camera.main.GetComponent<MainMenu>().showErrorMsg("You must get to World 60 to unlock this slot!");

            }

        }
        //check if the slot is unlocked IE you have gotten to the world required to use that slot

        petManager.getNewArrayValues(); //this changes the modifiers for the abilities

    }

    public void checkForDuplicatesEquip(int index)
    {

        bool duplicateFound = false;

        for (int i = 0; i < 3; i++)
        {
            if (currentSelectedIndex == petGenScript.petIndexEquipped[i]) //checks if that index is anywhere else in the slots
            {
                duplicateFound = true;
            }
        }

        if (duplicateFound == false)
        {
            petGenScript.petIndexEquipped[index] = currentSelectedIndex;

            petGenScript.savePetsUnlocked(); //this saves the arry of equipped pets

            //update the icons now...
            checkPetEquipped(); //this should update the icons of the equipped list

            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().closeEquipPet();

            AnalyticsEvent.Custom("PetIndexEquipped", new Dictionary<string, object> {

                {currentSelectedIndex.ToString(),1},
            });

        }

        else
        {

            Camera.main.GetComponent<MainMenu>().showErrorMsg("That pet is already equipped in a slot!");

        }

    }

    public void checkPetUnlocked()
    {
        ////find first pet selected

        bool petUnlocked = false; //if there is a petunlocked it will set this bool to true
        bool noPetEquipped = true; //this is if the player owns a pet but its not equipped

        for (int i = 0; i < petGenScript.UnlockedPets.Length; i++) //check for any pets unlocked
        {
            if (petGenScript.UnlockedPets[i] == 1)
            {
                petUnlocked = true;
            }
        }

        if (petUnlocked == true)
        {

            for (int j = 0; j < petGenScript.petIndexEquipped.Length; j++)//loads the first pet equipped
            {
                if (petGenScript.petIndexEquipped[j] >= 0)//-1 for  no pet equipped in that slot
                {
                    ListOfPets[petGenScript.petIndexEquipped[j]].TaskOnClick();

                    noPetEquipped = false;
                    break;
                }

            }

            if (noPetEquipped == true) { 

                for (int i = 0; i < petGenScript.UnlockedPets.Length; i++)//if no pet equipped, load first pet unlocked
                {
                    if (petGenScript.UnlockedPets[i] == 1)
                    {
                        ListOfPets[i].TaskOnClick();
                        break;
                    }
                }

            }
           
        }
        else //if no pets are unlocked, load the first pet, the fox as a display
        {
            ListOfPets[0].TaskOnClick();//take one for the team foxy boi

        }

        ////if you cannot find the pet, then make everything blank. This way you arent upgrading a pet that doesnt exist!
    }

    public void generatePetCards()
    {

        //NOTE MAKES SURE THE PLAYER CAN ONLY GENERATE THE PETS THAT ARE AVAILABLE. IN OTHER WORDS -- THERE SHOULD BE NO DUPLICATE NUMBER PETS! You can do this by modifying the Random.Range Values to make sure only a total
        //of 10 numbers will come up in the "Pet Generator script"!!!!!!!!!!!!

        //ALSO, IF ANYTHING HERE CHANGES, THE UPDATE PET SCRIPT ALSO NEEDS TO CHANGE

        if (ListOfPets.Count == 0)//there are no pets cards and they need to be generated
        {

            for(int i=0; i<32; i++)//10 amount of pets max at the moment
            {
                GameObject PetCardObj = (GameObject)Instantiate(petCardPrefab, petCardHolder.gameObject.transform);

                PetCard petCardScript = PetCardObj.GetComponent<PetCard>();

                ListOfPets.Add(petCardScript);

            }

        }

        updatePetCards(); //gives the pet cards all their data
    }

    public void updatePetCards()
    {

        Color myColorFaded = new Color();
        ColorUtility.TryParseHtmlString("#00000096", out myColorFaded);
        Color myColorNorm = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out myColorNorm);

        for (int i = 0; i < 32; i++)//10 amount of pets max at the moment
        {
            ListOfPets[i].petMasterScript = this.gameObject.GetComponent<PetsMaster>();

            ListOfPets[i].petSpriteList = petGenScript.getCardSprite(i); //gets the pet its spriteS

            ListOfPets[i].assignPetSprite(); //gives the pet its sprite

            if(ListOfPets[i].petSprite.sprite == blankTransparentSprite) //if the list has returned a blank sprite, dont show the card, because its empty anyways
            {
                ListOfPets[i].gameObject.SetActive(false);

            }
            



            //--Start--NONE OF THIS IS NEEDED, ITS JUST FOR REFERENCE AND IS NOT USED
            ListOfPets[i].petLvInt = petGenScript.UnlockedPetsLevel[i]; //gets the current level of the pet //it should be able to get this info based on the index of the card

            if (ListOfPets[i].petLvInt >= MainMenu.LVCAP)
            {
                ListOfPets[i].petLvInt = MainMenu.LVCAP;
            }

            ListOfPets[i].petName = petGenScript.PetName[i]; //gives the card the pet name

            ListOfPets[i].petNameTmpro.text = petGenScript.PetName[i]; //gives the card the pet name

            ListOfPets[i].ability = petGenScript.PetAbility[i]; //gives the card the pet ability

            ListOfPets[i].description = petGenScript.PetDesc[i]; //gives the pets attack desc.

            ListOfPets[i].petType = petGenScript.PetType[i];

            //--END--

            ListOfPets[i].rarity = petGenScript.PetRarity[i];

            ListOfPets[i].loadRarityCard(); //this loads the cards rarity bkg

            ListOfPets[i].index = i; //so the pet card can use this index to reference all the lists

            if (petGenScript.UnlockedPets[i] == 1)
            {
                ListOfPets[i].isUnlocked = true;

                ListOfPets[i].petSprite.color = myColorNorm;
                
            }
            else
            {
                if (ListOfPets[i].rarity != 4) //this is so premium pets can always be seen, but will have a lock if they are not unlocked yet
                {
                    ListOfPets[i].petSprite.color = myColorFaded;
                }
            }


        }

    }

    public void upgradePetLevel()
    {
        MainMenu CameraObj = Camera.main.GetComponent<MainMenu>();


        if (CameraObj.playerObj.playerGold >= upgradeGoldCost 
            && ingredientsScript.totalBlueGem>=3 
            && ingredientsScript.totalRedGem >= 3 
            && ingredientsScript.totalYellowGem >= 3
            && currentSelectedIndex >=0)
        {
            //you upgraded  your pet!+
            CameraObj.playerObj.playerGold -= upgradeGoldCost;

            ingredientsScript.totalBlueGem -= 3;
            ingredientsScript.totalRedGem -= 3;
            ingredientsScript.totalYellowGem -= 3;

            ingredientsScript.setNewPrefs();

            petGenScript.UnlockedPetsLevel[currentSelectedIndex] = petGenScript.UnlockedPetsLevel[currentSelectedIndex] + 1; //just adds 1 to the pets current level

            petGenScript.savePetsUnlocked(); //this will save pets level in an array

            //put in new price
            //upgradeGoldCost = petGenScript.petUpgradeCost[petGenScript.UnlockedPetsLevel[currentSelectedIndex] - 1]; //this should get the next levels cost
            //petUpgradeCost.text= LargeNumConverter.setDmgShort(upgradeGoldCost);

            updatePetCards(); //to make sure it changes the sprites

            checkPetEquipped();

            ListOfPets[currentSelectedIndex].TaskOnClick(); //this will refresh everything for the pet, add new prices/level etc.

            petManager.getNewArrayValues(); //makes sure the cards evolutionary states (ability values) are updated when the cards are upgraded

            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().upgradePet();

        }
        else if(CameraObj.playerObj.playerGold < upgradeGoldCost)
        {
            CameraObj.showErrorMsg("Not enough Gold!");

        }else if (ingredientsScript.totalBlueGem < 3
            || ingredientsScript.totalRedGem < 3
            || ingredientsScript.totalYellowGem < 3)
        {
            CameraObj.showErrorMsg("Not enough Gems!");
        }

        

    }

    public void pressHeroToggle()
    {

       heroPage.SetActive(true);
       petsPage.SetActive(false);
       petsToggle.setToDefaultSprite();

       heroItemDescription.SetActive(false);//this is to fix a bug that occured in main menu when the pets page toggle was pressed and the page was closed

       Color myToggleOn = new Color();
       ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
       Color myToggleOff = new Color();
       ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        petsToggleText.color = myToggleOff; //b/c unity's toggle system blows
        heroToggleText.color = myToggleOn;

    }

    public void pressPetsToggle()
    {
    //    print("pets toggle pressed!");

       heroPage.SetActive(false);
       petsPage.SetActive(true);
       heroToggle.setToDefaultSprite();

       petsToggle.toggleSprites();

       heroItemDescription.SetActive(false);

       Color myToggleOn = new Color();
       ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
       Color myToggleOff = new Color();
       ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        petsToggleText.color = myToggleOn; //b/c unity's toggle system blows
        heroToggleText.color = myToggleOff;


    }

    public void deselectAll() //this is called from the petcard script when a pet is selected. First it deselects all pets, then it selects the pet you clicked on. This is to make sure we dont select multiple pets
    {

        for(int i=0; i <ListOfPets.Count; i++)
        {
            if (ListOfPets[i].selected.activeSelf == true)
            {
                ListOfPets[i].selected.SetActive(false);
            }
        }

    }

    public void openEquipWindow()
    {

        checkPetEquipped();

        //check int selected (aka as the slot)
        

            if (levelsCompleteScript.unlockedLevelArray[30] == 1)//this means the level has been achieved
            {
                equipSlotLock[1].SetActive(false);
            }
            
            if (levelsCompleteScript.unlockedLevelArray[60] == 1)//this means the level has been achieved
            {
                equipSlotLock[2].SetActive(false);

            }

        petsEquipPage.SetActive(true);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openBag();
        
    }

    public void closeEquipWindow()
    {
        petsEquipPage.SetActive(false);

        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();



    }
}
