﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class RockCollider : MonoBehaviour {

	public int createdByPlayer=0;
	public float timeAlive = 0;
	public bool EnemyDead = false;
	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public GameObject ScreenShaker;
	public GameObject lifeStealObj;
	public GameObject ParticleExplosion;
	public GameObject floorPlane;

	public GameObject LeafExplosion;
	public GameObject ChildRock;
	public SoundManager soundManager;
	public double spellDMG = 0;
	public bool isCrit = false;





	// Use this for initialization
	void Start () {

		soundManager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		ScreenShaker = GameObject.Find ("ScreenShakerController");

		SpellCreator = GameObject.Find ("SpellListener");

		floorPlane = GameObject.Find ("FloorSprite");

//		if (createdByPlayer == 2) {
//
//			this.gameObject.GetComponent<ConstantForce2D>().force = new Vector2(-300,0);
//		}

//		StartCoroutine ("ClickDetector");

	}
	
	// Update is called once per frame
	void Update () {

		timeAlive += Time.deltaTime;
		if(timeAlive>2f){

			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;

				ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (this.gameObject.transform.position.x, 
				                                                                 this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;
				ParticleExplosion.GetComponent<ParticleSystem> ().collision.SetPlane (0,floorPlane.transform);


				LeafExplosion = Instantiate (LeafExplosion, new Vector3 (this.gameObject.transform.position.x, 
				                                                                 this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;
				LeafExplosion.SetActive(true);

			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
				soundManager.rockExplode();
			
	//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

				Destroy(this.gameObject);
			
			}

			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){

				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);

					if(EnemyDead==true){
					ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (this.gameObject.transform.position.x, 
					                                                                 this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;

					ParticleExplosion.GetComponent<ParticleSystem> ().collision.SetPlane (0,floorPlane.transform);


					LeafExplosion = Instantiate (LeafExplosion, new Vector3 (this.gameObject.transform.position.x, 
					                                                         this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;
					LeafExplosion.SetActive(true);
					soundManager.rockExplode();
		
					}
		//		ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

				Destroy(this.gameObject);

			}

		}

	}

	void OnCollisionEnter2D (Collision2D player) {
		
		if (player.gameObject.tag == "Player1" && createdByPlayer==2) {
		//	print ("there is a collision with player2!!!");
		}

		if (player.gameObject.tag == "Player2" && createdByPlayer==1) {
		//	print ("there is a collision with player2!!!");
		}

	}

	void OnTriggerEnter2D(Collider2D player) {


		if (player.tag == "Floor") {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();
		}

		if (player.tag == "Player1" && createdByPlayer == 1) {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();
		}

		if (player.tag == "Player2" && createdByPlayer == 2) {

			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen
		//	soundManager.RockOnFloor();

		}

		
//		if (player.tag == "Player2" && createdByPlayer==1 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			print ("there is a collision with player2!!!");
//
//
//				//where Damage is calcualted
//			if(player.gameObject.GetComponent<ControlPlayer2Character>().isShielded==true){
//				this.gameObject.GetComponent<CircleCollider2D>().enabled=false;
//			}
//
//				if(player.gameObject.GetComponent<ControlPlayer2Character>().isShielded==false){
//
//
//					
//					float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "EarthRock") 
//												+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[3] //adding fire item mods
//				                            	+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player1AttributeMods[4])*10; //adding overall atk mod
//					//condition for super effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//						
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//						
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//
//					}
//					
//					//condition for Not very effective
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//						
//					calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//						
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//					   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//						
//						
//					}
//
//				//check for crit against p2
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true  
//				   ){ //did the player crit on their turn??
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//
//				player.gameObject.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(calculatedHealthLoss); //damage p1 notification
//
//		//		StartCoroutine(LifeStealRoutine(player));
//				soundManager.RockHitEnemy();
//				}
//				//End where Damage is calcualted
//			
//			player.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen
//
//		}

//		if (player.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) {
//			print ("there is a collision with player1!!!");
//
//			//where Damage is calcualted
//
//			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==true){
//				this.gameObject.GetComponent<CircleCollider2D>().enabled=false;
//			}
//
//			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){
//
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("NetDictionary_" + "EarthRock") 
//					+ Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[3] //adding fire item mods
//				                              + Camera.main.GetComponent<TurnedBasedGameOrganizer>().player2AttributeMods[4])*10; //adding overall atk mod
//				
//				//condition for super effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WindSprite){
//
//
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				//condition for Not very effective
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().EarthSprite){
//
//					calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().WaterSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().FireSprite ||
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//				   == Camera.main.GetComponent<TurnedBasedGameOrganizer>().nullsprite){
//					
//
//				}
//
//
//				//check for crit against p1
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
//				   ){ //did the player crit on their turn??
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//				}
//
//				player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;
//
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(calculatedHealthLoss); //damage p1 notification
//
//				soundManager.RockHitEnemy();
//
//		//		StartCoroutine(LifeStealRoutine(player));
//			}
//
//			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen
//
//		}

		//this is for when hitting an enemy

		if (player.gameObject.tag == "Enemy") {

			damageEnemy(player); //this is to damage a non player enemy 
			soundManager.rockHit();

		}

//		if (player.gameObject.tag == "Player1" && createdByPlayer == 2 && Camera.main.GetComponent<CampaignGameOrganizer> () != null 
//		    && player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false) { //this is for p1 hit by player based enemy
//
//			damagePlayer(player);
//			soundManager.RockHitEnemy();
//
//		}

		if (player.gameObject.tag == "Player2" && createdByPlayer == 1 && Camera.main.GetComponent<CampaignGameOrganizer> () != null) { //this is for a player based enbemy
			
			damageEnemy(player); //this is to damage a player based enemy
			soundManager.rockHit();

		}

		//need damage player enemy 

		//need damage p1

	}

	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method

//		print ("there is a collision with enemy!!!");
		
		double calculatedHealthLoss = spellDMG;

		if (isCrit == false) {
			Camera.main.GetComponent<MainMenu> ().SpellDamage ((spellDMG)); //sends the spell damage to the spell dmg text thing
		} else {

			Camera.main.GetComponent<MainMenu> ().spellCritText ((spellDMG)); //sends the spell damage to the spell dmg text thing

		}
//		//condition for super effective
//		if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//			
//			calculatedHealthLoss = 2*calculatedHealthLoss;
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

//			
//		}
//		
//		//condition for Not very effective
//		if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//
//			calculatedHealthLoss = (.5f)*calculatedHealthLoss;
//
//			Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//			
//			
//		}
//		
//		if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
//		   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite ||
//		   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//			
//			//do nothing, reg dmg
//		}
//		
////		StartCoroutine(LifeStealRoutine(player));
//
//		if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//			
//			calculatedHealthLoss = 2*calculatedHealthLoss;
//			print("CRITS!!!!!!!!!");
//			
//		}
		
		player.gameObject.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
		int randoStun = Random.Range (0, 100);


		if (Camera.main.GetComponent<MainMenu> ().SpellStunChance >= randoStun && Camera.main.GetComponent<MainMenu> ().SpellStunEnabled == true) {
			
			player.gameObject.GetComponent<ControlEnemy> ().EnemyStunned = true;
		}

//		Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);

		this.gameObject.GetComponent<CircleCollider2D>().enabled=false;


		//player.gameObject.GetComponentInChildren<BoxCollider2D> ().enabled = false;
		if (calculatedHealthLoss >= player.GetComponent<ControlEnemy> ().playerHealthCurrent) {
			
			//player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
			//player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
			//player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
			
			ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.4f,0.1f); //shakes the screen

			EnemyDead=true; //activates the timer to destroy the rock
			
		}else{
			//	this.gameObject.GetComponentInChildren<CircleCollider2D>().enabled = false;
			ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (this.gameObject.transform.position.x, 
			                                                                 this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;

			ParticleExplosion.GetComponent<ParticleSystem> ().collision.SetPlane (0,floorPlane.transform);

			LeafExplosion = Instantiate (LeafExplosion, new Vector3 (this.gameObject.transform.position.x, 
			                                                         this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;
			LeafExplosion.SetActive(true);

			ChildRock.GetComponent<CircleCollider2D>().enabled = false;
			this.gameObject.GetComponent<SpriteRenderer>().enabled=false;
	
		}
	}

	public void damagePlayer(Collider2D player){
		

			double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
			
			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite){
//				
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				
//			}
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite){
//				
//				DamageToPlayer = 2* DamageToPlayer;
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
//			}
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
//				
//
//				DamageToPlayer = Mathf.FloorToInt(0.5f* DamageToPlayer);
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				
//			}
//			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite || 
//		   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer1.GetComponent<Image>().sprite 
//		   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
//				
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//			//	Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
//				
//			}
			

		if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
					
			//player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
			//player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
			//player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();

			EnemyDead=true; //activates the timer to destroy the rock

					
					
		} else {
			this.gameObject.GetComponentInChildren<CircleCollider2D>().enabled=false;
			this.gameObject.GetComponent<SpriteRenderer>().enabled=false;

			ParticleExplosion = Instantiate (ParticleExplosion, new Vector3 (this.gameObject.transform.position.x, 
			                                                                 this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;

			ParticleExplosion.GetComponent<ParticleSystem> ().collision.SetPlane (0,floorPlane.transform);


			LeafExplosion = Instantiate (LeafExplosion, new Vector3 (this.gameObject.transform.position.x, 
			                                                         this.gameObject.transform.position.y, 88), Quaternion.identity) as GameObject;
			LeafExplosion.SetActive(true);
		}

//		Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player


	}



//	IEnumerator ClickDetector(){
//		
//		while (true) {
//
//			yield return new WaitForSeconds(0.75f);
//
//			if(this.gameObject.GetComponent<ConstantForce2D>()!=null){
//				this.gameObject.GetComponent<ConstantForce2D>().enabled=true;
//
//			}
//
//			yield break;
//
//		}
//
//	}

//	IEnumerator LifeStealRoutine(Collider2D playerObject){
//
//		while (true) {
//			
//			yield return new WaitForSeconds(0.5f);
//			
//			GameObject LifeStealObjClone = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//
//			yield return new WaitForSeconds(0.5f);
//
//			GameObject LifeStealObjClone2 = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone2.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone2.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone2.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//			yield return new WaitForSeconds(0.5f);
//
//			GameObject LifeStealObjClone3 = (GameObject)Instantiate (lifeStealObj, new Vector3 (playerObject.gameObject.transform.position.x, playerObject.gameObject.transform.position.y, 0), Quaternion.identity);
//			LifeStealObjClone3.GetComponent<lifeStealScript> ().createdByPlayer = createdByPlayer;
//			LifeStealObjClone3.GetComponent<lifeStealScript>().amountOfStealFX=3;
//			LifeStealObjClone3.GetComponent<lifeStealScript>().healthStolenValue=5;
//
//
//			
//			yield break;
//
//			
//		}
//		
//	}

}
