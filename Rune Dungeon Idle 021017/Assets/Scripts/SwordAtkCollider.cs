﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class SwordAtkCollider : MonoBehaviour {

	public float timeAlive=0;
	public GameObject ScreenShaker;
	public int createdByPlayer=1;
	public GameObject SpellCreator;

	// Use this for initialization

	void Start () {
		SpellCreator = GameObject.Find ("SpellListener");

		ScreenShaker = GameObject.Find ("ScreenShakerController");
	}

	void Update(){
		
		timeAlive += Time.deltaTime;
		
		if(timeAlive>2){
			
			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
				
				Destroy(this.gameObject);

			}

			if(SpellCreator.GetComponent<SpellCreator>()!=null){
				
				SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
				
				Destroy(this.gameObject);
				
			}
			
		}
		
		
	}


	void OnTriggerEnter2D(Collider2D player) {
		
		
//		if (player.gameObject.tag == "Enemy" || player.gameObject.tag == "Player2" && createdByPlayer==1) {
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null && player.gameObject.GetComponent<ControlEnemy>().isShielded==false){ //when p1 is attackin enemy or enemyplayer in campaign
//
//				print ("there is a collision with enemy!!!");
//
//				
//				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("AutoAtkItemMod") + 
//				                              + PlayerPrefs.GetFloat ("atkMod") - 5)*10 + Random.Range(1,11);
//
//				
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//					
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					print("CRITS!!!!!!!!!");
//					
//				}
//
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().swordEnch[0]>=0){
//					calculatedHealthLoss = 3*calculatedHealthLoss;
//					Camera.main.GetComponent<CampaignGameOrganizer>().enemyDamageAmount.GetComponent<TextMeshProUGUI>().color = Color.magenta;
//					Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
//				}
//
//
//
//				player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
//
//				Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
//
//
//				if (calculatedHealthLoss >= player.GetComponent<ControlEnemy> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
//					player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
//					
//					ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f, 0.1f); //shakes the screen
//					
//				}			
//			}
//
//			if (player.gameObject.tag == "Player2" && createdByPlayer==1 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) { //this is for multiplayer
//				
//				if(player.gameObject.GetComponentInChildren<ControlPlayer2Character>().isShielded==false){
//					
//					print ("there is a collision with PLAYER!!!");
//					
//					ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//					
//					float DamageToPlayer = (PlayerPrefs.GetFloat ("AutoAtkItemMod") + 
//					                        + PlayerPrefs.GetFloat ("atkMod") - 5)*10 + Random.Range(1,11);
//
//
//		
//					//hit p2
//
//					//check for crit against p2
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
//					   ){ //did the player crit on their turn??
//						DamageToPlayer = 2*DamageToPlayer;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//						DamageToPlayer = 2*DamageToPlayer;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					}
//
//						if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer2Character> ().playerHealthCurrent) {
//							
//							player.gameObject.GetComponent<ControlPlayer2Character> ().playerDead = true;
//						//	player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//						//	player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//							
//						}
//						
//						player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//						player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//					player.GetComponent<ControlPlayer2Character>().playerHealthNew -= DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(DamageToPlayer); //damage p2 notification
//					
//
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmountText.GetComponent<Text> ().text = "-" + DamageToPlayer.ToString () + " hp";
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmount.SetActive (true);
//				}
//			}
//
//			this.gameObject.GetComponent<BoxCollider2D>().isTrigger=false;
//		}
//
//		if (player.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<CampaignGameOrganizer>()!=null) { //this is for campaign
//			
//			if(player.gameObject.GetComponentInChildren<ControlPlayer1Character>().isShielded==false){
//				
//				print ("there is a collision with PLAYER!!!");
//
//				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				float DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//
//				if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
//					player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//					
//					
//				}
//
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;		
//				Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//
//			}
//		}
//
//
//
//		if (player.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) { //this is for multiplayer
//			
//			if(player.gameObject.GetComponentInChildren<ControlPlayer1Character>().isShielded==false){
//				
//				print ("there is a collision with PLAYER!!!");
//				
//				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				float DamageToPlayer = (PlayerPrefs.GetFloat ("AutoAtkItemMod") + 
//				                        + PlayerPrefs.GetFloat ("atkMod") - 5)*10 + Random.Range(1,11);
//				
//
//
//				//hit p1
//				//check for crit against p1
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//				   ){ //did the player crit on their turn??
//					DamageToPlayer = 2*DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//					DamageToPlayer = 2*DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//
//				if (DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
//					//player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//				//	player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//					
//					
//				}
//				
//				
//				
//				player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//				player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(DamageToPlayer); //damage p2 notification
//
//
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmountText.GetComponent<Text> ().text = "-" + DamageToPlayer.ToString () + " hp";
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmount.SetActive (true);
//			}
//
//			this.gameObject.GetComponent<BoxCollider2D>().isTrigger=false;
//
//		}
//
//
//
//

	}
}
