﻿using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.Analytics.Experimental;

//using Facebook.Unity;

public class MainMenu : MonoBehaviour {

	public static int LVCAP = 145; //player can only play through 135

	public GameObject Canvas2; //this is the canvas that is used for particle effects and the background

	public List<ShopCard> buyPackageList = new List<ShopCard>();
	public List<ShopCard> buyGemsList = new List<ShopCard>();

	public List<GameObject> talentAnimations = new List<GameObject> ();

	public List<GameObject> itemList = new List<GameObject> ();
	public List<GameObject> NewSpellsList = new List<GameObject> ();
	public List<ResearchUpgrade> ResearchList = new List<ResearchUpgrade> ();
	public List<GameObject> SlashPuffList = new List<GameObject> ();
	public List<GameObject> SlashList = new List<GameObject> ();
	public List<GameObject> skeleParticlesList = new List<GameObject> ();
	public List<GameObject> chestParticlesList = new List<GameObject> ();

	public List<float> potionModList = new List<float> ();
	public List<SpriteToggle> MenuButtons = new List<SpriteToggle> ();

	public TutorialController TutorialObject;

	public TutorialCampaign TutorialCampaignObj;
	public NotificationTest notificationScript;

	public GameObject TutorialGameObject;
	public CampaignLevelsEnemySpawner campaignLevelEnemySpawner;

	public GameObject LoadingPane;
	public GameObject inventoryWindow;
	public Inventory InventoryScript;

	public GameObject itemDescriptionWindow;
	public GameObject itemDescriptionEquip;
	public GameObject spellWindow;
	public GameObject spellDescriptionWindow;
	public GameObject spellDescriptionEquip;
	public GameObject equipmentBackButton;
	public GameObject newItemWindow;
	public GameObject talentWindow;
	public GameObject potionsPage;
	public GameObject settingsPage;
	public GameObject prestigeWindow;
	public GameObject areYouSurePrestige;
	public GameObject shopWindow;
	//public GameObject petsWindow;



	public GameObject shadowObj;
	public GameObject chestHolder;

	//
	public GameObject MainCharacterPage;
	public Slider AtkSlider; //these are in the main char page
	public TextMeshProUGUI AtkSliderTxt;
	public TextMeshProUGUI AtkSliderTxtLeft;

	public Slider HPSlider;//these are in the main char page
	public TextMeshProUGUI HpSliderTxt;
	public TextMeshProUGUI HpSliderTxtLeft;


	public Slider LuckSlider;//these are in the main char page
	public TextMeshProUGUI LuckSliderTxt;
	public TextMeshProUGUI LuckSliderTxtLeft;


	public GameObject UpgradeWeaponPage;
	public MasterWeaponList masterWeaponListScript;
	public MasterSpellList masterSpellListScript;


	public GameObject ExplosionForce;

	public GameObject statsWindow;
    public StatsPoints statsPointsScript;

	public GameObject LevelWindow;
	public GameObject unlockLevelPopUp;
	//public GameObject activateSpellsButton;

	public GameObject Player1ExpBar;
	public GameObject Player1HPBar;

	private float playerExpCurrent;
	private float playerExpMax;
	private int playerGold;

	public TextMeshProUGUI playerRankText;
	public TextMeshProUGUI playerGoldText;
	public Transform gemGoldImgPos;

	public GameObject itemPurchaseButton;
	public GameObject ItemBuyParticles;
	public GameObject SpellBuyParticles;


	public GameObject FacebookItemSparkleInventory;

	public Image spellDescriptionImage;

	public TextMeshProUGUI spellDescritpionText;
	public TextMeshProUGUI spellTitleText;
	public TextMeshProUGUI spellPointsToSpend;

	public GameObject isEquipped;

	public bool inventoryIsOpen = false;
	public ItemInfo buttonHolder;
	public SpellInfo spellButtonHolder;

	public int totalFireMod; //may be able to use these for the total item mods later
	public int totalWaterMod;
	public int totalWindMod;
	public int totalEarthMod;
	public int totalHpMod;
	public int totalLuckMod;

	// Use this for initialization
	//for sfx persistent sound manager

	public CheckLevelsComplete LevelCompleteObject;
	public GameObject persistantSoundManager;

	public GameObject PlayerManequin; //this is the gameobject that is used for player clothing.
	public GameObject EquipItemParticles; //this is the gameobject that is used for player clothing.
	public GameObject EquipParticlesSpawnLocation; //this is the gameobject that is used for player clothing.

	public GameObject Act1LevelList;
	public GameObject Act2LevelList;

	public GameObject PlayerDeadScreen;
	public GameObject PlayerDeadScreenAtBoss;

    //these are for clicker game button press method
    public ControlEnemy EnemyControlObj;
    public GameObject EnemyObj;
    public GameObject enemyHeadObj;

    //for talents
    private float AutomatedAttackRate = 1;
	private bool automatedAttackBool = false;
	public bool spellsEnabled = false;
	public bool critEnabled = false;
	public int advCritEnabled = 0;
	public float damageIncreaseTalent1 = 0;
	private bool goldMiningEnabled = false;
	public bool goldCritUnlocked = false;
	public bool spellCritUnlocked = false;
	public int extraGemUnlocked = 0;
	public bool flurryUnlocked = false;
	public float flurryTimer = 0;
	//public TextMeshProUGUI flurryTimerText;
	public GameObject flurryButton;
	private bool spellflurryUnlocked = false;
	public int spellFlurryTime;
	public GameObject spellFlurryButton;
	public GameObject freezeTimeButton;
	public GameObject megaSlashButton;
    public GameObject shieldButton;
    public GameObject petFlurryButton;
    public GameObject soulChargeButton;
    public GameObject soulChargePrefab;

    public bool reduceWeaponCost = false;
	public bool reduceSpellCost = false;
	public bool reduceLevelCost = false;

	public int itemRateIncrease = 0;
	public TextMeshProUGUI researchPointsToSpend;
	public int researchPoints;
	public int flurryTimerBonus = 0;
	public int GoldMinerTimeReduction = 0;
	public int goldCritChanceIncrease = 0;
	public int extraGemChance = 0;

	public float weaponCostReduction = 0;
	public float SpellCostReduction = 0;
	public float LevelCostReduction = 0;

	private bool megaSlashEnabled = false;
	public MegaSlashCooldwn megaSlashScript;
	public GameObject megaSlashDmg;
	public bool potionMasteryEnabled = false;
	public int potionMasteryTimeBonus;
	public bool GemShardsEnabled = false;
	public float GemShardsBonus;
	public bool GemEssenceEnabled = false;
	public float GemEssenceBonus;
	public int megaSlashMultiplier;
	private bool FreezeTimeEnabled = false;
	public float FreezeTimeTime;
	public bool SpellStunEnabled = false;
	public float SpellStunChance;
	public bool WoundEnabled = false;
	public float WoundChance = 0;
	public GameObject woundBlood;
	public bool belowHPbool = false;
	public double belowHpLimit = 0;
	public bool InstaKillbool = false;
	public float InstaKillChance;
	public GameObject instaKillObj;
	public bool lifeStealTalent = false;
	public float lifeStealAmount;
	public int TwentyTapCount =0;
	public GameObject TwentyTapCountText;
	public GameObject TwentyTapHealAnim;
	public bool CurseTalent = false;
	public float CurseAmount;
	public bool ShallowGrave = false;
	public float GraveHP;
	public int RingAvailable;
    public float IncreasedAutoAtk;
    public bool shieldUnlocked = false;
    public float shieldModifier;
    public bool petFlurryUnlocked = false;
    public float petFlurrymodifier;
    public float petFlurryTime;
    public bool soulChargeUnlocked = false;
    public float soulChargeMod;
    public bool bowUnlocked = false;
    public bool petCritUnlocked = false;
    public float petCritMod = 0;
    public float bowMasteryMod = 0;
    //for talents

    //for prestige

    public float AtkPrestCritMult;
	public int PrestFlurryTimerBonus = 0;
	public float damageIncreaseTalentPrestige;
	public int megaSlashMultiplierPrestige;
	public double returnAtkPrestige;

	public float MagPrestCritMult;
	public float spellFlurryTimePrestige;
	//IncreasedSpellDmgPrestige is in playerobj
	public int FreezeTimePrestige;
	public double returnMagPrestige;

	public int potionMasteryTimePrestige;
	public float sellPricePrestige;
	public float goldCritChanceIncreasePrestige;
	public float BarginBinReductionPrestige;
	public float AutoGemCollector;

	//facebook buttons
	public GameObject facebookLoginButton; //for the item 
	public GameObject facebookShareButton;

	public ControlPlayer1Character playerObj;
	public ControlPlayer1Character animatedPlayerObj;


	public int playerAtkDamage;
	public GameObject ExplosiveForce;

	public bool researchCoroutine = false;
	public bool lifeRegenCoroutine = false;
	public bool GoldMinerCoroutine = false;

	public GameObject researchCompleteButton;
	public GameObject IdleTimeAway;

	public GameObject dmgAmt;
	public TextMeshProUGUI pDmgAmt;
	public GameObject expAmt;

	public List<GameObject> magicDmgAmtList = new List<GameObject> ();
	public List<GameObject> dmgAmtList = new List<GameObject> ();
	public List<GameObject> bloodAmtList = new List<GameObject> ();
	public List<GameObject> hitFXList = new List<GameObject> ();
	public List<GameObject> CritList = new List<GameObject> ();
	public List<GameObject> AutoAtkList = new List<GameObject> ();
	public List<GameObject> magicCritDmgList = new List<GameObject> ();
    public List<GameObject> petDmgAmtList = new List<GameObject>();
    public List<GameObject> bowArrowFx = new List<GameObject>();



    public ScreenShakerController ScreenShaker;
	public EnemyStatsInheriter enemyStatsObj;

	public GameObject newLevelNotification;
	public GameObject newWeaponNotification;
	public GameObject newSpellNotification;
	public GameObject newTalentNotification;
	public GameObject newPrestigeNotification;
	public GameObject shopGoldNotification;


	public GameObject statsAvailableNotification;

	public TextMeshProUGUI currentLevelText;

	public bool refreshAtkTimer = false;
	public float autoAtkTimer;

	private float initialFuryDekayTimer = 0.3f; //this is just to  detect its being held long enough
	private float FuryPurchaseTimer = 0.16f; //this is just to  detect its being held long enough

	public string atkTimeString;
	public TextMeshProUGUI atkTimeTxt;

	public bool windowOpen = false;

	public PrestigePoints prestPointsAmt; //this script is so enemy ragdoll can add to the prestige points counter etc.

	public PotionMasterScript _potionMasterScript;

	public GameObject errorMsgGo;
	public TextMeshProUGUI errorMsgTxt;
	public GameObject prestigeRdyParticleFx;

	public bool mouseDown = false;

	//for gold miner
	private float goldCastTimer = -1f;
	public TextMeshProUGUI goldTimeText;
	public string goldTimeString;

	public TwoHourShopRotation twoHourShopRotation;

	public GameObject menuPausedScreen;
	public GameObject GDPRwindow;
    public LimitedTimeOffers LimitedTimeOffers;

    //public GameObject dailyDungeonWindow;
    public GameObject PetGenerator;
    public DungeonCelebration dungeonCelebration;
    public PetManager petManager;

    //for events
    public GameObject eventWindow;

    public GameObject DailyDungeonNotification;
    public GameObject DailyDungeonTabNotification;
    public GameObject ReviewGameWindow;

    public AbilityMaster abilityMaster;

    public int BowTapCount = 0;
    public bool usingBow = false;

    public GameObject resetEventCard;
    public GameObject quitGamePrompt;

    public GameObject gpEarnObj;


    void OnEnable(){

		enemyStatsObj = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter>();

		enemyStatsObj.loadLevel (PlayerPrefs.GetInt("CurrentLevel")); //with the new level playerpreff this increases the level count. Need to leave this in since this script doesnt get removed. These values are constant
	
	}

	void Start () {
        PlayerPrefs.SetFloat("MemsGpBonus", 0);//so they dont get the bonus unless they login to their guild

        //print("start code running");
        //for skip[ping tutorials
        //PlayerPrefs.SetInt ("ClickToKillTutorial",1);
        //PlayerPrefs.SetInt ("LevelUpTutorial",1);
        //PlayerPrefs.SetInt ("FirstItemFound",1); //this is located in autosell script :0
        // ////
        // //////		//end skip  tutorials
        //PlayerPrefs.SetInt ("FirstGem", 1);
        //PlayerPrefs.SetInt("FoughtFirstBoss", 1);
        //PlayerPrefs.SetInt("DungeonComp", 0); //this unlocks the daily dungeon to be played
        //PlayerPrefs.SetInt("PrestigePts", 50); //These are now Prestige Points you can spend
        //PlayerPrefs.SetInt("ResearchPoints", 85); //play pref for talents
        //PlayerPrefs.SetInt("EventLevel", 10);
        //PlayerPrefs.SetInt("EventPoints", 10);

        //for unlocking all upgrades



        for (int i=0; i<ResearchList.Count; i++)
        {
            ResearchList[i].pointsInvested = ResearchList[i].pointCap;


        }
        //so the items dont default at zero for sell time.

        startAppsFlyer();

        Vector3 charPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 5f,0, 0));

        animatedPlayerObj.gameObject.transform.parent.position = new Vector3(charPos.x,0.97f,90);


        if (PlayerPrefs.GetInt ("AutoSellTime") == 0) {

			PlayerPrefs.SetInt ("AutoSellTime", 60);
		}

		Screen.sleepTimeout = SleepTimeout.NeverSleep; //makes it so the phone wont go to sleep

		Input.multiTouchEnabled = false; //makes it so only one mouse click at a time on the scren
			
		currentLevelText.text = "World " + PlayerPrefs.GetInt("CurrentLevel").ToString();

		//
		AnalyticsEvent.LevelStart (PlayerPrefs.GetInt ("CurrentLevel").ToString ()); //tracks which levels the players have completed
		//
	//	AnalyticsEvent.Custom("TestStart");

	//	print ("the current level of the player is: " + PlayerPrefs.GetInt ("CurrentLevel").ToString ());

		persistantSoundManager = GameObject.Find("PersistentSoundManager");

		animatedPlayerObj.soundManager = persistantSoundManager.GetComponent<SoundManager> ();

	//	checkMods ();


		//checkForNotification (); //may be used later, but for now commented out

		updatePlayerInfo ();

        //	checkAct2Stuff ();
        //setNotifications();

        checkActivePassiveAbilities();

        //
       


	//	print ("start of mainmenu is running");
	//	PlayerPrefs.SetInt ("Gold",5000); //for testing only

		StartCoroutine ("AutoAtkRoutine"); //detects if a block is being selected

		if (researchCoroutine == false) {
			StartCoroutine ("ResearchChecker");
		}

		if (lifeRegenCoroutine == false) {
			StartCoroutine ("HealthRegen");
		}


		if (PlayerPrefs.GetInt ("GDPR") == 0) { //for crummy GDPR rules/..
			GDPRwindow.SetActive (true);
		}

        setNotifications();

        ResearchCheckAtStart();

        StartCoroutine("checkStuffAffordable"); //checks for notifications on affordable stuff like levels weapons spells talents etc

        statsPointsScript.updatePlayerInfo(); //this is mainly so the Owl pets can use this data (specifically pet 31)

        //for testing bow
        if (PlayerPrefs.GetInt("BowEnabled") == 1)
        {
            animatedPlayerObj.animator.SetBool("usingBow", true);
            usingBow = true;
        }
        else
        {
            animatedPlayerObj.animator.SetBool("usingBow", false);
            usingBow = false;
        }

        if (ObscuredPrefs.GetInt("UtilityPack") == 1)
        {
            _potionMasterScript.thirdPotionLock.SetActive(false); //removes the lock image on the 3rd potion 
        }
    }

    public void startAppsFlyer()
    {
       // print("appsflyuer being called first before purchases restore");
        /* Mandatory - set your AppsFlyer’s Developer key. */
        AppsFlyer.setAppsFlyerKey("dZufFwepkBw7q9Kg6YxDb9");
        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */
        #if UNITY_IOS
           /* Mandatory - set your apple app ID
              NOTE: You should enter the number only and not the "ID" prefix */
           AppsFlyer.setAppID ("YOUR_APP_ID_HERE");
           AppsFlyer.trackAppLaunch ();
        #elif UNITY_ANDROID
        /* Mandatory - set your Android package name */
        AppsFlyer.setAppID("com.Company.ClickerKnightIncrementalIdleRPG");
        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
        AppsFlyer.init("dZufFwepkBw7q9Kg6YxDb9", "AppsFlyerTrackerCallbacks");
        //print("starting appflyer on android");
#endif

    }

    public void checkActivePassiveAbilities(){

		AtkPrestCritMult=0;
		PrestFlurryTimerBonus = 0;
		damageIncreaseTalentPrestige=0;
		megaSlashMultiplierPrestige=0;
		returnAtkPrestige=0; 

		MagPrestCritMult = 0;
		spellFlurryTimePrestige=0;
		FreezeTimePrestige=0;
		returnMagPrestige=0;

		potionMasteryTimePrestige=0;
		sellPricePrestige=0;
		goldCritChanceIncreasePrestige=0;
		BarginBinReductionPrestige=0;
		AutoGemCollector=0;

        IncreasedAutoAtk = 0;

		if (megaSlashEnabled == false) { //if megaslash not unlocked disable button
			megaSlashButton.SetActive (false);
            abilityMaster.unlockedAbilities[1] = 0;

        }

        if (flurryUnlocked == false) {
			flurryButton.SetActive (false);
            abilityMaster.unlockedAbilities[0] = 0;

        }

		if (spellflurryUnlocked == false) {
			spellFlurryButton.SetActive (false);
            abilityMaster.unlockedAbilities[2] = 0;

        }

        if (FreezeTimeEnabled == false) {
			freezeTimeButton.SetActive (false);
            abilityMaster.unlockedAbilities[3] = 0;

        }

        if (shieldUnlocked == false)
        {
            shieldButton.SetActive(false);
            abilityMaster.unlockedAbilities[4] = 0;
        }

        if(petFlurryUnlocked == false)
        {
            petFlurryButton.SetActive(false);
            abilityMaster.unlockedAbilities[5] = 0;

        }

        if(soulChargeUnlocked == false)
        {
            soulChargeButton.SetActive(false);
            abilityMaster.unlockedAbilities[6] = 0;

        }


        //  print("check passive /. active abilities being called");

    }


	IEnumerator ResearchChecker(){ //***make sure this is only called once per game and doesnt repeatedly get called //this also checks for potions as well

		while (true) {

			researchCoroutine = true;

			//ResearchCheckAtStart ();
		
			PlayerPrefs.SetString ("SavedTime", System.DateTime.Now.ToBinary ().ToString ()); //this is so we know how much time has passed since the player last played

			if (PlayerPrefs.GetInt ("PrestigePtsEarned") >= 2) {
				prestigeRdyParticleFx.SetActive (true);
			} else {
				prestigeRdyParticleFx.SetActive (false);

			}

			yield return new WaitForSeconds(2f);

		}
	}


	public void ResearchCheckAtStart(){

        //   print("research check at start is being called at the start");

        //for (int i = 0; i < ResearchList.Count; i++)
        //{
        //   // ResearchList[i].pointsInvested = ResearchList[i].pointCap;
        //    PlayerPrefs.SetInt(ResearchList[i].gameObject.name + "isResearched", 2);
        //    if (PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested") < ResearchList[i].pointCap)
        //    {
        //        PlayerPrefs.SetInt(ResearchList[i].gameObject.name + "pointsInvested", ResearchList[i].pointCap);
        //    }
        //}

        int talentAvailable = 0;
        int prestigeTalentAvailable = 0;


        for (int i = 0; i < ResearchList.Count; i++)
        {
            if(ResearchList[i].pointsInvested < ResearchList[i].pointCap && ResearchList[i].PrestigeTalent==false && ResearchList[i].eventTalent == false)
            {
                talentAvailable += 1;
            }

            if (ResearchList[i].pointsInvested < ResearchList[i].pointCap && ResearchList[i].PrestigeTalent == true)
            {
                prestigeTalentAvailable += 1;
            }
        }

        if (talentAvailable > 0)
        {
            //   print("there are ta;ents to be filled!");
            ResearchList[0].researchStartScript.talentsMaxed = false;

        }
        else
        {
            ResearchList[0].researchStartScript.talentsMaxed = true;

          //  print("Talents are maxxed!");
        }
        ///
        if (prestigeTalentAvailable > 0)
        {
            //  print("there are Prestige ta;ents to be filled!");
            ResearchList[0].researchStartScript.prestigeTalentsMaxed = false;

        }
        else
        {
            ResearchList[0].researchStartScript.prestigeTalentsMaxed = true;

          //  print("Prestige Talents are maxxed!");
        }

        for (int i = 0; i < ResearchList.Count; i++) {

			//if (PlayerPrefs.GetInt (ResearchList [i].gameObject.name + "isResearched") == 1) {

			//	DateTime startingTime = DateTime.FromBinary (Convert.ToInt64 (PlayerPrefs.GetString (ResearchList [i].gameObject.name + "StartTime")));

			//	DateTime currentTime = System.DateTime.Now;

			//	TimeSpan differenceTime = currentTime.Subtract (startingTime);

			//	if (differenceTime.TotalSeconds > ResearchList [i]._requiredTime) {

			//	//	print (ResearchList [i].gameObject.name + "***Researh Finished!!");

			//		PlayerPrefs.SetInt (ResearchList [i].gameObject.name + "isResearched", 2); //this will show the research complete notification

			//		ResearchList [i].pointsInvested = PlayerPrefs.GetInt (ResearchList [i].gameObject.name + "pointsInvested"); //this will update the points invested in the talent

			//	}

			//}

			if (PlayerPrefs.GetInt (ResearchList[i].gameObject.name + "isResearched")==2) {

				if (ResearchList [i].UpgradeLevel == Upgrade.Spells) {

					if(spellsEnabled==false){ //i hink this was here so that when a spell is unlocked the spell list is immediately updated
					//	masterSpellListScript.loadSpellUpgrades (); //gets the data from the excel sheet for the weapons
					//	masterSpellListScript.updateSpellAvailability ();

					spellsEnabled = true; //this means auto attack has been researcheds
				//	activateSpellsButton.SetActive(true);
					}
				}

				if (ResearchList [i].UpgradeLevel == Upgrade.IncreasedCastSpeed) {

					playerObj.increasedSpellRateTalent = 0.2f*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");	//0.3 is currently 10% of the inital 3sec cast rate, could be changed if cast rate is decreased etc
					//this should be 0.3 but this is just to test the talent
				}

				if (ResearchList [i].UpgradeLevel == Upgrade.IncreasedSpellDmg) {

					playerObj.IncreasedSpellDmgTalent = PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv")) / 100f; //in creases spell dmg by 10% each time u upgrade
                }

						if (ResearchList [i].UpgradeLevel == Upgrade.MAGUpgradePrest) {

							playerObj.IncreasedSpellDmgPrestige = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/3f;	//in creases spell dmg by 10% each time u upgrade
						}

				if (ResearchList [i].UpgradeLevel == Upgrade.SpellCrit) {

					spellCritUnlocked = true;	//0.3 is currently 10% of the inital 3sec cast rate, could be changed if cast rate is decreased etc
					playerObj.spellCritChance = 13 + 2 *PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //
				}

						if (ResearchList [i].UpgradeLevel == Upgrade.MAGImprovedCrit) {

							MagPrestCritMult = 0.15f*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //initiall u get 3% + 2% for each time u upgrade it

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.SpellFlurry) {

					spellflurryUnlocked = true;	//0.3 is currently 10% of the inital 3sec cast rate, could be changed if cast rate is decreased etc
					spellFlurryTime = 5 + PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");
					spellFlurryButton.SetActive(true);

                    abilityMaster.unlockedAbilities[2] = 1;

                }

                if (ResearchList [i].UpgradeLevel == Upgrade.MAGImprovedFlurry) {

							spellFlurryTimePrestige = 0.5f*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");
						}

				if (ResearchList [i].UpgradeLevel == Upgrade.CriticalHit && critEnabled==false) {

					critEnabled = true; //this means auto attack has been researched

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.IncreasedCritHit) { //this needs research levels

					advCritEnabled = 1 + 2*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //initiall u get 3% + 2% for each time u upgrade it

				}

						if (ResearchList [i].UpgradeLevel == Upgrade.ATKImprovedCrit) { //this needs research levels

							AtkPrestCritMult = 0.15f*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //initiall u get 3% + 2% for each time u upgrade it

						}	
					
				if (ResearchList [i].UpgradeLevel == Upgrade.GoldMiner) {
					if(goldMiningEnabled==false){
					goldMiningEnabled = true; //this means auto attack has been researched
					}
					GoldMinerTimeReduction = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

					if (GoldMinerCoroutine == false) {
						StartCoroutine ("GoldMiner"); //gives gold/sec
					}
				}

				if (ResearchList [i].UpgradeLevel == Upgrade.GoldCrit) {

					goldCritUnlocked = true;
					goldCritChanceIncrease = PlayerPrefs.GetInt (ResearchList [i].gameObject.name + "pointsInvested"); //this is multiplied by 5 in the enemy ragdoll script

				}

						if (ResearchList [i].UpgradeLevel == Upgrade.GLDCritUpgradePrest) {

							goldCritChanceIncreasePrestige = 0.1f*PlayerPrefs.GetInt (ResearchList [i].gameObject.name + "pointsInvested"); //this is going to be added to the 1.5 in the en. ragdoll script

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.ExtraGemDrop) {

					extraGemUnlocked = 1; //0 if not unlocked
					extraGemChance = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.ReduceUpgradeCostWeapon) {

					reduceWeaponCost = true; //0 if not unlocked
					weaponCostReduction = 1 - PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/50f;

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.ReduceUpgradeCostSpell) {

					reduceSpellCost = true; //0 if not unlocked
					SpellCostReduction = 1 - PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/50f;

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.ReduceUpgradeCostLevel) {

					reduceLevelCost = true; //0 if not unlocked
					LevelCostReduction = 1 - PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/50f;

				}

						if (ResearchList [i].UpgradeLevel == Upgrade.GLDBargainBin) {

							BarginBinReductionPrestige = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/100f; //will be .01-.02-.03
							

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.IncreasedItemRate) {

					itemRateIncrease = 17 * PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //0 if not unlocked

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.AutoAtk) {

                    if (automatedAttackBool == false)
                    {
                        automatedAttackBool = true; //this means auto attack has been researched
                    }

                    IncreasedAutoAtk = (PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested") - 1) / 20f; //at 1 this is zero, but at 2 this becomes 0.05 and 3... 0.1.

				}


				//dmg return prestige from auto atk
						if (ResearchList [i].UpgradeLevel == Upgrade.ATKReturnDmg) {

							returnAtkPrestige = 3*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //0 if not unlocked

						}

						if (ResearchList [i].UpgradeLevel == Upgrade.MAGReturnDmg) {

							returnMagPrestige = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //already multiplies by 0.2 in controlplayer1 script

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.IncreaseAtkDmg) {

					damageIncreaseTalent1 = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")*(10+5*PlayerPrefs.GetInt ("PrestigeLv"))/100f; //need to add upgraded stats to this too

                    //a 0.x increase
				}

						if (ResearchList [i].UpgradeLevel == Upgrade.ATKUpgradePrest) {

							damageIncreaseTalentPrestige = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/3f; //need to add upgraded stats to this too

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.AtkFlurry) {

					flurryUnlocked = true; //need to add upgraded stats to this too

					flurryTimerBonus = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //this is multiplied by 2 on the button

					flurryButton.SetActive(true);

                    abilityMaster.unlockedAbilities[0] = 1;


                }

						if (ResearchList [i].UpgradeLevel == Upgrade.ATKImprovedFlurry) {

									PrestFlurryTimerBonus = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"); //each point increases the time by 1 second

								}

				if (ResearchList [i].UpgradeLevel == Upgrade.MegaSlash) {

					megaSlashEnabled = true; //this means auto attack has been researcheds
					megaSlashMultiplier = 5*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")+10;
					megaSlashButton.SetActive(true);

                    abilityMaster.unlockedAbilities[1] = 1;


                }

                if (ResearchList [i].UpgradeLevel == Upgrade.ATKImprovedUltraSlash) {

							megaSlashMultiplierPrestige = 5*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");
                    //print("mega slash mult is " + megaSlashMultiplierPrestige.ToString());
						}

				if (ResearchList [i].UpgradeLevel == Upgrade.FreezeTime) {

					FreezeTimeEnabled = true; //this means auto attack has been researcheds
					FreezeTimeTime = 4 + PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

					freezeTimeButton.SetActive(true);

                    abilityMaster.unlockedAbilities[3] = 1;

                }

                if (ResearchList [i].UpgradeLevel == Upgrade.MAGImprovedTimeFreeze) {

							FreezeTimePrestige = 2*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");
		
						}

				if (ResearchList [i].UpgradeLevel == Upgrade.PotionMastery) {

					potionMasteryEnabled = true; //this means auto attack has been researcheds
					potionMasteryTimeBonus = 4*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

				}

						if (ResearchList [i].UpgradeLevel == Upgrade.GLDImprovedChem) {

							potionMasteryTimePrestige = 3*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

						}

						if (ResearchList [i].UpgradeLevel == Upgrade.GLDImprovedSellPrice) {

							sellPricePrestige = PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")/25f;

						}

				if (ResearchList [i].UpgradeLevel == Upgrade.GemShards) {

					GemShardsEnabled = true; //this means auto attack has been researcheds
					GemShardsBonus = 2+(PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested"));

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.GemEssence) {

					GemEssenceEnabled = true; //this means auto attack has been researcheds
					GemEssenceBonus = 3*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

				}

					if (ResearchList [i].UpgradeLevel == Upgrade.GLDGemCollector) {

						AutoGemCollector = 4*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

					}

				if (ResearchList [i].UpgradeLevel == Upgrade.SpellStun) {

					SpellStunEnabled = true; //this means auto attack has been researcheds
					SpellStunChance = 5+5*PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.RupturedWound) {

					WoundEnabled = true; //this means auto attack has been researcheds
					WoundChance = 1+PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested");

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.DmgBelowHP) {

					belowHPbool = true; //this means auto attack has been researcheds
					belowHpLimit = 1+ PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested") * (10 + 5 * PlayerPrefs.GetInt("PrestigeLv")) / 100f; //this is now the damage gained 

                }

				if (ResearchList [i].UpgradeLevel == Upgrade.InstaKill) {

					InstaKillbool = true; //this means auto attack has been researcheds
					InstaKillChance = 2*(PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")); //goes 1-->6-->11
				//	print("the gameobj mname for instakill is " +  ResearchList[i].gameObject.name);
				}

				if (ResearchList [i].UpgradeLevel == Upgrade.LifestealHit) {

					lifeStealTalent = true; //this means auto attack has been researcheds
					lifeStealAmount = 2+(PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")); //goes 3-4-5

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.Curse) {

					CurseTalent = true; //this means auto attack has been researcheds
					CurseAmount = 2+(PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")-1)/2f; //goes 1-->6-->11

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.ShallowGrave) {

					ShallowGrave = true; //this means auto attack has been researcheds
					GraveHP = 5+5*(PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")); //goes 1-->6-->11

				}

				if (ResearchList [i].UpgradeLevel == Upgrade.FindRing) {

					RingAvailable = (PlayerPrefs.GetInt(ResearchList[i].gameObject.name +"pointsInvested")); //goes 1-->6-->11

				}

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTSpellShield)
                {

                    shieldUnlocked = true;
                    shieldModifier = 0.10f +0.05f*(PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested")); //goes 15-20-25% of your hp
                    abilityMaster.unlockedAbilities[4] = 1;
                    shieldButton.SetActive(true);
                }

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTPetFlurry)
                {

                    petFlurryUnlocked = true;
                    abilityMaster.unlockedAbilities[5] = 1;
                    petFlurrymodifier = 0.25f * (PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested")); //goes 15-20-25% of your hp

                    petFlurryButton.SetActive(true);
                   // print("this is being called");
                }

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTSoulCharge)
                {

                    soulChargeUnlocked = true;
                    abilityMaster.unlockedAbilities[6] = 1;
                    soulChargeMod = (PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested")); //the dmg will be calculated based on ... the player pref of the ability

                    soulChargeButton.SetActive(true);
                    // print("this is being called");
                }

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTUseBow)
                {
                    if(ResearchList[i].pointsInvested == ResearchList[i].pointCap)
                    {
                        bowUnlocked = true;

                    }
                }

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTPetCrit)
                {

                        petCritUnlocked = true;
                        petCritMod = 3f*(PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested"));

                }

                if (ResearchList[i].UpgradeLevel == Upgrade.EVNTBowMastery)
                {

                    bowMasteryMod = 3*(PlayerPrefs.GetInt(ResearchList[i].gameObject.name + "pointsInvested"));

                }

            }

			ResearchList [i].refreshTalentPoints (); //makes sure the correct number of talen points are there
		}

        abilityMaster.updateAbilities();

    }

    public void returnEnemyDmgAtk() {

        //	if(windowOpen == false){

                EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>();
                EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
                enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso; //this makes a new obj ever click...
                //print("new enemy instance ID found!");
            
            //players dmg is a combination of item mods and stats increases

            double _PlayersDmg = CalcPlayerDmg (); //this method calcs the players dmg convieniently

				
			//this is to do the auto atk swipe
			for (int i = 0; i < AutoAtkList.Count; i++) {

					int randSlash = UnityEngine.Random.Range (0, AutoAtkList.Count);

					if (!AutoAtkList [randSlash].activeInHierarchy) {

						AutoAtkList [randSlash].SetActive (true);

						break;
					} else if(!AutoAtkList [i].activeInHierarchy) {

						AutoAtkList [randSlash].SetActive (true);

						break;
					}

				}

			EnemyObj.GetComponentInChildren<Animator> ().SetTrigger ("TapEnemy");

			if(EnemyControlObj.playerHealthNew>0){

				EnemyControlObj.playerHealthNew -= _PlayersDmg*returnAtkPrestige;

			}

				for (int i = 0; i < dmgAmtList.Count; i++) {

					if (!dmgAmtList [i].activeInHierarchy) {

						dmgAmtList[i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);

						dmgAmtList [i].transform.position = new Vector3 (enemyHeadObj.transform.position.x, enemyHeadObj.transform.position.y+1, 90); //instantiates the beam offscre

						dmgAmtList [i].GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (_PlayersDmg*returnAtkPrestige);

						//	dmgAmtList[i].GetComponent<DroppedDmgScript> ().setDmg (_PlayersDmg);

						dmgAmtList [i].SetActive (true);
						//for dmg all over teh eplace
						//dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
						dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 1.5f);

						break;
					}

				}

			activateBlood (enemyHeadObj, EnemyControlObj);

			EnemyObj.transform.localPosition += new Vector3 (UnityEngine.Random.Range (-5f, 5f) / 40f,0, 0); //slightly moves the enemy for fx

	//	}

	}
		
	IEnumerator HealthRegen(){ //***make sure this is only called once per game and doesnt repeatedly get called

		while (true) {

			if (lifeRegenCoroutine == false) {
				lifeRegenCoroutine = true;
			}

			if (windowOpen==false) {

                double playerTotalHP = playerObj.returnPlayerMaxHP();

				if (playerObj.playerHealthCurrent < playerTotalHP) {

					playerObj.playerHealthNew += InventoryScript.totalListOfAttributes [5]
						+ playerTotalHP * ((potionModList[4]+potionModList[9]+potionModList[14])) + playerTotalHP*petManager.abilityArray[25]; //total hp regen from items, and total hp regen from potions
						
					playerObj.playerHealthCurrent += InventoryScript.totalListOfAttributes [5]
						+ playerTotalHP * ((potionModList[4]+potionModList[9]+potionModList[14])) + playerTotalHP*petManager.abilityArray[25]; //total hp regen from items, and total hp regen from potions

                    if(petManager.abilityArray[28]>0)
                    {
                        playerObj.playerHealthNew -= playerTotalHP * petManager.abilityArray2[28];

                    }

                    if (petManager.abilityArray[29]>0)
                    {
                        playerObj.playerHealthNew -= playerTotalHP * petManager.abilityArray2[29];
                    }

                    playerObj.quickHPUpdate ();

				}

				if (CurseTalent == true) {

					campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ().playerHealthNew -= (CurseAmount / 100f) * campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ().playerHealthNew;
				}

                if (petManager.abilityArray[9] > 0 && playerObj.playerHealthCurrent < playerTotalHP*0.4f) //if Daemon pet is active and the players hp <30%
                {
                    campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>().playerHealthNew -= petManager.abilityArray[9] * campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>().playerHealthMax;
                }

            }

			yield return new WaitForSeconds(1);

		}
	}


	IEnumerator GoldMiner(){ //***make sure this is only called once per game and doesnt repeatedly get called

		while (true) {

			if (GoldMinerCoroutine == false) {
				GoldMinerCoroutine = true;
			}
			if(windowOpen==false){
                if (playerObj != null && enemyStatsObj!=null) //to avoid random error
                {
                    playerObj.playerGold += enemyStatsObj.Enemy1Gold; //this value will eventually change based on the level it has been researched
                }
			}
            goldCastTimer = 6 - GoldMinerTimeReduction - (5 * petManager.abilityArray[6]); //this is so the gold miner text is the same as this

            yield return new WaitForSeconds(6-GoldMinerTimeReduction-(5*petManager.abilityArray[6])); //maybe vary this time? 8-1*playprefes.getint(skil blah blah) 7->6->5  has about the same increase in percent gold gained. for 100 gold/s its 14.2 for 7 sec, 16.6 for 6 and 20 for 5
            //	ability array6 is the gold miner time reduction percent. For example 5*.065*3 = ~1sec reduction
        }
	}

	public double CalcPlayerDmg(){

        float additionalTemplePetGain = ((petManager.abilityArray[10] + petManager.abilityArray[11]) * 100f) * PlayerPrefs.GetInt("StatsAdd");//the ability array gives .02-4-6 times 100 gives 2-4-6 and then times the stats ad give the increase $ 


        double PlayersDmg = (playerObj.playerAtkDmg + playerObj.playerAtkDmg*InventoryScript.totalListOfAttributes [1]/100f +
			playerObj.playerAtkDmg * ((PlayerPrefs.GetFloat ("atkMod") + additionalTemplePetGain) / 100f + damageIncreaseTalent1 + damageIncreaseTalentPrestige))*(1+potionModList[0]+potionModList[5]+potionModList[10]); 

		if (ObscuredPrefs.GetInt ("StarterPack") == 1) {

			PlayersDmg = 2 * PlayersDmg; //this doubles player dmg
		}

		if (ObscuredPrefs.GetInt ("FuryPack") == 1) {

			PlayersDmg = 2 * PlayersDmg; //this doubles player dmg
		}

        if (petManager.abilityArray[30] > 0)
        {
            PlayersDmg = PlayersDmg + petManager.abilityArray[30] * playerObj.spellDamageCalc();
        }

        if (petManager.abilityArray[31] > 0)
        {
            PlayersDmg = PlayersDmg * (1+petManager.abilityArray[31] * statsPointsScript.HealthMod/100f);
        }



        return PlayersDmg;
	}

	public void clickerHeld(){

		if (ObscuredPrefs.GetInt ("FuryPack") == 1) {
			mouseDown = true;
		}

	}

	public void clickerUnheld(){

		mouseDown = false;
		initialFuryDekayTimer = 0.3f;

	}

	public void ClickerGameButtonPress(){

//		print ("button clicked");
		if(windowOpen == false){

                EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>();
                EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
                enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso; //this makes a new obj ever click...
                //print("new enemy instance ID found!");


            bool isCrit = false;

			if (megaSlashEnabled == true) {

				megaSlashScript.decreaseMegaSlashCooldown ();
			}

			//players dmg is a combination of item mods and stats increases

			double _PlayersDmg = CalcPlayerDmg (); //this method calcs the players dmg convieniently

			if (UnityEngine.Random.Range (0, 100) <= (InventoryScript.totalListOfAttributes [7]+advCritEnabled + petManager.abilityArray[17]*100) && critEnabled==true) { ////checking crit % and also if crit is enabbled

				_PlayersDmg = _PlayersDmg * (2 + AtkPrestCritMult); //this can change
				isCrit = true;

                if (petManager.abilityArray[4] > 0) //checks if this is active first before running the method
                {
                    critHeal(); //this is for the pet crit healing
                }

            } else {

				//no crit
			}

			if (playerObj.playerHealthCurrent <= .4f * (playerObj.playerHealthMax) && belowHPbool==true) {
				_PlayersDmg = _PlayersDmg * (double)belowHpLimit; //25% extra dmg
			}


            if (EnemyControlObj.isHittable == true)
            {


                playerAtkAnim(); //this is what makes the player swing his sword or shoot the bow

                

				//for the twenty tap heal skill
				if (lifeStealTalent == true) {
					TwentyTapCount += 1;
					TwentyTapCountText.GetComponent<TextMeshPro> ().text = TwentyTapCount.ToString ();
					TwentyTapCountText.GetComponent<Animator> ().SetTrigger ("Tap");
					TwentyTapCountText.SetActive (true);

					if (TwentyTapCount >= 30) {

						twentyTapHeal ();
						TwentyTapCount = 0;
					}
				}

                //this is where the final damage is applied to the enemy
                if (usingBow == true && BowTapCount == 4)
                {


                    _PlayersDmg = (4.1f + bowMasteryMod / 10f) * _PlayersDmg;
                   

                    EnemyControlObj.playerHealthNew -= _PlayersDmg; //x4.somthing times dmg ADD THE MASTERY BONUS HERE LATER
                    playerAtkHitandFX(isCrit, _PlayersDmg);

                    if (EnemyObj.GetComponentInChildren<Animator>())
                    {
                        EnemyObj.GetComponentInChildren<Animator>().SetTrigger("TapEnemy");
                    }
                }
                else if(usingBow == false)
                {
                    EnemyControlObj.playerHealthNew -= _PlayersDmg; //x4.somthing times dmg
                    playerAtkHitandFX(isCrit, _PlayersDmg);

                    if (EnemyObj.GetComponentInChildren<Animator>())
                    {
                        EnemyObj.GetComponentInChildren<Animator>().SetTrigger("TapEnemy");
                    }

                }


            }

		
		}
	}

	public void ActivateSwordPuff(){

		for (int i = 0; i < SlashPuffList.Count; i++) {

			if (!SlashPuffList [i].activeInHierarchy) {

				SlashPuffList [i].SetActive (true);

				break;
			}

		}

	}

	public void ActivateSwordSlash(){

		//make a sword slash list and /...
		for (int i = 0; i < SlashList.Count; i++) {

			if (!SlashList [i].activeInHierarchy) {

				SlashList [i].SetActive (true);

				break;
			}

		}

	}

	public void playerAtkAnim(){

        if (usingBow == true)
        {
            BowTapCount += 1;

            if (BowTapCount <= 4)
            {
                animatedPlayerObj.animator.SetBool("BowAtk" + (BowTapCount).ToString(), true);//bow tap starts at 0 so we have to add 1 because the anim bools start at 1
            }
        }
        else
        {

            animatedPlayerObj.animator.SetTrigger("PlayerAtk1");//sword atk anim

        }
        //resetting of bow anim bools is done at the end of the 4th animation after it is done playing\



        //    
        //	animatedPlayerObj.animator.ResetTrigger ("PlayerAtk1");//i dont think this is ever used


    }

    public void playerAtkHitandFX(bool isCrit, double _PlayersDmg)
    {




        if (WoundEnabled == true && WoundChance >= UnityEngine.Random.Range(0, 100))
        {

            if (woundBlood.transform.parent != enemyHeadObj.transform)
            { // THIS CAN BE OPTIMIZED A LOT ************!_!_!_!_!_!_!_!_!__!
                woundBlood.transform.parent = enemyHeadObj.transform;
            }

            if (woundBlood.transform.parent == enemyHeadObj.transform)
            {

                woundBlood.transform.localPosition = new Vector3(0, 0, 0);
                woundBlood.transform.localScale = new Vector3(1, 1, 1);

                ParticleSystem woundParticles = woundBlood.GetComponent<ParticleSystem>();
                ParticleSystem.EmissionModule emis1 = woundParticles.emission;
                emis1.enabled = true;
            }

            EnemyControlObj.WoundTime = 5;

        }

        for (int i = 0; i < hitFXList.Count; i++)
        {

            if (!hitFXList[i].activeInHierarchy)
            {

                float Rand1 = UnityEngine.Random.Range(-5f, 5f) / 20f;
                float Rand2 = UnityEngine.Random.Range(-5f, 5f) / 20f;


                hitFXList[i].transform.position = new Vector3(enemyHeadObj.transform.position.x + Rand1, enemyHeadObj.transform.position.y + Rand2, 90);
                //new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, 90); //instantiates the beam offscre
                hitFXList[i].SetActive(true);

                //persistantSoundManager.GetComponent<SoundManager> ().EnchantSword ();

                break;
            }

        }

        if (usingBow == true)
        {

            for (int i = 0; i < bowArrowFx.Count; i++)
            {

                if (!bowArrowFx[i].activeInHierarchy)
                {

                    //float Rand1 = UnityEngine.Random.Range(-5f, 5f) / 20f;
                   // float Rand2 = UnityEngine.Random.Range(-5f, 5f) / 20f;


                   // bowArrowFx[i].transform.position = new Vector3(enemyHeadObj.transform.position.x + Rand1, enemyHeadObj.transform.position.y + Rand2, 90);
                    //new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, 90); //instantiates the beam offscre
                    bowArrowFx[i].SetActive(true);

                    //persistantSoundManager.GetComponent<SoundManager> ().EnchantSword ();

                    break;
                }

            }

        }

        //this is for stunning the enemy with the enemy stun pet
        int randoStun = UnityEngine.Random.Range(1, 101);

        if (petManager.abilityArray[7] * 100 >= randoStun) //ability aray 7 starts out at .01 so *100 = 1.
        {
            EnemyControlObj.EnemyStunned = true;
        }


        if (isCrit == false)
        {

            for (int i = 0; i < dmgAmtList.Count; i++)
            {

                if (!dmgAmtList[i].activeInHierarchy)
                {

                    dmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                    dmgAmtList[i].transform.position = new Vector3(enemyHeadObj.transform.position.x - 0.2f, enemyHeadObj.transform.position.y + 1, 90); //instantiates the beam offscre

                    dmgAmtList[i].GetComponent<DroppedDmgScript>().dmgAmountText.text = LargeNumConverter.setDmg(_PlayersDmg);

                    //	dmgAmtList[i].GetComponent<DroppedDmgScript> ().setDmg (_PlayersDmg);

                    dmgAmtList[i].SetActive(true);
                    //for dmg all over teh eplace
                    //dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
                    dmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.5f);

                    break;
                }

            }


        }
        else
        {

            for (int i = 0; i < CritList.Count; i++)
            {

                if (!CritList[i].activeInHierarchy)
                {

                    persistantSoundManager.GetComponent<SoundManager>().atkCritSound();


                    CritList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                    CritList[i].transform.position = new Vector3(enemyHeadObj.transform.position.x - 0.3f, enemyHeadObj.transform.position.y + 1, 90); //instantiates the beam offscre

                    CritList[i].GetComponent<DroppedDmgScript>().dmgAmountText.text = LargeNumConverter.setDmg(_PlayersDmg);

                    CritList[i].SetActive(true);
                    //this is for dmg all over the place
                    //CritList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-10, 10) / 10f, UnityEngine.Random.Range (5, 9));

                    CritList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.5f);

                    break;
                }

            }

        }

        activateBlood(enemyHeadObj, EnemyControlObj);


        if (EnemyObj.gameObject.name != "Egg")//we dont want the egg to move
        {
            EnemyObj.transform.localPosition += new Vector3(UnityEngine.Random.Range(-3f, 3f) / 50f, 0, 0); //slightly moves the enemy for fx
        }

    }

    public void resetBowAnimBools(bool toReset)
    {
        BowTapCount = 0;

        for (int i = 1; i < 5; i++) //goes from 0 to 4
        {
            animatedPlayerObj.animator.SetBool("BowAtk" + i.ToString(), toReset); //set all the bow atk animations to false so we can start the anim again

        }

    }

    public void twentyTapHeal(){

		TwentyTapHealAnim.SetActive (true);

		persistantSoundManager.GetComponent<SoundManager> ().twentyTapHeal ();

			if (playerObj.playerHealthCurrent < (playerObj.playerHealthMax)) {

			playerObj.playerHealthNew += (playerObj.playerHealthMax) * lifeStealAmount/100f;

			playerObj.playerHealthCurrent += (playerObj.playerHealthMax) * lifeStealAmount/100f;

				playerObj.quickHPUpdate ();

			}
	}

    public void critHeal() //this is for the pet that can crit heal!
    {

        if (playerObj.playerHealthCurrent < (playerObj.playerHealthMax))
        {

            playerObj.playerHealthNew += (playerObj.playerHealthMax) * (petManager.abilityArray[4]);

            playerObj.playerHealthCurrent += (playerObj.playerHealthMax) * (petManager.abilityArray[4]);

            playerObj.quickHPUpdate();

        }
    }

    public void spellCritHeal() //this is for the pet that can crit heal!
    {

        if (playerObj.playerHealthCurrent < playerObj.playerHealthMax)
        {

            playerObj.playerHealthNew += (playerObj.playerHealthMax) * (petManager.abilityArray[5]);

            playerObj.playerHealthCurrent += (playerObj.playerHealthMax) * (petManager.abilityArray[5]);

            playerObj.quickHPUpdate();

        }
    }

    public void gemEssenceActivate(float GranikMult)
    {

		if (GemEssenceEnabled == true) { //this means auto attack has been researcheds

            if (playerObj.playerHealthCurrent < playerObj.returnPlayerMaxHP())
            {

                playerObj.playerHealthNew += (playerObj.playerHealthMax) * (GemEssenceBonus * GranikMult / 100);

                playerObj.playerHealthCurrent += (playerObj.playerHealthMax) * (GemEssenceBonus * GranikMult / 100);

                playerObj.quickHPUpdate();

            }
		}
	}

	public void gemShardsActivate(float GranikMult){

		if(GemShardsEnabled == true && windowOpen==false){

			ControlEnemy EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();
			GameObject EnemyObj = campaignLevelEnemySpawner.EnemySpawned;


			//players dmg is a combination of item mods and stats increases
			double _PlayersDmg = CalcPlayerDmg();
            double _PlayersSplDmg = playerObj.spellDamageCalc();

            //total damage calc: (Gemshards bonus (3x.4x.5x) * physical dmg + gemshards pct (30,40,50%)*spell dmg)*granikmult( 1 or 2x)
            double _playerTotalDmg = (GemShardsBonus * _PlayersDmg + GemShardsBonus/10f*_PlayersSplDmg) * GranikMult;


            if (EnemyControlObj.isHittable == true) {
                EnemyControlObj.playerHealthNew -= _playerTotalDmg;

            }
			//Mathf.FloorToInt (playerObj.playerAtkDmg * (InventoryScript.totalListOfAttributes [1] / 100f)) ; 

			EnemyObj.GetComponentInChildren<Animator> ().SetTrigger ("TapEnemy");

			//		ScreenShaker.activateScreenShake (0.25f, 0.2f);

			if(EnemyControlObj.playerHealthNew<=0){

				//		EnemyObj.GetComponentInChildren<EnemyRagdoll> ().createExplosion();
			//	EnemyObj.GetComponentInChildren<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll

			}
			//get the enemy head positions here before so u can quickly add them and the game doesnt have to do it 4 times...
			GameObject enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll> ().Torso;


				for (int i = 0; i < dmgAmtList.Count; i++) {

					if (!dmgAmtList [i].activeInHierarchy) {

						dmgAmtList[i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);

					dmgAmtList [i].transform.position = new Vector3 (enemyHeadObj.transform.position.x-0.2f, enemyHeadObj.transform.position.y+1, 90); //instantiates the beam offscre

					//	dmgAmtList [i].GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (GemShardsBonus * _PlayersDmg);

						dmgAmtList [i].GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (_playerTotalDmg);

						dmgAmtList [i].SetActive (true);
						//for dmg all over teh eplace
						//dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
						dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 1.5f);

						break;
					}

				}

			activateBlood (enemyHeadObj, EnemyControlObj);

//			for (int i = 0; i < bloodAmtList.Count; i++) {
//
//				if (!bloodAmtList [i].activeInHierarchy) {
//
//					bloodAmtList [i].transform.position = new Vector3 (enemyHeadObj.transform.position.x, enemyHeadObj.transform.position.y, 90); //instantiates the beam offscre
//
//					bloodAmtList [i].SetActive (true);
//
//					break;
//				}
//
//			}

		}

	}

	public void activateBlood(GameObject enemyObj, ControlEnemy enemyControlObj){


		if (enemyControlObj.isSkele == false) {

			for (int i = 0; i < bloodAmtList.Count; i++) {

				if (!bloodAmtList [i].activeInHierarchy) {

					ParticleSystem.MainModule main = bloodAmtList [i].GetComponent<ParticleSystem> ().main; 

					main.startColor = enemyControlObj.enemyBldColor;

					bloodAmtList [i].transform.position = new Vector3 (enemyObj.transform.position.x, enemyObj.transform.position.y, 90); //instantiates the beam offscre

					bloodAmtList [i].SetActive (true);

					break;
				}

			}

		} 
		if (enemyControlObj.isSkele == true) {

			for (int i = 0; i < skeleParticlesList.Count; i++) {

				if (!skeleParticlesList [i].activeInHierarchy) {

					skeleParticlesList [i].transform.position = new Vector3 (enemyObj.transform.position.x, enemyObj.transform.position.y, 90); //instantiates the beam offscre

					skeleParticlesList [i].SetActive (true);

					break;
				}

			}

		} 
		if (enemyControlObj.isChest == true) {

		//	chestParticlesList

			for (int i = 0; i < chestParticlesList.Count; i++) {

				if (!chestParticlesList [i].activeInHierarchy) {

					chestParticlesList [i].transform.position = new Vector3 (enemyObj.transform.position.x, enemyObj.transform.position.y, 90); //instantiates the beam offscre

					chestParticlesList [i].SetActive (true);

					break;
				}

			}
		}

	}

	public void MegaSlashButtonPress(){

			//	print ("Megha slash button clicked");


			ControlEnemy EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();
			GameObject EnemyObj = campaignLevelEnemySpawner.EnemySpawned;

			//25x player dmg for MegaSlash
			double _PlayersDmg = CalcPlayerDmg();

			if (EnemyControlObj.isHittable == true) {
			EnemyControlObj.playerHealthNew -= (_PlayersDmg*(megaSlashMultiplier+megaSlashMultiplierPrestige));
			}
			//Mathf.FloorToInt (playerObj.playerAtkDmg * (InventoryScript.totalListOfAttributes [1] / 100f)) ; 

			EnemyObj.GetComponentInChildren<Animator> ().SetTrigger ("TapEnemy");

			//		ScreenShaker.activateScreenShake (0.25f, 0.2f);

			if (EnemyControlObj.playerHealthNew <= 0) {

				//		EnemyObj.GetComponentInChildren<EnemyRagdoll> ().createExplosion();
				EnemyObj.GetComponentInChildren<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll

			}
			//get the enemy head positions here before so u can quickly add them and the game doesnt have to do it 4 times...
			GameObject enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll> ().Torso;


		//	megaSlashDmg.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);

		//	megaSlashDmg.transform.position = new Vector3 (0, 1.5f, 86); //instantiates the beam offscre

		megaSlashDmg.GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (_PlayersDmg*(megaSlashMultiplier+megaSlashMultiplierPrestige));


			megaSlashDmg.SetActive (true);
			//for dmg all over teh eplace
			//dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
		//	megaSlashDmg.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 2.5f);

	//activate all blood
			for (int i = 0; i < bloodAmtList.Count; i++) {

				if (!bloodAmtList [i].activeInHierarchy) {

					bloodAmtList [i].transform.position = new Vector3 (enemyHeadObj.transform.position.x, enemyHeadObj.transform.position.y, 90); //instantiates the beam offscre

					bloodAmtList [i].SetActive (true);

					
				}

			}



			EnemyObj.transform.localPosition += new Vector3 (UnityEngine.Random.Range (-5f, 5f) / 40f, 0, 0); //slightly moves the enemy for fx

		


	}

	public void SpellDamage(double SpellDamage){

		GameObject EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
		ControlEnemy EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy> ();

		GameObject EnemyTorso = EnemyObj.GetComponentInChildren<EnemyRagdoll> ().Torso;

		for (int i = 0; i < magicDmgAmtList.Count; i++) {

			if (!magicDmgAmtList [i].activeInHierarchy) {

				magicDmgAmtList[i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);

				magicDmgAmtList [i].transform.position = new Vector3 (EnemyTorso.transform.position.x-0.2f, EnemyTorso.transform.position.y, 90); //instantiates the beam offscre

				//magicDmgAmtList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

				magicDmgAmtList[i].GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (SpellDamage);


				magicDmgAmtList [i].SetActive (true);

				magicDmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 3f);

				break;
			}

		}

		activateBlood (EnemyTorso, EnemyControlObj);

		EnemyObj.GetComponentInChildren<Animator> ().SetTrigger ("TapEnemy");
		EnemyObj.transform.localPosition += new Vector3 (UnityEngine.Random.Range (-5f, 5f) / 50f,0, 0); //slightly moves the enemy for fx


	}

	public void spellCritText(double SpellDamage){

		GameObject EnemyObj = campaignLevelEnemySpawner.EnemySpawned;


		for (int i = 0; i < magicCritDmgList.Count; i++) {

			if (!magicCritDmgList [i].activeInHierarchy) {

				persistantSoundManager.GetComponent<SoundManager> ().splCritSound ();

				magicCritDmgList[i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);

				magicCritDmgList [i].transform.position = new Vector3 (EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso.transform.position.x-0.3f, EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso.transform.position.y, 90); //instantiates the beam offscre

			//	magicCritDmgList[i].GetComponent<DroppedDmgScript> ().setDmg (SpellDamage);

				magicCritDmgList[i].GetComponent<DroppedDmgScript> ().dmgAmountText.text = LargeNumConverter.setDmg (SpellDamage);


				magicCritDmgList [i].SetActive (true);

				magicCritDmgList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 1.5f);

				break;
			}

		}

	}

	void Update(){

		if (refreshAtkTimer == true && autoAtkTimer >= 0) {

			autoAtkTimer -= Time.deltaTime;
			atkTimeString =  autoAtkTimer.ToString ();
			atkTimeString = atkTimeString.Substring (0, 3);
			atkTimeTxt.text = "Auto Atk: " + atkTimeString;
		}


		if (goldMiningEnabled == true && goldCastTimer >= 0) {

			goldCastTimer -= Time.deltaTime;

			goldTimeString = goldCastTimer.ToString ();
            if (goldTimeString.Length > 3)
            {
                goldTimeString = goldTimeString.Substring(0, 3);
            }
            else
            {

            }
			goldTimeText.text = "Gold Miner: " + goldTimeString;

		} else {
			goldCastTimer = 5f;
		}

		if (mouseDown == true) {
		//	print ("holding");
			if (initialFuryDekayTimer >= 0) { //this is just so we are counting it down forever
				initialFuryDekayTimer -= Time.deltaTime;
			}

			if (initialFuryDekayTimer < 0) {//this is so a hold is found 

				FuryPurchaseTimer -= Time.deltaTime;


				if (FuryPurchaseTimer < 0) {//this is after a hold is found

					ClickerGameButtonPress ();
					FuryPurchaseTimer = 0.16f;
				}

			}
		}

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            openQuitGameMenu();
        }


    }

    public void openQuitGameMenu()
    {

        closeAllWindows(quitGamePrompt);

        quitGamePrompt.SetActive(true);

        windowOpen = true;
       

    }

    public void QuitGame()
    {

        Application.Quit();
    }


    IEnumerator AutoAtkRoutine(){

		while (true) {

			if (automatedAttackBool == true 
				&& playerObj.lvUpButton.activeSelf==false
				&& windowOpen==false) {
			//	print("auto atk!");
				ClickerGameButtonPress ();

                if (usingBow == false)
                {
                    for (int i = 0; i < AutoAtkList.Count; i++)
                    {

                        int randSlash = UnityEngine.Random.Range(0, AutoAtkList.Count);

                        if (!AutoAtkList[randSlash].activeInHierarchy)
                        {

                            AutoAtkList[randSlash].SetActive(true);

                            break;
                        }
                        else if (!AutoAtkList[i].activeInHierarchy)
                        {

                            AutoAtkList[randSlash].SetActive(true);

                            break;
                        }

                    }
                }
				refreshAtkTimer = true;
				autoAtkTimer = AutomatedAttackRate*(1-petManager.abilityArray[2]- IncreasedAutoAtk); //incr auto atk is only available if the pt is >2
			}


			yield return new WaitForSeconds (autoAtkTimer);// - potionModList[2] - potionModList[7]);




		}
	}

	public void startFlurry(){

		if(flurryUnlocked==true && flurryButton.GetComponentInChildren<AtkFlurryCooldown>().isReady==true){
			flurryTimer = 8 + 2 * flurryTimerBonus + PrestFlurryTimerBonus;

//			Input.mousePosition = new Vector2 (0, 0); //sets the moust at 0,0

			StartCoroutine ("FlurryAutoAtk");
		}
	}

    public void startSoulCharge()
    {

        if (soulChargeUnlocked == true && soulChargeButton.GetComponentInChildren<SoulChargeCooldown>().isReady == true)
        {

            GameObject soulCharge = (GameObject)Instantiate(soulChargePrefab, new Vector3(1.89f, 2.47f, 90), Quaternion.identity) as GameObject;


        }
    }


    public void startPetFlurry()
    {

        if (petFlurryUnlocked == true && petFlurryButton.GetComponentInChildren<PetFlurryCooldown>().isReady == true)
        {
            petFlurryTime = 8;

            //pet flurry mod is changed here because its not needed before you use the ability. Also, we dont want the PetManager to get this value all the time
            

            StartCoroutine("PetFlurryAtk");
        }
    }

    public void startShield()
    {

        if (shieldUnlocked == true && shieldButton.GetComponentInChildren<ShieldCooldown>().isReady == true)
        {

            playerObj.createShield(shieldModifier*playerObj.returnPlayerMaxHP());

        }
    }

    public void startSpellFlurry(){

		if(spellflurryUnlocked==true && spellFlurryButton.GetComponentInChildren<AtkFlurryCooldown>().isReady==true){
			playerObj.spellFlurryTimer = spellFlurryTime + spellFlurryTimePrestige;
			playerObj.StartCoroutine ("SpellFlurry");
			persistantSoundManager.GetComponent<SoundManager> ().SpellFlurryActivate ();
		}
	}

	IEnumerator FlurryAutoAtk(){

		while (true) {

			if (automatedAttackBool == true 
				&& windowOpen == false
				&& playerObj.lvUpButton.activeSelf==false
				&& flurryTimer>0) {
			//	print("auto atk!");
				ClickerGameButtonPress ();

				for (int i = 0; i < AutoAtkList.Count; i++) {

					int randSlash = UnityEngine.Random.Range (0, AutoAtkList.Count);

					if (!AutoAtkList [randSlash].activeInHierarchy) {

						AutoAtkList [randSlash].SetActive (true);

						break;
					} else if(!AutoAtkList [i].activeInHierarchy) {

						AutoAtkList [randSlash].SetActive (true);

						break;
					}

				}
			//	flurryTimerText.text = flurryTimer.ToString ();
				flurryTimer -= .25f;
			}

		//	print ("Auto atk flurry is running!!");

			yield return new WaitForSeconds(0.25f);

			if (flurryTimer <= 0) {

				StopCoroutine ("FlurryAutoAtk");
			}
		}
	}

    IEnumerator PetFlurryAtk()
    {

        while (true)
        {

            if (windowOpen == false
                && petFlurryTime > 0)
            {
                //	flurryTimerText.text = flurryTimer.ToString ();
                petFlurryTime -= 1f;
            }

            //	print ("Auto atk flurry is running!!");

            yield return new WaitForSeconds(1f);

            if (petFlurryTime <= 0)
            {
                //petFlurrymodifier = 0;//resets the modifier so the pets dont attack faster. The only time this is changed is in startPetFlurry(); when the button is actually pressed
                StopCoroutine("PetFlurryAtk");
            }
        }
    }

    IEnumerator checkStuffAffordable(){

		while (true) {

	
			int spellAvailable = 0;
			int weaponAvailable = 0;
			int levelAvailable = 0;
			//checking for weapon
			if(masterWeaponListScript.ListOfWeapons[0]._newItemCost==0){ //so it doesnt rerun the same laoding of data over and over (very intensvie and causes stutter)

				//masterWeaponListScript.loadSword1Dict (); 
				masterWeaponListScript.updateWeaponAvailability(); //assigns the numbers in the list to the weapons and also if a weap is purchasable
				masterWeaponListScript.loadWeaponData (); //this gets the data and also loads the prices
               // masterWeaponListScript.updatePrices();

             //   print("wpn cost is 0");
            }


            for (int i = 0; i < masterWeaponListScript.ListOfWeapons.Count; i++) {

				masterWeaponListScript.ListOfWeapons [i].updateItemCosts ();
				//this is now contained in wpnupgrade because the spell version wouldnt work in the mmbolie version. will do for spells and levels too...
				if (masterWeaponListScript.ListOfWeapons [i]._newItemCost <= playerObj.playerGold 
					&& PlayerPrefs.GetInt ("Wpn" + masterWeaponListScript.ListOfWeapons [i].itemNumber.ToString())<5) { //doesnt check weapons that are maxed

					weaponAvailable += 1; //amount available
				}
					
			}
			if (weaponAvailable >= 1) {

				newWeaponNotification.SetActive (true);

			} else {
				newWeaponNotification.SetActive (false);

			}
			//checking for weapon

			//checking for Spells
			if(masterSpellListScript.ListOfSpells[0]._newItemCost==0){ //so it doesnt rerun the same laoding of data over and over (very intensvie and causes stutter)
	
				//print ("loading the new spells!!");

				masterSpellListScript.updateSpellAvailability ();
				masterSpellListScript.loadSpellUpgrades (); //gets the data from the excel sheet for the spells
                masterSpellListScript.updatePrices();
            }

			for (int i = 0; i < masterSpellListScript.ListOfSpells.Count; i++) {
				
					masterSpellListScript.ListOfSpells [i].updateSpellCosts ();

				if (masterSpellListScript.ListOfSpells [i]._newItemCost <= playerObj.playerGold 
					&& PlayerPrefs.GetInt ("Spl" + masterSpellListScript.ListOfSpells [i].itemNumber.ToString())<5
					&& spellsEnabled==true) { //doesnt check weapons that are maxed

					spellAvailable += 1; //amount available
				}

			}

			if (spellAvailable >= 1) {

				newSpellNotification.SetActive (true);

			} else {
				newSpellNotification.SetActive (false);

			}
			//checking for Spell

			//checking for level
			if (LevelCompleteObject.ListOfLevels [1].GetComponent<LevelUpgrade> ()._levelCost == 0) { //so it doesnt rerun the same laoding of data over and over (very intensvie and causes stutter)
				LevelCompleteObject.updateLevelAvailability();
				LevelCompleteObject.updateLevelCost ();//updates level cost after the talent is taken or if the cost hasnet been updated yet
			}

			for (int i = 0; i < LevelCompleteObject.ListOfLevels.Count; i++) {

				if (LevelCompleteObject.ListOfLevels[i].GetComponent<LevelUpgrade> ()._levelCost <= playerObj.playerGold 
					&& LevelCompleteObject.ListOfLevels[i].GetComponent<LevelUpgrade> ().levelUnlocked==0) {

					levelAvailable += 1; //amount available
				}

			}
			if (levelAvailable >= 1) {

				newLevelNotification.SetActive (true);

				if (PlayerPrefs.GetInt ("UnlockLevelExplain") == 0) {

					if (masterWeaponListScript.ListOfWeapons [0]._itemLV >= 2 && enemyStatsObj.CurrentLevel==1 || masterSpellListScript.ListOfSpells [0]._itemLV >= 2 && enemyStatsObj.CurrentLevel==1) { //players may find this out before lv1

						backFromItemDescription (); //closes all windows open at the moment so we can go to tutorial

						TutorialObject.wizardSprite.sprite = TutorialObject.wizardSprites [1];
						TutorialObject.twoPartTutorial = true;
						TutorialObject.unlockLevelTut = true;

						TutorialObject.disableButtons (TutorialObject.buttonList [0]);

						persistantSoundManager.GetComponent<SoundManager> ().oldManHappy ();

						TutorialObject.TutPartOne ("Looks like you are ready for the next world!");

						TutorialObject.secondThingToSay = "Tap on World 2 to unlock it!";

						PlayerPrefs.SetInt ("UnlockLevelExplain", 1);
					}

				//	<color=#dd480a>Attack</color>, <color=#20fff2>Magic</color>,
					//explain unlocking levels here
				}

			} else {
				
				newLevelNotification.SetActive (false);

			}
            //checking for dungeons

            if (PlayerPrefs.GetInt("DungeonComp") == 0 && PlayerPrefs.GetInt("PrestigeLv") >= 1 || PlayerPrefs.GetInt("BonusDungeonComp") == 0 && PlayerPrefs.GetInt("PrestigeLv") >= 1)
            {
                DailyDungeonNotification.SetActive(true);
                DailyDungeonTabNotification.SetActive(true);
            }

            if (PlayerPrefs.GetInt("DungeonComp") == 1 && PlayerPrefs.GetInt("BonusDungeonComp") == 1)
            {
                DailyDungeonNotification.SetActive(false);
                DailyDungeonTabNotification.SetActive(false);
            }

            //checking for Talents

            if (PlayerPrefs.GetInt ("ResearchPoints") >= 1) {

				newTalentNotification.SetActive (true);

			} else {
				newTalentNotification.SetActive (false);

			}

			if (PlayerPrefs.GetInt ("PrestigePts") >= 1) {

				newPrestigeNotification.SetActive (true);

			} else {

				newPrestigeNotification.SetActive (false);

			}

			if (PlayerPrefs.GetFloat ("SkillPoints") > 0 && statsWindow.activeSelf == false && playerObj.lvUpButton.activeSelf==false) {

				statsAvailableNotification.SetActive (true);
			} else {

				statsAvailableNotification.SetActive (false);
			}
			//checking for Spell

			//Check for new gold alert from the shop

				if(PlayerPrefs.GetInt("GoldCardCollection")==0){
					shopGoldNotification.SetActive (true);
				} else {

					//twohoursshoprotation
				twoHourShopRotation.refreshSavedShopTime();//this will refresh the player pref if the time difference is greater than 2 hrs

					shopGoldNotification.SetActive (false);
				}
			


			//PlayerPrefs.GetInt (ListOfLevels [i].gameObject.name);

			//	public GameObject newLevelNotification;
			//	public GameObject newSpellNotification;
			//	public GameObject newTalentNotification;

			//PlayerPrefs.SetFloat ("TimePlayed", PlayerPrefs.GetFloat ("TimePlayed") + 3);

			

			yield return new WaitForSeconds(3f);

		}
	}

	public void setNotifications(){

		notificationScript.Stop ();
		notificationScript.OneTimeBigIconOneDay ();
		notificationScript.OneTimeBigIcon3Days ();
		notificationScript.OneTimeBigIcon5Days ();
	}

	public void closeAllWindows(GameObject pageOpen){
		
		MainCharacterPage.SetActive (false);

        LimitedTimeOffers.closeOffers();//this closes the offer window if the menu is clicked

		if (pageOpen != UpgradeWeaponPage) {
			UpgradeWeaponPage.SetActive (false);
		}

		if (pageOpen != spellWindow) {
			spellWindow.SetActive (false);
		}

			unlockLevelPopUp.SetActive (false);

		if (pageOpen != LevelWindow) {
			LevelWindow.SetActive (false);
		}

        //if (pageOpen != dailyDungeonWindow)
        //{
        //    dailyDungeonWindow.SetActive(false);
        //}

        if (pageOpen != talentWindow) {
			talentWindow.SetActive (false);
		}

		if (pageOpen != potionsPage) {
			potionsPage.SetActive (false);
		}

		if (pageOpen != shopWindow) {
			shopWindow.SetActive (false);
		}
		unlockLevelPopUp.SetActive (false);

		statsWindow.SetActive (false);

		playerObj.lvUpRaysGfx.SetActive (false);

        if (pageOpen != quitGamePrompt) {
            quitGamePrompt.SetActive(false);
        }


		settingsPage.SetActive (false);

		equipmentBackButton.SetActive (false);

		//for loop to reset all sprites to unpress 
		for (int i = 0; i < MenuButtons.Count; i++) {

			MenuButtons [i].setToDefaultSprite ();

		}

		menuPausedScreen.SetActive (false);

	//	persistantSoundManager.GetComponent<SoundManager> ().BackButton ();
	}

	public void OpenMainCharPage(){

		closeAllWindows (null);
		MainCharacterPage.SetActive (true);
		persistantSoundManager.GetComponent<SoundManager> ().openBag ();//this is already taken care of for the menu icons in SpriteToggle.Cs
		//print ("homepage pressed");

		windowOpen = true;

		if (PlayerPrefs.GetInt ("StatsMax") == 0) { //this is for prestige lv0
			PlayerPrefs.SetInt ("StatsMax", 100); //this is the max slider value

		}

        float additionalTemplePetGain = ((petManager.abilityArray[10] + petManager.abilityArray[11]) * 100f) * PlayerPrefs.GetInt("StatsAdd");//the ability array gives .02-4-6 times 100 gives 2-4-6 and then times the stats ad give the increase $ 


        float maxStatsValue = PlayerPrefs.GetInt ("StatsMax")*(PlayerPrefs.GetInt("PrestigeLv")+2);

		AtkSlider.value = PlayerPrefs.GetFloat("atkMod")/maxStatsValue; 
		HPSlider.value = PlayerPrefs.GetFloat("HealthMod")/maxStatsValue; 
		LuckSlider.value = PlayerPrefs.GetFloat ("luckMod")/maxStatsValue;

        AtkSliderTxt.GetComponent<TextMeshProUGUI>().text = "+ " + (PlayerPrefs.GetFloat("atkMod") + additionalTemplePetGain).ToString() + "%";// + maxStatsValue.ToString(); //atk slider "SKILL (+ATK %) " + 
		AtkSliderTxtLeft.GetComponent<TextMeshProUGUI> ().text = "P Atk %"; //atk slider "SKILL (+ATK %) " + 

        HpSliderTxt.GetComponent<TextMeshProUGUI>().text = "+ " + (PlayerPrefs.GetFloat("HealthMod") + additionalTemplePetGain).ToString() + "%";// + maxStatsValue.ToString(); //hp slider "STRENGTH (+HP %) " + 
		HpSliderTxtLeft.GetComponent<TextMeshProUGUI> ().text = "HP %"; //hp slider "STRENGTH (+HP %) " + 

        LuckSliderTxt.GetComponent<TextMeshProUGUI>().text = "+ " + (PlayerPrefs.GetFloat("luckMod") + additionalTemplePetGain).ToString() + "%";// + maxStatsValue.ToString(); //int slider "INT (+ MAGIC %) " + 
		LuckSliderTxtLeft.GetComponent<TextMeshProUGUI> ().text ="M Atk %"; //int slider "INT (+ MAGIC %) " + 


		//add the text change, get comp. in childer of the orig gameobj
		OpenInventory();

	//	equipmentBackButton.SetActive (true);
	}

	public void OpenUpgradeWeapon(){

		if (UpgradeWeaponPage.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound


		} else {




			closeAllWindows (UpgradeWeaponPage);

			MenuButtons [2].toggleSprites ();


			equipmentBackButton.SetActive (true);

			UpgradeWeaponPage.SetActive (true);

			if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {
				windowOpen = true;
				menuPausedScreen.SetActive (true);
			} else {
				windowOpen = false;

			}


			if (TutorialObject.wpnMenuTap == true) {

				TutorialObject.pointAtWpnUpgrade ();
			}
		}


	//	equipmentBackButton.SetActive (true);

	}

	public void checkAct2Stuff(){


	}

	public void checkForNotification(){

	}

	public void updatePlayerInfo(){

		InventoryScript.totalMods (); //just to make sure the mods are added together before enemy is spawned

	}

public void resetPlayerPrefs(){

		playerObj.playerGold = 0;
		playerObj.playerGoldOld = 0; //there is a coroutine that runs every 2 seconds so it may actually keep the players gold even if u reset it (corountine is in control p1 script)

      

        PlayerPrefs.DeleteAll ();
        ResearchCheckAtStart ();

        for (int i = 0; i < abilityMaster.unlockedAbilities.Length; i++)
        {
            abilityMaster.unlockedAbilities[i] = 0;

        }
        //	Application.LoadLevel ("ShadowTestScene");

        for (int i = 0; i < abilityMaster.equippedAbilities.Length; i++)
        {
            abilityMaster.equippedAbilities[i] = -1;

        }
        //	Application.LoadLevel ("ShadowTestScene");

        abilityMaster.saveAbilitiesEquipped();
        //unlocked abilities is automatically populated by researchcheckatstart();
    }

public void loadLevel(int Level){


        //PlayerPrefs.SetInt("CurrentLevel",Level); //this has been moved to the levelupgrade script because are loading the level index now.

        //level index corresponds to the list of levels in checkLevComplete to go to [0,1,2]

        //playerprefs CurrentLevel will tell the CSV reader which rows to grab, and levelIndex will be which level of those rows to go to 

        //actually maybe we will just kleep it as it was, and change all the indexing to 0 because the rows will be changing based on current level...

            PlayerPrefs.SetInt("CurrentLevel", Level);

            LoadingPane.SetActive (true);
	
		    Application.LoadLevel ("ShadowTestScene");

			Canvas2.SetActive (true);


	//	}

}

    public void loadDungeon(int Level)
    {

        LoadingPane.SetActive(true);

        Application.LoadLevel("DailyDungeonScene");

        Canvas2.SetActive(true);

    }

    public void loadPreviousLevel(){

        //	GameObject enemyStatsObj = GameObject.Find ("EnemyStatsInheriter");

        if (enemyStatsObj.dungeonMode==false && enemyStatsObj.eventMode == false) {
            PlayerPrefs.SetInt("DefeatedAtBoss", 1);//so we can show the player a damage ad if they need it. Set to =0 at EnemyRagdoll when boss is defeated
            loadLevel(enemyStatsObj.GetComponent<EnemyStatsInheriter>().CurrentLevel - 1);
        }
        else
        {
            enemyStatsObj.eventMode = false;
            enemyStatsObj.dungeonMode = false;
            enemyStatsObj.dungeonModeBonus = false;
            loadLevel(enemyStatsObj.GetComponent<EnemyStatsInheriter>().CurrentLevel);

        }

        //	LoadingPane.SetActive (true);

        //	Application.LoadLevel ("ShadowTestScene");


    }

	public void OnClicked(Button button)
	{
		if (button.GetComponent<ItemInfo> () != null) {


			buttonHolder = button.GetComponent<ItemInfo> (); //this holds the button that was clicked so we can get the playerprefs from it etc.



			if (PlayerPrefs.GetInt (buttonHolder.GetComponent<Image> ().sprite.name) == 1) {
				
				itemDescriptionEquip.SetActive(true);

				itemPurchaseButton.SetActive(false);

			} else {
				
				itemDescriptionEquip.SetActive(false);

				itemPurchaseButton.SetActive(true);
			}
		}

		if (button.GetComponent<SpellInfo> () != null) {

			if(persistantSoundManager!=null){
				persistantSoundManager.GetComponent<SoundManager> ().openBag ();
			}

//			print(button.name);

			spellButtonHolder = button.GetComponent<SpellInfo> (); //this holds the button that was clicked so we can get the playerprefs from it etc.

	//		print(spellButtonHolder.spellPlayerPref.ToString() + " = " + PlayerPrefs.GetFloat(spellButtonHolder.spellPlayerPref));

			if(PlayerPrefs.GetFloat(spellButtonHolder.spellPlayerPref)==1){ //this is a float... just make sure

	//			print ("spell has already been unlocked");

				spellDescriptionEquip.SetActive(false);
	
			}else{

				spellDescriptionEquip.SetActive(true);


			}
			//this is all the spell information
			spellTitleText.text = button.GetComponent<SpellInfo>().spellTitle;
			spellDescritpionText.text = button.GetComponent<SpellInfo>().spellDescription;
			spellDescriptionImage.sprite = button.GetComponent<SpellInfo>().spellSprite;

			//this was to close the spellbook behind the player, but u cant see the current spell poits...


			spellWindow.GetComponent<ScrollRect>().enabled=false;
			spellWindow.GetComponent<RawImage>().enabled=false;

			spellDescriptionWindow.SetActive(true);
		//	spellWindow.SetActive(false);
		

		}



		//this is where the player prefs will be set depending on which button you choose
	}

	public void OpenSpellBook(){

	//	Canvas2.SetActive(false);

		if (spellWindow.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound

		} else {

			if (spellsEnabled == false) {
				showErrorMsg ("You need the Learn Magic Talent for spells!");
			}

				closeAllWindows (spellWindow);

			if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {
				windowOpen = true;
				menuPausedScreen.SetActive (true);
			} else {
				windowOpen = false;

			}
				MenuButtons [1].toggleSprites ();

				spellWindow.SetActive (true);
				equipmentBackButton.SetActive (true);

		}

	//	equipmentBackButton.SetActive (true);


	}

	public void OpenIAPShop(){

		if (shopWindow.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound

		} else {

			closeAllWindows (shopWindow);

			shopWindow.SetActive (true);

			MenuButtons [5].toggleSprites ();

			windowOpen = true;
		}

        if(PlayerPrefs.GetInt("EventLevel") == 10)
        {

            resetEventCard.SetActive(true);
      //      print("setting the event card reset to true");
        }
        else
        {
            resetEventCard.SetActive(false);
        }

    }

	public void OpenSettings(){

		//	Canvas2.SetActive(false);

		persistantSoundManager.GetComponent<SoundManager> ().openBag ();//this is already taken care of for the menu icons in SpriteToggle.Cs

		closeAllWindows(settingsPage);
		settingsPage.SetActive (true);

		windowOpen = true;


		//	equipmentBackButton.SetActive (true);


	}

	public void OpenResearchTalent(){

		if (talentWindow.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound

		} else {

			closeAllWindows (talentWindow);

			talentWindow.SetActive (true);

			MenuButtons [3].toggleSprites ();

			windowOpen = true;


				if(TutorialObject.explainStats == true){//if we are  coming from the stats tutorial... here we are refreshing all variables and starting from 0 on a 2 part tutorial.

					TutorialController tutObj = TutorialObject;

					tutObj.refreshAllVariables();

					tutObj.wizardSprite.sprite = tutObj.wizardSprites [2];
					tutObj.wizardHand.sprite = tutObj.wizardSprites [4];

					tutObj.twoPartTutorial = true;
					tutObj.talentsTutorial = true;

					persistantSoundManager.GetComponent<SoundManager> ().oldManConfident ();


					tutObj.TutPartOne ("Welcome to the Talent Screen! Talents will help us defeat those pesky monsters faster!");
					tutObj.secondThingToSay = "Talents give your hero special upgrades. There are <color=#dd480a>Attack</color>, <color=#20fff2>Magic</color>, and <color=#fff949>Wealth</color> talents. Explore them all!";

					tutObj.gameObject.SetActive(true);

				}

		}




	//	equipmentBackButton.SetActive (true);


	}

	public void OpenPotionsPage(){

		if (potionsPage.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound

		} else {

			closeAllWindows (potionsPage);

			potionsPage.SetActive (true);


			MenuButtons [4].toggleSprites ();

			if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {
				windowOpen = true;
				menuPausedScreen.SetActive (true);
			} else {
				windowOpen = false;

			}

			equipmentBackButton.SetActive (true);


			if (TutorialObject.potionTutorial == true) {

				TutorialObject.refreshAllVariables (); //yay this should be the last of the tutorials

			}
				
		}
	}


	public void PurchaseSpell(Button button)
	{
		//spellWindow.GetComponent<SpellPoints> ().PointsToSpend >= 1

		if (spellWindow.GetComponent<SpellPoints> ().PointsToSpend > 0) {

			spellWindow.GetComponent<SpellPoints> ().PointsToSpend -=1;

			spellWindow.GetComponent<SpellPoints>().SpellPointsText.text = spellWindow.GetComponent<SpellPoints> ().PointsToSpend.ToString();

				PlayerPrefs.SetFloat("SpellPoints",spellWindow.GetComponent<SpellPoints> ().PointsToSpend); //unlocks da spell

				PlayerPrefs.SetFloat(spellButtonHolder.spellPlayerPref,1);//this will make the spell learned.

			spellButtonHolder.unlockSpell();
		
			GameObject SpellParticles = (GameObject)Instantiate (SpellBuyParticles, new Vector3 (button.transform.position.x, button.transform.position.y, 0), Quaternion.identity) as GameObject;

			spellDescriptionEquip.SetActive(false);


			if(TutorialCampaignObj!=null && TutorialCampaignObj.isTutorial==true && TutorialCampaignObj.EnemyStatsInheriterObj.CurrentLevel==1){

				TutorialCampaignObj.BackFromSpellUnlock();

			}


//			print ("this spell should now be leanered " + spellButtonHolder.spellPlayerPref);

			/*
			Analytics.CustomEvent("SpellUnlocked", new Dictionary<string, object>
			                      {
				{spellButtonHolder.spellPlayerPref , true }
			});
			*/
		//shows me which spells ppl are unlocking the most
//			Analytics.CustomEvent(spellButtonHolder.spellPlayerPref, new Dictionary<string, object>
//			                      {
//				{spellButtonHolder.spellPlayerPref, true }
//				
//			});
		
		}

	}
		
	public void openSpellList(){


		if (TutorialCampaignObj != null && TutorialCampaignObj.TutorialFinger.GetComponent<Animator> ().GetBool ("NextLevelBool") == true) {

			TutorialCampaignObj.TutorialFinger.GetComponent<Animator> ().SetBool ("NextLevelBool",false);
		}


		LevelWindow.SetActive (false);
		unlockLevelPopUp.SetActive (false);
		inventoryWindow.SetActive (false);
		itemDescriptionWindow.SetActive (false);
		itemDescriptionEquip.SetActive (false);
		spellDescriptionWindow.SetActive (false);
		spellDescriptionEquip.SetActive (false);
		statsWindow.SetActive (false);
		potionsPage.SetActive (false);
		settingsPage.SetActive (false);


		if (spellWindow.activeSelf == true) {

			Canvas2.SetActive(true);

			spellWindow.SetActive (false);

		} else {


			spellWindow.SetActive (true);
			spellWindow.GetComponent<ScrollRect>().enabled=true;
			spellWindow.GetComponent<RawImage>().enabled=true;

			Canvas2.SetActive(false);
		}

		persistantSoundManager.GetComponent<SoundManager> ().openSpellBook ();

	}
	public void openStats(){

		backFromItemDescription ();


		if (statsWindow.activeSelf == true) {
		//	Canvas2.SetActive(true);
			settingsPage.SetActive (false);

			statsWindow.SetActive (false);
		} else {

			statsWindow.SetActive (true);
			playerObj.lvUpRaysGfx.SetActive (true);
			playerObj.lvUpCountdown.SetActive (false);
			playerObj.lvUpCountdown.GetComponentInParent<LevelUpCountdwn>().StopCoroutine ("SellCountdown");

			playerObj.lvUpButton.SetActive (false);
		//	Canvas2.SetActive(false);

			
		}

		windowOpen = true;

		persistantSoundManager.GetComponent<SoundManager> ().openStats ();

		if (PlayerPrefs.GetInt ("LevelUpTutorial") == 0) {

	//		TutorialController tutObj = Camera.main.GetComponent<MainMenu> ().TutorialObject;

			persistantSoundManager.GetComponent<SoundManager> ().oldManHappy ();


			TutorialObject.wizardSprite.sprite = TutorialObject.wizardSprites [1];
			TutorialObject.twoPartTutorial = true;
			TutorialObject.levelUpTutorial = true; 
			TutorialObject.explainStats = true; //this is for when we need the okay button (on stats) to check if its ok to procede to next part of the tutorial. See StatsPoints script...

			TutorialObject.TutPartOne ("Amazing! You have reached a new level!");
			TutorialObject.secondThingToSay = "Here you can upgrade your <color=#dd480a>Attack</color>, <color=#ffc119>Health</color>, and <color=#20fff2>Magic</color> powers!";

			PlayerPrefs.SetInt ("LevelUpTutorial", 1);
		}

	}

	public void openDungeons(){

		if (LevelWindow.activeSelf == true) {

			closeAllWindows (null);

			windowOpen = false;

			//Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound


		} else {
			
			closeAllWindows (LevelWindow);


			MenuButtons [0].toggleSprites ();

			equipmentBackButton.SetActive (true);

			LevelWindow.SetActive (true);

			if (PlayerPrefs.GetInt ("WindowsOpenPausing") == 0) {
				windowOpen = true;
				menuPausedScreen.SetActive (true);
			} else {
				windowOpen = false;

			}

			if (TutorialObject.unlockLevelTut == true) { //the player pref is in the first step of this tutorial...

				TutorialObject.disableButtons (TutorialObject.buttonList [12]);
				TutorialObject.worldMenuScrollRect.enabled = false; //so the player cant scroll around...
				TutorialObject.TutPartTwo (); //plays the 2nd phrase... "tap on world 2 to unlock, tap again to go there etc.."

			}
		}
	}

	public void openItemDescription(){
			
		persistantSoundManager.GetComponent<SoundManager> ().openBag ();
		FacebookItemSparkleInventory.SetActive(false);

		
		if (TutorialCampaignObj!=null && PlayerPrefs.GetInt (buttonHolder.GetComponent<Image> ().sprite.name.ToString ()) == 2 && TutorialCampaignObj.ItemExplainEquipBool==true) { 
		//this shows a newly unlocked item in the campain tutorl
			TutorialCampaignObj.EquipNewItem();

			equipmentBackButton.SetActive(false);

		} 

		if (TutorialCampaignObj!=null && TutorialCampaignObj.explainItemBuyBool==true) {
			
			TutorialCampaignObj.explainItemBuyBool=false; //this makes it so the buy item dialogue pane is removed.
			TutorialCampaignObj.ReadyToEquipItemBool = false;
			TutorialCampaignObj.QuestDisplay.SetActive(false);

		}

		if (itemDescriptionWindow.activeSelf == false) {
			itemDescriptionWindow.SetActive(true);
			itemDescriptionEquip.SetActive(true);

			//checks if item is owned
			if(buttonHolder.owned==true){
			
				PlayerPrefs.SetInt (buttonHolder.GetComponent<Image> ().sprite.name.ToString (), 1);
				if(buttonHolder.NewSparklingItemHolder!=null){ //makes it stop the new item sparkle
				buttonHolder.NewSparklingItemHolder.SetActive(false);
				}

				itemPurchaseButton.SetActive(false); //not purchasable if owned
			}
			else if (buttonHolder.owned==false){

				itemPurchaseButton.SetActive(true);
				itemDescriptionEquip.SetActive(false);
			}

	

			if (buttonHolder.head == true && PlayerPrefs.GetInt("head")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}
			if (buttonHolder.arms == true && PlayerPrefs.GetInt("arms")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}
			if (buttonHolder.chest == true && PlayerPrefs.GetInt("chest")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}
			if (buttonHolder.legs == true && PlayerPrefs.GetInt("legs")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}
			if (buttonHolder.cape == true && PlayerPrefs.GetInt("cape")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}
			if (buttonHolder.sword == true && PlayerPrefs.GetInt("Sword")==buttonHolder.gearLevel) {
				itemDescriptionEquip.SetActive(false);
			}

				if(buttonHolder.isFacebookItem==true){
					
				FacebookItemSparkleInventory.SetActive(true);

				//	itemPurchaseButton.SetActive(false);
				//	itemDescriptionEquip.SetActive(false);
					//check if they are logged in
//					if(FB.IsLoggedIn==true){
//						//show the sharebutton
//						facebookLoginButton.SetActive(false);
//						facebookShareButton.SetActive(true);
//					}else{
//						facebookLoginButton.SetActive(true);
//						//show the facebook login button
//					}
					
					if(PlayerPrefs.GetInt("FacebookShare")==1){

					facebookLoginButton.SetActive(false);
					facebookShareButton.SetActive(false);

					if(buttonHolder.owned==true){
							itemDescriptionEquip.SetActive(true); //if they dont own it they can buy it
							
						}

					}else{

						itemPurchaseButton.SetActive(false);
						itemDescriptionEquip.SetActive(false);

				}
					//	buttonHolder.
					
					//check if they have shared the game yet...
					
				}else{
					facebookShareButton.SetActive(false);
					facebookLoginButton.SetActive(false);
					
			}

		} 

	}

	public void backFromItemDescription (){

		equipmentBackButton.SetActive (false); //unless it needs to be enabled do so in the if statement



		if (MainCharacterPage.activeSelf == true && inventoryWindow.activeSelf==true) {
		//	inventoryWindow.SetActive (false);
			MainCharacterPage.SetActive(false);
		//	MainCharacterPage.GetComponentInChildren<ItemSlots> ().attributeViewer.SetActive (false);

			inventoryWindow.SetActive (false);
			windowOpen = false;

		}

		if (shopWindow.activeSelf == true) {
			//	inventoryWindow.SetActive (false);
			shopWindow.SetActive(false);
			windowOpen = false;

		}

        if (eventWindow.activeSelf == true)
        {
            //	inventoryWindow.SetActive (false);
            eventWindow.SetActive(false);
            windowOpen = false;

        }

        if(quitGamePrompt.activeSelf == true)
        {
            quitGamePrompt.SetActive(false);
            windowOpen = false;

        }

        if (UpgradeWeaponPage.activeSelf == true) {
			UpgradeWeaponPage.SetActive (false);
	//		MainCharacterPage.SetActive (true);
	//		equipmentBackButton.SetActive (false); //unless it needs to be enabled do so in the if statement
			windowOpen = false;

		}

		if (LevelWindow.activeSelf == true) {
			LevelWindow.SetActive (false);
			unlockLevelPopUp.SetActive (false);
	//		Canvas2.SetActive(true);
			windowOpen = false;

		}

        if (spellWindow.activeSelf == true) {
			spellWindow.SetActive (false);
		//	MainCharacterPage.SetActive (true);
	//		equipmentBackButton.SetActive (false);
		//	Canvas2.SetActive(true);
			windowOpen = false;

		}

		if (statsWindow.activeSelf == true) {

			statsWindow.SetActive (false);
			playerObj.lvUpRaysGfx.SetActive (false);
			windowOpen = false;


		}

		if (talentWindow.activeSelf == true) {
			talentWindow.SetActive (false);
		//	MainCharacterPage.SetActive (true);
		//	equipmentBackButton.SetActive (true);
		//	Canvas2.SetActive(true);
			windowOpen = false;


		}
			
		if (potionsPage.activeSelf == true) {
			potionsPage.SetActive (false);
			//	MainCharacterPage.SetActive (true);
			//	equipmentBackButton.SetActive (true);
			//	Canvas2.SetActive(true);
			windowOpen = false;
	//		equipmentBackButton.SetActive (false); //unless it needs to be enabled do so in the if statement


		}

		if (settingsPage.activeSelf == true) {

			settingsPage.SetActive (false);
			windowOpen = false;
		}

		if (prestigeWindow.activeSelf == true) {

			prestigeWindow.SetActive (false);
			windowOpen = false;

		}

		//if (petsWindow.activeSelf == true) {

		//	petsWindow.SetActive (false);
		//	windowOpen = false;

		//}

		if (IdleTimeAway.activeSelf == true) {

			IdleTimeAway.SetActive (false);
			equipmentBackButton.SetActive (false);

			//give gold and exp here (this is for showing the exp text)
			expAmount(IdleTimeAway.GetComponent<IdleTimeAway>().expCounter);

            double expTotal = playerObj.playerExpCurrent + IdleTimeAway.GetComponent<IdleTimeAway>().expCounter;

            if (checkStoreReview() == true)
            {
                if (expTotal >= playerObj.playerExpMax) //this checks if the player will elvel up
                {
                    playerObj.playerExpNew = playerObj.playerExpMax - 10; //so the player wont lv up
                }
                else
                {
                    playerObj.playerExpNew += IdleTimeAway.GetComponent<IdleTimeAway>().expCounter;

                }

                ReviewGameWindow.SetActive(true);//show the review prompt for the game
                windowOpen = true;


            }
            else 
            {
                playerObj.playerExpNew += IdleTimeAway.GetComponent<IdleTimeAway>().expCounter;
                windowOpen = false;

            }


            playerObj.playerGold += IdleTimeAway.GetComponent<IdleTimeAway> ().goldCounter;
			if (IdleTimeAway.GetComponent<IdleTimeAway> ().dailyLoginBonusCounter > 0) {
				playerObj.playerGold += IdleTimeAway.GetComponent<IdleTimeAway> ().dailyLoginBonusCounter;
			}

		}
			
		closeAllWindows (null); //this is so it resets all the toggles

		if (TutorialObject.wpnMenuTap == true) {

			TutorialObject.refreshAllVariables ();
		}

        checkIfEggAlive(); //this checks if an egg is there, if it is, it will leave the window open so the player cannot attack it anymore

     


        persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound

	}

    public bool checkStoreReview()
    {

        if(PlayerPrefs.GetInt("StoreReview")==0 
            && PlayerPrefs.GetInt("CurrentLevel") >=7
            && PlayerPrefs.GetInt("StartupSinceReview")>=2) //was 5
            //checks if they havent reviewed the game, if the lv>10, if they have prestiged, 
            //and if its been 4 startups since we last asked them to review
        {
            return true;

        }

        else

        {
            PlayerPrefs.SetInt("StartupSinceReview", PlayerPrefs.GetInt("StartupSinceReview") + 1);

            //print("the amt of startups " + PlayerPrefs.GetInt("StartupSinceReview").ToString());
            return false;
        }

    }

    public void checkIfEggAlive()
    {

        if (EnemyObj != null && EnemyObj.gameObject.name == "Egg" || EnemyObj != null && EnemyObj.gameObject.name == "EventChest")
        {
          //  print("egg detected!");
            if (campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>().playerDead == true)
            {
                windowOpen = true; //if the egg has been broken, we do not want the player to be able to atk or cast spells

            }



        }

    }

	public void OpenInventory(){ //this is opened with the maincharacter screen now

	//	backFromItemDescription ();

		inventoryWindow.SetActive (true);

	//	equipmentBackButton.SetActive (true);
		windowOpen=true;


	}

	//public void OpenPetsToggle(){ //this is opened with the maincharacter screen now

	//	//	backFromItemDescription ();
	//	persistantSoundManager.GetComponent<SoundManager> ().openBag ();//this is already taken care of for the menu icons in SpriteToggle.Cs

	//	petsWindow.SetActive (true);

	//	//	equipmentBackButton.SetActive (true);
	//	windowOpen=true;


	//}

	public void OpenItemChest(){ //the gameobject it sends is so the chestholder


        //backFromItemDescription ();

        if (PlayerPrefs.GetInt("WindowsOpenPausing") == 0)
        {
            backFromItemDescription(); //this will close all other windows

            windowOpen = true;
        }
        else
        {
            //this doesnt close all other windows

            windowOpen = true;

        }

        newItemWindow.SetActive (true); //opens the item gen window (will have opening animation eventually)
		inventoryWindow.GetComponentInChildren<Inventory> ().GenerateNewItem (); //generates a new item


		//windowOpen=true;
	}

	public void OpenPrestigeWindow(){ //the gameobject it sends is so the chestholder

	//	if (PlayerPrefs.GetInt ("PrestigePtsEarned") >= 2) {

		persistantSoundManager.GetComponent<SoundManager> ().openBag ();//this is already taken care of for the menu icons in SpriteToggle.Cs

		closeAllWindows (null);


		if (TutorialObject.prestigeTutorial == true) {

			TutorialObject.refreshAllVariables ();//resets everything so we can restart the tutorial. Tutpart one will enable the obj
	
		}
			

			int nxtPrestLv = ((PlayerPrefs.GetInt ("PrestigeLv") + 1));

			prestigeWindow.GetComponent<PrestigeHolder> ().amtPoints.text = PlayerPrefs.GetInt ("PrestigePtsEarned").ToString ();

			//for normal Ascension
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [0].text = (nxtPrestLv * 5).ToString (); //start on world
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [1].text = (nxtPrestLv * 5).ToString (); //player Lv
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [2].text = (PlayerPrefs.GetInt ("StatsAdd")+PlayerPrefs.GetInt ("PrestigePtsEarned")).ToString (); //% gain/lv
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [3].text = (nxtPrestLv * 5 * 3).ToString (); //Stats points Points
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [4].text = (nxtPrestLv * 5).ToString (); //Talent Points..

            //loading the data for the prestige gold
            enemyStatsObj.loadPrestigeData(nxtPrestLv * 5);
            //

			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [5].text = LargeNumConverter.setDmgShort ((double)enemyStatsObj.prestigeData[1] ["LVCOST"] * .9f); //Starting Gold
			prestigeWindow.GetComponent<PrestigeHolder> ().AscendList [6].text = PlayerPrefs.GetInt ("PrestigePtsEarned").ToString (); //Prestige Points to Spend

			//for 2x premium Ascension
			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [0].text = ((nxtPrestLv+1) * 5).ToString (); //start on world
			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [1].text = ((nxtPrestLv+1) * 5).ToString (); //player Lv
			//this needs to be X2 (below)
			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [2].text = (PlayerPrefs.GetInt ("StatsAdd")+PlayerPrefs.GetInt ("PrestigePtsEarned")*2).ToString (); //% gain/lv

			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [3].text = ((nxtPrestLv+1) * 5 * 3).ToString (); //Stats points Points
			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [4].text = ((nxtPrestLv+1) * 5).ToString (); //Talent Points

			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [5].text = LargeNumConverter.setDmgShort ((double)enemyStatsObj.prestigeData[6] ["LVCOST"] * .9f); //Starting Gold
			prestigeWindow.GetComponent<PrestigeHolder> ().PremiumAscendList [6].text = (PlayerPrefs.GetInt ("PrestigePtsEarned") * 2).ToString (); //Prestige Points to Spend

			prestigeWindow.SetActive (true); //opens the item gen window (will have opening animation eventually)

			windowOpen = true;

//		} else { //if they dont have greater than 2 pst points
//
//		showErrorMsg ("You need 2 Ascend Points to unlock Ascend");
//
//	}

	}

	public void areYouSurePrestigeWindow(){ //the gameobject it sends is so the chestholder

		//chestHolder.GetComponent<EnemyRagdoll>().shadowObj.StartCoroutine ("fadeShadow");

		areYouSurePrestige.SetActive (true); //opens the item gen window (will have opening animation eventually)

		windowOpen=true;
	}

	public void setPrestige(){

        //this is standard prestige***

		//-prestige animation?


			int tempPrestigeLevel = PlayerPrefs.GetInt ("PrestigeLv"); //need to store the prestige level before it gets erased

			int tempPrestigePts = PlayerPrefs.GetInt ("PrestigePts"); //this is the prestigepoints to spend

			int tempPrestigePtsEarned = PlayerPrefs.GetInt ("PrestigePtsEarned"); //this is the prestigepoints to spend

		//	PlayerPrefs.DeleteKey ("PrestigePts"); //this is so the player already doesnt have prestige poiints earned when they start over (071718 not deletin this now so unspent points arent erased)

			PlayerPrefs.DeleteKey ("PrestigePtsEarned"); //this is so the player already doesnt have prestige poiints earned when they start over
            PlayerPrefs.DeleteKey("PrestigeCounter"); //this is so slider bar pts arent transfterred over to the next prestige



        //for deleting items, lets try not deleting them this time
   //         for (int i = 0; i < InventoryScript.InventoryList.Count; i++) {

			//	InventoryScript.InventoryList [i].clearMods ();
			//}

			//clear wpn data and wpn levels

			for (int j = 0; j < masterWeaponListScript.unlockedWpnArray.Length; j++) {
				if (j > 1) {
					masterWeaponListScript.unlockedWpnArray [j] = 0;
				} else {
					masterWeaponListScript.unlockedWpnArray [j] = 1;
				}
			}

			//clear the wpn levels?
			for (int k = 0; k < masterWeaponListScript.unlockedWpnArray.Length; k++) {

				PlayerPrefs.DeleteKey ("Wpn" + (k).ToString ()); //gets the item of the wpn

			}


			masterWeaponListScript.writeWpnArray (); //saves the reset array

			PlayerPrefs.DeleteKey ("setCharacterIconBkg"); //deletes the character button bkg color

			//clear spell data

			for (int j = 0; j < masterSpellListScript.unlockedSplArray.Length; j++) {
				if (j > 1) {
					masterSpellListScript.unlockedSplArray [j] = 0;
				} else {
					masterSpellListScript.unlockedSplArray [j] = 1;
				}
			}

			//clear the spl levels?
			for (int k = 0; k < masterSpellListScript.unlockedSplArray.Length; k++) {

				PlayerPrefs.DeleteKey ("Spl" + (k).ToString ()); //gets the item of the wpn

			}


			masterSpellListScript.writeSplArray (); //saves the reset array

			//clear player stats

			PlayerPrefs.SetFloat ("atkMod", 0);

			PlayerPrefs.SetFloat ("HealthMod", 0);

			PlayerPrefs.SetFloat ("luckMod", 0);


			//clear research tree
			for (int l = 0; l < 30; l++) { //30 is the amount of talents upgraded, but not up to prestige talents which start at 30

				PlayerPrefs.DeleteKey (ResearchList [l].gameObject.name + "isResearched"); //1 is research has began, 2 is finished - i changed it to 2 to remove research time

				PlayerPrefs.DeleteKey (ResearchList [l].gameObject.name + "pointsInvested");

			}

			//clear lev progress

			//see below

			//

			//clear player attributes

			PlayerPrefs.DeleteKey ("Exp"); //deletes okayers current exp, the exp max is determined through the player level which is defined later in this method
			PlayerPrefs.DeleteKey ("playerAtkDmg");
			PlayerPrefs.DeleteKey ("playerSplDmg");
			//PlayerPrefs.DeleteKey ("TempHPBoost"); //this is from the shop.

			PlayerPrefs.SetInt ("PrestigeLv", tempPrestigeLevel + 1);

            for (int i = 0; i < abilityMaster.equippedAbilities.Length; i++)
            {
                abilityMaster.equippedAbilities[i] = -1;

            }

            abilityMaster.saveAbilitiesEquipped();

            AnalyticsEvent.Custom ("Prestige", new Dictionary<string, object> {

				{"PrestigeLv",PlayerPrefs.GetInt("PrestigeLv")}
			});


			PlayerPrefs.SetInt ("PrestigePts", PlayerPrefs.GetInt("PrestigePts")+ tempPrestigePtsEarned); //These are now Prestige Points you can spend

			int tempLevToLoad = PlayerPrefs.GetInt ("PrestigeLv") * 5; ///declare this int so the player can load from an earlier lv 1=5, 2=10, 3=15, 4=20 etc.

			int statsPointsToGive = tempLevToLoad * 3; //player will start with stats points, 3 per level they are on assuming they lv'd once per level

			int talentPointsToGive = tempLevToLoad; //player will start with talent points, 1 per level they are on assuming they lv'd once per level



			PlayerPrefs.SetInt ("Rank", tempLevToLoad); //so the player is rank 5 with the correct EXP MAX

			PlayerPrefs.SetFloat ("SkillPoints", statsPointsToGive); //play pref for stats

			PlayerPrefs.SetInt ("ResearchPoints", tempLevToLoad); //play pref for talents

			//will have to go through all the previous levels and mark them as completed in the array

			//LevelCompleteObject.unlockedLevelArray.

			for (int i = 0; i < LevelCompleteObject.unlockedLevelArray.Length; i++) { //this clears the array

				if (i <= tempLevToLoad) {
			
					LevelCompleteObject.unlockedLevelArray [i] = 1; //the level is unlocked

					if (i > 0) {
						PlayerPrefs.SetInt ("Boss" + (i - 1).ToString (), 1);
					}

				} else {
					LevelCompleteObject.unlockedLevelArray [i] = 0; //the level is not unlocked
					PlayerPrefs.SetInt ("Boss" + (i - 1).ToString (), 0); //boss has not been defeated!

				}

			}

			PlayerPrefsX.SetIntArray ("LevelsUnlocked", LevelCompleteObject.unlockedLevelArray);//sets the unlocked levels as a mod

			double goldToGive = (double)enemyStatsObj.prestigeData[1]["LVCOST"] * .9f; //gold the player will start with

			PlayerPrefs.SetString ("Gold", goldToGive.ToString ()); //this converts the Double value to a string so we can store larger numbers

			//for the next statspoints/lv
			PlayerPrefs.SetInt ("StatsAdd",PlayerPrefs.GetInt("StatsAdd")+ tempPrestigePtsEarned);

            Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false; //So the game doesnt think we are going into the bonus dungeon
            Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = false; //so a dungeon is loaded with the next lv. 


        //this code is for scaling the dungeons based on if you prestiged recently
        int[] DungeonIntArray =  PlayerPrefsX.GetIntArray("DungeonArray");

                if (DungeonIntArray.Length > 0)//in case a dungeon was never created and the player prestiged on
                {
                DungeonIntArray[3] = tempLevToLoad;
                PlayerPrefsX.SetIntArray("DungeonArray", DungeonIntArray); //refreshes the dungeon to make it start at the current players level if they have prestiged. Prevents a gold bug
                }

            int[] BonusDungeonIntArray = PlayerPrefsX.GetIntArray("BonusDungeonArray");

                if (BonusDungeonIntArray.Length > 0)//in case a dungeon was never created and the player prestiged on
                {
                BonusDungeonIntArray[3] = tempLevToLoad + 2;
                PlayerPrefsX.SetIntArray("BonusDungeonArray", BonusDungeonIntArray); //refreshes the dungeon to make it start at the current players level if they have prestiged. Prevents a gold bug
                }

        //this code is so the event levels rescale after prestige
                for (int i = 0; i < LevelCompleteObject.eventLevels.Length; i++)
                {
                    LevelCompleteObject.eventLevels[i] = tempLevToLoad + i;
                }

                PlayerPrefsX.SetIntArray("EventLevels", LevelCompleteObject.eventLevels);


                loadLevel(tempLevToLoad); //loads the level for the player to skip now that they have prestiged

	}

	public void setPremiumPrestige(){


		//-prestige animation?
	
		int tempPrestigeLevel = PlayerPrefs.GetInt ("PrestigeLv"); //need to store the prestige level before it gets erased

		int tempPrestigePts = PlayerPrefs.GetInt ("PrestigePts"); //this is the prestigepoints to spend

		int tempPrestigePtsEarned = 2*PlayerPrefs.GetInt ("PrestigePtsEarned"); //this is the prestigepoints to spend

		//PlayerPrefs.DeleteKey ("PrestigePts"); //this is so the player already doesnt have prestige poiints earned when they start over

		PlayerPrefs.DeleteKey ("PrestigePtsEarned"); //this is so the player already doesnt have prestige poiints earned when they start over
        PlayerPrefs.DeleteKey("PrestigeCounter"); //this is so slider bar pts arent transfterred over to the next prestige

        //lets try not deleting itmes hehe
  //      for (int i = 0; i < InventoryScript.InventoryList.Count; i++) {

		//	InventoryScript.InventoryList [i].clearMods ();
		//}

		//clear wpn data and wpn levels

		for (int j = 0; j < masterWeaponListScript.unlockedWpnArray.Length; j++) {
			if (j > 1) {
				masterWeaponListScript.unlockedWpnArray [j] = 0;
			} else {
				masterWeaponListScript.unlockedWpnArray [j] = 1;
			}
		}

		//clear the wpn levels?
		for (int k = 0; k < masterWeaponListScript.unlockedWpnArray.Length; k++) {

			PlayerPrefs.DeleteKey ("Wpn" + (k).ToString ()); //gets the item of the wpn

		}


		masterWeaponListScript.writeWpnArray (); //saves the reset array

		PlayerPrefs.DeleteKey ("setCharacterIconBkg"); //deletes the character button bkg color

		//clear spell data

		for (int j = 0; j < masterSpellListScript.unlockedSplArray.Length; j++) {
			if (j > 1) {
				masterSpellListScript.unlockedSplArray [j] = 0;
			} else {
				masterSpellListScript.unlockedSplArray [j] = 1;
			}
		}

		//clear the spl levels?
		for (int k = 0; k < masterSpellListScript.unlockedSplArray.Length; k++) {

			PlayerPrefs.DeleteKey ("Spl" + (k).ToString ()); //gets the item of the wpn

		}


		masterSpellListScript.writeSplArray (); //saves the reset array

		//clear player stats

		PlayerPrefs.SetFloat ("atkMod", 0);

		PlayerPrefs.SetFloat ("HealthMod", 0);

		PlayerPrefs.SetFloat ("luckMod", 0);


		//clear research tree
		for (int l = 0; l < 30; l++) { //30 is the amount of talents upgraded, but not up to prestige talents which start at 30

			PlayerPrefs.DeleteKey (ResearchList [l].gameObject.name + "isResearched"); //1 is research has began, 2 is finished - i changed it to 2 to remove research time

			PlayerPrefs.DeleteKey (ResearchList [l].gameObject.name + "pointsInvested");

		}

		//clear lev progress

		//see below

		//

		//clear player attributes

		PlayerPrefs.DeleteKey ("Exp"); //deletes okayers current exp, the exp max is determined through the player level which is defined later in this method
		PlayerPrefs.DeleteKey ("playerAtkDmg");
		PlayerPrefs.DeleteKey ("playerSplDmg");
		//PlayerPrefs.DeleteKey ("TempHPBoost"); //this is from the shop.

		PlayerPrefs.SetInt ("PrestigeLv", tempPrestigeLevel + 2); //adds two levels instead of one because you are going ahead 2 prestige levels

        for (int i = 0; i < abilityMaster.equippedAbilities.Length; i++)
        {
            abilityMaster.equippedAbilities[i] = -1;

        }

        abilityMaster.saveAbilitiesEquipped();


        AnalyticsEvent.Custom ("Prestige", new Dictionary<string, object> {

			{"PremiumPrestige",1},
			{"PrestigeLv",PlayerPrefs.GetInt("PrestigeLv")}

		});



		PlayerPrefs.SetInt ("PrestigePts", PlayerPrefs.GetInt("PrestigePts") + tempPrestigePtsEarned); //These are now Prestige Points you can spend

		int tempLevToLoad = PlayerPrefs.GetInt ("PrestigeLv") * 5; ///declare this int so the player can load from an earlier lv 1=5, 2=10, 3=15, 4=20 etc.

		int statsPointsToGive = tempLevToLoad * 3; //player will start with stats points, 3 per level they are on assuming they lv'd once per level

		int talentPointsToGive = tempLevToLoad; //player will start with talent points, 1 per level they are on assuming they lv'd once per level



		PlayerPrefs.SetInt ("Rank", tempLevToLoad); //so the player is rank 5 with the correct EXP MAX

		PlayerPrefs.SetFloat ("SkillPoints", statsPointsToGive); //play pref for stats

		PlayerPrefs.SetInt ("ResearchPoints", tempLevToLoad); //play pref for talents

		//will have to go through all the previous levels and mark them as completed in the array

		//LevelCompleteObject.unlockedLevelArray.

		for (int i = 0; i < LevelCompleteObject.unlockedLevelArray.Length; i++) { //this clears the array

			if (i <= tempLevToLoad) {

				LevelCompleteObject.unlockedLevelArray [i] = 1; //the level is unlocked

				if (i > 0) {
					PlayerPrefs.SetInt ("Boss" + (i - 1).ToString (), 1);
				}

			} else {
				LevelCompleteObject.unlockedLevelArray [i] = 0; //the level is not unlocked
				PlayerPrefs.SetInt ("Boss" + (i - 1).ToString (), 0); //boss has not been defeated!

			}

		}

		PlayerPrefsX.SetIntArray ("LevelsUnlocked", LevelCompleteObject.unlockedLevelArray);//sets the unlocked levels as a mod

		double goldToGive = (double)enemyStatsObj.prestigeData[6]["LVCOST"] * .9f; //gold the player will start with

		PlayerPrefs.SetString ("Gold", goldToGive.ToString ()); //this converts the Double value to a string so we can store larger numbers

        //for the next statspoints/lv
   //     print("test prestige points earned is :" + tempPrestigePtsEarned.ToString());
   //     print("stats add is  :" + PlayerPrefs.GetInt("StatsAdd").ToString());



        PlayerPrefs.SetInt("StatsAdd", (PlayerPrefs.GetInt("StatsAdd") + tempPrestigePtsEarned));

        Camera.main.GetComponent<MainMenu>().enemyStatsObj.eventMode = false;
        Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonModeBonus = false; //So the game doesnt think we are going into the bonus dungeon
        Camera.main.GetComponent<MainMenu>().enemyStatsObj.dungeonMode = false; //so a dungeon is loaded with the next lv. 


        //this code is for scaling the dungeons based on if you prestiged recently
        int[] DungeonIntArray = PlayerPrefsX.GetIntArray("DungeonArray");

            if (DungeonIntArray.Length > 0)//in case a dungeon was never created and the player prestiged on
            {
                DungeonIntArray[3] = tempLevToLoad;
                PlayerPrefsX.SetIntArray("DungeonArray", DungeonIntArray); //refreshes the dungeon to make it start at the current players level if they have prestiged. Prevents a gold bug
            }

        int[] BonusDungeonIntArray = PlayerPrefsX.GetIntArray("BonusDungeonArray");

            if (BonusDungeonIntArray.Length > 0)//in case a dungeon was never created and the player prestiged on
            {
                BonusDungeonIntArray[3] = tempLevToLoad + 2;
                PlayerPrefsX.SetIntArray("BonusDungeonArray", BonusDungeonIntArray); //refreshes the dungeon to make it start at the current players level if they have prestiged. Prevents a gold bug
            }

        //this code is so the event levels rescale after prestige
        for (int i = 0; i < LevelCompleteObject.eventLevels.Length; i++)
        {
            LevelCompleteObject.eventLevels[i] = tempLevToLoad + i;
        }

        PlayerPrefsX.SetIntArray("EventLevels", LevelCompleteObject.eventLevels);

        loadLevel(tempLevToLoad); //loads the level for the player to skip now that they have prestiged

	}


    public void loadAnyLevel( int lvNum)
    {

        for (int i = 0; i < LevelCompleteObject.unlockedLevelArray.Length; i++)
        { //this clears the array

            if (i <= lvNum)
            {

                LevelCompleteObject.unlockedLevelArray[i] = 1; //the level is unlocked

                if (i > 0)
                {
                    PlayerPrefs.SetInt("Boss" + (i - 1).ToString(), 1);
                }

            }
            else
            {
                LevelCompleteObject.unlockedLevelArray[i] = 0; //the level is not unlocked
                PlayerPrefs.SetInt("Boss" + (i - 1).ToString(), 0); //boss has not been defeated!

            }

        }

        PlayerPrefsX.SetIntArray("LevelsUnlocked", LevelCompleteObject.unlockedLevelArray);//sets the unlocked levels as a mod

       // PlayerPrefs.SetInt("Rank", lvNum); //sets players rank as the new rank in player prefs


        //        double goldToGive = (double)enemyStatsObj.data[lvNum - 1]["GOLD"] * 20; //gold the player will start with

        //      PlayerPrefs.SetString("Gold", goldToGive.ToString()); //this converts the Double value to a string so we can store larger numbers

        loadLevel(lvNum); //loads the level for the player to skip now that they have prestiged
    }



	public int getPrestigeStats(int PrestigePts){

		int Stats = 0;

		switch (PrestigePts) {

		case 0:
			//Player will never be at prestige lv0
			break;
		case 1:

			Stats = 4;

			break;
		case 2:

			Stats = 7;


			break;
		case 3:

			Stats = 11;


			break;
		case 4:

			Stats = 14;


			break;
		}

		return Stats;
	}

	public void goldHax(){

		playerObj.playerGold += playerObj.playerGold*2; //this value will eventually change based on the level it has been researched

	}

	public void expHax(){

		playerObj.playerExpNew += playerObj.playerExpNew*2; //this value will eventually change based on the level it has been researched

	}

	public void prestHax(){

		prestPointsAmt.updatePrestigeCounter ();
	}

	public void CloseItemChest(){

		chestHolder.gameObject.SetActive (false);//this deactivates the chest

		//chestHolder.GetComponent<EnemyRagdoll>().shadowObj.StartCoroutine ("fadeShadow");

		newItemWindow.SetActive (false); //opens the item gen window (will have opening animation eventually)
		chestHolder.gameObject.SetActive(false);
		campaignLevelEnemySpawner.setMoveTime (0);
		windowOpen = false;
		campaignLevelEnemySpawner.chestTimer = 70;

	//continue to spawn new unit?
	
	}

	public void closeWindows(){
	//	pressCounter+=1; //this is so the windows wont start closed because press counter >1
		spellWindow.SetActive (false);
		LevelWindow.SetActive (false);
		unlockLevelPopUp.SetActive (false);
		inventoryWindow.SetActive (false);
		itemDescriptionWindow.SetActive (false);
		itemDescriptionEquip.SetActive (false);
		spellDescriptionWindow.SetActive (false);
		spellDescriptionEquip.SetActive (false);
		statsWindow.SetActive (false);
		potionsPage.SetActive (false);
		settingsPage.SetActive (false);

		windowOpen = false;


	}




	public void expAmount(double expAmount){

		expAmt.GetComponentInChildren<TextMeshPro>().text = "+ " + LargeNumConverter.setDmg(expAmount).ToString () + " EXP";

	//	GameObject EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
	//	GameObject TorsoObj = EnemyObj.GetComponentInChildren<EnemyRagdoll> ().Torso;

		expAmt.transform.position = new Vector3 (0 ,0.5f,90);

		expAmt.SetActive (true);

		expAmt.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 2);


	}
		
	public void dmgPlayerAmount(double expAmount){

		pDmgAmt.text = "- " + LargeNumConverter.setDmg(expAmount).ToString () + " HP";
		pDmgAmt.gameObject.SetActive (true);
		playerObj.quickHPUpdate (); //because there was a delay

	}

	public void showErrorMsg(string msg){

		errorMsgTxt.text = msg;
		errorMsgGo.SetActive (true);

	}

    public void petPhysicalAttack(float dmgModifier)
    {

        //		print ("button clicked");
        if (windowOpen == false)
        {
            //for atk color : F20000FF
            //for spell color: 0056FFFF

            Color myDmgColor = new Color();
            ColorUtility.TryParseHtmlString("#F20000FF", out myDmgColor);

                EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>();
                EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
                enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso; //this makes a new obj ever click...
                //print("new enemy instance ID found!");

            //players dmg is a combination of item mods and stats increases

            double _PlayersDmg = CalcPlayerDmg() * (dmgModifier + petManager.abilityArray[28] + petManager.abilityArray[29]); //this method calcs the players dmg convieniently


            //adding in pet crit!
            if (UnityEngine.Random.Range(0, 100) <= petCritMod  && petCritUnlocked == true)
            { ////checking crit % and also if crit is enabbled

                _PlayersDmg = _PlayersDmg * 2f; //this can change

            }
            else
            {

                //no crit
            }

            //this is where the final damage is applied to the enemy
            if (EnemyControlObj.isHittable == true && EnemyControlObj.playerDead == false)
            {
                EnemyControlObj.playerHealthNew -= _PlayersDmg;
            }

            //change this for the pet dmg list

                for (int i = 0; i < petDmgAmtList.Count; i++)
                {

                    if (!petDmgAmtList[i].activeInHierarchy)
                    {

                    petDmgAmtList[i].GetComponentInChildren<TextMeshPro>().color = myDmgColor;

                    petDmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                    petDmgAmtList[i].transform.position = new Vector3(enemyHeadObj.transform.position.x - 0.4f, enemyHeadObj.transform.position.y, 90); //instantiates the beam offscre

                    petDmgAmtList[i].GetComponent<DroppedDmgScript>().dmgAmountText.text = LargeNumConverter.setDmg(_PlayersDmg);

                    //        //	dmgAmtList[i].GetComponent<DroppedDmgScript> ().setDmg (_PlayersDmg);

                    petDmgAmtList[i].SetActive(true);
                    //        //for dmg all over teh eplace
                    //        //dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
                    petDmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.5f);

                        break;
                    }

                }


             

            activateBlood(enemyHeadObj, EnemyControlObj);


            if (EnemyObj.gameObject.name != "Egg")//we dont want the egg to move
            {
                EnemyObj.transform.localPosition += new Vector3(UnityEngine.Random.Range(-5f, 5f) / 50f, 0, 0); //slightly moves the enemy for fx
            }

        }
    }

    public void petMagicAttack(float dmgModifier)
    {

        //		print ("button clicked");
        if (windowOpen == false)
        {
            //for atk color : F20000FF
            //for spell color: 0056FFFF

            Color myDmgColor = new Color();
            ColorUtility.TryParseHtmlString("#0056FFFF", out myDmgColor);

                EnemyControlObj = campaignLevelEnemySpawner.EnemySpawned.GetComponentInChildren<ControlEnemy>();
                EnemyObj = campaignLevelEnemySpawner.EnemySpawned;
                enemyHeadObj = EnemyObj.GetComponentInChildren<EnemyRagdoll>().Torso; //this makes a new obj ever click...
                                                                                      //print("new enemy instance ID found!");
            //players dmg is a combination of item mods and stats increases

            double _PlayersDmg = playerObj.spellDamageCalc() * (dmgModifier + petManager.abilityArray[28] + petManager.abilityArray[29]) ;
            //this method calcs the players dmg convieniently

            //adding in pet crit!
            if (UnityEngine.Random.Range(0, 100) <= petCritMod && petCritUnlocked == true)
            { ////checking crit % and also if crit is enabbled

                _PlayersDmg = _PlayersDmg * 2f; //this can change

            }
            else
            {

                //no crit
            }

            //this is where the final damage is applied to the enemy
            if (EnemyControlObj.isHittable == true && EnemyControlObj.playerDead==false)//this is needed so the pets dont kill the enemy before he arrives at the player
            {
                EnemyControlObj.playerHealthNew -= _PlayersDmg;
            }
            //change this for the pet dmg list

            for (int i = 0; i < petDmgAmtList.Count; i++)
            {

                if (!petDmgAmtList[i].activeInHierarchy)
                {
                    petDmgAmtList[i].GetComponentInChildren<TextMeshPro>().color = myDmgColor;

                    petDmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                    petDmgAmtList[i].transform.position = new Vector3(enemyHeadObj.transform.position.x - 0.4f, enemyHeadObj.transform.position.y, 90); //instantiates the beam offscre

                    petDmgAmtList[i].GetComponent<DroppedDmgScript>().dmgAmountText.text = LargeNumConverter.setDmg(_PlayersDmg);

                    //        //	dmgAmtList[i].GetComponent<DroppedDmgScript> ().setDmg (_PlayersDmg);

                    petDmgAmtList[i].SetActive(true);
                    //        //for dmg all over teh eplace
                    //        //dmgAmtList [i].GetComponent<Rigidbody2D> ().velocity = new Vector2 (UnityEngine.Random.Range (-40, 40) / 10f, UnityEngine.Random.Range (5, 7));
                    petDmgAmtList[i].GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.5f);

                    break;
                }

            }




            activateBlood(enemyHeadObj, EnemyControlObj);


            if (EnemyObj.gameObject.name != "Egg")//we dont want the egg to move
            {
                EnemyObj.transform.localPosition += new Vector3(UnityEngine.Random.Range(-5f, 5f) / 50f, 0, 0); //slightly moves the enemy for fx
            }

        }
    }


    public void fakeTimeCheat(){

		//print ("fake time cheat");
	}



}
