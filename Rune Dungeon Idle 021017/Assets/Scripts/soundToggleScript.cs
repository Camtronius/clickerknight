﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class soundToggleScript : MonoBehaviour {

	// Use this for initialization

	public GameObject PersistantSoundManager;

	public bool musicDisabler = false;

	public Sprite SoundOffSprite;
	public Sprite SoundOnSprite;


	void Start () {
	
		if (musicDisabler == false) {
			if (PlayerPrefs.GetInt ("SoundOnOff") == 0) {
			
				this.gameObject.GetComponent<Image> ().sprite = SoundOnSprite;

			
			} else if (PlayerPrefs.GetInt ("SoundOnOff") == 1) {
			
				this.gameObject.GetComponent<Image> ().sprite = SoundOffSprite;
			
			}
		} else {

			if (PlayerPrefs.GetInt ("MusicOnOff") == 0) {

				this.gameObject.GetComponent<Image> ().sprite = SoundOnSprite;


			} else if (PlayerPrefs.GetInt ("MusicOnOff") == 1) {

				this.gameObject.GetComponent<Image> ().sprite = SoundOffSprite;

			}



		}
	}

	public void OffSFx(){

		if (PersistantSoundManager == null) {
			PersistantSoundManager = GameObject.Find("PersistentSoundManager");
		}

		PersistantSoundManager.gameObject.GetComponent<SoundManager> ().TurnOffSound ();

		if (this.gameObject.GetComponent<Image> ().sprite == SoundOnSprite) {
			this.gameObject.GetComponent<Image>().sprite = SoundOffSprite;


		} else {
			this.gameObject.GetComponent<Image>().sprite = SoundOnSprite;

		}
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

    }

    public void OffMusic(){

		if (PersistantSoundManager == null) {
			PersistantSoundManager = GameObject.Find("PersistentSoundManager");
		}

		PersistantSoundManager.gameObject.GetComponent<SoundManager> ().TurnOffMusic ();

		if (this.gameObject.GetComponent<Image> ().sprite == SoundOnSprite) {
			this.gameObject.GetComponent<Image>().sprite = SoundOffSprite;


		} else {
			this.gameObject.GetComponent<Image>().sprite = SoundOnSprite;

		}
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

    }

}
