﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using UnityEngine;

public class LoadingBar : MonoBehaviour {

    public Slider loadingBar;

    void Start()
    {
        StartCoroutine("LoadYourAsyncScene");
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("ShadowTestScene");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {

            float progress = Mathf.Clamp01(asyncLoad.progress/.9f);

            loadingBar.value = progress;

          //  print("the progress is " + progress.ToString());


            yield return null;
        }
    }
}
