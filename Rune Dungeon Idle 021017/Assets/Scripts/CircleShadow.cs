﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleShadow : MonoBehaviour {

	// Use this for initialization
	//public GameObject shadowObj;
	public LayerMask chooseLayer;
	public GameObject torsoObj;

	public int xInitialScale;
	public int xScaleMultiplier;
	public bool enemyAlive = true;

	public float initialAlpha;
	public float currentAlpha;
	public SpriteRenderer spriteAlpha;

	public bool enemyDead = false;

	void Start () {
		initialAlpha = this.gameObject.GetComponent<SpriteRenderer> ().color.a;
	}
	
	// Update is called once per frame
	void Update () {
		if (torsoObj != null ) {
			RaycastHit2D hit =	Physics2D.Raycast (torsoObj.transform.position, Vector2.down, 100, LayerMask.GetMask ("Floor"));

	//		Debug.DrawRay (torsoObj.transform.localPosition, Vector2.down);

			if (hit.collider != null) {

				float tempX = (float)hit.point.x;
				float tempY = (float)hit.point.y;

				this.transform.position = new Vector3 (tempX, tempY, 87.8f);

			//	print ("the distance is " + hit.distance);

				float hitDist = (float)hit.distance;
		
				if (hitDist <= xScaleMultiplier) {

					this.gameObject.transform.localScale = new Vector3 ((float)(xInitialScale - hitDist * xScaleMultiplier), 1.5f-hitDist/xScaleMultiplier, 1f); 
				} else {
					this.gameObject.transform.localScale = new Vector3 (0, 1, 1f); 

				}

				if (enemyDead == false) {
					Color tmpColor = spriteAlpha.color;
					tmpColor.a = initialAlpha - (initialAlpha * hitDist / 8f);
					currentAlpha = tmpColor.a;
					spriteAlpha.color = tmpColor;
				} else { //the enemy is dead

					Color tmpColor = spriteAlpha.color;

					if (tmpColor.a >=0.01) {
						tmpColor.a -= 0.02f;
						currentAlpha = tmpColor.a;
						spriteAlpha.color = tmpColor;
					}
				}

			}
		}
	}

//	IEnumerator fadeShadow(){
//
//		while (true) {
//
//			enemyDead = true; //this makes sure the automatic shadow fading doesnt apply
//
//
//			Color tmpColor = spriteAlpha.color;
//
//			if (tmpColor.a >=0.01) {
//				tmpColor.a -= 0.01f;
//				currentAlpha = tmpColor.a;
//				spriteAlpha.color = tmpColor;
//			} else {
//
//				enemyDead = false; //this makes sure the automatic shadow fading doesnt apply
//
//				//resetInitAplha ();
//			//	this.transform.position = new Vector3 (-100, -100, 87.8f); //to temp move it away so it doesnt reappear on the enemy
//
//				yield break;
//			}
//
//			yield return new WaitForSeconds(0.1f);
//
//		}
//	}

	public void resetInitAplha(){

	//	currentAlpha = spriteAlpha.color;
	//	this.gameObject.SetActive(false);
	//	this.transform.position = new Vector3 (-100, -100, 87.8f); //to temp move it away so it doesnt reappear on the enemy
	//	Color tmpColor = spriteAlpha.color;
	//	tmpColor.a = 1;
	//	spriteAlpha.color = tmpColor;
		//enemyDead = false;
	}
}
