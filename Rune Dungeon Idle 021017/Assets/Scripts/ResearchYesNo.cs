﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ResearchYesNo : MonoBehaviour {

	// Use this for initialization
	public List <GameObject> TreeList = new List<GameObject>();
	public List <String> TreeTitleList = new List<String> ();

//	public TextMeshProUGUI talentTitle;

	public TextMeshProUGUI researchTitle;
	public TextMeshProUGUI researchDescription;
	public TextMeshProUGUI amountSeconds;
	public TextMeshProUGUI upgradeText;
	public TextMeshProUGUI amountOfResearchPoints;
    public TextMeshProUGUI amountOfEventPoints;
    public TextMeshProUGUI totalResearchPoints;

    public GameObject talentBkg; //the backdrop for showing a talent icon
    public TextMeshProUGUI treeDescription; //describes the atk/mag/wealth trees before a talent is pressed


    public GameObject sunGlowHolder;
	public GameObject moonGlowHolder;
	public GameObject starGlowHolder;
	public GameObject prestigeGlowHolder;
    public GameObject eventGlowHolder;

    public GameObject prestigeNotification;

	public Toggle AtkToggle;
	public Toggle MagToggle;
	public Toggle WealthToggle;
	public Toggle PrestigeToggle;

    public Sprite ResearchBkg;
	public Sprite PrestigeBkg;

	public Image ResearchIconForAnim;
	public GameObject ResearchIconForAnimParticles;

	public Sprite ResearchUpgButton;
	public Sprite PrestigeUpgButton;

	public GameObject BackgroundObj;
	public GameObject ResearchButtonObj;


	public Sprite transparentSprite;

	public GameObject UnlockTalentButton;

	public MainMenu CameraObj;

	public GameObject talentHolder;
	public GameObject unlockTalentParticlesPrefab;
    public GameObject toggleHolder; //this is for the regular talent tree holder


	public Image researchIcon;
	public int timeHolder;
    public bool talentsMaxed = false;
    public bool prestigeTalentsMaxed = false;

    public GameObject talentsHolderObj;
    public GameObject eventTalentsHolder;

    public TextMeshProUGUI eventTalentToggleText;
    public TextMeshProUGUI normalTalentToggleText;


    void Awake(){

		for (int i = 0; i < CameraObj.ResearchList.Count; i++) { //trying to make it so all the spells can read their requirements quickly
			CameraObj.ResearchList [i].pointsInvested = PlayerPrefs.GetInt (CameraObj.ResearchList [i].gameObject.name + "pointsInvested"); 
		}

	}

	void Start () {
		//for testing
    //	PlayerPrefs.SetInt ("ResearchPoints",100);
	//	PlayerPrefs.SetInt ("PrestigePts",99);
	//	UnlockTalentButton.SetActive (false); //alreadyt in onenable

	}

	void OnEnable () {

        TalentsToggle(); //so the talents turn on at the start

        //this just makes sure its a blank slate whenever they hit the research talenets button
		ResearchIconForAnim.sprite = transparentSprite;
	//	ResearchIconForAnimParticles.SetActive (false);
		this.gameObject.GetComponentInParent<Animator> ().SetBool ("ResearchIconAnim",false);

		for (int i = 0; i < CameraObj.ResearchList.Count; i++) { //updates all the spells to make sure the new talents available are highlighted
			CameraObj.ResearchList [i].checkPrereqs (); //this is already done with the individual spells
		}

        researchTitle.text = "";
		researchDescription.text= "";
		amountSeconds.text= "";
		upgradeText.text= "";
		amountOfResearchPoints.text= "";
		UnlockTalentButton.SetActive (false);
		researchIcon.sprite = transparentSprite;
		talentHolder = null;

        talentBkg.SetActive(false); //the backdrop for showing a talent icon
        treeDescription.text =""; //describes the atk/mag/wealth trees before a talent is pressed

		for (int i = 0; i < TreeList.Count; i++) {
			TreeList [i].SetActive (false);
		}

		if (AtkToggle.isOn == true) {
			ActivateAttackTalentTree ();
		}
		if (MagToggle.isOn == true) {
			ActivateMagicTalentTree ();
		}
		if (WealthToggle.isOn == true) {
			ActivateWealthTalentTree ();
		}
		if (PrestigeToggle.isOn == true) {
			ActivatePrestigeTalentTree ();
		}

      //  print("talent window open!");

		updateResearchPoints ();

    }

	public void updateResearchPoints(){


		//amountOfResearchPoints.text = "Amount of Points Available: " ;
		if (PrestigeToggle.isOn == false) {
			totalResearchPoints.text = PlayerPrefs.GetInt ("ResearchPoints").ToString ();
		} else {
			totalResearchPoints.text = PlayerPrefs.GetInt ("PrestigePts").ToString ();

		}

        

            amountOfEventPoints.text = PlayerPrefs.GetInt("EventPoints").ToString(); //this text is unreleated to the others, so it doesnt matter when its updated
        

        if (PlayerPrefs.GetInt ("PrestigePts") >= 1) {
			prestigeNotification.SetActive (true);
		} else {
			prestigeNotification.SetActive (false);

		}

		Camera.main.GetComponent<MainMenu> ().ResearchCheckAtStart (); //checks what research has been completed. If new research has been completed recently
	}

//	public void ActivateAttackTalentTree(){ //remnant for reference only
//
//		for (int i = 0; i < TreeList.Count; i++) {
//
//			if (TreeList [i].activeSelf == false) {
//
//				TreeList [i].SetActive (false); //checks if 'i' is active and if it is deactivates it
//
//				if (i + 1 <= 2) { //checks if the next in the list is availablie and if it is activates it
//					TreeList [i + 1].SetActive (true);
//					talentTitle.text = TreeTitleList[i+1];
//
//				} else {
//					TreeList [0].SetActive (true); //if its not available, go back and activate beginning of the list
//					talentTitle.text = TreeTitleList[0];
//
//				}
//				break;
//			}
//		}
//	}

	public void PrestigeBkgSwitch(){

		BackgroundObj.GetComponent<Image> ().sprite = PrestigeBkg;
		ResearchButtonObj.GetComponent<Image> ().sprite = PrestigeUpgButton;
	}


	public void NormalTalentBkg(){

		BackgroundObj.GetComponent<Image> ().sprite = ResearchBkg;
		ResearchButtonObj.GetComponent<Image> ().sprite = ResearchUpgButton;

	}

    public void eventTalentsToggle()
    {
       
            amountOfResearchPoints.gameObject.SetActive(false);

            TreeList[1].SetActive(false); //checks if 'i' is active and if it is deactivates it
            TreeList[2].SetActive(false); //checks if 'i' is active and if it is deactivates it
            TreeList[3].SetActive(false); //checks if 'i' is active and if it is deactivates it
            TreeList[0].SetActive(false); //checks if 'i' is active and if it is deactivates it

            eventTalentsHolder.SetActive(true);
            toggleHolder.SetActive(false);

            //set all the toggles for the talent tree as false

            endTalentTutorial();


            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

            Color myToggleOn = new Color();
            ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
            Color myToggleOff = new Color();
            ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

            eventTalentToggleText.color = myToggleOn;
            normalTalentToggleText.color = myToggleOff;

            // print("evet talents toggle pressed");
            treeDescription.text = "<color=#423227FF>Event Tree</color> \n Use Event Points earned in Temple Events to learn special Talents. "; //describes the atk/mag/wealth trees before a talent is pressed
        

    }

    public void TalentsToggle()
    {
        //talentsHolderObj.SetActive(true);
        //eventTalentsHolder.SetActive(false);
        amountOfResearchPoints.gameObject.SetActive(true);
        eventTalentsHolder.SetActive(false);

        toggleHolder.SetActive(true);

        if (AtkToggle.isOn == true)
        {
            ActivateAttackTalentTree();
        }
        if (MagToggle.isOn == true)
        {
            ActivateMagicTalentTree();
        }
        if (WealthToggle.isOn == true)
        {
            ActivateWealthTalentTree();
        }
        if (PrestigeToggle.isOn == true)
        {
            ActivatePrestigeTalentTree();
        }

        //Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

        Color myToggleOn = new Color();
        ColorUtility.TryParseHtmlString("#F7E5C7FF", out myToggleOn);
        Color myToggleOff = new Color();
        ColorUtility.TryParseHtmlString("#B6864DFF", out myToggleOff);

        eventTalentToggleText.color = myToggleOff;
        normalTalentToggleText.color = myToggleOn;

      //  print("reg talents toggle pressed");

    }

    public void ActivateAttackTalentTree(){
		NormalTalentBkg ();
		resetBottomPane();
		amountOfResearchPoints.text = "<color=#423227FF>Talent Points Available: </color>" ;

		endTalentTutorial ();

		if (TreeList [0].activeSelf == true) {
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();
		}

        talentBkg.SetActive(false); //the backdrop for showing a talent icon
        treeDescription.text = "<color=#423227FF>Attack Tree</color> \n Attack talents make your tap attack stronger and give you attack abilities. Tap on a talent to see its description."; //describes the atk/mag/wealth trees before a talent is pressed

        TreeList [1].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [2].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [3].SetActive (false); //checks if 'i' is active and if it is deactivates it

		TreeList [0].SetActive (true); //checks if 'i' is active and if it is deactivates it

	}

	public void ActivateMagicTalentTree(){
		NormalTalentBkg ();
		resetBottomPane();
		amountOfResearchPoints.text = "<color=#423227FF>Talent Points Available: </color>" ;

		endTalentTutorial ();

		if (TreeList [1].activeSelf == true) {
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();
		}

        talentBkg.SetActive(false); //the backdrop for showing a talent icon
        treeDescription.text = "<color=#423227FF>Magic Tree</color> \n Magic talents make your Spells stronger and give you magic abilities. Tap on a talent to see its description."; //describes the atk/mag/wealth trees before a talent is pressed


        TreeList[2].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [0].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [3].SetActive (false); //checks if 'i' is active and if it is deactivates it

		TreeList [1].SetActive (true); //checks if 'i' is active and if it is deactivates it


	}

	public void ActivateWealthTalentTree(){
		NormalTalentBkg ();
		resetBottomPane();
		amountOfResearchPoints.text = "<color=#423227FF>Talent Points Available: </color>" ;

		endTalentTutorial ();

		if (TreeList [2].activeSelf == true) {
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();
		}

        talentBkg.SetActive(false); //the backdrop for showing a talent icon
        treeDescription.text = "<color=#423227FF>Wealth Tree</color> \n Wealth talents reduce costs, and improve other areas of your hero. Tap on a talent to see its description."; //describes the atk/mag/wealth trees before a talent is pressed


        TreeList[0].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [1].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [3].SetActive (false); //checks if 'i' is active and if it is deactivates it

		TreeList [2].SetActive (true); //checks if 'i' is active and if it is deactivates it


	}

	public void ActivatePrestigeTalentTree(){
		PrestigeBkgSwitch ();
		resetBottomPane();
		amountOfResearchPoints.text = "<color=#ED7CF9FF>Ascend Points Available: </color>" ;

        endTalentTutorial();

        if (TreeList [3].activeSelf == true) {
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();
		}

        talentBkg.SetActive(false); //the backdrop for showing a talent icon
        treeDescription.text = "<color=#423227FF>Ascend Tree</color> \n Further improve the Attack/Magic/Wealth of your character. You can upgrade Ascend talents after you Ascend"; //describes the atk/mag/wealth trees before a talent is pressed


        TreeList[0].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [1].SetActive (false); //checks if 'i' is active and if it is deactivates it
		TreeList [2].SetActive (false); //checks if 'i' is active and if it is deactivates it

		TreeList [3].SetActive (true); //checks if 'i' is active and if it is deactivates it


	}

	public void endTalentTutorial(){

		if (CameraObj.TutorialObject.talentsTutorial == true) {


			CameraObj.TutorialObject.refreshAllVariables ();
			//end tutorial
		}

	}

	public void resetBottomPane(){

		researchTitle.text = "";
		researchDescription.text= "";
		amountSeconds.text= "";
		upgradeText.text= "";
		amountOfResearchPoints.text= "";
		UnlockTalentButton.SetActive (false);
		researchIcon.sprite = transparentSprite;
		talentHolder = null;
		updateResearchPoints ();

	}

	public void resetGlowHolders(){

		sunGlowHolder.SetActive (false);

		moonGlowHolder.SetActive (false);

		starGlowHolder.SetActive (false);

        eventTalentsHolder.SetActive(false);

    }

	// Update is called once per frame
	public void ResearchDetails(GameObject researchObj, int requiredTime, string researchUpdateText){ //req time is in seconds

		talentHolder = researchObj; //this is so we can get the research time 
		timeHolder = requiredTime;
		amountSeconds.text = "";//"This talent takes <#00FF0CFF> " + timeHolder.ToString () + " seconds </color> to research.";

		if (TreeList [0].activeSelf == true) { //sun tree

			sunGlowHolder.transform.position = researchObj.transform.position;
		}

		if (TreeList [1].activeSelf == true) { //moon tree
			moonGlowHolder.transform.position = researchObj.transform.position;

		}

		if (TreeList [2].activeSelf == true) { //star tree
			starGlowHolder.transform.position = researchObj.transform.position;

		}

		if (TreeList [3].activeSelf == true) { //star tree
			prestigeGlowHolder.transform.position = researchObj.transform.position;

		}

        if (eventTalentsHolder.activeSelf == true)
        {
            eventGlowHolder.transform.position = researchObj.transform.position;

        }

        researchIcon.sprite = talentHolder.GetComponentInChildren<Image> ().sprite; //sets the sprite of the research as this GOs sprite

	//	if(talentHolder.GetComponent<ResearchUpgrade>().unlockable==true){
		
		researchTitle.text = talentHolder.GetComponent<ResearchUpgrade>().researchTitle.text; //sets the text as to the title of the research

		researchDescription.text = talentHolder.GetComponent<ResearchUpgrade>().researchDescription.text; //sets the text as to the description of the research
	//	}else{
			
		//	researchTitle.text = "???????????????"; //sets the text as to the title of the research

		//	researchDescription.text = "?????? ??? ?????? ?????? ?????? ??????"; //sets the text as to the description of the research

	//	}
		upgradeText.text = researchUpdateText; 

		//whether or not button appears should go here

	}

	public void beginResearch(){

        if (PlayerPrefs.GetInt(talentHolder.gameObject.name + "pointsInvested") < talentHolder.GetComponent<ResearchUpgrade>().pointCap &&
            PlayerPrefs.GetInt("ResearchPoints") > 0 && talentHolder.GetComponent<ResearchUpgrade>().PrestigeTalent == false && talentHolder.GetComponent<ResearchUpgrade>().eventTalent == false

            ||

            PlayerPrefs.GetInt(talentHolder.gameObject.name + "pointsInvested") < talentHolder.GetComponent<ResearchUpgrade>().pointCap + talentHolder.GetComponent<ResearchUpgrade>().pointCapOvercharge &&
            PlayerPrefs.GetInt("ResearchPoints") > 0 && talentHolder.GetComponent<ResearchUpgrade>().PrestigeTalent == false && talentHolder.GetComponent<ResearchUpgrade>().eventTalent == false && talentsMaxed == true)
        {

		//PlayerPrefs.SetString (talentHolder.gameObject.name + "StartTime", System.DateTime.Now.ToBinary ().ToString ()); //sets the starting time for this upgrade
		//PlayerPrefs.SetString (talentHolder.gameObject.name + "EndTime", System.DateTime.Now.AddSeconds(timeHolder).ToBinary().ToString()); //sets the starting time for this upgrade
		//important below vvvvv
		PlayerPrefs.SetInt (talentHolder.gameObject.name + "isResearched", 2); //1 is research has began, 2 is finished - i changed it to 2 to remove research time

		PlayerPrefs.SetInt (talentHolder.gameObject.name + "pointsInvested", (PlayerPrefs.GetInt (talentHolder.gameObject.name + "pointsInvested"))+1);

                PlayerPrefs.SetInt("ResearchPoints", PlayerPrefs.GetInt("ResearchPoints") - 1);

            talentHolder.GetComponent<ResearchUpgrade> ().pointsInvested += 1; //THIS wasnt added before and it needs to be updated because nowhere else adds it.

			talentHolder.GetComponent<ResearchUpgrade> ().refreshTalentPoints ();//this udates the text (and will also subtract the points from the points available?)

		talentHolder.gameObject.SetActive (false);
		talentHolder.gameObject.SetActive (true); //will this reenable the obj and start the coroutin? it also just refreshes the amount of points put into the talent

			talentHolder.GetComponent<ResearchUpgrade>().updateTalentUpgrade (); //updates teh descriptn
			talentHolder.GetComponent<ResearchUpgrade>().checkPrereqs ();


			//makes the upgrade line visible after its maxes
//			if (talentHolder.GetComponent<ResearchUpgrade>().relatedUpgradeLine != null) {
//
//				talentHolder.GetComponent<ResearchUpgrade>().relatedUpgradeLine.SetActive (true);
//			}



			updateResearchPoints (); //subtracts the point u just used

			for (int i = 0; i < Camera.main.GetComponent<MainMenu> ().ResearchList.Count; i++) { //updates all the spells to make sure the new talents available are highlighted
				CameraObj.ResearchList [i].checkPrereqs ();
                CameraObj.ResearchList[i].checkOvercharge();

            }

            talentHolder.GetComponent<ResearchUpgrade> ().researchButtonPress (); //to update the text after you researched it once
			//		closeWindow ();



			//all this code is for unlocking talents animations....
			ResearchIconForAnim.sprite = researchIcon.sprite;
			GameObject	_unlockTalentParticles = (GameObject)Instantiate (unlockTalentParticlesPrefab, new Vector3 (ResearchIconForAnimParticles.transform.position.x, ResearchIconForAnimParticles.transform.position.y, 90), Quaternion.Euler(new Vector3(90,0,0))); //instantiates the beam offscreen
			this.gameObject.GetComponentInParent<Animator> ().SetBool ("ResearchIconAnim",true);
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().UnlockTalent ();//plays sound for unlocking


		}else{
		}




		//***************this is for when the player upgrades a prestige talent
		if (PlayerPrefs.GetInt (talentHolder.gameObject.name + "pointsInvested") < talentHolder.GetComponent<ResearchUpgrade> ().pointCap &&
			PlayerPrefs.GetInt ("PrestigePts") > 0 && talentHolder.GetComponent<ResearchUpgrade> ().PrestigeTalent == true

            ||

            PlayerPrefs.GetInt(talentHolder.gameObject.name + "pointsInvested") < talentHolder.GetComponent<ResearchUpgrade>().pointCap + talentHolder.GetComponent<ResearchUpgrade>().pointCapOvercharge &&
            PlayerPrefs.GetInt("PrestigePts") > 0 && talentHolder.GetComponent<ResearchUpgrade>().PrestigeTalent == true && prestigeTalentsMaxed == true) {


			PlayerPrefs.SetInt (talentHolder.gameObject.name + "isResearched", 2); //1 is research has began, 2 is finished - i changed it to 2 to remove research time

			PlayerPrefs.SetInt (talentHolder.gameObject.name + "pointsInvested", (PlayerPrefs.GetInt (talentHolder.gameObject.name + "pointsInvested"))+1);

			PlayerPrefs.SetInt ("PrestigePts", PlayerPrefs.GetInt ("PrestigePts") -1);

			talentHolder.GetComponent<ResearchUpgrade> ().pointsInvested += 1; //THIS wasnt added before and it needs to be updated because nowhere else adds it.

			talentHolder.GetComponent<ResearchUpgrade> ().refreshTalentPoints ();//this udates the text (and will also subtract the points from the points available?)

			talentHolder.gameObject.SetActive (false);
			talentHolder.gameObject.SetActive (true); //will this reenable the obj and start the coroutin? it also just refreshes the amount of points put into the talent

			talentHolder.GetComponent<ResearchUpgrade>().updateTalentUpgrade (); //updates teh descriptn
			talentHolder.GetComponent<ResearchUpgrade>().checkPrereqs ();

			ResearchIconForAnim.sprite = researchIcon.sprite;
		//	ResearchIconForAnimParticles.SetActive (true);
			GameObject	_unlockTalentParticles = (GameObject)Instantiate (unlockTalentParticlesPrefab, new Vector3 (ResearchIconForAnimParticles.transform.position.x, ResearchIconForAnimParticles.transform.position.y, 90), Quaternion.Euler(new Vector3(90,0,0))); //instantiates the beam offscreen


			this.gameObject.GetComponentInParent<Animator> ().SetBool ("ResearchIconAnim",true);
			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().UnlockTalent ();//plays sound for unlocking


			updateResearchPoints (); //subtracts the point u just used

			for (int i = 0; i < Camera.main.GetComponent<MainMenu> ().ResearchList.Count; i++) { //updates all the spells to make sure the new talents available are highlighted
				CameraObj.ResearchList [i].checkPrereqs ();
			}

			talentHolder.GetComponent<ResearchUpgrade> ().researchPrestigeButtonPress (); //to update the text after you researched it once
			//		closeWindow ();
		}


        //*********************this is when a player upgrades an event talent

        if (PlayerPrefs.GetInt(talentHolder.gameObject.name + "pointsInvested") < talentHolder.GetComponent<ResearchUpgrade>().pointCap &&
        PlayerPrefs.GetInt("EventPoints") > 0 && talentHolder.GetComponent<ResearchUpgrade>().eventTalent == true)
        {

           // PlayerPrefs.SetString(talentHolder.gameObject.name + "StartTime", System.DateTime.Now.ToBinary().ToString()); //sets the starting time for this upgrade
           // PlayerPrefs.SetString(talentHolder.gameObject.name + "EndTime", System.DateTime.Now.AddSeconds(timeHolder).ToBinary().ToString()); //sets the starting time for this upgrade
                                                                                                                                               //important below vvvvv
            PlayerPrefs.SetInt(talentHolder.gameObject.name + "isResearched", 2); //1 is research has began, 2 is finished - i changed it to 2 to remove research time

            PlayerPrefs.SetInt(talentHolder.gameObject.name + "pointsInvested", (PlayerPrefs.GetInt(talentHolder.gameObject.name + "pointsInvested")) + 1);

            PlayerPrefs.SetInt("EventPoints", PlayerPrefs.GetInt("EventPoints") - 1);

            talentHolder.GetComponent<ResearchUpgrade>().pointsInvested += 1; //THIS wasnt added before and it needs to be updated because nowhere else adds it.

            talentHolder.GetComponent<ResearchUpgrade>().refreshTalentPoints();//this udates the text (and will also subtract the points from the points available?)

            talentHolder.gameObject.SetActive(false);
            talentHolder.gameObject.SetActive(true); //will this reenable the obj and start the coroutin? it also just refreshes the amount of points put into the talent

            talentHolder.GetComponent<ResearchUpgrade>().updateTalentUpgrade(); //updates teh descriptn
            talentHolder.GetComponent<ResearchUpgrade>().checkPrereqs();


            //makes the upgrade line visible after its maxes
            //			if (talentHolder.GetComponent<ResearchUpgrade>().relatedUpgradeLine != null) {
            //
            //				talentHolder.GetComponent<ResearchUpgrade>().relatedUpgradeLine.SetActive (true);
            //			}



            updateResearchPoints(); //subtracts the point u just used

            for (int i = 0; i < Camera.main.GetComponent<MainMenu>().ResearchList.Count; i++)
            { //updates all the spells to make sure the new talents available are highlighted
                CameraObj.ResearchList[i].checkPrereqs();
                CameraObj.ResearchList[i].checkOvercharge();

            }

            talentHolder.GetComponent<ResearchUpgrade>().eventButtonPress(); //to update the text after you researched it once
                                                                                //		closeWindow ();



            //all this code is for unlocking talents animations....
            ResearchIconForAnim.sprite = researchIcon.sprite;
            GameObject _unlockTalentParticles = (GameObject)Instantiate(unlockTalentParticlesPrefab, new Vector3(ResearchIconForAnimParticles.transform.position.x, ResearchIconForAnimParticles.transform.position.y, 90), Quaternion.Euler(new Vector3(90, 0, 0))); //instantiates the beam offscreen
            this.gameObject.GetComponentInParent<Animator>().SetBool("ResearchIconAnim", true);
            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();//plays sound for unlocking


        }
        else
        {
        }



    }

    public void closeWindow(){
		this.gameObject.SetActive (false);

	}


}
