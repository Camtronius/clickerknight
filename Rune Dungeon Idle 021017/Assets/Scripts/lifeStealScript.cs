﻿using UnityEngine;
using System.Collections;

public class lifeStealScript : MonoBehaviour {

	public int createdByPlayer = 0;
	public int lerpSpeed =5;
	public int randFloorPosY;
	public int healthStolenValue = 10; //there are 8 instances of the lifestealfxcreation so this total needs to be divided by 8 since there are 8
	public int amountOfStealFX = 8;

	public Vector3 player1pos;
	public Vector3 player2pos;

	public Vector3 floorPos;
	// Use this for initialization
	void Start () {
	if (createdByPlayer == 1) {
			player1pos = GameObject.Find ("Player1Character/Player1/Torso").transform.position;
		}
	if (createdByPlayer == 2) {
			player2pos = GameObject.Find ("Player2Character/Player2/Torso").transform.position;
		}

		floorPos = GameObject.Find ("FloorSprite").GetComponent<BoxCollider2D> ().bounds.center;

		//randFloorPosY = Random.Range (10, 15);
	}

	public Vector2 Bezier(float t, Vector2 a, Vector2 b, Vector2 c, Vector2 d)
	{
		var cd = Vector2.Lerp(a, b,t);
		var ab = Vector2.Lerp(c,b,t);

		return Vector2.Lerp(ab,cd,t);
	}

	
	// Update is called once per frame
	void Update () {
	
		if (createdByPlayer == 1) {
		
			//maybe try a bezier curve to fix this eventually?
			transform.position = Vector2.Lerp(this.transform.position,player1pos,Time.deltaTime*lerpSpeed);
		}

		if (createdByPlayer == 2) {
			
			//maybe try a bezier curve to fix this eventually?
			transform.position = Vector2.Lerp(this.transform.position,player2pos,Time.deltaTime*lerpSpeed);
		}

	}

	void OnTriggerEnter2D (Collider2D player) {
		
		if (player.tag == "Player1" && createdByPlayer == 1) {

			player.GetComponent<ControlPlayer1Character>().lifeSteal=true;
			player.GetComponent<ControlPlayer1Character>().lifeStealValue=(float)healthStolenValue/amountOfStealFX;

			Destroy(this.gameObject);
			
		}

		if (player.tag == "Player2" && createdByPlayer == 2) {
			
			player.GetComponent<ControlPlayer2Character>().lifeSteal=true;
			player.GetComponent<ControlPlayer2Character>().lifeStealValue=(float)healthStolenValue/amountOfStealFX;
			
			Destroy(this.gameObject);
			
		}
	}


}
