﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LargeNumConverter : MonoBehaviour {

	// Use this for initialization
	public int dmgGiven = 0;


	public static string[] suffixArray = new string[]{

		"K ","M ","B ","T ","aa","bb","cc","dd","ee","ff","gg","hh", "ii","jj","kk","ll","mm","nn","oo","pp","qq","rr","ss","tt","uu","vv","ww","xx","yy","zz" 
		,"Aa","Ab","Ac","Ad","Ae","Af","Ag","Ah", "Ai","Aj","Ak","Al","Am","An","Ao","Ap","Aq","Ar","As","At","Au","Av","Aw","Ax","Ay","Az"
		,"Ba","Bb","Bc","Bd","Be","Bf","Bg","Bh", "Bi","Bj","Bk","Bl","Bm","Bn","Bo","Bp","Bq","Br","Bs","Bt","Bu","Bv","Bw","Bx","By","Bz"
		,"Ca","Cb","Cc","Cd","Ce","Cf","Cg","Ch", "Ci","Cj","Ck","Cl","Cm","Cn","Co","Cp","Cq","Cr","Cs","Ct","Cu","Cv","Cw","Cx","Cy","Cz"


	};

	public static string setDmg(double dmgAmount){

		string dmgString = dmgAmount.ToString ();
		string dmgAmountText="";

		int numZeros = (int)Math.Log10 (dmgAmount); //converts it to a float right here... converted to Math instead of Mathf to handle double values

		//	print("Number of zeros is " + numZeros.ToString());

		int suffixIndex = (int)(numZeros/3); //this changes the suffix based on how many zeros: 3 zeros = thousands, 6 zeros = millions etc

		suffixIndex -=1;  //makes it so it starts at thousands, if you have three zeros and the int is at 1, then it will start at millions. Subtracting one makes it start at index 0 which is thousands

		if (suffixIndex >= 0) { //if we are past the thousands place...
			
			double newDmg = dmgAmount / Math.Pow (10, (suffixIndex + 1) * 3); //this is what moves the decimal place. 
			//Divides damage by 10^X. For example if we have 1000, our suffix index is 1, then we subtract 1 which would make it zero. We add one then and multiple by 3 which gives us 1000. 
			//Therefor 1000dmg/1000 = 1.000K

			dmgString = newDmg.ToString (); //making a temp string for newDmg so i can replace the , with a .

			dmgString = dmgString.Replace (",", "."); //replaces , with . 

			//	print("New Dmg is " + dmgString + suffixArray[suffixIndex]); //writes the string

			if(dmgString.Length>4){
				dmgString = dmgString.Substring (0, 4); //10B
			}

			dmgAmountText = dmgString + " "+suffixArray [suffixIndex];

		} else {
			if(dmgString.Length>3){
			dmgString = dmgString.Substring (0, 3); //10B
			}
			dmgAmountText = dmgString;
		}

		return dmgAmountText;

	}

	public static string setDmgShort(double dmgAmount){

		string dmgString = dmgAmount.ToString ();
		string dmgAmountText="";

		int numZeros = (int)Math.Log10 (dmgAmount); //converts it to a float right here... converted to Math instead of Mathf to handle double values

		//	print("Number of zeros is " + numZeros.ToString());

		int suffixIndex = (int)(numZeros/3); //this changes the suffix based on how many zeros: 3 zeros = thousands, 6 zeros = millions etc

		suffixIndex -=1;  //makes it so it starts at thousands, if you have three zeros and the int is at 1, then it will start at millions. Subtracting one makes it start at index 0 which is thousands

		if (suffixIndex >= 0) { //with prestige we will never be less than thousands place

			double newDmg = dmgAmount / Math.Pow (10, (suffixIndex + 1) * 3); //this is what moves the decimal place. 
			//Divides damage by 10^X. For example if we have 1000, our suffix index is 1, then we subtract 1 which would make it zero. We add one then and multiple by 3 which gives us 1000. 
			//Therefor 1000dmg/1000 = 1.000K

			dmgString = newDmg.ToString (); //making a temp string for newDmg so i can replace the , with a .

			dmgString = dmgString.Replace (",", "."); //replaces , with . 

			//	print("New Dmg is " + dmgString + suffixArray[suffixIndex]); //writes the string

			if(dmgString.Length>4){
				dmgString = dmgString.Substring (0, 3); //10B
			}

			dmgAmountText = dmgString + " "+suffixArray [suffixIndex];

		}

		return dmgAmountText;

	}
}
