﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LightningColliderScript : MonoBehaviour {
	public float timeAlive=0;
	public GameObject SpellCreator;
	// Use this for initialization
	public int createdByPlayer=1;

	void Start () {
	
		SpellCreator = GameObject.Find ("SpellListener");
		
		//ScreenShaker = GameObject.Find ("ScreenShakerController");

		StartCoroutine ("BallUpDown");

	}

	void Update(){
		
		timeAlive += Time.deltaTime;
		
		if(timeAlive>3){
			
			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
				
				Destroy(this.gameObject);
				
			}
			
			if(SpellCreator.GetComponent<SpellCreator>()!=null){
				
				SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
				
				SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
				
				Destroy(this.gameObject);
				
			}
			
		}
		
		
	}

	void OnTriggerEnter2D (Collider2D player) {
	
		damageEnemy (player);
		this.gameObject.GetComponent<BoxCollider2D> ().isTrigger = false;
	}


	IEnumerator BallUpDown(){
		
		while (true) {

			this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,-7);
			yield return new WaitForSeconds(0.15f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,12);
			yield return new WaitForSeconds(0.15f);

		}
		
	}
	/*
	public void damageEnemy(Collider2D player){ //this will pass in the object so we can just call one method instead of one giant method
		
		Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen
		
				
		float calculatedHealthLoss = 1000;//(PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball") 
		                             // + PlayerPrefs.GetFloat ("FireItemMod") 
		                             // + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
		
				
			//player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
			
			
			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
			   == Camera.main.GetComponent<CampaignGameOrganizer>().EarthSprite){
				
				
		//		calculatedHealthLoss = 2*calculatedHealthLoss;
				
		//		Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(1);//1 = super effective, 2 = not very effectiveness
			}
			
			//condition for Not very effective
			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
			   == Camera.main.GetComponent<CampaignGameOrganizer>().FireSprite){
				
		//		calculatedHealthLoss = (.5f)*calculatedHealthLoss;
				
		//		Camera.main.GetComponent<CampaignGameOrganizer>().effectiveness(2);//1 = super effective, 2 = not very effectiveness
				
			}
			
			if(Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
			   == Camera.main.GetComponent<CampaignGameOrganizer>().WindSprite ||
			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
			   == Camera.main.GetComponent<CampaignGameOrganizer>().WaterSprite ||
			   Camera.main.GetComponent<CampaignGameOrganizer>().LastSpellPlayer2.GetComponent<Image>().sprite 
			   == Camera.main.GetComponent<CampaignGameOrganizer>().nullsprite){
				
				//	player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
				
			}
			
			if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
				
				calculatedHealthLoss = 2*calculatedHealthLoss;
				print("CRITS!!!!!!!!!");
				
			}
			
		if (player.GetComponent<ControlEnemy> () != null) {
			player.GetComponent<ControlEnemy> ().playerHealthNew -= calculatedHealthLoss;
		}
			Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
			
	
		/*
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().createBurntRunes();
				
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 1); //this used to be 0,1
				}
				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true){
					Camera.main.GetComponent<TurnedBasedGameOrganizer> ().BurntRunesManager (0, 2);
				}

		//	}
		//End where Damage is calcualted
		
		
		
		if(player.GetComponent<ControlEnemy>()!=null && calculatedHealthLoss >= player.GetComponent<ControlEnemy>().playerHealthCurrent){
			
			player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
			player.gameObject.GetComponent<EnemyRagdoll>().enabled=true; //sets the player to a ragdoll
			player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll();
			
		}
		
//		this.GetComponent<SpriteRenderer>().enabled = false; //this is for the fireball not the player....
	//	this.GetComponent<BoxCollider2D>().enabled=false;
		
	}
	*/
	public void damageEnemy(Collider2D player) {
		
		
//		if (player.gameObject.tag == "Enemy" || player.gameObject.tag == "Player2" && createdByPlayer==1) {
//			
//			if(Camera.main.GetComponent<CampaignGameOrganizer>()!=null && player.gameObject.GetComponent<ControlEnemy>().isShielded==false){ //when p1 is attackin enemy or enemyplayer in campaign
//				
//				print ("there is a collision with enemy!!!");
//				
//				
//				Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("AutoAtkItemMod")  
//				                              + PlayerPrefs.GetFloat ("WaterItemMod") 
//				                              + PlayerPrefs.GetFloat ("WindItemMod") 					                  
//				                              + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
//				
//				
//				if(Camera.main.GetComponent<CampaignGameOrganizer>().criticalHit() ==true){
//					
//					calculatedHealthLoss = 2*calculatedHealthLoss;
//					print("CRITS!!!!!!!!!");
//					
//				}
//				
//				player.GetComponent<ControlEnemy>().playerHealthNew -= calculatedHealthLoss;
//				
//				Camera.main.GetComponent<CampaignGameOrganizer>().damageEnemyNotification(calculatedHealthLoss);
//				
//				
//				if (player.GetComponent<ControlEnemy>()!=null && calculatedHealthLoss >= player.GetComponent<ControlEnemy> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlEnemy>().playerDead=true;
//					player.gameObject.GetComponent<EnemyRagdoll> ().enabled = true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<EnemyRagdoll> ().createRagDoll ();
//					
//					Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f, 0.1f); //shakes the screen
//					
//				}			
//			}
//			
//			if (player.gameObject.tag == "Player2" && createdByPlayer==1 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) { //this is for multiplayer
//				
//				if(player.gameObject.GetComponentInChildren<ControlPlayer2Character>().isShielded==false){
//					
//					print ("there is a collision with PLAYER!!!");
//					
//					Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//					
//					float DamageToPlayer = (PlayerPrefs.GetFloat ("AutoAtkItemMod")  
//					                        + PlayerPrefs.GetFloat ("WaterItemMod") 
//					                        + PlayerPrefs.GetFloat ("WindItemMod") 					                  
//					                        + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
//					
//					
//					
//					//hit p2
//					
//					//check for crit against p2
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 &&
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true 
//					   ){ //did the player crit on their turn??
//						DamageToPlayer = 2*DamageToPlayer;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//						
//					}
//					
//					if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 && 
//					   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(1)==1){ //did P1 crit P2 and P2 is recieveing the atk.
//						DamageToPlayer = 2*DamageToPlayer;
//						Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					}
//					
//					if (player.gameObject.GetComponent<ControlPlayer2Character> ()!=null 
//					    && DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer2Character> ().playerHealthCurrent) {
//						
//						player.gameObject.GetComponent<ControlPlayer2Character> ().playerDead = true;
//						//	player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//						//	player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//						
//					}
//					
//					player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//					
//					player.GetComponent<ControlPlayer2Character>().playerHealthNew -= DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP2Notification(DamageToPlayer); //damage p2 notification
//					
//					
//					//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmountText.GetComponent<Text> ().text = "-" + DamageToPlayer.ToString () + " hp";
//					//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmount.SetActive (true);
//				}
//			}
//			
//			this.gameObject.GetComponent<BoxCollider2D>().isTrigger=false;
//		}
//		
//		if (player.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<CampaignGameOrganizer>()!=null) { //this is for campaign
//			
//			if(player.gameObject.GetComponentInChildren<ControlPlayer1Character>().isShielded==false){
//				
//				print ("there is a collision with PLAYER!!!");
//				
//				Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				double DamageToPlayer = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().enemyDamage;
//				
//				if (player.gameObject.GetComponent<ControlPlayer1Character> ()!=null && 
//				    DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
//					player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//					player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//					
//					
//				}
//				
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;		
//				Camera.main.GetComponent<CampaignGameOrganizer>().damagePlayerNotification(DamageToPlayer); //this is the dmg notification for the player
//				
//			}
//		}
//		
//		
//		
//		if (player.gameObject.tag == "Player1" && createdByPlayer==2 && Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null) { //this is for multiplayer
//			
//			if(player.gameObject.GetComponentInChildren<ControlPlayer1Character>().isShielded==false){
//				
//				print ("there is a collision with PLAYER!!!");
//				
//				Camera.main.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.1f); //shakes the screen
//				
//				float DamageToPlayer = (PlayerPrefs.GetFloat ("AutoAtkItemMod")  
//				                        + PlayerPrefs.GetFloat ("WaterItemMod") 
//				                        + PlayerPrefs.GetFloat ("WindItemMod") 
//				                        + PlayerPrefs.GetFloat ("atkMod"))*10 + Random.Range(1,11);
//				
//				
//				
//				//hit p1
//				//check for crit against p1
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2 &&
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().criticalHit()==true
//				   ){ //did the player crit on their turn??
//					DamageToPlayer = 2*DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//				
//				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1 && 
//				   Camera.main.GetComponent<TurnedBasedGameOrganizer>().recieveCrit(2)==1){ //did P2 crit P1 and P1 is recieveing the atk.
//					DamageToPlayer = 2*DamageToPlayer;
//					Camera.main.GetComponent<TurnedBasedGameOrganizer>().critTextObj.SetActive(true);
//					
//				}
//				
//				if (player.gameObject.GetComponent<ControlPlayer1Character> ()!= null && 
//				    DamageToPlayer >= player.gameObject.GetComponent<ControlPlayer1Character> ().playerHealthCurrent) {
//					
//					player.gameObject.GetComponent<ControlPlayer1Character> ().playerDead = true;
//					//player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
//					//	player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll ();
//					
//					
//				}
//				
//				
//				
//				player.gameObject.GetComponent<TurnOnRagdoll>().enabled=true; //sets the player to a ragdoll
//				player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
//				
//				player.GetComponent<ControlPlayer1Character>().playerHealthNew -= DamageToPlayer;
//				Camera.main.GetComponent<TurnedBasedGameOrganizer>().damageP1Notification(DamageToPlayer); //damage p2 notification
//				
//				
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmountText.GetComponent<Text> ().text = "-" + DamageToPlayer.ToString () + " hp";
//				//	Camera.main.GetComponent<CampaignGameOrganizer> ().playerDamageAmount.SetActive (true);
//			}
//			
//			this.gameObject.GetComponent<BoxCollider2D>().isTrigger=false;
//			
//		}
//		
//		
//		
//
//		
//		
	}
}


