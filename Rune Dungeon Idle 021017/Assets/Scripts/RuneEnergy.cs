﻿using UnityEngine;
using System.Collections;

public class RuneEnergy : MonoBehaviour {

	public Material FireMat;
	public Material WaterMat;
	public Material EarthMat;
	public Material WindMat;

	public GameObject energyExplosion;

	public Vector3 endPosition;
	// Use this for initialization

	// Update is called once per frame

	public void setSprite(Sprite sentRuneSprite){

		this.GetComponent<SpriteRenderer> ().sprite = sentRuneSprite;

	}

	public void setEndPosition(Vector3 endPositionRecieved){


		endPosition = endPositionRecieved;
	}

	void Update () {
	

		this.transform.position = Vector2.Lerp(this.transform.position, endPosition ,Time.deltaTime*10f);

		if (Mathf.Abs(this.transform.position.x - endPosition.x)<0.01f ) {

			this.gameObject.GetComponentInChildren<ParticleSystem>().enableEmission = false;
			energyExplosion.SetActive(true);
		}
	}


}
