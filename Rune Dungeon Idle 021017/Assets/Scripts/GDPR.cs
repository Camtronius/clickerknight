﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GDPR : MonoBehaviour {

	// Use this for initialization
	public Image checkmark;
	public Button okayButton;

	void OnEnable () {
		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().showInventoryItem ();
        Camera.main.GetComponent<MainMenu>().windowOpen = true;
	}

	void OnDisable(){

		this.gameObject.GetComponent<Animator> ().SetBool ("checkMarkTrue", false);

	}
	
	// Update is called once per frame
	public void clickedCheckBox(){

		Color tempColor = checkmark.color;

		tempColor.a = 1f;

		checkmark.color = tempColor;

		okayButton.gameObject.SetActive (true);
		this.gameObject.GetComponent<Animator> ().SetBool ("checkMarkTrue", true);

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().TalentSelect ();

		PlayerPrefs.SetInt ("GDPR", 1);
	}

	public void viewEULA(){

		Application.OpenURL ("https://www.ironhorsegames.org/eula");

	}

	public void viewPrivacyPolicy(){

		Application.OpenURL ("https://www.ironhorsegames.org/privacy-policy");

	}

	public void clickedOK(){

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().BackButton ();

        Camera.main.GetComponent<MainMenu>().windowOpen = false;

        if (PlayerPrefs.GetInt("ClickToKillTutorial") == 0)
        {
            //	disabling this for now to update unity and fix IAP issue
            TutorialController tutObj = Camera.main.GetComponent<MainMenu>().TutorialObject;

            tutObj.clickOnMonsterTut = true;
            tutObj.twoPartTutorial = true;

            Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().oldManConfident();


            tutObj.TutPartOne("Welcome! I am the great wizard Setheron!");
            tutObj.secondThingToSay = "Tap Anywhere to attack this monster!";

            //the playtpref gets set to 1 in enemy ragdoll of the enemey
        }

        this.gameObject.SetActive (false);
	}
}
