﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class CSVReaderShort
{
	static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
	static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
	static char[] TRIM_CHARS = { '\"' };

	public static List<Dictionary<string, object>> Read(string file,int iterateStart, int iterateEnd)
	{
		var list = new List<Dictionary<string, object>>();
		TextAsset data = Resources.Load (file) as TextAsset;

		var lines = Regex.Split (data.text, LINE_SPLIT_RE);

		if(lines.Length <= 1) return list;

		var header = Regex.Split(lines[0], SPLIT_RE);
		for(var i= iterateStart; i < iterateEnd; i++) { // i=1 and was lines.length for i<20. changing this to reduce overhead of loading so much crap

			var values = Regex.Split(lines[i], SPLIT_RE);
			if(values.Length == 0 ||values[0] == "") continue;

      //      Debug.Log("the line is " + lines[i]); //this shows the CSV broken up into rows

			var entry = new Dictionary<string, object>();

            for (var j=0; j < header.Length && j < values.Length; j++ ) {


                    string value = values[j]; //this starts at the first column of the CSV thats split into rows

                //this finds the value, trims it, and sees if it can parse a double out of it. It then puts this value under that header in the dictionary
                //the header is defined as the very 1st row of the CSV
                    value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
                    object finalvalue = value;
                    int n;
                    double f;
                    if (double.TryParse(value, out f))
                    {
                        finalvalue = f;
                    }
                    entry[header[j]] = finalvalue;


            }

            list.Add (entry);
		}
		return list;
	}
}