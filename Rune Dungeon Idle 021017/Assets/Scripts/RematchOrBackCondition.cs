﻿using UnityEngine;
using System.Collections;
//using ChartboostSDK;

public class RematchOrBackCondition : MonoBehaviour {

	public GameObject playerObject;
	// Use this for initialization
	public void backPressed(){

		storeExpGoldLevel ();

		Camera.main.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.MainMenuMusic ();

		Application.LoadLevel ("RunesMainScene");
	}

	public void rematchPressed(){
		Application.LoadLevel ("RunesTestScene");

	}

	public void loadNextLevel(){

		//this will load the next level
		GameObject enemyStatsObj = GameObject.Find ("EnemyStatsInheriter");
		
		int currentLevel = enemyStatsObj.GetComponent<EnemyStatsInheriter> ().CurrentLevel;

	//	Chartboost.showInterstitial(CBLocation.HomeScreen);

		enemyStatsObj.GetComponent<EnemyStatsInheriter> ().loadLevel (currentLevel + 1);

		storeExpGoldLevel ();

		Application.LoadLevel ("RunesCampaign");
		
	}

	public void retryCurrentLevel(){

		GameObject enemyStatsObj = GameObject.Find ("EnemyStatsInheriter");
		
		int currentLevel = enemyStatsObj.GetComponent<EnemyStatsInheriter> ().CurrentLevel;

		storeExpGoldLevel ();

		enemyStatsObj.GetComponent<EnemyStatsInheriter> ().loadLevel (currentLevel);

		Application.LoadLevel ("RunesCampaign");

	}

	public void storeExpGoldLevel(){

	//	PlayerPrefs.SetInt ("Gold", playerObject.GetComponent<ControlPlayer1Character>().playerGold);
//		PlayerPrefs.SetInt ("Exp", Mathf.FloorToInt( playerObject.GetComponent<ControlPlayer1Character>().playerExpNew));
//		PlayerPrefs.SetInt ("ExpMax", Mathf.FloorToInt( playerObject.GetComponent<ControlPlayer1Character>().playerExpMax));
//		PlayerPrefs.SetInt ("Rank", playerObject.GetComponent<ControlPlayer1Character>().playerRank);



	}

}
