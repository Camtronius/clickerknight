﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;


public class CountDownToAttack : MonoBehaviour {

	public TextMeshProUGUI CountDownText;
	public int TimeStart = 3;
	public bool fromBoss = false;
	public GameObject bossButtonSet;
	public ControlPlayer1Character playerObj;
	public ControlEnemy enemyObj;
	public CampaignLevelsEnemySpawner enemySpawnerObj;


	// Use this for initialization
	void OnEnable () {
	
		if (fromBoss == true || enemySpawnerObj.InheritStats.eventMode==true) {
            fromBoss = true; //this is so if eventmode is true, it will restart the event from the beginning
            TimeStart = 15;
			bossButtonSet.SetActive (true);
		} else {
			bossButtonSet.SetActive (false);
			TimeStart = 5;
		}
		this.gameObject.GetComponent<Animator> ().enabled = true;

		CountDownText.text = TimeStart.ToString();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CountDown(){ //this is played with an animation. every second it calls CountDown on the animation.
	
		if (TimeStart > 1) {
			CountDownText.text = TimeStart.ToString ();
			TimeStart -= 1;
			CountDownText.text = TimeStart.ToString ();
            Camera.main.GetComponent<MainMenu>().windowOpen = true;



        }
        else if (fromBoss == false) {
			CountDownText.text = "";

			this.gameObject.GetComponent<Animator> ().enabled = false;
			this.gameObject.SetActive (false);
			TimeStart = 5;

			playerObj.resetPlayerHP ();
			playerObj.quickHPUpdate ();

			Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool ("charDown", false);

			Camera.main.GetComponent<MainMenu> ().closeAllWindows (null);

			Camera.main.GetComponent<MainMenu> ().windowOpen = false;
		} else {

			retryBoss ();
			Camera.main.GetComponent<MainMenu> ().closeAllWindows (null);

			Camera.main.GetComponent<MainMenu> ().windowOpen = false;
		}
		
	}

	public void retryBoss(){
        if (enemySpawnerObj.InheritStats.dungeonMode == false && enemySpawnerObj.InheritStats.eventMode == false)//if u no in a dngeon
        {
            this.gameObject.GetComponent<Animator>().enabled = false;
            this.gameObject.SetActive(false);
            TimeStart = 5;

            playerObj.resetPlayerHP();
            playerObj.quickHPUpdate();

            Camera.main.GetComponent<MainMenu>().animatedPlayerObj.GetComponent<Animator>().SetBool("charDown", false);


            //reset the enemy HP
            enemySpawnerObj.EnemySpawned.GetComponentInChildren<ControlEnemy>().resetBossHP();
            //reset boss slider

            bossButtonSet.SetActive(false);
            Camera.main.GetComponent<MainMenu>().closeAllWindows(null);

            Camera.main.GetComponent<MainMenu>().windowOpen = false;
        }
        else if(enemySpawnerObj.InheritStats.dungeonMode == true || enemySpawnerObj.InheritStats.eventMode==true)
        { //if u die in dungon or bnus dungon
            Camera.main.GetComponent<MainMenu>().loadLevel(PlayerPrefs.GetInt("CurrentLevel"));
        }

	}
}
