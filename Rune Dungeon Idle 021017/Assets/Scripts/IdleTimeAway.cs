﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics.Experimental;

using TMPro;
using System;


public class IdleTimeAway : MonoBehaviour {

	public double goldCounter = 0;
	public double goldEarnedAmt;

	public double expCounter = 0;
	public double expEarnedAmt;

	public double dailyLoginBonusCounter = 0;
	public double dailyLoginBonusAmt;

	public int numberOfZerosGold;
	public int numberOfZerosExp;
	public int numberOfZerosDailyLogin;

	public double _TotalHours;

	public TextMeshProUGUI goldEarned;
	public Animator goldEarnedAnimation;

	public TextMeshProUGUI expEarned;
	public Animator expEarnedAnimation;

	public TextMeshProUGUI timeAway;

	public GameObject dailyLoginBonus;
	public Animator dailyLoginBonusAnim;

	public TextMeshProUGUI dailyLoginBonusText;
	public Animator dailyLoginBonusTextAnim;
	public LoginBonusAnim loginBonusScript;

	public bool loginAnimFinished;
	public bool dailyLoginBonusBool;

//	public Image dailyLoginBonusGem;

	public DateTime oldDate;
	public DateTime newDate;

	public DateTime dailyOldDate;
	public DateTime dailyNewDate;

	public List<Dictionary<string,object>> data = new List<Dictionary<string, object>>(); //this is to get the players EXP max based on his current Rank
    public List<Dictionary<string, object>> expData = new List<Dictionary<string, object>>(); //this is to get the players EXP max based on his current Rank


    public ControlPlayer1Character controlPlayerScript;
	public EnemyStatsInheriter ememyStatsScript;

	public GameObject backButton;
	public GameObject watchAdButton;
	public AdManager adMngrScrpt;
	public SoundManager soundManager;

	public bool adwatched = false;
	public int currentLevel;
	public int currentRank;


	// Use this for initialization
	void OnEnable () {

		Camera.main.GetComponent<MainMenu> ().windowOpen = true;

		currentLevel = PlayerPrefs.GetInt ("CurrentLevel");
		currentRank = PlayerPrefs.GetInt ("Rank");

		TimeDifferenceChecker ();

		DailyLoginChecker ();

		soundManager = Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ();


	}

	public void TimeDifferenceChecker(){



		newDate = DateTime.Now; //stores the new date of the system at time of game start

      //  print("idle time away running data");

		long temp = Convert.ToInt64 (PlayerPrefs.GetString ("SavedTime"));

		oldDate = DateTime.FromBinary (temp);

		TimeSpan difference = newDate.Subtract (oldDate);

		timeAway.text = difference.Days + " Days / " + difference.Hours + " Hours / " + difference.Minutes + " Minutes";

        data = Camera.main.GetComponent<MainMenu>().enemyStatsObj.data;//CSVReader.Read("RuneIdleClicker", PlayerPrefs.GetInt("CurrentLevel"), PlayerPrefs.GetInt("CurrentLevel") + 35);
        expData = Camera.main.GetComponent<MainMenu>().enemyStatsObj.playerExpMaxData;
        //was data = CSVReader.Read ("RuneIdleClicker");

        _TotalHours = difference.TotalHours;



		//for testing
		//_TotalHours = 2;

		if (_TotalHours < 12) {
        // 9.6/24=0.4
			expEarnedAmt = (_TotalHours/30)*((double)expData[0] ["EXPMAX"]);


		} else {
			//if you have been gone longer than 24 hrs it will default to giving you 40% of your Max Exp

			expEarnedAmt = (0.4f)*((double)expData[0] ["EXPMAX"]);

		}

		if (_TotalHours < 12) {

            goldEarnedAmt = (double)data[2]["LVCOST"] * _TotalHours/24f; //gives up to half of the cost of the next level

			if (goldEarnedAmt < 100) { //this is so it can dbl on an ad watch, not that you would ever want to.
				goldEarnedAmt = 100;
			}

		} else {

            goldEarnedAmt = (double)data[2]["LVCOST"] * 0.5f;

        }

		numberOfZerosGold = (int)Math.Log10 (goldEarnedAmt); 
		numberOfZerosExp = (int)Math.Log10 (expEarnedAmt); 



	}

	public void startRoutine (){

		StartCoroutine ("GoldIncrementer");

	}

	public void DailyLoginChecker(){

	
			//this means the daily reward hasnt been claimed yet 
		if (PlayerPrefs.GetInt ("NewDay") == 0) {

			PlayerPrefs.SetString ("OldDay", DateTime.Today.ToBinary ().ToString ()); //sets the starting time for this upgrade

			//sets the int to 1 so the day isnt recorded again
			PlayerPrefs.SetInt ("NewDay",1);
		}

		//it was OldDay when it was originally created so thats why its called oldday
		DateTime OldDay = DateTime.FromBinary (Convert.ToInt64 (PlayerPrefs.GetString ("OldDay"))); 
		//player prefs cant use the same string to identify something
		DateTime todaysDate = DateTime.Today.Date;

	//	print ("todays date is " + todaysDate);
	//	print ("old date is " + OldDay.Date);

		if (OldDay.Date.Equals (todaysDate)) {

	//		print ("The Days ARE equal!");
		} else {
	//		print ("The Days ARE NOT equal! -- Daily Login Bonus!");

			dailyLoginBonusBool = true;

			dailyLoginBonusAmt = (double)data[2]["LVCOST"] * 0.25f;
	//		print ("the current level is " + currentLevel);


			PlayerPrefs.SetString ("OldDay", DateTime.Today.ToBinary ().ToString ()); //sets the starting time for this upgrade

            createNormDungeon(); //creates the normak dungeon

            createExtremeDungeon();

            resetTipAmount(); //this makes it so the player can tip again after 24hrs


            PlayerPrefs.SetInt("FreeGemAd", 0); //resets the free ad gem in the brew pot page

        }

        checkEventRefresh(); //so an event can be refreshed if the time has passed but its not a new day


        numberOfZerosDailyLogin = (int)Math.Log10 (dailyLoginBonusAmt); 

	}

    public void checkEventRefresh()
    {
        if (PlayerPrefs.GetInt("EventComplete")==1) {

            PlayerPrefs.SetFloat("LimitedEventHours", 0); //resets this value so we are starting from another 72hr  value
            PlayerPrefs.SetInt("EventComplete", 0);
            PlayerPrefs.SetInt("EventLevel", 0); //so the player starts at the first event index
            PlayerPrefs.SetString("LimitedEvent", ""); //starts the new event time at 0 frin whenever the player logged in.


        }

    }

    public void createNormDungeon()
    {

        
            PlayerPrefs.SetInt("DungeonComp", 0); //this unlocks the daily dungeon to be played

            //generate array to hold dungeon data

            int bossInt1 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
            int bossInt2 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
            int bossInt3 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)

            int currentLevelInt = PlayerPrefs.GetInt("CurrentLevel");

            //randomly selects a boss enemy, and then
            int[] dungeonArray = new int[] { bossInt1, bossInt2, bossInt3, currentLevelInt };
            PlayerPrefsX.SetIntArray("DungeonArray", dungeonArray); //Setting it as the new array so the dungeon is the same every time


        

    }

    public void createExtremeDungeon()
    {


            PlayerPrefs.SetInt("BonusDungeonComp", 0); //this unlocks the BONUS daily dungeon to be played
            PlayerPrefs.SetInt("DungeonAdWatched", 0); //so the player has to watch the ad again

            //  print("bonus dungeon array is null, making a new array now");

            int bossInt1 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
            int bossInt2 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)
            int bossInt3 = UnityEngine.Random.Range(1, 33); //goes from 1-32 (33 not included - 33 is EGG!!)

            int currentLevelInt = PlayerPrefs.GetInt("CurrentLevel");

            //randomly selects a boss enemy, and then
            int[] bonusDungeonArray = new int[] { bossInt1, bossInt2, bossInt3, currentLevelInt + 2 };//add 2 to the current level int. This makes the player have to play more to unlock the next pet. 
            PlayerPrefsX.SetIntArray("BonusDungeonArray", bonusDungeonArray); //Setting it as the new array so the dungeon is the same every time

        

    }

    private void resetTipAmount()
    {

        PlayerPrefs.SetInt("TipsLeft", 3); //this unlocks the BONUS daily dungeon to be played

    }

    IEnumerator GoldIncrementer(){ //***make sure this is only called once per game and doesnt repeatedly get called

		while (true) {

			if(goldCounter<goldEarnedAmt){ //for gold gain

				if (numberOfZerosGold >= 3) {
					goldCounter += Math.Pow (10, numberOfZerosGold - 1); //yada yada find an easier way to do this
					soundManager.dailyCountUp();
				} else {
					goldCounter += 100;
					soundManager.dailyCountUp();
				}


				goldEarned.text = LargeNumConverter.setDmg(goldCounter).ToString();

			}

			if (goldCounter >= goldEarnedAmt) { //for exp gain

				if (goldEarnedAnimation.enabled == false) { //plays the anim for gold scaling thingy
					goldEarnedAnimation.enabled = true;
					soundManager.dailyCountEnd ();

				}

				if(expCounter<expEarnedAmt){ //for gold gain


					if (numberOfZerosExp >= 3) {
						expCounter += Math.Pow (10, numberOfZerosExp - 1); //yada yada find an easier way to do this
						soundManager.dailyCountUp();
					} else {
						expCounter += 100;
						soundManager.dailyCountUp();
					}


					expEarned.text = LargeNumConverter.setDmg(expCounter).ToString();

				}

				if (expCounter >= expEarnedAmt) {

					if (expEarnedAnimation.enabled == false) { //plays the anim for gold scaling thingy
						expEarnedAnimation.enabled = true;
						soundManager.dailyCountEnd ();

					}
					//daily login bonus!

					if (dailyLoginBonusBool == true) {

						dailyLoginBonus.SetActive(true);

					//	soundManager.dailyLoginBonus ();//plays sound for daily login bonus

						dailyLoginBonusAnim.enabled = true;

						dailyLoginBonusText.enabled = true; //this is the actual amount of extra coins that gets counted
				
					//	print ("daily log in bonus supposed to be activated here!");

						//		dailyLoginBonusTextAnim.enabled = true;

				//		dailyLoginBonusGem.enabled = true;
					} else {

						if (adwatched == false) {
							watchAdButton.SetActive (true);
						}

						if (backButton.activeSelf == false && dailyLoginBonusBool==false) {
							yield return new WaitForSeconds(1f);//so the animation can finsih
							backButton.SetActive (true);
						}

						StopCoroutine ("GoldIncrementer");


					}

					if (dailyLoginBonusBool==true 
						&& dailyLoginBonusCounter < dailyLoginBonusAmt 
						&& loginBonusScript.AnimComplete==true) {

						if (numberOfZerosDailyLogin >= 3) {
							dailyLoginBonusCounter += Math.Pow (10, numberOfZerosDailyLogin - 1); //yada yada find an easier way to do this
							soundManager.dailyCountUp();
						} else {
							dailyLoginBonusCounter += 100;
							soundManager.dailyCountUp();
						}


						dailyLoginBonusText.text = LargeNumConverter.setDmg(dailyLoginBonusCounter);

						if (dailyLoginBonusCounter >= dailyLoginBonusAmt) {

							dailyLoginBonusText.text = LargeNumConverter.setDmg(dailyLoginBonusCounter);

							dailyLoginBonusTextAnim.enabled = true;
							soundManager.dailyCountEnd ();

						
							if (adwatched == false) {
								watchAdButton.SetActive (true);
							}

							if (backButton.activeSelf == false) {

								yield return new WaitForSeconds(1f);//so the anim can finish
								backButton.SetActive (true);
							}

							StopCoroutine ("GoldIncrementer");



						}

					}




			//		print ("coroutine for daily gold count stopped");
				}



			}

			yield return new WaitForSeconds(0.01f);

		}
	}

	public void showIdleAd(){

			adMngrScrpt.setIdleType ();
			adMngrScrpt.showUnityAd ("rewardedVideo",false);
	//		print ("button to show ad pressed");
			watchAdButton.SetActive (false);
			backButton.SetActive (false);
			adwatched = true;

		AnalyticsEvent.Custom("IdleAd");

	}

	public  void applyIdleAdBonus(){


		goldCounter = 0;
		goldEarnedAnimation.enabled = false;
		goldEarnedAmt = goldEarnedAmt * 3;

		if (dailyLoginBonusBool == true) {
			dailyLoginBonusAnim.enabled = false;
			dailyLoginBonusCounter = 0;
			dailyLoginBonusAmt = dailyLoginBonusAmt * 3;
		}

		StartCoroutine ("GoldIncrementer");
	}

}
