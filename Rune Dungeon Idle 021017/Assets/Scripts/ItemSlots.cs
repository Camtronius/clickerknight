﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class ItemSlots: MonoBehaviour {

	public enum ArmorType {Helm,Chest,Arms,Legs,Cape,Charm};
	public ArmorType ArmorEnum;
	public int armorLevel; //1 is default, 2 is fire, 3 is water, 4 is wind, 5 is earth, 6 is king, 7 is wolf, 8 is purp, 9 is invo 
	public Inventory inventoryScript;
	public List<Sprite> ArmorSprites = new List<Sprite> (); //itemSlots is a class by iteself. This is a list of the item slots class which will get serialized into an Xml file.
	public List<TextMeshProUGUI> AttributeList = new List<TextMeshProUGUI> (); //itemSlots is a class by iteself. This is a list of the item slots class which will get serialized into an Xml file.
	//public GameObject attributeLocation;

	public string[] arrayOfMods;
	public Image ArmorImage;
	public Image PopOutBorderImg;

	// Use this for initialization
	public string itemName = null;
	public TextMeshProUGUI itemNameTMPro;
	public Image IconRarityBorder;
	public Color itemColor = Color.gray;

	public double HPmod =0;
	public double ATKmod =0;
	public double MAGmod =0;
	public double GOLDmod =0;
	public double EXPmod =0;
	public double LSmod =0; //% lifesteal
	public double CRmod =0; //increased magic cast speed
	public double CRITmod =0; //increased critical Atk chance

	public string itemRarity;
	public string itemRaritySuffix;

	public GameObject attributeViewer;

	void Start () {

	//	print (ArmorEnum.ToString () + "Sprite");

	//	getSprite ();

		loadMods ();
	}

	public void saveMods(){

		PlayerPrefs.SetString (this.gameObject.name + "Name", itemName);//sets the name of the item

		PlayerPrefsX.SetColor (this.gameObject.name + "Color", itemColor);

		//PlayerPrefsX.SetFloatArray (this.gameObject.name + "Mods", arrayOfMods);//sets the mods of the item

		PlayerPrefsX.SetStringArray (this.gameObject.name + "Mods", arrayOfMods); //like the old array except for strings. then converts back to double

		PlayerPrefs.SetInt (this.gameObject.name + "Sprite", armorLevel); //sets the level of the armor so we know which sprite to use

	}

	public void loadMods(){

		itemName = PlayerPrefs.GetString (this.gameObject.name + "Name");

 			//***name is colored in GetAttributes

		itemColor = PlayerPrefsX.GetColor (this.gameObject.name + "Color");

	//	arrayOfMods = PlayerPrefsX.GetFloatArray (this.gameObject.name + "Mods");
		arrayOfMods = PlayerPrefsX.GetStringArray (this.gameObject.name + "Mods"); //like old but i converted array of mods into a string array


		armorLevel = PlayerPrefs.GetInt (this.gameObject.name + "Sprite");

		//this array doesnt exist for anything to save items to. You need to check if the array has been created from the Inventory script, and if it hasnt to make it
		if (arrayOfMods.Length != 0) {

			HPmod = double.Parse(arrayOfMods [0]);
			ATKmod = double.Parse(arrayOfMods [1]);
			MAGmod = double.Parse(arrayOfMods [2]);
			GOLDmod = double.Parse(arrayOfMods [3]);
			EXPmod = double.Parse(arrayOfMods [4]);
			LSmod = double.Parse(arrayOfMods [5]);
			CRmod = double.Parse(arrayOfMods [6]);
			CRITmod = double.Parse(arrayOfMods [7]);

		} else {

			arrayOfMods = new string[8];

		}
		getSprite ();

		getAttributes (); //writes what the items modifiers are
	}

	public void clearMods(){ //this is for th prestige system

	//	Array.Clear (arrayOfMods,0,arrayOfMods.Length); //this clears the array to make it length 0

		for (int i = 0; i < arrayOfMods.Length; i++) {

			arrayOfMods [i] = "0";
		}

		PlayerPrefs.SetString (this.gameObject.name + "Name", "");//sets the name of the item

		PlayerPrefsX.SetColor (this.gameObject.name + "Color", Color.gray); //this may need to be changed. im not sure what the default is...

		//PlayerPrefsX.SetFloatArray (this.gameObject.name + "Mods", arrayOfMods);//sets the mods of the item

		PlayerPrefsX.SetStringArray (this.gameObject.name + "Mods", arrayOfMods); //this would be clear now

		PlayerPrefs.SetInt (this.gameObject.name + "Sprite", 0); //sets the level of the armor so we know which sprite to use

	}

	public void getAttributes(){

		int amountOfMods =0;
		string tempColorString = "";

		for (int i = 0; i < arrayOfMods.Length; i++) {

			if (arrayOfMods [i] != null) {
				
				if (double.Parse (arrayOfMods [i]) > 0) {
					amountOfMods += 1;
				}
			}

		}

		for (int z = 0; z < AttributeList.Count; z++) { //hopeflly this will reset the attributes txt
			AttributeList [z].enabled = false;

		}

		switch (amountOfMods) {

		case 0:
			tempColorString = "#FFFFFF00";
		//	print ("0 attributes out");
			break;
		case 1:
			itemRarity = "<color=#7F603BFF>"; //</color> needs at the end
			itemRaritySuffix = "(Common)";
			if (IconRarityBorder != null) {
				IconRarityBorder.sprite = inventoryScript.RaritySprites [0];
			}
			tempColorString = "#7F603BFF";

			break;
		case 2:
			itemRarity = "<color=#36FD40FF>"; //</color> needs at the end, this is green
			itemRaritySuffix = "(Uncommon)";
			if (IconRarityBorder != null) {
				IconRarityBorder.sprite = inventoryScript.RaritySprites [1];
			}

			tempColorString = "#36FD40FF";

			break;
		case 3:
			itemRarity = "<color=#1F63ECFF>"; //</color> needs at the end
			itemRaritySuffix = "(Rare)";
			if (IconRarityBorder != null) {
				IconRarityBorder.sprite = inventoryScript.RaritySprites[2];
			}

			tempColorString = "#1F63ECFF";

			break;
		case 4:
			itemRarity = "<color=#8827F8FF>"; //</color> needs at the end //blue is C652ffff
			itemRaritySuffix = "(Epic)";
			if (IconRarityBorder != null) {
				IconRarityBorder.sprite = inventoryScript.RaritySprites [3];
			}

			tempColorString = "#8827F8FF";

			break;
			}

		itemNameTMPro.text = itemRarity + itemName + "</color>"; 

		if (PopOutBorderImg != null) {
			Color myColor = new Color ();
			if (ColorUtility.TryParseHtmlString (tempColorString, out myColor)) {
				PopOutBorderImg.color = myColor;
			}
		}

		if (IconRarityBorder!=null) { //this is for the item icon border
			
			Color myColor = new Color ();

			if (ColorUtility.TryParseHtmlString (tempColorString, out myColor)) {
			//	IconRarityBorder.color = myColor;
			}
		}

	//	print (amountOfMods + " is amt of mods for this GO");

		for (int k = 0; k < arrayOfMods.Length; k++) {

			if (arrayOfMods[k]!=null && double.Parse(arrayOfMods [k]) > 0 ) {

				if (k == 0) { //if its the HP mod
					
					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

							//all the k==0 etc may be able to be included solely in here without the need to repeat all those and run the loops again
						//	AttributeList [j].color = Color.red;
							AttributeList [j].text = "+ " + LargeNumConverter.setDmg(HPmod).ToString () + " Health";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 1) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

							AttributeList [j].text = "+ " + LargeNumConverter.setDmg(ATKmod).ToString()	 + "% Weapon Dmg";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 2) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

				//			AttributeList [j].color = Color.blue;
							AttributeList [j].text = "+ " + LargeNumConverter.setDmg(MAGmod).ToString() + "% Spell Dmg";
						//	print ("~~~~~~~~~~the magic mod is: " + MAGmod);

							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 3) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

				//			AttributeList [j].color = Color.yellow;
							AttributeList [j].text = "+ " + LargeNumConverter.setDmg(GOLDmod).ToString() + "% Gold per Kill";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 4) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

				//			AttributeList [j].color = Color.yellow;
							AttributeList [j].text = "+ " + EXPmod + "% EXP per Kill";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 5) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

				//			AttributeList [j].color = Color.red;
							AttributeList [j].text = "+ " +LargeNumConverter.setDmg(LSmod).ToString() + " HP/sec";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 6) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

				//			AttributeList [j].color = Color.blue;
							AttributeList [j].text = "+ " + (CRmod).ToString() + "s Spell Cast Rate";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}

				if (k == 7) { //if its the HP mod

					for (int j = 0; j < amountOfMods; j++) { //isnt printing any of the attribute names for some reason

						if (AttributeList [j].enabled == false) {

					//		AttributeList [j].color = Color.red;
							AttributeList [j].text = "+ " + CRITmod + "% Chance to Crit";
							AttributeList [j].enabled = true;
							break;
						} 

					}
				}


			}



		}



	}

	public void openAttributeViewer(){

		ItemSlots attributeItemSlot = attributeViewer.GetComponentInChildren<ItemSlots> ();

		if (arrayOfMods [0] != null) { //this is because the ring doesnt load any mods if it doesnt have any - and throws a null exception

			arrayOfMods.CopyTo (attributeItemSlot.arrayOfMods, 0); //copys the array over, not sure if this will reduce or expand the list

			PlayerPrefs.SetString (attributeItemSlot.gameObject.name + "Name", itemName);//sets the name of the item
//
			PlayerPrefsX.SetStringArray (attributeItemSlot.gameObject.name + "Mods", attributeItemSlot.arrayOfMods);//sets the mods of the item



			attributeItemSlot.loadMods ();
			attributeItemSlot.getAttributes ();

			int amountOfMods = 0;

			for (int i = 0; i < arrayOfMods.Length; i++) {

				if (arrayOfMods [i] != null && double.Parse (arrayOfMods [i]) > 0) {
					amountOfMods += 1;
				}

			}
			if (amountOfMods > 0) {
				//attributeViewer.transform.position = attributeLocation.transform.position;
				attributeViewer.SetActive (true);
			} else {
				attributeViewer.SetActive (false);

			}
		}

	}

	public void closeAttributeViewer(){
		attributeViewer.SetActive (false);


	}

	public void getSprite(){

		//	ItemSlots.ArmorType TestValue;

		if (ItemSlots.ArmorType.Helm == ArmorEnum) {

			switch (armorLevel) {

			case 1: //there is no case 1 now
			//	ArmorImage.sprite = inventoryScript.dictSprites ["NoviceHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["WolfHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["RoyalHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				ArmorImage.sprite = inventoryScript.dictSprites ["KnightHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				ArmorImage.sprite = inventoryScript.dictSprites ["DragonHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}

		}

		if (ItemSlots.ArmorType.Chest == ArmorEnum) {

			switch (armorLevel) {

			case 1:
				ArmorImage.sprite = inventoryScript.dictSprites ["NoviceTorsoFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["RoyalTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				ArmorImage.sprite = inventoryScript.dictSprites ["KnightTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				ArmorImage.sprite = inventoryScript.dictSprites ["DragonTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}
		}

		if (ItemSlots.ArmorType.Arms == ArmorEnum) {
			
			switch (armorLevel) { //remeber the upper arms are switched right is left and vise versa
			
			case 1:
				ArmorImage.sprite = inventoryScript.dictSprites ["NoviceGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["WolfGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["RoyalGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				ArmorImage.sprite = inventoryScript.dictSprites ["KnightGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				ArmorImage.sprite = inventoryScript.dictSprites ["DragonGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			}
		}

		if (ItemSlots.ArmorType.Legs == ArmorEnum) {
			
			switch (armorLevel) {

			case 1:
				ArmorImage.sprite = inventoryScript.dictSprites ["NoviceBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["WolfBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				ArmorImage.sprite = inventoryScript.dictSprites ["KnightBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				ArmorImage.sprite = inventoryScript.dictSprites ["DragonBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			}
		}

		if (ItemSlots.ArmorType.Cape == ArmorEnum) {

			switch (armorLevel) {
			case 1:
				//ArmorImage.sprite = inventoryScript.dictSprites ["NoviceCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["WolfCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["RoyalCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 4:
				ArmorImage.sprite = inventoryScript.dictSprites ["KnightCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 5:
				ArmorImage.sprite = inventoryScript.dictSprites ["DragonCape"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}
		}

		if (ItemSlots.ArmorType.Charm == ArmorEnum) {

		//	NewItemImage.transform.localScale = new Vector3 (1f, 1f, 1f);

			switch (armorLevel) {
			case 1:
				ArmorImage.sprite = inventoryScript.dictSprites ["BronzeRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 2:
				ArmorImage.sprite = inventoryScript.dictSprites ["SilverRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 3:
				ArmorImage.sprite = inventoryScript.dictSprites ["GoldRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;

			}
	//		print ("cape itemsss");
		}

	}
		
}

