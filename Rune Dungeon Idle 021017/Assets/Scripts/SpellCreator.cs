﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpellCreator : MonoBehaviour {

	public List<int> sentSpell = new List<int>();
	public List<int> receivedSpell = new List<int>();

	public int fireCount;
	public int waterCount;
	public int windCount;
	public int leafCount;

	public int primarySpellType = -1;

	public GameObject player1;	
	public GameObject player2;

	public bool spellCompleted = false;

	// Use this for initialization
	void Start () {


	}

	public void sentSpellDesignatorPlayer1(){

		for (int i = 0; i< sentSpell.Count; i++) {

			switch (sentSpell[i]){
			
			case 0:
				windCount+=1;
				break;

			case 1:
				fireCount+=1;
				break;

			case 2:
				waterCount+=1;
				break;

			case 3:
				leafCount+=1;
				break;

			}
		}
		determineSpell(1);

	}

	public void sentSpellDesignatorPlayer2(){
		
		for (int i = 0; i< sentSpell.Count; i++) {
			
			switch (sentSpell[i]){
				
			case 0:
				windCount+=1;
				break;
				
			case 1:
				fireCount+=1;
				break;
				
			case 2:
				waterCount+=1;
				break;
				
			case 3:
				leafCount+=1;
				break;
			}
		}
		
		determineSpell(2);
	}

	public void recievedSpellDesignatorPlayer(int Player){

		for (int i = 0; i< receivedSpell.Count; i++) {
			
			switch (receivedSpell[i]){
				
			case 0:
				windCount+=1;
				break;
				
			case 1:
				fireCount+=1;
				break;
				
			case 2:
				waterCount+=1;
				break;
				
			case 3:
				leafCount+=1;
				break;
				
			}
		}

		if (Player == 1) {

			player1.GetComponent<TurnOnRagdoll> ().healthRecordable = true;

			determineSpell(2); //because player 1 is receiving a spell from player two... the last casted spell...

			resetAllCounts();
		}
		if (Player == 2) {

			player2.GetComponent<TurnOnRagdoll> ().healthRecordable = true;

			determineSpell(1);

			resetAllCounts();
		}

	}

	public void determineSpell(int Player){

		if (Player == 1 && player1.GetComponent<ControlPlayer1Character>().player1Stun==false) {

			if (fireCount == 3) {

				player1.GetComponent<ControlPlayer1Character> ().castFireSpell (); //this starts the fire spell
			}

			else if (fireCount == 4){

				player1.GetComponent<ControlPlayer1Character> ().castDragSpell (); //this starts the fire spell

			}

			else if(leafCount == 3){
				
				player1.GetComponent<ControlPlayer1Character> ().castRockThrowSpell (); //this starts the fire spell
			}

			else if(leafCount == 4){
				
				player1.GetComponent<ControlPlayer1Character> ().castTreantSpell (); //this starts the fire spell
			}
		
			else if(waterCount == 3){
				player1.GetComponent<ControlPlayer1Character> ().castWaterBeamSpell (); //this starts the fire spell
				
			}

			else if(waterCount== 4){

				player1.GetComponent<ControlPlayer1Character> ().castWaterBubble(); //this starts the fire spell

			}

			else if(windCount == 4){
				player1.GetComponent<ControlPlayer1Character> ().castInvisSpell (); //this starts the fire spell
				
			}

			else if(windCount == 3){
				player1.GetComponent<ControlPlayer1Character> ().castTornadoSpell(); //this starts the fire spell
				
			}

			else if(fireCount==1 && leafCount==1 && waterCount==1 && windCount==1){
				//player1.GetComponent<ControlPlayer1Character> ().castSpellShield(); //this starts the shield spell
				player1.GetComponent<ControlPlayer1Character> ().castDefaultAttack(); //this starts the fire spell

			}
			else {
			
				print("default atk spell p1");
				player1.GetComponent<ControlPlayer1Character> ().castDefaultAttack(); //this starts the fire spell

			}
		}

		if (Player == 2 && player2.GetComponent<ControlPlayer2Character>().player2Stun==false) {
			
			if (fireCount == 3) {
				
				player2.GetComponent<ControlPlayer2Character> ().castFireSpell (); //this starts the fire spell
			}
			
			else if (fireCount == 4){
				
				player2.GetComponent<ControlPlayer2Character> ().castDragSpell (); //this starts the fire spell
				
			}
			
			else if(leafCount ==3){
				
				player2.GetComponent<ControlPlayer2Character> ().castRockThrowSpell (); //this starts the fire spell
			}

			else if(leafCount ==4){
				
				player2.GetComponent<ControlPlayer2Character> ().castTreantSpell (); //this starts the fire spell
			}
			
			else if(waterCount ==3){
				player2.GetComponent<ControlPlayer2Character> ().castWaterBeamSpell (); //this starts the fire spell
				
			}
			
			else if(waterCount==4){
				
				player2.GetComponent<ControlPlayer2Character> ().castWaterBubble(); //this starts the fire spell
				
			}
	
			else if(windCount ==4){
				player2.GetComponent<ControlPlayer2Character> ().castInvisSpell (); //this starts the fire spell
				
			}

			else if(windCount ==3){
				player2.GetComponent<ControlPlayer2Character> ().castTornadoSpell(); //this starts the fire spell
				
			}

			else if(fireCount==1 && leafCount==1 && waterCount==1 && windCount==1){
			//	player2.GetComponent<ControlPlayer2Character> ().castSpellShield(); //this starts the fire spell
				player2.GetComponent<ControlPlayer2Character> ().castDefaultAttack(); //this starts the fire spell
			}
			else{

				print("default atk spell p2");
				player2.GetComponent<ControlPlayer2Character> ().castDefaultAttack(); //this starts the fire spell


			}

		}

		if (Player == 1 && player1.GetComponent<ControlPlayer1Character>().player1Stun==true) {

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==1){
			StartCoroutine("TakeTurnWhileStunned"); //this makes it so if the player is stunned, he can respawn
			}else{
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);
			}
		}

		if (Player == 2 && player2.GetComponent<ControlPlayer2Character>().player2Stun==true) {

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().playerInt==2){
			StartCoroutine("TakeTurnWhileStunned"); //this makes it so if the player is stunned, he can respawn
			}else{
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);

			}
					
		}
	}

	public void CounterAttackSpell(int createdByPlayer){
		print ("counter atk stopped early...");

		//not in Unity, this is for receiving a counter attack
		if (createdByPlayer == 2 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false) {
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
			
		}

		//not in Unity
		if (createdByPlayer == 1 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    && Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==false) {
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
			
		}

		//this is for the normal counter attack

		if (createdByPlayer == 2 
		    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true) {
			print ("counter atk working");
			player1.GetComponent<ControlPlayer1Character> ().castFireSpell (); //this starts the fire spell

		}

		if (createdByPlayer == 1 
		    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true) {
			print ("counter atk working");
			player2.GetComponent<ControlPlayer2Character> ().castFireSpell (); //this starts the fire spell

		}
		//end the normal counter attack

			//if both the players are invis
				//if in Unity
			if (createdByPlayer == 2 
			    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
		    	&& player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    	&& Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true) {

				Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);

			}
				

			if (createdByPlayer == 1 
			    && player1.GetComponent<ControlPlayer1Character>().player1Invis==true
			    && player2.GetComponent<ControlPlayer2Character>().player2Invis==true
		    	&& Camera.main.GetComponent<TurnedBasedGameOrganizer>().UnityMode==true) {
				
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(1,1);
				
			}			

			//end if both the players are invis

		if (createdByPlayer == 2 //this burns p2 after spell casted
		    && player2.GetComponent<ControlPlayer2Character>().player2FlameDOT==true
		    ) {
			print ("counter atk working");
			player2.GetComponent<ControlPlayer2Character> ().CreateFlameDOT(); //this creates the flame DOT on the player 
																				
		}

		if (createdByPlayer == 1 //this burns p1 after spell casted
		    && player1.GetComponent<ControlPlayer1Character>().player1FlameDOT==true
		    ) {
			print ("counter atk working");
			player1.GetComponent<ControlPlayer1Character> ().CreateFlameDOT (); //this creates the flame DOT on the player 
			
		}

		if (createdByPlayer == 2 //this closes shield after spell is complete
		    && player1.GetComponent<ControlPlayer1Character>().isShielded==true
		    ) {
			print ("shield closer working");
			player1.GetComponentInChildren<SphereShieldScript> ().closeShield=true; //this creates the flame DOT on the player 
			
		}

		if (createdByPlayer == 1 //this burns p1 after spell casted
		    && player2.GetComponent<ControlPlayer2Character>().isShielded==true
		    ) {
			print ("shield closer working");
			player2.GetComponentInChildren<SphereShieldScript> ().closeShield=true; //this creates the flame DOT on the player 
			
		}

	}


	public void primaryElement(){


		if (windCount >= 3) {
			primarySpellType = 0;
		} else

		if (fireCount >= 3) {
			primarySpellType = 1;
		} else

		if (waterCount >= 3) {
			primarySpellType = 2;
		} else

		if (leafCount >= 3) {
			primarySpellType = 3;
		} else {
			primarySpellType = 4;
		}

		resetAllCounts(); //makes sure there are no remaining counts after this spell is casted

	}

	public int getPrimaryElement{

		get { 

			primaryElement();


			return primarySpellType; }

	}

	public void resetAllCounts(){

		windCount = 0;
		waterCount = 0;
		fireCount = 0;
		leafCount = 0;

		sentSpell.Clear ();

		receivedSpell.Clear ();

	}

	IEnumerator TakeTurnWhileStunned(){
		
		while (true) {

			yield return new WaitForSeconds(3f);

			Camera.main.GetComponent<TurnedBasedGameOrganizer>().sortMeteorList();
			
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().TakeTurn();

			//Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);

			yield break;

		}

	}

	// Update is called once per frame
	void Update () {
	
	}

}
