﻿using UnityEngine;
using System.Collections;

public class ScreenShakerController : MonoBehaviour {


	public GameObject mainCamera;
	//public GameObject UIbackground;

//this script is solely used to simplify the code for sending the screen shaking
	void Start () {
	
	}

	public void activateScreenShake(float duration, float intensity){

		mainCamera.GetComponent<ScreenShake> ().ShakeAmount = intensity;

			mainCamera.GetComponent<ScreenShake> ().ShakeTimer = duration;

	//	UIbackground.GetComponent<ScreenShake> ().ShakeAmount = intensity;

	//		UIbackground.GetComponent<ScreenShake> ().ShakeTimer = duration;
	}

}
