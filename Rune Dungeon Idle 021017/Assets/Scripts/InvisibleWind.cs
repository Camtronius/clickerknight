﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InvisibleWind : MonoBehaviour {

	// This script is used to make the player fade away and set his status to invisible for 1 turn...

	public List<GameObject> ListOfBodyParts = new List<GameObject>();

	public GameObject SpellCreator; //this is for the isSpellComplete condition

	public int createdByPlayer = 0;

	public float timeAlive =0f;

	public bool hasCountered = false;

	void Start () {

		SpellCreator = GameObject.Find ("SpellListener");

	
		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().Head);
		if (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().Hair != null) {
			ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().Hair);
		}

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().Torso);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().UpperLeftArm);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LowerLeftArm);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LeftLegUpper);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LeftLegLower);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().UpperRightArm);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LowerRightArm);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().RightLegUpper);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().RightLegLower);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().RightHand);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LeftHand);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().LeftFoot);

		ListOfBodyParts.Add (this.gameObject.GetComponentInParent<TurnOnRagdoll> ().RightFoot);



		if (this.transform.parent.tag == "Player1" 
		    //this will be triggered when the player first starts the spell in ControlPlayer1 etc.
		    //fireball collider returns the player to visible after the counter attk spll is casted
		    && GetComponentInParent<ControlPlayer1Character>().player1Invis==false 
		    && GetComponentInParent<ControlPlayer1Character>().player1ReturnToVisible==false) { //set player to invis

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(2,0);
			}
			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
				Camera.main.GetComponent<TurnedBasedGameOrganizer>().CounterAtkRdy();

			}

			if(SpellCreator.GetComponent<SpellCreatorCampaign>()!=null){ //for campaign mode

				Camera.main.GetComponent<CampaignGameOrganizer>().CounterAtkRdy();

				SpellCreator.GetComponent<SpellCreatorCampaign>().CounterAttackSpell(createdByPlayer);
			}

			GetComponentInParent<ControlPlayer1Character>().player1Invis=true;
			GetComponentInParent<BoxCollider2D>().enabled=false;
		}
		
		if (this.transform.parent.tag == "Player2" 
		    //this will be triggered when the player first starts the spell in ControlPlayer2 etc.
		    //fireball collider returns the player to visible after the counter attk spll is casted
		    && GetComponentInParent<ControlPlayer2Character>().player2Invis==false
		    && GetComponentInParent<ControlPlayer2Character>().player2ReturnToVisible==false) { //set player to invis

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null){
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().invisManager(0,2);
			}

			if(SpellCreator.GetComponent<SpellCreator>()!=null){
			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
			Camera.main.GetComponent<TurnedBasedGameOrganizer>().CounterAtkRdy();
			}

			//p2 cant invis in capmaign.... yet...

			GetComponentInParent<ControlPlayer2Character>().player2Invis=true;
			GetComponentInParent<BoxCollider2D>().enabled=false;

			
		}

				if (this.transform.parent.tag == "Player1" && GetComponentInParent<ControlPlayer1Character>().player1ReturnToVisible==true) { //set player to invis
					
				for (int i=0; i<ListOfBodyParts.Count; i++) {

						ListOfBodyParts[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);
					}
				}
			
			
				if (this.transform.parent.tag == "Player2" && GetComponentInParent<ControlPlayer2Character>().player2ReturnToVisible==true) { //set player to invis
				
				for (int i=0; i<ListOfBodyParts.Count; i++) {

						ListOfBodyParts[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);

					}
					
				}

	}

	public void InvisSound(){


	}
	
	// Update is called once per frame
	void Update () {


		if(GetComponentInParent<ControlPlayer1Character>()!=null && GetComponentInParent<ControlPlayer1Character>().player1Invis==true
		   || GetComponentInParent<ControlPlayer2Character>()!=null && GetComponentInParent<ControlPlayer2Character>().player2Invis==true){

		for (int i=0; i<ListOfBodyParts.Count; i++) {

			if(ListOfBodyParts[i].GetComponent<SpriteRenderer>().color.a>0){

				ListOfBodyParts[i].GetComponent<SpriteRenderer>().color -= new Color(0,0,0,0.0075f);

				}


				if(Camera.main.GetComponent<TurnedBasedGameOrganizer>()!=null &&
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.activeSelf==true
				   && ListOfBodyParts[i].GetComponent<SpriteRenderer>().color.a<=0 
				   && Camera.main.GetComponent<TurnedBasedGameOrganizer>().nextTurnBool == false){ //this is so if someone recieves the spell it will unblock da runes since they are only unblocked after the ragdoll runs
					
					Camera.main.GetComponent<TurnedBasedGameOrganizer>().BlockClickMousePanel.SetActive(false);
					
					
				}

			}
	}


		//this doesnt work right now because the player doesnt start a new turn invis online, he starts as default
		if (GetComponentInParent<ControlPlayer1Character> () != null && GetComponentInParent<ControlPlayer1Character> ().player1ReturnToVisible == true
			|| GetComponentInParent<ControlPlayer2Character> () != null && GetComponentInParent<ControlPlayer2Character> ().player2ReturnToVisible == true) {

				if (GetComponentInParent<ControlPlayer1Character>()!=null && GetComponentInParent<ControlPlayer1Character>().player1Invis==true){
					GetComponentInParent<ControlPlayer1Character>().player1Invis=false;
				}
					if (GetComponentInParent<ControlPlayer2Character>()!=null && GetComponentInParent<ControlPlayer2Character>().player2Invis==true){
						GetComponentInParent<ControlPlayer2Character>().player2Invis=false;
					}

			for (int i=0; i<ListOfBodyParts.Count; i++) {
				
				if(ListOfBodyParts[i].GetComponent<SpriteRenderer>().color.a<1){
					
					ListOfBodyParts[i].GetComponent<SpriteRenderer>().color += new Color(0,0,0,0.0075f);

					if(ListOfBodyParts[i].GetComponent<SpriteRenderer>().color.a>0.98 && 
					   GetComponentInParent<ControlPlayer1Character> () != null && 
					   GetComponentInParent<ControlPlayer1Character> ().player1ReturnToVisible == true){

							GetComponentInParent<ControlPlayer1Character> ().player1ReturnToVisible = false;

					}

					if(ListOfBodyParts[i].GetComponent<SpriteRenderer>().color.a>0.98 && 
					   GetComponentInParent<ControlPlayer2Character> () != null && 
					   GetComponentInParent<ControlPlayer2Character> ().player2ReturnToVisible == true){

							GetComponentInParent<ControlPlayer2Character> ().player2ReturnToVisible = false;

					}

				}
				
			}

		}

	}
}
