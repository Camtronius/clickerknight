﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using System;

public class Inventory: MonoBehaviour {


	public List<ItemSlots> InventoryList = new List<ItemSlots> (); //itemSlots is a class by iteself. This is a list of the item slots class which will get serialized into an Xml file.
	public Dictionary<string, Sprite> dictSprites = new Dictionary<string, Sprite>();
	public List <Texture2D> CapeTextures = new List<Texture2D> (); //4 index list of the blocks that are selected

	public List <Sprite> RaritySprites = new List<Sprite> (); //4 index list of the blocks that are selected
	public Image RaritySpriteHolder;

	public List <TextMeshProUGUI> NewItemAttribList = new List<TextMeshProUGUI>();

	public TextMeshProUGUI sellPrice;
	public TextMeshProUGUI rerollPrice;

	public GameObject rerollObject;
	public GameObject rerollLock;

	public List <TextMeshProUGUI> OldItemAttribList = new List<TextMeshProUGUI>();
	public List <TextMeshProUGUI> TextTotalItemAttribList = new List<TextMeshProUGUI>();
	public List <TextMeshProUGUI> TextTotalItemAttribListValues = new List<TextMeshProUGUI>();


	//i think this one below will be images maybe?
	public List <Image> IncreaseOrDecrease = new List<Image>();
	public Sprite UpArrow;
	public Sprite DownArrow;
	public Sprite Blank128;

	public double itemPrice;
	public double itemRerollPrice;
	public bool reRollBool = true; //when true u can reroll. Can only reroll once per item

	public float itemTypeMultiplier;

	public List <int> through7Holder = new List<int>();
	public List <int> generatedAttributes = new List<int>();
	public List <double> listOfAttributes = new List<double>();
	public List <double> totalListOfAttributes = new List<double>();


	//0 HPmodGen =0;
	//1 ATKmodGen =0;
	//2 MAGmodGen =0;
	//3 GOLDmodGen =0;
	//4 EXPmodGen =0;
	//5 LSmodGen =0; //% lifesteal
	//6 CRmodGen =0; //increased magic cast speed
	//7 CRITmodGen =0; //increased critical Atk chance
	//add others like chance to cast a spell?

	public GameObject newItemObj; //setactive if a new item is found
	public TextMeshProUGUI itemAttributes;
	public TextMeshProUGUI itemRarityText;
	public GameObject rerollgoldIcon;

	public string itemNameString;
	public string itemRarity;
	public string itemRaritySuffix;


	public enum ArmorType {Helm,Chest,Arms,Legs,Cape,Charm};
	public ArmorType RandArmorEnum;
	public int armorLevel;
	public Image NewItemImage;
	public Color itemColor;
	public TextMeshProUGUI itemName;
	public EnemyStatsInheriter enemyStatsScript;
	//for item generation
	public float HPmodGen =0;
	public float ATKmodGen =0;
	public float MAGmodGen =0;
	public float GOLDmodGen =0;
	public float EXPmodGen =0;
	public float LSmodGen =0; //% lifesteal
	public float CRmodGen =0; //increased magic cast rate
	public float CRITmodGen =0; //increased critical Atk chance

	public ClothePlayerButton clothePlayerButton;


	public MainMenu cameraObj;

	void Awake(){

		loadDict (); //loads all the armor sprites

	}

	void OnEnable () {

		totalMods (); //gets the total attribute sum of all items
	}

	public void totalMods(){
		
		loadDict (); //loads all the sprites, this is usually called from other sources


		if (totalListOfAttributes.Count > 0) { //resets the list so u dont keep readding gold
			for (int h = 0; h < totalListOfAttributes.Count; h++) {

				totalListOfAttributes [h] = 0;
			}
		}


		for (int i = 0; i < InventoryList.Count; i++) {

			InventoryList [i].loadMods ();

			for (int j = 0; j < InventoryList [i].arrayOfMods.Length; j++) {
				//have to do doub.parse this since its a string array now
				if(InventoryList [i].arrayOfMods [j]!=null){

			//	print ("the item attribute value is this: " + double.Parse (InventoryList [i].arrayOfMods [j]));

				totalListOfAttributes [j] += double.Parse(InventoryList [i].arrayOfMods [j]); //this sums up the total attributes for the items
				
				}
			}


		}

		//for the total attributes of the character:

		TextTotalItemAttribList [0].text = "Total DMG "; //this ok
		TextTotalItemAttribList [1].text = "Total HP "; //this ok
		TextTotalItemAttribList [2].text = "Total Spell DMG "; //check total with talents/stats etc
		TextTotalItemAttribList [3].text = "Item HP "; //check total with talents/stats etc
		TextTotalItemAttribList [4].text = "Item Weapon DMG "; //check total with talents etc
		TextTotalItemAttribList [5].text = "Item Spell DMG "; //check total with talents/stats etc
		TextTotalItemAttribList [6].text = "Item Gold Bonus "; //check total with talents/stats etc
		TextTotalItemAttribList [7].text = "Item EXP Bonus "; //check total with talents/stats etc
		TextTotalItemAttribList [8].text = "Item Health Regen "; //check total with talents/stats etc
		TextTotalItemAttribList [9].text = "Item Spell Cast Rate "; //check total with talents/stats etc
		TextTotalItemAttribList [10].text = "CRIT % "; //check total with talents/stats etc

		//total dmg
		//TotalAtrib[0] is now a percent Date:030318
		TextTotalItemAttribListValues[0].text = LargeNumConverter.setDmg(cameraObj.playerObj.playerAtkDmg + cameraObj.playerObj.playerAtkDmg*(totalListOfAttributes [1]/100) + cameraObj.playerObj.playerAtkDmg * ((PlayerPrefs.GetFloat ("atkMod") / 100f)+cameraObj.damageIncreaseTalent1)).ToString() + " DMG";

		//tpotal hp
		//TotalAtrib[1] is still a flat value so no changes there. Leave as it Date:030318
		TextTotalItemAttribListValues [1].text = LargeNumConverter.setDmg(cameraObj.playerObj.playerHealthMax + (totalListOfAttributes [0])).ToString() + " HP";

		if (PlayerPrefs.GetString ("playerSplDmg")=="") { //adding this here because for some reason it needs to know this when determining the spell/weapon prices?

		//	print ("this is running at the beginning of the game in inventory");
            cameraObj.masterWeaponListScript.loadWeaponData();//loading this because this reference comes up empty if i dont
            PlayerPrefs.SetString ("playerSplDmg",((double)cameraObj.masterWeaponListScript.data[0]["SPELLDMG"]).ToString());

		}
		//total spl dmg
		TextTotalItemAttribListValues [2].text = LargeNumConverter.setDmg(double.Parse(PlayerPrefs.GetString ("playerSplDmg")) + (double.Parse(PlayerPrefs.GetString ("playerSplDmg"))*(totalListOfAttributes [2]/100f)) +  double.Parse(PlayerPrefs.GetString ("playerSplDmg"))*(PlayerPrefs.GetFloat ("luckMod")/100+cameraObj.playerObj.IncreasedSpellDmgTalent)).ToString() + " DMG";

		TextTotalItemAttribListValues [3].text = LargeNumConverter.setDmg(totalListOfAttributes [0]).ToString() + " HP";

		//item weap dmg
		TextTotalItemAttribListValues [4].text = LargeNumConverter.setDmg(totalListOfAttributes [1]).ToString() + " % DMG";

		TextTotalItemAttribListValues [5].text = LargeNumConverter.setDmg(totalListOfAttributes [2]).ToString() + " % DMG";

		TextTotalItemAttribListValues [6].text = LargeNumConverter.setDmg(totalListOfAttributes [3]) + " % Gold" ; 

		TextTotalItemAttribListValues [7].text = (totalListOfAttributes [4]).ToString() + " % EXP" ; 

		TextTotalItemAttribListValues [8].text = LargeNumConverter.setDmg((totalListOfAttributes [5])).ToString() + " HP/Sec" ; 


		//this is to correctly truncate caste rate values
			string timeString = (totalListOfAttributes[6] + cameraObj.playerObj.increasedSpellRateTalent).ToString (); 
			if(timeString.Length>4){
				timeString = timeString.Substring (0, 4); //10B
			}

		TextTotalItemAttribListValues [9].text = timeString + " Sec" ; //what about with the talen increase

		TextTotalItemAttribListValues [10].text = (totalListOfAttributes[7] + cameraObj.advCritEnabled).ToString() + " % CRIT" ; 

		//end total attributes of the char

		cameraObj.playerObj.quickHPUpdate(); //updates the players HP


	}

	public void loadDict(){
		//to load all the sprites we have available
		if (dictSprites.Count == 0) {
			Sprite[] sprites = Resources.LoadAll<Sprite> ("NewArmor");

			foreach (Sprite sprite in sprites) {
				dictSprites.Add (sprite.name, sprite);
			}
		}

	}

	public void through7refresh(){

		HPmodGen =0;
		ATKmodGen =0;
		MAGmodGen =0;
		GOLDmodGen =0;
		EXPmodGen =0;
		LSmodGen =0; //% lifesteal
		CRmodGen =0; //increased magic cast speed
		CRITmodGen =0; //increased critical Atk chance

		generatedAttributes.Clear (); //clears any previously generated item attributes
		through7Holder.Clear (); //clears the list because it had items taken from it previously. items need to be removed so it doesnt select them twice

		for (int i = 0; i < 8; i++) {

			through7Holder.Add (i); //makes it have the ints 0-7
		}

	}

	public void itemAttribSelect(){

		through7refresh (); //makes sure the through 7 list has 1-7 in it.

		int amountOfAttributes=0;
		int randomAttributeRoll = UnityEngine.Random.Range (0, 101);

		if (randomAttributeRoll >= 90) { //20% chance for 4 attributes = Epic Tier
			amountOfAttributes = 4;
			itemRarity = "<color=#8827F8FF>"; //</color> needs at the end
			itemRaritySuffix = "(Epic)";
			RaritySpriteHolder.sprite = RaritySprites [3];
		}
		if (randomAttributeRoll < 90 && randomAttributeRoll > 70) { //20% chance for 3 attributes = Rare Tier
			amountOfAttributes = 3;
			itemRarity = "<color=#1F63ECFF>"; //</color> needs at the end, this is blue
			itemRaritySuffix = "(Rare)";
			RaritySpriteHolder.sprite = RaritySprites [2];
		}
		if (randomAttributeRoll <= 70 && randomAttributeRoll > 35) { //30% chance for 2 attributes = Rare Tier
			amountOfAttributes = 2;
			itemRarity = "<color=#36FD40FF>"; //</color> needs at the end, this is green
			itemRaritySuffix = "(Uncommon)";
			RaritySpriteHolder.sprite = RaritySprites [1];
		}
		if (randomAttributeRoll <= 35 && randomAttributeRoll >= 0) { //30% chance for 1 attributes = Rare Tier
			amountOfAttributes = 1; //so it can be 1 or 2
			itemRarity = "<color=#7F603BFF>"; //</color> needs at the end
			itemRaritySuffix = "(Common)";
			RaritySpriteHolder.sprite = RaritySprites [0];
		}

		itemRarityText.text = itemRarity + itemRaritySuffix + "</color>";

		for (int i = 0; i < amountOfAttributes; i++) {		//picks a random amount of numbers 1 through 4 and 
			
			replaceGenAtribAndThru7 ();
		}
	}


	public void replaceGenAtribAndThru7(){

		//through7
		//generatedAttributes
		//run this method as many times as you want depending on how many attributes the item gets.
		int indexToAdd = UnityEngine.Random.Range (0, through7Holder.Count); //random index to take, each index corresponds to an attribute type from the item

			generatedAttributes.Add(through7Holder[indexToAdd]); //adds to gen attributes

			through7Holder.RemoveAt (indexToAdd); //removes from through7

	}

	public void GenerateNewItem(){
		//this resets the arrows

		if (enemyStatsScript == null) {

		//	print ("looking for stats script now");
			 
			enemyStatsScript = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ();

			if (enemyStatsScript == null) {

		//		print ("Couldnt find Stats Script");
			}

		} else {

		//	print ("there is a def for stats inheriter");

		}

		int currentLevel = PlayerPrefs.GetInt ("CurrentLevel");


		for (int j = 0; j < IncreaseOrDecrease.Count; j++) {
			IncreaseOrDecrease [j].sprite = Blank128;
		}

//		if (dictSprites.Count == 0) { //only import new sprites int othe dictionary if its empty.
//			Sprite[] sprites = Resources.LoadAll<Sprite> ("NewArmor");
//			print ("sprite counts are null!");
//			foreach (Sprite sprite in sprites) {
//		//		print (" adding this sprite : " + sprite);
//				dictSprites.Add (sprite.name, sprite);
//			}
//		}

		itemAttribSelect (); //gets which attributes the armor has (it will be a list in generatedAttributes of up to 4 #'s 1-7 non repeating.)

		generatedAttributes.Sort (); //i think this is the non repeating numbers



//		for (int i = 0; i < generatedAttributes.Count; i++) { //prints the gen attributes (for reference)
//
//			print (" The attribute generated at " + i + "is " + generatedAttributes[i]);
//		}

		for (int j = 0; j < listOfAttributes.Count; j++) { //resets the attributes to zero, this is where the attributes are stored??

			listOfAttributes [j] = 0;

		}

		//which type of item will be found?
		if (cameraObj.RingAvailable > 0) { //RandomArmorEnum is to designate the armortype ENUM (0,1,2etc)
			
			RandArmorEnum = (ArmorType)UnityEngine.Random.Range (0, 6); //if you can find the ring... //goes from 0 to 5

		} else { //ring is not available

			RandArmorEnum = (ArmorType)UnityEngine.Random.Range (0, 5); //if u cant find a ring... //goes from 0 to 4

		}
        //which sprite will it have


        if (RandArmorEnum != ArmorType.Charm) {

			//if its not a charm then do this, but helm and cape need to have the novice set removed because they dont have helms or capes
			if (RandArmorEnum == ArmorType.Helm) {
				armorLevel = UnityEngine.Random.Range (2, 6); //goes from 1 to 4 because we only have 4 pairs at the moment

			} else if (RandArmorEnum == ArmorType.Cape) {
				armorLevel = UnityEngine.Random.Range (2, 6); //goes from 1 to 4 because we only have 4 pairs at the moment

			} else {
				armorLevel = UnityEngine.Random.Range (1, 6); //goes from 1 to 4 because we only have 4 pairs at the moment

			}
	

		} else { //if the ring has been found

		//	armorLevel = Random.Range (1, 4); //goes from 1 to 3 for different sprites for the ring

			through7refresh (); //this generates a list of numbers 1-7 and clears the previous lists

			//this use as many attributes as the ring can hold
			int randomAttributeRoll = UnityEngine.Random.Range (1, cameraObj.RingAvailable+1); //this could be 0-1-2 depending on ring level

			armorLevel = randomAttributeRoll;

			switch(randomAttributeRoll){

			case 1:
				itemRarity = "<color=#36FD40FF>"; //</color> needs at the end, this is green
				itemRaritySuffix = "(Uncommon)";
				RaritySpriteHolder.sprite = RaritySprites [1];

				break;
			case 2:
				itemRarity = "<color=#1F63ECFF>"; //</color> needs at the end, this is blue
				itemRaritySuffix = "(Rare)";
				RaritySpriteHolder.sprite = RaritySprites [2];


				break;
			case 3:
				itemRarity = "<color=#8827F8FF>"; //</color> needs at the end
				itemRaritySuffix = "(Epic)";
				RaritySpriteHolder.sprite = RaritySprites [3];

				break;

            case 4:
                itemRarity = "<color=#8827F8FF>"; //</color> needs at the end
                itemRaritySuffix = "(Epic+)";
                RaritySpriteHolder.sprite = RaritySprites[3];

                break;
            }



			for (int i = 0; i <randomAttributeRoll; i++) {		//picks a random amount of numbers 1 through 4 and 

				replaceGenAtribAndThru7 ();
			}

		}
	//	print ("the random armor enum is " + RandArmorEnum.ToString () + " with an int value of " + ((int)RandArmorEnum).ToString ());
		//what name will it have// replace with this afer test (int)RandArmorEnum)
		itemNameString = enemyStatsScript.getItemName ((int)RandArmorEnum); //gets the item name depending on the type of armor it is, 0 is chest others havent been assigned
		itemName.text =  itemRarity + itemNameString + " " + itemRaritySuffix + "</color>"; //changes the color based on rarity

		calculateItemPrice ();

	//	print ("gen attribute count is :" + generatedAttributes.Count);
	//	print ("current level for item gen is: " + currentLevel); //lv the player is on
	//	print ("current level for item type multiplier gen is: " + itemTypeMultiplier);


		//this is in case the itrems generated need to know the curent level
		EnemyStatsInheriter enemyStatsObj = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter> ();

		//item type mods are now 1.75, 1.45, 1.15 from 5, 3 , 2

		//1.8 is the total for all the attributes added togethre. You ignore the 1 and just use what comes after the decimal. They are additive.

		//3.9 is 7.8/2

		//1.36 is the average attribute gained 5*(5+LV*1.36/10) = 91 max % gained from Atk/Mag/HP
		for (int i = 0; i < generatedAttributes.Count; i++) {

			switch (generatedAttributes [i]) { //chooses which attribute was generated

			case 0: //goes to +3450HP at level 50, but with a high variability (one is X5 and the other is X10). So on average they will have +2600HP

				HPmodGen = UnityEngine.Random.Range (Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)*(5+PlayerPrefs.GetInt("PrestigeLv"))), Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)* (10 + PlayerPrefs.GetInt("PrestigeLv")))); //this is goiving the max of 28%A hp at lv100
			
				listOfAttributes [0] = HPmodGen;

				break;

			case 1:

				// at prestige level 0, and world 50, the player is to find an item giving +207-219% damage increase

				ATKmodGen = UnityEngine.Random.Range (1+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)*(1+0.85f*PlayerPrefs.GetInt("PrestigeLv"))), 5+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)*(1+0.85f*PlayerPrefs.GetInt("PrestigeLv")))); //this is goiving the max of 28%A hp at lv100

				listOfAttributes [1] = ATKmodGen; //((cameraObj.playerObj.playerAtkDmg + (cameraObj.playerObj.playerAtkDmg * (PlayerPrefs.GetFloat ("atkMod") / 100f))) * (ATKmodGen/100f));

				break;

			case 2:

				// at prestige level 0, and world 50, the player is to find an item giving +207-219% damage increase

				MAGmodGen = UnityEngine.Random.Range (1+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)*(1+0.85f*PlayerPrefs.GetInt("PrestigeLv"))), 5+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)*(1+0.85f*PlayerPrefs.GetInt("PrestigeLv")))); //this is goiving the max of 28%A hp at lv100

				listOfAttributes [2] = MAGmodGen; // (double.Parse(PlayerPrefs.GetString ("playerSplDmg")) +  double.Parse(PlayerPrefs.GetString ("playerSplDmg"))*((PlayerPrefs.GetFloat ("luckMod"))/100f)) * (MAGmodGen/100f);

				break;
//////////
			case 3: 
				// at lv50 w/3 items it gives +9% extra gold/ monster. Gold is the driver, so you dont want to give too much.

				GOLDmodGen =  UnityEngine.Random.Range (40+ enemyStatsObj.CurrentLevel, 50 + enemyStatsObj.CurrentLevel); 

				GOLDmodGen = GOLDmodGen * itemTypeMultiplier / 55;

		//		print ("GOLD MOD GEN % IS: " + GOLDmodGen);

				listOfAttributes [3] = Mathf.CeilToInt(GOLDmodGen);// (enemyStatsObj.GetComponent<EnemyStatsInheriter> ().Enemy1Gold * (GOLDmodGen/100f));

				break;

			case 4:
				
				//at lv 50 w/ 3 items it gives +10.2% exp gain. Which isnt that broken b/c EXP isnt what drives the game. Gold is.

				EXPmodGen = UnityEngine.Random.Range (40+ enemyStatsObj.CurrentLevel, 50 + enemyStatsObj.CurrentLevel); 

				EXPmodGen = EXPmodGen * itemTypeMultiplier / 55;

			//	print ("EXP MOD GEN % IS: " + GOLDmodGen);

				listOfAttributes [4] = Mathf.CeilToInt(EXPmodGen);

				break;

			case 5:

				//assuming the player has 3 items with max HP regen, they will have about 240hp/sec at level 50

                float lifeRegenGrowth = 0.0125f / (2*Mathf.Exp(-enemyStatsObj.CurrentLevel / 75f)+1);

                    //print("regen growth " + lifeRegenGrowth.ToString());
                    //print("item typ mult " + itemTypeMultiplier.ToString());
                    //print("p max hp " + ((float)Camera.main.GetComponent<MainMenu>().playerObj.returnPlayerMaxHP()).ToString());



                    LSmodGen = UnityEngine.Random.Range (10+lifeRegenGrowth*itemTypeMultiplier*(float)Camera.main.GetComponent<MainMenu>().playerObj.returnPlayerMaxHP() ,15+ lifeRegenGrowth * itemTypeMultiplier * (float)Camera.main.GetComponent<MainMenu>().playerObj.returnPlayerMaxHP()); //0.03/(e^(-x/50)+1) multiply by max hp

			//	LSmodGen = LSmodGen * itemTypeMultiplier / 100; //this makes it go to +11% at lv100

			//	print ("LS MOD GEN % IS: " + LSmodGen);

				listOfAttributes [5] = LSmodGen;

				break;

			case 6: 
				
				//2.25 seconds is the max time you can remove with items from spell cast rate. 1.8x is the max combination of item multipler (the +10 and +40 are so it doesnt give really small numbers)

				CRmodGen = UnityEngine.Random.Range (10+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier)), 40+Mathf.CeilToInt(enemyStatsObj.CurrentLevel*(itemTypeMultiplier))); //this is goiving the max of 28%A hp at lv100

				listOfAttributes [6] = (CRmodGen)/400f; //dividing by 400 allows us to remove the fudging factor. so we can get a random range of decimals
		
				break;

			case 7: 

				CRITmodGen = UnityEngine.Random.Range (40+ enemyStatsObj.CurrentLevel, 50 + enemyStatsObj.CurrentLevel); 

				CRITmodGen = CRITmodGen * itemTypeMultiplier / 65; //this makes it go to +15% at lv100

				listOfAttributes [7] = Mathf.CeilToInt(CRITmodGen); //9 is a fudge number to make it so the crit rate doesnt go above 25%

				break;

			}

		}



		//THESE ARE THE TOTALS MAKE SURE IT MATCHES WHAT YOU HAVE ABOVE FOR THE TOTAL ITEM TOTALS...

		//this is new code from 121617
		totalMods(); //this is so we can load what the player has currently in terms of bonuses from stats and items

		for (int k = 0; k < totalListOfAttributes.Count; k++) { //OI think these are the damage totals

			if (totalListOfAttributes [k] >= 0 ) { //sets the text of all the attributes (this loop runs through them all) Notice they are not enabled...

				if (k == 0) { //if its the HP mod

					NewItemAttribList [0].text = LargeNumConverter.setDmg(cameraObj.playerObj.playerHealthMax + (totalListOfAttributes [0])).ToString() + " Total HP";

				}

				if (k == 1) { //if its the atk mod
		
					NewItemAttribList [1].text = "+ " +LargeNumConverter.setDmg(totalListOfAttributes [1]).ToString() + " % ATK DMG";
					//
				}

				if (k == 2) { //if its the mag  mod


					NewItemAttribList [2].text =  "+ " + LargeNumConverter.setDmg(totalListOfAttributes [2]).ToString() + " % Spell DMG";
					//

				}

				if (k == 3) { //if its the GOLD mod
			//		print (k + " is > 0 " + 0); //gold extra total

					NewItemAttribList [3].text = "+ " +LargeNumConverter.setDmg (totalListOfAttributes [3]).ToString() + " % Extra Gold";//extra gold / kill

				}

				if (k == 4) { //if its the HP mod
			//		print (k + " is > 0 " + 0);

					NewItemAttribList [4].text = "+ " + (totalListOfAttributes [4]).ToString() + " % Exp/Kill"; //this is okay dont need to change

				}

				if (k == 5) { //if its the HP mod
			//		print (k + " is > 0 " + 0);
				//	print ("players max HP is " + cameraObj.playerObj.playerHealthMax);
					NewItemAttribList [5].text =  "+ " +LargeNumConverter.setDmg((totalListOfAttributes [5])).ToString() + " HP/Sec" ;  //hp regen a sec

				}

				if (k == 6) { //if its the HP mod
			//		print (k + " is > 0 " + 0);
					string timeString = totalListOfAttributes[6].ToString (); 
					if(timeString.Length>4){
					timeString = timeString.Substring (0, 4); //10B
					}
					//***//create substring here
					NewItemAttribList [6].text = "+ " + timeString + " Sec" ;//spell rate 0.X/Sec
	
				}

				if (k == 7) { //if its the HP mod
			//		print (k + " is > 0 " + 0);

					NewItemAttribList [7].text = "+ " +(totalListOfAttributes[7]).ToString() + " % Crit Chance";

				}

				NewItemAttribList [k].gameObject.SetActive (true);// cause its greater than 0

			}



		}


		//for comparing to the old item
		for (int i = 0; i < InventoryList.Count; i++) {

			if ((int)RandArmorEnum == (int)InventoryList [i].ArmorEnum) { //chooses the specific gear type
			
			
				for (int k = 0; k < InventoryList [i].arrayOfMods.Length; k++) {


					if (InventoryList [i].arrayOfMods [k] == null) { //this is to protect a null value going through if listofattributes[k]>0
						InventoryList [i].arrayOfMods [k] = "0";
					}

					if (InventoryList [i].arrayOfMods [k]!=null && double.Parse(InventoryList [i].arrayOfMods [k]) > 0 || listOfAttributes [k] > 0) {

						if (k == 0) { //if its the HP mod

							double playerHealthMax = cameraObj.playerObj.playerHealthMax;

							double HpChange = ((listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])));

							OldItemAttribList [0].text = "<color=#A4D163FF>+ " + LargeNumConverter.setDmg(HpChange) + " HP</color>";// totalListOfAttributes [k];
						}

						if (k == 1) { //if its the atk mod

							//get the players attak

							double _PlayersDmgChange = ((listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])));

							OldItemAttribList [1].text = "<color=#A4D163FF>+ " + LargeNumConverter.setDmg(_PlayersDmgChange) + "% Atk DMG</color>";	
						}

						if (k == 2) { //if its the HP mod


							double SpellDamageChange = ((listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])));

							OldItemAttribList [2].text = "<color=#A4D163FF>+ " + LargeNumConverter.setDmg(SpellDamageChange) + "% Spell DMG</color>";
						}

						if (k == 3) { //if its the GOLD mod

							OldItemAttribList [3].text = "<color=#A4D163FF>+ " + LargeNumConverter.setDmg(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) +"% Extra Gold</color>";
						}

						if (k == 4) { //if its the HP mod

							OldItemAttribList [4].text = "<color=#A4D163FF>+ " + (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) +" % EXP/Kill</color>";
						}

						if (k == 5) { //if its the HP mod


							OldItemAttribList [5].text = "<color=#A4D163FF>+ " + LargeNumConverter.setDmg((listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k]))) + " HP/sec</color>" ;

						//	OldItemAttribList [5].text = "HP Regen: " + totalListOfAttributes [k];
						}

						if (k == 6) { //if its the HP mod

							string timeString = (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])).ToString (); 
							if(timeString.Length>4){
								timeString = timeString.Substring (0, 4); //10B
							}

							OldItemAttribList [6].text = "<color=#A4D163FF>+ " + timeString + " sec Spell Rate</color>";
						}

						if (k == 7) { //if its the HP mod

							OldItemAttribList [7].text =  "<color=#A4D163FF>+ " + (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) + "% Crit Chance </color>";
						}


					}else if (InventoryList [i].arrayOfMods [k] == null) { //if this attribute exists{
						//b/c this is a string now we can just check for "" instead of 0
				//	OldItemAttribList [k].text ="";
					}

					//for up or down arrows

					//listofattributes[k] is the new item attribute
					//inventorylist[i] is the item type and array of mods is its current mod

					if (InventoryList [i].arrayOfMods[k]!=null && (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) > 0) {

						for (int j = 0; j < IncreaseOrDecrease.Count; j++) { //goes through and gets the first arrow available and makes it up arrow, the same is true for downarrow. this should line up with the lists

							if (IncreaseOrDecrease [j].sprite == Blank128) {
								IncreaseOrDecrease [j].gameObject.SetActive (true);
								IncreaseOrDecrease [j].sprite = UpArrow;
								break;
							}
						}
						OldItemAttribList [k].gameObject.SetActive (true);
				//		OldItemAttribList [k].text = "+ " + (listOfAttributes[k] - InventoryList [i].arrayOfMods[k]).ToString()  + " %";

					} else if (InventoryList [i].arrayOfMods[k]!=null && (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) < 0){

						for (int j = 0; j < IncreaseOrDecrease.Count; j++) {

							if (IncreaseOrDecrease [j].sprite == Blank128) {
								IncreaseOrDecrease [j].gameObject.SetActive (true);
								IncreaseOrDecrease [j].sprite = DownArrow;
								break;
							}
						}

						if (k == 0) { //if its the HP mod

						//	float playerHealthMax = Camera.main.GetComponent<MainMenu> ().playerObj.playerHealthMax;

							double HpChange = Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k]));
							//

							OldItemAttribList [0].text =  "<color=#FA6C6BFF> - " + LargeNumConverter.setDmg(HpChange) + " HP</color>";// totalListOfAttributes [k];
						}

						if (k == 1) { //if its the atk mod

							double _PlayersDmgChange = Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k]));

							OldItemAttribList [1].text = "<color=#FA6C6BFF> - " + LargeNumConverter.setDmg(_PlayersDmgChange) + " % Atk DMG</color>";	
						}

						if (k == 2) { //if its the HP mod

							double SpellDamageChange = Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k]));


							OldItemAttribList [2].text = "<color=#FA6C6BFF> - " + LargeNumConverter.setDmg(SpellDamageChange) + " % Spell DMG</color>";					
						}

						if (k == 3) { //if its the GOLD mod

							OldItemAttribList [3].text = "<color=#FA6C6BFF> - " + LargeNumConverter.setDmg((Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])))) + " % Extra Gold</color>"; //gold
						}

						if (k == 4) { //if its the HP mod

							OldItemAttribList [4].text = "<color=#FA6C6BFF> - " + Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) + " % Exp/Kill</color>"; //exp
						}

						if (k == 5) { //if its the HP mod

							OldItemAttribList [5].text = "<color=#FA6C6BFF> - " + LargeNumConverter.setDmg((Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])))) + " HP/Sec</color>"; //hp regen
						}

						if (k == 6) { //if its the HP mod

							string timeString = (Math.Abs((listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])))).ToString (); 
							if(timeString.Length>4){
								timeString = timeString.Substring (0, 4); //10B
							}

							OldItemAttribList [6].text = "<color=#FA6C6BFF> - " + timeString  + " sec Spell Rate</color>"; //spell rate
						}

						if (k == 7) { //if its the HP mod

							OldItemAttribList [7].text = "<color=#FA6C6BFF> - " + Math.Abs(listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k])) + " % Crit Chance</color>"; //crit change
						}

					//	OldItemAttribList [k].text = " " + (listOfAttributes[k] - InventoryList [i].arrayOfMods[k]).ToString()  + " %";
						OldItemAttribList [k].gameObject.SetActive (true);


					} else if (InventoryList [i].arrayOfMods[k]!=null && (listOfAttributes[k] - double.Parse(InventoryList [i].arrayOfMods[k]))== 0) {

					//	IncreaseOrDecrease [k].sprite = Blank128;
						OldItemAttribList [k].gameObject.SetActive (false);// sprite = Blank128;
						NewItemAttribList [k].gameObject.SetActive (false);// sprite = Blank128;

						// ******* the item attributes need checking, but the arrows arent working. We need the increase % to show up arrows in order with which they appear, and everything after them to be nblank 128 arrows

					
					}


				}
			
			
			
			}
		}
		//for comparing to the old item



		if (Inventory.ArmorType.Helm == RandArmorEnum) {
	//		print ("helm itemsss");

			NewItemImage.transform.localScale = new Vector3 (0.9f, 0.9f, 1f);

			switch (armorLevel) {

			case 1:
				//NewItemImage.sprite = dictSprites ["NoviceHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["WolfHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["RoyalHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				NewItemImage.sprite = dictSprites ["KnightHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				NewItemImage.sprite = dictSprites ["DragonHelmFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
//			case 6: //for king
//				NewItemImage.sprite = dictSprites ["LV1_KingHead"]; //use this to set a sprite to a specific sprite in the dict.
//
//				break;
//			case 7: //for Wolf Armor
//				NewItemImage.sprite = dictSprites ["LV1_WolfHead"]; //use this to set a sprite to a specific sprite in the dict.
//
//				break;
//			case 8: //for Wolf Armor
//				NewItemImage.sprite = dictSprites ["LV1_PurpHead"]; //use this to set a sprite to a specific sprite in the dict.
//
//				break;
//			case 9: //for Invo Armor
//				NewItemImage.sprite = dictSprites ["LV1_InvoHead"]; //use this to set a sprite to a specific sprite in the dict.
//
//				break;
			}


		}

		if (Inventory.ArmorType.Chest == RandArmorEnum) {
	//		print ("chest itemsss");

			NewItemImage.transform.localScale = new Vector3 (1f, 1f, 1f);

			switch (armorLevel) {

			case 1:
				NewItemImage.sprite = dictSprites ["NoviceTorsoFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["RoyalTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				NewItemImage.sprite = dictSprites ["KnightTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				NewItemImage.sprite = dictSprites ["DragonTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
//			case 6: //for king
//				NewItemImage.sprite = dictSprites ["LV1_KingTorso"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 7: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 8: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_PurpTorso"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 9: //for invo
//				NewItemImage.sprite = dictSprites ["LV1_InvoTorso"]; //use this to set a sprite to a specific sprite in the dict.
//				break;

			}
		}

		if (Inventory.ArmorType.Arms == RandArmorEnum) {
	//		print ("arms itemsss");

			NewItemImage.transform.localScale = new Vector3 (0.7f, 0.7f, 1f);

			switch (armorLevel) { //remeber the upper arms are switched right is left and vise versa

			case 1:
				NewItemImage.sprite = dictSprites ["NoviceGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["WolfGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["RoyalGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				NewItemImage.sprite = dictSprites ["KnightGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				NewItemImage.sprite = dictSprites ["DragonGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
//			case 6: //for king
//				NewItemImage.sprite = dictSprites ["LV1_KingUpperRightArm"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 7: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_WolfUpperRightArm"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 8: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_PurpUpperRightArm"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 9: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_InvoUpperRightArm"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
			}
		}

		if (Inventory.ArmorType.Legs == RandArmorEnum) {
		//	print ("legs itemsss");

			NewItemImage.transform.localScale = new Vector3 (1f, 1f, 1f);

			switch (armorLevel) {

			case 1:
				NewItemImage.sprite = dictSprites ["NoviceBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["WolfBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				NewItemImage.sprite = dictSprites ["KnightBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				NewItemImage.sprite = dictSprites ["DragonBoots"]; //use this to set a sprite to a specific sprite in the dict.
				break;
//			case 6: //for king
//				NewItemImage.sprite = dictSprites ["LV1_KingLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 7: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_WolfLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 8: //for wolf
//				NewItemImage.sprite = dictSprites ["LV1_PurpLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
//			case 9: //for invo
//				NewItemImage.sprite = dictSprites ["LV1_InvoUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				break;
			}
		}

		if (Inventory.ArmorType.Cape == RandArmorEnum) {

			NewItemImage.transform.localScale = new Vector3 (1f, 1f, 1f);

			switch (armorLevel) {
			case 1:
			//	NewItemImage.sprite = dictSprites ["NoviceCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["WolfCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["RoyalCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 4:
				NewItemImage.sprite = dictSprites ["KnightCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 5:
				NewItemImage.sprite = dictSprites ["DragonCape"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
//			case 6:
//				NewItemImage.sprite = dictSprites ["KingCape 1"]; //use this to set a sprite to a specific sprite in the dict.				//	print ("the player should be wearing the leaf cape!!");
//				break;
//			case 7://for king
//				NewItemImage.sprite = dictSprites ["WolfCape 1"]; //use this to set a sprite to a specific sprite in the dict.				//		print ("the player should be wearing the king cape!!");
//				break;
//			case 8://for king
//				NewItemImage.sprite = dictSprites ["PurpCape 1"]; //use this to set a sprite to a specific sprite in the dict.				//		print ("the player should be wearing the king cape!!");
//				break;
//			case 9://for king
//				NewItemImage.sprite = dictSprites ["InvoCape 1"]; //use this to set a sprite to a specific sprite in the dict.				//		print ("the player should be wearing the king cape!!");
//				break;

			}
	//	print ("cape itemsss");
		}

		if (Inventory.ArmorType.Charm == RandArmorEnum) {

			NewItemImage.transform.localScale = new Vector3 (1f, 1f, 1f);

			switch (armorLevel) {
			case 1:
				NewItemImage.sprite = dictSprites ["BronzeRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 2:
				NewItemImage.sprite = dictSprites ["SilverRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;
			case 3:
				NewItemImage.sprite = dictSprites ["GoldRing"]; //use this to set a sprite to a specific sprite in the dict.				
				break;

			}
	//		print ("RING itemsss");
		}

	//	NewItemImage.color= itemColor; //sets the color of the sprite based on what was randomized

		if (Camera.main.GetComponent<MainMenu> ().itemRateIncrease <= 0) { //check if we should show the price to reroll and the gold icon
			rerollPrice.text = "";
			rerollgoldIcon.SetActive (false);
			rerollObject.GetComponent<Button> ().interactable = false;
			rerollLock.SetActive (true);

		} else {
			rerollgoldIcon.SetActive (true);
			//price text is available and not altered 
			//lock icon not changed because its not sure if a reroll has been rolled or not

		}

		reRollReady (); //checks if u land the reroll chance
	}

	public void reRollReady(){

	//	uncomment everything after new items have been applied and are testing and working 112117 CR

		int randomRoll = UnityEngine.Random.Range (1, 100);

		if (Camera.main.GetComponent<MainMenu> ().itemRateIncrease >= randomRoll && reRollBool == true) {

		rerollObject.GetComponent<Button> ().interactable = true;
		rerollLock.SetActive (false);

			reRollBool = false;
		} else { //cannot reroll again
			rerollPrice.text = "";
			rerollObject.GetComponent<Button> ().interactable = false;	
			rerollLock.SetActive (true);

		}


	}

	public void SaveItem(){

		for (int i = 0; i < InventoryList.Count; i++) {

			if((int)RandArmorEnum == (int)InventoryList[i].ArmorEnum){ //chooses the specific gear type

				InventoryList [i].armorLevel = armorLevel; //gives the generated gear it should be

				InventoryList [i].itemName = itemNameString;
				//gives the mods the item should have

				if (InventoryList [i].arrayOfMods.Length == 0) {

			//		print("length of this array is 0");

				}

				InventoryList [i].arrayOfMods [0] = listOfAttributes [0].ToString();// HPmodGen;
				InventoryList[i].arrayOfMods [1] = listOfAttributes [1].ToString();//ATKmodGen;
				InventoryList[i].arrayOfMods [2] = listOfAttributes [2].ToString();//MAGmodGen;
				InventoryList[i].arrayOfMods [3] = listOfAttributes [3].ToString();//GOLDmodGen;
				InventoryList[i].arrayOfMods [4] = listOfAttributes [4].ToString();//EXPmodGen;
				InventoryList[i].arrayOfMods [5] = listOfAttributes [5].ToString();//LSmodGen;
				InventoryList[i].arrayOfMods [6] = listOfAttributes [6].ToString();//CRmodGen;
				InventoryList[i].arrayOfMods [7] = listOfAttributes [7].ToString();//CRITmodGen;


				InventoryList [i].saveMods ();
				InventoryList [i].loadMods ();
			}


		}

		totalMods (); //this just updates the players item list with the new stats from the item

		cameraObj.CloseItemChest ();

		reRollBool = true; //this is so you can reroll the next item you get

		clothePlayerButton.putOnClothes ();
		clothePlayerButton.refreshAnimatedPlayerClothes ();

		cameraObj.persistantSoundManager.GetComponent<SoundManager> ().equipItem ();

		if (cameraObj.TutorialObject.equipTap == true) {

			cameraObj.TutorialObject.refreshAllVariables ();//this will close the tutorial and reset everything
		}

	}

	public void calculateItemPrice(){

		switch ((int)RandArmorEnum) {

		case 0: //helm
			itemTypeMultiplier = 1.45f;
		break;

		case 1://chest
			itemTypeMultiplier = 1.75f;

		break;
		
		case 2://arms
			itemTypeMultiplier = 1.15f;

		break;
		
		case 3://legs
			itemTypeMultiplier = 1.15f;

		break;
		
		case 4: //cape
			itemTypeMultiplier = 1.15f;

		break;

		case 5: //RING/CHARM/NECKLACE
			itemTypeMultiplier = 1.15f;

			break;



		}

	//	MainMenu cameraObj = Camera.main.GetComponent<MainMenu> ();
	//	EnemyStatsInheriter enemyStatsObj = GameObject.Find ("EnemyStatsInheriter").GetComponent<EnemyStatsInheriter>();

		int amountOfAttributes = 0; //work around because i kept getting cannot cast from source to dest type error for genattributes.count

		for (int i = 0; i < generatedAttributes.Count; i++) { 

			amountOfAttributes += 1;

		}

		double goldFromEnemy = enemyStatsScript.Enemy1Gold;

	//	print ("enemy gold is :" + goldFromEnemy + " attrib amt is : " + amountOfAttributes + "item type multipler is : " + itemTypeMultiplier);
		//item price is = current amount of gold dropped * 3, for max gold dropped from enemy * the amount of attributes contained in item * the type of item it is.
	//	print("sell price before prestige is " + (goldFromEnemy*generatedAttributes.Count*itemTypeMultiplier*1.5f).ToString() + "after prestige is " + (goldFromEnemy*generatedAttributes.Count*itemTypeMultiplier*1.5f*(1+cameraObj.sellPricePrestige)).ToString());
		itemPrice = goldFromEnemy*generatedAttributes.Count*itemTypeMultiplier*1.5f*(1+cameraObj.sellPricePrestige); 

		//OLD 2.05 attribute avg, 2.8 item type mod avg. total gold avg is 4.85 coins per sell
		//New 030418: added the x2 since the item type multipliers have been cut into about half. This is to keep the selling price about the same. (.1 less now though which is probably needed)


		itemRerollPrice = goldFromEnemy * 3;

		sellPrice.text = LargeNumConverter.setDmg(itemPrice).ToString ();

		rerollPrice.text = LargeNumConverter.setDmg(itemRerollPrice).ToString ();

	}

	public void sellItem(){

	//	print ("item sold!");
		Camera.main.GetComponent<MainMenu> ().playerObj.playerGold += itemPrice;
		Camera.main.GetComponent<MainMenu> ().CloseItemChest ();

		reRollBool = true; //this is so you can reroll the next item you get

		cameraObj.persistantSoundManager.GetComponent<SoundManager> ().sellItem ();

		if (cameraObj.TutorialObject.equipTap == true) {

			cameraObj.TutorialObject.refreshAllVariables ();//this will close the tutorial and reset everything
		}

	}

	public void rerollItem(){

		if (Camera.main.GetComponent<MainMenu> ().playerObj.playerGold >= itemRerollPrice) {
		//	print ("item rerolld!");
			Camera.main.GetComponent<MainMenu> ().playerObj.playerGold -= itemRerollPrice;

			Camera.main.GetComponent<MainMenu> ().newItemWindow.SetActive (false); //opens the item gen window (will have opening animation eventually)


			Camera.main.GetComponent<MainMenu> ().OpenItemChest ();

			Camera.main.GetComponent<MainMenu> ().persistantSoundManager.GetComponent<SoundManager> ().rerollSound ();
		}
	}



	//test of modifying the sprites color

}
