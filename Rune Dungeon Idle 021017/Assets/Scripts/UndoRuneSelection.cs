﻿using UnityEngine;
using System.Collections;

public class UndoRuneSelection : MonoBehaviour {

	public GameObject TurnBasedOrganizer;

	// Use this for initialization
	void Start () {
	
	}

	public void undoRunes(){

		if (TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> () != null) {
			if (TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().selectedBlocksCount <= 3) {

				for (int i=0; i<TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer>().selectedBlocks.Count; i++) {

					TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().selectedBlocks [i].GetComponent<MeteorCode> ().isSelected = false;

				}

				TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().selectedBlocks.Clear ();
				TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().selectedBlocksCount = 0;
				TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().PersistantSoundManager.undoSelection();

				//TurnBasedOrganizer.GetComponent<TurnedBasedGameOrganizer> ().
			}
		}

		if (TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> () != null) {
			if (TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> ().selectedBlocksCount <= 3) {
				
				for (int i=0; i<TurnBasedOrganizer.GetComponent<CampaignGameOrganizer>().selectedBlocks.Count; i++) {
					
					TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> ().selectedBlocks [i].GetComponent<MeteorCode> ().isSelected = false;
					
				}
				
				TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> ().selectedBlocks.Clear ();
				TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> ().selectedBlocksCount = 0;
				TurnBasedOrganizer.GetComponent<CampaignGameOrganizer> ().PersistantSoundmanager.undoSelection();
			}
		}
	}
}
