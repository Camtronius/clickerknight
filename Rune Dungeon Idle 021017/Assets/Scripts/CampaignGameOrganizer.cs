﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms;
using System.Linq;
using UnityEngine.Analytics;
using TMPro;

public class CampaignGameOrganizer : MonoBehaviour {

	public bool UnityMode = false;
	public bool linearRuneSelection = false;
	
	//~~Networking declarations
//	private TurnBasedMatch mMatch = null;
	private MatchDataRunes mMatchData = null; //remember to set this back to null when done    new MatchDataRunes();

	public GameObject netDictionaryObj;

	const int MinOpponents = 1; //makes it so you can only invite one person max
	const int MaxOpponents = 1; //makes it so you can only invite one person max
	const int Variant = 0;  // default
	private bool mWaitingForAuth = false;

	public GameObject SpellGuideWindow;//shows whats happening in the game
		public int pressCounter = 0; //for opening and closing the window
		private string playerName;

//	public Text healthRecorded; //shows the player iD
	public GameObject PlayerDeadPanel;
	public GameObject WhosTurnPanel;
	public GameObject WhoWon;
	public GameObject EndMatchPanel;
	public GameObject BlockClickMousePanel; //this makes it so you cant click during the enemy turn

	public GameObject enemyDamageAmount;
	public GameObject enemyCritNotification;
	public GameObject newItemBurst;
	public GameObject enemyHealthBar;

	//public Text enemyDamageAmountText;
	public GameObject playerDamageAmount;
	public TextMeshProUGUI playerDamageAmountText;


	public GameObject plusExp;
	public GameObject levelUpObj;
	public GameObject addGoldObj;
	public TextMeshProUGUI goldText;
	public GameObject addHPObj;

	public GameObject addHPFinaBossObj;

	public GameObject addHPText;
	public GameObject addHPFinalBossText;

	public GameObject effectivenessObj;
	public GameObject itemObj;
	public GameObject gemExplosion;
	public GameObject HealingObj;
	public GameObject LevelUpRays;
	public GameObject LifeStealObj;

	public GameObject CountDownTimer;



	public float player1InvisValue=0;
	public float player2InvisValue=0;

	public int playerInt = 1; //int represents which players turn it is, 1 = player 1, 2 = player 2, 
	//~~NON - networking declarations

	public List <Vector2> touchList = new List<Vector2>(); //has a list of all the available meteors on the grid
	public List <GameObject> selectedBlocks = new List<GameObject> (); //4 index list of the blocks that are selected
	public List <GameObject> sentBlocks = new List<GameObject> (); //4 index list of the blocks that are selected
	public List <float> swordEnch = new List<float> (); //4 index list of the blocks that are selected

	public GameObject SpellListener;

	public GameObject LastSpellPlayer1;//the UI element of the last spell casted.
	public GameObject LastSpellPlayer2;
		public Sprite WindSprite; //this is so the last spell casted can be changed to this...
		public Sprite FireSprite;
		public Sprite WaterSprite;
		public Sprite EarthSprite;
		public Sprite nullsprite;
	public GameObject travelingRuneObject; //this is the rune that is instantiated and flies to the rune that it will replaced.
	public GameObject runeEnergy;
	public GameObject runeExplosion;

	//for meteor spawning//
	public GameObject TestMeteor; //meteor object which gets spawned
	public GameObject GhostMeteor; //meteor object which gets spawned
	public GameObject InvisParticles; //this is for making the characters invis if they are already invisible.

	public TutorialCampaign TutorialScript;

	public int ColumnLength; //number of columns for spawning blocks
	public int RowHeight; //number or rows for spawning blocks

	public List<MeteorCode> meteorList = new List<MeteorCode>();
	public List<float> copiedPlayerMeteorList = new List<float>();
	public List<GameObject> ghostMeteorList = new List<GameObject>();
	//public List<GameObject> travelingRunePool = new List<GameObject>();

	//for meteor spawning//

	public int	selectedBlocksCount =0; //to show how many blocks have been selected
	public bool mouseDown = false; //is mouse down or not bool
	//private GameObject Canvas; 
	public GameObject CanvasBehindPlayer;
	
	RaycastHit2D hit; //for detecting if a meteor is selcted
	public LayerMask myLayerMask;

	public GameObject player1;
//	public GameObject player2;

	public bool nextTurnBool = false;
	public bool LevelUpBool = false;
	//public bool gameOver = false; //may not be needed if we just check if a player is dead instead

	//SFX
	public SoundManager PersistantSoundmanager;

	void Start () {

		if (Application.platform == RuntimePlatform.WindowsEditor){
			StartCoroutine ("ClickDetector"); //detects if a block is being selected
			StartCoroutine ("TravelRuneRoutine");
		}

		if (Application.platform == RuntimePlatform.Android){
			StartCoroutine ("ClickDetectorAndroid"); //detects if a block is being selected
			StartCoroutine ("TravelRuneRoutine");

		}

	//		GooglePlayGames.PlayGamesPlatform.Activate(); // authenticate user for GPGS(Google play game services):

		//to spawn meteors when not online ***

	if (UnityMode == true) {

			mMatchData =  new MatchDataRunes();

		//	SetupObjects (true); //*** remove this when finished with offline mode test
		}

	
		PersistantSoundmanager = GameObject.Find ("PersistentSoundManager").GetComponent<SoundManager> ();

		Camera.main.GetComponent<MainMenu> ().persistantSoundManager = PersistantSoundmanager.gameObject; //this is so it wont be confused when playing sounds.

	//	PersistantSoundmanager.SoundCheck ();

		musicSelector ();

	}

	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			mouseDown=true;
		}
		
		if (Input.GetMouseButtonUp (0)) {
			mouseDown=false;
			
			touchList.Clear();
		}
	}

	public void playSelectionSound(){


	//	PersistantSoundmanager.soundEffect.pitch = 0.5f + selectedBlocks.Count/4f;
		
			if (selectedBlocks.Count == 0) {
			PersistantSoundmanager.runeSelection ();
		}
		//persistantsoundmanaer.
		//45 - rune select
		//46 rune undo

	}

	IEnumerator ClickDetector(){
		
		while (true) {
	
			//this is to make sure the touches are blocked when running in the unity editor

			if(mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && selectedBlocksCount<4){
				
				Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //gets the mouse screen coordinates 


						Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
						RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, myLayerMask);  //shhots the ray in that direction and detects whatever was hit

					if(hit.collider!=null && hit.collider.GetComponent<MeteorCode>()!=null && linearRuneSelection==true && hit.collider.GetComponent<MeteorCode>().isSelectable==true){ //this is if u want to try it with the linear rune selection
							//	print (hit.collider); 
							//this is the code to allow only adajacent blocks to be selected

							//play sound thing here for selection:
						
					if(hit.collider.GetComponent<MeteorCode>().isSelected==false && hit.collider.GetComponent<MeteorCode>().isSelectable==true){
					playSelectionSound();
					}

							for(int i = 0; i<meteorList.Count; i++){

								if(meteorList[i].GetComponent<MeteorCode>().isSelected == true
								   && meteorList[i].GetComponent<MeteorCode>().Neighbors.Contains(hit.collider.gameObject)
								   || selectedBlocksCount==0){

									if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
									   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
									   ){
										
										hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
										
										selectedBlocks.Add(hit.collider.gameObject);
										selectedBlocksCount+=1; //counts how many blocks have been selected
									}

								}

							}

						}

					if(hit.collider!=null && linearRuneSelection==false){ //this is for testing purposes where u can select any rune

								if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
								   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
								   ){
									
									hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
									
									selectedBlocks.Add(hit.collider.gameObject);
									selectedBlocksCount+=1; //counts how many blocks have been selected
								}
					}

				
				
			}


			
			yield return new WaitForSeconds(0.01f);
			
		}
	}

	IEnumerator TravelRuneRoutine(){
		
		while (true) {

			if(selectedBlocksCount>=4){
				
				nextTurnBool = true; //this is to check if 4 blocks has been taken or not, so tht they can recieve a spell without taking a turn

				if(CountDownTimer.activeSelf==true && playerInt==1){
					
					CountDownTimer.SetActive(false);//sets the countdown to inactive if its one and yo uselected 4 runes
				}

				PersistantSoundmanager.runeTravel();

				for(int i =0; i<4; i++){
					
					if(selectedBlocks[i]!=null){
						
						mMatchData.mRuneTypes.Add(selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this is the code to make it work online
						
						SpellListener.GetComponent<SpellCreatorCampaign>().sentSpell.Add((int)selectedBlocks[i].gameObject.GetComponent<MeteorCode>().runeIntDesignation); //this adds to the sent spell list
						//so the spell listener can decide which spell it is that was sent.
						
						GameObject RuneBreaking = (GameObject)Instantiate (runeExplosion, new Vector3 (selectedBlocks[i].transform.position.x,selectedBlocks[i].transform.position.y, 0), Quaternion.identity); //instantiates the beam offscreen
						
						selectedBlocks[i].GetComponent<SpriteRenderer>().sprite = null; // makes the sprite invis 
						
						GameObject TravelRune = (GameObject)Instantiate (travelingRuneObject, new Vector3 (sentBlocks[i].transform.position.x,sentBlocks[i].transform.position.y, 0), Quaternion.identity); //instantiates the beam offscreen
						
						TravelRune.GetComponent<TravelingRune>().runeIndex = i;
						
						TravelRune.GetComponent<TravelingRune>().setSprite(sentBlocks[i].gameObject.GetComponent<Image>().sprite); //sets the sprite to the sent sprite rune
						
						TravelRune.GetComponent<TravelingRune>().setEndPosition (selectedBlocks[i].transform.position);
						
						sentBlocks[i].GetComponent<Image>().sprite = nullsprite; // makes the sprite invis 

						yield return new WaitForSeconds(0.25f);

					}
				}
				
				if(playerInt==1){
					SpellListener.GetComponent<SpellCreatorCampaign>().sentSpellDesignatorPlayer1(); //calls through the list and determines what spell you are casting

					SpellListener.GetComponent<SpellCreatorCampaign>().sentByPlayer1=true;
					
						mMatchData.lastSpellTypeCasted[0] = SpellListener.GetComponent<SpellCreatorCampaign>().getPrimaryElement;
//					print ("primary play 1 element is " + mMatchData.lastSpellTypeCasted[0]);

				}
				if(playerInt==2){
					SpellListener.GetComponent<SpellCreatorCampaign>().sentSpellDesignatorPlayer2(); //calls through the list and determines what spell you are casting

					SpellListener.GetComponent<SpellCreatorCampaign>().sentByPlayer1=false;

					mMatchData.lastSpellTypeCasted[1] = SpellListener.GetComponent<SpellCreatorCampaign>().getPrimaryElement;
		//			print ("primary play 1 element is " + mMatchData.lastSpellTypeCasted[1]);
					
				}
				
				selectedBlocksCount=0;
				//	selectedBlocks.Clear (); this used to be here, but the traveling runes made this wierd when coloring new runes
				setPlayersLastSpell();


				if(BlockClickMousePanel.activeSelf==false){
					BlockClickMousePanel.SetActive(true); //if the block mouse pane is already up
				}
			}

			yield return new WaitForSeconds(0.1f);

		}

	}



	//______________________________________Android Routines Only


	IEnumerator ClickDetectorAndroid(){
		
		while (true) {
			
			Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //gets the mouse screen coordinates 

			//this is for detecting the touches on mobile and to make sure they are blocked by the UI

			if(mouseDown==true && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId) && selectedBlocksCount<4){

					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, myLayerMask);  //shhots the ray in that direction and detects whatever was hit
					
					if(hit.collider!=null && hit.collider.GetComponent<MeteorCode>()!=null && linearRuneSelection==true && hit.collider.GetComponent<MeteorCode>().isSelectable==true){ //this is if u want to try it with the linear rune selection
						//	print (hit.collider); 
						//this is the code to allow only adajacent blocks to be selected

					if(hit.collider.GetComponent<MeteorCode>().isSelected==false){
						playSelectionSound();
					}

						for(int i = 0; i<meteorList.Count; i++){
							
							if(meteorList[i].GetComponent<MeteorCode>().isSelected == true
							   && meteorList[i].GetComponent<MeteorCode>().Neighbors.Contains(hit.collider.gameObject)
							   || selectedBlocksCount==0){
								
								if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
								   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
								   ){
									
									hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
									
									selectedBlocks.Add(hit.collider.gameObject);
									selectedBlocksCount+=1; //counts how many blocks have been selected
								}
								
							}
							
						}
						
					}
					
					if(hit.collider!=null && linearRuneSelection==false){ //this is for testing purposes where u can select any rune
						
						if(hit.collider.gameObject.GetComponent<MeteorCode>()!=null 
						   && hit.collider.gameObject.GetComponent<MeteorCode>().isSelected == false 
						   ){
							
							hit.collider.gameObject.GetComponent<MeteorCode>().isSelected=true;
							
							selectedBlocks.Add(hit.collider.gameObject);
							selectedBlocksCount+=1; //counts how many blocks have been selected
						}
					}
	
			}

			yield return new WaitForSeconds(0.01f);
			
		}
	}

	public void fakeRuneListFill(){

		for (int i=0; i<selectedBlocks.Count; i++) {
			
			selectedBlocks [i].GetComponent<MeteorCode> ().isSelected = false;
			
		}
		
		selectedBlocks.Clear ();
		selectedBlocksCount = 0;
		PersistantSoundmanager.undoSelection();

		playerInt = 2;

			BlockClickMousePanel.SetActive(true);
			//take the turn for the enemy
			
			List<GameObject> NullList1 = null;
			List<GameObject> NullList2 = null;
			
			//	object[] parms = new object[2]{NullList1, NullList2};
			
//			StartCoroutine(GameObject.Find("Enemy").GetComponentInChildren<EnemyAI>().ChooseRunes(NullList1,NullList2));
			

	}

	public void colorOldBlocks (int i, Sprite runeSprite){ //gets the passed index of the for loop


		if (runeSprite == WindSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (0); 

		
				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;


	//		print ("WHITE DETECTED");
		}
		if (runeSprite == FireSprite) {

		
				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (1); 
		//	

				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;


//		print ("red DETECTED");
			
		}
		if (runeSprite == WaterSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (2); 


			selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;

	//			print ("blue DETECTED");

		}
		if (runeSprite == EarthSprite) {


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=false;


			selectedBlocks[i].GetComponent<MeteorCode> ().createRuneColor (3); 


				selectedBlocks[i].GetComponent<MeteorCode>().LockColor=true;

//			print ("grey DETECTED");
			
		}

		selectedBlocks[i].GetComponent<MeteorCode>().isSelected=false;

		sentBlocks [i].GetComponent<Image> ().sprite = nullsprite; //this makes the blocks invis after they are spawned

			
	}

	public void createNextBlocks (){

		if (mMatchData.RunesIncoming.Count == 0) { //this is the condition if the game is just starting...

			for (int i = 0; i<4; i++) {

				int RandColor = Random.Range (0, 4);

				switch (RandColor) {
					
				case 0:
					sentBlocks [i].GetComponent<Image> ().sprite = WindSprite; //its sent blocks because we used to use this list to track which blocks the player last sente
					break;													//to cast a spell
				case 1:
					sentBlocks [i].GetComponent<Image> ().sprite = FireSprite;					
					break;
				case 2:
					sentBlocks [i].GetComponent<Image> ().sprite = WaterSprite;	
					break;
				case 3:
					sentBlocks [i].GetComponent<Image> ().sprite = EarthSprite;	
					break;
				}

			}

		} 
		 //this will set the runes received as the runes
		if (mMatchData.RunesIncoming.Count > 0) {

			for (int i = 0; i<mMatchData.RunesIncoming.Count; i++) {
			
				switch ((int)mMatchData.RunesIncoming[i]) {
					
				case 0:
					sentBlocks [i].GetComponent<Image> ().sprite = WindSprite;
					break;
				case 1:
					sentBlocks [i].GetComponent<Image> ().sprite = FireSprite;					
					break;
				case 2:
					sentBlocks [i].GetComponent<Image> ().sprite = WaterSprite;	
					break;
				case 3:
					sentBlocks [i].GetComponent<Image> ().sprite = EarthSprite;	
					break;
				}
			}
		}

		mMatchData.RunesIncoming.Clear (); //clears the list to make sure there isnt any overlap between what we are sending...

		//this is where the next players runes are created...
		for (int i =0; i<4; i++) {

			int RandColor2 = Random.Range (0, 4); //this is for the runes to be sent
		
//			print ("the next incoming runes for the next player are :" + RandColor2);
		
			mMatchData.RunesIncoming.Add (RandColor2); //creates the incoming runes
		}
	}

	IEnumerator GhostMeteorList(){

		while (true) {
			
			yield return new WaitForSeconds(2f);
			
			for(int i=0; i<meteorList.Count; i++){
				
				GhostMeteor = Instantiate (GhostMeteor, new Vector3 (meteorList[i].transform.position.x, meteorList[i].transform.position.y, 0), Quaternion.identity) as GameObject;
				
				GhostMeteor.GetComponent<GhostMeteor>().ghostIndex = i;
				
				ghostMeteorList.Add(GhostMeteor);
				
				
				GhostMeteor.name = "Ghost Meteor" + i.ToString();
				
			}
//			return false;
		}
	}

	public void printIndex(MeteorCode MeteorObject){ //is this method ever used?
		
		print("the index of this game object is: " + meteorList.IndexOf(MeteorObject));	//begins searching at index 0	
		
		MeteorObject.isSelected = true;
		
	}
	
	public void spawnMeteorStart(){

		meteorList.Clear ();

		PersistantSoundmanager.RuneSpawnSound ();

			for (int i = 0; i<RowHeight; i++) {

				for (int j=0; j<ColumnLength; j++) {
				
					GameObject Meteor = Instantiate (TestMeteor, new Vector3 (j + j * .1f, i, 0), Quaternion.identity) as GameObject;
				
					meteorList.Add (Meteor.GetComponent<MeteorCode> ()); //adds the meteor to the list
				
					//	int currentIndex = meteorList.IndexOf(Meteor.GetComponent<MeteorCode>());
				
					Meteor.GetComponent<MeteorCode> ().columnSpawned = j;
				
					Meteor.name = Meteor + meteorList.IndexOf (Meteor.GetComponent<MeteorCode> ()).ToString ();
				
				}
			
			}

		
		if (TutorialScript.isTutorial == true && TutorialScript.EnemyStatsInheriterObj.CurrentLevel==1 ||
		    TutorialScript.isTutorial == true && TutorialScript.EnemyStatsInheriterObj.CurrentLevel==3 ) {

			for( int i=0; i<meteorList.Count; i++){

				copiedPlayerMeteorList.Add(meteorList[i].GetComponent<MeteorCode>().runeIntDesignation); //this copies the list or rather translates it to just meteors not scriptz

			}


			for(int i=0; i<copiedPlayerMeteorList.Count; i++){

				copiedPlayerMeteorList[i] = Random.Range(0,4); //randomly assigns new runes to replace existing runes

				if(copiedPlayerMeteorList[i] ==1){ //if a fire rune change to earth rune

					copiedPlayerMeteorList[i] = 3 ;

				}

				if(i==0 || i==2 || i==5 || i==12 || i==13 || i==15){ //if any of these runes change to fire rune for the tutorial

					copiedPlayerMeteorList[i]=1;
				}

			}

			ChangeBlockColor(); //change the block color to the copied player list. (only for level 1)

		}//end of tutorial is true
		
			
	}
	
	public void ChangeBlockColor(){
		
		if(copiedPlayerMeteorList.Count>0){	
			
			print("copied meteorlist count is: " + copiedPlayerMeteorList.Count);
			
			print("meteorlist count is: " + meteorList.Count);

			for(int k=0; k<meteorList.Count; k++){
				
//				print ("k index" + k);

				meteorList[k].LockColor=false;

				meteorList[k].createRuneColor((int)copiedPlayerMeteorList[k]);

				meteorList[k].LockColor=true;
				
			}
			
		}
		print ("block color changeds!");
	}

	public void RestoreRuneColor(){

		for(int k=0; k<meteorList.Count; k++){

			meteorList[k].LockColor=false;
			
			meteorList[k].isSelectable=true; //this is to reset the Burnt Runes condiditon of isSelectable = fallse

			meteorList[k].RestoreBurntRunes(); //restores any burnt runes back to normal
			
			meteorList[k].LockColor=true;
			
		}
	}
	
	public void spawnMeteor(int MeteorColumn, int spawnNumber){  //int MeteorIndexDestroyed
		
		GameObject Meteor =  Instantiate(TestMeteor,new Vector3(MeteorColumn + MeteorColumn*.1f ,4 ,0),Quaternion.identity) as GameObject ;
		Meteor.GetComponent<MeteorCode>().columnSpawned = MeteorColumn;
		
		meteorList.Add(Meteor.GetComponent<MeteorCode> ());
		Meteor.name = Meteor + meteorList.IndexOf(Meteor.GetComponent<MeteorCode>()).ToString();

		print ("block number being spawned is " + spawnNumber);

		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == WindSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (0); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("WHITE DETECTED");
		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == FireSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (1); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("red DETECTED");

		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == WaterSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (2); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("blue DETECTED");

		}
		if (sentBlocks [spawnNumber].GetComponent<Image> ().sprite == EarthSprite) {
			Meteor.GetComponent<MeteorCode> ().createRuneColor (3); 
			Meteor.GetComponent<MeteorCode>().LockColor=true;
			print ("grey DETECTED");

		}
		//of a take turn googleplay method

	}

	public void sortMeteorList(){

		meteorList = meteorList.OrderBy(x=>x.indexSpawned).ToList(); //this was originally in take turn but im guessing u cannot sort a list in the midd

	//	Debug.Log ("the count of the meteor list is: " + meteorList.Count); 

//		print ("sorting finished");
		
		for (int j =0; j<meteorList.Count; j++) {
			

	//		Debug.Log ("index spawned is: " + (int)meteorList[j].GetComponent<MeteorCode>().indexSpawned);
	//		Debug.Log ("runeintdesg spawned is: " + (float)meteorList[j].GetComponent<MeteorCode>().runeIntDesignation);

			mMatchData.Player1Blocks.Add((float)meteorList[j].GetComponent<MeteorCode>().runeIntDesignation);
			

		}
	}

	//--------------------------------------------------------this is where the network code is kept--------------------------------------------------------//
	/*
	void OnMatchStarted(bool success, TurnBasedMatch match) {

		if (success) {

		//	multiplayerGameMenu.SetActive(false);
		
			mMatch = match;  //REMEMBER TO SET BACK TO UNCOMMENT WHEN done

			//	networkText.text = mMatchData.networkStatus;

				try {
					// Note that mMatch.Data might be null (when we are starting a new match).
					// MatchData.MatchData() correctly deals with that and initializes a
					// brand-new match in that case.
					mMatchData = new MatchDataRunes(mMatch.Data); 
						

				if(match.SelfParticipantId=="p_1"){

					playerInt = 1;
				}

				if(match.SelfParticipantId=="p_2"){
					
					playerInt = 2;
				}

		//		player2.SetActive(true);

//				networkText.text = mMatchData.networkStatus;

				} catch (MatchData.UnsupportedMatchFormatException ex) {
				//	mFinalMessage = "Your game is out of date. Please update your game\n" +
				//		"in order to play this match.";
					Debug.LogWarning("Failed to parse board data: " + ex.Message);
					return;
				}

			// I can only make a move if the match is active and it's my turn!
			bool canPlay = (match.Status == TurnBasedMatch.MatchStatus.Active &&
			                match.TurnStatus == TurnBasedMatch.MatchTurnStatus.MyTurn);

			playerName =	match.PendingParticipant.DisplayName; //gets the name of the players so it can be shown before the turn, only works online

			if (canPlay) {

				SetupObjects(canPlay);

			} else {

			//sampleString = ExplainWhyICantPlay(); this explains why you cant play right now


			}

			} else {
			// show error message
		}

	}
	*/
	/*
	public void CreateQuickMatch(){

		PlayGamesPlatform.Instance.TurnBased.CreateQuickMatch(MinOpponents, MaxOpponents,
		                                                      Variant, OnMatchStarted);

	}


	public void GooglePlayAuth(){
		print ("google play being authe'd");

		if (mWaitingForAuth) {
			return;
		}

		if (Social.localUser.authenticated) {

			if (Social.localUser.image != null) {


			} else {

			}
		} else {

		}

			if (!Social.localUser.authenticated) {
				// Authenticate
				mWaitingForAuth = true;

			Social.localUser.Authenticate((bool success) => {
					
				mWaitingForAuth = false;

					if (success) {

					} else {

				}
				});
			} else {
				// Sign out!
				((GooglePlayGames.PlayGamesPlatform) Social.Active).SignOut();
			}

	}
*/
	/*
	public void InviteFriends(){

		PlayGamesPlatform.Instance.TurnBased.CreateWithInvitationScreen (MinOpponents, MaxOpponents,
		                                                                Variant, OnMatchStarted);

	}

	public void CheckInbox(){

		PlayGamesPlatform.Instance.TurnBased.AcceptFromInbox(OnMatchStarted);
	}
	*/

	private class sort : IComparer<MeteorCode>{
		int IComparer<MeteorCode>.Compare(MeteorCode _objA, MeteorCode _objB) {
			float t1 = _objA.indexSpawned;
			float t2 = _objB.indexSpawned;
			print ("this is sorting the list!" + t1.ToString() + " " +  t2.ToString());
			return t1.CompareTo(t2);

		}
	}

	public void TakeTurn(bool enemyDead) {

	//	Debug.Log ("the count of the player1blocks list is: " + mMatchData.Player1Blocks.Count); 

		if (UnityMode == true) {	
		mMatchData.ReadFromBytes(mMatchData.ToBytes()); //*** remove this when finished with offline mode test
		}

	
		//when testing the game offline
	if (UnityMode == true) {

			selectedBlocks.Clear (); //clears the blkocks that have been selected list

			SpellListener.GetComponent<SpellCreatorCampaign>().resetAllCounts();

			playerInt += 1;
			if (playerInt > 2) {
				playerInt = 1;
				BlockClickMousePanel.SetActive(false);

			}

				if(playerInt==2 && enemyDead==true){

					playerInt=1;
				BlockClickMousePanel.SetActive(true);

				}

			nextTurnBool=false; //reset

			SetupObjects(true);

			if(playerInt==2){
				BlockClickMousePanel.SetActive(true);
				//take the turn for the enemy

				List<GameObject> NullList1 = null;
					List<GameObject> NullList2 = null;

			//	object[] parms = new object[2]{NullList1, NullList2};

//				StartCoroutine(GameObject.Find("Enemy").GetComponentInChildren<EnemyAI>().ChooseRunes(NullList1,NullList2));

			}
		}

		//use the code below when running online
//		if (UnityMode == false) {
//			PlayGamesPlatform.Instance.TurnBased.TakeTurn (mMatch, mMatchData.ToBytes (),
//		                                              DecideNextToPlay (), (bool success) => {
//
//			});
//		}

/*		print("player prefs Fireball " + PlayerPrefs.GetFloat ("NetDictionary_" + "Fireball"));
		print("player prefs FireDrag " + PlayerPrefs.GetFloat ("NetDictionary_" + "FireDragon"));
		print("player prefs WaterBeam " + PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBeam"));
		print("player prefs WaterBubble " + PlayerPrefs.GetFloat ("NetDictionary_" + "WaterBubble"));
		print("player prefs EarthRock " + PlayerPrefs.GetFloat ("NetDictionary_" + "EarthRock"));
		print("player prefs EarthTreant " + PlayerPrefs.GetFloat ("NetDictionary_" + "EarthTreant"));
		print("player prefs Tornado " + PlayerPrefs.GetFloat ("NetDictionary_" + "Tornado"));
		print("player prefs Invis " + PlayerPrefs.GetFloat ("NetDictionary_" + "Invis"));
		print("player prefs Shield " + PlayerPrefs.GetFloat ("NetDictionary_" + "Shield"));
*/
	//	for (int i =0; i < netDictionaryObj.GetComponent<NetDictionary>().spellDamageReference.Count; i++) {

	//		print ("spl dmg is " + netDictionaryObj.GetComponent<NetDictionary>().spellDamageReference[i]);
//		print ("trying to get a float" + netDictionaryObj.GetComponent<NetDictionary> ().GetFloat ("Fireball"));
	//	}
	}
	
//	string DecideNextToPlay() {
//		if (mMatch.AvailableAutomatchSlots > 0) {
//			// hand over to an automatch player
//			return null;
//		} else {
//			// hand over to our (only) opponent
//			Participant opponent = UtilRunes.GetOpponent(mMatch);
//			return opponent == null ? null : opponent.ParticipantId;
//		}
//
//	}

	public void SetupObjects(bool canPlay) {

	//	print ("setup objects called");

	//	if (UnityMode == true) { //there will be a seperate instance in the match started code
	//		WhosTurn (canPlay, playerName); //makes the player turn panel come out and shows who's turn it is...
	//	}
			setupHealth (); //sets up the health bars for the players if unity is false see the code inside the method



				checkForModifiers ();

		if (GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().enemyTypeNumber == 10
		    && playerInt == 1
		    && player1.GetComponent<ControlPlayer1Character>().playerHealthNew>=0
		    && GameObject.Find ("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner> ().EnemySpawned.GetComponentInChildren<ControlEnemy>().playerHealthCurrent>0) { //checks the enemy type and whos turn it is

			print ("should be activating the countdown!!!");
			CountDownTimer.GetComponent<CountDownToAttack> ().CountDownText.text = "4";
			CountDownTimer.GetComponent<CountDownToAttack> ().TimeStart = 4;
			CountDownTimer.GetComponent<Animator> ().enabled = true;

			CountDownTimer.SetActive (true);

						
		} 
		if(playerInt==2){
			CountDownTimer.SetActive (false);
			
		}
		createNextBlocks ();	//creates the next four blocks to be spawned.
	
		if (mMatchData.Player1Blocks.Count > 0) {
	
			for (int z=0; z<mMatchData.Player1Blocks.Count; z++) { //when reading from the data, this transfers choosable blocks stored to copiedPlayersMeteorList so the color can be changed
				
				copiedPlayerMeteorList.Add(mMatchData.Player1Blocks [z]);
				
			}

			if(UnityMode==false){ //if you are playing online...

			spawnMeteorStart (); //spawns with setting the players blocks colors
			ChangeBlockColor (); 
			
			

			//	checkForDrownedMod();

				checkForConfusionMod (); //this checks for confusion after the blocks are already spawned this should fix the problem and make the blocks instantly 
				//mystery blocks

			StartCoroutine ("GhostMeteorList"); //creates feeler objects that will get the index of any meteor in that area at that time. So they can

			}
		//	checkForBurntMod();
		} else {

			spawnMeteorStart (); //spawns without setting up new colors
			StartCoroutine ("GhostMeteorList"); //creates feeler objects that will get the index of any meteor in that area at that time. So they can
		}

		if (mMatchData.mRuneTypes.Count == 0) { 
				
		}

		if (mMatchData.lastSpellTypeCasted.Count == 0) {
			//mMatchData.lastSpellTypeCasted.Capacity=2;
			for (int i =0; i<2; i++) {
				mMatchData.lastSpellTypeCasted.Add (4); //set this back to -1 when done testing...
			}

		} else {

		//	setPlayersLastSpell(); //this sets the blocks to the last spell the player casted
		
		}

		if (UnityMode == true) {

		checkForBurntMod ();
			
	//		checkForDrownedMod ();

			checkForConfusionMod (); //this checks for confusion after the blocks are already spawned this should fix the problem and make the blocks instantly 
			//mystery blocks

		}

		if (mMatchData.mRuneTypes.Count > 0) { //checks if the list contains any objects

		
			for (int i=0; i<mMatchData.mRuneTypes.Count; i++) {
			
				SpellListener.GetComponent<SpellCreatorCampaign>().receivedSpell.Add((int)mMatchData.mRuneTypes [i]); //this adds to the spell creator so spells  	
			}


			if(playerInt==1 && UnityMode==false){ //for showing the last spell casted... doesnt work when using in Unity
				SpellListener.GetComponent<SpellCreatorCampaign>().recievedSpellDesignatorPlayer(playerInt);

			}
			
			if(playerInt==2 && UnityMode==false ){ //for showing the last spell casted...doesnt work when using in Unity
				SpellListener.GetComponent<SpellCreatorCampaign>().recievedSpellDesignatorPlayer(playerInt);

			}

			mMatchData.mRuneTypes.Clear(); //clears the list so we can write new items into it...
			mMatchData.Player1Blocks.Clear (); //clears the list so we dont add blocks to an already existing list
			//Health is cleared in the setuphealth method

		} else {
		
		//	statusText.text = "Nothing contained in mMatchData :/";
		}

		//checking if the other enemy is the purple reptillian so we can start the timer.
		print ("setup objs is being called");


	}

	public void recordHealth(){
		//this needs to go at the end of the spell recieved 

//		mMatchData.playersHealth[0] = (player1.GetComponent<ControlPlayer1Character>().playerHealthCurrent);
////		print ("enemy's health is : " + (GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ().playerHealthCurrent));
//		mMatchData.playersHealth[1] = (GameObject.Find("Enemy").GetComponentInChildren<ControlEnemy>().playerHealthCurrent);
//
	}

	public void BurntRunesManager(float player1burnt, float player2burnt){

		mMatchData.checkBurnt [0] = player1burnt;
		mMatchData.checkBurnt [1] = player2burnt;
	}

	public void DrownedRunesManager(float player1drowned, float player2drowned){
		
		mMatchData.checkDrowned [0] = player1drowned;
		mMatchData.checkDrowned [1] = player2drowned;
	}

	public void confusionManager(float player1confus, float player2confus){

		mMatchData.checkConfusion [0] = player1confus;
		mMatchData.checkConfusion [1] = player2confus;
		
	}

	public void invisManager(float player1invis, float player2invis){

		player1InvisValue = player1invis;
		player2InvisValue = player2invis;

		mMatchData.checkInvis [0] = player1invis;
		mMatchData.checkInvis [1] = player2invis;

	}

	public void stunManager(float player1Stun, float player2Stun){

		print ("stun manager is running");

		mMatchData.checkStun [0] = player1Stun;
		mMatchData.checkStun [1] = player2Stun;
		
	}

	public void flameDOTManager(float player1FlameDOT, float player2FlameDOT){
		
		print ("stun manager is running");
		
		mMatchData.FlameDOT [0] = player1FlameDOT;
		mMatchData.FlameDOT [1] = player2FlameDOT;
		
	}

	public void shieldManager(float player1Shield, float player2Shield){
		
		print ("stun manager is running");
		
		mMatchData.checkShield [0] = player1Shield;
		mMatchData.checkShield [1] = player2Shield;

	}

	public void enchManager(float player1Ench, float player2Ench){
		
		print ("added enchantments " + player1Ench);
		
		swordEnch [0] = player1Ench;
		swordEnch [1] = player2Ench;
		
	}

	public void setupHealth(){

//		if (mMatchData.playersHealth.Count > 0) {
//
//			if(UnityMode==false){
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent = mMatchData.playersHealth [0];
//			player1.GetComponent<ControlPlayer1Character> ().playerHealthNew = mMatchData.playersHealth [0];
//						
//			player1.GetComponent<ControlPlayer1Character> ().Player1HealthBar.GetComponent<Slider> ().value = mMatchData.playersHealth [0] / player1.GetComponent<ControlPlayer1Character> ().playerHealthMax;
//
//				GameObject Enemy = GameObject.Find("Enemy");
//
//				Enemy.GetComponent<ControlEnemy> ().playerHealthCurrent = mMatchData.playersHealth [1];
//				Enemy.GetComponent<ControlEnemy> ().playerHealthNew = mMatchData.playersHealth [1];
//						
//				Enemy.GetComponent<ControlEnemy> ().Player2HealthBar.GetComponent<Slider> ().value = mMatchData.playersHealth [1] / Enemy.GetComponent<ControlEnemy> ().playerHealthMax;
//			}
//
//		} 
//
//			if (mMatchData.playersHealth.Count == 0) {
//			mMatchData.playersHealth.Add (player1.GetComponent<ControlPlayer1Character> ().playerHealthCurrent);
//			mMatchData.playersHealth.Add (GameObject.Find("Enemy").GetComponentInChildren<ControlEnemy>().playerHealthCurrent);
//		}

	}

	public void checkForModifiers(){

		if (mMatchData.checkBurnt.Count == 0) {
			mMatchData.checkBurnt.Add(0);
			mMatchData.checkBurnt.Add(0);
		}

		if (mMatchData.checkDrowned.Count == 0) {
			mMatchData.checkDrowned.Add(0);
			mMatchData.checkDrowned.Add(0);
		}

		if (mMatchData.checkConfusion.Count == 0) {

			mMatchData.checkConfusion.Add(0);
			mMatchData.checkConfusion.Add(0);
		}

//THIS IS FOR INVISIBLITY!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (mMatchData.checkInvis.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkInvis.Add(0);
			mMatchData.checkInvis.Add(0);
		}


		if (mMatchData.checkInvis.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkInvis.Add(0);
			mMatchData.checkInvis.Add(0);
		}

		//invisManager (1, 0);//to test if invisstuff works.

		if (mMatchData.checkInvis [0] > 0 ) { //check if player 1 is invis if 1 that means the player is invis
		//player 1 is invis
			mMatchData.checkInvis [0] -=1;

			player1.GetComponent<ControlPlayer1Character>().player1Invis=true; //this makes it so the player doesnt become invis again

			player1.GetComponent<BoxCollider2D>().enabled=false; //this makes it so the player cannot be hit

			if(UnityMode==false){
				GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);
				InvisParticlesClone.transform.parent = player1.transform;

			}

			if(mMatchData.checkInvis[0]==0 
			   && UnityMode==true 
			   && player1.GetComponent<ControlPlayer1Character>().player1Invis==true){
	
				player1.GetComponent<ControlPlayer1Character>().player1Invis=false; //this makes it so the player doesnt become invis again
				player1.GetComponent<ControlPlayer1Character>().player1ReturnToVisible=true;
				player1.GetComponent<BoxCollider2D>().enabled=true; //this makes it so the player cannot be hit

			}

		}
		/*
		if (mMatchData.checkInvis [1] > 0 ) { //check if player 1 is invis if 1 that means the player is invis
		//player 2 is invis
			mMatchData.checkInvis [1] -=1;

			player2.GetComponent<ControlPlayer2Character>().player2Invis=true; //this makes it so the player doesnt become invis again

			player2.GetComponent<BoxCollider2D>().enabled=false; //this makes it so the player cannot be hit

			if(UnityMode==false){
				GameObject InvisParticlesClone = (GameObject)Instantiate (InvisParticles, new Vector3 (player2.transform.position.x, player2.transform.position.y, 0), Quaternion.identity);
				InvisParticlesClone.transform.parent = player2.transform;
			}

			if(mMatchData.checkInvis[1]==0 
			   && UnityMode==true 
			   && player2.GetComponent<ControlPlayer2Character>().player2Invis==true){

				player2.GetComponent<ControlPlayer2Character>().player2Invis=false; //this makes it so the player doesnt become invis again
				player2.GetComponent<ControlPlayer2Character>().player2ReturnToVisible=true;
				player2.GetComponent<BoxCollider2D>().enabled=true; //this makes it so the player cannot be hit

			}
		}
*/
//END INVISIBLITY!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//***

//THIS IS FOR STUNS -----------------------------------------------------------------------------------
	

		if (mMatchData.checkStun.Count == 0) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkStun.Add(0);
			mMatchData.checkStun.Add(0);
		}


		if (mMatchData.checkStun [0] > 0 ) { //check if player 1 is stunned if > 1 that means the player is stunned
			print ("player 1 is stunned");

			mMatchData.checkStun [0] -= 1;


			player1.GetComponent<ControlPlayer1Character> ().player1Stun = true;
			player1.GetComponent<Animator>().SetBool("IsStunned",true);


			if(mMatchData.checkStun[0] ==0 
			   && player1.GetComponent<ControlPlayer1Character> ().player1Stun == true
			   ){

				player1.GetComponent<ControlPlayer1Character> ().player1Stun = false;
				player1.GetComponent<Animator>().SetBool("IsStunned",false);
			}
		}


//END STUNS -----------------------------------------------------------------------------------
//this is for FLAME DOT

		if (mMatchData.FlameDOT.Count == 0) { //this sets up the Flame DOT list as 0 for both players.
			
			mMatchData.FlameDOT.Add(0);
			mMatchData.FlameDOT.Add(0);
		}

		//flameDOTManager (0,0);

		if (mMatchData.FlameDOT [0] > 0) { //detects if the player should be receiving damage over time

			mMatchData.FlameDOT[0] -=1;

			player1.GetComponent<ControlPlayer1Character>().player1FlameDOT=true;

			if(UnityMode==true){
				player1.GetComponent<ControlPlayer1Character>().justBurned=false;
				
				if(mMatchData.FlameDOT[0]==0){
					
					player1.GetComponent<ControlPlayer1Character>().player1FlameDOT=false;
					
				}
			}			
		}


		if (mMatchData.FlameDOT [1] > 0) { //detects if the player should be receiving damage over time

			ControlEnemy currentEnemy = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy>();

			mMatchData.FlameDOT[1] -=1;

			if(UnityMode==true){

				currentEnemy.justBurned=false;
			
				if(mMatchData.FlameDOT[1]==0){

					currentEnemy.EnemyFlameDOT=false;

				}
			}
		}

//END FLAME DOT

//CHECK FOR Shield Modifier

		if (mMatchData.checkShield.Count == 0) { //this sets up the Flame DOT list as 0 for both players.
			
			mMatchData.checkShield.Add(0);
			mMatchData.checkShield.Add(0);
		}

		if (swordEnch.Count == 0) { //this sets up the Flame DOT list as 0 for both players.
			
			swordEnch.Add(0);
			swordEnch.Add(0);
		}

		if (mMatchData.swordEnchnt.Count == 0) { //this is just so the mmatchdata class doesnt freak out
			
			mMatchData.swordEnchnt.Add(0);
			mMatchData.swordEnchnt.Add(0);
		}

		if (swordEnch [0] > 0) {

			print ("sword is enchanted and..." + swordEnch [0]);

		//	swordEnch [0] -=1;

			player1.GetComponent<ControlPlayer1Character> ().isEnchanted = true;

		} else {

			player1.GetComponent<ControlPlayer1Character> ().isEnchanted = false;

		}
		//shieldManager (0,1);

		/*
		if (mMatchData.checkShield [0] > 0) { //detects if the player should be receiving damage over time
			
			mMatchData.checkShield[0] -=1;

			if(UnityMode==false){
			player1.GetComponent<ControlPlayer1Character>().isShielded=true; //this is so it wont add the shield amount again
			player1.GetComponent<Animator>().SetBool("SummonShield", true);
			}
		}
		
		if (mMatchData.checkShield [1] > 0) { //detects if the player should be receiving damage over time
			
			mMatchData.checkShield[1] -=1;

			if(UnityMode==false){
			player2.GetComponent<ControlPlayer2Character>().isShielded=true;
			player2.GetComponent<Animator>().SetBool("SummonShield", true);
			}
		}
		*/

	}

	public void InvisCounterCheckP2(){
		//this method is a special condition where both players are invis, and one player is receiving a spell
		//it is placed in this class because this method needs to reference mMatchData and I dont want to have any other class reference mMatchData
	
		/*
		if (UnityMode == false) { //makes it so the game doesnt reset the invis when played offline
			
			if (mMatchData.checkInvis [0] == 0 //if p2 is countering back p1
				&& mMatchData.checkInvis [1] == 0
				&& player2.GetComponent<ControlPlayer2Character> ().player2Invis == true
				&& player1.GetComponent<ControlPlayer1Character> ().player1Invis == true) {
				
				player1.GetComponent<ControlPlayer1Character> ().castFireSpell ();
				
				player1.GetComponent<ControlPlayer1Character> ().player1Invis = false; //this makes it so the player doesnt become invis again
				player1.GetComponent<ControlPlayer1Character> ().player1ReturnToVisible = true;
				player1.GetComponent<BoxCollider2D> ().enabled = true; //this makes it so the player cannot be hit
			}
		}
		*/

	}
	public void InvisCounterCheckP1(){		
		/*
		if(UnityMode==false){

		if (mMatchData.checkInvis [0] == 0 //if p1 is countering back p2
			&& mMatchData.checkInvis [1] == 0
			&& player1.GetComponent<ControlPlayer1Character> ().player1Invis == true
			&& player2.GetComponent<ControlPlayer2Character> ().player2Invis == true) {
				
			player2.GetComponent<ControlPlayer2Character> ().castFireSpell ();
				
			player2.GetComponent<ControlPlayer2Character> ().player2Invis = false; //this makes it so the player doesnt become invis again
			player2.GetComponent<ControlPlayer2Character> ().player2ReturnToVisible = true;
			player2.GetComponent<BoxCollider2D> ().enabled = true; //this makes it so the player cannot be hit
		}
	}
	*/
}

	public void setPlayersLastSpell(){

		if (playerInt == 1) {
			switch ((int)mMatchData.lastSpellTypeCasted [0]) {
		
			case(0):
				LastSpellPlayer1.GetComponent<Image> ().sprite = WindSprite; //wind 
				break;
			case(1):
				LastSpellPlayer1.GetComponent<Image> ().sprite = FireSprite; //fire
				break;
			case(2):
				LastSpellPlayer1.GetComponent<Image> ().sprite = WaterSprite; //water
				break;
			case(3):
				LastSpellPlayer1.GetComponent<Image> ().sprite = EarthSprite; //earth
				break;
			case(4):
				LastSpellPlayer1.GetComponent<Image> ().sprite = nullsprite; //earth
				break;
			default:
				LastSpellPlayer1.GetComponent<Image> ().sprite = nullsprite; //earth
				break;

				print ("spell icon has been changed p1");

			}
		}
		if (playerInt == 2) {
			switch ((int)mMatchData.lastSpellTypeCasted [1]) {

			case(0):
				LastSpellPlayer2.GetComponent<Image> ().sprite = WindSprite; //wind 
				break;
			case(1):
				LastSpellPlayer2.GetComponent<Image> ().sprite = FireSprite; //wind 
				break;
			case(2):
				LastSpellPlayer2.GetComponent<Image> ().sprite = WaterSprite; //wind 
				break;
			case(3):
				LastSpellPlayer2.GetComponent<Image> ().sprite = EarthSprite; //wind 
				break;
			case(4):
				LastSpellPlayer2.GetComponent<Image> ().sprite = nullsprite; //earth
				break;
			default:
				LastSpellPlayer2.GetComponent<Image> ().sprite = nullsprite; //earth
				break;
				print ("spell icon has been changed p2");

			}
		}
	}

	public void checkForConfusionMod(){
		/*
		if (mMatchData.checkConfusion[0] > 0 && playerInt==1
		    || mMatchData.checkConfusion[0] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkConfusion [0] -= 1;
			
			if(mMatchData.checkConfusion[0]!=0){
				

				createConfusion();
				
			}
			
			if(mMatchData.checkConfusion[0]==0 && mMatchData.checkBurnt[0]==0 && mMatchData.checkDrowned[0]==0){
				ChangeBlockColor();

			}
		}
		
		if (mMatchData.checkConfusion[1] > 0 && playerInt==2 
		    || mMatchData.checkConfusion[1] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			
			mMatchData.checkConfusion [1] -= 1;
			
			if(mMatchData.checkConfusion[1]!=0){
				

				createConfusion();
				

			}
			
			if(mMatchData.checkConfusion[1]==0 && mMatchData.checkBurnt[1]==0 && mMatchData.checkDrowned[1]==0){
				ChangeBlockColor();

			}
		}
		*/
	}

	public void checkForBurntMod(){



		if ( playerInt==2) { //this sets up the invis list as 0 for both.

		
				RestoreRuneColor();
			}

	}
	/*
	public void checkForDrownedMod(){

		if (mMatchData.checkDrowned[0] > 0 && playerInt==1
		    || mMatchData.checkDrowned[0] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkDrowned [0] -= 1;
			if(mMatchData.checkDrowned[0]!=0){
				createDrownedRunes();
			}
			if(mMatchData.checkDrowned[0]==0 && mMatchData.checkBurnt[0]==0 && mMatchData.checkConfusion[0]==0){
				ChangeBlockColor();
			}
		}
		if (mMatchData.checkDrowned[1] > 0 && playerInt==2 
		    || mMatchData.checkDrowned[1] > 0 && UnityMode==true) { //this sets up the invis list as 0 for both.
			mMatchData.checkDrowned [1] -= 1;
			if(mMatchData.checkDrowned[1]!=0){
				createDrownedRunes();
			}
			if(mMatchData.checkDrowned[1]==0 && mMatchData.checkBurnt[1]==0 && mMatchData.checkConfusion[1]==0){
				ChangeBlockColor();
			}
		}
	}

	*/
	public void openSpellGuideWindow(){

		PersistantSoundmanager.openSpellBook ();

		if (SpellGuideWindow.activeSelf == false && pressCounter==0) {
			SpellGuideWindow.SetActive(true);

			if(TutorialScript!=null && TutorialScript.villageUnderAttack==true || TutorialScript!=null && TutorialScript.explainEffectiveness3 == true){

				TutorialScript.spellBookOpened(); //this makes the dialogue disapeer

			}
		} 

		pressCounter+=1;
		if (pressCounter == 2) {
			SpellGuideWindow.SetActive(false);
			pressCounter=0;

			if(TutorialScript!=null && TutorialScript.villageUnderAttack==true || TutorialScript!=null && TutorialScript.explainEffectiveness3 == true){
				TutorialScript.endSpellBookFinger(); //this just removes the finger from the spellbook icon
			}
		}
	}

	public void WhosTurn(bool canplay, string playerName){


		if (UnityMode == true && player1.GetComponent<ControlPlayer1Character>().playerDead==false) {
		    //&& player2.GetComponent<ControlPlayer2Character>().playerDead==false

			if(playerInt==1){
				WhosTurnPanel.SetActive(true);
				WhosTurnPanel.GetComponentInChildren<TextMeshProUGUI>().text = "Your Turn";
			}

			if(playerInt==2){
//				print ("player 2's turn on whos turn pane!");
				WhosTurnPanel.SetActive(true);
				WhosTurnPanel.GetComponentInChildren<TextMeshProUGUI>().text = "Enemy Turn";
			}
		}
	}

	public void createConfusion(){

		for (int i=0; i<meteorList.Count; i++) {


			if(i>=0 && i<4){
			if(i%2 == 0){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
				meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
				meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=4 && i<8){
				if(i%2 == 1){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=8 && i<12){
				if(i%2 == 0){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}

			if(i>=12 && i<16){
				if(i%2 == 1){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(4);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
				}else{
					meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
					meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
					meteorList[i].LockColor=true;
				}
			}
		}
	}

	public void createBurntRunes(){
		
		for (int i=0; i<meteorList.Count; i++) {
			if(i==0 || i==5 || i==10 || i==15){
					meteorList[i].GetComponent<MeteorCode>().LockColor=false;
					meteorList[i].GetComponent<MeteorCode>().createRuneColor(5);
					meteorList[i].GetComponent<MeteorCode>().LockColor=true;
			}else{
				meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
				meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
				meteorList[i].LockColor=true;
			}
		}
	}
	/*
	public void createDrownedRunes(){
		
		for (int i=0; i<meteorList.Count; i++) {
			if(i==3 || i==6 || i==9 || i==12){
				meteorList[i].GetComponent<MeteorCode>().LockColor=false;
				meteorList[i].GetComponent<MeteorCode>().createRuneColor(5);
				meteorList[i].GetComponent<MeteorCode>().LockColor=true;
			}
			else{
				meteorList[i].LockColor=false; //this makes sure any other rune modifications are removed like confus
				meteorList[i].createRuneColor((int)meteorList[i].runeIntDesignation);
				meteorList[i].LockColor=true;

			}
		}
	}
	*/
	public void showExpGain(int enemyRank){

		string expGainedString;

		if (PlayerPrefs.GetFloat ("Level14") == 0) {
			player1.GetComponent<ControlPlayer1Character>().playerExpNew +=300*(enemyRank); // was 200

			expGainedString = "+ " + (300 * enemyRank).ToString () + " Exp";	

		} else {
			player1.GetComponent<ControlPlayer1Character>().playerExpNew +=1000*(enemyRank); // was 200

			expGainedString = "+ " + (1000 * enemyRank).ToString () + " Exp";		


		}


		plusExp.GetComponent<TextMeshProUGUI> ().text = expGainedString;
		plusExp.SetActive (true);

	}

	public void levelUp(){

		//all the other level up stuff is handled inside of the exp function in controlplayer1
		LevelUpBool = true;
		levelUpObj.SetActive (true);
		player1.GetComponent<ControlPlayer1Character> ().LevelUpMethod ();
		GameObject LvUpRays = (GameObject)Instantiate (LevelUpRays, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);
		PersistantSoundmanager.GetComponent<SoundManager> ().LevelUp ();

	}

	public void addGold(int gold){

		goldText.GetComponent<TextMeshProUGUI> ().text = (player1.GetComponent<ControlPlayer1Character> ().playerGold + gold).ToString ();


		if (PlayerPrefs.GetFloat ("Level14") == 0) {
		
			//do nada

		} else {

			gold = 70 + gold; //gives the player 70 more gold per drop
		}

		player1.GetComponent<ControlPlayer1Character> ().playerGold += gold;
		
		PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt("Gold")+gold);


//		print ("playerprefs being set to " + PlayerPrefs.GetInt("Gold"));


		addGoldObj.GetComponent<TextMeshProUGUI> ().text = "+ " + gold.ToString ();

		addGoldObj.SetActive (true);

		PersistantSoundmanager.goldSound ();

		GameObject gemExplosionObj = (GameObject)Instantiate (gemExplosion, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);

	}

	public void addItem(){

		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.white, Color.white, Color.magenta, Color.magenta);
		//goes top left/right //bottom left/right

		itemObj.GetComponent<TextMeshProUGUI> ().text = "New Item Found!";

		GameObject newItemBurstObj = (GameObject)Instantiate (newItemBurst, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);

		itemObj.SetActive (true);
		PersistantSoundmanager.NewItemFound ();
	}

	public void EnemyBurned(){

		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.red, Color.red, Color.yellow, Color.yellow);
																		//goes top left/right //bottom left/right
		itemObj.GetComponent<TextMeshProUGUI> ().text = "Enemy Burned!";
		itemObj.SetActive (true);
		
		//PersistantSoundmanager.NewItemFound ();
	}

	public void EnemyStunned(){

		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.blue, Color.blue, Color.white, Color.white);
		//goes top left/right //bottom left/right

		itemObj.GetComponent<TextMeshProUGUI> ().text = "Enemy Stunned!";
		itemObj.SetActive (true);
		
		//PersistantSoundmanager.NewItemFound ();
	}

	public void PlayerStunned(){
		
		itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.blue, Color.blue, Color.white, Color.white);
		//goes top left/right //bottom left/right
		
		itemObj.GetComponent<TextMeshProUGUI> ().text = "You are Stunned!";
		itemObj.SetActive (true);
		
		//PersistantSoundmanager.NewItemFound ();
	}

	public void CounterAtkRdy(){

		if (itemObj.activeSelf == false) {

			itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.grey, Color.grey, Color.white, Color.white);
			//goes top left/right //bottom left/right

			itemObj.GetComponent<TextMeshProUGUI> ().text = "Counter Attack Ready!";
			itemObj.SetActive (true);

		}
		//PersistantSoundmanager.NewItemFound ();
	}

	public void EnchantmentRdy(){
		
		if (itemObj.activeSelf == false) {

			GameObject newItemBurstObj = (GameObject)Instantiate (newItemBurst, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);


			itemObj.GetComponent<TextMeshProUGUI> ().colorGradient = new VertexGradient(Color.magenta, Color.magenta, Color.white, Color.white);
			//goes top left/right //bottom left/right
			
			itemObj.GetComponent<TextMeshProUGUI> ().text = "Sword Enchanted!";
			itemObj.SetActive (true);
			
		}
		//PersistantSoundmanager.NewItemFound ();
	}


	public void addHP(int HPadd){

		addHPText.GetComponent<TextMeshProUGUI> ().text = "+ " + HPadd + " HP"; 

		addHPObj.SetActive (true);

		player1.GetComponent<ControlPlayer1Character> ().lifeStealValue = HPadd;
		
		player1.GetComponent<ControlPlayer1Character> ().lifeSteal = true;

		PersistantSoundmanager.getPotion();

		GameObject HealingObjClone = (GameObject)Instantiate (HealingObj, new Vector3 (player1.transform.position.x, player1.transform.position.y, 0), Quaternion.identity);

	}

	public void addHPEnemy(int HPadd){

		addHPFinalBossText.GetComponent<TextMeshProUGUI> ().text = "+ " + HPadd + " HP"; 
		
		addHPFinaBossObj.SetActive (true);
		
		ControlEnemy enemyScript = GameObject.Find ("Enemy").GetComponentInChildren<ControlEnemy> ();
		
		enemyScript.lifeStealValue = HPadd;
		
		enemyScript.lifeSteal = true;
		
		GameObject HealingObjClone = (GameObject)Instantiate (HealingObj, new Vector3 (enemyScript.transform.position.x, enemyScript.transform.position.y, 0), Quaternion.identity);
		
	}

	public void damageEnemyNotification(float damage){

		enemyDamageAmount.GetComponent<TextMeshProUGUI>().text = "- " + damage.ToString() + " HP"; //needs to be added to every spell
		enemyDamageAmount.SetActive(true);

		if (player1.GetComponent<ControlPlayer1Character> ().LifeStealLearned == true) {

			addHPLifesteal(Mathf.FloorToInt(0.15f*damage));

		}

	}

	public void addHPLifesteal(float healthStolen){

		addHPText.GetComponent<TextMeshProUGUI> ().text = "+ " + healthStolen + " HP"; 
		
		addHPObj.SetActive (true);
		
		player1.GetComponent<ControlPlayer1Character> ().lifeStealValue = healthStolen;
		
		player1.GetComponent<ControlPlayer1Character> ().lifeSteal = true;
		
		GameObject LifeStealClone = (GameObject)Instantiate (LifeStealObj, new Vector3 (GameObject.Find("Enemy").GetComponentInChildren<EnemyRagdoll>().Torso.transform.position.x, GameObject.Find("Enemy").GetComponentInChildren<EnemyRagdoll>().Torso.transform.position.y, 0), Quaternion.identity);


	}

	public void damagePlayerNotification(float damage){

		player1.GetComponent<ControlPlayer1Character> ().playerHit ();

		playerDamageAmountText.GetComponent<TextMeshProUGUI>().text = "-" + damage.ToString() +" HP";
		playerDamageAmount.SetActive(true);

		this.gameObject.GetComponent<ScreenShakerController> ().activateScreenShake (0.5f,0.1f); //shakes the screen

	}

	public void effectiveness(int effectivenessInt){

		if(effectivenessInt==1){//this means it is super effective
			effectivenessObj.GetComponent<TextMeshProUGUI>().text = "Super Effective!";
			effectivenessObj.gameObject.SetActive(true);
		}
		if(effectivenessInt==2){//this means it is super effective
			effectivenessObj.GetComponent<TextMeshProUGUI>().text = "Not Very Effective";
			effectivenessObj.gameObject.SetActive(true);
		}
	}

	public void resetEnemySpellType(){ //this is so when a new enemy spawns it doesnt use the rune type of the last enemy

		if (mMatchData.lastSpellTypeCasted.Count != 0) {
			mMatchData.lastSpellTypeCasted [1] = 4;
		}

//		print ("resettin enemy spell type");
	}

	public bool criticalHit(){

		int CritDice = Random.Range (0, 100); //test to see if the range works
		
//		print ("random roll is " + CritDice);

	//	print ("the crit mods are: " + "luck Mod= " + Mathf.FloorToInt (PlayerPrefs.GetFloat ("luckMod")) + "The item luck mod is " + PlayerPrefs.GetFloat ("LuckItemMod"));

		if (CritDice <= (Mathf.FloorToInt (PlayerPrefs.GetFloat ("luckMod") ) 
		                + PlayerPrefs.GetFloat ("LuckItemMod"))) {

			enemyDamageAmount.GetComponent<critScript>().didCrit=true;
			enemyDamageAmount.GetComponent<critScript>().critNotification();


			return true; //crit is gonna happen

		} else {

			return false;

		}

	}

	public void playerHasDied(){
		CanvasBehindPlayer.GetComponent<GraphicRaycaster> ().enabled = true;
		PlayerDeadPanel.SetActive (true);
	}

	public void musicSelector(){ //analytics also going here

		switch (GameObject.Find("EnemySpawn").GetComponent<CampaignLevelsEnemySpawner>().InheritStats.CurrentLevel) {

		case(1):
			PersistantSoundmanager.Level1Music ();

			Analytics.CustomEvent("Level1Started", new Dictionary<string, object>
			                      {
				{ "Level1Started", true }

			});

			break;
		case(2):
			PersistantSoundmanager.Level2Music ();

			Analytics.CustomEvent("Level2Started", new Dictionary<string, object>
			                      {
				{ "Level2Started", true }
				
			});

			break;
		case(3):
			PersistantSoundmanager.Level3Music ();

			Analytics.CustomEvent("Level3Started", new Dictionary<string, object>
			                      {
				{ "Level3Started", true }
				
			});

			break;
		case(4):
			PersistantSoundmanager.Level4Music ();


			Analytics.CustomEvent("Level4Started", new Dictionary<string, object>
			                      {
				{ "Level4Started", true }
				
			});

			break;
		case(5):
			PersistantSoundmanager.Level5Music ();

			Analytics.CustomEvent("Level5Started", new Dictionary<string, object>
			                      {
				{ "Level5Started", true }
				
			});

			break;
		case(6):
			PersistantSoundmanager.Level6Music ();
			
			Analytics.CustomEvent("Level6Started", new Dictionary<string, object>
			                      {
				{ "Level6Started", true }
				
			});
			
			break;
		case(7):
			PersistantSoundmanager.Level7Music ();
			
			Analytics.CustomEvent("Level7Started", new Dictionary<string, object>
			                      {
				{ "Level7Started", true }
				
			});
			
			break;
		case(8):
			PersistantSoundmanager.Level8Music ();
			
			Analytics.CustomEvent("Level8Started", new Dictionary<string, object>
			                      {
				{ "Level8Started", true }
				
			});
			
			break;

		case(9):
			PersistantSoundmanager.Level9Music ();
			
			Analytics.CustomEvent("Level9Started", new Dictionary<string, object>
			                      {
				{ "Level9Started", true }
				
			});

			break;

		case(10):
			PersistantSoundmanager.Level10Music ();
			
			Analytics.CustomEvent("Level10Started", new Dictionary<string, object>
			                      {
				{ "Level10Started", true }
				
			});
			break;


		case(11):
			PersistantSoundmanager.Level11Music ();
			
			Analytics.CustomEvent("Level11Started", new Dictionary<string, object>
			                      {
				{ "Level11Started", true }
				
			});
			break;


		case(12):
			PersistantSoundmanager.Level12Music ();
			
			Analytics.CustomEvent("Level12Started", new Dictionary<string, object>
			                      {
				{ "Level12Started", true }
				
			});
			break;


		case(13):
			PersistantSoundmanager.Level13Music ();
			
			Analytics.CustomEvent("Level13Started", new Dictionary<string, object>
			                      {
				{ "Level13Started", true }
				
			});
			break;


		case(14):
			PersistantSoundmanager.Level14Music ();
			
			Analytics.CustomEvent("Level14Started", new Dictionary<string, object>
			                      {
				{ "Level14Started", true }
				
			});
			
			break;

		
		//new levels
		case(15):
		PersistantSoundmanager.Level15Music ();
		
		Analytics.CustomEvent("Level15Started", new Dictionary<string, object>
		                      {
			{ "Level15Started", true }
			
		});
		
		break;
		
	

		case(16):
		PersistantSoundmanager.Level16Music ();
		
		Analytics.CustomEvent("Level16Started", new Dictionary<string, object>
		                      {
			{ "Level16Started", true }
			
		});
		
		break;
	


		case(17):
		PersistantSoundmanager.Level17Music ();

		Analytics.CustomEvent("Level17Started", new Dictionary<string, object>
		                      {
			{ "Level17Started", true }
			
		});

		break;

		

		case(18):
		PersistantSoundmanager.Level18Music ();

		Analytics.CustomEvent("Level18Started", new Dictionary<string, object>
		                      {
			{ "Level18Started", true }
			
		});

		break;

		

		case(19):
		PersistantSoundmanager.Level19Music ();

		Analytics.CustomEvent("Level19Started", new Dictionary<string, object>
		                      {
			{ "Level19Started", true }
			
		});

		break;

		

		case(20):
		PersistantSoundmanager.Level20Music ();

		Analytics.CustomEvent("Level20Started", new Dictionary<string, object>
		                      {
			{ "Level20Started", true }
			
		});

		break;

		

		case(21):
		PersistantSoundmanager.Level21Music ();

		Analytics.CustomEvent("Level21Started", new Dictionary<string, object>
		                      {
			{ "Level21Started", true }
			
		});

		break;

		case(22):
			PersistantSoundmanager.Level22Music ();
			
			Analytics.CustomEvent("Level22Started", new Dictionary<string, object>
			                      {
				{ "Level22Started", true }
				
			});
			
			break;

		case(23):
			PersistantSoundmanager.Level23Music ();
			
			Analytics.CustomEvent("Level23Started", new Dictionary<string, object>
			                      {
				{ "Level23Started", true }
				
			});
			
			break;

		case(24):
			PersistantSoundmanager.Level24Music ();
			
			Analytics.CustomEvent("Level24Started", new Dictionary<string, object>
			                      {
				{ "Level24Started", true }
				
			});
			
			break;

		case(25):
			PersistantSoundmanager.Level25Music ();
			
			Analytics.CustomEvent("Level25Started", new Dictionary<string, object>
			                      {
				{ "Level25Started", true }
				
			});
			
			break;

		case(26):
			PersistantSoundmanager.Level26Music ();
			
			Analytics.CustomEvent("Level26Started", new Dictionary<string, object>
			                      {
				{ "Level26Started", true }
				
			});
			
			break;

		case(27):
			PersistantSoundmanager.Level27Music ();
			
			Analytics.CustomEvent("Level27Started", new Dictionary<string, object>
			                      {
				{ "Level27Started", true }
				
			});
			
			break;

		case(28):
			PersistantSoundmanager.Level28Music ();
			
			Analytics.CustomEvent("Level28Started", new Dictionary<string, object>
			                      {
				{ "Level28Started", true }
				
			});
			
			break;

		}



	}

}
