﻿	using System;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Purchasing;

	// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
	// one of the existing Survival Shooter scripts.
		// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
	public class PurchaseManager : MonoBehaviour, IStoreListener
		{
			
	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	//public static string kProductIDConsumable =    "consumable";   
	
	public static string gemsFist = "fist05";   //this will be for gems purchasing
	public static string gemsPile = "pile05";   //this will be for gems purchasing
	public static string gemsSack = "sack05";   //this will be for gems purchasing
					
	public static string gemsCrate = "crate05";   //this will be for gems purchasing
	public static string gemsChest = "chest05";   //this will be for gems purchasing
	public static string gemsTon = "ton05";   //this will be for gems purchasing

	//public static string kProductIDNonConsumable = "nonconsumable";

	public static string starterPackNonConsumable = "starter05";
	public static string furyPackNonConsumable = "fury05";
	public static string arcanePackNonConsumable = "arcane05";
	public static string chemistPackNonConsumable = "chemist05";
	public static string comboPackNonConsumable = "combo05";
    public static string eventPackNonConsumable = "event05";
    public static string utilityPackNonConsumable = "utility05";


    public static string starterPackNonConsumablePromo = "starter06";
    public static string furyPackNonConsumablePromo = "fury06";
    public static string arcanePackNonConsumablePromo = "arcane06";
    public static string comboPackNonConsumablePromo = "combo06";





    public List <ShopCard> packagesList = new List<ShopCard>();
		//0 = starter pacl
		//1 = arcane pack
		//2 = fury pack
		//3 = chemist oack
		//4 = combo pack c-c-c-combo breakkrr

			public List <ShopCard> gemsList = new List<ShopCard>();

		//0 = gemfist
		//1 = gempile
		//2 = gemsack
		//3 = gemcrate
		//4 = gemchest
		//5 = gemton


//			public static string kProductIDSubscription =  "subscription"; 

//			// Apple App Store-specific product identifier for the subscription product.
//			private static string kProductNameAppleSubscription =  "com.unity3d.subscription.new";

//			// Google Play Store-specific product identifier subscription product.
//			private static string kProductNameGooglePlaySubscription =  "com.unity3d.subscription.original"; 
			
			void Start()
			{
               // print("store code running");

				// If we haven't set up the Unity Purchasing reference
				if (m_StoreController == null)
				{
					// Begin to configure our connection to Purchasing
					InitializePurchasing();
				}
			}

			public void InitializePurchasing() 
			{
				// If we have already connected to Purchasing ...
				if (IsInitialized())
				{
					// ... we are done here.
					return;
				}

				// Create a builder, first passing in a suite of Unity provided stores.
				var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		//for non consumables

		builder.AddProduct(starterPackNonConsumable, ProductType.NonConsumable);
				
		builder.AddProduct(furyPackNonConsumable, ProductType.NonConsumable);
				
		builder.AddProduct(arcanePackNonConsumable, ProductType.NonConsumable);
				
		builder.AddProduct(chemistPackNonConsumable, ProductType.NonConsumable);

		builder.AddProduct(comboPackNonConsumable, ProductType.NonConsumable);

        builder.AddProduct(eventPackNonConsumable, ProductType.NonConsumable);

        builder.AddProduct(utilityPackNonConsumable, ProductType.NonConsumable);



        //for promotions
        builder.AddProduct(starterPackNonConsumablePromo, ProductType.NonConsumable);

        builder.AddProduct(furyPackNonConsumablePromo, ProductType.NonConsumable);

        builder.AddProduct(arcanePackNonConsumablePromo, ProductType.NonConsumable);

        builder.AddProduct(comboPackNonConsumablePromo, ProductType.NonConsumable);


        //for consumables

        builder.AddProduct(gemsFist, ProductType.Consumable);

		builder.AddProduct(gemsPile, ProductType.Consumable);

		builder.AddProduct(gemsSack, ProductType.Consumable);

		builder.AddProduct(gemsCrate, ProductType.Consumable);

		builder.AddProduct(gemsChest, ProductType.Consumable);

		builder.AddProduct(gemsTon, ProductType.Consumable);

				//builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);

//				builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
//					{ kProductNameAppleSubscription, AppleAppStore.Name },
//					{ kProductNameGooglePlaySubscription, GooglePlay.Name },
//				});

				// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
				// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
				UnityPurchasing.Initialize(this, builder);
			}


			private bool IsInitialized()
			{
				// Only say we are initialized if both the Purchasing references are set.
				return m_StoreController != null && m_StoreExtensionProvider != null;
			}


			public void BuyGemFist()
			{
				BuyProductID(gemsFist);
			}
			
			public void BuyGemPile()
			{
				BuyProductID(gemsPile);
			}
			
			public void BuyGemSack()
			{
				BuyProductID(gemsSack);
			}
			
			public void BuyGemCrate()
			{
				BuyProductID(gemsCrate);
			}
			
			public void BuyGemChest()
			{
				BuyProductID(gemsChest);
			}
			
			public void BuyGemTon()
			{
				BuyProductID(gemsTon);
			}
			
	//no consumables - 1 time purchases
			public void BuyStarterPack()
			{
				BuyProductID(starterPackNonConsumable);
			}
			public void BuyFuryPack()
			{
				BuyProductID(furyPackNonConsumable);
			}
			public void BuyArcanePack()
			{
				BuyProductID(arcanePackNonConsumable);
			}
			public void BuyChemistPack()
			{
				BuyProductID(chemistPackNonConsumable);
			}
			public void BuyComboPack()
			{
				BuyProductID(comboPackNonConsumable);
			}

            public void BuyEventPack()
            {
                BuyProductID(eventPackNonConsumable);
            }

            public void BuyUtilityPack()
            {
                BuyProductID(utilityPackNonConsumable);
            }

    //for promos
    public void BuyStarterPackPromo()
            {
                BuyProductID(starterPackNonConsumablePromo);
            }
            public void BuyFuryPackPromo()
            {
                BuyProductID(furyPackNonConsumablePromo);
            }
            public void BuyArcanePackPromo()
            {
                BuyProductID(arcanePackNonConsumablePromo);
            }
            public void BuyComboPackPromo()
            {
                BuyProductID(comboPackNonConsumablePromo);
            }


    //			public void BuySubscription()
    //			{
    //				// Buy the subscription product using its the general identifier. Expect a response either 
    //				// through ProcessPurchase or OnPurchaseFailed asynchronously.
    //				// Notice how we use the general product identifier in spite of this ID being mapped to
    //				// custom store-specific identifiers above.
    //				BuyProductID(kProductIDSubscription);
    //			}


    void BuyProductID(string productId)
			{
				// If Purchasing has been initialized ...
				if (IsInitialized())
				{
					// ... look up the Product reference with the general product identifier and the Purchasing 
					// system's products collection.
					Product product = m_StoreController.products.WithID(productId);

					// If the look up found a product for this device's store and that product is ready to be sold ... 
					if (product != null && product.availableToPurchase)
					{
						Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
						// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
						// asynchronously.
						m_StoreController.InitiatePurchase(product);
					}
					// Otherwise ...
					else
					{
						// ... report the product look-up failure situation  
						Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
					}
				}
				// Otherwise ...
				else
				{
					// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
					// retrying initiailization.
					Debug.Log("BuyProductID FAIL. Not initialized.");
				}
			}


			// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
			// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
			public void RestorePurchases()
			{
				// If Purchasing has not yet been set up ...
				if (!IsInitialized())
				{
					// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
					Debug.Log("RestorePurchases FAIL. Not initialized.");
					return;
				}

				// If we are running on an Apple device ... 
				if (Application.platform == RuntimePlatform.IPhonePlayer || 
					Application.platform == RuntimePlatform.OSXPlayer)
				{
					// ... begin restoring purchases
					Debug.Log("RestorePurchases started ...");

					// Fetch the Apple store-specific subsystem.
					var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
					// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
					// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
					apple.RestoreTransactions((result) => {
						// The first phase of restoration. If no more responses are received on ProcessPurchase then 
						// no purchases are available to be restored.
						Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
					});
				}
				// Otherwise ...
				else
				{
					// We are not running on an Apple device. No work is necessary to restore purchases.
					Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
				}
			}


			//  
			// --- IStoreListener
			//

			public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
			{
				// Purchasing has succeeded initializing. Collect our Purchasing references.
				Debug.Log("OnInitialized: PASS");

				// Overall Purchasing system, configured with products for this application.
				m_StoreController = controller;
				// Store specific subsystem, for accessing device-specific store features.
				m_StoreExtensionProvider = extensions;
			}


			public void OnInitializeFailed(InitializationFailureReason error)
			{
				// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
				Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
			}


			public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
			{

				refreshListOfPurchasables ();//this is so the list is updated if a new scene is loaded and the objects were destroyed. Kind of a work around...

				// A consumable product has been purchased by this user.
				if (String.Equals(args.purchasedProduct.definition.id, gemsFist, StringComparison.Ordinal))
						
						{
							Debug.Log(string.Format("ProcessPurchase gemsfist: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[0].afPurchaseIsConsumable(gemsFist, "4.99");
                            gemsList[0].giveGems ();

						}

				else if (String.Equals(args.purchasedProduct.definition.id, gemsPile, StringComparison.Ordinal))
						
						{
							Debug.Log(string.Format("ProcessPurchase gemspile: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[1].afPurchaseIsConsumable(gemsPile, "9.99");
                            gemsList[1].giveGems ();

						}

				else if (String.Equals(args.purchasedProduct.definition.id, gemsSack, StringComparison.Ordinal))
						
						{
							Debug.Log(string.Format("ProcessPurchase gemsSack: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[2].afPurchaseIsConsumable(gemsSack, "14.99");
                            gemsList[2].giveGems ();

						}

				else if (String.Equals(args.purchasedProduct.definition.id, gemsCrate, StringComparison.Ordinal))
						
						{
							Debug.Log(string.Format("ProcessPurchase gemsCrate: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[3].afPurchaseIsConsumable(gemsCrate, "19.99");
                            gemsList[3].giveGems ();

						}

				else if (String.Equals(args.purchasedProduct.definition.id, gemsChest, StringComparison.Ordinal))
					
						{
							Debug.Log(string.Format("ProcessPurchase gemsChest: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[4].afPurchaseIsConsumable(gemsChest, "29.99");
                            gemsList[4].giveGems ();

						}
			
				else if (String.Equals(args.purchasedProduct.definition.id, gemsTon, StringComparison.Ordinal))
					
						{
							Debug.Log(string.Format("ProcessPurchase gemsTon: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                            gemsList[5].afPurchaseIsConsumable(gemsTon, "49.99");
                            gemsList[5].giveGems ();

						}

		//--------------------------------------------------this is for the packages now

				
				            else if (String.Equals(args.purchasedProduct.definition.id, starterPackNonConsumable, StringComparison.Ordinal)) 

						    {
						    Debug.Log(string.Format("ProcessPurchase starterPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[0].afPurchaseNonConsume(starterPackNonConsumable,"4.99");
                                packagesList[0].purchaseStarterPack ();

						    }

                            else if (String.Equals(args.purchasedProduct.definition.id, starterPackNonConsumablePromo, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase starterPackNonConsumablePromo: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[0].afPurchaseNonConsume(starterPackNonConsumablePromo, "3.99");
                                packagesList[0].purchaseStarterPack();

                            }

                            else if (String.Equals(args.purchasedProduct.definition.id, furyPackNonConsumable, StringComparison.Ordinal))

						    {
							    Debug.Log(string.Format("ProcessPurchase furyPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[2].afPurchaseNonConsume(furyPackNonConsumable, "5.99");
                                packagesList[2].purchaseFuryPack ();

						    }

                            else if (String.Equals(args.purchasedProduct.definition.id, furyPackNonConsumablePromo, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase furyPackNonConsumablePromo: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[2].afPurchaseNonConsume(furyPackNonConsumablePromo, "4.99");
                                packagesList[2].purchaseFuryPack();
                            }

                            else if (String.Equals(args.purchasedProduct.definition.id, arcanePackNonConsumable, StringComparison.Ordinal))
				
						    {
							    Debug.Log(string.Format("ProcessPurchase arcanePackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[1].afPurchaseNonConsume(arcanePackNonConsumable, "5.99");
                                packagesList[1].purchaseArcanePack ();
						    }

                            else if (String.Equals(args.purchasedProduct.definition.id, arcanePackNonConsumablePromo, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase arcanePackNonConsumablePromo: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[1].afPurchaseNonConsume(arcanePackNonConsumablePromo, "4.99");
                                packagesList[1].purchaseArcanePack();
                            }

                            else if (String.Equals(args.purchasedProduct.definition.id, chemistPackNonConsumable, StringComparison.Ordinal))

						    {
							    Debug.Log(string.Format("ProcessPurchase chemistPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[3].afPurchaseNonConsume(chemistPackNonConsumable, "5.99");
                                packagesList[3].purchaseChemistPack ();
						    }

				            else if (String.Equals(args.purchasedProduct.definition.id, comboPackNonConsumable, StringComparison.Ordinal))

						    {
							    Debug.Log(string.Format("ProcessPurchase comboPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[4].afPurchaseNonConsume(comboPackNonConsumable, "14.99");
                                packagesList[4].purchaseComboPack ();
						    }

                            else if (String.Equals(args.purchasedProduct.definition.id, comboPackNonConsumablePromo, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase comboPackNonConsumablePromo: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[4].afPurchaseNonConsume(comboPackNonConsumablePromo, "8.99");
                                packagesList[4].purchaseComboPack();
                            }

                            else if (String.Equals(args.purchasedProduct.definition.id, eventPackNonConsumable, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase eventPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[5].afPurchaseNonConsume(eventPackNonConsumable, "4.99");
                                packagesList[5].purchaseEventPack();
                            }

                            else if (String.Equals(args.purchasedProduct.definition.id, utilityPackNonConsumable, StringComparison.Ordinal))

                            {
                                Debug.Log(string.Format("ProcessPurchase utilityPackNonConsumable: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

                                packagesList[6].afPurchaseNonConsume(utilityPackNonConsumable, "3.99");
                                packagesList[6].purchaseUtilityPack();
                            }

                            else 
			
					        {
							    Debug.Log(string.Format("ProcessPurchase : Fail unrecognized product. Product: '{0}'", args.purchasedProduct.definition.id));

					        }

				// Return a flag indicating whether this product has completely been received, or if the application needs 
				// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
				// saving purchased products to the cloud, and when that save is delayed. 
				return PurchaseProcessingResult.Complete;
			}


			public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
			{
				// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
				// this reason with the user to guide their troubleshooting actions.
				Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
			}

	public void refreshListOfPurchasables(){

		//because this is an instance, so... I think its not destroyed on scene load, but the other purchasable objects in its list are destroyed... but this keeps the objects in the list. 
		//So they need to be refreshed before each purchase so that when a new scene is loaded the proper objects are in the list...
	
		for (int i = 0; i < packagesList.Count; i++) {
			packagesList [i] = Camera.main.GetComponent<MainMenu> ().buyPackageList [i];

		}

		for (int j = 0; j < gemsList.Count; j++) {

			gemsList [j] = Camera.main.GetComponent<MainMenu> ().buyGemsList [j];
		}
	}
		}
