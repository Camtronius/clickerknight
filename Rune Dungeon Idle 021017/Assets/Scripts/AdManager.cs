﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;


public class AdManager : MonoBehaviour {

	public string gameID;

	public bool idleType=false;
    public bool dungeonType = false;

    public bool goldType=false;
	public bool silverType=false;
	public bool purpleType=false;
	public bool orangeType=false;
    public bool whiteType = false;
    public bool freeGemType = false;


    public bool showingAd = false;

	public BrewPotion goldBrewPotScript;
	public BrewPotion silvBrewPotScript;

	public BrewPotion purpBrewPotScript;
	public BrewPotion orangeBrewPotScript;

    public BrewPotion whiteBrewPotScript;
    public GemPerDay freeGemDayScript;


    public IdleTimeAway idleScript;
    public CheckLevelsComplete levelCompleteScript;




	void Start(){
		//Advertisement.Initialize(gameID);
	//	print ("ad should be initialized");
	//	print ("the unity ad version is : " + Advertisement.version);

	}

	public void showUnityAd(string placementID, bool enableCallback){

		showingAd = true;

		if (ObscuredPrefs.GetInt ("ArcanePack") == 0) {
			
			if (!Advertisement.IsReady (placementID)) {

			//	print ("ad was not ready");
			} else {

				ShowOptions options = new ShowOptions ();

				//	if(enableCallback){

				options.resultCallback = onAdResult;

				Advertisement.Show (placementID, options);

                //	print ("ad should be playing");

            //using code below for appsflyer testing
                Dictionary<string, string> eventValue = new Dictionary<string, string>();
                eventValue.Add("ad_provider", "Unity");
                AppsFlyer.trackRichEvent("ad_watched", eventValue);
                //print("showing apps flyer ad");
                //showsing the apps flyer ad
            }

        } else {

			onAdResult (ShowResult.Finished);
		}

	}

	private void onAdResult(ShowResult result){

		switch (result) {

		case ShowResult.Finished:
			//print ("ad was showed from start to end");

			if (idleType == true) {
				//print ("an idle reward should be given now boi!");
				idleScript.applyIdleAdBonus (); //X2 gold for watching an ad when return from idle.
				showingAd = false;

				//call an idle method
			}

            if (dungeonType == true)
            {
              //  print("a dungeon should be unlocked now boi!");
                levelCompleteScript.unlockBonusDungeon(); //X2 gold for watching an ad when return from idle.
                showingAd = false;

            }

            if (goldType == true) {
			//print ("an gold potion reward should be given now boi!");
			goldBrewPotScript.brewGoldSilvPot ();//it checks whether its gold or silver based on the potion designation
			showingAd = false;

			}

			if (silverType == true) {
				//print ("an silver potion reward should be given now boi!");
				silvBrewPotScript.brewGoldSilvPot (); //it checks whether its gold or silver based on the potion designation
				showingAd = false;

			}

			if (purpleType == true) {
				//print ("an silver potion reward should be given now boi!");
				purpBrewPotScript.brewGoldSilvPot ();//it checks whether its gold or silver based on the potion designation
				showingAd = false;

			}

			if (orangeType == true) {
				//print ("an silver potion reward should be given now boi!");
				orangeBrewPotScript.brewGoldSilvPot ();//it checks whether its gold or silver based on the potion designation
				showingAd = false;

			}

            if (whiteType == true)
            {
                //print ("an silver potion reward should be given now boi!");
                whiteBrewPotScript.brewGoldSilvPot();//it checks whether its gold or silver based on the potion designation
                showingAd = false;

            }

            if (freeGemType == true)
            {
                //print ("an silver potion reward should be given now boi!");
                //whiteBrewPotScript.brewGoldSilvPot();//it checks whether its gold or silver based on the potion designation
                whiteBrewPotScript.potionMaster.masterIngredientsHolder.totalGreenGems += 1;
                whiteBrewPotScript.potionMaster.masterIngredientsHolder.setNewPrefs(); //saves the gems
                    freeGemDayScript.rewardAnim();//shows the anim and plays the sound
                showingAd = false;

            }


                break;

		case ShowResult.Skipped:
			//print ("ad was skipped");
			showingAd = false;

			break;
		case ShowResult.Failed:
			//print ("ad failed to show!");
			showingAd = false;

			break;


		}

	}

	public void setGoldType(){

		resetAllTypes ();
		goldType = true;


	}

	public void setSilverType(){

		resetAllTypes ();
		silverType = true;

	}

	public void setIdleType(){

		resetAllTypes ();
		idleType = true;


	}

    public void setDungeonType()
    {

        resetAllTypes();
        dungeonType = true;


    }

    public void setPurpleType(){

		resetAllTypes ();
		purpleType = true;


	}

    public void setWhiteType()
    {

        resetAllTypes();
        whiteType = true;


    }

    public void setOrangeType(){

		resetAllTypes ();
		orangeType = true;


	}

    public void setFreeGemType()
    {

        resetAllTypes();
        freeGemType = true;


    }

    public void resetAllTypes(){

	idleType=false;
    dungeonType = false;

	goldType=false;
	silverType=false;
	purpleType=false;
	orangeType=false;
    whiteType=false;
    freeGemType=false;

    }
}
