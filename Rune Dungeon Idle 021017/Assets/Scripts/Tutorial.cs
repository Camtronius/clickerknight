﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

	private static Tutorial instance = null;

	public GameObject mainMenuFinger;

	public bool isTutorial = false;

	public bool villageUnderAttack = false;
	public bool castFireSpell = false;

	//for main menu
	public GameObject QuestButton; //this is the actual selection of levels
	public GameObject QuestDisplay; //this is the actual selection of levels

	public GameObject LV1;

	public GameObject MagicButton;
	public GameObject EquipButton;
	public GameObject StatsButton;
	public GameObject PvpButton;

	public GameObject TutorialGameObject;

	

	// Use this for initialization
	void Awake() {

//		print ("Enemy Stats Inheriter is awake!");
		
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
		
	}

	void Start () {
	
		//for the first time playing

		if (PlayerPrefs.GetFloat ("Level1") == 0) {
			if (Camera.main.GetComponent<MainMenu> () != null) { //and first time playing?

				MagicButton.GetComponentInParent<Button> ().interactable = false;
				EquipButton.GetComponentInParent<Button> ().interactable = false;
				StatsButton.GetComponentInParent<Button> ().interactable = false;
				PvpButton.GetComponentInParent<Button> ().interactable = false;

			}

			StartCoroutine ("ClickOnQuest");
		}

	}
	

	public void closeBeginningDialogue(){
		
	
	/*	
		if(villageUnderAttack = true){
			
			villageUnderAttack = false;
		}
		
		TutorialGameObject.SetActive (false);
	//	TutorialObject.castFireSpell = true;
	*/	
	}

	IEnumerator ClickOnQuest(){
		
		while (true) {
			
			if(mainMenuFinger.activeSelf==false){
				mainMenuFinger.SetActive(true);
			}
			if(QuestDisplay.activeSelf==true){
				QuestButton.GetComponentInParent<Button>().interactable = false;
				mainMenuFinger.transform.position = LV1.transform.position;
				villageUnderAttack = true;
				
			}
			
			yield return new WaitForSeconds(0.25f);
			
		}
		
	}

}
