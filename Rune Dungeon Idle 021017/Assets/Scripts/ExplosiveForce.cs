﻿using UnityEngine;
using System.Collections;

public class ExplosiveForce : MonoBehaviour {

	public CircleCollider2D explosiveCollider;
	public float explosionColliderRadius = 1.5f;
	public float explosionExpansionTime = .05f;
	public int explosionMultiplier = 10;

	// Use this for initialization
	void Start () {
	


	}
	
	// Update is called once per frame
	void Update () {
	
		if (explosiveCollider.radius < explosionColliderRadius+.1f) {

			explosiveCollider.radius+=explosionExpansionTime;

			if(explosiveCollider.radius>=explosionColliderRadius){

				Destroy(this.gameObject);
			}
		}

	}

	void OnTriggerEnter2D (Collider2D col) {

		var dir = (col.transform.position - this.transform.position);
		if (col.GetComponent<Rigidbody2D> () != null) {
			col.GetComponent<Rigidbody2D> ().AddForce (dir.normalized * explosionMultiplier);
		}
	//add force to rigidbody in the normal of the direction of the explosion vector.

	}
}
