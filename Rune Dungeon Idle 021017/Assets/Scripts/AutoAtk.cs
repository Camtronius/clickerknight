﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AutoAtk : MonoBehaviour {

	public int createdByPlayer = 0;

	public GameObject ParticleExplosion;
	public GameObject ExplosiveForce;
	public GameObject ScreenShaker;
	private GameObject SpellCreator;
	public float timeAlive;

	// Use this for initialization
	void Start () {

		ScreenShaker = GameObject.Find ("ScreenShakerController");
		SpellCreator = GameObject.Find ("SpellListener");


	}

	void Update(){

		timeAlive += Time.deltaTime;
		
		if(timeAlive>4){
			
			SpellCreator.GetComponent<SpellCreator>().spellCompleted=true;
			
			SpellCreator.GetComponent<SpellCreator>().CounterAttackSpell(createdByPlayer);
			
			Destroy(this.gameObject);
			
		}


	}

	void OnCollisionEnter2D (Collision2D player) {
		
		
		if (player.gameObject.tag == "Player2" && createdByPlayer==1) {

			if(player.gameObject.GetComponent<ControlPlayer2Character>().isShielded==false){
				
				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.02f); //shakes the screen
				
				float calculatedHealthLoss = (5 + PlayerPrefs.GetFloat ("AutoAtkDmgMod"));
									
				player.gameObject.GetComponent<ControlPlayer2Character>().playerHealthNew -= calculatedHealthLoss;
			}





			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
			GameObject ExplosiveObj = (GameObject)Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.gameObject.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity);
			ExplosiveObj.GetComponent<ExplosiveForce> ().explosionMultiplier = 50;

		}
		
		if (player.gameObject.tag == "Player1" && createdByPlayer==2) {

			if(player.gameObject.GetComponent<ControlPlayer1Character>().isShielded==false){
				
				ScreenShaker.GetComponent<ScreenShakerController> ().activateScreenShake (2.0f,0.02f); //shakes the screen
				
				
				float calculatedHealthLoss = (PlayerPrefs.GetFloat ("AutoAtkDmgMod")-5);

	
					player.gameObject.GetComponent<ControlPlayer1Character>().playerHealthNew -= calculatedHealthLoss;

			}






			player.gameObject.GetComponent<TurnOnRagdoll> ().enabled = true; //sets the player to a ragdoll
			player.gameObject.GetComponent<TurnOnRagdoll> ().createRagDoll();
			GameObject ExplosiveObj = (GameObject)Instantiate (ExplosiveForce, new Vector3 (player.transform.position.x, player.gameObject.GetComponent<TurnOnRagdoll>().LeftLegLower.transform.position.y, 0), Quaternion.identity);
			ExplosiveObj.GetComponent<ExplosiveForce> ().explosionMultiplier = 50;
		}
		
	}

}
