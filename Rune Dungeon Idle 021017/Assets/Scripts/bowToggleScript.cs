﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bowToggleScript : MonoBehaviour {

	// Use this for initialization

	//public bool bowDisabler = false;

	public Sprite SoundOffSprite;
	public Sprite SoundOnSprite;
    public MainMenu cameraObj;


	void Start () {

        cameraObj = Camera.main.GetComponent<MainMenu>();

			if (PlayerPrefs.GetInt ("BowEnabled") == 1) {
			
				this.gameObject.GetComponent<Image> ().sprite = SoundOnSprite;

           // print("bow is enabled");
			
			} else if (PlayerPrefs.GetInt ("BowEnabled") == 0) {
			
				this.gameObject.GetComponent<Image> ().sprite = SoundOffSprite;

          //  print("bow is disabled");

            }
		
	}

	public void Togglebow(){


		if (this.gameObject.GetComponent<Image> ().sprite == SoundOnSprite) {
			this.gameObject.GetComponent<Image>().sprite = SoundOffSprite;
          //  print("bow disabled");
            PlayerPrefs.SetInt("BowEnabled", 0);

            cameraObj.resetBowAnimBools(true);
            cameraObj.usingBow = false;
            cameraObj.animatedPlayerObj.animator.SetBool("usingBow", false);



        }
        else {
			this.gameObject.GetComponent<Image>().sprite = SoundOnSprite;
         //   print("bow enabled");
            PlayerPrefs.SetInt("BowEnabled", 1);

            cameraObj.resetBowAnimBools(false);
            cameraObj.usingBow = true;
            cameraObj.animatedPlayerObj.animator.SetBool("usingBow", true);


        }
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().BackButton();

    }

}
