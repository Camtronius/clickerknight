﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class adRewardsGift : MonoBehaviour {

	// Use this for initialization

	public List<GameObject> rewardType = new List<GameObject> ();
	public GameObject yourGiftWindow; 

	public enum RewardType {Stat,Gem,Talent,Gold};
	public RewardType _rewardType;
	//0 is stat reward
	//1 is gem reward
	//2 is talent reward
	//3 is gold reward
	public GameObject  goldAmountRewarded;
	public IngredientsMaster ingredMasterScript;


	void Start () {

		//for testing 

	}

	void OnEnable(){

		Camera.main.GetComponent<MainMenu> ().windowOpen = true;


	}

	public void CloseGiftWindow(){ //apply rewards

		applyRewards ();
		yourGiftWindow.SetActive (false);
	//	Camera.main.GetComponent<MainMenu> ().windowOpen = false;
		this.gameObject.GetComponent<Animator> ().ResetTrigger("OpenPresent");

		this.gameObject.SetActive (false);

	}

	public void openGift(){
		this.gameObject.GetComponent<Animator> ().SetTrigger ("OpenPresent");

	}

    public void giftAppearSound()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UpgradeSpell();

    }

    public void giftOpenSound()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().openBag();

    }

    public void giftShownSound()
    {
        Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager>().UnlockTalent();

    }

    public void setReward(){

		int Reward = PlayerPrefs.GetInt ("RewardIteration"); //gets the lastr used reward type. 0 if never used before :)

	//	print ("The reward int is! : " + Reward);

		for (int i = 0; i < rewardType.Count; i++) { //resets all the reward types

			rewardType [i].SetActive (false);
		}

		rewardType [Reward].SetActive (true);

	//	print ("Setting this reward active : " + rewardType [Reward]);


		switch (Reward) { //this is for choosing the reward to give, not the window. See apply rewards method below

		case 0:
			//stat reward
			_rewardType = RewardType.Stat;

			break;
		case 1:
			////gem reward
			_rewardType = RewardType.Gem;

			break;
		case 2:
			//talent reward
			_rewardType = RewardType.Talent;

			break;
		case 3:
			//gold reward
			_rewardType = RewardType.Gold;

			EnemyStatsInheriter statsInheritScript = Camera.main.GetComponent<MainMenu> ().enemyStatsObj;

			goldAmountRewarded.GetComponent<TextMeshProUGUI>().text = LargeNumConverter.setDmg((double)statsInheritScript.data [1] ["GOLD"]*20); //around 7-10 enemy deaths worth of gold

			break;

		}

		if (Reward + 1 > 3) { //checks if the reward goes out of range

			Reward = 0;

			PlayerPrefs.SetInt ("RewardIteration", Reward); //cycles around again for the reward


		} else {
			PlayerPrefs.SetInt ("RewardIteration", Reward+1); //iterates for the next reward

		}

	}

	public void applyRewards(){

		switch(_rewardType){

		case RewardType.Stat:

			PlayerPrefs.SetFloat("SkillPoints",PlayerPrefs.GetFloat("SkillPoints")+1);

			break;

		case RewardType.Gem:
			//ads 10 total gems to the players inventory
			ingredMasterScript.totalRedGem += 10;
			ingredMasterScript.totalBlueGem += 10;
			ingredMasterScript.totalYellowGem += 10;
			ingredMasterScript.setNewPrefs ();


			break;

		case RewardType.Talent:

			PlayerPrefs.SetInt ("ResearchPoints", PlayerPrefs.GetInt ("ResearchPoints") + 1);

			break;

		case RewardType.Gold:

			EnemyStatsInheriter statsInheritScript = Camera.main.GetComponent<MainMenu> ().enemyStatsObj;

			double goldAwarded = ((double)statsInheritScript.data [1] ["GOLD"])*20; //around 7-10 enemy deaths worth of gold

			GameObject.FindObjectOfType<ControlPlayer1Character> ().playerGold += goldAwarded; //this is set by the SetGold Method
	
			break;

		}

	
	
	}
}
