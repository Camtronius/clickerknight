﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelUpCountdwn : MonoBehaviour {

	public TextMeshProUGUI countdown;
	public GameObject lvUpBtn;
	public int closeTime;

	// Use this for initialization
	void OnEnable () {
		closeTime = 15;
		StartCoroutine ("SellCountdown");
	}

	void OnDisable(){

	}

	IEnumerator SellCountdown(){ //***make sure this is only called once per game and doesnt repeatedly get called

		while (true) {

			yield return new WaitForSeconds(1f);
			closeTime -= 1;
	//		print ("sell coroutine running!!asdja;ldk");
			countdown.text = "This will close in " + closeTime.ToString() + " seconds";

			if (closeTime <= 0) {
				this.gameObject.SetActive (false);
				lvUpBtn.SetActive (false);
				closeTime = 15;
				Camera.main.GetComponent<MainMenu> ().windowOpen = false;
                Camera.main.GetComponent<MainMenu>().checkIfEggAlive();

            }

		}
	}
}
