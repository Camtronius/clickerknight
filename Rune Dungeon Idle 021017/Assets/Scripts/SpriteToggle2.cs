﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteToggle2 : MonoBehaviour {

	public Sprite ToggleOffSprite;
	public Sprite ToggleOnSprite;

	void Start () {
		
	}
	
	// Update is called once per frame
	public void clickedMe(){


	}

	public void toggleSprites(){

		if (this.gameObject.GetComponent<Image> ().sprite == ToggleOnSprite) { //if sprite is on turn off

			//setToDefaultSprite ();
		//	Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound
		//	print("toggling sprite on");

		} else {

			this.gameObject.GetComponent<Image>().sprite = ToggleOnSprite;
			Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager> ().BackButton (); //plays back btrn sound
		//	print("toggling sprite off");

		}

	}

	public void setToDefaultSprite(){
		this.gameObject.GetComponent<Image>().sprite = ToggleOffSprite;
	//	print("setting sprite to default");

	}


}
