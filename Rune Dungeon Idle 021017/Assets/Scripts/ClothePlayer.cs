﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClothePlayer : MonoBehaviour {

	// Use this for initialization
	public GameObject Head, Torso, Hair,
					UpperLeftArm,LowerLeftArm,LeftHand,
					LeftLegUpper,LeftLegLower,LeftFoot,
					UpperRightArm,LowerRightArm, RightHand,
					RightLegUpper,RightLegLower,RightFoot, Cape, Sword;

	public Inventory _InventoryScript;



	public Dictionary<string, Sprite> dictSprites = new Dictionary<string, Sprite>();

	public List <Texture2D> CapeTextures = new List<Texture2D> (); //4 index list of the blocks that are selected

	public List <GameObject> NeckList = new List<GameObject> ();  //0 is RoyalNeck, 1 is Knight Neck, 2 is DragonNeck, 3 is wolf

	public bool TurnOnClothes=false;

	void Start()
	{

	}

	void OnEnable(){
		if (dictSprites.Count == 0) {
			Sprite[] sprites = Resources.LoadAll<Sprite> ("NewArmor");

			foreach (Sprite sprite in sprites) {
				dictSprites.Add (sprite.name, sprite);
			}
		}

		enableClothes ();
	}

	public void enableClothes(){

		if (this.gameObject.tag == "Player1") {
			putOnClothes ();
		} else if(this.gameObject.tag == "AnimatedPlayer") {

			//this is just to test that all the clothes are wearable. Comment out after you are done. You have to comment out the script that loads the mods to test this

//			for (int i = 0; i < 6; i++) {
//				for (int j = 0; j < 6; j++) {
//					_InventoryScript.InventoryList [i].armorLevel = j;
//					putOnClothesAnimated ();
//
//				}
//
//			}
//
//			//this is to test all the swords are usable
//
//			for (int k = 0; k < 9; k++) {
//
//				PlayerPrefs.SetInt ("Sword", k);
//
//				putOnClothesAnimated ();
//
//			}


			putOnClothesAnimated ();

		}
	}

	public void SwordPuff(){

		Camera.main.GetComponent<MainMenu> ().ActivateSwordPuff ();

	}

	public void SwordSlash(){

		Camera.main.GetComponent<MainMenu> ().ActivateSwordSlash ();

	}

	public void putOnClothes(){

		_InventoryScript.loadDict (); //preloads the sprites for the hero.

		//you need to load all the values for the clothes so when you load the sprites it gets what the player actually has instead of the placeholder
		for (int i = 0; i < _InventoryScript.InventoryList.Count; i++) {

			_InventoryScript.InventoryList[i].loadMods();

			//these armor level things will have to be colored eventually
		}

		if (TurnOnClothes == true && Camera.main.GetComponent<CampaignGameOrganizer> () != null 
		    || TurnOnClothes == true && Camera.main.GetComponent<MainMenu>()!=null) {


			for (int z = 0; z < NeckList.Count; z++) {

				NeckList [z].SetActive (false);

			}

//			print ("head case is " + PlayerPrefs.GetInt ("head").ToString ());
			
			switch (_InventoryScript.InventoryList[0].armorLevel) { //head
				
			case 0:
				break;
			case 1:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
			//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["ShortBrownHair"]; //hair sprite?
				
				break;
			case 2:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				break;
			case 3:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				
				break;
			case 4:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
	//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				
				break;
			case 5:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonHelmWorn"]; //use this to set a sprite to a specific sprite in the dict.
			//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				break;

			}
			
			switch (_InventoryScript.InventoryList[1].armorLevel) {
			case 0:
				break;
			case 1:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceTorso"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[3].SetActive(true);
				break;
			case 3:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[0].SetActive(true);
				break;
			case 4:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[1].SetActive(true);

				break;
			case 5:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[2].SetActive(true);

				break;

			}
			
			switch (_InventoryScript.InventoryList[2].armorLevel) { //remeber the upper arms are switched right is left and vise versa
			case 0:
				break;
			case 1:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
			//	LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerLeftArm"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.
				
				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				//LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerRightArm"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}
			
			switch (_InventoryScript.InventoryList[3].armorLevel) {
			case 0:
				break;
			case 1:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeggings"]; //use this to set a sprite to a specific sprite in the dict.
				//LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegUpper"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_LeftLegLower"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_RightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeggings"]; //use this to set a sprite to a specific sprite in the dict.
//				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeggings"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonBoots"]; //use this to set a sprite to a specific sprite in the dict.
				
//				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthUpperRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthLowerRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}

			Cape.gameObject.GetComponent<Renderer>().sortingLayerName = "PlayerCampaign";

			switch (_InventoryScript.InventoryList[4].armorLevel) {
			case 0:
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4; //was -10
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [0]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 1://novice
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [1]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2://wolf
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [2]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3://royal
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [3]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4://dragon
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [4]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -4;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [5]; //use this to set a sprite to a specific sprite in the dict.
			//	print ("the player should be wearing the leaf cape!!");
				break;

			}

//			print ("the starting player int for sword is " + PlayerPrefs.GetInt ("Sword").ToString());


			switch (PlayerPrefs.GetInt ("Sword")) { //for the sword

			case 0:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv1"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 1:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv2"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 2:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 6:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 7:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 8:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			}


		}
	}

	public void putOnClothesAnimated(){

		_InventoryScript.loadDict (); //preloads the sprites for the hero.

	//	print ("putting on animated clothese!");
		//you need to load all the values for the clothes so when you load the sprites it gets what the player actually has instead of the placeholder

		for (int i = 0; i < _InventoryScript.InventoryList.Count; i++) {

			_InventoryScript.InventoryList[i].loadMods();

			//these armor level things will have to be colored eventually
		}

		if (TurnOnClothes == true && Camera.main.GetComponent<CampaignGameOrganizer> () != null 
			|| TurnOnClothes == true && Camera.main.GetComponent<MainMenu>()!=null) {


			for (int z = 0; z < NeckList.Count; z++) {

				NeckList [z].SetActive (false);

			}

			//			print ("head case is " + PlayerPrefs.GetInt ("head").ToString ());

			switch (_InventoryScript.InventoryList[0].armorLevel) { //head

			case 0:
				break;
			case 1:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceAnimatedHead"]; //use this to set a sprite to a specific sprite in the dict.
				//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["ShortBrownHair"]; //hair sprite?

				break;
			case 2:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfAnimatedHead"]; //use this to set a sprite to a specific sprite in the dict.
				//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;
				break;
			case 3:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalAnimatedHead"]; //use this to set a sprite to a specific sprite in the dict.
				//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;

				break;
			case 4:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightAnimatedHead"]; //use this to set a sprite to a specific sprite in the dict.
				//			Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;

				break;
			case 5:
				Head.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonAnimatedHead"]; //use this to set a sprite to a specific sprite in the dict.
				//	Hair.gameObject.GetComponent<SpriteRenderer> ().sprite = null;

				break;

			}

			switch (_InventoryScript.InventoryList[1].armorLevel) {
			case 0:
				//this is for the default, when player starts with nada
				NeckList[4].SetActive(true);

				break;
			case 1:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[4].SetActive(true);
				break;
			case 2:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[3].SetActive(true);
				break;
			case 3:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[0].SetActive(true);
				break;
			case 4:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[1].SetActive(true);

				break;
			case 5:
				Torso.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonTorso"]; //use this to set a sprite to a specific sprite in the dict.
				NeckList[2].SetActive(true);

				break;

			}

			switch (_InventoryScript.InventoryList[2].armorLevel) { //remeber the upper arms are switched right is left and vise versa
			case 0:
				break;
			case 1:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceRightArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceRightArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfRightArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfRightArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalRightArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalRightArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightRightArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightRightArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				UpperLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerLeftArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				LeftHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftGauntlet"]; //use this to set a sprite to a specific sprite in the dict.

				UpperRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonRightArmUpper"]; //use this to set a sprite to a specific sprite in the dict.
				LowerRightArm.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonRightArmLower"]; //use this to set a sprite to a specific sprite in the dict.
				RightHand.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonGauntletsFound"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}

			switch (_InventoryScript.InventoryList[3].armorLevel) {
			case 0:
				break;
			case 1:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceLeftBoot"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceBoots"]; //use this to set a sprite to a specific sprite in the dict.

				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["NoviceRightBoot"]; //use this to set a sprite to a specific sprite in the dict.
				//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_RightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfLeftBoot"]; //use this to set a sprite to a specific sprite in the dict.
		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfBoots"]; //use this to set a sprite to a specific sprite in the dict.

				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfRightBoot"]; //use this to set a sprite to a specific sprite in the dict.
						//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_FireRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalLeftBoot"]; //use this to set a sprite to a specific sprite in the dict.

		//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalRightBoot"]; //use this to set a sprite to a specific sprite in the dict.			//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightLeftBoot"]; //use this to set a sprite to a specific sprite in the dict.

				//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["KnightRightBoot"]; //use this to set a sprite to a specific sprite in the dict.			//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
							//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WindRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				LeftLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftLeg"]; //use this to set a sprite to a specific sprite in the dict.
				LeftLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonLeftBoot"]; //use this to set a sprite to a specific sprite in the dict.

				//		LeftFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalBoots"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegUpper.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonRightLeg"]; //use this to set a sprite to a specific sprite in the dict.
				RightLegLower.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonRightBoot"]; //use this to set a sprite to a specific sprite in the dict.			//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_WaterRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
						//				RightFoot.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["LV1_EarthRightFoot"]; //use this to set a sprite to a specific sprite in the dict.
				break;

			}

			Cape.gameObject.GetComponent<Renderer>().sortingLayerName = "PlayerCampaign";

			switch (_InventoryScript.InventoryList[4].armorLevel) {
			case 0:
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15; //was -10
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [0]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 1://novice
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [1]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 2://wolf
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [2]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3://royal
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [3]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4://dragon
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [4]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				Cape.gameObject.GetComponent<Renderer>().sortingOrder = -15;
				Cape.gameObject.GetComponent<Renderer> ().material.mainTexture = CapeTextures [5]; //use this to set a sprite to a specific sprite in the dict.
				//	print ("the player should be wearing the leaf cape!!");
				break;

			}

			//			print ("the starting player int for sword is " + PlayerPrefs.GetInt ("Sword").ToString());


			switch (PlayerPrefs.GetInt ("Sword")) { //for the sword

			case 0:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv1"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 1:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv2"]; //lowercase because the playerpref is also the same "sword"
				break;
			case 2:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["WolfSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 3:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 4:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 5:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["RoyalSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 6:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv1"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 7:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv2"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			case 8:
				Sword.gameObject.GetComponent<SpriteRenderer> ().sprite = dictSprites ["DragonSwordLv3"]; //use this to set a sprite to a specific sprite in the dict.
				break;
			}


		}
	}

}
