﻿using UnityEngine;
using System.Collections;

public class GhostMeteor : MonoBehaviour {


	private GameObject GameBoard;
	public int ghostIndex;


	// Use this for initialization
	void Start () {
	
		GameBoard = GameObject.Find ("GameBoardObject");


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D (Collider2D rune) {

	//	makes any rune that is touching the ghost collider take the index of the ghost rune. This is so I can save the indicies of new blocks
		if (Camera.main.GetComponent<TurnedBasedGameOrganizer> () != null) {

			ghostIndex = Camera.main.GetComponent<TurnedBasedGameOrganizer> ().ghostMeteorList.IndexOf (this.gameObject); //determines its own index

			int runeIndex = Camera.main.GetComponent<TurnedBasedGameOrganizer>().meteorList.IndexOf (rune.GetComponent<MeteorCode>()); //determines index of touching rune

			if(Camera.main.GetComponent<TurnedBasedGameOrganizer>().meteorList[runeIndex]!=null){

			Camera.main.GetComponent<TurnedBasedGameOrganizer>().meteorList[runeIndex].GetComponent<MeteorCode> ().indexSpawned = ghostIndex;
			}
		//	print ("ghost index copied for meteor index ---------------------------------" + ghostIndex);

		}
		if (Camera.main.GetComponent<CampaignGameOrganizer> () != null) {

			ghostIndex = Camera.main.GetComponent<CampaignGameOrganizer> ().ghostMeteorList.IndexOf (this.gameObject);

			int runeIndex = Camera.main.GetComponent<CampaignGameOrganizer>().meteorList.IndexOf (rune.GetComponent<MeteorCode>());

			if(Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[runeIndex]!=null){
			Camera.main.GetComponent<CampaignGameOrganizer>().meteorList[runeIndex].GetComponent<MeteorCode> ().indexSpawned = ghostIndex;
			}
		}




	}
}
