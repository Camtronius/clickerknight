﻿using UnityEngine;
using System.Collections;

public class DestroyTornado : MonoBehaviour {

	public GameObject SpellCreator; //this is for the isSpellComplete condition
	public int createdByPlayer = 0;

	public GameObject FloorObj; //this is for the destruction of the sprite when it goes below this y pos
	public float destroyTime = 5f;

	public SoundManager soundManager;

	// Use this for initialization
	void Start () {

		SpellCreator = GameObject.Find ("SpellListener");

		FloorObj = GameObject.Find ("FloorSprite");
		soundManager =Camera.main.GetComponent<MainMenu>().persistantSoundManager.GetComponent<SoundManager> ();

		soundManager.startTornado ();
	}
	
	// Update is called once per frame
	void Update () {

		destroyTime -= Time.deltaTime;

		if (destroyTime < 0) {

			Destroy (this.gameObject);
		}

	}

}