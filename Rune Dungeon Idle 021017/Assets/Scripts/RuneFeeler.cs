﻿using UnityEngine;
using System.Collections;

public class RuneFeeler : MonoBehaviour {
	public MeteorCode OwnerObject;
	// Use this for initialization

	public void OnTriggerEnter2D(Collider2D c){

		if (c.tag == "Meteor") {
			OwnerObject.AddNeighbors(c.transform.gameObject);
		}
	}

	public void OnTriggerExit2D(Collider2D c){
		OwnerObject.RemoveNeighbors (c.transform.gameObject);
	}
}
