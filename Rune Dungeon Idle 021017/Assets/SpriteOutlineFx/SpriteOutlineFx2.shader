﻿Shader "Sprite Shadow/Sprite Outline Shadow"
{
    Properties
    {
        [PerRendererData] _MainTex  ("Sprite Texture", 2D) = "white" {}
        _Distance ("Distance", Float) = 1
        _Color    ("Color", Color) = (1, 0, 0, 0)
        _Cutoff ("Alpha Cutoff", Range(0,1)) = 0.45
    }

    CGINCLUDE

    #include "UnityCG.cginc"

    sampler2D _MainTex;
    float4 _MainTex_TexelSize;
    float _Distance;
    half4 _Color;

    half4 frag(v2f_img i) : SV_Target 
    {
        // Simple sobel filter for the alpha channel.
        float d = _MainTex_TexelSize.xy * _Distance;

        half a1 = tex2D(_MainTex, i.uv + d * float2(-1, -1)).a;
        half a2 = tex2D(_MainTex, i.uv + d * float2( 0, -1)).a;
        half a3 = tex2D(_MainTex, i.uv + d * float2(+1, -1)).a;

        half a4 = tex2D(_MainTex, i.uv + d * float2(-1,  0)).a;
        half a6 = tex2D(_MainTex, i.uv + d * float2(+1,  0)).a;

        half a7 = tex2D(_MainTex, i.uv + d * float2(-1, +1)).a;
        half a8 = tex2D(_MainTex, i.uv + d * float2( 0, +1)).a;
        half a9 = tex2D(_MainTex, i.uv + d * float2(+1, +1)).a;

        float gx = - a1 - a2*2 - a3 + a7 + a8*2 + a9;
        float gy = - a1 - a4*2 - a7 + a3 + a6*2 + a9;

        float w = sqrt(gx * gx + gy * gy) / 4;

        // Mix the contour color.
        half4 source = tex2D(_MainTex, i.uv);
        return half4(lerp(source.rgb, _Color.rgb, w), 1);
    }

    ENDCG 

    Subshader
    {
    	Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

        ZTest Always Cull Off ZWrite Off Lighting Off Blend One OneMinusSrcAlpha
        Fog { Mode off }      
        CGPROGRAM
        #pragma vertex vert_img
        #pragma fragment frag
        ENDCG

        CGPROGRAM
		#pragma surface surf Lambert vertex:vert nofog keepalpha addshadow fullforwardshadows

		fixed _Cutoff;

		struct Input
		{
			float2 uv_MainTex;
			fixed4 color;
		};
		
		void vert (inout appdata_full v, out Input o)
		{
			v.vertex = UnityPixelSnap (v.vertex);
			
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.color = v.color;
		}

		void surf (Input IN, inout SurfaceOutput o)
		{
			float2 uv = IN.uv_MainTex;
			fixed4 c = tex2D (_MainTex, uv) * IN.color;

			o.Albedo = c.rgb * c.a;
			o.Alpha = c.a;

			clip(o.Alpha - _Cutoff);
		}
		ENDCG
    }

    Fallback "Transparent/VertexLit"
}
